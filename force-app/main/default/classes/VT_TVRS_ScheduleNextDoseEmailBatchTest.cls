/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-30-2021
 * @last modified by  : Mohit Karani
**/
@isTest
public with sharing class VT_TVRS_ScheduleNextDoseEmailBatchTest {
    
    @TestSetup
    static void makeData(){
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String citizenCovidId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        
        Account createAccountTestSite1 = TestDataFactoryAccount.createAccount('VaccinationSite', testingCenter, false);
        createAccountTestSite1.BillingCity = 'testCity';
        createAccountTestSite1.BillingCountry = 'testCountry';
        createAccountTestSite1.BillingState = 'testState';
        createAccountTestSite1.BillingStreet = 'testStreet';
        createAccountTestSite1.BillingPostalCode = '12345';
        createAccountTestSite1.Event_Process__c = 'Vaccination';
        createAccountTestSite1.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite1.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite1;

        Contact con = new Contact (
            AccountId = createAccountTestSite1.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-44151',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = createAccountTestSite1.Id,
            RecordTypeId = citizenCovidId,
            what_was_the_last_date_of_contact__c = Date.newInstance(2020, 2, 20),
            Did_you_have_a_nasal_swab__c = 'Yes	',
            What_was_your_test_result__c = 'Negative',
            Approximately_what_date_were_you_tested__c = Date.newInstance(2020, 3, 3),
            Had_contact_with_anyone_diagon__c = 'Yes',
            Do_you_take_prescription_medications__c = '	Yes',
            medication_for_fever_in_the_past_3_days__c = 'Yes',
            First_Date_you_have_been_feeling_sick__c = Date.newInstance(2020, 3, 1) ,
            First_date_you_began_feeling_well__c= Date.newInstance(2020, 3, 10),
            Fever_Cough_or_Difficulty_breathing__c = 'Yes',
            Sick_since_Jan_1_2020__c = 'Yes',
            Opt_out_for_SMS_Updates__c = true,
            HasOptedOutOfEmail = false,
            Current_Step__c = 1,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false,
            Stop_Second_Dose_Notification__c=false,
            MailingState = 'VT'
        );
        insert con;
		Pre_Registration_Group__c preRegGroup = new  Pre_Registration_Group__c();
        preRegGroup.Status__c = 'Activate';
        preRegGroup.Group_Type__c = 'Age-Based';
        preRegGroup.Email_Acknowledgement__c  = true;
        insert preRegGroup;
        Pre_Registration__c preReg = new Pre_Registration__c(
            Contact__c=con.Id,
            Is_Immune_Weak__c='No',
            Pre_Registration_Group__c  = preRegGroup.Id
        );
        insert preReg;

        Private_Access__c privateAccess = new Private_Access__c(
            Name = 'Private Access',
            Age_Based__c = 40,
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-2',
            Min__c = 7,
            Max__c=10,
            Requires_Followup_Dose__c='Yes',
            Weak_Immune_Min_Days__c=0,
            Is_Alternate_Dose_Available__c=true,
            Alternate_Doses__c='Pfizer',
            Is_Active__c  = true
        );
        insert privateAccess;
        
         Private_Access__c alternatPrivateAccess = new Private_Access__c(
            Name = 'Private Access',
            Age_Based__c = 40,
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Pfizer',
            Vaccine_Type__c = 'Vaccination-3',
            Min__c = 7,
            Max__c=10,
            Requires_Followup_Dose__c='Yes',
            Weak_Immune_Min_Days__c=0,
            Is_Active__c  = true
        );
        insert alternatPrivateAccess;
        
        Private_Access_Assignment__c alternateDosePrivateAccessAssignment = new Private_Access_Assignment__c();
        alternateDosePrivateAccessAssignment.Pre_Registration_Groups__c = preRegGroup.Id;
        alternateDosePrivateAccessAssignment.Private_Access_Groups__c  = alternatPrivateAccess.Id;
        alternateDosePrivateAccessAssignment.Active__c = true;
        insert alternateDosePrivateAccessAssignment;

        VTS_Event__c event = new VTS_Event__c( 
            Status__c = 'Open',
            Location__c = createAccountTestSite1.Id,
      		Private_Access__c = privateAccess.Id,
            Start_Date__c = Date.Today() ,
            End_Date__c = Date.Today() + 10                  
        );
        insert event;

        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
        createAppointment.Lab_Center__c = createAccountTestSite1.id;
        createAppointment.Status__c = 'Completed';
        createAppointment.Event__c = event.Id;
        createAppointment.Appointment_Complete_Date__c=System.now();
        createAppointment.Email_Flow__c='Prior Reminder';
        createAppointment.Second_Dose_Reminder_Completed__c=false;

        insert createAppointment;

    }

    @isTest 
    static void testNextDoseBatchCase1() {
        Test.startTest();
            Database.executeBatch(new VT_TVRS_ScheduleNextDoseEmailBatch(),5);
        Test.stopTest();
        
        Appointment__c app = [SELECT Id,Name,Email_Flow__c,
                            Event__r.Private_Access__r.Requires_Followup_Dose__c,
                            Event__r.Private_Access__r.Vaccine_Type__c,Next_Dose_Notification_Process__c
                            FROM Appointment__c 
                            WHERE Event__r.Private_Access__r.Requires_Followup_Dose__c='Yes'
                            AND Event__r.Private_Access__r.Vaccine_Type__c='Vaccination-2' LIMIT 1];
        
        System.assertEquals('Send', app.Next_Dose_Notification_Process__c,'ERROR');
    }
}