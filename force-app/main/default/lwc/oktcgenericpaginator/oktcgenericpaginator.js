import {
    LightningElement,
    api,
    track
} from 'lwc';
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';
import getRecords from '@salesforce/apex/OKTC_GenericPaginatorController.retrieveRecords';

export default class Oktcgenericpaginator extends LightningElement {
    @api objectName = "User";
    @api columns = [];
    @api selectedRecordsIds = [];
    @api buttonsLabel = [];
    @api pageSizeOptions = [10, 25, 50, 100, 200];
    @api numberofcolumns;
    @api previousPageNo = 0;
    @api whereClause = "";
    @api recId = "";
    @api recVal = "";
    @api placeholder = "Search..";
    @api searchStr = "";
    @api sortBy = "FirstName";
    @api sortDir = "ASC";
    @api sharing = "With";
    @api norecordMessage = "No records found.";
    @api showAllPaginatorButtons = false;
    @api tableBodyId = "tableBody";
    @api tableClass = "property-table slds-table slds-table_cell-buffer slds-table_bordered";
    @api showQuery = false;
    @api checkboxButtonClass;
    @api checkboxButtonVariant;
    @api selectedTabName;
    @track records = [];
    @track rows = [];
    @track fromCount;
    @track toCount;
    @track totalCount;
    @track pageSize = 10;
    @track newPageNo = 1;
    @track maxPageNo = 1;
    @track showSpinner;
    @track query = '';
    selectedRecordId = [];
    @api checkboxButtonLabel = '';
    @api showCheckbox = false;
    allRecordMap = new Map();
    allRecords = [];
    @api
    get resetTable() {
        return false;
    }
    set resetTable(val) {
        if (val) {
            this.retrieveRecords();
        }
    }
    @api
    get reloadTable() {
        return false;
    }
    set reloadTable(val) {
        if (val) {
            this.resetPagination();
        }
    }


    get norows() {
        return this.rows.length > 0;
    }

    connectedCallback() {
        this.retrieveRecords();
    }

    @api
    retrieveRecords() {
        this.showSpinner = true;
        let columns = this.columns;
        let fields = [];
        columns.forEach(function (column, index) {
            if (column.name)
                fields.push(column.name);
        });

        fields.push('Event__r.Name');
        fields.push('Event__r.Start_Date__c');
        fields.push('Event__r.End_Date__c');

        if (this.whereClause.includes('Completed')) {
            fields.push('Event__r.Private_Access__r.Vaccine_Type__c');
            fields.push('Event__r.Private_Access__r.Requires_Followup_Dose__c');
        }

        let parameters = {
            'objectName': this.objectName,
            'fields': fields,
            'searchStr': this.searchStr,
            'whereClause': this.whereClause,
            'previousPageNo': this.previousPageNo,
            'newPageNo': this.newPageNo,
            'limit': this.pageSize,
            'sortBy': this.sortBy,
            'sortDir': this.sortDir,
            'recId': this.recId,
            'recVal': this.recVal,
            'maxPageNo': this.maxPageNo,
            'sharing': this.sharing,
        };

        let calloutParams = {
            "parameters": parameters
        };
        getRecords(calloutParams).then(result => {
                let rec = JSON.parse(JSON.stringify(result.records));
                this.allRecords = JSON.parse(JSON.stringify(result.allrecords));
                let vtsEvents = {};
                rec.forEach(function (row) {
                    if (row.Event__c) {
                        row.Event__r.Description__c = (row.Event__r.Description__c ? row.Event__r.Description__c : row.Event__r.Name) + ' (' + row.Event__r.Start_Date__c + ' ' + row.Event__r.End_Date__c + ')';
                        vtsEvents[row.Event__c] = row.Event__r.Description__c;
                    }
                });

                this.records = rec;
                this.totalCount = result.totalCount;
                this.query = result.query;
                this.setFooter();
                this.createTable();
                this.resetFlag();

                if (location.href.includes('TestingSite') === false) {
                }
            })
            .catch(error => {
                this.error = error;
                showAsyncErrorMessage(this, error);
            });
    }

    searchRecords(event) {
        let searchTimeout = this.searchTimeout;
        if (searchTimeout) {
            clearTimeout(searchTimeout);
        }
        this.searchStr = event.target.value;

        searchTimeout = window.setTimeout(() => {
            this.retrieveRecords();
            this.searchTimeout = null;
        }, 300);
        this.searchTimeout = searchTimeout;
        this.resetPagination();
    }
    searchTimeout;

    setPageSize(event) {
        this.pageSize = event.target.value;
        this.resetPagination();
    }
    handleButtonClick(event) {
        let buttonLabel = event.target.label;
        if (buttonLabel != this.newPageNo) {
            this.previousPageNo = this.newPageNo;
            let records = this.records;
            if (buttonLabel === 'Next') {
                this.newPageNo = this.newPageNo + 1;
                if (records) {
                    this.recId = records[records.length - 1].Id;
                    this.recVal = records[records.length - 1][this.sortBy];
                }
            } else if (buttonLabel === 'Previous') {
                this.newPageNo = this.newPageNo - 1;
                if (records) {
                    if (this.newPageNo === 1) {
                        this.previousPageNo = 0;
                        this.recId = '';
                        this.recVal = '';
                    } else if (records) {
                        this.recId = records[0].Id;
                        this.recVal = records[0][this.sortBy];
                    }
                }
            } else {
                this.newPageNo = parseInt(event.target.label);
                if (this.newPageNo === 1) {
                    this.previousPageNo = 0;
                    this.recId = '';
                    this.recVal = '';
                } else if (parseInt(this.previousPageNo) > parseInt(this.newPageNo)) {
                    if (records) {
                        this.recId = records[0].Id;
                        this.recVal = records[0][this.sortBy];
                    }
                } else {
                    if (records) {
                        this.recId = records[records.length - 1].Id;
                        this.recVal = records[records.length - 1][this.sortBy];
                    }
                }
            }
            this.retrieveRecords();
        }
    }
    handleAction(event) {
        let data = {
            recordId: event.target.dataset.id,
            actionName: event.target.dataset.action,
            selectedTabName: this.selectedTabName
        };

        this.customAction(data);
    }

    handleCheckboxChange(event) {
        let temparr = [];
        temparr = [...this.selectedRecordId];
        if (event.target.checked) {
            if (temparr.indexOf(event.target.dataset.id) === -1) {
                let anyId = [];
                anyId.push(event.target.dataset.id);
                temparr.push(event.target.dataset.id);
            }
        } else {
            if (temparr.indexOf(event.target.dataset.id) !== -1) {
                temparr.splice(temparr.indexOf(event.target.dataset.id), 1);
            }
        }
        this.selectedRecordId = [...temparr];
    }
    handleCheckboxButton() {
        this.dispatchEvent(new CustomEvent('checkboxdata', {
            detail: this.selectedRecordId
        }));
    }



    setFooter() {
        this.fromCount = ((parseInt(this.newPageNo) - 1) * parseInt(this.pageSize)) + 1;
        let toCount = parseInt(this.fromCount) + parseInt(this.pageSize) - 1;
        let totalCount = parseInt(this.totalCount);
        let maxPageNo = parseInt(totalCount / parseInt(this.pageSize)) + ((totalCount % parseInt(this.pageSize)) > 0 ? 1 : 0);
        if (maxPageNo === parseInt(this.newPageNo))
            toCount = totalCount
        this.maxPageNo = maxPageNo;
        this.toCount = toCount;
        let buttonsLabel = [];
        if (this.newPageNo <= 4) {
            buttonsLabel.push({
                label: 'Previous',
                disabled: this.newPageNo === 1,
                varient: 'Neutral'
            });
            for (let counter = 1; counter <= (maxPageNo > 5 ? 5 : maxPageNo); counter++) {
                buttonsLabel.push({
                    label: counter,
                    disabled: false,
                    varient: counter === parseInt(this.newPageNo) ? 'Brand' : 'Neutral'
                });
            }
            if (maxPageNo > 5) {
                if (maxPageNo !== 6)
                    buttonsLabel.push({
                        label: '...',
                        disabled: true,
                        varient: 'Base'
                    });
                buttonsLabel.push({
                    label: maxPageNo,
                    disabled: false,
                    varient: this.newPageNo === maxPageNo ? 'Brand' : 'Neutral'
                });
            }
            buttonsLabel.push({
                label: 'Next',
                disabled: this.newPageNo === this.maxPageNo || this.maxPageNo < 2,
                varient: 'Neutral'
            });
        } else if (maxPageNo - this.newPageNo <= 3) {
            buttonsLabel.push({
                label: 'Previous',
                disabled: this.newPageNo === 1,
                varient: 'Neutral'
            });
            buttonsLabel.push({
                label: '1',
                disabled: false,
                varient: 1 === this.newPageNo ? 'Brand' : 'Neutral'
            });
            if (maxPageNo > 6)
                buttonsLabel.push({
                    label: '...',
                    disabled: true,
                    varient: 'Base'
                });
            for (let counter = maxPageNo - 4; counter <= maxPageNo; counter++) {
                if (counter !== 1) {
                    buttonsLabel.push({
                        label: counter,
                        disabled: false,
                        varient: counter === this.newPageNo ? 'Brand' : 'Neutral'
                    });
                }
            }
            buttonsLabel.push({
                label: 'Next',
                disabled: this.newPageNo === this.maxPageNo || this.maxPageNo < 2,
                varient: 'Neutral'
            });
        } else {
            buttonsLabel.push({
                label: 'Previous',
                disabled: this.newPageNo === 1,
                varient: 'Neutral'
            });
            buttonsLabel.push({
                label: '1',
                disabled: false,
                varient: 1 === this.newPageNo ? 'Brand' : 'Neutral'
            });
            if (maxPageNo !== 6)
                buttonsLabel.push({
                    label: '...',
                    disabled: true,
                    varient: 'Base'
                });
            buttonsLabel.push({
                label: this.newPageNo - 1,
                disabled: false,
                varient: (this.newPageNo - 1) === this.newPageNo ? 'Brand' : 'Neutral'
            });
            buttonsLabel.push({
                label: this.newPageNo,
                disabled: false,
                varient: (this.newPageNo) === this.newPageNo ? 'Brand' : 'Neutral'
            });
            buttonsLabel.push({
                label: this.newPageNo + 1,
                disabled: false,
                varient: (this.newPageNo + 1) === this.newPageNo ? 'Brand' : 'Neutral'
            });
            if (maxPageNo !== 6)
                buttonsLabel.push({
                    label: '...',
                    disabled: true,
                    varient: 'Base'
                });
            buttonsLabel.push({
                label: maxPageNo,
                disabled: false,
                varient: this.newPageNo === maxPageNo ? 'Brand' : 'Neutral'
            });
            buttonsLabel.push({
                label: 'Next',
                disabled: this.newPageNo === this.maxPageNo || this.maxPageNo < 2,
                varient: 'Neutral'
            });
        }
        this.buttonsLabel = buttonsLabel;
    }
    resetPagination() {
        this.newPageNo = 1;
        this.previousPageNo = 0;
        this.recId = '';
        this.recVal = '';
        this.retrieveRecords();
    }


    scheduleNextDose() {
        let tempRecords = this.allRecords.filter(record => record.Event__c && record.Event__r.Private_Access__c && record.Event__r.hasOwnProperty('Private_Access__r') &&
            record.Event__r.Private_Access__r.hasOwnProperty('Vaccine_Type__c') && record.Event__r.Private_Access__r.hasOwnProperty('Requires_Followup_Dose__c'));
        for (let temp = 0; temp < tempRecords.length; temp++) {
            let appt = tempRecords[temp];
            if (this.allRecordMap.has(appt.Patient__c)) {
               
                if (appt.Event__r.Private_Access__r.Vaccine_Type__c.split('-')[1] > this.allRecordMap.get(appt.Patient__c).Event__r.Private_Access__r.Vaccine_Type__c.split('-')[1]) {
                    if (appt.Event__r.Private_Access__r.Requires_Followup_Dose__c === 'Yes') {
                        this.allRecordMap.set(appt.Patient__c, appt);
                    } else {
                        this.allRecordMap.delete(appt.Patient__c)
                    }
                }
            } else if (appt.Event__r.Private_Access__r.Requires_Followup_Dose__c === 'Yes') {
                this.allRecordMap.set(appt.Patient__c, appt);
            }
        }
    }
    createTable() {
        this.scheduleNextDose();
        let records = this.records;
        let columns = JSON.parse(JSON.stringify(this.columns));
        this.rows = [];
        let rows = [];
        let _this = this;
        records.forEach(function (record, index) {
            let isSelected = _this.selectedRecordId.indexOf(record.Id) !== -1;
            let row = {
                id: record.Id,
                columns: [],
                isChecked: isSelected
            };

            columns.forEach(function (column) {
                let val = '';
                if (column.name.indexOf('.') !== -1) {
                    if (record[column.name.split('.')[0]]) {
                        if (column.type === 'time')
                            val = _this.msToTimeAMPM(record[column.name.split('.')[0]][column.name.split('.')[1]]);
                        else
                            val = record[column.name.split('.')[0]][column.name.split('.')[1]];
                    }
                } else if (column.type === 'time') {
                    val = _this.msToTimeAMPM(record[column.name]);
                } else {
                    val = record[column.name];
                }
                let label = '';
                if (column.type === 'text' || column.type === 'time') {
                    label = column.label
                }
                let recordColumn = {
                    label: label,
                    value: val !== undefined && val.hasOwnProperty('Vaccine_Class_Name__c') ? val.Vaccine_Class_Name__c : val !== undefined && val.hasOwnProperty('Id') ? '' : val,
                    isText: column.type === 'text' || column.type === 'time',
                    isButton: column.type === 'button',
                    isButtons: column.type === 'buttons',
                    action: column.action,
                    varient: column.varient,
                    buttonLabel: column.buttonlabel,
                    buttons: column.buttons,
                    class: 'slds-truncate slds-col slds-cell-wrap ' + column.class
                };

                // Adding Second Dose button dynamically

                if (_this.allRecordMap.has(record.Patient__c) && _this.allRecordMap.get(record.Patient__c).Id === record.Id && recordColumn.buttonLabel) {
                    recordColumn.isButton = true;
                }
                row.columns.push(recordColumn);
            });
            rows.push(row);
        });
        this.rows = rows;
        this.showSpinner = false;
    }


    customAction(data) {
        this.dispatchEvent(new CustomEvent('paginatoraction', {
            detail: data
        }));
    }

    resetFlag() {
        this.dispatchEvent(new CustomEvent('resetflag', {
            detail: false
        }));
    }

    msToTimeAMPM(s) {
        if (s === '' || s === null || typeof s === 'undefined')
            return '';
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;
        let radian = 'AM';
        if (hrs > 12) {
            radian = 'PM';
            hrs = hrs - 12;
        }

        return this.pad(hrs) + ':' + this.pad(mins) + ' ' + radian;
    }

    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }
}