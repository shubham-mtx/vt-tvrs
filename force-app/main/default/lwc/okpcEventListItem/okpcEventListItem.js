import { LightningElement, api } from 'lwc';

export default class OkpcEventListItem extends LightningElement {
    @api eventRecord;

    get eventSyleClass() {
        return 'slds-box slds-m-top_small event-container ' +
            (this.eventRecord.isSelectable ? this.eventRecord.isSelected ? 'event-selected' : '' : 'event-disabled');
    }

    get ariaChecked() {
        return this.eventRecord.isSelected;
    }


    handleKeyEventSelect(event){
        if (event.keyCode === 13 || event.keyCode === 32) {
            this.handleEventSelect(event); // this can be the function which is being called for the onclick event
        }
    }

    handleEventSelect(event) {
        if (this.eventRecord.isSelectable) {
            this.dispatchEvent(new CustomEvent('eventselected', { detail: this.eventRecord.Id }));
        }
    }
}