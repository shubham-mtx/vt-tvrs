import {
    LightningElement,
    track,
    api
} from 'lwc';
import getContactDetails from "@salesforce/apex/OkpcContactInformationController.getContactDetails";
import saveContactDetails from "@salesforce/apex/OkpcContactInformationController.saveContactDetails";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import fetchPicklist from "@salesforce/apex/OkpcContactInformationController.fetchPicklist";


export default class OkpcContactInfo extends LightningElement {
    @track dataObj = {};
    @track extraWrapper = {};
    @track contactId;
    @track showSpinner = false;
    @track disableInputs = true;
    @track stateOptions = [];
    @track salutations = [];
    @track LangOptions = [];
    @track raceOptions = [];
    @track maritialStatusOptions = [];
    @track phoneTypeOptions;
    @track age;
    @api currentStep;
    @track todayDate;
    @track selectOneCommunicationError = false;
    @api dependentContactId = '';
    @api isDependentContact = false;

    fields = ["Name", "Birthdate", "Age__c", "Gender__c", "Street_Address1__c", "Street_Address_2__c", "City__c", "State__c", "ZIP__c", "Email", "MobilePhone",'Phone_Type__c'];

    get genderOptions() {
        return [{
                label: "Male",
                value: "Male"
            },
            {
                label: "Female",
                value: "Female"
            },
            {
                label: "Other",
                value: "Other"
            }
        ];
    }
    get resultCopyOptions(){
        return [{
            label: "E-Mail",
            value: "E-Mail"
        },
        {
            label: "USPS Letter",
            value: "USPS Letter"
        },
        {
            label: "Phone Call",
            value: "Phone Call"
        }
    ];
    }

     
    @track registrationValues = [{
            label: 'Myself',
            value: 'Myself'
        },
        {
            label: 'Someone Else',
            value: 'Someone Else'
        }
    ];

    get ismyself() {
        return this.dataObj.registrationFor == 'Myself';
    }

    get onlyYesNoOptions() {
        return [{
                label: "Yes",
                value: "Yes"
            },
            {
                label: "No",
                value: "No"
            },
        ];
    }

    connectedCallback() {
        this.getContact();
        this.todayDate = this.getTodayDate();
        fetchPicklist({
                objectName: "Contact",
                fieldName: "State__c"
            })
            .then((result) => {
                this.stateOptions = result;
            })
            fetchPicklist({
                objectName: "Contact",
                fieldName: "Salutation"
            })
            .then((result) => {
                this.salutations = result;
            })
            fetchPicklist({
                objectName: "Contact",
                fieldName: "Primary_Language__c"
            })
            .then((result) => {
                this.LangOptions = result;
            })
            fetchPicklist({
                objectName: "Contact",
                fieldName: "Race__c"
            })
            .then((result) => {
                this.raceOptions = result;
            })
            fetchPicklist({
                objectName: "Contact",
                fieldName: "Marital_Status__c"
            })
            .then((result) => {
                this.maritialStatusOptions = result;
            })
            .catch((error) => {
                let message = error.message || error.body.message;
                console.log(message);
            });
            fetchPicklist({
                objectName: "Contact",
                fieldName: "Phone_Type__c"
            })
            .then((result) => {
                this.phoneTypeOptions = result;
            })
            .catch((error) => {
                let message = error.message || error.body.message;
                console.log(message);
            });
    }

    @api
    editContacts() {
        this.disableInputs = false;
    }

    getContact() {
        this.showSpinner = true;
        if( this.dependentContactId == '') {
            getContactDetails()
            .then(result => {

                // console.log('data=>', JSON.stringify(result));
                this.dataObj.firstName = result.firstName;
                this.dataObj.middleName = result.middleName;
                this.dataObj.lastName = result.lastName;
                this.dataObj.email = result.email;
                this.dataObj.phone = result.phone;
                if(result.phone){
                    this.dataObj.phone = this.formatPhoneInput(this.dataObj.phone);
                }
                this.dataObj.phoneType = result.phoneType;
                this.dataObj.patientId = result.patientId;
                this.contactId = result.contactId;
                this.dataObj.contactId = this.contactId;
                this.dataObj.address2 = result.address2;
                this.dataObj.address1 = result.address1;
                this.dataObj.dob = result.dob;
                this.dataObj.city = result.city;
                this.dataObj.state = result.state;
                this.dataObj.zip = result.zip;
                this.dataObj.gender = result.gender;
                this.dataObj.smsCheckbox = result.smsCheckbox ? 'Yes' : 'No';
                this.dataObj.emailCheckbox = result.emailCheckbox ? 'Yes' : 'No';
                this.dataObj.eighteenPlus = result.eighteenPlus;
                this.dataObj.testResultCopy = result.testResultCopy;
                this.dataObj.suffix = result.suffix;
                this.dataObj.stateofBirth = result.stateofBirth;
                this.dataObj.countryofBirth = result.countryofBirth;
                this.dataObj.race = result.race;
                this.dataObj.ethnicity = result.ethnicity;
                this.dataObj.primaryLanguage = result.primaryLanguage;
                this.dataObj.maritialStatus = result.maritialStatus;
                this.dataObj.registrationFor = result.registrationFor ? result.registrationFor : 'Myself';
                this.dataObj.registrationForAgree=result.registrationForAgree;
                if (!this.dataObj.state) {
                    this.dataObj.state = "VT";
                }
                if (this.dataObj.dob) {
                    this.calculateAge(this.dataObj.dob);
                }
                // this.extraWrapper = {...this.dataObj};
                console.log('>>' + JSON.stringify( this.dataObj));
                this.showSpinner = false;

            })
            .catch(error => {
                console.log('error=>', JSON.stringify(error));
                this.showSpinner = false;
            })
        }else{
            getContactDetails({ contactid : this.dependentContactId})
            .then(result => {

                // console.log('data=>', JSON.stringify(result));
                this.dataObj.firstName = result.firstName;
                this.dataObj.middleName = result.middleName;
                this.dataObj.lastName = result.lastName;
                this.dataObj.email = result.email;
                this.dataObj.phone = result.phone;
                if(result.phone){
                    this.dataObj.phone = this.formatPhoneInput(this.dataObj.phone);
                }
                this.dataObj.phoneType = result.phoneType;
                this.dataObj.patientId = result.patientId;
                this.contactId = result.contactId;
                this.dataObj.contactId = this.contactId;
                this.dataObj.address2 = result.address2;
                this.dataObj.address1 = result.address1;
                this.dataObj.dob = result.dob;
                this.dataObj.city = result.city;
                this.dataObj.state = result.state;
                this.dataObj.zip = result.zip;
                this.dataObj.gender = result.gender;
                this.dataObj.smsCheckbox = result.smsCheckbox ? 'Yes' : 'No';
                this.dataObj.emailCheckbox = result.emailCheckbox ? 'Yes' : 'No';
                this.dataObj.eighteenPlus = result.eighteenPlus;
                this.dataObj.testResultCopy = result.testResultCopy;
                this.dataObj.suffix = result.suffix;
                this.dataObj.stateofBirth = result.stateofBirth;
                this.dataObj.countryofBirth = result.countryofBirth;
                this.dataObj.race = result.race;
                this.dataObj.ethnicity = result.ethnicity;
                this.dataObj.primaryLanguage = result.primaryLanguage;
                this.dataObj.maritialStatus = result.maritialStatus;
                this.dataObj.registrationFor = result.registrationFor ? result.registrationFor : 'Myself';
                this.dataObj.registrationForAgree=result.registrationForAgree;
                if (!this.dataObj.state) {
                    this.dataObj.state = "VT";
                }
                if (this.dataObj.dob) {
                    this.calculateAge(this.dataObj.dob);
                }
                // this.extraWrapper = {...this.dataObj};
                console.log('>>' + JSON.stringify( this.dataObj));
                this.showSpinner = false;

            })
            .catch(error => {
                console.log('error=>', JSON.stringify(error));
                this.showSpinner = false;
            })
        }
    }

    handleInputChange(event) { 
        this.selectOneCommunicationError = false;
        if (event.target.name == 'firstName') {
            this.dataObj.firstName = event.target.value;
        } else if (event.target.name == 'middleName') {
            this.dataObj.middleName = event.target.value;
        } else if (event.target.name == 'lastName') {
            this.dataObj.lastName = event.target.value;
        } else if (event.target.name == 'email') {
            this.dataObj.email = event.target.value;
        }else if (event.target.name == 'Suffix') {
            this.dataObj.suffix = event.target.value;
        }else if (event.target.name == 'StateofBirth') {
            this.dataObj.stateofBirth = event.target.value;
        }else if (event.target.name == 'CountryofBirth') {
            this.dataObj.countryofBirth = event.target.value;
        }else if (event.target.name == 'Ethnicity') {
            this.dataObj.ethnicity = event.target.value;
        }else if (event.target.name == 'PrimaryLanguage') {
            this.dataObj.primaryLanguage = event.target.value;
        }else if (event.target.name == 'Race') {
            this.dataObj.race = event.target.value;
        }else if (event.target.name == 'MaritialStatus') {
            this.dataObj.maritialStatus = event.target.value;
        }else if (event.target.name == 'phone') {
            this.dataObj.phone = event.target.value;
            this.formatPhone(event.target);
        }else if (event.target.name == 'phoneType') {
            this.dataObj.phoneType = event.target.value;
        }else if (event.target.name == 'emailCheckbox') {
            this.dataObj.emailCheckbox = event.target.value;
        } else if (event.target.name == 'smsCheckbox') {
            this.dataObj.smsCheckbox = event.target.value;
        } else if (event.target.name == 'patientId') {
            this.dataObj.patientId = event.target.value;
        } else if (event.target.name == 'gender') {
            this.dataObj.gender = event.target.value;
        } else if (event.target.name == 'zip') {
            this.dataObj.zip = event.target.value;
        } else if (event.target.name == 'state') {
            this.dataObj.state = event.target.value;
        } else if (event.target.name == 'city') {
            this.dataObj.city = event.target.value;
        } else if (event.target.name == 'address1') {
            this.dataObj.address1 = event.target.value;
        } else if (event.target.name == 'address2') {
            this.dataObj.address2 = event.target.value;
        } 
        else if (event.target.name == 'eighteenPlus'){
            this.dataObj.eighteenPlus = event.target.checked;
        }
        else if (event.target.name == 'testResultCopy') {
            this.dataObj.testResultCopy = event.target.value;
        }else if (event.target.name == 'registrationFor') {
            this.dataObj.registrationFor = event.target.value;
            console.log('Reg for :' +  this.dataObj.registrationFor);
        }
        else if(event.target.name =='registrationForAgree') {
            this.dataObj.registrationForAgree  = event.target.checked ? true : false; 
            console.log('RegFor Agree :' + this.dataObj.registrationForAgree);
        }
        else if (event.target.name == 'dob') {
            this.dataObj.dob = event.target.value;
            this.calculateAge(event.target.value);
            if (!this.validateDate(event.target.value)) {
                this.dataObj.dob = '';
            }
        }
        console.log(JSON.stringify(this.dataObj));
    }

    @api
    saveContacts() {
        console.log('data=>', JSON.stringify(this.dataObj));
        this.dataObj.contactId = this.contactId;
        if (this.dataObj.zip == "" || typeof this.dataObj.zip == 'undefined') {
            this.dataObj.zip = null;
        }
        if ((this.dataObj.registrationFor == 'Someone Else') && (!this.dataObj.registrationForAgree)) {
            return;
        }

         if ( this.dataObj.emailCheckbox != 'Yes'  && this.dataObj.smsCheckbox != 'Yes') {
             let tempObj = {};
             tempObj.message = "Please Select atleast one way to recieve notifications.";
             tempObj.status = "error";
             //this.sendEventToParent(tempObj);
             this.selectOneCommunicationError = true;
             return;
         }

        this.dataObj.emailCheckbox = this.dataObj.emailCheckbox == 'Yes' ? true : false;
        this.dataObj.smsCheckbox = this.dataObj.smsCheckbox == 'Yes' ? true : false;

        // if (this.dataObj.smsCheckbox == true && (this.dataObj.phone == '' || typeof this.dataObj.phone == 'undefined')) {
        //     let tempObj = {};
        //     tempObj.message = "Please Complete the phone field to recieve SMS notifications";
        //     tempObj.status = "error";
        //     this.sendEventToParent(tempObj);
        //     return;
        // }

        // if (!this.validateTextField(this.dataObj.lastName)) {
        //     return;
        // }

        // if (!this.validateTextField(this.dataObj.phone)) {
        //     return;
        // }

        // if (!this.validateTextField(this.dataObj.email)) {
        //     return;
        // }

        // if(!this.validateEmail(this.dataObj.email)){
        //     return;
        // }

        // if(!this.validatePhone(this.dataObj.phone)){
        //     return;
        // }
        // if(this.dataObj.zip){
        //     if(!this.validateZip(this.dataObj.zip)){
        //         return;
        //     }
        // }

        if (!this.isValid()) {
            return;
        }

        saveContactDetails({
                jsonData: JSON.stringify(this.dataObj),
                currentstep: this.currentStep,
                isDependentContact : this.isDependentContact
            })
            .then(result => {
                console.log('Successfully Saved');
                this.disableInputs = true;
                // this.extraWrapper = {...this.dataObj};
                let tempObj = {};
                tempObj.message = "saved successfully";
                tempObj.status = "success";
                this.sendEventToParent(tempObj);
            })
            .catch(error => {
                console.log('error in saving: ', JSON.stringify(error));
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Error',
                }));
            })
    }

    @api
    handleCancelButton() {
        this.disableInputs = true;
        // this.dataObj = {...this.extraWrapper};
    }

    calculateAge(date) {
        if (!this.validateDate(date)) {
            return;
        }
        var Bday = +new Date(date);
        this.age = ~~((Date.now() - Bday) / (31557600000));
    }

    sendEventToParent(obj) {

        const selectedEvent = new CustomEvent("contactsave", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }

    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }

    validateTextField(field) {
        if (field == '' || typeof field == 'undefined') {
            let tempObj = {};
            tempObj.message = "Please complete the required fields.";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return false;
        } else {
            return true;
        }
    }

    // formatPhone(obj) {
    //     let phone = obj.value;
    //     let formatedPhone='('+phone.substring(0,3)+')'+phone.substring(3,6)+'-'+phone.substring(6,10);
    // }

    validateEmail(email) {
        var regExpEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/;
        var isValid = email.match(regExpEmail);
        console.log("isValid", isValid);
        if (!isValid) {
            let tempObj = {};
            tempObj.message = "Please provide a correct format for email.";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
        }
        return isValid;
    }

    // validatePhone(phone) {
    //     var regExpPhone = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
    //     var isValid = phone.match(regExpPhone);
    //     console.log("isValid", isValid);
    //     if(!isValid){
    //         let tempObj = {};
    //         tempObj.message = "Please provide a correct format for phone.";
    //         tempObj.status = "error";
    //         this.sendEventToParent(tempObj);
    //     }
    //     return isValid;
    // }

    validateDate(date) {
        var regExpPhone = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
        var isValid = date.match(regExpPhone);
        console.log("isValid", isValid);
        if (!isValid) {
            let tempObj = {};
            tempObj.message = "Please provide a valid format for date";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
        }
        return isValid;
    }


    // validateZip(zip) {
    //     var regExpZip = /^[0-9]*$/;
    //     var isValid = (zip.match(regExpZip) && zip.length == 5);
    //     console.log("isValid", isValid);
    //     if(!isValid){
    //         let tempObj = {};
    //         tempObj.message = 'Please enter a valid format ("XXXXX") for Zip code.';
    //         tempObj.status = "error";
    //         this.sendEventToParent(tempObj);
    //     }
    //     return isValid;
    // }

    isValid() {
        let valid = true;
        let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        valid = isAllValid;
        return valid;
    }

    handlePhoneFocus(event) {
        event.target.value = this.dataObj.phone;
    }

    formatPhone(obj) {
        var numbers = obj.value.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj.value = "";
        for (var i = 0; i < numbers.length; i++) {
            obj.value += (char[i] || "") + numbers[i];
        }
    }
    formatPhoneInput(obj) {
        var numbers = obj.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj = "";
        for (var i = 0; i < numbers.length; i++) {
            obj += (char[i] || "") + numbers[i];
        }
        return obj;

    }
    

}