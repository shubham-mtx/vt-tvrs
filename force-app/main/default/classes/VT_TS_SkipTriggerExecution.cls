/**
 * @description       : 
 * @author            : Wasef Mohiuddin
 * @group             : 
 * @last modified on  : 02-16-2021
 * @last modified by  : Wasef Mohiuddin
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   02-15-2021   Wasef Mohiuddin   Initial Version
**/
public inherited sharing class VT_TS_SkipTriggerExecution {
    public static Boolean executeContactTrigger = true;
    public static Boolean executeAppointmentTrigger = true;
    public static Boolean executePreRegistrationGroupTrigger = true;
    public static Boolean executeEventChangeLogicFromLayout = true;
}