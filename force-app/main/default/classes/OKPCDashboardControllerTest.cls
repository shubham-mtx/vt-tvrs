/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-21-2022
 * @last modified by  : Vamsi Mudaliar
**/
@isTest
public class OKPCDashboardControllerTest {
    
    @TestSetup
    static void makeData(){
        List<Account> accList = new List<Account>();
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String defaultAccount = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Default').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.Name = 'Self Registration';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Event_Process__c ='Vaccination';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        accList.add(createAccountTestSite);
        
        Account createAccountTestSite2 = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite2.BillingCity = 'testCity';
        createAccountTestSite2.Name = 'Self Registration';
        createAccountTestSite2.RecordTypeId = defaultAccount;
        createAccountTestSite2.BillingCountry = 'testCountry';
        createAccountTestSite2.BillingState = 'testState';
        createAccountTestSite2.BillingStreet = 'testStreet';
        createAccountTestSite2.BillingPostalCode = '12345';
        createAccountTestSite2.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite2.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        accList.add(createAccountTestSite2);
        
        insert accList;
        Contact con = new Contact (
            AccountId = accList[0].id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            MailingCity = 'Tulsa',
            MailingStreet = '12 N Cheyenne Ave',
            MailingCountry = 'USA',
            MailingState = 'OK',
            MailingPostalCode = '74103',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con;
        
        Contact con2 = new Contact (
            AccountId = accList[1].id,
            LastName = 'portalTestUser',
            MiddleName = 'm',
            FirstName = 'test',
            Email = 'testportal@gmail.com',
            Birthdate = Date.newInstance(1998, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44251',
            Phone = '(702)379-4425',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            MailingCity = 'Tulsa',
            MailingStreet = '12 N Cheyenne Ave',
            MailingCountry = 'USA',
            MailingState = 'OK',
            MailingPostalCode = '74103',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false,
            Parent_Contact__c = con.Id
        );
        insert con2;




        Private_Access__c privateAccessCase2 = new Private_Access__c(
                Name = 'private test',
                Event_Process__c = 'Vaccination',
                Vaccine_Class_Name__c = 'Moderna',
                Vaccine_Type__c = 'Vaccination-2',
                Age_Based__c = 44,
                Requires_Followup_Dose__c = 'Yes',
                Is_Alternate_Dose_Available__c=true,
                Alternate_Doses__c='Pfizer;Moderna',
                Min__c = 0,
                Max__c = 2,
                Weak_Immune_Min_Days__c=0,
                Is_Active__c = true
        );
        insert privateAccessCase2;

        VTS_Event__c event = new VTS_Event__c( 
            Status__c = 'Open',
            Location__c = createAccountTestSite.Id,
      		Private_Access__c = privateAccessCase2.Id,
            Start_Date__c = Date.Today(),
            End_Date__c = Date.Today() + 10                  
        );
        insert event; 


        Pre_Registration_Group__c prGroup1 = new Pre_Registration_Group__c();
        prGroup1.Status__c = 'Activate';
        prGroup1.Group_Type__c = 'Chronic';
        prGroup1.Email_Acknowledgement__c = true;
        prGroup1.Verification_Delay__c = 72;
        prGroup1.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        insert prGroup1;
        

        Pre_Registration__c pr = new Pre_Registration__c(
            Contact__c = con.Id,
            Chronic_Condition__c='No',
            Pre_Registration_Group__c=prGroup1.Id
        );
        insert pr;

        Private_Access_Assignment__c pAssign = new Private_Access_Assignment__c(
            Private_Access_Groups__c = privateAccessCase2.Id,
            Pre_Registration_Groups__c = prGroup1.Id,
            Active__c = true
        );
        insert pAssign;



        List<Appointment__c> appointmentList = new List<Appointment__c>();
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        createAppointment.Status__c = 'Completed';
        createAppointment.Event__c=event.Id;
        createAppointment.Appointment_Complete_Date__c=System.now();
        appointmentList.add(createAppointment);
        
        insert appointmentList;
        
        Antibody_Testing__c antibodyTesting = TestDataFactoryAntiBody.createAppointment(true, createAccountTestSite.Id, appointmentList[0].Id, createSlot.Id, con.Id);

        TVRS_Portal_Content__c pcontent = new TVRS_Portal_Content__c(
            Active__c = true,
            Type__c='News Section',
            Start_Date__c = Date.today(),
            End_Date__c=Date.today().addDays(3),
            Content__c='This is some dummy Text',
            Sequence__c = 1
        );
        insert pcontent;

       // OKPCDashboardController.PortalContent obj = new OKPCDashboardController.PortalContent(pcontent);
       
    }
    
    // public static testMethod void retrieveResultTest(){
    //     UserRole userRole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'Management' Limit 1];
    //     User adminUser = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' and isActive=true Limit 1];
    //     adminUser.UserRoleId = userRole.Id;
    //     update adminUser;

    //     System.runAs(adminUser)
    //     {
    //         String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
            
    //         Account acc = [SELECT Id FROM Account WHERE RecordTypeId=:testingCenter];
    //         acc.Event_Process__c = 'Vaccination';
    //         update acc;

    //         Contact contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
    //         Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
    //         // Profile prfile = [select Id,name from Profile where Name = 'Covid Citizen User' limit 1];  
    //         // User newUser = new User(
    //         //     profileId = prfile.id,
    //         //     username = 'newUser@yahoo.com',
    //         //     email = 'pb@f.com',
    //         //     emailencodingkey = 'UTF-8',
    //         //     localesidkey = 'en_US',
    //         //     languagelocalekey = 'en_US',
    //         //     timezonesidkey = 'America/Los_Angeles',
    //         //     alias='nuser',
    //         //     lastname='lastname',
    //         //     contactId = contactList.Id
    //         // );
    //         // insert newUser;
            
    //         List<Private_Access__c> privateAccessCaseList = new List<Private_Access__c>();
            
            
    //         Private_Access__c privateAccessCase2 = new Private_Access__c(
    //             Name = 'private test',
    //             Event_Process__c = 'Vaccination',
    //             Vaccine_Class_Name__c = 'Moderna',
    //             Vaccine_Type__c = 'Vaccination-2',
    //             Age_Based__c = 44,
    //             Requires_Followup_Dose__c = 'Yes',
    //             Is_Alternate_Dose_Available__c=true,
    //             Alternate_Doses__c='Pfizer;Moderna',
    //             Min__c = 0,
    //             Max__c = 2,
    //             Weak_Immune_Min_Days__c=0,
    //             Is_Active__c = true
    //         );
    //         Private_Access__c privateAccessCase3 = new Private_Access__c(
    //             Name = 'private test',
    //             Is_Alternate_Dose_Available__c = true,
    //             Alternate_Doses__c = 'Moderna',
    //             Event_Process__c = 'Vaccination',
    //             Vaccine_Class_Name__c = 'Moderna',
    //             Vaccine_Type__c = 'Vaccination-3',
    //             Age_Based__c = 44,
    //             Requires_Followup_Dose__c = 'Yes',
    //             Min__c = 0,
    //             Max__c = 2,
    //             Is_Active__c = true
    //         );
    //         privateAccessCaseList.add(privateAccessCase2);
    //         privateAccessCaseList.add(privateAccessCase3);
    //         insert privateAccessCaseList;

    //         VTS_Event__c event2 = new VTS_Event__c( 
    //             Status__c = 'Open', 
    //             Location__c = acc.Id,
    //             Private_Access__c = privateAccessCase2.Id,
    //             Start_Date__c = Date.today() - 10
    //         );
    //         insert event2;


    //         Pre_Registration_Group__c rd = new Pre_Registration_Group__c(
    //             Name = 'COVID Testing',
    //             Group_Type__c = 'Chronic',
    //             Verification_Delay__c=2
    //         );
    //         insert rd;

    //         Pre_Registration__c preReg1 = new Pre_Registration__c();
    //         preReg1.BIPOC__c = 'Yes';
    //         preReg1.Contact__c = contactList.Id;
    //         preReg1.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
    //         preReg1.Is_Immune_Weak__c = 'No';
    //         insert preReg1;


    //         Private_Access_Assignment__c obj = new Private_Access_Assignment__c();
    //         obj.Private_Access_Groups__c = privateAccessCase2.Id;
    //         obj.Pre_Registration_Groups__c = rd.Id;
    //         obj.Active__c = true;
    //         insert obj;

    //         Appointment__c appointmentCase2 = new Appointment__c();
    //         appointmentCase2.Lab_Center__c = acc.Id;
    //         appointmentCase2.Patient__c = contactList.Id;
    //         appointmentCase2.Status__c = 'Completed';
    //         appointmentCase2.Event__c = event2.Id;
    //         appointmentCase2.Appointment_Complete_Date__c=System.now();

    //         //appointmentCase2.Appointment_Slot__c = appSlotRec.Id;
    //         insert appointmentCase2; 
            
    //        // Map<String,object> maoObj = OKPCDashboardController.retrieveResult();
    //         Map<String,List<OKPCDashboardController.PortalContent>> portalContent = OKPCDashboardController.retrievePortalContent();
            
    //     }
    // }
    
    @IsTest
    public static void testPortalContent(){
        Test.startTest();
            Map<String,List<OKPCDashboardController.PortalContent>> result = OKPCDashboardController.retrievePortalContent();
        System.assert(true,result!=null);
        Test.stopTest();
        
    }


    public static  testMethod void retrieveCurrentUserTest(){
        // List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        
        // Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        //     Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        // User newUser = new User(
        //     profileId = prfile.id,
        //     username = 'newUser@yahoo.com',
        //     email = 'pb@f.com',
        //     emailencodingkey = 'UTF-8',
        //     localesidkey = 'en_US',
        //     languagelocalekey = 'en_US',
        //     timezonesidkey = 'America/Los_Angeles',
        //     alias='nuser',
        //     lastname='lastname',
        //     contactId = contactList[0].id
        // );
        // insert newUser;
        UserRole userRole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'Management' Limit 1];
        User adminUser = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' and isActive=true Limit 1];
        adminUser.UserRoleId = userRole.Id;
        update adminUser;
        System.runAs(adminUser){
            List<User> userList =  OKPCDashboardController.retrieveCurrentUser();
            System.assertEquals(userList.isEmpty(), false);    
        }
    }
    
    public static  testMethod void fetchPicklistTest(){
        List < OKPCDashboardController.SelectOptionWrapper > wrapperList = OKPCDashboardController.fetchPicklist('Appointment__c', 'Status__c');
        System.assertEquals(wrapperList.isEmpty(), false);
    }
    
    public static  testMethod void getAllPicklistValueForVaccineTest(){
        Map<String,Object> picklistMaps = OKPCDashboardController.getAllPicklistValueForVaccine();
        System.assertEquals(picklistMaps.values().isEmpty(),false);
    }
    
    public static  testMethod void getAllPicklistValueForDemographicsTest(){
        Map<String,Object> picklistMaps = OKPCDashboardController.getAllPicklistValueForDemographics();
        System.assertEquals(picklistMaps.values().isEmpty(),false);
    }
    
    public static  testMethod void getAllPicklistValueTestingQuestionsTest(){
        Map<String,Object> picklistMaps = OKPCDashboardController.getAllPicklistValueTestingQuestions();
        System.assertEquals(picklistMaps.values().isEmpty(),false);
    }
    
    public static  testMethod void isPrivateAccessObjectTest(){
        Private_Access__c privateAccess = new Private_Access__c(
        	Name = 'private test',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'Yes',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        insert privateAccess;  
       Boolean result = OKPCDashboardController.isPrivateAccessObject(privateAccess.Id);
        System.assertEquals(result,true);
    }


    @isTest 
    static void testRetrieveResult() {
    
        Test.startTest();
                
            try{
              //  OKPCDashboardController.retrieveResultFilter(new List<String>{'Scheduled','Completed'},'Vaccination','60','Patient__r.Name');
                OKPCDashboardController.getAppointments('{"listStatus":["Scheduled","Completed"],"type":["Vaccination","Testing"],"days":"30","sortValue":"Patient__r.Name"}');
                
            }
            catch(Exception e){
                System.assert(true,'ERROR');
            }
        Test.stopTest();
    }

    @IsTest
    static void testGetCurrentUserDetail(){
        // String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        // Account acc = [SELECT Id FROM Account WHERE BillingCity='testCity' LIMIT 1];
        // Contact con = new Contact(LastName='LastTest',RecordTypeId=testingSiteId,AccountId=acc.Id);
        // insert con;

        // Profile pr = [SELECT Id FROM Profile WHERE Name IN :VT_Constants.TVRS_PROFILES LIMIT 1];

        // User user = new User();
        // user.ContactId = con.Id;
        
        // user.ProfileId = pr.Id;
        // user.EmailEncodingKey = 'ISO-8859-1';
        // user.LanguageLocaleKey = 'en_US';
        // user.TimeZoneSidKey = 'America/New_York';
        // user.LocaleSidKey = 'en_US';
        // user.FirstName = 'first';
        // user.LastName = 'last';
        // user.Username = 'guestUser@test1203200.com';
        // user.CommunityNickname = 'guestUser123';
        // user.Alias = 't1';
        // user.Email = 'guestUser@email.com';
        // user.IsActive = true;
        // insert user; 
        UserRole userRole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'Management' Limit 1];
        User adminUser = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' and isActive=true Limit 1];
        adminUser.UserRoleId = userRole.Id;
        update adminUser;

        Test.startTest();
        System.runAs(adminUser) {
            try{
                OKPCDashboardController.getCurrentUserDetail();
            }
            catch(Exception ex) {
            }
        }
        Test.stopTest(); 
    }
}