import {
    LightningElement,
    track
} from 'lwc';
import {
    NavigationMixin
} from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import register from '@salesforce/apex/OK_SelfRegistration.selfRegisterContact';
import contactType from '@salesforce/label/c.OKSF_Registration_Covid_Citizen';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {
    validateEmail
} from 'c/vtTsUtility';

export default class OkpcRegistrationpage extends NavigationMixin(LightningElement) {

    label = {
        contactType
    }
    @track emailConfirm = '';

    @track showError = false;
    @track showSuccess = false;
    @track errorMessage = '';
    @track thankYouMessage = '';
    @track firstName = '';
    @track lastName = '';
    @track email = '';
    @track phone = '';
    @track accountId;
    @track portalName;
    @track showThankYou = false;
    @track accounts = [];
    @track portal = [{
        label: 'Citizen (COVID)',
        value: "COVID"
    }, {
        label: 'Citizen (Antibody)',
        value: 'Antibody'
    }];
    @track showSpinner = false;

    @track dob;
    customImage = nysdohResource;
    captchaVerified;

    get randomName() {
        return this.makeid(10);
    }

    connectedCallback() {
        //Disbaling the copy paste
        let _this = this;

        document.addEventListener("grecaptchaEnterpriseExpired", function (e) {
            _this.captchaVerified = false;
        })

        document.addEventListener("grecaptchaEnterpriseError", function (e) {
            _this.captchaVerified = false;
        })

        document.addEventListener("grecaptchaEnterpriseVerified", function(e) {
        });

        setTimeout(() => {

            let emailBox = _this.template.querySelector(".pastenoevent1");

            if( emailBox ){
                emailBox.addEventListener('paste', 
                e => {
                    e.preventDefault();
                    this.showError = true;
                    this.errorMessage = 'Copy paste is disabled.';
                }
                );
            }

            let confirmEmailBox = _this.template.querySelector(".pastenoevent2");

            if( confirmEmailBox ){
                confirmEmailBox.addEventListener('paste', 
                e => {
                    e.preventDefault();
                    this.showError = true;
                    this.errorMessage = 'Copy paste is disabled.';
                }
                );
            }
        }, 1000);
    }

    renderedCallback() {
            }

    getRecaptch() {
        const v2cmp = this.template.querySelector('c-recaptcha');
        let token;
        if (v2cmp) {
          token = v2cmp.fetchToken();
        }
        return token;
    }
    
    makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    firstNameChange(event) {
        this.firstName = event.target.value;
    }
    lastNameChange(event) {
        this.lastName = event.target.value;
    }
    phoneChange(event) {
        this.phone = event.target.value;
        this.formatPhone(event.target);
    }
    emailChange(event) {
        this.email = event.target.value;
    }

    confirmEmailChange(event) {
        this.emailConfirm = event.target.value;
    }

    isFutureDate (dateValue) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        // if date is less then 10, then append 0 before date   
        if (dd < 10) {
            dd = '0' + dd;
        }
        // if month is less then 10, then append 0 before date    
        if (mm < 10) {
            mm = '0' + mm;
        }

        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;

        if(new Date() < dateValue) {
            return true;
        } else {
            return false;
        }
    }

    updateDOBField(event){
        this.dob = event.detail;
    }

   


    accountChange(event) {
        this.accountId = event.target.value;
    }

    portalChange(event) {
        this.portalName = event.target.value;
    }
    formatPhone(obj) {
        var numbers = obj.value.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj.value = "";
        for (var i = 0; i < numbers.length; i++) {
            obj.value += (char[i] || "") + numbers[i];
        }
    }

    get loginUrl(){
        let myurl = window.location.href;
        let newURL = new URL(myurl).searchParams;
        let eventId = newURL.get('eventId');
        if( eventId ){
            return '/events/s/login/?eventId='+eventId;
        }
        return '/events/s/login/';
    }

    doRegisteration() {
        let token = this.getRecaptch();
        if(token) {
            let myurl = window.location.href;
            let newURL = new URL(myurl).searchParams;
            let eventId = newURL.get('eventId');
            this.firstName
            this.showError = false;
            this.showSuccess = false;
            this.showThankYou = false;
            this.firstName = this.firstName.trim();
            this.lastName = this.lastName.trim();

            if (this.firstName === '' || typeof this.firstName === 'undefined') {
                this.errorMessage = 'Please provide First Name.';
                this.showError = true;
                return;
            }
            if (this.lastName === '' || typeof this.lastName === 'undefined') {
                this.errorMessage = 'Please provide Last Name';
                this.showError = true;
                return;
            }
            if (this.phone === '' || typeof this.phone === 'undefined') {
                this.errorMessage = 'Please provide Mobile Number';
                this.showError = true;
                return;
            }
            if (!validateEmail(this.email)) {
                this.errorMessage = 'Please provide a valid email address. Example: "abc@xyz.com"';
                this.showError = true;
                return;
            }
            if (this.dob === '' || typeof this.dob === 'undefined') {
                this.errorMessage = 'Please provide Date of Birth';
                this.showError = true;
                return;
            } 
            if(this.isFutureDate(new Date(this.dob))) {
                this.errorMessage = 'Date of Birth can not be in future.';
                this.showError = true;
                return;
            }

            if(this.isFutureDate(new Date(this.dob))) {
                this.errorMessage = 'Date of Birth can not be in future.';
                this.showError = true;
                return;
            } else {
                this.showError = false;
            }

            if(this.email !== this.emailConfirm){
                this.errorMessage = 'The Email Addresses you provided do not match';
                this.showError = true;
                return;
            }

            if(this.isValid()){
                let dateArray = this.dob.split('-');
                this.showSpinner = true;
                register({
                    firstName: this.firstName,
                    lastName: this.lastName,
                    phone: this.phone,
                    email: this.email,
                    type: this.label.contactType,
                    eventId: eventId, //Added delimiter by Sajal
                    dob: this.dob, //dateArray[2]+'-'+dateArray[0]+'-'+dateArray[1],
                    captchaResponse: token
                })
                .then(data => {
                    this.showSpinner = false;
                    if (data.type === 'error') {
                        document.dispatchEvent( new CustomEvent( 'grecaptchaEnterpriseReset') );
                        this.showError = true;
                        this.errorMessage = data.message;
                    } else if (data.type === 'success') {
                        this.showThankYou = true;
                        this.thankYouMessage = data.message;
                    }

                })
                .catch(error => {
                    document.dispatchEvent( new CustomEvent( 'grecaptchaEnterpriseReset') );
                    this.showSpinner = false;
                    this.showError = true;
                    var msg = JSON.stringify(error.body.message);
                    this.errorMessage = error.body.message;
                });
            }
        } else {
            this.dispatchEvent(new ShowToastEvent({
                message: 'Please Confirm the reCaptcha!',
                variant: 'error'
            }));
        }
    }

    isValid() {
        let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        return isAllValid;
    }

    formatDOB(dob) {

        var dobDate = new Date(dob);

        var dd = dobDate.getDate();
        var mm = dobDate.getMonth() + 1;
        var yyyy = dobDate.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }
        
        dobDate = dd + '/' + mm + '/' + yyyy;
        return dobDate;
    }

}