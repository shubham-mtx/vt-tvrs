/*
*   Class Name: VT_TS_CP_ValidateRequiredContactField
*   Description: Cllas To Validate Required Fields on Contact While Scheduling Appointment
*
*   Date            New/Modified         User                 Identifier                Description
*   01/10/2020         New         Shubham Dadhich(mtx)
*/
public with sharing class VT_TS_CP_ValidateRequiredContactField {

    /*
    * Method Name: validateFields
    * Description: To Validate Fields from the contact
    * @param: String contactId
    * @return: Map<String,Object>
    */
    
    @AuraEnabled
    public static Map<String,Object> validateFields(String contactId, String eventId, Integer contactAge, Boolean isVaccination){
        VTTS_AppointmentHelper.checkUserAccess(contactId, null);
        Map<String,Object> result = new Map<String,Object>();
        if(isVaccination){
            VTTS_AppointmentHelper appHlpr = new VTTS_AppointmentHelper();
            //Check 1
            VTTS_AppointmentHelper.VT_TS_Wrapper validationWrapper = new VTTS_AppointmentHelper.VT_TS_Wrapper();
            validationWrapper.contactId = contactId;
            validationWrapper.currentVaccine = VT_TS_Constants.FIRST_DOSE_TEXT;
            for(OKPCDashboardController.SelectOptionWrapper wrapper : OKPCDashboardController.fetchPicklist('Private_Access__c','Vaccine_Type__c')){
                validationWrapper.vaccineTypes = String.isBlank(validationWrapper.vaccineTypes) ? wrapper.value : validationWrapper.vaccineTypes + ',' +wrapper.value;
            }
            appHlpr.validateAppointmentsBooster(JSON.serialize(validationWrapper));
            // appHlpr.validateAppointments(contactId , 'Vaccination-1,Vaccination-2');
            if( appHlpr.isInvalid() ){
                result.putAll(appHlpr.getResult());
                return result;
            }

            //Check 2
            appHlpr.identifyPrivateAcccessUsingContactId(contactId, 'Vaccination-1', '');
            result.putAll(appHlpr.getResult());
        }

        String validateString = [SELECT Field_API_Name__c FROM VT_TS_Contact_Required_Field__mdt LIMIT 1].Field_API_Name__c;
        
        String query = 'SELECT Id,'+validateString+' FROM Contact WHERE Id=:contactId WITH SECURITY_ENFORCED';
        System.debug('>>> CONTACT ID :'+contactId);
        System.debug('>>> VALIDATE STRING :'+validateString);
        List<String> validateStringList = new List<String>();
        Boolean isValidated = true;

        validateStringList = validateString.split(',');
        Contact con = Database.query(query);
        System.debug('>>>> DATA   :'+Database.query(query));
        result.put('missingFields','');

        if( !con.Consent__c ){
            result.put('consentIssue', true);
            return result;
        }
        // added by vamsi 
        if(!(con.Gender__c!=null && con.Race__c!=null && con.Ethnicity__c!=null && con.Primary_Language__c!=null )){
            // Displaying Error message to Update the Demographic Details. 
            result.put('missingDemographicDetails',true);
            //return result;
        }

        for(String fieldValue : validateStringList){
            if(con.get(fieldValue) == null || con.get(fieldValue) == ''){
                isValidated = false;
                String labelField  = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().get(fieldValue).getDescribe().getLabel();
                result.put('missingFields',result.get('missingFields')+labelField+', ');
            }
        }

        if(!isValidated){
            String fields = String.valueOf(result.get('missingFields'));
            result.put('missingFields',fields.removeEnd(', '));
        }
        result.put('isValidate',isValidated);

        return result;
    }
}