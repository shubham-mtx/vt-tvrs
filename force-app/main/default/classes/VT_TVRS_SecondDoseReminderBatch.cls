/**
 * Author Rahul Ahuja
 * Email rahul.ahuja@mtxb2b.com
 * Description This is schedulable and batch class to update Reminders__c if 2nd dose is required.
 */

global class VT_TVRS_SecondDoseReminderBatch implements Database.Batchable<sObject>, Schedulable,Database.Stateful{
    
    /**
     * Author Rahul Ahuja
     * Description: Default start :  Query On Appointment Object
    */
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query =  'SELECT Id, Email_Workflow_Datetime__c, Email_Flow__c,Patient__c,Appointment_Complete_Date__c FROM Appointment__c WHERE Status__c = \'Completed\' AND Event_Process__c = \'Vaccination\''+
                        ' AND Event__r.Private_Access__r.Vaccine_Type__c = \'Vaccination-1\' AND Event__r.Private_Access__r.Requires_Followup_Dose__c = \'Yes\''+
                        ' AND Second_Dose_Reminder_Completed__c = false AND Patient__r.Stop_Second_Dose_Notification__c = false';
        return Database.getQueryLocator(query);
    }

   /**
     * Author Rahul Ahuja
     * Description: Default execute : To Send Email to the patient and Update their field 
    */
    global void execute(Database.BatchableContext BC, List<Appointment__c> appointmentList){
        
        //Get Days Configrations for the Reminders
        VT_TS_AppointmentReminderWrapper appointmentReminder = (VT_TS_AppointmentReminderWrapper) JSON.deserialize(System.Label.TVRS_APPOINTMENT_REMINDERS , VT_TS_AppointmentReminderWrapper.class);
        
        // Varibale Declaration
        Map<String, Appointment__c> mapOfContactAppointments = new Map<String, Appointment__c>();
        List <Appointment__c> appList = new List<Appointment__c>();
        Set<Id> patientIdSet = new Set<Id>();
        Date appCompleteDate;
        Integer gap;
        
        //Step 1: Store Contact Ids in a Set
        for(Appointment__c appointmentRecord: appointmentList){
            patientIdSet.add(appointmentRecord.Patient__c);
        }
        
        //Step 1.1: Query on their existing Scheduled Second Dose Appointment
        for(Appointment__c appointmentRecord : [SELECT Id, Status__c, Patient__c 
                                                FROM Appointment__c 
                                                WHERE Patient__c IN: patientIdSet 
                                                AND (Status__c = 'Completed' OR Status__c = 'Scheduled') 
                                                AND Event__r.Private_Access__r.Vaccine_Type__c = 'Vaccination-2' 
                                                AND Event_Process__c = 'Vaccination']){
            mapOfContactAppointments.put(appointmentRecord.Patient__c, appointmentRecord);
        }
        
        //Step 2: Loop on appointmentList and if condition met update Reminders__c
        for(Appointment__c app : appointmentList){
            
            //Step 2.1: Check for is 2nd Appointment is Scheduled or Completed
            if(!mapOfContactAppointments.containsKey(app.Patient__c)){
                
                //Calculate Number of Days Remaing to Send Reminder
                appCompleteDate = (app.Appointment_Complete_Date__c).date();
                
                Date runningBatchDate = Date.today().addDays(appointmentReminder.day_to_add_for_testing ); 
                 
                gap = appCompleteDate.daysBetween(runningBatchDate);  
                
                if(Integer.valueOf(gap) == Integer.valueOf(appointmentReminder.firstDoseReminderDay) || Integer.valueOf(gap) == Integer.valueOf(appointmentReminder.secondDoseReminderDay)
                 || Integer.valueOf(gap) == Integer.valueOf(appointmentReminder.thirdDoseReminderDay) || Integer.valueOf(gap) == Integer.valueOf(appointmentReminder.fourthDoseReminderDay)){

                    app.Email_Workflow_Datetime__c = Datetime.now();

                    if(Integer.valueOf(gap) == Integer.valueOf(appointmentReminder.fourthDoseReminderDay)){
                        app.Second_Dose_Reminder_Completed__c = true;
                    }
                    
                    app.Email_Flow__c = '2nd Dose Missed Reminder';
                    appList.add(app);
                } 
                
            }else{
                // Turn off the notification if days passes
                app.Second_Dose_Reminder_Completed__c = true;
                appList.add(app);
            }   
        }

        // Update appList if list is not empty
        if(!appList.isEmpty()){
            VT_TS_SkipTriggerExecution.executeAppointmentTrigger = false;
            update appList;
        }
    }

    /**
     * Author Rahul Ahuja
     * Description: Default finish
    */
    global void finish(Database.BatchableContext BC){}

    /**
     * Author Rahul Ahuja
     * Description: Default execute: For Schedule Job
    */
    global void execute(SchedulableContext ctx) {
        VT_TVRS_SecondDoseReminderBatch batch = new VT_TVRS_SecondDoseReminderBatch();
        Database.executeBatch(batch, 200);

        /** Cron Expression: To schedule job for every day
        * VT_TVRS_SecondDoseReminderBatch sc = new VT_TVRS_SecondDoseReminderBatch();
        * String cronExp = '0 0 2 1/1 * ? *';
        * String jobID = System.schedule('Daily Schedule 2nd Dose Reminder VT_TVRS_SecondDoseReminderBatch', cronExp, sc);
        **/
    }
}