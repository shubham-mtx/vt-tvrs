/**
 * @description       : 
 * @author            : Vamsi Mudaliar
 * @group             : 
 * @last modified on  : 01-15-2022
 * @last modified by  : Mohit Karani
**/
public without sharing class VT_PreRegAssignmentEngine {

    //Constant for the Class
    public static String CHRONIC_YES = 'Yes';
    public static String PRE_REG_STATUS_PENDING_VERIFICATION = 'Pending Verification';
    public static String PRE_REG_STATUS_ASSIGNED = 'Assigned';

    /* Method Name: processAssignment
    ** Description: Runtime Assignment of Pre Reg Group
    ** Author: Shubham Dadhich (shubham.dadhich@mtxb2b.com)
    ** Parameter Arguments: Map<Contact, Pre_Registration__c>
    ** Return: Map<Contact, Pre_Registration__c> */
    public static Map<Contact, Pre_Registration__c> processAssignment(Map<Contact, Pre_Registration__c> preReg) {
        //Variables
        List<Pre_Registration_Group__c> listOfPreRegGroup = new List<Pre_Registration_Group__c>();
        Map<String, Pre_Registration_Group__c> passCodeAndGroup = new Map<String, Pre_Registration_Group__c>();

        //Step 1: Get all passcodes
        Set<String> passcodes = new Set<String>();
        for( Pre_Registration__c pReg : preReg.values() ){
            if( String.isNotBlank(pReg.Pass_Code_Health__c) ){
                passcodes.add(pReg.Pass_Code_Health__c);
            }
            if( String.isNotBlank(pReg.Pass_Code_Risk_Group__c) ){
                passcodes.add(pReg.Pass_Code_Risk_Group__c);
            }
        }
        
        //Step 2: Get all Pre-Reg Group and Passcode Groups
        for(Pre_Registration_Group__c grp : [SELECT Id, Name, Estimated_Date__c, Group_Description__c, Beginning_DOB__c, 
                                            Ending_DOB__c, Status__c, Verification_Delay__c ,
                                            Pre_Registration_Group__c,RecordType.DeveloperName,Passcode__c,Group_Type__c,
                                            Pre_Registration_Group__r.Id,Pre_Registration_Group__r.Status__c,
                                            Pre_Registration_Group__r.Group_Type__c,Pre_Registration_Group__r.Beginning_DOB__c,
                                            Pre_Registration_Group__r.Ending_DOB__c
                                            FROM Pre_Registration_Group__c 
                                            WHERE ( (Status__c = 'Activate' OR Status__c = 'Open') AND RecordType.DeveloperName = 'Pre_Registration_Group' ) 
                                            OR ( Passcode__c in : passcodes AND Status__c = 'Activate' AND Pre_Registration_Group__r.Status__c = 'Activate' ) WITH SECURITY_ENFORCED ]){
            if( grp.RecordType.DeveloperName == 'Pre_Registration_Group' ){
                listOfPreRegGroup.add(grp);
            }
            else{
                passCodeAndGroup.put(grp.Passcode__c, grp.Pre_Registration_Group__r);
            }
        }


        //Step 3: Loop Over preReg and mapOfPreRegGroup to assign eligible group to preReg Record.
        for(Contact con : preReg.keySet()){            
            Pre_Registration__c preRegRecord = preReg.get(con);    
            Set<String> eligibleStatus = new Set<String>();
            boolean isAnyGroupActivateApartFromChronic = false;

            //A. Risk based early access group based on passcode
            if( String.isNotBlank(preRegRecord.Pass_Code_Risk_Group__c) && 
                passCodeAndGroup.containsKey(preRegRecord.Pass_Code_Risk_Group__c) ){
                Pre_Registration_Group__c pGrp = passCodeAndGroup.get(preRegRecord.Pass_Code_Risk_Group__c);
                preRegRecord.Early_Group_2_Access_Group__c = null;
                //Check age and group type should be Risk Type
                if( pGrp.Group_Type__c == 'Risk Group' && 
                    con.Birthdate >= pGrp.Beginning_DOB__c && con.Birthdate <= pGrp.Ending_DOB__c ){

                    preRegRecord.Early_Group_2_Access_Group__c = pGrp.Id;
                    preRegRecord.Early_Group_2_Access_Group__r = pGrp;
                    eligibleStatus.add(PRE_REG_STATUS_ASSIGNED);

                    if( pGrp.Status__c == 'Activate'){
                        isAnyGroupActivateApartFromChronic = true;
                        preRegRecord.Is_Email_Sent__c = true;
                    }
                }
            }

            //B. Chronic based on passcode access group
            if( preRegRecord.Chronic_Condition__c == CHRONIC_YES && 
                String.isNotBlank(preRegRecord.Pass_Code_Health__c) && 
                passCodeAndGroup.containsKey(preRegRecord.Pass_Code_Health__c) ){
                Pre_Registration_Group__c pGrp = passCodeAndGroup.get(preRegRecord.Pass_Code_Health__c);
                //Check age and group type should be Chronic
                if( pGrp.Group_Type__c == 'Chronic' && con.Birthdate >= pGrp.Beginning_DOB__c && con.Birthdate <= pGrp.Ending_DOB__c ){
                    preRegRecord.Early_Group1_Health__c = pGrp.Id;
                    preRegRecord.Early_Group1_Health__r = pGrp;
                    eligibleStatus.add(PRE_REG_STATUS_ASSIGNED);

                    if( pGrp.Status__c == 'Activate'){
                        isAnyGroupActivateApartFromChronic = true;
                        preRegRecord.Is_Email_Sent__c = true;
                    }
                }
            }

            for(Pre_Registration_Group__c preRegGroup : listOfPreRegGroup){
                //C. Age based assignment
                if( preRegGroup.Group_Type__c == 'Age-Based' && 
                    con.Birthdate >= preRegGroup.Beginning_DOB__c && con.Birthdate <= preRegGroup.Ending_DOB__c ){
                    
                    preRegRecord.Pre_Registration_Group__c = preRegGroup.Id;
                    preRegRecord.Pre_Registration_Group__r = preRegGroup;
                    eligibleStatus.add(PRE_REG_STATUS_ASSIGNED);

                    if( preRegGroup.Status__c == 'Activate' ){
                        isAnyGroupActivateApartFromChronic = true;
                        preRegRecord.Is_Email_Sent__c = true;
                    }
                }

                //D. Chronic based access group without passcode
                // Remove following conditions w.r.t 58198
                // String.isBlank(preRegRecord.Early_Group1_Health__c) &&
                // String.isBlank(preRegRecord.Pass_Code_Health__c) && 

                else if( preRegGroup.Group_Type__c == 'Chronic' &&
                         preRegRecord.Chronic_Condition__c == CHRONIC_YES && 
                         con.Birthdate >= preRegGroup.Beginning_DOB__c && con.Birthdate <= preRegGroup.Ending_DOB__c){

                    preRegRecord.Early_Group1_Health__c = preRegGroup.Id;
                    preRegRecord.Early_Group1_Health__r = preRegGroup;
                    eligibleStatus.add( ( preRegGroup.Verification_Delay__c > 0 ? PRE_REG_STATUS_PENDING_VERIFICATION : PRE_REG_STATUS_ASSIGNED ) );
                            
                    if( preRegGroup.Status__c == 'Activate' ){
                        preRegRecord.Is_Email_Sent__c = true;
                    }
                }
            }

            //Priority of status will go to assigned
            if( isAnyGroupActivateApartFromChronic ){
                preRegRecord.Status__c = PRE_REG_STATUS_ASSIGNED;
            }
            else if( eligibleStatus.contains(PRE_REG_STATUS_PENDING_VERIFICATION) && !isAnyGroupActivateApartFromChronic ){
                preRegRecord.Status__c = PRE_REG_STATUS_PENDING_VERIFICATION;
            }
            else if( eligibleStatus.contains(PRE_REG_STATUS_ASSIGNED) ){
                preRegRecord.Status__c = PRE_REG_STATUS_ASSIGNED;
            }
        }
        //STEP 3: Return Updated PreReg Record
        return preReg;
    }

    public static List<Pre_Registration_Group__c> identifyGroupBasedOnDOB(Date dob) {
        return [SELECT Id, Name, Group_Description__c, Estimated_Date__c 
                FROM Pre_Registration_Group__c 
                WHERE (Status__c = 'Activate' OR Status__c = 'Open') AND RecordType.DeveloperName = 'Pre_Registration_Group' AND
                Beginning_DOB__c <=: dob AND Ending_DOB__c >=: dob AND Group_Type__c = 'Age-Based' WITH SECURITY_ENFORCED];
    }

    /**
    * @description       : This method verifies the passcode.
    * @author            : Vishnu Kumat
    **/
    public static Map<String, String> passCodeValidation(Pre_Registration__c preReg, Pre_Registration__c oldPreReg) {
        Map<String, String> passcodes = new Map<String, String>();
        passcodes.put('Pass_Code_Health__c', 'NONE');
        passcodes.put('Pass_Code_Risk_Group__c', 'NONE');

        //Step 1: Get all passcodes
        Set<String> passcodeSet = new Set<String>();

        if( preReg.id == null ){
            if( String.isNotBlank(preReg.Pass_Code_Health__c) ){
                passcodeSet.add(preReg.Pass_Code_Health__c);
                passcodes.put('Pass_Code_Health__c', 'NOT_VALID');
            }
            if( String.isNotBlank(preReg.Pass_Code_Risk_Group__c) ){
                passcodeSet.add(preReg.Pass_Code_Risk_Group__c);
                passcodes.put('Pass_Code_Risk_Group__c', 'NOT_VALID');
            }

            if(String.isNotBlank(preReg.Actual_Pass_Code__c) && String.isBlank(preReg.Pass_Code_Risk_Group__c)) {
                passcodeSet.add(preReg.Actual_Pass_Code__c);
                String actualPassCode = String.valueOf(preReg.Actual_Pass_Code__c);
                preReg.Pass_Code_Risk_Group__c = actualPassCode;
            }
        }
        else{

            if(String.isNotBlank(preReg.Actual_Pass_Code__c)) {
                String actualPassCode = String.valueOf(preReg.Actual_Pass_Code__c);
                preReg.Pass_Code_Risk_Group__c = actualPassCode;
                if(String.isBlank(oldPreReg.Actual_Pass_Code__c)) {
                    preReg.Actual_Pass_Code__c = oldPreReg.Pass_Code_Risk_Group__c;
                }
            }

            if( String.isNotBlank(preReg.Pass_Code_Health__c) && 
                preReg.Chronic_Condition__c == 'Yes' &&
                oldPreReg.Chronic_Condition__c != 'Yes'
            ){
                passcodeSet.add(preReg.Pass_Code_Health__c);
                passcodes.put('Pass_Code_Health__c', 'NOT_VALID');
            }

            if( String.isNotBlank(preReg.Pass_Code_Risk_Group__c) && preReg.Affiliated_to_any_other_Risk_Group__c == 'Yes' && (oldPreReg.Affiliated_to_any_other_Risk_Group__c != 'Yes' || String.isNotBlank(preReg.Actual_Pass_Code__c))) {
                passcodeSet.add(preReg.Pass_Code_Risk_Group__c);
                passcodes.put('Pass_Code_Risk_Group__c', 'NOT_VALID');
            }
        }

        //Step 2: Get all Passcode Groups and verify eligibility
        for(Pre_Registration_Group__c grp : [SELECT Id,Passcode__c,Passcode_Type__c
                                            FROM Pre_Registration_Group__c 
                                            WHERE Passcode__c in : passcodeSet AND Status__c = 'Activate' 
                                            AND RecordType.DeveloperName = 'Pre_Registration_Passcode' WITH SECURITY_ENFORCED]){
            if( preReg.Pass_Code_Health__c == grp.Passcode__c && grp.Passcode_Type__c == 'Health' ){
                passcodes.put('Pass_Code_Health__c', 'VALID');
            }
            else if( preReg.Pass_Code_Risk_Group__c == grp.Passcode__c && grp.Passcode_Type__c == 'Risk Group' ){
                passcodes.put('Pass_Code_Risk_Group__c', 'VALID');
            }
        }

        return passcodes;
    }
}