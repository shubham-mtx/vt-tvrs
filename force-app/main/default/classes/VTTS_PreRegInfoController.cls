/**
 * @lastModifiedBy balram.dhawan@mtxb2b.com
 * @lastModifiedDate 2021-02-15
 * @desc Controller for Pre-Reg Story 14565
*/
public without sharing class VTTS_PreRegInfoController {
    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc return pre-reg record
    */
    @AuraEnabled
    public static Contact getPreRegRecord(Id contactId) {
        try {
            VTTS_AppointmentHelper.checkUserAccess(contactId, null);
            List<Contact> contacts = [SELECT Id, Birthdate, 
                                    (SELECT Id, Name, Chronic_Condition__c, Pre_Registration_Group__c, BIPOC__c, Is_Immune_Weak__c, 
                                    Pre_Registration_Group__r.Name, Pre_Registration_Group__r.Estimated_Date__c, Pre_Registration_Group__r.Group_Description__c, 
                                    Early_Group1_Health__r.Estimated_Date__c, Do_you_have_a_Health_Risk_Passcode__c,/* Do_not_have_a_Provider__c, */
                                    Early_Group1_Health__c, Early_Group1_Health__r.Name, Early_Group1_Health__r.Group_Description__c, 
                                    Early_Group_2_Access_Group__c, Early_Group_2_Access_Group__r.Name, Early_Group_2_Access_Group__r.Estimated_Date__c,
                                    Early_Group_2_Access_Group__r.Group_Description__c, Status__c, Pass_Code_Health__c, Pass_Code_Risk_Group__c, 
                                    /*Are_you_eligible_for_Vaccine_Risk_Group__c, Provider_Name__c, 
                                    Provider_Phone__c,*/ Organization_Name__c, Organization_Phone__c, Affiliated_to_any_other_Risk_Group__c 
                                    FROM Pre_Registrations__r )
                                    FROM Contact WHERE Id =:contactId WITH SECURITY_ENFORCED];
            if(!contacts.isEmpty()) {
                Contact c = contacts[0];
                if(!c.Pre_Registrations__r.isEmpty()) {
                    if(String.isNotBlank(c.Pre_Registrations__r[0].Pass_Code_Risk_Group__c)) {
                        c.Pre_Registrations__r[0].Pass_Code_Risk_Group__c = '********';
                    }
                    if(String.isNotBlank(c.Pre_Registrations__r[0].Pass_Code_Health__c)) {
                        c.Pre_Registrations__r[0].Pass_Code_Health__c = '********';
                    }
                }
                return c;
            } 
            return null;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Map<String, Object> getAgeBasedGroup(Date dob, String contactId){
        try {
            VTTS_AppointmentHelper.checkUserAccess(contactId, null);
            if(String.isNotBlank(contactId)) {
                List<Appointment__c> completedAppointments = [SELECT Id FROM Appointment__c 
                                                                WHERE Patient__c =:contactId AND Status__c = 'Completed'
                                                                AND Event__r.Private_Access__r.Vaccine_Type__c = 'Vaccination-1' WITH SECURITY_ENFORCED];
                if(!completedAppointments.isEmpty()) {
                    return new Map<String, Object>{'group' => false, 'hasCompletedAppointment' => true};
                }
            }
            List<Pre_Registration_Group__c> groups = VT_PreRegAssignmentEngine.identifyGroupBasedOnDOB(dob);
            if(!groups.isEmpty()) {
                return new Map<String, Object>{'group' => groups[0]};
            } 
            return new Map<String, Object>{'group' => false};
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}