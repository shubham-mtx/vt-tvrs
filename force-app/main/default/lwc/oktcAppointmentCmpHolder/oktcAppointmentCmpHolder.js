import {
    LightningElement,
    track,
    api,
} from 'lwc';
import getTestingSite from '@salesforce/apex/OKPCHeaderController.getUserDetailsAndTestingSite';
import getAccountType from '@salesforce/apex/OKPCHeaderController.getAccountDetail';
import sendEmail from '@salesforce/apex/OKPCHeaderController.sendEmail';
import canCheckedIn from '@salesforce/apex/OKPCHeaderController.canCheckedIn';
import formFactorPropertyName from '@salesforce/client/formFactor';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import {
    showMessage,
    showMessageWithLink,
    msgObj,
    createMessage,
    showAsyncErrorMessage
} from 'c/vtTsUtility';
import EMAIL_CONFIRMATION_LABEL from '@salesforce/label/c.Testing_Site_Appointment_Confirmation_Email_Success_Message';
import runMicro220Batch from '@salesforce/apex/VTTS_TP_Micro220Controller.runMicro220Batch';
import DOWNLOAD_MICRO220_SUCCESS_MSG from "@salesforce/label/c.VTTS_DownloadMicro220SuccessMessage";
import fetchEvents from "@salesforce/apex/VTTS_TP_Micro220Controller.fetchEvents";
import checkVaccinationReschedule from "@salesforce/apex/DC_ScheduleAppointment.checkVaccinationReschedule";
import checkExistingAppointmentFor2Dose from "@salesforce/apex/DC_ScheduleAppointment.checkExistingAppointmentForNextDose"; // replace of checkExistingAppointmentFor2Dose as part of Booster Dose


const micro220Columns = ['Patient FirstName', 'Patient LastName', 'Patient MiddleName',
    'Date of Birth', 'Gender', 'Race', 'Ethnicity', 'Mailing Street', 'Mailing City', 'State', 'County',
    'ZIP', 'MRN', 'Mobile', 'EPIC MRN', 'Type of Insurance', 'Insured First Name', 'Patients Relationship to insured',
    'Insurance ID Number', 'Policy Group or FECA Number', 'Additional Insurance - Insured First Name',
    'Additional Insurance - Insured Last Name', 'Additional Insurance - Policy/Group Number',
    'Additional Insurance - Relationship to Insured', 'Test Ordered', 'Appointment Date', 'Source',
    'If Other Specimen specify', 'Appointment Number', 'EPIC Instrument ID', 'Reason for Testing', '* Submitting Institution',
    'Ordering Provider Last Name', 'Ordering Provider First Name', 'NPI', 'Ordering Provider Phone Number', 'Ordering Provider Fax Number',
    'Email', 'Event Description', 'First COVID-19 Test', 'Employed by Healthcare', 'Symptomatic as defined by CDC?',
    'Symptoms Onset Date', 'Is the patient hospitalized?', 'Is the patient currently in the ICU?', 'Resident in Concregate Care Setting?',
    'Are you Pregnant?'
];

export default class OktcAppointmentCmpHolder extends LightningElement {
    get isMobile() {
        return formFactorPropertyName != 'Large';
    }
    @api recordId;
    @track columnsVaccination = [{
            label: 'Patient Name',
            name: 'Patient__r.Name',
            type: 'text',
            action: ''
        },
        {
            label: 'Phone Number',
            name: 'Patient__r.MobilePhone',
            type: 'text',
            action: ''
        },
        {
            label: 'DOB',
            name: 'Patient__r.formatted_Date_of_Birth__c',
            type: 'text',
            action: '',
            class: 'width-100'
        },
        {
            label: 'Gender',
            name: 'Gender__c',
            type: 'text',
            action: '',
            class: 'width-100'
        },
        {
            label: 'Patient Id',
            name: 'Patient__r.Patient_Id__c',
            type: 'text',
            action: '',
            class: 'width-100'
        },
        {
            label: 'Event',
            name: 'Event__r.Description__c',
            type: 'text',
            action: '',
            class: 'width-150'
        },
        {
            label: 'Vaccine Class',
            name: 'Event__r.Private_Access__r.Vaccine_Class_Name__c',
            type: 'text',
            action: '',
            class: 'width-100'
        },
        {
            label: 'Date',
            name: 'Appointment_Start_Date_v1__c',
            type: 'text',
            action: '',
            class: 'custom-hide-comp width-100'
        },
        {
            label: 'Time',
            name: 'Formatted_Appointment_Start_Time__c',
            type: 'text',
            action: '',
            class: 'custom-hide-comp width-100'
        },
        {
            label: 'Action',
            name: '',
            type: 'buttons',
            action: 'Schedule',
            varient: 'brand',
            buttons: [{
                label: 'Re-Schedule',
                class: 'custom-hide-comp paginator-button'
            }, {
                label: 'Checked-In',
                class: 'paginator-button'
            }, {
                label: 'Cancel',
                class: 'custom-hide-comp paginator-button'
            }, {
                label: 'Email',
                class: 'custom-hide-comp paginator-button'
            }]
        }
    ];
    @track columns = [{
            label: 'Patient Name',
            name: 'Patient__r.Name',
            type: 'text',
            action: ''
        },
        {
            label: 'Phone Number',
            name: 'Patient__r.MobilePhone',
            type: 'text',
            action: ''
        },
        {
            label: 'DOB',
            name: 'Patient__r.formatted_Date_of_Birth__c',
            type: 'text',
            action: '',
            class: 'width-100'
        },
        {
            label: 'Gender',
            name: 'Gender__c',
            type: 'text',
            action: '',
            class: 'width-100'
        },
        {
            label: 'Patient Id',
            name: 'Patient__r.Patient_Id__c',
            type: 'text',
            action: '',
            class: 'width-100'
        },
        {
            label: 'Event',
            name: 'Event__r.Description__c',
            type: 'text',
            action: '',
            class: 'width-150'
        },
        {
            label: 'Date',
            name: 'Appointment_Start_Date_v1__c',
            type: 'text',
            action: '',
            class: 'custom-hide-comp width-100'
        },
        {
            label: 'Time',
            name: 'Formatted_Appointment_Start_Time__c',
            type: 'text',
            action: '',
            class: 'custom-hide-comp width-100'
        },
        {
            label: 'Action',
            name: '',
            type: 'buttons',
            action: 'Schedule',
            varient: 'brand',
            buttons: [{
                label: 'Re-Schedule',
                class: 'custom-hide-comp paginator-button'
            }, {
                label: 'Checked-In',
                class: 'paginator-button'
            }, {
                label: 'Cancel',
                class: 'custom-hide-comp paginator-button'
            }, {
                label: 'Email',
                class: 'custom-hide-comp paginator-button'
            }]
        }
    ];
    @track whereClause;
    @track showPaginator = false;

    @track columns2 = [{
            label: 'Patient Name',
            name: 'Patient__r.Name',
            type: 'text',
            action: ''
        },
        {
            label: 'DOB',
            name: 'Patient__r.formatted_Date_of_Birth__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Patient Id',
            name: 'Patient__r.Patient_Id__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Event',
            name: 'Event__r.Description__c',
            type: 'text',
            action: '',
            class: 'width-150'
        },
        {
            label: 'Preferred Date',
            name: 'Patient__r.Formatted_preferred_Date__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Preferred Time',
            name: 'Patient__r.Preferred_Time__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Action',
            name: '',
            type: 'button',
            action: 'Schedule',
            varient: 'brand',
            buttonlabel: 'Schedule'
        }
    ];
    @track whereClause2;
    @track showPaginator2 = false;

    @track columns3 = [{
            label: 'Patient Name',
            name: 'Patient__r.Name',
            type: 'text',
            action: ''
        },
        {
            label: 'DOB',
            name: 'Patient__r.formatted_Date_of_Birth__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Patient Id',
            name: 'Patient__r.Patient_Id__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Event',
            name: 'Event__r.Description__c',
            type: 'text',
            action: '',
            class: 'width-150'
        },
        {
            label: 'Canceled Date',
            name: 'Canceled_Date__c',
            type: 'text',
            action: ''
        },
        
        {
            label: 'Action',
            name: '',
            type: 'buttons',
            action: 'Schedule',
            varient: 'brand',
            buttons: [{
                label: 'Re-Schedule',
                class: 'custom-hide-comp paginator-button'
            }, {
                label: 'Checked-In',
                class: 'paginator-button'
            }]
        }
    ];

    @track columns4 = [{
            label: 'Patient Name',
            name: 'Patient__r.Name',
            type: 'text',
            action: ''
        },
        {
            label: 'DOB',
            name: 'Patient__r.formatted_Date_of_Birth__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Patient Id',
            name: 'Patient__r.Patient_Id__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Event',
            name: 'Event__r.Description__c',
            type: 'text',
            action: '',
            class: 'width-150'
        },
        {
            label: 'Completed Date',
            name: 'Formatted_Complete_Date__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Micro220',
            name: '',
            type: 'buttons',
            action: 'micro220',
            varient: 'brand',
            buttons: [{
                label: 'Download',
                class: 'paginator-button'
            }]
        }
    ];

    @track columns4_vaccination = [{
            label: 'Patient Name',
            name: 'Patient__r.Name',
            type: 'text',
            action: ''
        },
        {
            label: 'DOB',
            name: 'Patient__r.formatted_Date_of_Birth__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Patient Id',
            name: 'Patient__r.Patient_Id__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Event',
            name: 'Event__r.Description__c',
            type: 'text',
            action: '',
            class: 'width-150'
        },
        {
            label: 'Vaccine Class',
            name: 'Event__r.Private_Access__r.Vaccine_Class_Name__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Completed Date',
            name: 'Formatted_Complete_Date__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Action',
            name: '',
            // type: 'button',
            action: 'Schedule2ndDose',
            varient: 'brand',
            buttonlabel: 'Next Dose',
            // buttons: [{
            //     label: 'Schedule Next Dose',
            //     class: 'custom-hide-comp paginator-button'
            // }]
        }
    ];

    @track whereClause4;
    @track whereClause3;
    @track showPaginator3 = false;

    @track resetTable = false;
    @track reloadTable = false;
    @track showCalendar = false;
    @track showConfirmation = false;
    @track appointmentId;
    @track accountId;
    @track modalType = '';
    @track showBatchModal = false;
    @track eventOptions = [];
    @track eventProcess;
    @track isVaccination = false;
    @track eventIds;
    @track isRescheudle;

    showSpinner = false;
    selectedAppointmentIds = [];
    showEventsList = false;
    micro220SuccessMsg = DOWNLOAD_MICRO220_SUCCESS_MSG;
    isSecondDose = false;
    oldVaccineClass;
    msgObjData = msgObj();
    customErrorMessage;
    get type() {
        return this.modalType;
    }

    connectedCallback() {
        this.fetchEvents();
        if (this.recordId) {
            getAccountType({
                    recordId: this.recordId
                })
                .then(data => {
                    this.eventProcess = data.Event_Process__c;
                    if (this.eventProcess === 'Vaccination') {
                        this.isVaccination = true;
                        this.columns = this.columnsVaccination;
                    } else {
                        this.isVaccination = false;
                    }
                })
                .catch(error => {
                    showAsyncErrorMessage(this, error);
                });
            this.accountId = this.recordId;
            this.whereClause = ' Status__c = \'Scheduled\' AND Lab_Center__c = \'' + this.recordId + '\' AND Lab_Center__c != null';
            this.whereClauseFinal = ' Status__c = \'Scheduled\' AND Lab_Center__c = \'' + this.accountId + '\' AND Lab_Center__c != null';
            this.whereClause2 = ' Status__c = \'To Be Scheduled\' AND Lab_Center__c = \'' + this.recordId + '\' AND Lab_Center__c != null';
            this.whereClause3 = ' Status__c = \'Cancelled\' AND Lab_Center__c = \'' + this.recordId + '\' AND Lab_Center__c != null';
            this.whereClause4 = ' Status__c = \'Completed\' AND Lab_Center__c = \'' + this.recordId + '\' AND Lab_Center__c != null';
            this.showPaginator = true;
        } else {
            getTestingSite({})
                .then(data => {
                    this.eventProcess = data.testingSiteType;
                    if (this.eventProcess === 'Vaccination') {
                        this.isVaccination = true;
                        this.columns = this.columnsVaccination;
                    } else {
                        this.isVaccination = false;
                    }
                    if (data.userDetail.ContactId)
                        this.accountId = data.testingSiteId;
                    if (data.userDetail.ContactId && data.testingSiteId) {
                        this.whereClause = ' Status__c = \'Scheduled\' AND Lab_Center__c = \'' + this.accountId + '\' AND Lab_Center__c != null';
                        this.whereClause2 = ' Status__c = \'To Be Scheduled\' AND Lab_Center__c = \'' + this.accountId + '\' AND Lab_Center__c != null';
                        this.whereClause3 = ' Status__c = \'Cancelled\' AND Lab_Center__c = \'' + this.accountId + '\' AND Lab_Center__c != null';
                        this.whereClause4 = ' Status__c = \'Completed\' AND Lab_Center__c = \'' + this.accountId + '\' AND Lab_Center__c != null';
                    } else {
                        this.whereClause = ' Id = null ';
                        this.whereClause2 = ' Id = null ';
                        this.whereClause3 = ' Id = null ';
                        this.whereClause4 = ' Id = null ';
                    }
                    this.showPaginator = true;
                })
                .catch(error => {
                    showAsyncErrorMessage(this, error);
                });
        }
    }

    renderedCallback() {
        this.reloadTable = false;
    }

    handlePaginatorAction(event) {
        this.showError = false;
        let data = event.detail;
        this.isRescheudle = false;

        if (data.actionName === 'Re-Schedule' || data.actionName === 'Schedule') {
            this.appointmentId = data.recordId;
            this.showCalendar = true;
            this.isSecondDose = false;
            this.isRescheudle = true;

            if (this.isVaccination) {
                this.showSpinner = true;
                this.showCalendar = false;

                checkVaccinationReschedule({
                        appointmentId: this.appointmentId
                    })
                    .then(result => {
                        if (result.furtherDosesNotReqd) {
                            showMessageWithLink(this, 'error', this.msgObjData['furtherDosesNotReqd'], this.msgObjData['vermontMsgData']);
                        } else if (result.isSelectedDoseAlreadyScheduled && (data.actionName === 'Schedule' || (data.actionName === 'Re-Schedule' && data.selectedTabName === 'Cancelled Appointments'))) {
                            createMessage(result.doseNumber, this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyScheduled']);
                        } else if (result.isSelectedDoseAlreadyCompleted) {
                            showMessage(this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyCompleted']);
                        } else if (result.ageRestriction) {
                            showMessageWithLink(this, 'error', this.msgObjData['ageRestriction'], this.msgObjData['vermontMsgData']);
                        }else if (result.noPrivateAccessAssignmentFound) {
                            this.customErrorMessage = result.noPrivateAccessAssignmentFound;
                            this.template.querySelector('c-lwc-custom-toast').showCustomNotice();  
                        }   
                        else if (result.preRegGroupNotActive) {
                            showMessage(this, 'Error!', 'error', this.msgObjData['preRegGroupNotActive']);
                        } else if (result.noOpenEventsFoundInPreRegGroups) {
                            showMessage(this, 'Error!', 'error', result.noOpenEventsFoundInPreRegGroups);
                        } else {
                            // this.eventIds = result.eventIds;
                            this.eventIds = (result.eventIds) ? result.eventIds : (result.isSelectedDoseAlreadyScheduled && result.existingAppointmentData && this.appointmentId === result.existingAppointmentData.Id) ?result.existingAppointmentData.Event__r.Private_Access__r.Id : null;

                            this.showCalendar = true;
                             if (!result.isFirstAppointment) {
                                this.oldVaccineClass = result.oldVaccineClass;
                                this.isSecondDose = true;
                            }
                        }
                        this.showSpinner = false;


                    })
                    .catch(error => {
                        showAsyncErrorMessage(this, error);
                        this.showSpinner = false;
                    });
            }
        } else if (data.actionName === 'Schedule2ndDose') { // S-14429 (Second Dose)
            this.appointmentId = data.recordId;
            this.showCalendar = false;
            this.isSecondDose = false;
            this.showSpinner = true;

            checkExistingAppointmentFor2Dose({
                    appointmentId: this.appointmentId
                })
                .then(result => {
                    if (result.furtherDosesNotReqd) {
                        showMessageWithLink(this, 'error', this.msgObjData['furtherDosesNotReqd'], this.msgObjData['vermontMsgData']);
                    } else if (result.doseNumber) {
                        createMessage(result.doseNumber, this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyScheduled']);
                    } else if (result.isSelectedDoseAlreadyScheduled) {
                        showMessage(this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyScheduled']);
                    } else if (result.isSelectedDoseAlreadyCompleted) {
                        showMessage(this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyCompleted']);
                    } else if (result.isImmuneWeakNotAnsered) {
                        showMessage(this, 'Error!', 'error', result.isImmuneWeakNotAnsered);
                    } else if (result.ageRestriction) {
                        showMessageWithLink(this, 'error', this.msgObjData['ageRestriction'], this.msgObjData['vermontMsgData']);

                    }
                    else if (result.noPrivateAccessAssignmentFound) {
                        this.customErrorMessage = result.noPrivateAccessAssignmentFound;
                        this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
                        
                    } 
                    else if (result.preRegGroupNotActive) {
                        showMessage(this, 'Error!', 'error', result.preRegGroupNotActive);
                    } else if (result.noOpenEventsFoundInPreRegGroups) {
                        showMessage(this, 'Error!', 'error', result.noOpenEventsFoundInPreRegGroups);
                    }
                    else {
                        this.oldVaccineClass = result.oldVaccineClass;
                        this.eventIds = result.eventIds;

                        this.isSecondDose = true;
                        this.showCalendar = true;
                    }
                    this.showSpinner = false;
                })
                .catch(error => {
                    showAsyncErrorMessage(this, error);
                    this.showSpinner = false;
                });
        }
        //Commented for Issue I-78299
        // else if(data.actionName == 'Checked-In' && data.selectedTabName !== 'Cancelled Appointments') { 
        else if (data.actionName === 'Checked-In') {
            this.appointmentId = data.recordId;
            canCheckedIn({
                appointmentId: this.appointmentId
            }).then(result => {
                if (result) {
                    if (result.invalidAppointment) {
                        showMessage(this, 'Error!', 'error', this.msgObjData['invalidAppointment']);
                    }
                    if (result.furtherDosesNotReqd) {
                        showMessageWithLink(this, 'error', this.msgObjData['furtherDosesNotReqd'], this.msgObjData['vermontMsgData']);
                    } else if (result.isSelectedDoseAlreadyCompleted) {
                        showMessage(this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyCompleted']);
                    } else if (!result.canCheckIn) {
                        showMessage(this, 'Error!', 'error', this.msgObjData['checkInError']);
                    } else if (result.canCheckIn) {
                        this.showConfirmation = true;
                        this.modalType = 'complete';
                    }
                }
            }).catch(error => {
                showAsyncErrorMessage(this, error);

                // TODO Error handling
            });
        }
        else if (data.actionName === 'Cancel') {
            this.appointmentId = data.recordId;
            this.showConfirmation = true;
            this.modalType = 'cancel';
        } else if (data.actionName === 'Email') {
            this.appointmentId = data.recordId;
            sendEmail({
                appointmentId: this.appointmentId
            }).then(result => {
                if (result) {
                    this.dispatchEvent(new ShowToastEvent({
                        variant: 'success',
                        title: '',
                        message: EMAIL_CONFIRMATION_LABEL
                    }));
                } else {
                    this.dispatchEvent(new ShowToastEvent({
                        variant: 'error',
                        title: '',
                        message: 'Attendee does not have an email'
                    }));

                }
            }).catch(error => {
                showAsyncErrorMessage(this, error);

                // TODO Error handling
            });
        } else if (data.actionName === 'Download') {
            this.appointmentId = data.recordId;
            this.downloadMicro211Data();
        }
    }

    hideModal(event) {
        this.showCalendar = false;
        this.showConfirmation = false;
        this.isSecondDose = false;

        if (event.detail == 'save' || event.detail == 'cancel') {
            this.template.querySelectorAll('c-oktcgenericpaginator').forEach(element => {
                element.retrieveRecords();
            });
        } else if (event.detail == 'complete') {
            this.template.querySelector('c-oktcgenericpaginator').retrieveRecords();
        } 
    }

    resetFlag(event) {
        // this.resetTable = event.detail;
    }
    handleNoPrivateAccessFound(){
        this.showCalendar = false;
    }

    getSelectedIds(event) {
        let temparr = [];
        temparr = [...event.detail]
        temparr.forEach(record => {
            this.selectedAppointmentIds = [...this.selectedAppointmentIds, record];
        })
        this.showBatchModal = true;
    }

    hideBatchModal(event) {
        this.showBatchModal = false;
        if (event.detail === 'save') {
            this.template.querySelectorAll('c-oktcgenericpaginator').forEach(element => {
                element.retrieveRecords();
            });
        }
    }

    downloadMicro211Data() {
       

        if (location.href.includes('TestingSite') === true) {
            window.open("/TestingSite/VTTS_TP_Micro220_Download?appointmentIds=" + this.appointmentId, "_blank");
        } else {
            window.open("/apex/VTTS_TP_Micro220_Download?appointmentIds=" + this.appointmentId, "_blank");
        }
    }

    downloadCSVFile(record) {
        let rowEnd = '\n';
        let csvString = '';

        csvString += micro220Columns.join(',');
        csvString += rowEnd;
        let addressString = '';
       
        csvString += '"' + (record.Patient_FirstName__c ? record.Patient_FirstName__c : '') + '",';
        csvString += '"' + (record.Patient_LastName__c ? record.Patient_LastName__c : '') + '",';
        csvString += '"' + (record.Patient_Middle_Name__c ? record.Patient_Middle_Name__c : '') + '",';
        csvString += '"' + (record.Date_of_Birth__c ? record.Date_of_Birth__c : '') + '",';
        csvString += '"' + (record.Gender__c ? record.Gender__c : '') + '",';
        csvString += '"' + (record.Race__c ? record.Race__c : '') + '",';
        csvString += '"' + (record.Ethnicity__c ? record.Ethnicity__c : '') + '",';
        csvString += '"' + (record.Patient_Street__c ? record.Patient_Street__c : '') + '",';
        csvString += '"' + (record.Patient_City__c ? record.Patient_City__c : '') + '",'; //'Mailing Street'
        csvString += '"' + (record.Patient_State__c ? record.Patient_State__c : '') + '",';
        csvString += '"' + (record.Patient__r && record.Patient__r.MailingCountry ? record.Patient__r.MailingCountry : '') + '",';
        csvString += '"' + (record.Patient_Zip__c ? record.Patient_Zip__c : '') + '",';


        csvString += '"' + (record.MRN__c ? record.MRN__c : '') + '",';
        csvString += '"' + (record.Patient_Phone_Number__c ? record.Patient_Phone_Number__c : '') + '",';


        csvString += '"' + (record.EPIC_MRN__c ? record.EPIC_MRN__c : '') + '",';
        csvString += '"' + (record.Type_of_Insurance__c ? record.Type_of_Insurance__c : '') + '",';
        csvString += '"' + (record.Insured_First_Name__c ? record.Insured_First_Name__c : '') + '",';

        csvString += '"' + (record.Patient_s_relationship_to_insured__c ? record.Patient_s_relationship_to_insured__c : '') + '",';

        csvString += '"' + (record.Insurance_ID_Number__c ? record.Insurance_ID_Number__c : '') + '",';
        csvString += '"' + (record.Policy_Group_or_FECA_number__c ? record.Policy_Group_or_FECA_number__c : '') + '",';

        csvString += '"' + (record.Additional_Insurance_First_Name__c ? record.Additional_Insurance_First_Name__c : '') + '",';
        csvString += '"' + (record.Additional_Insurance_Last_Name__c ? record.Additional_Insurance_Last_Name__c : '') + '",';
        csvString += '"' + (record.Additional_Insurance_Policy_Group_Number__c ? record.Additional_Insurance_Policy_Group_Number__c : '') + '",';
        csvString += '"' + (record.Add_l_Insurance_Relationship_to_Insured__c ? record.Add_l_Insurance_Relationship_to_Insured__c : '') + '",';

        csvString += '"' + (record.Test_Ordered__c ? record.Test_Ordered__c : '') + '",';
        csvString += '"' + (record.Appointment_Date__c ? record.Appointment_Date__c : '') + '",';
        //csvString += '"' + (record.Patient__r.LeadSource ? record.Patient__r.LeadSource : '') + '",';
        csvString += '"' + (record.Patient__r && record.Patient__r.Source__c ? record.Patient__r.Source__c : '') + '",';


        csvString += '"' + (record.If_Other_Specimen_specify__c ? record.If_Other_Specimen_specify__c : '') + '",';
        csvString += '"' + (record.Name ? record.Name : '') + '",';
        csvString += '"' + (record.EPIC_Instrument_ID__c ? record.EPIC_Instrument_ID__c : '') + '",';
        csvString += '"' + (record.Reason_for_Testing__c ? record.Reason_for_Testing__c : '') + '",';
        csvString += '"' + (record.Submitting_Institution__c ? record.Submitting_Institution__c : '') + '",';
        csvString += '"' + (record.Ordering_Provider_Last_Name__c ? record.Ordering_Provider_Last_Name__c : '') + '",';
        csvString += '"' + (record.Ordering_Provider_First_Name__c ? record.Ordering_Provider_First_Name__c : '') + '",';
        csvString += '"' + (record.NPI__c ? record.NPI__c : '') + '",';
        csvString += '"' + (record.Ordering_Provider_Phone_Number__c ? record.Ordering_Provider_Phone_Number__c : '') + '",';
        csvString += '"' + (record.Ordering_Provider_Fax_Number__c ? record.Ordering_Provider_Fax_Number__c : '') + '",';
        csvString += '"' + (record.Patient_Email__c ? record.Patient_Email__c : '') + '",';
        csvString += '"' + (record.Event__r.Description__c ? record.Event__r.Description__c : '') + '",';
        csvString += '"' + (record.First_COVID_19_Test__c ? record.First_COVID_19_Test__c : '') + '",';
        csvString += '"' + (record.Employed_in_Healthcare__c ? record.Employed_in_Healthcare__c : '') + '",';
        csvString += '"' + (record.Symptomatic_as_defined_by_CDC__c ? record.Symptomatic_as_defined_by_CDC__c : '') + '",';
        csvString += '"' + (record.Symptoms_Onset_Date__c ? record.Symptoms_Onset_Date__c : '') + '",';
        csvString += '"' + (record.Is_the_patient_hospitalized__c ? 'Yes' : 'No') + '",';
        csvString += '"' + (record.Is_the_patient_currently_in_the_ICU__c ? 'Yes' : 'No') + '",';
        csvString += '"' + (record.Resident_in_Congregate_Care_Setting__c ? record.Resident_in_Congregate_Care_Setting__c : '') + '",';
        csvString += '"' + (record.Are_you_pregnant__c ? record.Are_you_pregnant__c : '') + '",';

       


        csvString += rowEnd;

         let downloadElement = document.createElement('a');

           downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
        downloadElement.target = '_self';
        // CSV File Name
        downloadElement.download = 'micro220.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
        downloadElement.click();

    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @desc this method fetches the VTS Event based on AccountId
     */

    fetchEvents() {
        if (this.eventOptions.length === 0) {
            fetchEvents({
                    accountId: this.recordId
                })
                .then(result => {
                    let tempArray = [{
                        label: '--Select Event--',
                        value: ''
                    }];
                    result.forEach((record) => {
                        tempArray.push({
                            label: (record.Description__c === undefined ? record.Name + ' (' + record.Start_Date__c + ' - ' + record.End_Date__c + ')' : record.Description__c + ' (' + record.Start_Date__c + ' - ' + record.End_Date__c + ')'),
                            value: record.Id
                        });
                    })
                    this.eventOptions = tempArray;
                })
                .catch(error => {
                    showAsyncErrorMessage(this, error);
                });
        }
        this.showEventsList = true;
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @desc Runs the batch to download Micro220 Form in Bulk
     */
    downloadMicro220() {
        this.template.querySelector('.downloadMicro220').disabled = true;
        let vtsEventId = this.template.querySelector('lightning-combobox').value;
        if (vtsEventId !== '' && vtsEventId !== undefined) {
            runMicro220Batch({
                    eventId: vtsEventId,
                    labCenterId: this.recordId
                })
                .then(result => {
                    this.showToast('success', this.micro220SuccessMsg);
                    this.template.querySelector('.downloadMicro220').disabled = false;
                })
                .catch(error => {
                    this.showToast('error', error.body.message);
                    this.template.querySelector('.downloadMicro220').disabled = false;
                });
        }
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @desc This method fires when user change VTS event to download Micro220
     */
    handleVTSEventChange(event) {
        if (event.detail.value !== undefined && event.detail.value !== '') {
            this.template.querySelector('.downloadMicro220').disabled = false;
            this.whereClause = this.whereClauseFinal + ' AND Event__c = \'' + event.detail.value + '\'';
            this.reloadTable = true;
        } else {
            this.whereClause = this.whereClauseFinal;
            this.reloadTable = true;
            this.template.querySelector('.downloadMicro220').disabled = true;
        }
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @desc Notification Utility
     */
    showToast(variant, message) {
        this.dispatchEvent(new ShowToastEvent({
            message: message,
            variant: variant
        }));
    }
}