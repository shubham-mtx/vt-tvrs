import {
    LightningElement,
    track,api
} from 'lwc';

import getAccountdetails from '@salesforce/apex/OKPC_LocationMapController.getAccountdetails';
import getSiteAccounts from '@salesforce/apex/DC_ScheduleAppointment.getSiteAccounts';
import identifyVaccinationType from '@salesforce/apex/DC_ScheduleAppointment.identifyVaccinationType';
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';


export default class Vt_ts_pick_location_scheduler extends LightningElement {
    @api accountId;
    @api isFromApptScheduler;
    @track showSpinner = false;
    @track showMapModal = false;
    @track showSingleMap = false;
    @track showLocationDetails = false;
    @track showtestingSite = false;
    @track hideAppointment = false;
    @track hidePrefferedTime = true;
    @track canSchedule = false;
    @track isModalOpen = false;
    @track ifcovid = false;
    @track appointmentId = null;
    @track selectValue = null;
    @api eventId;
    @api preRegMinDateTime;
    @api eventIds;
    @track mapMarkers = [];
    @track accountDetails = {};
    @track location = {};
    @track selectedSlotId = '';
    @track DateFrom;
    @track DateTo;
    @track TimeFrom;
    @track TimeTo;
    @track map;

    @track eventProcess;
    @track isIdentifyingEventProcess = false;
    @track vaccinationType;


    get isScheduleAppointmentDisabled() {
        return !this.eventId;
    }

    handleMapModalClose(event) {
        this.showMapModal = false;
        if (event.detail !== 'close') {
            this.selectValue = event.detail
            this.canSchedule = true;
            this.getAccount(this.selectValue);
        }
        
    }

    handleEventSelected(event) {
        this.eventId = event.detail.eventId;
        this.selectValue = event.detail.accountId;
        this.getAccount(this.selectValue);
        this.closeModal();
    }

    handlesingleMapClose() {
        this.showSingleMap = false;
    }

    handleSingleMap() {
        this.showSingleMap = true;
    }

    handleMapModal() {
        this.showMapModal = true;
    }

    showAppointments() {
        this.isModalOpen = true;
    }

    handleCloseModal(event) {
        this.isModalOpen = false;
    }

    getAccount(Id) {
        this.showSpinner = true;

        const selectedEvent = new CustomEvent("accountselected", {
            detail: Id
        });
        this.dispatchEvent(selectedEvent);

        getAccountdetails({
                accountId: Id
            })
            .then(result => {
                this.accountDetails = JSON.parse(JSON.stringify(result));
                this.location = result.location;
                if (this.accountDetails) {
                    this.showLocationDetails = true;
                    this.mapMarkers.length = 0;
                    this.mapMarkers.push(this.accountDetails);
                }
                if (result.businessStartTime) {
                    this.preferredTimeMin = this.msToTime(result.businessStartTime);
                } else {
                    this.preferredTimeMin = '07:00:00.000Z';
                }
                if (result.businessEndTime) {
                    this.preferredTimeMax = this.msToTime(result.businessEndTime);
                } else {
                    this.preferredTimeMax = '19:00:00.000Z';
                }
                this.showSpinner = false;
            })
            .catch(error => {
                showAsyncErrorMessage(this,error);
                this.showSpinner = false;
            })
    }

    msToTime(s) {
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;

        return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs) + '.' + this.pad(ms, 3);
    }

    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }

    retrieveTestingSites() {
        getSiteAccounts({})
            .then(data => {
                data.forEach((account) => {
                    var optionValue = new Object();
                    optionValue.label = account.Name;
                    optionValue.value = account.Id;
                    this.accountList.push(optionValue);
                    this.map.set(account.Id, account.Name);
                    this.selectSiteName = this.map.get(this.selectValue);
                });
                this.options = this.accountList;
            })
            .catch(error => {
                showAsyncErrorMessage(this,error);
            });
    }

    connectedCallback() {
        this.isIdentifyingEventProcess = false;
        this.ifcovid = true;
        this.todayDate = this.getTodayDate();
        this.retrieveTestingSites();


        if( this.accountId ){
            this.selectValue = this.accountId;
            this.getAccount(this.accountId);

            if( this.eventId ){
                identifyVaccinationType({ eventId: this.eventId })
                .then(data => {

                    if( data === 'Vaccination-1' || data === 'Vaccination-2' ){
                        this.eventProcess = 'Vaccination';
                        this.vaccinationType = data;
                    }
                    else if( data === 'Testing Site'){
                        this.eventProcess = 'Testing Site';
                    }
                    this.isIdentifyingEventProcess = true;
                })
                .catch(error => {
                    let message = (error)?((error.message)?error.message:((error.body)?((error.body.message)?error.body.message:JSON.stringify(error)):JSON.stringify(error))):"Something went wrong!";
                    showAsyncErrorMessage(this,message);
                });
            }
            else{
                this.isIdentifyingEventProcess = true;
            }
        }
        else{
            this.isIdentifyingEventProcess = true;
        }
    }

    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }

    setId(event){
        this.selectedSlotId = event.detail;
        const selectedEvent = new CustomEvent("submit", {
            detail: {eventId : this.eventId, slotId : this.selectedSlotId, selAccId : this.selectValue}
        });
        this.dispatchEvent(selectedEvent);
        this.isModalOpen = false;
    }

    setSlotId(event){
        
    }

    closeModal(){
        this.showMapModal = false;
    }

    disableButton(){
        
    }
}