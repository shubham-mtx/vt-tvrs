/**
 * @description       : 
 * @author            : Balram Dhawan
 * @group             : 
 * @last modified on  : 01-13-2022
 * @last modified by  : Balram Dhawan
**/
import { LightningElement, track } from 'lwc';
import TVRS_INTERNAL_STYLE from '@salesforce/resourceUrl/TVRS_InternalStyle';
import { loadStyle } from 'lightning/platformResourceLoader';

export default class OkpcAdminAppointmentSchedule extends LightningElement {
    currentStep = 1;
    accountId;
    eventId;
    slotId;
    registrationComplete = false;

    @track DateFrom;
    @track DateTo;
    @track TimeFrom;
    @track TimeTo;
    @track preRegMinDate;
    @track preRegMinTime;
    @track eventIds;

    get isStep1() {
        return this.currentStep === 1;
    }

    get isStep2() {
        return this.currentStep === 2;
    }

    get isStep3() {
        return this.currentStep === 3;
    }

    get modalHeader() {
        return this.isStep1 ? 'Testing Site & Event' : this.isStep2 ? 'Appointment Slot' : 'Contact Information';
    }

    get isNextEnabled() {
        return !((this.isStep1 && this.accountId && this.eventId) || (this.isStep2 && this.slotId));
    }


    get contentStyle() {
        return this.isStep3 ? '' : 'overflow-y: auto; height: 410px;';
    }

    loadStaticResource() {
        Promise.all([
            loadStyle(this, TVRS_INTERNAL_STYLE)
        ]).then(() => {
            console.log('css loaded');
        }).catch((error) => {
            showAsyncErrorMessage(this, error);
        });
    }    

    connectedCallback() {
        this.loadStaticResource();
    }

    onload(event){
    }

    handleClick(event) {
        switch (event.target.name) {
            case 'previous':
                this.currentStep--;
                if (this.isStep1) {
                    this.slotId = '';
                }
                this.accountId = '';
                this.eventId = '';
                break;
            case 'next':
                this.currentStep++;
                break;
            case 'save':
                this.template.querySelector('c-vtpc-create-contact').doRegisteration();
                break;
            case 'finish':
                this.currentStep = 1;
                this.accountId = '';
                this.eventId = '';
                this.slotId = '';
                this.registrationComplete = false;
                break;

            default:
                break;
        }
    }

    backToAppointment(){
        this.currentStep = 1;
        this.slotId = '';
        this.eventId = '';

    }
    handleEventSelected(event) {
        this.accountId = event.detail.accountId;
        this.eventId = event.detail.eventId;
        this.DateFrom = event.detail.DateFrom;
        this.DateTo = event.detail.DateTo;
        this.TimeFrom = event.detail.TimeFrom;
        this.TimeTo = event.detail.TimeTo;
    }

    handleSlotSelected(event) {
        this.slotId = event.detail;
    }

    handleRegistrationSuccess() {
        this.registrationComplete = true;
    }

    updateMinDateTimePreReg(event){
        
        this.preRegMinDateTime = event.detail.preRegMinDateTime;
        this.eventIds = event.detail.eventIds;
    }
}