/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 01-15-2022
 * @last modified by  : Vamsi Mudaliar
**/
@isTest
public class OKPC_LocationMapControllerTest {
    private static final String LabSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
    private static final String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
    @testSetup    
    public static void makeData(){
        String recordTypeID = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        String citizenCovidId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Event_Process__c = 'Testing Site';
        createAccountTestSite.Event_Type__c = 'Other';
        createAccountTestSite.Active__c = true;
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;
        
        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-44151',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            what_was_the_last_date_of_contact__c = Date.newInstance(2020, 2, 20),
            Did_you_have_a_nasal_swab__c = 'Yes	',
            What_was_your_test_result__c = 'Negative',
            Approximately_what_date_were_you_tested__c = Date.newInstance(2020, 3, 3),
            Had_contact_with_anyone_diagon__c = 'Yes',
            Do_you_take_prescription_medications__c = '	Yes',
            medication_for_fever_in_the_past_3_days__c = 'Yes',
            First_Date_you_have_been_feeling_sick__c = Date.newInstance(2020, 3, 1) ,
            First_date_you_began_feeling_well__c= Date.newInstance(2020, 3, 10),
            Fever_Cough_or_Difficulty_breathing__c = 'Yes',
            Sick_since_Jan_1_2020__c = 'Yes',
            Opt_out_for_SMS_Updates__c = true,
            HasOptedOutOfEmail = false,
            Current_Step__c = 1,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con;
        
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType IN : customerUserTypes  limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            //userRoleId = portalRole.Id,
            contactId = con.id
        );
        insert newUser;
        
        Account acc = new Account(
            RecordTypeId = recordTypeID,
            Name = 'test',
            BillingStreet = 'test',
            BillingCity = 'test',
            BillingState = 'test',
            BillingCountry = 'test',
            BillingPostalCode = 'test',
            Active__c = true,
            Vermont_County__c = 'Addison',
            Type = 'Testing Site',
            Number_of_Weeks_Available__c= '3',
            BillingLatitude = 0.12,
            BillingLongitude = 0.13,
            Site_URL__c = 'test.com',
            Event_Type__c = 'Other',
            Event_Process__c = 'Vaccination'
        );
        insert acc;
        
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'Test';
        contactRecord.LastName = '00002';
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord.Email = 'test2@test.com';
        contactRecord.AccountId = acc.Id;
        contactRecord.Consent__c = true;
        INSERT contactRecord;
        
        Date appointmentDate = Date.today();
        String startTime= '10.30';
        String endTime ='12.30';
        Private_Access__c pAccess = new Private_Access__c(
            Name = 'access1',
            Is_Active__c = true,
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Event_Process__c = 'Vaccination'
        );
        insert pAccess;
        Private_Access__c pAccess2 = new Private_Access__c(
            Name = 'access2',
            Vaccine_Type__c = 'Vaccination-2'
        );
        insert pAccess2;
        
        VTS_Event__c evt = new VTS_Event__c(
            Location__c = acc.Id,
            Status__c = 'Closed Citizen Portal',
            Start_Date__c = Date.today(),
            End_Date__c = Date.today()+1,
            Private_Access__c = pAccess.Id,
            End_Time__c = Time.newInstance(12, 0, 0, 0)
        );
        insert evt;
        
        VTS_Event__c evt2 = new VTS_Event__c(
            Location__c = acc.Id,
            Status__c = 'Open',
            Start_Date__c = Date.today(),
            End_Date__c = Date.today()+1,
            Private_Access__c = pAccess2.Id
        );
        insert evt2;
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, acc.Id);

        createSlot.Date__c = Date.today()+5;
        createSlot.Start_Time__c = Time.newInstance(11, 0, 0, 0);
        update createSlot;
        


        Appointment__c appointment3 = new Appointment__c(
        	Patient__c = contactRecord.Id,
            Status__c = 'Completed',
            Appointment_Slot__c = createSlot.id,
            Event__c = evt2.Id
        );
        insert appointment3;
        
        Appointment__c appointment4 = new Appointment__c(
        	Patient__c = contactRecord.Id,
            Status__c = 'Scheduled',
            Event__c = evt2.Id
        );
        insert appointment4;
        
        Appointment__c appointment = new Appointment__c(
        	Patient__c = contactRecord.Id,
            Status__c = 'Completed',
            Event__c = evt.Id
        );
        insert appointment;

        // Appointment__c appointment2 = new Appointment__c(
        // 	Patient__c = contactRecord.Id,
        //     Status__c = 'Scheduled',
        //     Event__c = evt.Id
        // );
        // insert appointment2;
    }
   
    @isTest
    static void testGetTestingSites(){
        User usr = [select id from User where userName='newUser@yahoo.com'];
        Account acc = [SELECT Id, Event_Type__c, Site_URL__c From Account WHERE Name = 'test' AND Event_Type__c = 'Other'];
        Test.startTest();
        List<OKPC_LocationMapController.MapMarker> mapList = OKPC_LocationMapController.getTestingSites();
        
        system.runAs(usr){
            try{
                system.assertEquals(true, OKPC_LocationMapController.getTestingSites().size() > 0, 'Sites Found');
            } Catch(Exception e){
                
            }
        }
        Test.stopTest();
    }
    @isTest
    static void testGetAccountdetails(){
        
        Account acc = [SELECT Id From Account WHERE Name = 'test' AND Site_URL__c='test.com'];
        Test.startTest();
        OKPC_LocationMapController.MapMarker mapValue = OKPC_LocationMapController.getAccountdetails(acc.Id);
        
        User usr = [select id from User where userName='newUser@yahoo.com'];
        system.runAs(usr){
            try{
                system.assertEquals(true, OKPC_LocationMapController.getAccountdetails(acc.Id) != null, 'Account Details Found');
            }catch(Exception e){
                
            }
        }
        Test.stopTest();
    }
    
    @isTest 
    static void getEventsTest(){
        User usr = [select id from User where userName='newUser@yahoo.com'];
        
        Account acc = [SELECT Id From Account WHERE Name = 'test'];
        
        String res = '{"accountId":"'+acc.Id+'","cTime":"01:00:0"}';

        Test.startTest();
        List<OKPC_LocationMapController.EventWrapper> eventList2 = OKPC_LocationMapController.getEvents(res);
                
        system.runAs(usr){
            try{
                List<OKPC_LocationMapController.EventWrapper> eventList1 = OKPC_LocationMapController.getEvents(res);
               // system.assertEquals(true, eventList1.size() > 0, 'Events Found');
            }catch(Exception e){
                System.assert(true,e.getMessage());
            }
        }
        Test.stopTest();
    }
    
    @isTest
    static void getTestingSiteWithFilterTest(){
        
        Account acc = [SELECT Id From Account WHERE Name = 'test'];
        Private_Access__c access = [SELECT Id FROM Private_Access__c WHERE Name = 'access1'];
        String json = '{"sTime":"22:00:05","eTime":"23:00:05","sDate":"1/1/2020","eDate":"12/12/2020","county":"test","sponBy":"test"}';
        Test.startTest();
        List<OKPC_LocationMapController.MapMarker> objList = OKPC_LocationMapController.getTestingSitesWithFilters(json,acc.Id, access.Id,'test','Moderna');
        User usr = [select id from User where userName='newUser@yahoo.com'];
        system.runAs(usr){
            try{
                OKPC_LocationMapController.getTestingSitesWithFilters(json,acc.Id, access.Id,'test','Moderna');
                system.assertEquals(false, OKPC_LocationMapController.getTestingSitesWithFilters(json,acc.Id, access.Id,'test','Moderna').size() > 0, 'Events Not Found');
            } catch(Exception e){
                
            }
        }
        Test.stopTest();
    }
    
    @isTest
    static void getTestingSiteWithFilterTest2(){
        
        Account acc = [SELECT Id From Account WHERE Name = 'test'];
        Private_Access__c access = [SELECT Id FROM Private_Access__c WHERE Name = 'access1'];
        // String json = '{"sTime":"22:00:05","eTime":"23:00:05","sDate":"10/20/2020","eDate":"12/20/2020","county":"test","sponBy":"test"}';
        String json = '{"vaccineType":"Vaccination-1","eventProcess":"Vaccination","privateAccess":"Testing","sTime":"22:00:05","eTime":"23:00:05","sDate":"1/1/2020","eDate":"12/12/2020","county":"test","sponBy":"Other-Vaccination","eventType":"Private Access"}';
            
        Test.startTest();
        try{
            List<OKPC_LocationMapController.MapMarker> objList = OKPC_LocationMapController.getTestingSitesWithFilters(json,acc.Id, access.Id,'test','Moderna');
            system.assertEquals(false, objList.size() > 0, 'Map Marker Found');
        }
        catch(Exception ex) {
            // Error handle Part
        }
        Test.stopTest();
    }
    
    @isTest
    static void getEventsWithFilters(){
        
        Account acc = [SELECT Id From Account WHERE Name = 'test'];
        Private_Access__c access = [SELECT Id FROM Private_Access__c WHERE Name = 'access1'];
        Private_Access__c pAccess2 = [SELECT Id FROM Private_Access__c WHERE Name = 'access2'];
        String temp =pAccess2.Id;
       // String json = '{"sTime":"22:00:05","eTime":"23:00:05","sDate":"10/20/2020","eDate":"12/20/2020","county":"test","sponBy":"test","privateAccess":"Testing"}';
       String json = '{"vaccineType":"Vaccination-1","eventProcess":"Vaccination","privateAccess":"Testing","sTime":"22:00:05","eTime":"23:00:05","sDate":"1/1/2020","eDate":"12/12/2020","county":"test","sponBy":"Other-Vaccination","eventType":"Private Access"}';
         
       Test.startTest();
       try{
            List<OKPC_LocationMapController.EventWrapper> objList1 = OKPC_LocationMapController.getEventsWithFilters(json,acc.Id, access.Id,'test','Moderna');
       }
       catch(Exception ex) {
           // Error Handler
       }
        User usr = [select id from User where userName='newUser@yahoo.com'];
        system.runAs(usr){
            try{
                system.assertEquals(true, OKPC_LocationMapController.getEventsWithFilters(json,acc.Id, access.Id,'test','Moderna').size() > 0, 'Events Found');
            } catch(Exception e){
                
            }
        }
        
        Test.stopTest();
    }

    @isTest
    static void getFetchPrivateAccess_Case1(){
       // List<OKPC_LocationMapController.PrivateAccess> listOfPrivateAccess = OKPC_LocationMapController.fetchPrivateAccess('Vaccination-1','Moderna','test');
    	User usr = [select id from User where userName='newUser@yahoo.com'];
        
    // public class PrivateAccessWrapper{
    //     @AuraEnabled public string eProcess;
    //     @AuraEnabled public string vaccineBrand;
    //     @AuraEnabled public string vaccinationPrivateAccess;
    //     @AuraEnabled public Boolean isSecondDose;
    // }

        Private_Access__c privateAccessCase1 = new Private_Access__c(
                Name = 'private test',
                Event_Process__c = 'Vaccination',
                Vaccine_Class_Name__c = 'Moderna',
                Vaccine_Type__c = 'Vaccination-2',
                Age_Based__c = 44,
                Requires_Followup_Dose__c = 'Yes',
                Min__c = 1,
                Max__c = 2,
                Is_Active__c = true
            );
            insert privateAccessCase1;


            String response = '{"eProcess":"Vaccination","vaccineBrand":"Moderna","vaccinationPrivateAccess":"'+privateAccessCase1.Id+'","isSecondDose":true}';

       // system.runAs(usr){
                Test.startTest();
                    try{
                        OKPC_LocationMapController.fetchPrivateAccess(response);
                    }
                    catch(Exception e) {

                    }
                Test.stopTest();
        //}
    }
    
    @isTest
    static void getFetchPrivateAccess_Case1234(){
       // List<OKPC_LocationMapController.PrivateAccess> listOfPrivateAccess = OKPC_LocationMapController.fetchPrivateAccess('Vaccination-1','Moderna','test');
       String recordTypeID = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
       //User usr = [select id from User where userName='newUser@yahoo.com'];
       Account acc = [SELECT Id FROM Account WHERE RecordTypeId =:recordTypeID LIMIT 1]; 
       Contact con = [SELECT Id FROM Contact WHERE LastName = 'portalTestUserv1' LIMIT 1];
        
        Private_Access__c privateAccessCase1 = new Private_Access__c(
                Name = 'private test',
                Event_Process__c = 'Vaccination',
                Vaccine_Class_Name__c = 'Moderna',
                Vaccine_Type__c = 'Vaccination-2',
                Age_Based__c = 44,
                Requires_Followup_Dose__c = 'Yes',
                Min__c = 1,
                Max__c = 2,
                Is_Active__c = true,
                Is_Alternate_Dose_Available__c=true,
                Alternate_Doses__c='Moderna;Pfizer'
            );
            insert privateAccessCase1;
        
            Private_Access__c privateAccessCase12 = new Private_Access__c(
                Name = 'private test 13',
                Event_Process__c = 'Vaccination',
                Vaccine_Class_Name__c = 'Moderna',
                Vaccine_Type__c = 'Vaccination-3',
                Age_Based__c = 44,
                Requires_Followup_Dose__c = 'Yes',
                Min__c = 1,
                Max__c = 2,
                Is_Active__c = true,
                Is_Alternate_Dose_Available__c=true,
                Alternate_Doses__c='Moderna;Pfizer'
            );
            insert privateAccessCase12;
        
        VTS_Event__c event = new VTS_Event__c(
            Location__c = acc.Id,
            Status__c = 'Open',
            Start_Date__c = Date.today(),
            End_Date__c = Date.today()+1,
            Private_Access__c = privateAccessCase1.Id,
            End_Time__c = Time.newInstance(12, 0, 0, 0)
        );

        Pre_Registration_Group__c prGroup = new Pre_Registration_Group__c();
        prGroup.Status__c = 'Activate';
        prGroup.Group_Type__c = 'Chronic';
        prGroup.Email_Acknowledgement__c = true;
        prGroup.Verification_Delay__c = 72;
        prGroup.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        insert prGroup;
        

        Pre_Registration_Group__c prGroup1 = new Pre_Registration_Group__c();
        prGroup1.Status__c = 'Activate';
        prGroup1.Group_Type__c = 'Chronic';
        prGroup1.Email_Acknowledgement__c = true;
        prGroup1.Verification_Delay__c = 72;
        prGroup1.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        insert prGroup1;
        

        Pre_Registration__c pr = new Pre_Registration__c(
            Contact__c = con.Id,
            Chronic_Condition__c='No',
            // Are_you_eligible_for_Vaccine_Risk_Group__c='No',
            Pre_Registration_Group__c=prGroup.Id
        );
        insert pr;

        Private_Access_Assignment__c pAssign = new Private_Access_Assignment__c(
            Private_Access_Groups__c = privateAccessCase12.Id,
            Pre_Registration_Groups__c = prGroup.Id,
            Active__c = true
        );
        insert pAssign;

        Private_Access_Assignment__c pAssign1 = new Private_Access_Assignment__c(
            Private_Access_Groups__c = privateAccessCase1.Id,
            Pre_Registration_Groups__c = prGroup1.Id,
            Active__c = true
        );
        insert pAssign1;

            String response = '{"eProcess":"Vaccination-3","vaccineBrand":"Moderna","vaccinationPrivateAccess":"'+privateAccessCase1.Id+'","isSecondDose":true,"contactId":"'+con.Id+'","isFirstAppointment":false}';
            String response1 = '{"eProcess":"Vaccination-3","vaccineBrand":"Moderna","vaccinationPrivateAccess":"'+privateAccessCase1.Id+'","isSecondDose":true,"contactId":"","isFirstAppointment":false}';

                Test.startTest();
                    try{
                        OKPC_LocationMapController.fetchPrivateAccess(response);

                        OKPC_LocationMapController.fetchPrivateAccess(response1);
                    }
                    catch(Exception e) {
                        System.assert(true,'ERROR');
                    }
                Test.stopTest();
    }
    


    @isTest
    static void getFetchPrivateAccess_Case2(){
      //  List<OKPC_LocationMapController.PrivateAccess> listOfPrivateAccess = OKPC_LocationMapController.fetchPrivateAccess(response);
    	User usr = [select id from User where userName='newUser@yahoo.com'];

        String response = '{"eProcess":"Testing Site","vaccineBrand":"Moderna","isSecondDose":true}';

        system.runAs(usr){
            try{
                OKPC_LocationMapController.fetchPrivateAccess(response);
            }catch(Exception e){
                System.debug(e.getMessage());
            }
        }
    }

    @isTest
    static void getAppointmentEventTypeTest(){
        Id privateAccess = [SELECT Id, Name FROM Private_Access__c WHERE Name = 'access2'].Id;
        Id eventId = [SELECT Id, Name FROM VTS_Event__c WHERE Private_Access__c =:privateAccess].Id;
        Id appointmentId = [SELECT Id, Name FROM Appointment__c WHERE Status__c = 'Scheduled' LIMIT 1].Id;
        Id appointmentId2 = [SELECT Id, Name FROM Appointment__c WHERE Event__c=:eventId LIMIT 1].Id;
        User usr = [select id from User where userName='newUser@yahoo.com'];
        Test.startTest();
        Appointment__c appointment = OKPC_LocationMapController.getAppointmentEventType(appointmentId);
        Appointment__c appointment2 = OKPC_LocationMapController.getAppointmentEventType(appointmentId2);
        system.runAs(usr){
            try{
                system.assertEquals(true, OKPC_LocationMapController.getAppointmentEventType(appointmentId2) != null,'Appointment Found with Event');
            } catch(Exception e){
                
            }
        }
        Test.stopTest();
    }
    
    @isTest
    static void getAvailableLocationAndEventTest_Case1(){
        Map<Id, List<VTS_Event__c>> finallAvailableAccIdAndEvents = new Map<Id, List<VTS_Event__c>>();
        Map<Id, Account> finallAvailableAccIdAndAccounts = new Map<Id, Account>();
        Account acc = [SELECT Id From Account WHERE Name = 'test'];
        Private_Access__c access = [SELECT Id FROM Private_Access__c WHERE Name = 'access1'];
        String json = '{"vaccineType":"Vaccination-1","sTime":"22:00:05","ctime":"23:59:00","eTime":"23:00:05","sDate":"1/1/2020","eDate":"12/12/2022","county":"test","sponBy":"Other","privateAccess":"Testing","eventType":"Private Access"}';
        try{
            OKPC_LocationMapController.getAvailableLocationAndEvent(json,acc.Id,access.Id, finallAvailableAccIdAndEvents, finallAvailableAccIdAndAccounts,'Test', 'Moderna');
            OKPC_LocationMapController.getAvailableLocationAndEvent(null,null,null, null, null,'', '');
            system.assertEquals(true, acc != null, 'Account Found');
        }catch(Exception e){
            
        }
    }

    @isTest
    static void getAvailableLocationAndEventTest_Case1203(){
        Map<Id, List<VTS_Event__c>> finallAvailableAccIdAndEvents = new Map<Id, List<VTS_Event__c>>();
        Map<Id, Account> finallAvailableAccIdAndAccounts = new Map<Id, Account>();
        Account acc = [SELECT Id From Account WHERE Name = 'test'];
        Private_Access__c access = [SELECT Id FROM Private_Access__c WHERE Name = 'access1'];
        String json = '{"vaccineType":"Vaccination-1","sTime":"22:00:05","ctime":"23:59:00","eTime":"23:00:05","sDate":"1/1/2020","eDate":"12/12/2022","county":"test","sponBy":"Other","privateAccess":"All","eventType":"Private Access","eventProcess":"Vaccination","vaccineClassList":["Moderna","Pfizer"]}';
       Test.startTest();
            OKPC_LocationMapController.getAvailableLocationAndEvent(json,acc.Id,access.Id, finallAvailableAccIdAndEvents, finallAvailableAccIdAndAccounts,'Test', 'All');
       Test.stopTest();
    }

    @isTest
    static void getAvailableLocationAndEventTest_Case1204(){
        
        Account acc = [SELECT Id From Account WHERE Name = 'test'];
        Private_Access__c access = [SELECT Id FROM Private_Access__c WHERE Name = 'access1'];
        
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, acc.Id);
        createSlot.Date__c = Date.today()+5;
        createSlot.Start_Time__c = Time.newInstance(22, 10, 0, 0);
        createSlot.Appointment_Count__c=0;
        update createSlot;

        Map<Id, List<VTS_Event__c>> finallAvailableAccIdAndEvents = new Map<Id, List<VTS_Event__c>>();
        Map<Id, Account> finallAvailableAccIdAndAccounts = new Map<Id, Account>();
        
        String json1 = '{"vaccineType":"Vaccination-1","sTime":"22:00:05","ctime":"23:59:00","eTime":"23:00:05","sDate":"1/1/2020","eDate":"12/12/2022","county":"Addison","sponBy":"Test","privateAccess":"Testing","eventType":"Private Access","eventProcess":"Vaccination","vaccineClassList":["Moderna","Pfizer"]}';
        String json2 = '{"vaccineType":"Vaccination-1","sTime":"22:00:05","ctime":"23:59:00","eTime":"23:00:05","sDate":"1/1/2020","eDate":"12/12/2022","county":"Addison","sponBy":"Test","privateAccess":"Testing","eventType":"Private Access","eventProcess":"Vaccination","vaccineClassList":["Moderna","Pfizer"]}';
        
        Test.startTest();
            try{
            OKPC_LocationMapController.getAvailableLocationAndEvent(json1,acc.Id,access.Id, finallAvailableAccIdAndEvents, finallAvailableAccIdAndAccounts,'Test', 'Moderna');
            
            OKPC_LocationMapController.getAvailableLocationAndEvent(json2,acc.Id,access.Id, finallAvailableAccIdAndEvents, finallAvailableAccIdAndAccounts,'Test', 'Moderna');
            
            //OKPC_LocationMapController.getAvailableLocationAndEvent(null,null,null, null, null,'', '');
            system.assertEquals(true, acc != null, 'Account Found');
            }
            catch(Exception ex) {
                System.debug('>>>>ERROR 1222' + ex.getMessage());
            }
        Test.stopTest();
    }

    @isTest
    static void getAvailableLocationAndEventTest_Case30(){

        Map<Id, List<VTS_Event__c>> finallAvailableAccIdAndEvents = new Map<Id, List<VTS_Event__c>>();
        Map<Id, Account> finallAvailableAccIdAndAccounts = new Map<Id, Account>();
        Account acc = [SELECT Id From Account WHERE Name = 'test'];
        Private_Access__c access = [SELECT Id FROM Private_Access__c WHERE Name = 'access1'];

        Appointment_Slot__c appSlot = new Appointment_Slot__c(
            Account__c=acc.Id,
            Appointment_Count__c=0,
            Date__c = Date.parse('03/12/2021'),
            Start_Time__c = Time.newInstance(22, 0, 0, 0)
        );
        insert appSlot;

        String json1 = '{"vaccineType":"Vaccination-2","isFirstAppointment":false,"sTime":"22:00:05","ctime":"23:59:00","eTime":"23:00:05","sDate":"1/1/2020","eDate":"12/12/2022","county":"Addison","sponBy":"Other-Testing Site","privateAccess":"All","eventType":"Private Access1","eventProcess":"Vaccination","vaccineClassList":["Moderna","Pfizer"]}';
        String json2 = '{"vaccineType":"Vaccination-2","isFirstAppointment":false,"sTime":"22:00:05","ctime":"23:59:00","eTime":"23:00:05","sDate":"1/1/2020","eDate":"12/12/2022","county":"Addison","sponBy":"TestPortal","privateAccess":"All","eventType":"Private Access1","eventProcess":"Vaccination","vaccineClassList":["Moderna","Pfizer"]}';
        String json3 = '{"vaccineType":"Vaccination-2","isFirstAppointment":false,"sTime":"21:00:00","ctime":"23:59:00","eTime":"23:00:00","sDate":"1/1/2020","eDate":"12/12/2022","county":"Addison","sponBy":"TestPortal","privateAccess":"All","eventType":"Private Access1","eventProcess":"Vaccination","vaccineClassList":["Moderna","Pfizer"]}';
        
        Test.startTest();
        try{
            OKPC_LocationMapController.getAvailableLocationAndEvent(json1,acc.Id,access.Id, finallAvailableAccIdAndEvents, finallAvailableAccIdAndAccounts,'Test', 'Moderna');
            OKPC_LocationMapController.getAvailableLocationAndEvent(json2,acc.Id,access.Id, finallAvailableAccIdAndEvents, finallAvailableAccIdAndAccounts,'Test', 'Moderna');
            OKPC_LocationMapController.getAvailableLocationAndEvent(json3,acc.Id,access.Id, finallAvailableAccIdAndEvents, finallAvailableAccIdAndAccounts,'Test', 'Moderna');
       
        }catch(Exception e){
            System.assert(true,'ERROR');
        }
        Test.stopTest();
    }




    public static void prepareDataForCase1() {
        
        Account accountRecord = new Account();
        accountRecord.Name = 'Self Registration';
        accountRecord.Event_Process__c = 'Vaccination';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Test_Center').getRecordTypeId();
        INSERT accountRecord;

        Contact contactRecordCase1 = new Contact();
        contactRecordCase1.FirstName = 'Test1';
        contactRecordCase1.LastName = '00036';
        contactRecordCase1.Birthdate = date.today()-10;
        contactRecordCase1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecordCase1.Email = 'test1@test.com';
        contactRecordCase1.AccountId = accountRecord.Id;
        contactRecordCase1.Consent__c = true;
        INSERT contactRecordCase1;
        

        Private_Access__c privateAccessCase1 = new Private_Access__c(
            Name = 'private test',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'No',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        insert privateAccessCase1;

        VTS_Event__c eventCase1 = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccessCase1.Id,
            Description__c = 'Private Testing Event',
            Event_Type__c = 'Private',
            Start_Date__c = Date.parse('6/30/2021'),
            End_Date__c = Date.parse('6/30/2021')
        );
        insert eventCase1;


        Appointment__c appointmentCase1 = new Appointment__c();
        appointmentCase1.Patient__c = contactRecordCase1.Id;
        appointmentCase1.Status__c = 'Completed';
        appointmentCase1.Event__c = eventCase1.Id;
        appointmentCase1.Lab_Center__c = accountRecord.Id;
        insert appointmentCase1;
    }


    public static void prepareDataForCase2() {

        Account accountRecord = new Account();
        accountRecord.Name = 'Self Registration';
        accountRecord.Event_Process__c = 'Vaccination';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Test_Center').getRecordTypeId();
        INSERT accountRecord;

        Contact contactRecordCase2 = new Contact();
        contactRecordCase2.FirstName = 'Test2';
        contactRecordCase2.LastName = '00037';
        contactRecordCase2.Birthdate = date.today()-10;
        contactRecordCase2.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecordCase2.Email = 'test2@test.com';
        contactRecordCase2.AccountId = accountRecord.Id;
        contactRecordCase2.Consent__c = true;
        INSERT contactRecordCase2;
        
    
        Private_Access__c privateAccessCase2 = new Private_Access__c(
            Name = 'private test',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-2',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'No',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        insert privateAccessCase2;
    
        VTS_Event__c eventCase2 = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccessCase2.Id,
            Description__c = 'Private Testing Event',
            Event_Type__c = 'Private',
            Start_Date__c = Date.parse('6/30/2021'),
            End_Date__c = Date.parse('6/30/2021')
        );
        insert eventCase2;
    
        OKPC_LocationMapController.EventWrapper te = new OKPC_LocationMapController.EventWrapper(eventCase2,true); // to test wrapper
    
        Appointment__c appointmentCase2 = new Appointment__c();
        appointmentCase2.Patient__c = contactRecordCase2.Id;
        appointmentCase2.Status__c = 'Completed';
        appointmentCase2.Event__c = eventCase2.Id;
    
        insert appointmentCase2;    
    }

    @isTest
    public static void testGetAppointmentEventTypeNew_Case1() {

        prepareDataForCase1();

        Contact con = [SELECT Id FROM Contact WHERE LastName='00036' LIMIT 1];
        List<Appointment__c> app = [SELECT Id FROM Appointment__c WHERE Patient__c=:con.Id];

        
        Test.startTest();
        try {
            OKPC_LocationMapController.getAppointmentEventTypeNew(app[0].Id);
        }
        catch(Exception err) {
            System.assert(true,err.getMessage());
        }
        Test.stopTest();
    }
    @isTest
    public static void testGetAppointmentEventTypeNew_Case2() {

        prepareDataForCase2();

        Contact con = [SELECT Id FROM Contact WHERE LastName='00037' LIMIT 1];
        List<Appointment__c> app = [SELECT Id FROM Appointment__c WHERE Patient__c=:con.Id];

        
        Test.startTest();
        try {
            OKPC_LocationMapController.getAppointmentEventTypeNew(app[0].Id);
        }
        catch(Exception err) {
            System.assert(true,err.getMessage());
        }
        Test.stopTest();

    
    }
    

    @IsTest
    static void testFetchPicklist(){
        OKPC_LocationMapController.SelectOptionWrapper obj = new OKPC_LocationMapController.SelectOptionWrapper('test','test');

        Test.startTest();
            try {
                OKPC_LocationMapController.fetchPicklist('Appointment__c', 'First_COVID_19_Test__c');
            }
            catch(Exception er) {
                // TODO;
            }
        Test.stopTest();   
    }

    





}