public class VT_VTS_EventTriggerHandler {
    
    public static void BeforeUpdate(Map<Id, VTS_Event__c> newMap, Map<Id, VTS_Event__c> oldMap) {
        sendEmailNotificationOnUpdate(newMap, oldMap);
        if(VT_TS_SkipTriggerExecution.executeEventChangeLogicFromLayout) {
            preventEventChangeFromLayout(newMap, oldMap);
        }
    }

    public static void sendEmailNotificationOnUpdate(Map<Id, VTS_Event__c> newMap, Map<Id, VTS_Event__c> oldMap) {
        Set<Id> cancelledEventIdSet = new Set<Id>();
        Set<Id> updatedEventIdSet = new Set<Id>();

        for (VTS_Event__c evt : newMap.values()) {
            VTS_Event__c oldEvt = oldMap.get(evt.Id);
            if ((oldEvt.status__c <> evt.Status__c && evt.Status__c == 'Canceled')) {
                cancelledEventIdSet.add(evt.Id);
            } else if((oldEvt.Location__c <> evt.Location__c) || 
                    (oldEvt.Start_Date__c <> evt.Start_Date__c)) {
                updatedEventIdSet.add(evt.Id);
            }
        }

        if(!cancelledEventIdSet.isEmpty() || !updatedEventIdSet.isEmpty()) {
            Database.executeBatch(new VT_TVRS_ProcessEventsAppointmentsBatch(cancelledEventIdSet, updatedEventIdSet, newMap));
        }
    }

    private static void preventEventChangeFromLayout(Map<Id, VTS_Event__c> newMap, Map<Id, VTS_Event__c> oldMap) {
        for(VTS_Event__c event : newMap.values()) {
            VTS_Event__c oldEvent = oldMap.get(event.Id);
            if(event.Location__c <> oldMap.get(event.Id).Location__c || 
                event.Start_Date__c <> oldMap.get(event.Id).Start_Date__c || 
                event.End_Date__c <> oldMap.get(event.Id).End_Date__c) {
                event.addError('You cannot update Location or Date from Record Page. Please update the same from quick actions');
            }
        }
    }
}