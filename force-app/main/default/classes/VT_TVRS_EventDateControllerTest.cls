@IsTest
private class VT_TVRS_EventDateControllerTest {
	@testSetup
    private static void makeData() {
        // Account, Contact, Event, Slot, Appointment
        Account acc = new Account();
        acc.Name = 'Testing Account 1';
        acc.RecordTypeId = Account.SObjectType.getDescribe().RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        acc.Event_Process__c = 'Testing Site';
        acc.Vermont_County__c = 'Addison';
        acc.Business_Start_Time__c = Time.newInstance(00, 00, 00, 00);
        acc.Business_End_Time__c = Time.newInstance(11, 00, 00, 00);
        acc.Event_Type__c = 'State-Sponsored';
        insert acc;
        
        Account acc1 = new Account();
        acc1.Name = 'Testing Account 2';
        acc1.RecordTypeId = Account.SObjectType.getDescribe().RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        acc1.Event_Process__c = 'Testing Site';
        acc1.Vermont_County__c = 'Addison';
        acc1.Business_Start_Time__c = Time.newInstance(00, 00, 00, 00);
        acc1.Business_End_Time__c = Time.newInstance(11, 00, 00, 00);
        acc1.Event_Type__c = 'State-Sponsored';
        insert acc1;
        
        Appointment_Slot__c slot = new Appointment_Slot__c();
        slot.Account__c = acc.Id;
        slot.Date__c = Date.today();
        slot.Start_Time__c = Time.newInstance(01, 00, 00, 00);
        slot.End_Time__c = Time.newInstance(02, 00, 00, 00);
        insert slot;
        
        VTS_Event__c event = new VTS_Event__c();
        event.Account__c = acc.Id;
        event.Start_Date__c = Date.today();
        event.End_Date__c = Date.today();
        event.Start_Time__c = Time.newInstance(00, 00, 00, 00);
        event.End_Time__c = Time.newInstance(11, 00, 00, 00);
        event.Event_Type__c = 'Public';
        event.Status__c = 'Open';
        event.Location__c = acc.Id;
        insert event;
        
        Contact c = new Contact();
        c.FirstName = 'First Name';
        c.LastName = 'Last Name';
        c.Email = 'first@yopmail.com';
        c.RecordTypeId = Contact.SObjectType.getDescribe().RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        c.Birthdate = Date.newInstance(1995, 07, 23);
        insert c;
        
        Appointment__c app = new Appointment__c();
        app.Patient__c = c.Id;
        app.Appointment_Slot__c = slot.Id;
        app.Lab_Center__c = acc.Id;
        insert app;
        
        
        String recordTypeID = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        String citizenCovidId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Event_Process__c = 'Testing Site';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;
        
        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-44151',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            what_was_the_last_date_of_contact__c = Date.newInstance(2020, 2, 20),
            Did_you_have_a_nasal_swab__c = 'Yes	',
            What_was_your_test_result__c = 'Negative',
            Approximately_what_date_were_you_tested__c = Date.newInstance(2020, 3, 3),
            Had_contact_with_anyone_diagon__c = 'Yes',
            Do_you_take_prescription_medications__c = '	Yes',
            medication_for_fever_in_the_past_3_days__c = 'Yes',
            First_Date_you_have_been_feeling_sick__c = Date.newInstance(2020, 3, 1) ,
            First_date_you_began_feeling_well__c= Date.newInstance(2020, 3, 10),
            Fever_Cough_or_Difficulty_breathing__c = 'Yes',
            Sick_since_Jan_1_2020__c = 'Yes',
            Opt_out_for_SMS_Updates__c = true,
            HasOptedOutOfEmail = false,
            Current_Step__c = 1,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con;
        
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType IN : customerUserTypes  limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.comtestException',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            //userRoleId = portalRole.Id,
            contactId = con.id
        );
        insert newUser;
    }
    
    @IsTest
    private static void getAllEventDetails() {
        VT_TVRS_EventDateController.EventWrapper ew = new VT_TVRS_EventDateController.EventWrapper();
        ew.locationId = [SELECT Id FROM Account LIMIT 1]?.Id;
        ew.rcdId = [SELECT Id FROM VTS_Event__c LIMIT 1]?.Id;
        ew.status = 'Open';
        ew.eventStartDate = String.valueOf(Date.today());
        VT_TVRS_EventDateController.getAllEventDetail(JSON.serialize(ew), false, '2021-05-01');
        
        User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{
                Boolean result = VT_TVRS_EventDateController.getAllEventDetail(JSON.serialize(ew), false, '2021-05-01');
                system.assertEquals(true, result, 'getAllEventDetails');
            } catch(Exception e){
                
            }
        }
    }
    
    @IsTest
    private static void updateEventRecordTest() {
        String eventId = [SELECT Id FROM VTS_Event__c LIMIT 1]?.Id;
        VT_TVRS_EventDateController.updateEventRecord(eventId, String.valueOf(date.today()), date.today().addDays(-2));
        
        User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{
                VT_TVRS_EventDateController.updateEventRecord(eventId, String.valueOf(date.today()), date.today().addDays(-2));
                system.assertEquals(true, String.isNotBlank(eventId), 'updateEventRecordTest');
            }catch(Exception e){
                
            }
        }
    }
    
    @IsTest
    private static void getEventDetailTest() {
        String eventId = [SELECT Id FROM VTS_Event__c LIMIT 1]?.Id;
        VT_TVRS_EventDateController.getEventDetail(eventId);
        
        User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{
                Map<String, Object> result = VT_TVRS_EventDateController.getEventDetail(eventId);
                system.assertEquals(true, Boolean.valueOf(result.get('success')), 'getEventDetailTest');
            } catch(Exception e){
                
            }
        }
    }
}