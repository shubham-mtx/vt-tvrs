/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 12-27-2021
 * @last modified by  : Vamsi Mudaliar
**/
public without sharing class VT_TS_GetContacts {
    
    @AuraEnabled
    public static List<Contact> getContactList(String firstInitial, String lastInitial, Date dob, String patientId, Boolean needParentContactOnly, String alreadySelectedContact){
       
        // try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                Date birthDate;
                List<Contact> contactDetails = new List<Contact>();
                
                String firstInitialTemp = firstInitial+'%';
                String lastInitialTemp = lastInitial+'%';
                //Date myDate = dob.date();
                String devName = 'Citizen_COVID';
                
                if(dob!=null){
                    birthDate = date.newinstance(dob.year(), dob.month(), dob.day());
                }

                String parentContactFilter = '';
                if(needParentContactOnly == true) {
                    parentContactFilter = ' AND Parent_Contact__c = null ';
                }

                if(String.isNotBlank(alreadySelectedContact)) {
                    alreadySelectedContact = String.escapeSingleQuotes(alreadySelectedContact);
                    parentContactFilter += ' AND Id != :alreadySelectedContact ';
                }

                String query = 'SELECT Id, LastName, FirstName, Email, (SELECT Id, Name FROM Contacts__r LIMIT 1), Result_Recieve_Consent__c, Birthdate, Parent_Contact__r.Name, MailingAddress, Combined_Address__c, Patient_Id__c FROM Contact WHERE RecordType.DeveloperName =: devName '+parentContactFilter+' AND ( ';

                if( String.isNotBlank(firstInitial) && String.isNotBlank(lastInitial) && birthDate != null ){
                    firstInitial = String.escapeSingleQuotes(firstInitial);
                    lastInitial = String.escapeSingleQuotes(lastInitial);
                    query += ' ( FirstName LIKE :firstInitialTemp AND LastName LIKE :lastInitialTemp AND Birthdate =: birthDate ) ';
                    
                    if(String.isNotBlank(patientId)){
                        patientId = String.escapeSingleQuotes(patientId);
                        query += ' OR Patient_Id__c =:patientId ';
                    }
                }
                else if( String.isNotBlank(patientId) ){
                    patientId = String.escapeSingleQuotes(patientId);
                    query += ' Patient_Id__c =:patientId ';
                }

                query += ' ) WITH SECURITY_ENFORCED LIMIT 200';

                return Database.query(query);
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }
        // catch(Exception ex){
        //     throw new AuraHandledException(ex.getMessage());
        // }
        
       
        
          
}