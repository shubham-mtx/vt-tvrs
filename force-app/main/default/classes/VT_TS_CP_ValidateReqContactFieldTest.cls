/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-10-2022
 * @last modified by  : Vamsi Mudaliar
**/
@isTest
public class VT_TS_CP_ValidateReqContactFieldTest {
    @testSetup
    public static void testData() {


        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where Name = 'Testing Site Community Profile' limit 1]; 

        Account accountRecord = new Account();
        accountRecord.Name = 'Self Registration';
        accountRecord.Event_Process__c = 'Vaccination';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Test_Center').getRecordTypeId();
        INSERT accountRecord;

        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'Test';
        contactRecord.LastName = '00001';
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord.Email = 'test@test.com';
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Consent__c = true;
        INSERT contactRecord;
        
        Contact contactRecord2 = new Contact();
        contactRecord2.FirstName = 'Test';
        contactRecord2.LastName = '00002';
        contactRecord2.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord2.Email = 'test2@test.com';
        contactRecord2.AccountId = accountRecord.Id;
        contactRecord2.Consent__c = true;
        INSERT contactRecord2;
        
        Contact contactRecord3 = new Contact();
        contactRecord3.FirstName = 'Test3';
        contactRecord3.LastName = '00003';
        contactRecord3.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord3.Email = 'test3@test.com';
        contactRecord3.AccountId = accountRecord.Id;
        contactRecord3.Consent__c = true;
        INSERT contactRecord3;
        

        User newUser = new User(
            profileId = prfile.id,
            username = 'testingUser@test103.com',
            email = 'pMyselfb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactRecord.Id
        );
        insert newUser;


        User newUser3 = new User(
            profileId = prfile.id,
            username = 'testingUser@test105.com',
            email = 'pMyselfb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactRecord3.Id
        );
        insert newUser3;



        Private_Access__c privateAccess = new Private_Access__c(
        	Name = 'private test',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Is_Active__c = false
        );
        insert privateAccess;
        Private_Access__c privateAccess2 = new Private_Access__c(
        	Name = 'private test2',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Is_Active__c = true,
            Requires_Followup_Dose__c = 'Yes',
            Max__c = 2,
            Min__c = 1
        );
        insert privateAccess2;
        
        VTS_Event__c event = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccess2.Id
        );
        insert event;
        
        
        Appointment__c appointment = new Appointment__c(
        	Patient__c = contactRecord.Id,
            Status__c = 'Scheduled',
            Event__c = event.Id
        );
        insert appointment;
        // Appointment__c appointment2 = new Appointment__c(
        // 	Patient__c = contactRecord.Id,
        //     Status__c = 'Scheduled',
        //     Event__c = event.Id
        // );
        // insert appointment2;
        
        Appointment__c appointment1con2 = new Appointment__c(
        	Patient__c = contactRecord2.Id,
            Status__c = 'Completed',
            Event__c = event.Id
        );
        insert appointment1con2;
        // Appointment__c appointment2con2 = new Appointment__c(
        // 	Patient__c = contactRecord2.Id,
        //     Status__c = 'Completed',
        //     Event__c = event.Id
        // );
        // insert appointment2con2;
        
        Appointment__c appointment1con3 = new Appointment__c(
        	Patient__c = contactRecord2.Id,
            Status__c = 'Cancelled',
            Event__c = event.Id
        );
         insert appointment1con3;
        // Appointment__c appointment2con3 = new Appointment__c(
        // 	Patient__c = contactRecord2.Id,
        //     Status__c = 'Cancelled',
        //     Event__c = event.Id
        // );
        //insert appointment2con3;

    }

    @isTest
    public static void testValidateFields(){
        Contact contactRecord = [SELECT Id FROM Contact WHERE LastName = '00001' LIMIT 1];
        Contact contactRecord2 = [SELECT Id FROM Contact WHERE LastName = '00002' LIMIT 1];
        Contact contactRecord3 = [SELECT Id FROM Contact WHERE LastName = '00003' LIMIT 1];
        //User usr = [SELECT Id,Username FROM User WHERE Username ='testingUser@test103.com' LIMIT 1];
        
        User usr = [SELECT Id,Username FROM User WHERE Username ='testingUser@test105.com' LIMIT 1];
 
        Private_Access__c privateAccess = [SELECT Id, Name,Age_Based__c,Event_Process__c FROM Private_Access__c WHERE Name ='private test'];
        Id privateAccessId = [SELECT Id, Name FROM Private_Access__c WHERE Name ='private test'].Id;
        Id privateAccessId2 = [SELECT Id, Name FROM Private_Access__c WHERE Name ='private test2'].Id;
        //VTS_Event__c event = [SELECT Id FROM VTS_Event__c WHERE VTS_Event__c.Private_Access__c =:privateAccessId];
        String validateString = [SELECT Field_API_Name__c FROM VT_TS_Contact_Required_Field__mdt LIMIT 1].Field_API_Name__c;
        Test.startTest();
            System.runAs(usr) {
               // try{
               // Map<String,Object> result = VT_TS_CP_ValidateRequiredContactField.validateFields(contactRecord.Id,privateAccessId, 40, true);
               // Map<String,Object> result4 = VT_TS_CP_ValidateRequiredContactField.validateFields(contactRecord.Id,privateAccessId, 60, true);
               // Map<String,Object> result2 = VT_TS_CP_ValidateRequiredContactField.validateFields(contactRecord2.Id,privateAccessId2, 40, true);
                 //Map<String,Object> result3 = VT_TS_CP_ValidateRequiredContactField.validateFields(contactRecord2.Id,privateAccessId2, 60, true);
                 Map<String,Object> result5 = VT_TS_CP_ValidateRequiredContactField.validateFields(contactRecord3.Id,privateAccessId2, 40, true);
                // Map<String,Object> result6 = VT_TS_CP_ValidateRequiredContactField.validateFields(contactRecord3.Id,privateAccessId2, 60, true);
                system.assertEquals(true, true, 'testValidateFields');
            // }
            // catch(Exception e) {
            //         System.assert(true,'ERROR');
            // }
            }
        Test.stopTest();
    }

}