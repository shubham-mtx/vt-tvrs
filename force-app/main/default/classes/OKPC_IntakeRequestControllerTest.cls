@isTest(seeAllData=false)
public class OKPC_IntakeRequestControllerTest {
    
    
    private static final String LabSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
    private static final String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
    
    
    static testMethod void testMethod1(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
            Account acc1 = new Account (
                Name = 'newAcc1'
            );  
        insert acc1;
        Contact conCase = new Contact (
            AccountId = acc1.id,
            LastName = 'portalTestUserv1',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            MobilePhone = '123123123'
        );
        insert conCase;
      
        
        
        //Create user
        
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
        
        User newUser1 = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = conCase.id
        );
        insert newUser1;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, true);
        Account createAccountLabSite = TestDataFactoryAccount.createAccount('LabSite', LabSite, true);
        Contact createContact = TestDataFactoryContact.createContact(true , 'TestLastName','TestFirstName');
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = conCase.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        Appointment__c app = new Appointment__c();
        app.Patient__c = createContact.Id;
        app.Status__c = '';
        insert app;

        OKPC_IntakeRequestController.DataWrapper dataWrapperObj = (OKPC_IntakeRequestController.DataWrapper)OKPC_IntakeRequestController.getAppointmentData(createAppointment.Id);
        OKPC_IntakeRequestController.getAppointmentDataForContact(createContact.Id,'');
        OKPC_IntakeRequestController.getAppointmentDataForContact(null,'');
        
        OKPC_IntakeRequestController.saveAppointMentData('{"appointmentId":"'+createAppointment.Id+'","Status__c":"Eligible","appointmentStatus":"Scheduled"}',createAppointment.Id);
        system.runAs(newUser1){
            
            OKPC_IntakeRequestController.getAppointmentData(createAppointment.Id);
            OKPC_IntakeRequestController.getAppointmentDataForContact(createContact.Id,'');
            OKPC_IntakeRequestController.getAppointmentDataForContact(null,'');
            
            OKPC_IntakeRequestController.saveAppointMentData('{"appointmentId":"'+createAppointment.Id+'","Status__c":"Eligible","appointmentStatus":"Scheduled"}',createAppointment.Id);
            
        }
        
        system.assertEquals(dataWrapperObj.appointmentId!=null, true,'success');
    }
}