/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 01-03-2022
 * @last modified by  : Mohit Karani
**/
public with sharing class TVRS_Wrapper {
    
    public class Testing {
        @AuraEnabled public string testingId { get; set; }
        @AuraEnabled public string labName { get; set; }
        @AuraEnabled public string result { get; set; }
        @AuraEnabled public string specimen { get; set; }
        @AuraEnabled public datetime createddate { get; set; }
        @AuraEnabled public boolean isAfter24Hours { get; set; }
        @AuraEnabled public boolean isPositiveResult { get; set; }
    }
    public class SelectOptionWrapper {
        @AuraEnabled public string value;
        @AuraEnabled public string label;
        public SelectOptionWrapper(string value, string label) {
            this.value = value;
            this.label = label;
        }
    }

    public class MapMarker {
        @AuraEnabled public string title;
        @AuraEnabled public string description;
        @AuraEnabled public Location location;
        @AuraEnabled public String value;
        @AuraEnabled public Time businessStartTime;
        @AuraEnabled public Time businessEndTime;
        @AuraEnabled public String othersDescription;
        @AuraEnabled public String otherSiteLink;
        public MapMarker(Account acc) {
            Datetime timeValue;
            String startTime;
            String endTime;
            if(acc.Business_Start_Time__c != null){
                timeValue = Datetime.newInstance(Date.today(), acc.Business_Start_Time__c);
                startTime = timeValue.format('MM/dd/yyyy hh:mm a','America/New_York').substringAfter(' ');
            }
            if(acc.Business_End_Time__c != null){
                timeValue = Datetime.newInstance(Date.today(), acc.Business_End_Time__c);
                endTime = timeValue.format('MM/dd/yyyy hh:mm a','America/New_York').substringAfter(' ');
            }
            this.value = acc.Id;
            this.title = acc.Name;
            this.description = (acc.Name != null ? ' <br/> <b>Location Name:</b> ' + acc.Name : '') +
                    (acc.BillingStreet != null ? ' <br/> <b>Address:</b> ' + acc.BillingStreet : '') +
                    (acc.Phone != null ? ' <br/> <b>Phone:</b> ' + acc.Phone : '') +
                    (acc.Business_Start_Time__c != null ? ' <br/> <b>Business Hours:</b> ' + startTime + ' - ' + endTime : '');
            this.location = new Location(acc);
            this.businessStartTime = acc.Business_Start_Time__c;
            this.businessEndTime = acc.Business_End_Time__c;
            this.othersDescription = acc.Event_Type__c == 'Other' ? acc.Description : '';
            this.otherSiteLink = acc.Event_Type__c == 'Other' ? acc.Site_URL__c : '';
        }
    }

    public class Location {
        @AuraEnabled public string Street;
        @AuraEnabled public string City;
        @AuraEnabled public string State;
        @AuraEnabled public string Country;
        @AuraEnabled public string PostalCode;
        @AuraEnabled public Decimal Latitude;
        @AuraEnabled public Decimal Longitude;

        public Location(Account acc) {
            this.Street = acc.BillingStreet;
            this.City = acc.BillingCity;
            this.State = acc.BillingState;
            this.Country = acc.BillingCountry;
            this.PostalCode = acc.BillingPostalCode;
            this.Latitude = acc.BillingLatitude;
            this.Longitude = acc.BillingLongitude;
        }
    }

    public class DataWrapper {
        @AuraEnabled public String contactId;
        @AuraEnabled public String appointmentId;
        @AuraEnabled public AppointmentWrapper appointmentObj;

        public DataWrapper() {
            this.contactId = '';
            this.appointmentId = '';
        }
    }

    public class AppointmentWrapper{
        @AuraEnabled public String appointmentId;
        @AuraEnabled public Date appointmentDate;
        @AuraEnabled public String startTime;
        @AuraEnabled public String endTime;
        @AuraEnabled public String siteName;
        @AuraEnabled public String status;
        @AuraEnabled public Date preferredDate;
        @AuraEnabled public Time preferredTime;
        @AuraEnabled public String preferredSite;
        @AuraEnabled public String preferredSiteId;
		@AuraEnabled public String symptomatic;
        public AppointmentWrapper(Appointment__c appObj) {
            this.appointmentId = appObj.Id;
            this.appointmentDate = appObj.Appointment_Date__c;
            this.startTime = appObj.Start_Time__c;
            this.endTime = appObj.End_Time__c;
            this.siteName = appObj.Patient__r.Testing_Site__r.Name;
            this.status = appObj.Status__c;
            this.preferredDate = appObj.Patient__r.Preferred_Date__c;
            this.preferredTime = appObj.Patient__r.Preferred_Time__c;
            this.preferredSite = appObj.Patient__r.Testing_Site__r.Name;
            this.preferredSite = appObj.Patient__r.Testing_Site__c;
            this.symptomatic = appObj.Symptomatic__c;
        }
    }
    public class FilterWrapper{
        public List<String> listStatus = new List<String>();
        public List<String> type = new List<String>();
        public String days; 
        public String sortValue; 
    }
    public class AccountContactWrapper {

        @AuraEnabled public String id;
        @AuraEnabled public String eventProcess;
        @AuraEnabled public String typeOfContact;
        @AuraEnabled public String numOfWeeksAvailable;
        @AuraEnabled public String appointmentFrequency;
        @AuraEnabled public String name;
        @AuraEnabled public Time businessEndTime;
        @AuraEnabled public Time businessStartTime;
        @AuraEnabled public String billingStreet;
        @AuraEnabled public String billingCity;
        @AuraEnabled public String billingState;
        @AuraEnabled public String billingPostalCode;
        
        public AccountContactWrapper(Account accObj,AccountContactRelation accConObj) {
            Boolean flag = (accObj!=null);
            this.id = (flag)?accObj.Id:accConObj.AccountId;
            this.eventProcess = (flag)?accObj.Event_Process__c:accConObj.Account.Event_Process__c;

            this.typeOfContact = (flag)?accObj.Type_of_Contact__c:accConObj.Account.Type_of_Contact__c;
            this.numOfWeeksAvailable = (flag)?accObj.Number_of_Weeks_Available__c:accConObj.Account.Number_of_Weeks_Available__c;
            this.appointmentFrequency = (flag)?accObj.Appointment_Frequency__c:accConObj.Account.Appointment_Frequency__c;
            this.name = (flag)?accObj.Name:accConObj.Account.Name;
            this.businessEndTime = (flag)?accObj.Business_End_Time__c:accConObj.Account.Business_End_Time__c;
            this.businessStartTime = (flag)?accObj.Business_Start_Time__c:accConObj.Account.Business_Start_Time__c;
            this.billingStreet = (flag)?accObj.BillingStreet:accConObj.Account.BillingStreet;
            this.billingCity = (flag)?accObj.BillingCity:accConObj.Account.BillingCity;
            this.billingState = (flag)?accObj.BillingState:accConObj.Account.BillingState;
            this.billingPostalCode = (flag)?accObj.BillingPostalCode:accConObj.Account.BillingPostalCode; 
        }

    }


    
}