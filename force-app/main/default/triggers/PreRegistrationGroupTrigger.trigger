/**
 * @description       : Trigger on Pre_Registration_Group__c object.
 * @author            : Garima Saxena
 * @group             : 
 * @last modified on  : 02-16-2021
 * @last modified by  : Wasef Mohiuddin
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   22-01-2021   Garima Saxena   Initial Version
**/

trigger PreRegistrationGroupTrigger on Pre_Registration_Group__c (after update) {
    
    //After Update
    if (Trigger.isAfter && Trigger.isUpdate) {
        if(VT_TS_SkipTriggerExecution.executePreRegistrationGroupTrigger){
            PreRegistrationGroupTriggerHandler.afterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
        }  
    }

}