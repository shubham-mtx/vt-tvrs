import { LightningElement, api, track } from 'lwc';
import { fireEvent, sharedData } from "c/pubsub";

export default class OkContactIntakeFlow extends LightningElement {
    @track onLoadStep;
    @track isNewEntity;
    @track activeCss = 'event active';
    @track completedCss = 'event completed';
    @track step1CSS;
    @track step2CSS;
    @track step3CSS;
    @track step4CSS;
    @track step5CSS;
    @track step6CSS;
    @track step7CSS;
    @track step8CSS;
    @track step9CSS;

    @api originalStep = 0;
    @api isLicensedUser = false;
    @api isProvider = false;
    @api hideStep = false;
    @api areInitialStepsOver = false;
    @api isVaccineEvent;

    step1AriaLabel;
    step2AriaLabel;
    step3AriaLabel;
    step4AriaLabel;
    step5AriaLabel;

    step1TabIndex;
    step2TabIndex;
    step3TabIndex;
    step4TabIndex;
    step5TabIndex;

    connectedCallback() {
        this.onLoadStep = this.currentStep;
    }

    @api
    get currentStep() {
        return this.currentStep2;
    }
    set currentStep(value) {
        this.currentStep2 = value;
        var _currentStep = parseInt(value);
        this.step1CSS = 'event ' + (_currentStep == 1 ? 'active ' : '') + (_currentStep > 1 || this.originalStep >= 1 ? ' completed' : '');
        this.step2CSS = 'event ' + (_currentStep == 2 ? 'active ' : '') + (_currentStep > 2 || this.originalStep >= 2 ? ' completed' : '');
        this.step3CSS = 'event ' + (_currentStep == 3 ? 'active ' : '') + (_currentStep > 3 || this.originalStep >= 3 ? ' completed' : '');
        this.step4CSS = 'event ' + (_currentStep == 4 ? 'active ' : '') + (_currentStep > 4 || this.originalStep >= 4 ? ' completed' : '');
        this.step5CSS = 'event ' + (_currentStep == 5 ? 'active ' : '') + (_currentStep > 5 || this.originalStep >= 5 ? ' completed' : '');
        this.step6CSS = 'event ' + (_currentStep == 6 ? 'active ' : '') + (_currentStep > 6 || this.originalStep >= 6 ? ' completed' : '');
        this.step7CSS = 'event ' + (_currentStep == 7 ? 'active ' : '') + (_currentStep > 7 || this.originalStep >= 7 ? ' completed' : '');
        this.step8CSS = 'event ' + (_currentStep == 8 ? 'active ' : '') + (_currentStep > 8 || this.originalStep >= 8 ? ' completed' : '');
        this.step9CSS = 'event ' + (_currentStep == 9 ? 'active ' : '') + (_currentStep > 9 || this.originalStep >= 9 ? ' completed' : '');

        this.step1AriaLabel = 'Consent ' + (_currentStep > 1 ? 'Completed' : '');
        this.step2AriaLabel = 'Attendee Personal Information ' + (_currentStep > 2 ? 'Completed' : '');
        this.step3AriaLabel = 'Testing Questions ' + (_currentStep > 3 ? 'Completed' : '');
        this.step4AriaLabel = 'Vaccine Questions ' + (_currentStep > 4 ? 'Completed' : '');
        this.step5AriaLabel = 'Waiver ' + (_currentStep > 5 ? 'Completed' : '');

        this.step1TabIndex = _currentStep == 1 ? '0' : '-1';
        this.step2TabIndex = _currentStep == 2 ? '0' : '-1';
        this.step3TabIndex = _currentStep == 3 ? '0' : '-1';
        this.step4TabIndex = _currentStep == 4 ? '0' : '-1';
        this.step5TabIndex = _currentStep == 5 ? '0' : '-1';
    }

    @api
    handleCurrentStep(value) {
        this.currentStep2 = value;
        var _currentStep = parseInt(value);
        this.step1CSS = 'event ' + (_currentStep == 1 ? 'active ' : '') + (_currentStep > 1 || this.originalStep >= 1 ? ' completed' : '');
        this.step2CSS = 'event ' + (_currentStep == 2 ? 'active ' : '') + (_currentStep > 2 || this.originalStep >= 2 ? ' completed' : '');
        this.step3CSS = 'event ' + (_currentStep == 3 ? 'active ' : '') + (_currentStep > 3 || this.originalStep >= 3 ? ' completed' : '');
        this.step4CSS = 'event ' + (_currentStep == 4 ? 'active ' : '') + (_currentStep > 4 || this.originalStep >= 4 ? ' completed' : '');
        this.step5CSS = 'event ' + (_currentStep == 5 ? 'active ' : '') + (_currentStep > 5 || this.originalStep >= 5 ? ' completed' : '');
        this.step6CSS = 'event ' + (_currentStep == 6 ? 'active ' : '') + (_currentStep > 6 || this.originalStep >= 6 ? ' completed' : '');
        this.step7CSS = 'event ' + (_currentStep == 7 ? 'active ' : '') + (_currentStep > 7 || this.originalStep >= 7 ? ' completed' : '');
        this.step8CSS = 'event ' + (_currentStep == 8 ? 'active ' : '') + (_currentStep > 8 || this.originalStep >= 8 ? ' completed' : '');
        this.step9CSS = 'event ' + (_currentStep == 9 ? 'active ' : '') + (_currentStep > 9 || this.originalStep >= 9 ? ' completed' : '');
    }

    @api
    handlePaidValue(value) {
        this.isNewEntity = this.isNewEntity || value;
    }

    //Commented By Virendra Kumar as per S-16828
    handleUserClicks(event) {
        console.log(event.target.value + " " + this.originalStep);
        let _currentStep = event.target.value;
        if (_currentStep <= this.originalStep) {
            fireEvent(this.pageRef, 'handleSideBarClick', _currentStep);
        }
    }
}