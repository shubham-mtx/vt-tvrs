/**
 * @description       : 
 * @author            : Garima Saxena
 * @group             : 
 * @last modified on  : 02-16-2021
 * @last modified by  : Wasef Mohiuddin
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   02-15-2021   Wasef Mohiuddin   Initial Version
**/
global with sharing class VT_TS_VaccinationActivationEmailBatch implements Database.Batchable<sobject>{

    Set<Id> idSet = new Set<Id>();

    public VT_TS_VaccinationActivationEmailBatch(Set<Id> prGroupIdSet){
        idSet = prGroupIdSet;
    }

    //Start method - Retrieve Pre-Registration records to send email to related Contact
    global Database.Querylocator start(Database.BatchableContext bc){
        String soqlQuery = 'SELECT Id, Name, Pre_Registration_Group__c, Early_Group1_Health__c, Early_Group_2_Access_Group__c, Early_Group_2_Access_Group__r.Status__c, Early_Group1_Health__r.Status__c, Chronic_Condition__c, Contact__c, Contact__r.Email, Contact__r.Email_Flow__c, Contact__r.Email_Workflow_Datetime__c, Is_Email_Sent__c, ';
        soqlQuery += 'Pre_Registration_Group__r.Status__c, Pre_Registration_Group__r.Email_Acknowledgement__c, Early_Group1_Health__r.Email_Acknowledgement__c, Early_Group_2_Access_Group__r.Email_Acknowledgement__c ';
        soqlQuery += 'FROM Pre_Registration__c WHERE Is_Email_Sent__c = false AND (Status__c = \'Assigned\' OR Status__c = \'Pending Verification\') ';
        soqlQuery += 'AND (Pre_Registration_Group__c IN :idSet OR Early_Group1_Health__c IN: idSet OR Early_Group_2_Access_Group__c	IN: idSet)';
        return Database.getQueryLocator(soqlQuery);
    }
     
    global void execute(Database.BatchableContext bc, List<Pre_Registration__c> scope){

        List<Contact> patientList = new List<Contact>();
        List<Pre_Registration__c> regList = new List<Pre_Registration__c>();
        Set<String> setOfContactId = new Set<String>();
        Set<String> setOfContactIdHaveAppointments = new Set<String>();
        List<Pre_Registration_Group__c> updatePreRegGroupList = new List<Pre_Registration_Group__c>();
        
        //Get All Contact Ids to fetch There Scheduled or Completed Vaccination Appointments
        for(Pre_Registration__c reg: scope){
            setOfContactId.add(reg.Contact__c);
        }

        //Fetch Contact's Scheduled or Completed Vaccination Appointment
        for(Appointment__c appointmentRecord : [SELECT Id, Status__c, Patient__c FROM Appointment__c WHERE  (Status__c = 'Scheduled' OR STatus__c = 'Completed') AND Patient__c IN: setOfContactId AND Event_Process__c = 'Vaccination']){
            setOfContactIdHaveAppointments.add(appointmentRecord.Patient__c);
        }

        if(scope != null && !scope.isEmpty()){
            Map<Id, Integer> preRegGrpToEmailCountMap = new Map<Id, Integer>();
            for(Pre_Registration__c reg: scope){

                if(

                    (String.isNotBlank(reg.Pre_Registration_Group__c) && reg.Pre_Registration_Group__r.Status__c == 'Activate' && reg.Pre_Registration_Group__r.Email_Acknowledgement__c) || 
                    (String.isNotBlank(reg.Early_Group1_Health__c) && reg.Early_Group1_Health__r.Status__c == 'Activate' && reg.Early_Group1_Health__r.Email_Acknowledgement__c) || 
                    (String.isNotBlank(reg.Early_Group_2_Access_Group__c) && reg.Early_Group_2_Access_Group__r.Status__c == 'Activate' && reg.Early_Group_2_Access_Group__r.Email_Acknowledgement__c)
                ){
                    Boolean isEmailSent = true;
                    if(reg.Contact__r.Email != null && !setOfContactIdHaveAppointments.contains(reg.Contact__c)){
    
                        if(reg.Early_Group1_Health__c != null){
                            if(reg.Early_Group1_Health__r.Status__c == 'Activate' && reg.Chronic_Condition__c == 'Rejected'){
                                isEmailSent = false;
                            }
                        }
    
                        if(isEmailSent){
                            //Update contact fields to trigger the workflow email alert
                            reg.Contact__r.Email_Workflow_Datetime__c = Datetime.now();
                            reg.Contact__r.Email_Flow__c = 'Pre-Reg Announcement';
    
                            //Update Is Email Sent to true and Total Number of Email Sent when Emails are sent successfully
                            reg.Is_Email_Sent__c = true;
    
                            patientList.add(reg.Contact__r);
                            regList.add(reg);
                        }
                    }
                }
                
            }

            if(!patientList.isEmpty()){
                VT_TS_SkipTriggerExecution.executeContactTrigger = false;
                update patientList; 
            }
            if(!regList.isEmpty()){
                update regList;
            }
            
        } 
    }
     
    global void finish(Database.BatchableContext bc){ 
    }

}