import {
    LightningElement,
    api,
    track
} from 'lwc';
import getSiteAccounts from '@salesforce/apex/DC_ScheduleAppointment.getSiteAccounts';
// import getAccountSlots from '@salesforce/apex/DC_ScheduleAppointment.getAccountSlots';
// import updateAppointmentrec from '@salesforce/apex/DC_ScheduleAppointment.updateAppointment';
// import getSelectedTestingSiterec from '@salesforce/apex/DC_ScheduleAppointment.getSelectedTestingSite';
// import createAppointmentrec from '@salesforce/apex/DC_ScheduleAppointment.createAppointment';
// import getAppointmentDetailrec from '@salesforce/apex/DC_ScheduleAppointment.getAppointmentDetail';
import getPreferredValues from '@salesforce/apex/OkpcContactInformationController.getPreferredValues';
import setPreferredValues from '@salesforce/apex/OkpcContactInformationController.setPreferredValues';
// import getAccountdetails from '@salesforce/apex/OkpcContactInformationController.getAccountdetails';
import getAccountdetails from '@salesforce/apex/OKPC_LocationMapController.getAccountdetails';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent'

export default class OkpcPreferenceComponent extends LightningElement {
    // map = new map();
    @api recordid;
    @track options = [];
    @track selectedOption;
    @track isAttributeRequired = false;
    @api fieldName;
    @track contactid;
    @api objectName;
    @track fieldLabelName;
    @track accountList = [];
    @track isSiteSelected = false;
    @track selectValue = null;
    @track selectSiteName;
    @track slotId;
    @track showSchedule = false;
    @track showMapModal = false;
    @track preferredDate = null;
    @track preferredTime = null;
    @track testingSite;
    @track showtestingSite = false;
    @track accountDetails= {};
    @track showLocationDetails = false;
    @track showSpinner = false;
    @track showSingleMap = false;
    @track mapMarkers = [];
    @track location={};
    @track todayDate;
    @track preferredTimeMin = null;
    @track preferredTimeMax = null;
    map = new Map();
    connectedCallback() {
        this.todayDate = this.getTodayDate();
        this.retrieveTestingSites();
        this.getValuesPreferred();
    }

    getValuesPreferred() {
        this.showSpinner = true;
        getPreferredValues()
            .then(result => {
                console.log('preferred values: ', JSON.stringify(result));
                this.preferredDate = result.Preferred_Date__c;
                if (result.Preferred_Time__c) {
                    this.preferredTime = this.msToTime(result.Preferred_Time__c);
                }
                if (result.Testing_Site__c) {
                    this.selectValue = result.Testing_Site__c;
                    this.getAccount(this.selectValue);
                    this.showtestingSite = true;
                    this.showLocationDetails = true;
                   
                }
                /*if(result.Testing_Site__r.Business_Start_Time__c){
                    this.preferredTimeMin = this.msToTime(result.Testing_Site__r.Business_Start_Time__c);
                }
                else{
                    this.preferredTimeMin = '07:00:00.000Z';
                }
                if(result.Testing_Site__r.Business_End_Time__c){
                    this.preferredTimeMax = this.msToTime(result.Testing_Site__r.Business_End_Time__c);
                }
                else{
                    this.preferredTimeMax = '19:00:00.000Z';
                }*/
                this.showSpinner = false;

            })
            .catch(error => {
                console.log('error in data ', JSON.stringify(error));
                this.showSpinner = false;
            })
    }

    @api
    setValues() {
        setPreferredValues({
                preferredDate: this.preferredDate,
                preferredTime: this.preferredTime,
                preferredSite: this.selectValue
            })
            .then(result => {
                console.log('saved success');
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Saved successfully',
                }));
                this.handleCloseModalonSave();
            })
            .catch(error => {
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Error',
                }));
                console.log('error ', JSON.stringify(error));
            })
    }
    handleInputChange(event) {
        if (event.target.name == 'preferredDate') {
            this.preferredDate = event.target.value;
        } else if (event.target.name == 'preferredTime') {
            this.preferredTime = event.target.value;
            console.log('tiem=> ', event.target.value);
        }
    }

    retrieveTestingSites() {
        getSiteAccounts({})
            .then(data => {
                data.forEach((account) => {
                    var optionValue = new Object();
                    optionValue.label = account.Name;
                    optionValue.value = account.Id;
                    this.accountList.push(optionValue);
                    this.map.set(account.Id, account.Name);
                });
                this.options = this.accountList;
            })
            .catch(error => {
                console.log('Error-->', error);
            });
    }

    selectionChangeHandler(event) {
        this.isSiteSelected = true;
        this.selectValue = event.target.value;
    }
    setId(event) {
        this.showSchedule = true;
        this.slotId = event.detail;
    }

    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    handleCloseModalonSave() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    justCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    handleMapModal() {
        this.showMapModal = true;
    }

    handleMapModalClose(event) {
        this.showMapModal = false;

        if (event.detail != 'close') {
            this.selectValue = event.detail
            this.getAccount(this.selectValue);
            
        }
        this.selectSiteName = this.map.get(this.selectValue);

    }

    msToTime(s) {
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;

        return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs) + '.' + this.pad(ms, 3);
    }
    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }

    getAccount(Id) {
        this.showSpinner = true;
        getAccountdetails({
                accountId: Id
            })
            .then(result => {
                this.accountDetails = JSON.parse(JSON.stringify(result));
                this.location = result.location;
                console.log('account details', JSON.stringify(result));
                if(this.accountDetails){
                    this.showLocationDetails = true;
                    this.mapMarkers.length = 0;
                    this.mapMarkers.push(this.accountDetails);
                }
                console.log('---result.businessStartTime---'+result.businessStartTime);
                console.log('---result.businessEndTime---'+result.businessEndTime);
                if(result.businessStartTime){
                    this.preferredTimeMin = this.msToTime(result.businessStartTime);
                }
                else{
                    this.preferredTimeMin = '07:00:00.000Z';
                }
                if(result.businessEndTime){
                    this.preferredTimeMax = this.msToTime(result.businessEndTime);
                }
                else{
                    this.preferredTimeMax = '19:00:00.000Z';
                }
                this.showSpinner = false;
            })
            .catch(error => {
                console.log('error');
                this.showSpinner = false;
            })
    }

    handleSingleMap(){
        this.showSingleMap = true;
    }

    handlesingleMapClose(){
        this.showSingleMap = false;
    }

    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }
}