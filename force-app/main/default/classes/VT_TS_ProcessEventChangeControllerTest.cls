@IsTest
private class VT_TS_ProcessEventChangeControllerTest {
	@testSetup
    private static void makeData() {
        // Account, Contact, Event, Slot, Appointment
        Account acc = new Account();
        acc.Name = 'Testing Account 1';
        acc.RecordTypeId = Account.SObjectType.getDescribe().RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        acc.Event_Process__c = 'Testing Site';
        acc.Vermont_County__c = 'Addison';
        acc.Business_Start_Time__c = Time.newInstance(00, 00, 00, 00);
        acc.Business_End_Time__c = Time.newInstance(11, 00, 00, 00);
        acc.Event_Type__c = 'State-Sponsored';
        insert acc;
        
        Account acc1 = new Account();
        acc1.Name = 'Testing Account 2';
        acc1.RecordTypeId = Account.SObjectType.getDescribe().RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        acc1.Event_Process__c = 'Testing Site';
        acc1.Vermont_County__c = 'Addison';
        acc1.Business_Start_Time__c = Time.newInstance(00, 00, 00, 00);
        acc1.Business_End_Time__c = Time.newInstance(11, 00, 00, 00);
        acc1.Event_Type__c = 'State-Sponsored';
        insert acc1;
        
        Appointment_Slot__c slot = new Appointment_Slot__c();
        slot.Account__c = acc.Id;
        slot.Date__c = Date.today();
        slot.Start_Time__c = Time.newInstance(01, 00, 00, 00);
        slot.End_Time__c = Time.newInstance(02, 00, 00, 00);
        insert slot;
        
        VTS_Event__c event = new VTS_Event__c();
        event.Account__c = acc.Id;
        event.Start_Date__c = Date.today();
        event.End_Date__c = Date.today();
        event.Start_Time__c = Time.newInstance(00, 00, 00, 00);
        event.End_Time__c = Time.newInstance(11, 00, 00, 00);
        event.Event_Type__c = 'Public';
        event.Status__c = 'Open';
        event.Location__c = acc.Id;
        insert event;
        
        Contact c = new Contact();
        c.FirstName = 'First Name';
        c.LastName = 'Last Name';
        c.Email = 'first@yopmail.com';
        c.RecordTypeId = Contact.SObjectType.getDescribe().RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        c.Birthdate = Date.newInstance(1995, 07, 23);
        insert c;
        
        Appointment__c app = new Appointment__c();
        app.Patient__c = c.Id;
        app.Appointment_Slot__c = slot.Id;
        app.Lab_Center__c = acc.Id;
        insert app;
    }
    
    @IsTest
    private static void updateEventTest() {
        Map<String, Id> data = new Map<String, Id>();
        data.put('eventId', [SELECT Id FROM VTS_Event__c LIMIT 1]?.Id);
        data.put('newLocationId', [SELECT Id FROM Account LIMIT 1]?.Id);
        Map<String, Object> result = VT_TS_ProcessEventChangeController.updateEvent(data);
        system.assertEquals(true, Boolean.valueOf(result.get('success')), 'updateEventTest');
    }
    
    @IsTest
    private static void updateEventExceptionTest() {
        Map<String, Id> data = new Map<String, Id>();
        data.put('eventId', [SELECT Id FROM VTS_Event__c WHERE Name = 'test' LIMIT 1]?.Id);
        data.put('newLocationId', [SELECT Id FROM Account LIMIT 1]?.Id);
        try {
            VT_TS_ProcessEventChangeController.updateEvent(data);
        } catch(Exception e){
            system.assertEquals('System.DmlException', e.getTypeName(), 'updateEventExceptionTest');
        }
    }
    
    @IsTest
    private static void getMatchingEvents() {
        Map<String, Object> data = new Map<String, Object>();
        data.put('newLocationId', [SELECT Id FROM Account LIMIT 1]?.Id);
		List<VTS_Event__c> events = VT_TS_ProcessEventChangeController.getMatchingEventsOfNewLocation(data, System.today(), System.today());
        system.assertEquals(true, events.size() > 0,'getMatchingEvents');
    }
    
    @IsTest
    private static void getMatchingEventsPastDate() {
        Map<String, Object> data = new Map<String, Object>();
        data.put('newLocationId', [SELECT Id FROM Account LIMIT 1]?.Id);
        try {
            VT_TS_ProcessEventChangeController.getMatchingEventsOfNewLocation(data, System.today().addDays(-2), System.today());
        } catch(Exception e) {
            system.assertEquals('System.AuraHandledException', e.getTypeName(), 'updateEventExceptionTest');
        }
    }
}