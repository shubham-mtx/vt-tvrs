/**
 * @description       : 
 * @author            : Balram Dhawan
 * @group             : 
 * @last modified on  : 01-15-2022
 * @last modified by  : Mohit Karani
 **/
import {
    LightningElement,
    track,
    api
} from 'lwc';
import getContactDetails from "@salesforce/apex/OkpcContactInformationController.getContactDetails";
import getContactDetailsWithAppointments from "@salesforce/apex/OkpcContactInformationController.getContactDetailsWithAppointments";
import saveContactDetails from "@salesforce/apex/OkpcContactInformationController.saveContactDetails";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import fetchPicklist from "@salesforce/apex/OkpcContactInformationController.fetchPicklist";
import checkEmailExistOrNot from "@salesforce/apex/OkpcContactInformationController.checkEmailExistOrNot";
import getCovidPortalUserContactId from "@salesforce/apex/OkpcContactInformationController.getCovidPortalUserContactId";
import checkValid from "@salesforce/apex/OkpcContactInformationController.checkValid";
import createContact from "@salesforce/apex/OkpcContactInformationController.createContact";
import momentPath from "@salesforce/resourceUrl/momentJs";
import {
    loadScript
} from "lightning/platformResourceLoader";
import checkValidParentContact from "@salesforce/apex/OkpcContactInformationController.checkValidParentContact";
import getChildContacts from '@salesforce/apex/OkpcContactInformationController.getChildContacts';
import {
    showMessage,
    showAsyncErrorMessage,
    showMessageWithLink,
    msgObj,
    createMessage
} from 'c/vtTsUtility';



export default class OkpcContactInfo extends LightningElement {

    @api isAppointment;
    @api searchExistingContact;
    @api appointmentContact = {};
    @api eventId;
    @api istesting;
    @api appointmentType = 'Testing';
    @api isVaccineEvent;
    @api isFromTestingSite;
    @api dependentContactId = ''; //to be deleted
    @api isDependentContact; //to be deleted
    @api contactId;
    @api consentDetails;

    @track existEmail = false;
    @track dataObj = {};
    @track extraWrapper = {};
    // @track contactId;  // Duplicate Variable
    @track showSpinner = false;
    @track disableInputs = true;
    @track stateOptions = [];
    @track physicalStateOptions = [];
    @track salutations = [];
    @track LangOptions = [];
    @track raceOptions = [];

    // added by vamsi 
    @track ethnicityOptions = [];

    @track maritialStatusOptions = [];
    @track phoneTypeOptions;
    @track age;
    @api currentStep;
    @track todayDate;
    @track selectOneCommunicationError = false;
    @track openContactSearchWindow;
    @track isLandlineSelected;
    @track isCovidPortal = false;
    msgObjData = msgObj();
    showExistingContactSelection = true;
    existingContactId;
    existingContactName;
    ageLessThan18;
    parentContactId;
    parentContactName;
    openGuardianSearchWindow;
    resultRecieveConsent;
    initialGuardian;
    customErrorMessage;
    overrideConfirmation;
    preRegData;
    modalVisible;
    resultRecieveConsentLabel = "By checking this box and withholding consent to receive electronic communications from the Vermont Department of Health regarding my medical care, I acknowledge that I may not receive timely notice of canceled or rescheduled appointments for testing or vaccine administration.";
    contactVsAppointments = [];
    isLoaded;
    showPageHeader = true;
    isPhysicalAddressDifferent = true;
    fields = ["Name", "Birthdate", "Age__c", "Gender__c", "Street_Address1__c", "Street_Address_2__c", "City__c", "State__c", "ZIP__c", "Email", "MobilePhone", 'Phone_Type__c'];
    @track checkValidOutput = {};
    @track actionParamameters = {};
    showAlternateClassVaccineModal;
    get randomName() {
        return this.makeid(10);
    }

    makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    get genderOptions() {
        return [{
                label: "Male",
                value: "Male"
            },
            {
                label: "Female",
                value: "Female"
            },
            {
                label: "Other",
                value: "Other"
            }
        ];
    }

    get resultCopyOptions() {
        return [{
                label: "Electronic (Fastest way to get your test results; 2-3 business days)",
                value: "E-Mail"
            },
            {
                label: "USPS Letter (5-7 business days)",
                value: "USPS Letter"
            }
        ];
    }
    // added by vamsi mudaliar
    get showIsLanguageEqualsOther() {
        return this.dataObj.primaryLanguage === 'Other' ? true : false;
    }

    @track registrationValues = [{
            label: 'Myself',
            value: 'Myself'
        },
        {
            label: 'Someone Else',
            value: 'Someone Else'
        }
    ];

    get ismyself() {
        if (this.dataObj) {
            if (this.dataObj.hasOwnProperty('registrationForAgree')) {
                return this.dataObj.registrationForAgree === 'Myself';
            }
        }
        return false;
    }

    get onlyYesNoOptions() {
        return [{
                label: "Yes",
                value: "Yes"
            },
            {
                label: "No",
                value: "No"
            }
        ];
    }

    get disabledFirstNameLastNameDOB() {
        return this.existingContactId || (this.contactId && this.contactVsAppointments.length > 0);
    }

    get disabledEmail() {
        return (this.contactId || this.existingContactId);
    }

    onEmailOut(event) {
        if (this.isAppointment === 'true' && this.istesting != 'True' && event.target.value != '') {
            checkEmailExistOrNot({
                conEmail: event.target.value
            }).then((result) => {
                this.existEmail = result;
            }).catch((error) => {
                let message = error.message || error.body.message;
                showAsyncErrorMessage(this, message);
            });
        }
    }

    handlePhysicalAddressSameChecked(event) {
        const isChecked = event.target.checked;
        this.isPhysicalAddressDifferent = !isChecked;
        if (this.dataObj) {
            this.dataObj.physicalAddress1 = isChecked ? this.dataObj.address1 : '';
            this.dataObj.physicalCity = isChecked ? this.dataObj.city : '';
            this.dataObj.physicalState = isChecked ? this.dataObj.state : '';
            this.dataObj.physicalZip = isChecked ? this.dataObj.zip : '';
        }
    }

    connectedCallback() {
        loadScript(this, momentPath);
        this.doInit();
        this.getChildContactsHelper();
        this.getPicklist("Contact", "State__c", result => {
            let states = [];
            states.push({
                label: "VT",
                value: "VT"
            });

            result.forEach(element => {
                if (element.value != 'VT') {
                    states.push(element);
                }
            });

            this.stateOptions = states;
        });
        this.getPicklist("Contact", "Physical_State__c", result => {
            let states = [];
            states.push({
                label: "VT",
                value: "VT"
            });

            result.forEach(element => {
                if (element.value != 'VT') {
                    states.push(element);
                }
            });

            this.physicalStateOptions = states;
        });

        this.getPicklist("Contact", "Salutation", result => this.salutations = result);
        this.getPicklist("Contact", "Primary_Language__c", result => this.LangOptions = result);
        this.getPicklist("Contact", "Race__c", result => this.raceOptions = result);
        this.getPicklist("Contact", "Marital_Status__c", result => this.maritialStatusOptions = result);
        this.getPicklist("Contact", "Phone_Type__c", result => this.phoneTypeOptions = result);

        // getting ethnicity picklist values - added by vamsi 
        this.getPicklist("Contact", "Ethnicity__c", result => this.ethnicityOptions = result);


        this.focusTitle();
    }

    // w.r.t I-59727
    getChildContactsHelper() {
        if (this.appointmentContact.existingContactId) {
            getChildContacts({
                    contactId: this.appointmentContact.existingContactId
                })
                .then(result => {
                    if (result.length > 0) {
                        this.showExistingContactSelection = false;
                    }
                })
                .catch(error => {
                    showAsyncErrorMessage(this, error);
                    this.dispatchEvent(new ShowToastEvent({
                        message: error.body.message,
                        variant: 'error'
                    }));
                });
        }
    }

    doInit() {
        if (this.contactId) {
            this.dataObj.contactId = this.contactId;
            this.getContact();
        } else if (this.appointmentContact.existingContactId) {
            this.existingContactId = this.appointmentContact.existingContactId;
            this.existingContactName = this.appointmentContact.existingContactName;
        }

        if (this.appointmentContact.parentContactId) {
            this.initialGuardian = true;
            this.parentContactId = this.appointmentContact.parentContactId;
            this.parentContactName = this.appointmentContact.parentContactName;
        }

        if (this.isAppointment) {
            this.dataObj = JSON.parse(JSON.stringify(this.appointmentContact));
            if (this.dataObj.registrationForAgree) {
                this.dataObj.smsCheckbox = this.dataObj.smsCheckbox ? 'No' : 'Yes';
                this.dataObj.emailCheckbox = this.dataObj.emailCheckbox ? 'No' : 'Yes';
                this.dataObj.eighteenPlus = this.dataObj.eighteenPlus ? 'Yes' : 'No';

                if (this.dataObj.dob) {
                    this.calculateAge(this.dataObj.dob);
                }

                if (this.dataObj && this.dataObj.dob) {
                    let _this = this;
                    setTimeout(function () {
                        _this.template.querySelector('c-vt_ts_custom_date').setDobVal(_this.dataObj.dob);
                    }, 500);
                }
            }
            this.handleInputChange({
                target: {
                    dataset: {
                        name: 'smsCheckbox'
                    },
                    value: this.dataObj.smsCheckbox
                }
            });
        }
        this.todayDate = this.getTodayDate();
        this.getCovidPortalUserContact();
    }

    getPicklist(object, fieldName, callback) {
        fetchPicklist({
            objectName: object,
            fieldName: fieldName
        }).then((result) => {
            callback(result);
        }).catch((error) => {
            let message = error.message || error.body.message;
            showAsyncErrorMessage(this, message);
        });
    }

    getCovidPortalUserContact() {
        getCovidPortalUserContactId().then((result) => {
            //this.contactId = result;
            if (result === undefined || result === '') {
                this.isCovidPortal = false;
            } else {
                this.isCovidPortal = true;
            }
        }).catch((error) => {
            let message = error.message || error.body.message;
        });
    }

    @api
    editContacts() {
        this.disableInputs = false;
    }

    getContact() {
        this.showSpinner = true;
        getContactDetailsWithAppointments({
            contactid: this.contactId
        }).then(contactWithAppointments => {
            this.contactVsAppointments = contactWithAppointments.appointments;
            let result = contactWithAppointments.contact;
            this.dataObj.firstName = result.firstName;
            this.dataObj.middleName = result.middleName;
            this.dataObj.lastName = result.lastName;
            this.dataObj.email = result.email;
            this.dataObj.phone = result.phone;
            if (result.phone) {
                this.dataObj.phone = this.formatPhoneInput(this.dataObj.phone);
            }
            this.dataObj.phoneType = result.phoneType;
            this.dataObj.patientId = result.patientId;
            this.dataObj.contactId = this.contactId;
            this.dataObj.address2 = result.address2;
            this.dataObj.address1 = result.address1;
            this.dataObj.physicalAddress1 = result.physicalAddress1;
            this.dataObj.physicalCity = result.physicalCity;
            this.dataObj.physicalState = result.physicalState;
            this.dataObj.physicalZip = result.physicalZip;
            this.dataObj.dob = result.dob;

            this.template.querySelector('c-vt_ts_custom_date').setDobVal(this.dataObj.dob);

            let si = setInterval(() => {
                const preRegCmp = this.template.querySelector('c-vt_ts_pre-reg-info');
                if (preRegCmp) {
                    preRegCmp.setAgeBasedGroup(this.dataObj.dob);
                    clearInterval(si);
                }
            }, 1000);

            this.dataObj.city = result.city;
            this.dataObj.state = result.state;
            this.dataObj.zip = result.zip;
            this.dataObj.gender = result.gender;
            this.dataObj.smsCheckbox = result.smsCheckbox ? 'No' : 'Yes';
            this.dataObj.emailCheckbox = result.emailCheckbox ? 'No' : 'Yes';
            this.dataObj.eighteenPlus = result.eighteenPlus ? 'Yes' : 'No';

            if (this.dataObj.state === this.dataObj.physicalState &&
                this.dataObj.address1 === this.dataObj.physicalAddress1 &&
                this.dataObj.city === this.dataObj.physicalCity &&
                this.dataObj.zip === this.dataObj.physicalZip) {
                this.dataObj.sameAsMailingAddress = true;
            }

            this.handleInputChange({
                target: {
                    dataset: {
                        name: 'smsCheckbox'
                    },
                    value: this.dataObj.smsCheckbox
                }
            });
            this.handleInputChange({
                target: {
                    dataset: {
                        name: 'emailCheckbox'
                    },
                    value: this.dataObj.emailCheckbox
                }
            });
            this.dataObj.testResultCopy = result.testResultCopy;
            if (result.testResultCopy === 'E-Mail') {
                this.resultRecieveConsent = true;
            }
            this.dataObj.resultRecieveConsent = result.resultRecieveConsent;
            this.dataObj.suffix = result.suffix;
            this.dataObj.stateofBirth = result.stateofBirth;
            this.dataObj.countryofBirth = result.countryofBirth;
            this.dataObj.race = result.race;
            this.dataObj.ethnicity = result.ethnicity;
            this.dataObj.primaryLanguage = result.primaryLanguage;
            this.dataObj.otherLanguage = result.otherLanguage;
            this.dataObj.maritialStatus = result.maritialStatus;
            this.dataObj.registrationForAgree = result.registrationForAgree;
            if (this.dataObj.dob) {
                this.calculateAge(this.dataObj.dob);
            }
            this.showSpinner = false;
            if (this.dataObj.phoneType === 'Landline') {
                this.dataObj.smsCheckbox = 'No';
                this.isLandlineSelected = true;
            }

        }).catch(error => {
            showAsyncErrorMessage(this, error);
            this.dispatchEvent(new ShowToastEvent({
                message: error.body.message,
                variant: 'error'
            }));
            this.showSpinner = false;
        });
    }

    getExistingContactDetail() {
        this.showSpinner = true;
        getContactDetails({
            contactid: this.existingContactId
        }).then(result => {
            let preRegComponent = this.template.querySelector('c-vt_ts_pre-reg-info');
            if (preRegComponent) {
                preRegComponent.contactId = this.existingContactId;
                preRegComponent.doInit();
            }
            this.dataObj.firstName = result.firstName;
            this.dataObj.middleName = result.middleName;
            this.dataObj.lastName = result.lastName;
            this.dataObj.email = result.email;
            this.dataObj.phone = result.phone;
            if (result.phone) {
                this.dataObj.phone = this.formatPhoneInput(this.dataObj.phone);
            }
            this.dataObj.phoneType = result.phoneType;
            this.dataObj.patientId = result.patientId;
            this.dataObj.existingContactId = this.existingContactId;
            this.dataObj.contactId = this.existingContactId;
            this.dataObj.address2 = result.address2;
            this.dataObj.address1 = result.address1;
            this.dataObj.physicalAddress1 = result.physicalAddress1;
            this.dataObj.physicalCity = result.physicalCity;
            this.dataObj.physicalState = result.physicalState;
            this.dataObj.physicalZip = result.physicalZip;
            this.dataObj.dob = result.dob;

            this.template.querySelector('c-vt_ts_custom_date').setDobVal(this.dataObj.dob);

            const preRegCmp = this.template.querySelector('c-vt_ts_pre-reg-info');
            if (preRegCmp) {
                preRegCmp.setAgeBasedGroup(this.dataObj.dob);
            }

            this.dataObj.gender = result.gender;
            this.dataObj.city = result.city;
            this.dataObj.state = result.state;
            this.dataObj.zip = result.zip;
            this.dataObj.smsCheckbox = result.smsCheckbox ? 'No' : 'Yes';
            this.dataObj.emailCheckbox = result.emailCheckbox ? 'No' : 'Yes';
            this.dataObj.eighteenPlus = result.eighteenPlus ? 'Yes' : 'No';


            if (this.dataObj.state === this.dataObj.physicalState &&
                this.dataObj.address1 === this.dataObj.physicalAddress1 &&
                this.dataObj.city === this.dataObj.physicalCity &&
                this.dataObj.zip === this.dataObj.physicalZip) {
                this.dataObj.sameAsMailingAddress = true;
            }

            this.handleInputChange({
                target: {
                    dataset: {
                        name: 'smsCheckbox'
                    },
                    value: this.dataObj.smsCheckbox
                }
            });
            this.handleInputChange({
                target: {
                    dataset: {
                        name: 'emailCheckbox'
                    },
                    value: this.dataObj.emailCheckbox
                }
            });

            this.dataObj.testResultCopy = result.testResultCopy;
            if (result.testResultCopy === 'E-Mail') {
                this.resultRecieveConsent = true;
            }
            this.dataObj.resultRecieveConsent = result.resultRecieveConsent;
            this.dataObj.suffix = result.suffix;
            this.dataObj.stateofBirth = result.stateofBirth;
            this.dataObj.countryofBirth = result.countryofBirth;
            this.dataObj.primaryLanguage = result.primaryLanguage;
            this.dataObj.maritialStatus = result.maritialStatus;
            this.dataObj.registrationForAgree = result.registrationForAgree;
            // added by vamsi 
            this.dataObj.race = result.race;
            this.dataObj.ethnicity = result.ethnicity;

            if (this.dataObj.dob) {
                this.calculateAge(this.dataObj.dob);
            }
            this.showSpinner = false;

        }).catch(error => {
            showAsyncErrorMessage(this, error);
            this.dispatchEvent(new ShowToastEvent({
                message: error.body.message,
                variant: 'error'
            }));
            this.showSpinner = false;
        });
    }

    handleInputChange(event) {
        this.selectOneCommunicationError = false;
        //Added by Manthan Starts
        switch (event.target.dataset.name) {

            case 'firstName':
                this.dataObj.firstName = event.target.value;
                break;
            case 'middleName':
                this.dataObj.middleName = event.target.value;
                break;
            case 'lastName':
                this.dataObj.lastName = event.target.value;
                break;
            case 'email':
                this.dataObj.email = event.target.value;
                break;
            case 'Suffix':
                this.dataObj.suffix = event.target.value;
                break;
            case 'StateofBirth':
                this.dataObj.stateofBirth = event.target.value;
                break;
            case 'CountryofBirth':
                this.dataObj.countryofBirth = event.target.value;
                break;
            case 'Ethnicity':
                this.dataObj.ethnicity = event.target.value;
                break;
            case 'Gender':
                this.dataObj.gender = event.target.value;
                break;
            case 'PrimaryLanguage':
                if (event.target.value !== 'Other') {
                    this.dataObj.otherLanguage = null; // reseting the other Language if its not "Other"
                }
                this.dataObj.primaryLanguage = event.target.value;
                break;
            case 'OtherLanguage':
                this.dataObj.otherLanguage = event.target.value;
                break;
            case 'Race':
                this.dataObj.race = event.target.value;
                break;
            case 'MaritialStatus':
                this.dataObj.maritialStatus = event.target.value;
                break;
            case 'phone':
                this.dataObj.phone = event.target.value;
                this.formatPhone(event.target);
                break;
            case 'phoneType':
                if (event.target.value === 'Landline') {
                    this.dataObj.smsCheckbox = 'No';
                    this.isLandlineSelected = true;
                } else {
                    this.isLandlineSelected = false;
                }
                this.dataObj.phoneType = event.target.value;
                switch (event.target.value) {
                    case 'Landline':
                        this.dataObj.smsCheckbox = 'No';
                        this.isLandlineSelected = true;
                        break;
                    default:
                        this.isLandlineSelected = false;
                }
                this.dataObj.phoneType = event.target.value;
                break;

            case 'emailCheckbox':
                this.dataObj.emailCheckbox = event.target.value;
                break;
            case 'smsCheckbox':
                this.dataObj.smsCheckbox = event.target.value;
                break;
            case 'patientId':
                this.dataObj.patientId = event.target.value;
                break;
            case 'gender':
                this.dataObj.gender = event.target.value;
                break;
            case 'zip':
                this.dataObj.zip = event.target.value;
                this.dataObj.physicalZip = (!this.isPhysicalAddressDifferent) ? this.dataObj.zip : this.dataObj.physicalZip;
                break;
            case 'state':
                this.dataObj.state = event.target.value;
                this.dataObj.physicalState = (!this.isPhysicalAddressDifferent) ? this.dataObj.state : this.dataObj.physicalState;
                break;
            case 'city':
                this.dataObj.city = event.target.value;
                this.dataObj.physicalCity = (!this.isPhysicalAddressDifferent) ? this.dataObj.city : this.dataObj.physicalCity;
                break;
            case 'address1':
                this.dataObj.address1 = event.target.value;
                this.dataObj.physicalAddress1 = (!this.isPhysicalAddressDifferent) ? this.dataObj.address1 : this.dataObj.physicalAddress1;
                break;
            case 'physicalZip':
                this.dataObj.physicalZip = event.target.value;
                break;
            case 'physicalState':
                this.dataObj.physicalState = event.target.value;
                break;
            case 'physicalAddress1':
                this.dataObj.physicalAddress1 = event.target.value;
                break;
            case 'physicalCity':
                this.dataObj.physicalCity = event.target.value;
                break;
            case 'address2':
                this.dataObj.address2 = event.target.value;
                break;
            case 'eighteenPlus':
                this.dataObj.eighteenPlus = event.target.value;
                break;
            case 'testResultCopy':
                this.dataObj.testResultCopy = event.target.value;
                break;
            case 'registrationFor':
                this.dataObj.registrationFor = event.target.value;
                break;
            case 'resultRecieveConsent':
                this.dataObj.resultRecieveConsent = event.target.checked;
                break;
            case 'registrationForAgree':
                this.dataObj.registrationForAgree = event.target.checked ? true : false;
                break;
        }
        //Added by Manthan Ends

        // I-41337
        if ((event.target.dataset.name === 'smsCheckbox' || event.target.dataset.name === 'emailCheckbox') &&
            event.target.value === 'No' &&
            this.dataObj.smsCheckbox === 'No' &&
            this.dataObj.emailCheckbox === 'No') {
            this.resultRecieveConsentLabel = "By checking this box and withholding consent to receive electronic communications from the Vermont Department of Health regarding my medical care, I acknowledge that I may not receive timely notice of canceled or rescheduled appointments for testing or vaccine administration.";
            // this.resultRecieveConsent = true; I-46322
            let elements = this.template.querySelectorAll('lightning-input');
            elements.forEach(element => {
                if (element.dataset.name === 'resultRecieveConsent') {
                    element.checked = false;
                }
            });
            if (this.appointmentContact) {
                this.dataObj.resultRecieveConsent = this.appointmentContact.resultRecieveConsent;
            } else {
                this.dataObj.resultRecieveConsent = false;
            }
        } else if ((event.target.dataset.name === 'smsCheckbox' || event.target.dataset.name === 'emailCheckbox') &&
            (this.dataObj.smsCheckbox === 'Yes' ||
                this.dataObj.emailCheckbox === 'Yes')) {

            this.resultRecieveConsentLabel = "By checking this box, I consent to the Vermont Department of Health communicating with me by email and/or standard SMS messaging regarding various aspects of my medical care, including test results, appointments, and contact tracing. Standard SMS rates will apply. Communications sent by email or standard SMS are not encrypted. This means a third party may be able to access the information regarding my medical care. I understand the risks associated with unencrypted email and standard SMS and hereby give the Vermont Department of Health permission to send me my personal health information via unencrypted email and/or standard SMS message.";
            let elements = this.template.querySelectorAll('lightning-input');
            elements.forEach(element => {
                if (element.dataset.name === 'resultRecieveConsent') {
                    element.checked = false;
                }
            });
            if (this.appointmentContact) {
                this.dataObj.resultRecieveConsent = this.appointmentContact.resultRecieveConsent;
            } else {
                this.dataObj.resultRecieveConsent = false;
            }
        }
    }

    @api
    async getContactObj() {
        if (this.validate()) {
            let _dataObj = {
                ...this.dataObj
            };
            _dataObj.emailCheckbox = this.dataObj.emailCheckbox === 'No' ? true : false;
            _dataObj.smsCheckbox = this.dataObj.smsCheckbox === 'No' ? true : false;
            _dataObj.eighteenPlus = this.dataObj.eighteenPlus === 'Yes' ? true : false;
            let actionParam = {
                jsonData: JSON.stringify(_dataObj),
                currentstep: this.currentStep
            };
            if (Object.keys(this.consentDetails).length !== 0 && this.consentDetails.constructor === Object) {
                actionParam.consentValue = this.consentDetails.consentvalue;
                actionParam.screeningvalue = this.consentDetails.screeningvalue;
                this.dataObj.consentValue = this.consentDetails.consentvalue;
            }

            if (this.isVaccineEvent) {
                let preRegCmpTemp = this.template.querySelector('c-vt_ts_pre-reg-info');
                preRegCmpTemp.setContactAge(this.dataObj.dob);
                const data = preRegCmpTemp.getPreRegData();
                let tempPreRegData = JSON.parse(JSON.stringify(data.preRegData));
                delete tempPreRegData.Pre_Registration_Group__r;
                delete tempPreRegData.Early_Group1_Health__r;
                delete tempPreRegData.Early_Group_2_Access_Group__r;
                actionParam.preReg = tempPreRegData;
                this.preRegData = JSON.parse(JSON.stringify(data.preRegData));

                if (!data.success) {
                    this.modalVisible = false;
                    this.dispatchEvent(new ShowToastEvent({
                        variant: 'error',
                        message: data.msg
                    }));
                } else {

                    if (!this.modalVisible && !(this.preRegData.Id) &&
                        (
                            // this.preRegData.Are_you_eligible_for_Vaccine_Risk_Group__c === 'Yes' ||
                            this.preRegData.Chronic_Condition__c === 'Yes' ||
                            this.preRegData.Affiliated_to_any_other_Risk_Group__c === 'Yes')) {
                        this.template.querySelector('c-vt_ts_pre-reg-info').confirmationModal = true;
                        setTimeout(() => {
                            this.template.querySelector('c-vt_ts_pre-reg-info').focusFirstEle();
                        }, 200);
                        this.modalVisible = true;
                    } else if (!this.modalVisible &&
                        this.preRegData.Id &&
                        (
                            // (this.preRegData.Are_you_eligible_for_Vaccine_Risk_Group__c === 'Yes' &&
                            //     data.oldPreRegData.Are_you_eligible_for_Vaccine_Risk_Group__c == 'No') ||

                            (this.preRegData.Chronic_Condition__c === 'Yes' &&
                                data.oldPreRegData.Chronic_Condition__c === 'No') ||

                            (this.preRegData.Affiliated_to_any_other_Risk_Group__c === 'Yes' &&
                                data.oldPreRegData.Affiliated_to_any_other_Risk_Group__c === 'No')
                        )
                    ) {
                        this.template.querySelector('c-vt_ts_pre-reg-info').confirmationModal = true;
                        setTimeout(() => {
                            this.template.querySelector('c-vt_ts_pre-reg-info').focusFirstEle();
                        }, 200);
                        this.modalVisible = true;
                    } else {
                        this.showSpinner = true;
                        this.saveHelper(actionParam);
                    }
                }
            } else {
                checkValidParentContact({
                        JsonData: JSON.stringify(_dataObj)
                    })
                    .then(result => {
                        this.modalVisible = false;
                        const selectedEvent = new CustomEvent("setobject", {
                            detail: actionParam
                        });
                        this.dispatchEvent(selectedEvent);
                    })
                    .catch(error => {
                        showAsyncErrorMessage(this, error);
                        this.throwMessage(this.istesting, error.body.message);
                    });
            }
        }
    }

    throwMessage(isTestSite, message) {
        if (!isTestSite) {
            this.customErrorMessage = message;
            this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
        } else {
            this.dispatchEvent(new ShowToastEvent({
                message: message,
                variant: 'error'
            }));
        }
    }

    saveHelper(actionParam) {
        let tempData = {
            firstName: this.dataObj.firstName,
            lastName: this.dataObj.lastName,
            email: this.dataObj.email,
            eventId: this.eventId,
            age: this.age,
            contactId: this.contactId,
            isFromTestingSite: this.istesting
        }

        checkValid({
                responseWrapper: JSON.stringify(tempData)
            })
            .then(result => {
                this.showSpinner = false;
                this.checkValidOutput = result;
                this.actionParamameters = actionParam;
                if (result.isSelectedDoseAlreadyScheduled) {
                    if (!this.istesting) {
                        this.customErrorMessage = 'It looks like the Contact already have an appointment to get their ' + result.doseNumber + ' of a COVID-19 vaccine. If you want to change your appointment, you need to cancel your existing appointment first.';
                        this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
                    } else {
                        createMessage(result.doseNumber, this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyScheduled']);
                    }
                } else if (result.isSelectedDoseAlreadyCompleted) {
                    if (!this.istesting) {
                        this.customErrorMessage = 'It looks like the Contact already had their selected dose of a COVID-19 vaccine.  Please use the Schedule Next Dose Button from the contact record to schedule their Next dose Appointment.';
                        this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
                    } else {
                        showMessage(this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyCompleted']);
                    }
                    this.modalVisible = false;
                } else if (result.furtherDosesNotReqd) {
                    if (!this.istesting) {
                        this.customErrorMessage = 'You have already received all required doses of your COVID-19 vaccine, so you do not need to sign up to get any additional doses. If you have questions, please visit the <a href="https://www.healthvermont.gov/covid-19/vaccine/about-covid-19-vaccines-vermont" target="_blank">Vermont Department of Health website</a> or call the COVID-19 Call Center at 802-863-7240.';
                        this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
                    } else {
                        showMessageWithLink(this, 'error', this.msgObjData['furtherDosesNotReqd'], this.msgObjData['vermontMsgData']);
                    }
                    this.modalVisible = false;
                } else if (result.previousDoseInScheduledStatus) {
                    if (!this.istesting) {
                        this.customErrorMessage = result.previousDoseInScheduledStatus;
                        this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
                    } else {
                        showMessage(this, 'Error!', 'error', result.previousDoseInScheduledStatus);

                    }
                    this.modalVisible = false;
                } else if (result.is2ndDoseMismatch) {
                    this.customErrorMessage = result.is2ndDoseMismatch;
                    this.template.querySelector('c-lwc-custom-toast').showCustomNotice();

                } else if (result.privateAcesssAssignmentNotFound && (this.istesting || (!this.istesting && !result.eligiblePreRegGroupIds))) {
                    if (!this.istesting) {
                        this.customErrorMessage = result.privateAcesssAssignmentNotFound;
                        this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
                    } else {
                        showMessageWithLink(this, 'error', this.msgObjData['noPrivateAccessAssignmentFound'], this.msgObjData['vermontMsgData']);

                    }
                    this.modalVisible = false;
                } else if (result.isMinDateMismatch) {
                    this.customErrorMessage = result.isMinDateMismatch;
                    this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
                    this.modalVisible = false;
                } else if (result.preRegIncomplete) {
                    this.customErrorMessage = result.preRegIncomplete;
                    this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
                    this.modalVisible = false;
                } else if (result.selectedAlternateVaccineClass) {
                    this.showAlternateClassVaccineModal = true;
                } else {
                    this.createContactFunction(result, actionParam);
                }
            })
            .catch(error => {
                let e = error.message || error.body.message;
                showAsyncErrorMessage(this, e);
                this.throwMessage(this.istesting, e);
                this.showSpinner = false;
            });
    }

    createContactFunction(result, actionParam) {
        let tempPreRegData = JSON.parse(JSON.stringify(this.preRegData));
        delete tempPreRegData.Pre_Registration_Group__r;
        delete tempPreRegData.Early_Group1_Health__r;
        delete tempPreRegData.Early_Group_2_Access_Group__r;
        this.showSpinner = true;

        let _dataObj = {
            ...this.dataObj
        };
        _dataObj.emailCheckbox = _dataObj.emailCheckbox === 'No' ? true : false;
        _dataObj.smsCheckbox = _dataObj.smsCheckbox === 'No' ? true : false;
        _dataObj.eighteenPlus = _dataObj.eighteenPlus === 'Yes' ? true : false;
        _dataObj.consentValue = actionParam.consentValue;

        this.dataObj.emailCheckbox = (_dataObj.emailCheckbox === 'No' || _dataObj.emailCheckbox === true) ? true : false;
        this.dataObj.smsCheckbox = (_dataObj.smsCheckbox === 'No' || _dataObj.smsCheckbox === true) ? true : false;
        this.dataObj.eighteenPlus = (_dataObj.eighteenPlus === 'Yes' || _dataObj.eighteenPlus === true) ? true : false;
        // this.dataObj.resultRecieveConsent = this.resultRecieveConsent;

        createContact({
                JsonData: JSON.stringify(_dataObj),
                recordType: 'Citizen_COVID',
                preReg: tempPreRegData,
                eventId: this.eventId,
                istesting: this.istesting
            })
            .then(cc_result => {
                this.showSpinner = false;

                this.existingContactId = cc_result.data.Id;
                this.existingContactName = cc_result.data.FirstName + ' ' + cc_result.data.LastName;
                this.dataObj.existingContactId = cc_result.data.Id;
                this.dataObj.existingContactName = cc_result.data.FirstName + ' ' + cc_result.data.LastName;
                this.dataObj.existingContactId = cc_result.data.Id;
                this.dataObj.existingContactName = cc_result.data.FirstName + ' ' + cc_result.data.LastName;


                let invalidOuput = false;
                if (cc_result.Pass_Code_Health__c === 'NOT_VALID' ||
                    cc_result.Pass_Code_Risk_Group__c === 'NOT_VALID' ||
                    cc_result.invalidChronicAnswer ||
                    cc_result.invalidOtherRiskGroupAnswer ||
                    cc_result.noOpenEventsFoundInPreRegGroups ||
                    cc_result.samePasscodeError) {
                    invalidOuput = true;
                }

                if (!invalidOuput && result.privateAcesssAssignmentNotFound && result.eligiblePreRegGroupIds && result.eligiblePreRegGroupIds.length > 0) {
                    invalidOuput = !result.eligiblePreRegGroupIds.includes(cc_result.preReg.Early_Group1_Health__c) &&
                        !result.eligiblePreRegGroupIds.includes(cc_result.preReg.Early_Group_2_Access_Group__c) &&
                        !result.eligiblePreRegGroupIds.includes(cc_result.preReg.Pre_Registration_Group__c);
                    this.customErrorMessage = result.privateAcesssAssignmentNotFound;
                    this.template.querySelector('c-lwc-custom-toast').showCustomNotice();

                }

                //If can be scheduled using Pre Reg or Private Access Group
                if (!invalidOuput && cc_result.canSchedule) {

                    if (cc_result.privateAccessIDDoesNotMatchWithPreRegPrivateAccessAssignment) {
                        this.throwMessage(this.istesting, 'Sorry! You are not eligible for the selected event. Please choose a new Event or Override the passcode with Clinic Code.');
                    } else {
                        //If pending verification on test site then dont allow to move forward
                        if (this.istesting && cc_result.preRegMinDateTime) {
                            this.throwMessage(this.istesting, "Sorry, this contact is not qualified for this Event Location or Event Date. You will need to choose a new Event.");
                        } else {
                            const selectedEvent1 = new CustomEvent("mindatetime", {
                                bubbles: true,
                                composed: true,
                                detail: {
                                    preRegMinDateTime: cc_result.preRegMinDateTime,
                                    eventIds: cc_result.eventIds
                                }
                            });
                            this.dispatchEvent(selectedEvent1);

                            this.customErrorMessage = undefined;
                            actionParam.jsonData = JSON.stringify(this.dataObj);
                            actionParam.vaccineType = result.vaccineType;
                            actionParam.isTestingAndSelectedDoseCompleted = result.isTestingAndSelectedDoseCompleted;
                            const selectedEvent = new CustomEvent("setobject", {
                                detail: actionParam
                            });
                            this.dispatchEvent(selectedEvent);
                            this.modalVisible = false;
                        }
                    }
                } else {
                    if (cc_result.samePasscodeError) {
                        this.throwMessage(this.istesting, cc_result.samePasscodeError);
                    } else if ((cc_result.Pass_Code_Health__c === 'NOT_VALID' && cc_result.Pass_Code_Risk_Group__c === 'NOT_VALID')) {
                        this.throwMessage(this.istesting, 'Entered passcodes are Invalid');
                    } else if (cc_result.Pass_Code_Health__c === 'NOT_VALID') {
                        this.throwMessage(this.istesting, 'Invalid Health Passcode entered');
                    } else if (cc_result.Pass_Code_Risk_Group__c === 'NOT_VALID') {
                        this.throwMessage(this.istesting, 'Invalid Risk Group Passcode entered');
                    } else if (cc_result.invalidChronicAnswer) {
                        this.throwMessage(this.istesting, cc_result.invalidChronicAnswer);
                    } else if (cc_result.invalidOtherRiskGroupAnswer) {
                        this.throwMessage(this.istesting, cc_result.invalidOtherRiskGroupAnswer);
                    } else if (cc_result.preRegGroupNotActive) {
                        this.modalVisible = false;
                        this.throwMessage(this.istesting, cc_result.preRegGroupNotActive);
                        const preRegCmp = this.template.querySelector('c-vt_ts_pre-reg-info')
                        preRegCmp.contactId = cc_result.data.Id;
                        preRegCmp.doInit();
                    } else if (cc_result.noOpenEventsFoundInPreRegGroups) {
                        this.customErrorMessage = cc_result.noOpenEventsFoundInPreRegGroups;
                        this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
                    } else if (cc_result.ageRestriction) {
                        this.modalVisible = false;
                        if (!this.istesting) {
                            this.customErrorMessage = 'Because of your age, you are not yet eligible to get a COVID-19 vaccine. Visit the <a href="https://www.healthvermont.gov/covid-19/vaccine/about-covid-19-vaccines-vermont" target="_blank">Vermont Department of Health website</a> to get more information and find out who is eligible for a vaccine now.';
                            this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
                        } else {
                            showMessageWithLink(this, 'error', this.msgObjData['ageRestriction'], this.msgObjData['vermontMsgData']);

                            // this.dispatchEvent(new ShowToastEvent({
                            //     message: 'Because of your age, you are not yet eligible to get a COVID-19 vaccine. Visit the {0} to get more information and find out who is eligible for a vaccine now.',
                            //     variant: 'error',
                            //     messageData: [
                            //         {
                            //             url : 'https://www.healthvermont.gov/covid-19/vaccine/about-covid-19-vaccines-vermont',
                            //             label: 'Vermont Department of Health website'
                            //         }
                            //     ]
                            // }));
                        }
                    }
                }

                this.dataObj.emailCheckbox = _dataObj.emailCheckbox === true ? 'No' : 'Yes';
                this.dataObj.smsCheckbox = _dataObj.smsCheckbox === true ? 'No' : 'Yes';
                this.dataObj.eighteenPlus = _dataObj.eighteenPlus === true ? 'Yes' : 'No';
            })
            .catch(error => {
                let e = error.message || error.body.message;
                showAsyncErrorMessage(this, e);
                this.throwMessage(this.istesting, e);
                this.showSpinner = false;
            });
    }
    confirmationCancel() {
        this.overrideConfirmation = false;
    }

    cancelAlternateDose() {
        this.showAlternateClassVaccineModal = false;
        this.dispatchEvent(new CustomEvent('unlockvaccineevent', {
            bubbles: true,
            composed: true
        }));
    }

    confirmAlternateDose() {
        this.showAlternateClassVaccineModal = false;
        this.dispatchEvent(new CustomEvent('lockvaccineevent', {
            bubbles: true,
            composed: true
        }));
        this.createContactFunction(this.checkValidOutput, this.actionParamameters);
    }

    confirmOverride() {
        let _dataObj = {
            ...this.dataObj
        };
        _dataObj.emailCheckbox = this.dataObj.emailCheckbox === 'No' ? true : false;
        _dataObj.smsCheckbox = this.dataObj.smsCheckbox === 'No' ? true : false;
        _dataObj.eighteenPlus = this.dataObj.eighteenPlus === 'Yes' ? true : false;
        let actionParam = {
            jsonData: JSON.stringify(_dataObj),
            currentstep: this.currentStep
        };
        if (Object.keys(this.consentDetails).length !== 0 && this.consentDetails.constructor === Object) {
            actionParam.consentValue = this.consentDetails.consentvalue;
            actionParam.screeningvalue = this.consentDetails.screeningvalue;
        }
        const selectedEvent = new CustomEvent("setobject", {
            detail: actionParam
        });
        this.dispatchEvent(selectedEvent);
    }

    @api
    saveContacts(callback) {
        if (this.validate()) {
            let _dataObj = {
                ...this.dataObj
            };
            _dataObj.emailCheckbox = this.dataObj.emailCheckbox === 'No' ? true : false;
            _dataObj.smsCheckbox = this.dataObj.smsCheckbox === 'No' ? true : false;
            _dataObj.eighteenPlus = this.dataObj.eighteenPlus === 'Yes' ? true : false;
            let actionParam = {
                jsonData: JSON.stringify(_dataObj),
                currentstep: this.currentStep
            };
            if (Object.keys(this.consentDetails).length !== 0 && this.consentDetails.constructor === Object) {
                actionParam.consentValue = this.consentDetails.consentvalue;
                actionParam.screeningvalue = this.consentDetails.screeningvalue;
            }
            this.template.querySelector('c-vt_ts_pre-reg-info').setContactAge(this.dataObj.dob);
            const data = this.template.querySelector('c-vt_ts_pre-reg-info').getPreRegData();
            let tempPreRegData = JSON.parse(JSON.stringify(data.preRegData));
            delete tempPreRegData.Pre_Registration_Group__r;
            delete tempPreRegData.Early_Group1_Health__r;
            delete tempPreRegData.Early_Group_2_Access_Group__r;
            actionParam.preReg = tempPreRegData;
            this.preRegData = JSON.parse(JSON.stringify(data.preRegData));

            if (!data.success) {
                this.modalVisible = false;

                this.dispatchEvent(new ShowToastEvent({
                    variant: 'error',
                    message: data.msg
                }));
            } else {
                if (!this.modalVisible && !(this.preRegData.Id) &&
                    (
                        // this.preRegData.Are_you_eligible_for_Vaccine_Risk_Group__c === 'Yes' ||
                        this.preRegData.Chronic_Condition__c === 'Yes' ||
                        this.preRegData.Affiliated_to_any_other_Risk_Group__c === 'Yes')) {
                    this.template.querySelector('c-vt_ts_pre-reg-info').confirmationModal = true;
                    setTimeout(() => {
                        this.template.querySelector('c-vt_ts_pre-reg-info').focusFirstEle();
                    }, 200);
                    this.modalVisible = true;
                } else if (!this.modalVisible &&
                    this.preRegData.Id &&
                    (
                        // (this.preRegData.Are_you_eligible_for_Vaccine_Risk_Group__c === 'Yes' &&
                        //     data.oldPreRegData.Are_you_eligible_for_Vaccine_Risk_Group__c == 'No') ||

                        (this.preRegData.Chronic_Condition__c === 'Yes' &&
                            data.oldPreRegData.Chronic_Condition__c === 'No') ||

                        (this.preRegData.Affiliated_to_any_other_Risk_Group__c === 'Yes' &&
                            data.oldPreRegData.Affiliated_to_any_other_Risk_Group__c === 'No')
                    )
                ) {
                    this.template.querySelector('c-vt_ts_pre-reg-info').confirmationModal = true;
                    setTimeout(() => {
                        this.template.querySelector('c-vt_ts_pre-reg-info').focusFirstEle();
                    }, 200);
                    this.modalVisible = true;
                } else {
                    this.showSpinner = true;
                    this.saveHelperCovidPortal(actionParam, callback);
                }
            }
        } else {
            this.modalVisible = false;

            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please complete required fields',
            }));
        }
    }

    modalCancellationHandler() {
        this.modalVisible = false;
    }

    saveHelperCovidPortal(actionParam, callback) {
        saveContactDetails(actionParam).then(result => {
            this.showSpinner = false;
            if (result.Pass_Code_Health__c === 'NOT_VALID' && result.Pass_Code_Risk_Group__c === 'NOT_VALID') {
                this.showToast('error', 'Entered passcodes are Invalid');
            } else if (result.Pass_Code_Health__c === 'NOT_VALID') {
                this.showToast('error', 'Invalid Health Passcode entered');
            } else if (result.Pass_Code_Risk_Group__c === 'NOT_VALID') {
                this.showToast('error', 'Invalid Risk Group Passcode entered');
            } else if (result.invalidChronicAnswer) {
                this.showToast('error', result.invalidChronicAnswer);
            } else if (result.invalidOtherRiskGroupAnswer) {
                this.showToast('error', result.invalidOtherRiskGroupAnswer);
            } else if (result.preRegGroupNotActive) {
                this.showToast('error', result.preRegGroupNotActive);
                callback();
            } else if (result.noOpenEventsFoundInPreRegGroups) {
                this.showToast('error', result.noOpenEventsFoundInPreRegGroups);
            } else {
                this.disableInputs = true;
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: (this.contactId ? 'Saved ' : 'Added ') + 'successfully!',
                }));
                callback();
            }
        }).catch(error => {
            this.template.querySelector('c-vt_ts_pre-reg-info').confirmationModal = false;
            showAsyncErrorMessage(this, error);
            this.dispatchEvent(new ShowToastEvent({
                variant: 'error',
                message: error.body.message,
            }));
            this.showSpinner = false;
        });
    }

    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc disable popup if user already registered
     */
    alreadyRegisteredHandler(event) {
        this.preRegData = event.detail;
    }

    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc runs when user gives confirmation on non-editability of pre-reg record (Covid Portal)
     */
    preRegConfirmationHandler() {
        this.showSpinner = true;
        this.saveContacts(() => {
            this.dispatchEvent(new CustomEvent('preregsave'));
        });
        this.showSpinner = false;
    }

    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc pop-up confirmation for schedular
     */
    preRegConfirmationHandlerSchedular() {
        this.getContactObj();
    }

    validate() {
        let isValid = true;

        if (!this.isValid()) {
            isValid = false;
        }

        if (!this.dataObj.zip) {
            this.dataObj.zip = null;
            isValid = false;
        }

        if (!this.dataObj.registrationForAgree) {
            isValid = false;
        }

        if (this.dataObj.emailCheckbox != 'Yes' && this.dataObj.smsCheckbox != 'Yes') {
            let tempObj = {};
            tempObj.message = "Please Select atleast one way to recieve notifications.";
            tempObj.status = "error";
            //this.sendEventToParent(tempObj);
            // this.selectOneCommunicationError = true;
            // isValid = false;   I-46322
        }

        if (this.dataObj.smsCheckbox === true && !this.dataObj.phone) {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please Complete the phone field to recieve SMS notifications',
            }));
            isValid = false;
        }

        if (this.dataObj.state === '' || this.dataObj.state === undefined || this.dataObj.state === null) {
            isValid = false;
        }

        if (this.dataObj.physicalState === '' || this.dataObj.physicalState === undefined || this.dataObj.physicalState === null) {
            isValid = false;
        }

        if (!this.validateTextField(this.dataObj.lastName)) {
            isValid = false;
        }

        if (!this.validateTextField(this.dataObj.phone)) {
            isValid = false;
        }

        if (!this.validateTextField(this.dataObj.email)) {
            isValid = false;
        }

        if (!this.validateTextField(this.dataObj.address1)) {
            isValid = false;
        }

        if (!this.validateTextField(this.dataObj.city)) {
            isValid = false;
        }

        if (!this.validateTextField(this.dataObj.physicalAddress1)) {
            isValid = false;
        }

        if (!this.validateTextField(this.dataObj.physicalCity)) {
            isValid = false;
        }

        if (isValid && !this.validateEmail(this.dataObj.email)) {
            isValid = false;
        }

        if (isValid && !this.validatePhone(this.dataObj.phone)) {
            isValid = false;
        }

        if (this.dataObj.zip) {
            if (isValid && !this.validateZip(this.dataObj.zip)) {
                isValid = false;
            }
        }

        if (this.dataObj.physicalZip) {
            if (isValid && !this.validateZip(this.dataObj.physicalZip)) {
                isValid = false;
            }
        }

        if (!this.disabledFirstNameLastNameDOB) {
            if (!this.template.querySelector('c-vt_ts_custom_date').isValid()) {
                isValid = false;
            }
        }

        return isValid;
    }

    @api
    handleCancelButton() {
        this.disableInputs = true;
        // this.dataObj = {...this.extraWrapper};
    }

    calculateAge(date) {
        let enteredData = date;
        let enteredDataArray = enteredData.split('-');
        let enteredDataYear = parseInt(enteredDataArray[0]);
        let enteredDataMonth = parseInt(enteredDataArray[1]) - 1;
        let enteredDataDate = parseInt(enteredDataArray[2]);
        let todayDate = new Date();
        let initialAge = todayDate.getFullYear() - enteredDataYear;
        if (enteredDataMonth < todayDate.getMonth()) {
            this.age = initialAge;
        } else if (enteredDataMonth > todayDate.getMonth()) {
            initialAge--;
            this.age = initialAge;
        } else if (enteredDataDate > todayDate.getDate()) {
            initialAge--;
            this.age = initialAge;
        } else {
            this.age = initialAge;
        }
    }

    calculateAge_Deprecated(date) {
        // var Bday = new Date(date);
        let today = new Date();
        let dateArray = date.split('-');
        let isLeepYear = (parseInt(dateArray[0]) % 4) === 0;
        var Bday = new Date(parseInt(dateArray[0]), parseInt(dateArray[1]) - 1, parseInt(dateArray[2]), 0, 0, 0);
        this.age = ~~((new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0) - Bday) / (31557600000));

        if (isLeepYear) {
            this.age += 1;
        }

        if (this.age < 18) {
            this.ageLessThan18 = true;
        } else {
            this.ageLessThan18 = false;
        }
    }

    sendEventToParent(obj) {
        const selectedEvent = new CustomEvent("contactsave", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }

    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }

    validateTextField(field) {
        if (field === '' || typeof field === 'undefined') {
            let tempObj = {};
            tempObj.message = "Please complete the required fields.";
            tempObj.status = "error";
            return false;
        }
        return true;
    }

    validateEmail(email) {
        // var regExpEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;
        // var isValid = email.match(regExpEmail);
        if (!email) {
            let tempObj = {};
            tempObj.message = "Please provide a correct format for email.";
            tempObj.status = "error";
            //this.sendEventToParent(tempObj);
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please provide a correct format for email',
            }));
            return false;
        }
        return true;
    }

    validatePhone(phone) {
        var regExpPhone = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
        var isValid = phone.match(regExpPhone);
        if (!isValid) {
            let tempObj = {};
            tempObj.message = "Please provide a correct format for phone.";
            tempObj.status = "error";
            //this.sendEventToParent(tempObj);
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please provide a correct format for phone',
            }));
        }
        return isValid;
    }

    validateDate(date) {
        var regExpPhone = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
        var isValid = date.match(regExpPhone);
        if (!isValid) {
            let tempObj = {};
            tempObj.message = "Please provide a valid format for date";
            tempObj.status = "error";
            //this.sendEventToParent(tempObj);
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please provide a valid format for date',
            }));
        }
        return isValid;
    }

    validateZip(zip) {
        var regExpZip = /^[0-9]*$/;
        var isValid = (zip.match(regExpZip) && zip.length === 5);
        if (!isValid) {
            let tempObj = {};
            tempObj.message = 'Please enter a valid format ("XXXXX") for Zip code.';
            tempObj.status = "error";
            //this.sendEventToParent(tempObj);
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please enter a valid format ("XXXXX") for Zip code',
            }));
        }
        return isValid;
    }

    isValid() {
        let valid = true;
        let isAllValid = [...this.template.querySelectorAll("lightning-input, lightning-combobox")]
            .reduce((validSoFar, input) => {
                input.reportValidity();
                return validSoFar && input.checkValidity();
            }, true);

        if (!isAllValid) {
            let allInputs = [...this.template.querySelectorAll("lightning-input, lightning-combobox")]
            for (let index = 0; index < allInputs.length; index++) {
                const element = allInputs[index];
                if (!element.checkValidity()) {
                    element.focus();
                    break;
                }
            }
        }
        valid = isAllValid;
        return valid;
    }

    handlePhoneFocus(event) {
        event.target.value = this.dataObj.phone;
    }

    formatPhone(obj) {
        var numbers = obj.value.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj.value = "";
        for (var i = 0; i < numbers.length; i++) {
            obj.value += (char[i] || "") + numbers[i];
        }
    }

    formatPhoneInput(obj) {
        var numbers = obj.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj = "";
        for (var i = 0; i < numbers.length; i++) {
            obj += (char[i] || "") + numbers[i];
        }
        return obj;

    }

    openContactSearch() {
        this.openContactSearchWindow = true;
    }

    openGuardianSearch() {
        this.openGuardianSearchWindow = true;
    }

    closeGuardianSearch() {
        this.openGuardianSearchWindow = undefined;
    }

    closeContactSearch() {
        this.openContactSearchWindow = false;
    }

    getValuesForSelectedContact(event) {
        this.showExistingContactSelection = true;
        this.openContactSearchWindow = false;
        this.existingContactId = event.detail[0].Id;
        this.existingContactName = event.detail[0].FirstName + ' ' + event.detail[0].LastName;
        this.dataObj.existingContactId = event.detail[0].Id;
        this.dataObj.existingContactName = event.detail[0].FirstName + ' ' + event.detail[0].LastName;

        if (event.detail[0].hasOwnProperty('Parent_Contact__r')) {
            this.dataObj.parentContactId = event.detail[0].Parent_Contact__r.Id;
            this.dataObj.parentContactName = event.detail[0].Parent_Contact__r.Name;
            this.parentContactName = event.detail[0].Parent_Contact__r.Name;
            this.parentContactId = event.detail[0].Parent_Contact__r.Id;
            this.initialGuardian = true;
        } else {
            this.dataObj.parentContactId = null;
            this.dataObj.parentContactName = null;
            this.parentContactName = null;
            this.parentContactId = null;
            this.initialGuardian = null;
        }

        if (event.detail[0].hasOwnProperty('Contacts__r')) {
            this.showExistingContactSelection = false;
        }
        this.getExistingContactDetail();
    }

    getValuesForSelectedGuardian(event) {
        this.parentContactId = event.detail[0].Id;
        this.parentContactName = event.detail[0].FirstName + ' ' + event.detail[0].LastName;
        this.dataObj.parentContactId = event.detail[0].Id;
        this.dataObj.parentContactName = event.detail[0].FirstName + ' ' + event.detail[0].LastName;
        this.openGuardianSearchWindow = false;
    }

    removeExistingContactSelection() {
        this.showExistingContactSelection = true;
        this.existingContactName = undefined;
        this.existingContactId = undefined;
        this.dataObj = {};
        this.removeParentContactSelection();
        this.age = undefined;
        let preRegCmp = this.template.querySelector('c-vt_ts_pre-reg-info');
        if (preRegCmp) {
            preRegCmp.contactId = undefined;
            preRegCmp.setAgeBasedGroup(undefined);
            preRegCmp.doInit();
        }

        this.template.querySelector('c-vt_ts_custom_date').setDobVal(undefined);
    }

    removeParentContactSelection() {
        this.parentContactId = undefined;
        this.parentContactName = undefined;
        delete this.dataObj.parentContactId;
        delete this.dataObj.parentContactName;
    }

    updateDOBField(event) {
        if (event.detail) {
            this.setAgeBasedGroup(this.dataObj.dob, event.detail);
            this.dataObj.dob = '';
            this.dataObj.dob = event.detail;
            this.calculateAge(event.detail);
        } else {
            this.dataObj.dob = '';
            this.age = 0;
        }
    }

    setAgeBasedGroup(oldValue, newValue) {
        if (oldValue !== newValue) {
            if (this.contactId) {
                this.dispatchEvent(new ShowToastEvent({
                    message: 'IMPORTANT! Changing your Date of Birth could impact your ability to schedule. Please make sure your Date of Birth is accurate before clicking SAVE.',
                    variant: 'warning'
                }));
            }
            const preRegCmp = this.template.querySelector('c-vt_ts_pre-reg-info');
            if (preRegCmp) {
                preRegCmp.setAgeBasedGroup(newValue);
            }
        }
    }

    showToast(type, msg) {
        this.dispatchEvent(new ShowToastEvent({
            message: msg,
            variant: type
        }));
    }

    preRegModalClose() {
        this.modalVisible = false;
    }

    focusTitle() {
        let focus = setInterval(() => {
            let cmp = this.template.querySelector('.pageTitle');
            if (cmp) {
                cmp.focus();
                setTimeout(() => {
                    this.showPageHeader = false;
                }, 1000);
                clearInterval(focus);
            }
        }, 200);
    }
}