import { LightningElement, track, api } from 'lwc';
import getBusinessTimeOnAccount from '@salesforce/apex/OkpcContactInformationController.getBusinessTimeOnAccount';
import getAccountSlotsWithEvent from '@salesforce/apex/DC_ScheduleAppointment.getAccountSlotsWithEvent';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
const ENTER = 13;
const SPACE = 32;

export default class OkpcCalendar extends LightningElement {
    @api eventId;
    @api preferredDate;
    @api preferredTime;
    @api slotId;
    @track recid;
    @track showSpinner;
    @track startTime;
    @track endTime;
    @track data = {};
    @track appointmentFrequency = 30;
    @api hidePrefferedTime = false;
    timeSlots = [];
    @track systemTime;
    @track todaysDate;
    @track todaysDateWithTime;
    @track currentYear;
    @track siteName;
    @track eventName;
    @api endDateFilter;
    @api startDateFilter;
    @api endTimeFilter;
    @api startTimeFilter;
    @track minDate;
    @track maxDate;

    currentSelRecordId = '';

    connectedCallback() {
        var now = new Date();
        now.setHours(now.getHours());
        var isPM = now.getHours() >= 12;
        var hr = now.getHours();
        var min = now.getMinutes();
        var isMidday = now.getHours() === 12;
        var time = [now.getHours() - (isPM && !isMidday ? 12 : 0),
        (now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes())
        ].join(':') +
            (isPM ? ' PM' : ' AM');
        this.systemTime = time;
        var dd = String(now.getDate()).padStart(2, '0');
        var mm = String(now.getMonth()).padStart(2, '0'); //January is 0!
        var yyyy = now.getFullYear();
        this.currentYear = yyyy;
        this.todaysDate = new Date(yyyy, mm, dd).getTime();
        this.todaysDateWithTime = new Date(yyyy, mm, dd, hr, min);
    }

    @api
    get accountid() {
        return this.recid;
    }
    set accountid(val) {
        this.recid = val;
        this.callApex();
    }

    callApex() {
        getBusinessTimeOnAccount({ selectedAccountId: this.recid })
            .then(result => {
                if (result != null) { 
                    this.startTime = this.msToTime(result.Business_Start_Time__c);
                    this.endTime = this.msToTime(result.Business_End_Time__c);
                    
                    if (result.Appointment_Frequency__c) {
                        this.appointmentFrequency = result.Appointment_Frequency__c;
                    }
                }
                this.retrieveRecords();
                this.showSpinner = false;
            })
            .catch(error => {
                this.showSpinner = false;
            })
    }

    msToTime(s) {
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;
        return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs) + '.' + this.pad(ms, 3);
    }
    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }


    retrieveRecords() {
        this.showSpinner = true;
        let currentTime = new Date();
        getAccountSlotsWithEvent({
            accountId: this.recid,
            eventId: this.eventId,
            endDateFilter : this.endDateFilter,
            startDateFilter : this.startDateFilter,
            startTimeFilter: this.startTimeFilter ,
            endTimeFilter: this.endTimeFilter
        }).then(result => {
            for (const key in result.records) {
                if( key.includes(new Date().toDateString().substr(0, 10)) ) {
                    for (const slot in result.records[key]) {
                        let slotHour = parseInt(slot.substr(0, 2));
                        if( slot.substr(0, 2) === '12' && slot.substr(6,2) === 'AM' ) {
                            slotHour = 0;
                        } else if( slot.substr(0, 2) === '12' && slot.substr(6,2) === 'PM' ) {
                            slotHour = 12;
                        } else if( slot.substr(6,2) === 'PM' && slot.substr(0, 2) !== '12' ) {
                            slotHour = parseInt(slot.substr(0,2))+12;
                        } else {
                            slotHour = parseInt(slot.substr(0, 2));
                        }
                        let slotMinute = slot.substr(3,2);
                        let slotParsedTime = new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), slotHour, slotMinute);
                        if( slotParsedTime < currentTime ) {
                            result.records[key][slot] = [];
                        }
                    }
                }
            }

            if(result.eventData != undefined && result.eventData.Location__c != undefined) {
                this.siteName = result.eventData.Location__r.Name;
            }
            if(result.eventData != undefined  && result.eventData.Start_Date__c != undefined && result.eventData.End_Date__c != undefined) {
                this.minDate = result.eventData.Start_Date__c;
                this.maxDate = result.eventData.End_Date__c;
                this.eventName = (result.eventData.Description__c === undefined? "":result.eventData.Description__c) + " ("+result.eventData.Start_Date__c+" to "+result.eventData.End_Date__c+")";
            }
            this.createCalendar(result);
            this.showSpinner = false;
        })
		.catch(error => {
			this.showSpinner = false;
			this.error = error;
		});
    }

    createTimeSlots() {
        let minhr = 0;
        let maxhr = 23;
        let minmnt = 0;
        let maxmnt = 60;
        if (this.startTime) {
            let timeObj = this.getFormattedTime(this.startTime);
            minhr = timeObj.hourT;
            minmnt = timeObj.minT;
        }
        if (this.endTime) {
            let timeObj = this.getFormattedTime(this.endTime);
            maxmnt = timeObj.minT;
            maxhr = timeObj.hourT;
        }

        let timeInterval = this.appointmentFrequency;
        for (let hr = parseInt(minhr); hr <= parseInt(maxhr); hr++) {
            for (let mnt = 0; mnt < 60; mnt = mnt + parseInt(timeInterval)) {
                if (((parseInt(minmnt) <= parseInt(mnt) || parseInt(hr) > parseInt(minhr)) && parseInt(hr) != parseInt(maxhr)) ||
                    (parseInt(hr) === parseInt(maxhr) && parseInt(maxmnt) > parseInt(mnt))) {
                    let timeStr = this.generateTimeOption(hr, mnt);
                    this.timeSlots.push(timeStr);
                    this.addSlots(timeStr);
                }
            }
        }
    }

    generateTimeOption(hr, mnt) {
        let radian = hr < 12 ? 'AM' : 'PM';
        let timehr = hr <= 12 ? (hr === 0 ? 12 : hr) : hr - 12;
        let minute = mnt < 10 ? '0' + mnt : '' + mnt;
        return (timehr < 10 ? '0' + timehr : timehr) + ':' + minute + ' ' + radian;
    }

    getFormattedTime(timeInstance) {
        let hourT = parseInt(timeInstance.substr(0, 2));
        let minT = parseInt(timeInstance.substr(3, 2));
        let amPM = hourT <= 11 ? 'AM' : 'PM';
        return { hourT: hourT, minT: minT, amPM: amPM };
    }

    createCalendar(result) {

        this.data = {};
        this.data.rows = [];
        this.data.dates = result.dateHeaderList;
        this.data.weeks = result.weekHeaderList;
        this.createTimeSlots();
        let columnCount = 0;
        for (var m in result.records) {
            let recds = result.records[m];
            let rowCount = 0;
            this.timeSlots.forEach(element => {
                if (typeof recds[element] != 'undefined' && recds[element] !== null && recds[element].length > 0 && this.data && this.data.rows[rowCount]) {
                    let slotString = recds[element].length > 1 ? 'slots' : 'slot';
                    this.data.rows[rowCount].cols[columnCount].isAvailable = true;
                    this.data.rows[rowCount].cols[columnCount].availableSlots = recds[element].length;
                    this.data.rows[rowCount].cols[columnCount].slotId = recds[element][0].Id;
                    this.data.rows[rowCount].cols[columnCount].ariaLabel = 'Available '+recds[element].length+' '+slotString+' at '+this.data.rows[rowCount].cols[columnCount].slotDate+' '+this.data.rows[rowCount].timeSt+ ', click space to select';
                    this.data.rows[rowCount].cols[columnCount].selectedariaLabel = 'Selected slot at '+this.data.rows[rowCount].cols[columnCount].slotDate+' '+this.data.rows[rowCount].timeSt;
                    if(this.slotId && recds[element][0].Id === this.slotId) {
                        this.data.rows[rowCount].cols[columnCount].isSelected = true;
                    }
                }
                rowCount++;
            });

            columnCount++;
        }
        this.showSpinner = false;
    }

    bulkSlot(time, amPM) {
        for (let i = 0; i < 2; i++) {
            let timeStr = time + ':' + (i * 30) + (i === 0 ? '0' : '') + ' ' + amPM;
            if (time < 10)
                timeStr = '0' + timeStr;
            this.timeSlots.push(timeStr);
            for (let i = 0; i < this.data.dates.length; i++)
                this.addSlots(timeStr);
        }
    }

    addSlots(timeStr) {
        let cols = [];
        var numOfSlots = this.data.dates.length;

        for (var i = 0; i < numOfSlots; i++) {
            cols.push({
                availableSlots: 0,
                bookedSlots: 0,
                recId: '',
                slotDate: this.data.dates[i],
                newRecord: true,
                isAvailable: false,
                isSelected: false,
                slotId: this.makeid(18)
            });
        }
       

        this.data.rows.push({
            timeSt: timeStr,
            cols: cols
        });
    }

    keyHandleSelectedSlots(event){
        if (event.keyCode === ENTER || event.keyCode === SPACE) {
            this.handleSelectedSlots(event);
        }
    }

    handleSelectedSlots(event) {
        this.currentSelRecordId = event.target.dataset.recid;
        let tempSlotid = event.target.dataset.slotid;
        let slotTime = event.target.dataset.slottime;
        let todaysDate = '';
        let month = '';
        let year = this.currentYear;
        let day = '';
        let hours;
        let min = '';

        if (slotTime) {

            min = slotTime.substring(slotTime.indexOf(':') + 1, 5);
            hours = parseInt(slotTime.substring(0, slotTime.indexOf(':')));
            if (slotTime.substring(6, 8) === 'PM' && hours < 12) {
                hours = 12 + parseInt(slotTime.substring(0, slotTime.indexOf(':')));
            } else if (slotTime.substring(6, 8) === 'PM' && hours >= 12) {
                hours = 12;
            } else if (slotTime.substring(6, 8) === 'AM' && hours === 12) {
                hours = parseInt(slotTime.substring(0, slotTime.indexOf(':'))) - 12;
            } else {
                hours = parseInt(slotTime.substring(0, slotTime.indexOf(':')));
            }
        }

        for (let i = 0; i < this.data.rows.length; i++) {

            for (let j = 0; j < this.data.rows[i].cols.length; j++) {
                if (this.data.rows[i].cols[j].slotId === tempSlotid) {
                    this.data.rows[i].cols[j].isSelected = true;
                    todaysDate = this.data.dates[j];
                    month = todaysDate.substring(0, todaysDate.indexOf('/'));
                    day = todaysDate.substring(todaysDate.indexOf('/') + 1);

                }
                else {
                    if (this.data.rows[i].cols[j].isAvailable) {
                        this.data.rows[i].cols[j].isSelected = false;
                    }
                }
            }
        }

        if (this.todaysDate === new Date(year, month, day).getTime()) {

            if (this.todaysDateWithTime > new Date(year, month, day, hours, min).getTime()) {

                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Please select time greater than current time',
                }));
                this.dispatchEvent(new CustomEvent('disableschedule', {
                    detail: this.currentSelRecordId
                }));
                return;
            } else {

                this.dispatchEvent(new CustomEvent('select', {
                    detail: this.currentSelRecordId
                }));
            }
        } else {
            this.dispatchEvent(new CustomEvent('select', {
                detail: this.currentSelRecordId
            }));
        }

    }

    makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
}