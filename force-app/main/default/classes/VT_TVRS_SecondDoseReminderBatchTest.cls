/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 10-04-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
@isTest
public class VT_TVRS_SecondDoseReminderBatchTest {
    
    @testSetup
    static void makeData(){
    	
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Vaccine';
        accountRecord.Event_Process__c = 'Vaccination';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Test_Center').getRecordTypeId();
        INSERT accountRecord;

        List<Contact> contList = new List<Contact>();
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'FName';
        contactRecord.LastName = 'LName';
        contactRecord.Birthdate = date.today()-10;
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord.Email = 'test@test01.com';
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Consent__c = true;
        contList.add(contactRecord);
        
        contactRecord = new Contact();
        contactRecord.FirstName = 'TFName';
        contactRecord.LastName = 'TLName';
        contactRecord.Birthdate = date.today()-10;
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord.Email = 'test@test02.com';
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Consent__c = true;
        contList.add(contactRecord);
        
        contactRecord = new Contact();
        contactRecord.FirstName = 'TestF';
        contactRecord.LastName = 'TestL';
        contactRecord.Birthdate = date.today()-10;
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord.Email = 'test@test03.com';
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Consent__c = true;
        contList.add(contactRecord);
        INSERT contList;
        
        List<Private_Access__c> privateAccessList = new List<Private_Access__c>();
        Private_Access__c privateAccess = new Private_Access__c(
        	Name = 'private test',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'Yes',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        privateAccessList.add(privateAccess);
        
        privateAccess = new Private_Access__c(
        	Name = 'private test2',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-2',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'Yes',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        privateAccessList.add(privateAccess);
        insert privateAccessList;
        
        List<VTS_Event__c> eventList = new List<VTS_Event__c>();
        VTS_Event__c event = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccessList[0].Id
        );
        eventList.add(event);
        
        event = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccessList[1].Id
        );
        eventList.add(event);
        insert eventList;
        
        List<Appointment__c> appList = new List<Appointment__c>();
        Appointment__c appointment = new Appointment__c(
        	Patient__c = contList[0].Id,
            Status__c = 'Completed',
            Event__c = eventList[0].Id,
            Lab_Center__c = accountRecord.Id,
            Appointment_Complete_Date__c = date.today() - 2,
            Second_Dose_Reminder_Completed__c = false
        );
        appList.add(appointment);
        
        appointment = new Appointment__c(
        	Patient__c = contList[2].Id,
            Status__c = 'Completed',
            Event__c = eventList[0].Id,
            Lab_Center__c = accountRecord.Id,
            Appointment_Complete_Date__c = date.today(),
            Second_Dose_Reminder_Completed__c = false
        );
        appList.add(appointment);
        
        appointment = new Appointment__c(
        	Patient__c = contList[1].Id,
            Status__c = 'Completed',
            Event__c = eventList[0].Id,
            Lab_Center__c = accountRecord.Id,
            Appointment_Complete_Date__c = date.today()-10,
            Second_Dose_Reminder_Completed__c = false
        );
        appList.add(appointment);
        
        appointment = new Appointment__c(
        	Patient__c = contList[1].Id,
            Status__c = 'Completed',
            Event__c = eventList[1].Id,
            Lab_Center__c = accountRecord.Id,
            Appointment_Complete_Date__c = date.today(),
            Second_Dose_Reminder_Completed__c = true
        );
        appList.add(appointment);
        insert appList;
    }
    
    @isTest
    private static void executeBatchTest(){
        Test.startTest();
        Database.executeBatch(new VT_TVRS_SecondDoseReminderBatch());
        Test.stopTest();
    }
    
    @isTest
    private static void schedulableTest(){
        VT_TVRS_SecondDoseReminderBatch sh1 = new VT_TVRS_SecondDoseReminderBatch();
		String sch = '0 0 2 1/1 * ? *'; 
        system.schedule('Schedule Reminder', sch, sh1);
    }
}