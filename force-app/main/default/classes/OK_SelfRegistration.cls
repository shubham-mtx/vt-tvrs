/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 01-21-2022
 * @last modified by  : Mohit Karani
**/
public without sharing class OK_SelfRegistration {
    public static final String RT_CITIZEN_COVID = 'Citizen_COVID';
    public static final String RT_CITIZEN_LOBBY_VITAL = 'Citizen_Lobby_Vital_Records';
    public static final String REG_TYPE_CITIZEN_COVID = 'Citizen_COVID';
    public static final String REG_TYPE_VR_CITIZEN_LOBBY = 'Citizen_Lobby_Vital_Records';
    public static final String REG_TYPE_VR_CITIZEN_CONSULTATION = 'Citizen_Consultation_Vital_Records';
    static Id userId = UserInfo.getUserId();
    private static Map < String,Schema.RecordTypeInfo > recordTypeInfosMap = Account.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;
    public static final String EMAIL_STRING = '@okhla.com';
    private static Map < String,Schema.RecordTypeInfo > recordTypeInfosMapContact = Contact.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;

    @AuraEnabled
    public static Map < String, Object > selfRegisterContact(String firstName, String lastName, String phone, String email, String type,String eventId, Date dob, String captchaResponse) {
        Map < String, Object > result = new Map < String, Object > ();
        if(!RecaptchaEnterpriseController.verifyByNamedCredentials(captchaResponse, 'AHS_TVRS')) {
            throw new VT_Exception('Please confirm the recaptcha');
        }
        List < contact > existingCon = [SELECT ID, Name, Email FROM Contact 
                                        WHERE Email =: email AND Notify_Patients__c = TRUE AND 
                                        RecordTypeId =: recordTypeInfosMapContact.get(RT_CITIZEN_COVID).RecordTypeId];
        String userName = email + '.covid';
        List < user > existingUser = [SELECT ID, Name, Email from USER Where username =: userName];
        
        if (existingUser.size() > 0) {
            result.put('message', 'You Have Already Registered in the Vermont Event Registration Community, Please Click the Link Above to Access the Login Page');
            result.put('type', 'error');
            return result;
        }
        
        if (existingCon.size() > 0) {
            result.put('message', 'You Have Already Registered in the Vermont Event Registration Community, Please Check You Email Inbox for Your Patient ID');
            result.put('type', 'error');
            return result;
        }
        
        contact con = new contact();
        con.FirstName = firstName;
        con.LastName = lastName;
        con.MobilePhone = phone;
        con.Email = email;
        con.Birthdate = dob;

        if (type == System.Label.OKSF_Registration_Covid_Citizen) {
            con.AccountId = System.Label.Self_Register_Account_Id;
            con.RecordTypeId = recordTypeInfosMapContact.get(RT_CITIZEN_COVID).RecordTypeId;
            con.Private_Event_Register_Id__c = eventId != null ? eventId : '';
        }
        
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true;
        // List<Contact> contactsToInsert = VT_SecurityLibrary.getAccessibleData('Contact', new List<Contact>{con}, 'insert');
        Database.insert(con,dml);
        result.put('message',
                   'Thank you for providing your details for the COVID-19 Event Portal. Please check your email for further instructions.');
        result.put('type', 'success');
        
        return result;
        
    }
    
  
    
    private static Contact createContact(Contact c, Contact_Metadata_Setting__mdt settings){
        c.AccountId = settings.Account_Id__c;
        c.RecordTypeId = recordTypeInfosMapContact.get(settings.Contact_RT_Developer_Name__c).RecordTypeId;
        
        return c;
    }
    
    @AuraEnabled
    public static Map < String, Object > createContactFromTestingSite(Contact c, String recordType){
        Map < String, Object > result = new Map < String, Object > ();
        String recordTypeId = recordTypeInfosMapContact.get(recordType).RecordTypeId; 
        List<Contact> contacts = [SELECT FirstName,LastName,Birthdate,Email FROM Contact 
                                WHERE RecordTypeId =: recordTypeId AND 
                                Unique_Reference__c =: VT_ContactTriggerHandler.buildUniqueReference(c.email, c.FirstName, c.LastName ) WITH SECURITY_ENFORCED];
        if(!contacts.isEmpty()){
            result.put('message', 'Contact with same details already exist');
            result.put('type', 'error');
            return result;
        }
        c.RecordTypeId = recordTypeId;
        c.AccountId = System.Label.Self_Register_Account_Id;
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true;
        List<Contact> contactsToInsert = VT_SecurityLibrary.getAccessibleData('Contact', new List<Contact>{c}, 'insert');
        Database.SaveResult saveResult = Database.insert(contactsToInsert[0],dml);
        if(saveResult.isSuccess()){
            result.put('message', 'Contact has been successfully created.');
            result.put('type', 'success');
            result.put('contactId', saveResult.getId());
        }else{
            result.put('message', saveResult.getErrors());
            result.put('type', 'fail');
            result.put('contactId', null);
        }
        
        return result;
    }
    
    /**
     * @description: Used on Testing Site Portal
     */
    @AuraEnabled
    public static Map<String, Object> createContactWithAppointments(Contact c, String recordType, String slotId, String eventId) {
        Map<String, Object> result = new Map<String, Object>();
        result = createContactFromTestingSite(c, recordType);
        
        String recordTypeId = recordTypeInfosMapContact.get(recordType).RecordTypeId;
        
        //Issue Start I-25856
        if(result.get('type') == 'error'){
            List<Contact> contacts = [SELECT Id,FirstName,LastName,Birthdate,Email FROM Contact 
                                    WHERE RecordTypeId =: recordTypeId AND 
                                    Unique_Reference__c =: VT_ContactTriggerHandler.buildUniqueReference(c.email, c.FirstName, c.LastName ) WITH SECURITY_ENFORCED];
            Appointment__c app = new Appointment__c();
            app.Appointment_Slot__c = ((slotId == '') ? null : Id.valueOf(slotId)) ;
            app.Event__c = Id.valueOf(eventId);
            app.Patient__c = contacts[0].Id;
            app.Lab_Center__c = c.Testing_Site__c;
            app.status__c = 'Scheduled';
            List<Appointment__c> appsToInsert = VT_SecurityLibrary.getAccessibleData('Appointment__c', new List<Appointment__c>{app}, 'insert');
            insert appsToInsert;
            result.put('message', 'Contact has been successfully created with appointment.');
            result.put('type', 'success');
            result.put('contactId', contacts[0].Id);
        } else {
            List<Appointment__c> appointmentList = new List<Appointment__c>();
            appointmentList = [SELECT Id FROM Appointment__c WHERE Patient__c = :(Id)result.get('contactId') WITH SECURITY_ENFORCED];
            if(!appointmentList.isEmpty()) {
                appointmentList[0].Appointment_Slot__c = ((slotId == '') ? null : Id.valueOf(slotId));
                appointmentList[0].Event__c = Id.valueOf(eventId);
                appointmentList[0].Lab_Center__c = c.Testing_Site__c;
                appointmentList[0].status__c = 'Scheduled';
            }
            else{
                Appointment__c app = new Appointment__c();
                app.Appointment_Slot__c = ((slotId == '') ? null : Id.valueOf(slotId)) ;
                app.Event__c = Id.valueOf(eventId);
                app.Patient__c = (Id)result.get('contactId');
                app.Lab_Center__c = c.Testing_Site__c;
                app.status__c = 'Scheduled';
                appointmentList.add(app);
            }
            upsert VT_SecurityLibrary.getAccessibleData('Appointment__c', appointmentList, 'upsert');
        }
        return result;
    }

    @AuraEnabled
    public static Map<String, Object> createContactWithAppointmentsFromTesting(Contact c, String recordType, String slotId, String eventId) {
        Map<String, Object> result = new Map<String, Object>();
        result = createContactFromTestingSite(c, recordType);
        //Issue Start I-25856
        
        String recordTypeId = recordTypeInfosMapContact.get(recordType).RecordTypeId;
        List<Appointment__c> appointmentList = new List<Appointment__c>();
        
        
        if(result.get('type') == 'error'){
            List<Contact> contacts = [SELECT Id,FirstName,LastName,Birthdate,Email FROM Contact 
                                    WHERE RecordTypeId =: recordTypeId AND 
                                    Unique_Reference__c =: VT_ContactTriggerHandler.buildUniqueReference(c.email, c.FirstName, c.LastName ) WITH SECURITY_ENFORCED];
            Appointment__c app = new Appointment__c();
            app.Appointment_Slot__c = ((slotId == '') ? null : Id.valueOf(slotId)) ;
            app.Event__c = Id.valueOf(eventId);
            app.Patient__c = contacts[0].Id;
            app.Lab_Center__c = c.Testing_Site__c;
            app.status__c = 'Completed';
            List<Appointment__c> appsToInsert = VT_SecurityLibrary.getAccessibleData('Appointment__c', new List<Appointment__c>{app}, 'insert');
            insert appsToInsert;
            appointmentList.add(app);
            result.put('message', 'Contact has been successfully created with appointment.');
            result.put('type', 'success');
            result.put('contactId', contacts[0].Id);
            //Issue End I-25856
        }else{
            appointmentList = [SELECT Id FROM Appointment__c WHERE Patient__c = :(Id)result.get('contactId')];
            if(!appointmentList.isEmpty()) {
                appointmentList[0].Appointment_Slot__c = ((slotId == '') ? null : Id.valueOf(slotId));
                appointmentList[0].Event__c = Id.valueOf(eventId);
                appointmentList[0].status__c = 'Completed';
                appointmentList[0].Lab_Center__c = c.Testing_Site__c;
                appointmentList[0].Appointment_Complete_Date__c = System.now();
            }
            else{
                Appointment__c app = new Appointment__c();
                app.Appointment_Slot__c = ((slotId == '') ? null : Id.valueOf(slotId)) ;
                app.Event__c = Id.valueOf(eventId);
                app.Patient__c = (Id)result.get('contactId');
                app.status__c = 'Completed';
                app.Appointment_Complete_Date__c = System.now();
                app.Lab_Center__c =  c.Testing_Site__c;
                appointmentList.add(app);
            }
            upsert VT_SecurityLibrary.getAccessibleData('Appointment__c', appointmentList, 'insert');
        }
        createTestingRecordOnAppointmentComplete(appointmentList[0].Id);
        return result;
    }
    public static String createTestingRecordOnAppointmentComplete(String appointmentId){
        try {
            VTTS_AppointmentHelper.checkUserAccess(null, appointmentId);
            Id antibodyRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c', 'Antibody_Testing');
            Id covidRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c', 'COVID_Testing');
            Id covidRecordTypeIdForContact = OKPCRecordTypeUtility.retrieveRecordTypeId('Contact', 'Citizen_COVID');
            Antibody_Testing__c antiBodyTesting = new Antibody_Testing__c();
            for (Appointment__c appointment : [SELECT Id, Status__c, Appointment_Slot__c,Patient__r.RecordTypeId, Patient__c, Patient__r.Testing_Site__c FROM Appointment__c WHERE Id = :appointmentId WITH SECURITY_ENFORCED]) {
                if (appointment.Patient__r.RecordTypeId == covidRecordTypeIdForContact) {
                    antiBodyTesting.RecordTypeId = covidRecordTypeId;
                } else {
                    antiBodyTesting.RecordTypeId = antibodyRecordTypeId;
                }
                
                antiBodyTesting.Appointment__c = appointment.Id;
                antiBodyTesting.Appointment_Slot__c = appointment.Appointment_Slot__c;
                antiBodyTesting.Patient__c = appointment.Patient__c;
                antiBodyTesting.Testing_Site__c = appointment.Patient__r.Testing_Site__c;
            }
            List<Antibody_Testing__c> testingResults = VT_SecurityLibrary.getAccessibleData('Antibody_Testing__c', new List<Antibody_Testing__c>{antiBodyTesting}, 'insert');
            insert testingResults;
            return 'success';
        } catch (DMLException ex) {
            return ex.getDMLMessage(0);
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
    
    public class ContactWrapper{
        
        @AuraEnabled public String firstName {get;set;}
        @AuraEnabled public String lastName {get;set;}
        @AuraEnabled public Date dob {get;set;}
        @AuraEnabled public String race {get;set;}
    }
    
}