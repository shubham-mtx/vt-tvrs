import {
    LightningElement,
    api,
    track
} from 'lwc';
import getContactAppointment from '@salesforce/apex/VT_TS_GetContactAppointment.getContactAppointment';
import checkExistingAppointmentFor2Dose from "@salesforce/apex/DC_ScheduleAppointment.checkExistingAppointmentForNextDose";

import {
    showMessage,
    showMessageWithLink,
    msgObj,
    createMessage,
    showAsyncErrorMessage,
    validateEmail
} from 'c/vtTsUtility';
export default class Vt_ts_scheduleSecondAppointment extends LightningElement {
    @api recordId;
    @track showModel;
    @track appointmentId;
    @track accountId;
    @track eventProcess;
    @track oldVaccineClass;
    @track isSecondDose;
    @track eventIds;
    @track schedule2ndDose = false;
    msgObjData = msgObj();
    isValidEmail
    connectedCallback() {
        getContactAppointment({
            recordId: this.recordId
        }).then(data => {
            if (data.appointment && data.appointment.length > 0) {
                this.schedule2ndDose = true;
                this.appointmentId = data.appointment[0].Id;
                this.accountId = data.appointment[0].Event__r.Location__c;
                this.eventProcess = 'Vaccination';
                this.oldVaccineClass = data.appointment[0].Event__r.Private_Access__r.Vaccine_Class_Name__c;
                this.isSecondDose = true;
                this.isValidEmail = (data.appointment[0].Patient__r.Email  && validateEmail(data.appointment[0].Patient__r.Email));
            }
        }).catch(error => {
            showAsyncErrorMessage(this, error);
        });
    }

    openModel() {
            checkExistingAppointmentFor2Dose({
                    appointmentId: this.appointmentId
                })
                .then(result => {
                    if (result.furtherDosesNotReqd) {
                        showMessageWithLink(this, 'error', this.msgObjData['furtherDosesNotReqd'], this.msgObjData['vermontMsgData']);
                    } else if (result.doseNumber) {
                        createMessage(result.doseNumber, this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyScheduled']);
                    } else if (result.isSelectedDoseAlreadyScheduled) {
                        showMessage(this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyScheduled']);
                    } else if (result.isSelectedDoseAlreadyCompleted) {
                        showMessage(this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyCompleted']);
                    } else if (result.ageRestriction) {
                        showMessageWithLink(this, 'error', this.msgObjData['ageRestriction'], this.msgObjData['vermontMsgData']);
                    } else if (result.isImmuneWeakNotAnsered) {
                        showMessage(this, 'Error!', 'error', result.isImmuneWeakNotAnsered);
                    } else if (result.preRegGroupNotActive) {
                        showMessage(this, 'Error!', 'error', result.preRegGroupNotActive);
                    } else if (result.noOpenEventsFoundInPreRegGroups) {
                        showMessageWithLink(this, 'error', this.msgObjData['noPrivateAccessAssignmentFound'], this.msgObjData['noPrivateAccessAssignmentFoundUrl']);
                    } else {
                        this.showModel = true;
                        this.eventIds = result.eventIds;
                    }
                })
                .catch(error => {
                    showAsyncErrorMessage(this, error);
                });

    }

    hideModal() {
        this.showModel = false;
    }
}