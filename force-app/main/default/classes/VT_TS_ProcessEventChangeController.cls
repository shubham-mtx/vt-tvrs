public with sharing class VT_TS_ProcessEventChangeController {
    @AuraEnabled
    public static Map<String, Object> updateEvent(Map<String, Id> data){
        //try {
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                VTS_Event__c event = new VTS_Event__c();
                event.Id = data.get('eventId');
                event.Location__c = data.get('newLocationId');
                VT_TS_SkipTriggerExecution.executeEventChangeLogicFromLayout = false;

                //security check
                event = (VTS_Event__c)VT_SecurityLibrary.getAccessibleData('VTS_Event__c', new list<VTS_Event__c>{event}, 'update')[0];

                update event;
                Database.executeBatch(new VT_TS_ProcessEventChangeBatch(data, false));
                return new Map<String, Object> {'success' => true};
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
            
        //} catch (Exception e) {
         //   throw new AuraHandledException(e.getMessage());
        //}
    }

    @AuraEnabled
    public static List<VTS_Event__c> getMatchingEventsOfNewLocation(Map<String, Object> inputData, Date eventStartDate, Date eventEndDate){
        try {
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                if(eventStartDate < System.today()) {
                    throw new VT_Exception('You can\'t change the location of past events');
                }
                return [SELECT Id, Name FROM VTS_Event__c 
                        WHERE Start_Date__c >=:eventStartDate 
                        AND End_Date__c <=:eventEndDate 
                        AND Location__c =:String.valueOf(inputData.get('newLocationId')) WITH SECURITY_ENFORCED LIMIT 1];
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}