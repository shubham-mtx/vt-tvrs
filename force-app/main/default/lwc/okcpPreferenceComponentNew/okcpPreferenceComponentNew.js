import {
    LightningElement,
    api,
    track
} from 'lwc';
import getSiteAccounts from '@salesforce/apex/DC_ScheduleAppointment.getSiteAccounts';
// import getAccountSlots from '@salesforce/apex/DC_ScheduleAppointment.getAccountSlots';
// import updateAppointmentrec from '@salesforce/apex/DC_ScheduleAppointment.updateAppointment';
// import getSelectedTestingSiterec from '@salesforce/apex/DC_ScheduleAppointment.getSelectedTestingSite';
// import createAppointmentrec from '@salesforce/apex/DC_ScheduleAppointment.createAppointment';
// import getAppointmentDetailrec from '@salesforce/apex/DC_ScheduleAppointment.getAppointmentDetail';
import getPreferredValues from '@salesforce/apex/OkpcContactInformationController.getPreferredValuesForContact';
import setPreferredValues from '@salesforce/apex/OkpcContactInformationController.setPreferredValues';
// import getAccountdetails from '@salesforce/apex/OkpcContactInformationController.getAccountdetails';
import getContactDetails from "@salesforce/apex/OkpcContactInformationController.getContactDetails";
import getAccountdetails from '@salesforce/apex/OKPC_LocationMapController.getAccountdetails';
import getAppointmentDetail from '@salesforce/apex/OKPC_IntakeRequestController.getAppointmentDataForContact';
import saveAppointmentDetail from '@salesforce/apex/OKPC_IntakeRequestController.saveAppointMentData';
import getObjectName from '@salesforce/apex/DC_ScheduleAppointment.getObjectName';

import getAppointmentDetails from '@salesforce/apex/OkpcContactInformationController.getAppointmentDetails';

import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent'

export default class OkpcPreferenceComponent extends LightningElement {
    // map = new map();
    @api eventId;
    @api privateEventId;
    @api vaccinationPrivateAccess;
    @api recordid;
    @api ifcovid = false;
    @api hideAppointment = false;
    @track options = [];
    @track selectedOption;
    @track isAttributeRequired = false;
    @api fieldName;
    @track contactid;
    @api objectName;
    @api istesting;
    @api preRegDateTime;
    @track fieldLabelName;
    @track accountList = [];
    @track isSiteSelected = false;
    @track selectValue = null;
    @track appointmentId = null;
    @track selectSiteName;
    @track slotId;
    @track showSchedule = false;
    @track canSchedule = false;
    @track showMapModal = false;
    @track showCalendar = false;
    @track preferredDate = null;
    @track preferredTime = null;
    @track testingSite;
    @track showtestingSite = false;
    @track accountDetails = {};
    @track showLocationDetails = false;
    @track showSpinner = false;
    @track showSingleMap = false;
    @track mapMarkers = [];
    @track location = {};
    @track openModal = false;
    @track todayDate;
    @track preferredTimeMin = null;
    @track preferredTimeMax = null;
    @api showpickfromdashboard = false;
    @track symptomatic = '';
    @track showHelpText = false;
    @track isModalOpen = false;
    @track appData = {};
    map = new Map();
    @api selectedEventId = '';
    @api dependentContactId;
    @track privateEvent;
    @track privateGroupName;
    eventProcess;
    showPageHeader = true;
    secondDoseData = {};

    get isScheduleAppointmentDisabled() {
        return !this.eventId;
    }

    openModal() {
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
    }

    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        let valid = false;
        //     this.showSpinner = true;

        valid = [...this.template.querySelectorAll("lightning-radio-group")].reduce(
            (validSoFar, input) => {
                input.reportValidity();
                return validSoFar && input.checkValidity();
            },
            true
        );
        if (valid) {
            this.isModalOpen = false;
            this.showCalendar = true;
            this.showSpinner = true;
            this.appData.appointmentObj.symptomatic = this.symptomatic;
            saveAppointmentDetail(
                {
                    JsonData: JSON.stringify(this.appData.appointmentObj),
                    appointMentId: this.appointmentId
                }
            ).then(result => {
                this.appData = result;
                this.showSpinner = false;

            }).catch(error => {
                console.log('error in data ', JSON.stringify(error));
                this.showSpinner = false;

            })
        }
    }

    connectedCallback() {
        this.ifcovid = true;
        this.todayDate = this.getTodayDate();
        this.secondDoseData.contactId = this.dependentContactId;
        //If Vaccination then setup private access group details
        if( this.vaccinationPrivateAccess ){
            this.retrieveTestingSites();
            this.getContact();
            this.eventProcess = 'Vaccination';
            if(!this.eventId) {
                this.showMapModal = true;
            }
        }
        //If Private access group is for Testing Site
        else if( this.privateEventId ){
            getObjectName({recordId : this.privateEventId}).then(result => {
                this.retrieveTestingSites();
                this.getContact();
                if(result.objName === 'Private_Access__c'){
                    this.privateGroupName = result.PrivateGroupName;
                    this.eventProcess = result.PrivateGroupType;
                    if(!this.eventId) {
                        this.showMapModal = true;
                    }
                }
            })
            .catch(error => {
                console.log('Erorr : ' + JSON.stringify(error));
            });
        }

        //If no Vaccination and no Testing Site group found
        if( !(this.vaccinationPrivateAccess) && !(this.privateEventId) ){
            this.retrieveTestingSites();
            this.getContact();
        }

        if( this.eventId ){
            this.getAppointment();
        }
        setTimeout(() => {
            this.focusTitle();
        }, 200);
    }

    getAppointment() {
        this.showSpinner = true;
        getAppointmentDetail({
            contactId: this.dependentContactId,
            eventId: this.eventId
        }).then(result => {
            this.appointmentId = result.appointmentId;
            this.appData = result;
            // this.symptomatic = this.appData.hasOwnProperty('appointmentObj')?this.appData.appointmentObj.symptomatic:'';
            this.symptomatic = this.appData.appointmentObj.symptomatic;
            if (this.symptomatic == 'Yes') {
                this.showHelpText = true;
            } else {
                this.showHelpText = false;
            }
            if(this.appointmentId) {
                this.getAppointmentDetailsFromAppointmentLab();
            }
        }).catch(error => {
            let message =  (error)?((error.message)?error.message:((error.body)?((error.body.message)?error.body.message:JSON.stringify(error)):JSON.stringify(error))):"Something went wrong!";
            console.log('error in data ' +  message);
        })
        this.showSpinner = false;
    }

    getContact() {
        this.showSpinner = true;
        getContactDetails({
            contactid: this.dependentContactId
        }).then(result => {
            this.canSchedule = result.canSchedule;
            this.showSpinner = false;

        }).catch(error => {
            console.log('error=>', JSON.stringify(error));
            this.showSpinner = false;
        })
    }


    getAppointmentDetailsFromAppointmentLab() {

		getAppointmentDetails({appointmentId : this.appointmentId})
		.then(result => {
		    this.eventId = result.Event__c;
		    this.selectValue = result.Lab_Center__c;

		   if (this.eventId) {
			   this.dispatchEvent(new CustomEvent("eventselected", {
				   detail: this.eventId
			   }));
		   }
		   if(this.selectValue) {
		       this.getAccount(this.selectValue);
     	   }

  		})
		.catch(error => {
			console.log('getAppointmentDetails errors : ' + error);
  		})


    }


	// previously used
    getValuesPreferred() {
        this.showSpinner = true;
        getPreferredValues({
            contactId: this.dependentContactId
        }).then(result => {
            this.preferredDate = result.Preferred_Date__c;

            if (result.Preferred_Time__c) {
                this.preferredTime = this.msToTime(result.Preferred_Time__c);
            }
            if (result.Testing_Site__c) {
                this.selectValue = result.Testing_Site__c;
                this.eventId = result.Registered_Event__c;
                if (this.eventId) {
                    this.dispatchEvent(new CustomEvent("eventselected", {
                        detail: this.eventId
                    }));
                }
                this.getAccount(this.selectValue);
                this.showtestingSite = true;
                if (this.istesting) {
                    this.showtestingSite = false;
                }
                this.showLocationDetails = true;

                if (this.showpickfromdashboard) {
                    this.showtestingSite = false;
                } else {
                    this.showtestingSite = true;
                }

            }
            this.showtestingSite = false;
            this.showSpinner = false;

        }).catch(error => {
            console.log('error in data ', JSON.stringify(error));
            this.showSpinner = false;
        })
    }

    @api
    setValues() {
        setPreferredValues({
            preferredDate: this.preferredDate,
            preferredTime: this.preferredTime,
            preferredSite: this.selectValue
        })
            .then(result => {
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Saved successfully',
                }));
                this.handleCloseModalonSave();
            })
            .catch(error => {
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Error',
                }));
                console.log('error ', JSON.stringify(error));
            })
    }
    handleInputChange(event) {
        if (event.target.name == 'preferredDate') {
            this.preferredDate = event.target.value;
        } else if (event.target.name == 'preferredTime') {
            this.preferredTime = event.target.value;
        }
    }

    retrieveTestingSites() {
        getSiteAccounts({})
            .then(data => {
                data.forEach((account) => {
                    var optionValue = new Object();
                    optionValue.label = account.Name;
                    optionValue.value = account.Id;
                    this.accountList.push(optionValue);
                    this.map.set(account.Id, account.Name);
                });
                this.options = this.accountList;
            })
            .catch(error => {
                console.log('Error-->', error);
            });
    }

    selectionChangeHandler(event) {
        this.isSiteSelected = true;
        this.selectValue = event.target.value;
    }
    setId(event) {
        this.showSchedule = true;
        this.slotId = event.detail;
    }

    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    handleCloseModalonSave() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    justCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    handleMapModal() {
        this.showMapModal = true;
    }
    showAppointments() {
        //    this.openModal = true;
        this.isModalOpen = true;
        // this.showCalendar = true;
    }
    get Options() {
        return [
            { label: 'Yes', value: 'Yes' },
            { label: 'No', value: 'No' },
        ]
    }
    handleradiochange(event) {
        this.symptomatic = event.detail.value;
        this.appData.appointmentObj.symptomatic = event.detail.value;
        if (this.symptomatic == 'Yes') {
            this.showHelpText = true;
        } else {
            this.showHelpText = false;
        }
    }
    hideModal(event) {
        this.showCalendar = false;
        if (event.detail === 'save') {
            let tempObj = {};
            tempObj.name = "submit";
            const selectedEvent = new CustomEvent("submit", {
                detail: tempObj
            });
            this.dispatchEvent(selectedEvent);
        }
    }

    handleMapModalClose(event) {
        this.showMapModal = false;

        if (event.detail != 'close') {
            this.selectValue = event.detail
            this.canSchedule = true;
            this.getAccount(this.selectValue);

        }
        this.selectSiteName = this.map.get(this.selectValue);

        this.focusTitle();
    }

    msToTime(s) {
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;

        return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs) + '.' + this.pad(ms, 3);
    }
    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }

    getAccount(Id) {
        this.showSpinner = true;

        const selectedEvent = new CustomEvent("accountselected", {
            detail: Id
        });
        this.dispatchEvent(selectedEvent);

        getAccountdetails({
            accountId: Id
        })
        .then(result => {
            this.accountDetails = JSON.parse(JSON.stringify(result));
            this.location = result.location;
            if (this.accountDetails) {
                this.showLocationDetails = true;
                this.mapMarkers.length = 0;
                this.mapMarkers.push(this.accountDetails);
            }
            if (result.businessStartTime) {
                this.preferredTimeMin = this.msToTime(result.businessStartTime);
            }
            else {
                this.preferredTimeMin = '07:00:00.000Z';
            }
            if (result.businessEndTime) {
                this.preferredTimeMax = this.msToTime(result.businessEndTime);
            }
            else {
                this.preferredTimeMax = '19:00:00.000Z';
            }
            this.showSpinner = false;
        })
        .catch(error => {
            console.log('error : '+ JSON.stringify(error));
            this.showSpinner = false;
        })
    }




    handleSingleMap() {
        this.showSingleMap = true;
    }

    handlesingleMapClose() {
        this.showSingleMap = false;
    }

    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }

    handleEventSelected(event) {
        this.showPageHeader = true;
        this.focusTitle();
        this.eventId = event.detail;
        this.selectedEventId = event.detail;
        const selectedEvent = new CustomEvent("eventselected", {
            detail: event.detail
        });
        this.dispatchEvent(selectedEvent);
    }

    handleKeyDown(event) {
        if(event.code == 'Escape') {
            this.isModalOpen = false;
            event.preventDefault();
            event.stopImmediatePropagation();
        }
    }

    focusTitle() {
        this.showPageHeader = true;
        let focus = setInterval(() => {
            let cmp = this.template.querySelector('.pageTitle');
            if(cmp) {
                cmp.focus();
                setTimeout(() => {
                    this.showPageHeader = false;
                }, 1000);
                clearInterval(focus);
            }
        }, 200);   
    }
}