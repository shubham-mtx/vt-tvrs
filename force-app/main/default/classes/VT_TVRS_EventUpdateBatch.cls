/**
 * @description       : 
 * @author            : Gourav Sharma
 * @group             : 
 * Modifications Log 
 * Ver   Date         Author           Modification
 * 1.0   04-24-2021   Gourav Sharma   Initial Version
**/
global class VT_TVRS_EventUpdateBatch implements Database.Batchable<sObject>{

    public Set<Id> appointmentIds = new Set<Id>();
    public Set<String> completedSlotsSet = new Set<String>();
    public String accId;
    public Date oldDates;
    public Date eventStartDate;
    public String eventId;
    public Set<Id> appointmentSlotIdSet = new Set<Id>();
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
 
        List<VTS_Event__c> events = [SELECT Id, Start_Date__c, Start_Time__c, End_Time__c FROM VTS_Event__c WHERE Id =:eventId];
        Time startTime = events[0].Start_Time__c;
        Time endTime = events[0].End_Time__c;
        // String query = 'SELECT  Id, Account__c,Date__c  FROM Appointment_Slot__c WHERE Account__c =: accId and ID IN : appointmentSlotIdSet and  Id NOT IN: completedSlotsSet and Date__c =: oldDates AND (Start_Time__c >=:startTime AND End_Time__c <=:endTime)';

        String query = 'SELECT  Id, Account__c,Date__c,(SELECT Id FROM Appointments__r)  FROM Appointment_Slot__c WHERE Account__c =: accId  and  Id NOT IN: completedSlotsSet and Date__c =: oldDates AND (Start_Time__c >=:startTime AND End_Time__c <=:endTime)';
        return Database.getQueryLocator(query);       
    }
    
    public VT_TVRS_EventUpdateBatch(){}
    
    public VT_TVRS_EventUpdateBatch(Set<Id> appointmentIds , String accId,Date eventStartDate, String eventId,Date oldDates, Set<String> completedSlots,Set<Id> appointmentSlotIdSet){
        
            this.appointmentIds = appointmentIds;
            this.completedSlotsSet = completedSlots;
            this.accId = accId;
            this.eventStartDate = eventStartDate;
            this.eventId = eventId;
            this.oldDates = oldDates;
            this.appointmentSlotIdSet = appointmentSlotIdSet;

    }

    global void execute(Database.BatchableContext bc, List<Appointment_Slot__c> lstSlot) {
        
        List<Appointment__c> appToUpdate = new List<Appointment__c>();

        List<Appointment_Slot__c> appSlotToUpdate = new List<Appointment_Slot__c>();

        for(Id aIds: appointmentIds){
            Appointment__c a = new Appointment__c();
            a.Id = aIds;
            a.Appointment_Start_Date_v1__c = Date.valueOf(eventStartDate);
            a.Is_Event_Change__c = true;
            appToUpdate.add(a);
        }
        
        for(Appointment_Slot__c appSlot : lstSlot){
            // Appointment_Slot__c  a = new Appointment_Slot__c();
            // a.Id = appIds.Id;
            Boolean updateAppointmentSlot = (appSlot.Appointments__r == null || appSlot.Appointments__r.isEmpty() || appointmentSlotIdSet.contains(appSlot.Id));
            if(!updateAppointmentSlot){
                continue;
            }
            appSlot.Date__c = Date.valueOf(eventStartDate);
            appSlotToUpdate.add(appSlot);  
        }


        if (!appSlotToUpdate.isEmpty()) {
            update appSlotToUpdate;
        } 
        if (!appToUpdate.isEmpty()) {
            update appToUpdate;
        }


    }
    global void finish(Database.BatchableContext bc) {
          Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            string[] to = new string[] {UserInfo.getUserEmail()};
            
            email.setToAddresses(to);
            email.setSubject('Event\'s Date Change Request');
            
            email.setHtmlBody('Hello, <br/><br/>Your request for Date change has been processed succesfully');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
}