@isTest
public class VT_TVRS_AccountContactRelationShipTest {
    @TestSetup
    static void makeData(){
            
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String laboratoryRecTypeId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;

        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        String citizenCovidId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
       


        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, true);
        Account createAccountTestSite1 = TestDataFactoryAccount.createAccount('VaccinationSite', laboratoryRecTypeId, true);

        Contact createContact = TestDataFactoryContact.createCitizenContact(false , 'TestLastName','TestFirstName');
        createContact.Notify_Patients__c = true;
        createContact.AccountId=createAccountTestSite.id;
        createContact.Opt_out_for_SMS_Updates__c = false;
        createContact.recordtypeID=testingSiteId;
        insert createContact;

        Contact createContact1 = TestDataFactoryContact.createCitizenContact(false , 'TeswwwtLastName','TestFirstName1');
        createContact1.Notify_Patients__c = true;
        createContact1.AccountId=createAccountTestSite1.id;
        createContact1.Opt_out_for_SMS_Updates__c = false;
        createContact1.recordtypeID=citizenCovidId;
        insert createContact1;
        

    }
    @isTest
    public static void getContactDetailTest(){ 
  
        Contact con = [Select Id , Name FROM Contact WHERE FirstName = 'TestFirstName'];
        Contact con1 = [Select Id , Name,LastName FROM Contact WHERE FirstName = 'TestFirstName1'];
        String result = VT_TVRS_AccountContactRelationShip.getContactDetail(con.Id );
        String result1 = VT_TVRS_AccountContactRelationShip.getContactDetail(con1.Id );
        System.assert(true, result != null);

    }

    @isTest
    public static void getAccountContactRealtionshipTest(){ 
  
        Account acc = [Select Id , Name FROM Account where Name ='TestSite'];
        acc.Active__c = true;
        acc.Event_Type__c = 'State-Sponsored';
        update acc;
        Contact con = [Select Id , Name FROM Contact limit 1];
        Test.startTest();
        Map<String,Object> result = VT_TVRS_AccountContactRelationShip.getAccountContactRealtionship(con.Id , 'TestLabAccount');
        Map<String,Object> result2 = VT_TVRS_AccountContactRelationShip.getAccountContactRealtionship(con.Id , '');
        Test.stopTest();
    }

    @istest
    public static void insertAccountContactRelationShipTest() {
        List<Account> accId = [Select Id FROM Account where Name = 'TestSite' limit 1];
        Contact con = [Select Id , Name FROM Contact WHERE FirstName = 'TestFirstName'];
        List<String> accIdList = new List<String>();
        for(Account acct: accId){
            accIdList.add(acct.Id);
        }
        try{

        String result = VT_TVRS_AccountContactRelationShip.insertAccountContactRelationShip(accIdList , con.Id);
        }
        catch(exception e) {}
     }
    

}