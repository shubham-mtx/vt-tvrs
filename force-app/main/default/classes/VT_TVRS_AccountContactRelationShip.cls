/**
 * @description       : 
 * @author            : Gourav Sharma
 * @group             : 
 * @create on         : 04-04-2021
 * @last modified by  : Gourav Sharma
**/
public class VT_TVRS_AccountContactRelationShip {



    /*
    * Method Name: getContactDetail
    * Description: get all  contact  details
    * @param: String recordId 
    * @return: String
    */
    @AuraEnabled
    public Static String getContactDetail(String recordId){
       
        Id contactTestingSiteRecorTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Antibody_Testing_Site').getRecordTypeId();
                
        Contact con = [SELECT Id, RecordtypeId, RecordType.name FROM Contact where Id =: recordId];

        if(contactTestingSiteRecorTypeId == con.RecordtypeId){
            return 'Other User';
        }else{
            return 'Citizen Covid User';
        }


  }

    /*
    * Method Name: getAccountContactRealtionship
    * Description: get all account contact relationship details
    * @param: String recordId , String search account
    * @return: Map<String,Object>
    */
    @AuraEnabled
    public Static Map<String,Object> getAccountContactRealtionship(String recordId,String searchKey){

        Id AccountRecordTypeId = [SELECT Id FROM RecordType
                                  WHERE DeveloperName = 'Test_Center' AND sObjectType = 'Account'].Id;

        List<AccountContactWrapper> selectAccountswrapper = new List<AccountContactWrapper>();
        List<AccountContactWrapper> accountswrapper = new List<AccountContactWrapper>();
        Map<String,Object> response = new Map<String,Object>();
        Set<Id> selectAccountsIds = new Set<Id>();

        for(AccountContactRelation acc : [SELECT Id, AccountId,Account.Name,Account.Active__c,Account.Event_Type__c, ContactId, Roles, IsDirect, IsActive 
                    FROM AccountContactRelation 
                    WHERE ContactId =:recordId AND Account.RecordTypeId =:AccountRecordTypeId 
                    AND Account.Active__c = true 
                    AND Account.Event_Type__c != 'Other' 
                    AND Account.Event_Type__c != null ORDER BY Account.Name ASC ]){

            selectAccountsIds.add(acc.AccountId);
            AccountContactWrapper w1 = new AccountContactWrapper();
            w1.accountId = acc.AccountId;
            w1.Name = acc.Account.Name;
            w1.IsDirect = acc.IsDirect;
            selectAccountswrapper.add(w1);
        }

   
        if(!String.isBlank(searchKey)){
            string tempString = '';
            String query = '';
      

            if(!searchKey.replace('*','_____').split('_____').isEmpty()){
                for (String str : searchKey.replace('*','_____').split('_____')) {
                    tempString += ' Name LIKE \'%'+str+'%\' OR ';
                }
            }

            String finalSearchString = tempString.removeEnd(' OR ');
            String status = 'Other';

            if(!String.isBlank(finalSearchString)){
                 query = 'SELECT Id,Name FROM Account where RecordTypeId =:AccountRecordTypeId AND Id NOT IN:selectAccountsIds AND Active__c = true AND Event_Type__c !=: status AND Event_Type__c != null AND ('+finalSearchString+') ORDER BY Name ASC';
           
            }else{
                 query = 'SELECT Id,Name FROM Account where RecordTypeId =:AccountRecordTypeId AND Id NOT IN:selectAccountsIds AND Active__c = true AND Event_Type__c !=: status AND Event_Type__c != null ORDER BY Name ASC';
           
            }
            for(Account acc : Database.query(query)){
                AccountContactWrapper w2 = new AccountContactWrapper();
                w2.accountId = acc.Id;
                w2.Name = acc.Name;
                accountswrapper.add(w2);
            }
        }else{
            for(Account acc : [SELECT Id,Name FROM Account where RecordTypeId =:AccountRecordTypeId AND Id NOT IN:selectAccountsIds AND Active__c = true AND Event_Type__c != 'Other' AND Event_Type__c != null ORDER BY Name ASC ]){
                AccountContactWrapper w2 = new AccountContactWrapper();
                w2.accountId = acc.Id;
                w2.Name = acc.Name;
                accountswrapper.add(w2);
            }
        }

        response.put('selectAccounts',selectAccountswrapper);
        response.put('allAccounts',accountswrapper);

        return response;
    }

     /*
    * Method Name: insertAccountContactRelationShip
    * Description: insert account contact relationship 
    * @param: List<String> accountids , String contactId
    * @return: Map<String,Object>
    */
    @AuraEnabled
    public Static string insertAccountContactRelationShip(List<String> accIds, String recordId){

        List<AccountContactRelation> lst = new List<AccountContactRelation>();
        try {
            for(String Id : accIds){
                AccountContactRelation acc = new AccountContactRelation();
                acc.contactId =  recordId;
                acc.AccountId =  Id;
                lst.add(acc);
            }

            insert lst;
            return 'sucess';

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }


    // Wrapper Class Construction
    public class AccountContactWrapper{

        @AuraEnabled public Id AccountId{get;set;}
        @AuraEnabled public String Name{get;set;}
        @AuraEnabled public Boolean IsDirect{get;set;}

        // Wrapper class constructor

        public AccountContactWrapper(){}

    }
}