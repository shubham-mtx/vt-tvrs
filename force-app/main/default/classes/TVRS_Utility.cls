/**
 * @description       : 
 * @author            : Balram Dhawan
 * @group             : 
 * @last modified on  : 01-21-2022
 * @last modified by  : Balram Dhawan
**/
public without sharing class TVRS_Utility {
    /**
    * @description check required fields before appointment schedular from Citizen portal
    * @author Balram Dhawan | 01-14-2022 
    * @param Contact con 
    * @param String requiredFields 
    **/
    public static void requiredFieldsCheck(Contact con, String requiredFields) {
        String missingRequiredFields = '';
        Map<String, Schema.SObjectField> contactFieldMap = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap();
        if(!con.Consent__c){
            // 'The person you are trying to schedule has an Incomplete profile. Please click UPDATE DETAILS or PRE-REGISTER button!'
            throw new VT_Exception('missingProfileInformation');
        }

        for(String fieldName : requiredFields.split(',')){
            if(con.get(fieldName) == null) {
                String labelField = contactFieldMap.get(fieldName).getDescribe().getLabel();
                missingRequiredFields += (String.isNotBlank(missingRequiredFields) ? ','+labelField : labelField);
            }
        }

        if(String.isNotBlank(missingRequiredFields)) {
            throw new VT_Exception('missingProfileInformation');
        }

        if(con.Pre_Registrations__r.isEmpty()) {
            throw new VT_Exception('missingProfileInformation');
        }

        Pre_Registration__c preReg = con.Pre_Registrations__r[0];
        if(String.isBlank(preReg.Is_Immune_Weak__c) || String.isBlank(preReg.BIPOC__c) ) {
            throw new VT_Exception('missingProfileInformation');
        }
    }

    /**
    * @description check if contact has access to the contactId or appointmentId
    * @author Balram Dhawan | 01-18-2022 
    * @param String contactId 
    * @param String appointmentId 
    **/
    public static void checkUserAccess(String contactId, String appointmentId) {
        User currentUser = [SELECT Id, ProfileId, Profile.Name, ContactId From User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if(currentUser.Profile.Name == VT_Constants.TVRS_COVID_CITIZEN_PROFILE) {
            Set<Id> validContactIds = new Set<Id>();
            List<Contact> contacts = new List<Contact>();
            List<Appointment__c> appointments = new List<Appointment__c>();
            for (Contact contactRecord : [SELECT Id FROM Contact 
                                            WHERE 
                                            Id =:currentUser.ContactId OR Parent_Contact__c =:currentUser.ContactId 
                                            WITH SECURITY_ENFORCED]) {
                validContactIds.add(contactRecord.Id);
            }

            if(String.isNotBlank(appointmentId)) {
                appointments = [SELECT Id 
                                FROM Appointment__c 
                                WHERE 
                                    Id =:appointmentId 
                                        AND 
                                    Patient__c != ''
                                        AND
                                    Patient__c IN :validContactIds
                                WITH SECURITY_ENFORCED 
                                LIMIT 1];
            }

            if(((String.isNotBlank(contactId) && !validContactIds.contains(contactId)) || (String.isNotBlank(appointmentId) && appointments.isEmpty()))) {
                throw new VT_Exception(VT_Constants.CRUD_PERMSSION_ERROR_MSG);
            }
        }
    }

    /**
    * @description get complete data (Contact, Appointments, Pre_Registration) of patient by patientId
    * @author Balram Dhawan | 01-18-2022 
    * @param String patientId 
    * @return Contact 
    **/
    public static Contact getPatientDataWithVaccinations(String patientId) {
        checkUserAccess(patientId, null);
        return [SELECT Id, 
                    (SELECT Id, Event__c, Event__r.Private_Access__c, Event__r.Private_Access__r.Vaccine_Type__c,
                    Event__r.Private_Access__r.Min__c, Event__r.Private_Access__r.Max__c, Appointment_Date__c, Status__c, 
                    Event__r.Private_Access__r.Weak_Immune_Min_Days__c, Event__r.Private_Access__r.Requires_Followup_Dose__c, 
                    Event__r.Private_Access__r.Is_Alternate_Dose_Available__c, Event__r.Private_Access__r.Alternate_Doses__c,
                    Event__r.Private_Access__r.Vaccine_Class_Name__c, Event_Process__c, Event__r.Private_Access__r.Weak_Immune_Max_Days__c,Appointment_Complete_Date__c   
                    FROM Appointments2__r 
                    WHERE Event_Process__c = :VT_TS_Constants.VACCINATION_TEXT
                    AND Event__r.Private_Access__r.Event_Process__c = :VT_TS_Constants.VACCINATION_TEXT
                    AND Status__c IN (:VT_TS_Constants.SCHEDULED_TEXT, :VT_TS_Constants.COMPLETED_TEXT)
                    ORDER BY Event__r.Private_Access__r.Vaccine_Type__c DESC),
                    
                    (SELECT Id, Is_Immune_Weak__c, Pre_Registration_Group__c, Early_Group1_Health__c, Early_Group_2_Access_Group__c    
                    FROM Pre_Registrations__r)
                FROM Contact 
                WHERE Id =:patientId
                WITH SECURITY_ENFORCED];
    }

    /**
    * @description getMinimumNextDoseDate based on preReg and lastCompletedAppointment
    * @author Balram Dhawan | 12-21-2021 
    * @param Map<String Object> response 
    **/
    public static Map<String, Object> getMinimumNextDoseDate(Pre_Registration__c preReg, Appointment__c appointment) {
        Map<String, Object> response = new Map<String, Object>();
        Date finalStartDate = System.today();
        Date minimumNextDoseDate = System.today();
        
        if(preReg == null || appointment == null) {
            response.put('minimumNextDoseDate', minimumNextDoseDate);
            return response;
        }
        
        if(preReg.Is_Immune_Weak__c == 'Yes' && (
                (appointment.Event__r.Private_Access__r.Vaccine_Type__c != VT_TS_Constants.FIRST_DOSE_TEXT && 
                appointment.Event__r.Private_Access__r.Vaccine_Type__c != VT_TS_Constants.SECOND_DOSE_TEXT) || 
                appointment.Event__r.Private_Access__r.Vaccine_Class_Name__c == VT_TS_Constants.JANSSEN_TEXT)
        ) {
            Integer minDays = Integer.valueOf(convertDecimalToInteger(appointment.Event__r.Private_Access__r.Weak_Immune_Min_Days__c));
            minimumNextDoseDate = appointment.Appointment_Date__c.addDays(minDays);
            finalStartDate = minimumNextDoseDate > System.today() ? minimumNextDoseDate : System.today();
        } else {
            Integer minDays = Integer.valueOf(convertDecimalToInteger(appointment.Event__r.Private_Access__r.Min__c));
            minimumNextDoseDate = appointment.Appointment_Date__c.addDays(minDays);
            finalStartDate = minimumNextDoseDate > System.today() ? minimumNextDoseDate : System.today();
        }

        response.put('minimumNextDoseDate', minimumNextDoseDate);
        response.put('notCompletedRequiredInterval', (finalStartDate > System.today()));
        response.put('filterStartDate', finalStartDate);
        response.put('remainingDaysForNextDose', (finalStartDate > System.today() ? System.today().daysBetween(finalStartDate) : 0));
        response.put('filterEndDate', finalStartDate.addDays(30));
        return response;
    }

    /**
    * @description returns last vaccine Type E.g. for Vaccination-2, it will return Vaccination-1
    * @author Balram Dhawan | 01-07-2022 
    * @param String vaccineType // current vaccine type
    * @return String 
    **/
    public static String getPreviousVaccineType(String vaccineType) {
        if(vaccineType == 'Vaccination-1') {
            return null;
        }
        Integer previousDoseNumber = Integer.valueOf(vaccineType.split('-')[1])-1;
        return 'Vaccination-'+previousDoseNumber;
    }

    /**
    * @description if null returns 0 else value
    * @author Balram Dhawan | 01-18-2022 
    * @param Decimal value 
    * @return Integer 
    **/
    public static Integer convertDecimalToInteger(Decimal value) {
        if(value == null) {
            return 0;
        }
        return Integer.valueOf(value);
    }
}