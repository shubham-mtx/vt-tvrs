import { LightningElement, track, api } from 'lwc';
import createSlots from '@salesforce/apex/OKTC_ScheduleAppointment.createSlots';
import getUser from '@salesforce/apex/OKPCHeaderController.getUserDetailsAndTestingSite';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {
    showMessage,
    showMessageWithLink,
    msgObj,
    showAsyncErrorMessage
} from 'c/vtTsUtility';
export default class OktcSchedulerConfiguration extends LightningElement {

    @track accountId = '';
    @track numOfSlot = 1;
    @track showSpinner = true;
    slotData = [];
    @api recordId;
    @track isInternalUser = false;

    connectedCallback() {
        if(this.recordId) {
            this.accountId = this.recordId;
            this.showSpinner = false;
            this.isInternalUser = true;
        } else {
            this.callApex();
        }
    }

    callApex(){
        getUser({})
        .then(result => {

            // Assigning a defaut when testing with Admin
            this.accountId = (result.userDetail.Contact && result.testingSiteId) ? result.testingSiteId : '';
            this.showSpinner = false;
        })
        .catch(error => {
            this.error = error;
            showAsyncErrorMessage(this,error);
        });
        
    }

    createSlots(){
        this.showSpinner = true;
        this.slotData = [];
        let calendarPage = this.template.querySelector('c-oktc-calendar');
        if(calendarPage) {
            this.slotData = calendarPage.getSlots();
            if(this.slotData.length > 0) {
                if(!this.numOfSlot) {
                    this.toastErrorAndReturn('Please provide Number of Available Appointment Slots.');
                    return;
                }
                else if(this.numOfSlot < 0) {
                    this.toastErrorAndReturn('Number of Available Appointment Slots must be greater or equals to 0.');
                    return;
                } else if(this.numOfSlot > 100) {
                    this.toastErrorAndReturn('Number of Available Appointment Slots must be less or equals to 100.');
                    return;
                } else {
                    let totalSlots = 0
                    let creatingSlots = 0
                    let deletingSlots = 0
                    let slots = this.numOfSlot;
                    this.slotData.forEach(function(slot) {
                        if(slot.available > slots) {
                            deletingSlots += Math.abs(slot.available - slots);
                        } else if(slot.available < slots) {
                            creatingSlots += Math.abs(slot.available - slots);
                        }
                        totalSlots += Math.abs(slot.available - slots);// == -1 ? -1 * (slot.available - this.numOfSlot) : (slotData.available - this.numOfSlot);
                    });
                    if((totalSlots) > 10000) {
                        this.toastErrorAndReturn('Processing (' + totalSlots +') slots, creating(' + creatingSlots + '), deleting(' + deletingSlots + ') . Please unselect some of the slots as we can only process up to 10000 Appointment Slots in a single transaction.');
                        return;
                    }
                }
                createSlots({
                    accId: this.accountId,
                    slotWrapperString: JSON.stringify(this.slotData),
                    numOfSlot: this.numOfSlot
                }).then(result => {
                    const evt = new ShowToastEvent({
                        title: 'Success',
                        message: 'Succesfully updated the Appointment Slots.',
                        variant: 'success',
                    });
                    this.dispatchEvent(evt);
        
                    calendarPage.refreshCalendarView();
                    this.showSpinner = false;
                })
                .catch(error => {
                    this.error = error;
                    showAsyncErrorMessage(this,error);
                    this.toastErrorAndReturn('Unable to create the Slots, please contact your Administrator.');
                });
            } else {
                this.toastErrorAndReturn('Please select a slot to be created.');
            }
        }
    }

    toastErrorAndReturn(error) {
        const evt = new ShowToastEvent({
            title: 'Error',
            message: error,
            variant: 'error',
        });
        this.showSpinner = false;
        this.dispatchEvent(evt);
    }

    handleSlotChange(event) {
        this.numOfSlot = event.target.value;
    }
}