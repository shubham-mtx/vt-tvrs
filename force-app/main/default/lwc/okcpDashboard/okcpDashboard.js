/**
 * @description       : 
 * @author            : Balram Dhawan
 * @group             : 
 * @last modified on  : 01-20-2022
 * @last modified by  : Mohit Karani
 **/
import {
    LightningElement,
    track,
    api
} from 'lwc';

import basePath from '@salesforce/community/basePath';

import retrievePortalContent from '@salesforce/apex/OKPCDashboardController.retrievePortalContent';

import getAccountdetails from '@salesforce/apex/OKPC_LocationMapController.getAccountdetails';
import getCurrentUserDetail from '@salesforce/apex/OKPCDashboardController.getCurrentUserDetail';
import Enable_Vaccination_Button from "@salesforce/label/c.Enable_Vaccination_Button";
import checkExistingAppointment from "@salesforce/apex/DC_ScheduleAppointment.checkExistingAppointmentFor2Dose";
import checkExistingAppointmentForNextDose from "@salesforce/apex/DC_ScheduleAppointment.checkExistingAppointmentForNextDose";
import checkVaccinationReschedule from "@salesforce/apex/DC_ScheduleAppointment.checkVaccinationReschedule";
import getAppointments from '@salesforce/apex/OKPCDashboardController.getAppointments';
import TVRS_APPOINTMENT_REMINDERS from '@salesforce/label/c.TVRS_APPOINTMENT_REMINDERS';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import VT_TVRS_Filtter_CSS from "@salesforce/resourceUrl/VT_TVRS_Filtter_CSS";
import {
    loadStyle
} from "lightning/platformResourceLoader";
import {
    showMessage,
    showMessageWithLink,
    msgObj,
    showAsyncErrorMessage,
    createMessage
} from 'c/vtTsUtility';
export default class OkpcDashboard extends LightningElement {

    constructor() {
        super();
        Promise.all([loadStyle(this, VT_TVRS_Filtter_CSS)]);
    }

    @api forVaccination;
    @track contactName;
    @track contactId;
    @track streetAddress;
    @track showMap = false;
    @track showConfirmation = false;
    @track showAppointmentModal = false;
    @track address;
    @track mapMarkers = [];
    @track results = {};
    @track isCancelledWindow = false;
    @track hasResults = false;
    @track showpick = true;
    @track fromDashboard = false;
    @track eventId;
    @track vaccinationPrivateEventId;
    @track Enable_Vaccination_Button_1st_Dose = JSON.parse(Enable_Vaccination_Button).Enable_1st_Dose == 1;
    @track preRegDateTime;
    @track items = [];
    //Added by rajat as per story S-23784
    newsData = [];
    //End of S-23784
    showSpinnerCarousel = true;
    msgObjData = msgObj();
    options = {
        autoScroll: true,
        autoScrollTime: 7
    };
    appointmentIdVsData;
    @track rescheduleAppointment;
    showCalendar;
    eventIds;
    isSecondDose;
    scheduleSecondDoseAppointment = false;
    contactList = [];
    @track appointmentList = [];
    patientId;
    appointmentId = '';
    showContactSelectionModal = false;
    currentUserDetail = {};
    showSpinner = false;
    urlList = [];
    category = basePath;
    showVaccinationButton;
    secondDoseData;
    secondDoseEventIds;
    // customErrorMessage;
    doseNumVsPatientIdMap = new Map();
    showanydosemodal = false;
    patientAppInfo = null;
    showFilterModal = false;

    /* Added by Santhosh */
    @track
    isFiltered = false;
    statusValues = ['Scheduled', 'Completed'];
    typeValues = ['Vaccine', 'Test'];
    rangeValues = ['Last 30 Days'];
    sortValues = ['Date'];
    scheduled = false;
    completed = false;
    cancelled = false;
    vaccine = false;
    test = false;
    last30Days = false;
    last60Days = false;
    all = false;
    date = false;
    person = false;
    status = [];
    type = '';
    range = '30';
    sort = 'Appointment_Date_Time_Complete__c';
    @track inputcheckBox = [];
    @track selectedStatus = ['Scheduled', 'Completed'];
    @track updatedAppointmentList = [];
    @track statusOptions = [{
            label: 'Scheduled',
            value: true,
            queryValue: 'Scheduled',
            type: 'Status',
            class: 'slds-checkbox active'
        },
        {
            label: 'Completed',
            value: true,
            queryValue: 'Completed',
            type: 'Status',
            class: 'slds-checkbox active'
        },
        {
            label: 'Cancelled',
            value: false,
            queryValue: 'Cancelled',
            type: 'Status',
            class: 'slds-checkbox'
        }
    ];

    @track typeOptions = [{
            label: 'Vaccine',
            value: true,
            queryValue: 'Vaccination',
            type: 'Type',
            class: 'slds-checkbox active'
        },
        {
            label: 'Test',
            value: true,
            queryValue: 'Testing Site',
            type: 'Type',
            class: 'slds-checkbox active',
        },
    ];
    @track rangeOptions = [{
            label: 'Last 30 Days',
            value: true,
            queryValue: '30',
            type: 'Range',
            class: 'slds-checkbox active'
        },
        {
            label: 'Last 60 Days',
            value: false,
            queryValue: '60',
            type: 'Range',
            class: 'slds-checkbox'
        },
        {
            label: 'All',
            value: false,
            queryValue: 'All',
            type: 'Range',
            class: 'slds-checkbox'
        },
    ];
    @track sortOptions = [{
            label: 'Date',
            value: true,
            queryValue: 'Appointment_Date_Time_Complete__c',
            type: 'Sort By',
            class: 'slds-checkbox active'
        },
        {
            label: 'Person',
            value: false,
            queryValue: 'Patient__r.Name',
            type: 'Sort By',
            class: 'slds-checkbox'
        },
    ];

    @track allRecords = [];

    /*end added by santhosh */

    // added by vamsi Mudaliar
    // Contains Strings of (Type : Value) 
    //@track activePillFilter = [];
    @track rangeAndSortPills = [];
    @track statusAndTypePills = [];

    @track originalFilters = {
        typeOptions: JSON.parse(JSON.stringify(this.typeOptions)),
        sortOptions: JSON.parse(JSON.stringify(this.sortOptions)),
        rangeOptions: JSON.parse(JSON.stringify(this.rangeOptions)),
        statusOptions: JSON.parse(JSON.stringify(this.statusOptions))
    }

    appointmentReminderLabel = JSON.parse(TVRS_APPOINTMENT_REMINDERS);

    // added by vamsi Mudaliar
    // this utility function updated pill values accordingly. 
    updatePillFilter() {
        let activeFilterValues = [];
        this.rangeAndSortPills = [];
        this.statusAndTypePills = [];
        // status, type
        activeFilterValues = [...this.statusOptions.filter((currVal) => {
            return currVal.value === true;
        })];
        if(!this.fromDashboard){
            activeFilterValues.push(...this.typeOptions.filter((currVal) => {
                return currVal.value === true;
            }));
            // range,sort.
            activeFilterValues.push(...this.rangeOptions.filter((currVal) => {
                return currVal.value === true;
            }));
        }

        activeFilterValues.push(...this.sortOptions.filter((currVal) => {
            return currVal.value === true;
        }));

        activeFilterValues.forEach(record => {
            if(record.type==='Range' || record.type==='Sort By') {

                this.rangeAndSortPills.push({
                    typeOfFilter: record.type,
                    originalLabel: record.label
                });
            }
            else {
                this.statusAndTypePills.push({
                    typeOfFilter: record.type,
                    originalLabel: record.label
                });
            }
        })

    }
    // added by vamsi Mudaliar 
    // this func removes filter and updates filtered appointments on user click
    handlePillFilterRemove(event) {
        const currPillType = event.currentTarget.dataset.type;
        const currPillValue = event.currentTarget.dataset.label;
        let rangeAndSortItemPills =  this.rangeAndSortPills && this.rangeAndSortPills.length > 0 ? JSON.parse(JSON.stringify(this.rangeAndSortPills)): [];
        let statusAndTypeItemPills =  this.statusAndTypePills && this.statusAndTypePills.length > 0 ? JSON.parse(JSON.stringify(this.statusAndTypePills)) : [];
            
            // handling separte deletion of type pills 
            // Removing Pills from UI from the PillList based on the value clicked.
            if(currPillType==='Status' || currPillType==='Type') {

                statusAndTypeItemPills.splice(statusAndTypeItemPills.findIndex(item => item.originalLabel === currPillValue),1);
                this.statusAndTypePills = JSON.parse(JSON.stringify(statusAndTypeItemPills));
            }
            else {
                rangeAndSortItemPills.splice(rangeAndSortItemPills.findIndex(item => item.originalLabel === currPillValue),1);
                this.rangeAndSortPills =  JSON.parse(JSON.stringify(rangeAndSortItemPills));
            }

        // make clicked pill as unchecked and invoke callApex method
        switch (currPillType) {
            case 'Status':
                const currStatusIdx = this.statusOptions.findIndex(record => record.label === currPillValue);
                this.statusOptions[currStatusIdx].value = false;
                this.statusOptions[currStatusIdx].class = 'slds-checkbox';
                // removing pillValue from the currentSelectedStatus List.
                this.selectedStatus = this.selectedStatus.filter(e => e !== currPillValue);
                break;
            case 'Range':
                const currRangeIdx = this.rangeOptions.findIndex(record => record.label === currPillValue);
                this.rangeOptions[currRangeIdx].value = false;
                this.rangeOptions[currRangeIdx].class = 'slds-checkbox';
                break;
            case 'Sort By':
                const currSortIdx = this.sortOptions.findIndex(record => record.label === currPillValue);
                this.sortOptions[currSortIdx].value = false;
                this.sortOptions[currSortIdx].class = 'slds-checkbox';
                break;
            case 'Type':
                const currTypeIdx = this.typeOptions.findIndex(record => record.label === currPillValue);
                this.typeOptions[currTypeIdx].value = false;
                this.typeOptions[currTypeIdx].class = 'slds-checkbox';
                break;
        }
        // refreshes UI with updated Appointments 
        this.callApex();
    }


    get hasAppointment() {
        return this.appointmentList.length > 0;
    }

    get siteId() {
        return this.currentUserDetail.Contact.AccountId;
    }

    get checkBannerInfoExist() {
        return this.items.length > 0;
    }


    connectedCallback() {
        this.fromDashboard = true;
        this.showSpinner = true;
        this.getCurrentUser();
        this.doInit();
        this.retrievePortalContentHandler();
        this.updatePillFilter();
        let forFocus = setInterval(() => {
            let input = this.template.querySelector('lightning-button');
            if (input) {
                input.focus();
                clearInterval(forFocus);
            }
        }, 100);
    }


    // added by vamsi 
    retrievePortalContentHandler() {
        // items for gen info
        // urlList for links
        //newsData for News
        // retrieving Portal Content 
        retrievePortalContent()
            .then(result => {
                //let dataFetched=JSON.parse(JSON.stringify(result));
                for (const [key, value] of Object.entries(result)) {

                    let finalData = value;
                    switch (key) {

                        case 'Important Links':

                            finalData.forEach(element => {
                                element.ariaLabel = element.displayName + ', this will open in new Tab';
                            });
                            this.urlList = [...finalData];
                            break;
                        case 'News Section':
                            finalData.forEach(element => {
                                if (!element.linkLabel) {
                                    element.linkLabel = 'CLICK HERE';
                                }
                            });
                            this.newsData = [...finalData];
                            break;
                        case 'Main Section':
                            this.showSpinnerCarousel = false;
                            this.items = finalData.map(record => {
                                return {
                                    description: record.message
                                }
                            });
                            break;
                    }
                }
            })
    }


    getCurrentUser() {
        getCurrentUserDetail()
            .then(result => {
                this.currentUserDetail = result;
                if (this.siteId) {
                    this.callApex();
                }
            })
            .catch(error => {
                this.dispatchEvent(new ShowToastEvent({
                    message: error.body.message,
                    variant: 'error'
                }));
            });
    }

    doInit() {
        let queryParams = new URLSearchParams(window.location.search);
        if (queryParams.has('eventId')) {
            this.eventId = queryParams.get('eventId');
            if (this.forVaccination) {
                this.showVaccinationButton = true;
            }
        }
    }

    callApex() {
        this.appointment = {};
        let selectedTypes = [];
        this.typeOptions && this.typeOptions.forEach(element => {
            element.value && selectedTypes.push(element.queryValue);
        })
        let wrapperToFilter = {
            listStatus: this.selectedStatus,
            type: selectedTypes,
            days: this.range,
            sortValue: this.sort
        }
        this.originalFilters = {
            typeOptions: JSON.parse(JSON.stringify(this.typeOptions)),
            sortOptions: JSON.parse(JSON.stringify(this.sortOptions)),
            rangeOptions: JSON.parse(JSON.stringify(this.rangeOptions)),
            statusOptions: JSON.parse(JSON.stringify(this.statusOptions))
        }
        this.showSpinner = true;
        getAppointments({
                request: JSON.stringify(wrapperToFilter)
            })
            .then(result => {
                this.processRecords(result);
            }).catch(error => {
                showAsyncErrorMessage(this, error);
                this.showSpinner = false;
            });
    }

    checkforCancelledAppointments(appointmentList) {
        let cancelledAppointments = appointmentList.filter(appt => appt.isCancelled);
        cancelledAppointments.forEach(appt => {
            appt.hideReschedule = this.doseNumVsPatientIdMap.get(appt.patientId) && (this.doseNumVsPatientIdMap.get(appt.patientId)[0] == appt.vaccineType || (parseInt(this.doseNumVsPatientIdMap.get(appt.patientId)[0].split('-')[1]) > parseInt(appt.vaccineType)));
        });
        this.showSpinner = false;
    }
    retrieveMarker(accountId, callback) {
        this.mapMarkers = [];
        getAccountdetails({
            accountId: accountId
        }).then(result => {
            this.accountDetails = JSON.parse(JSON.stringify(result));
            if (this.accountDetails) {
                this.mapMarkers.push(this.accountDetails);
            }
            callback();
        }).catch(error => {
            showAsyncErrorMessage(this, error);
        });
    }

    handleAction(event) {
        switch (event.detail.actionName) {
            case 'viewCertificate':
                this.viewCertificate(event.detail.actionParams.contactId);
                break;
            case 'reschedule':
                this.eventIds = undefined;
                this.oldVaccineClass = undefined;
                this.isSecondDose = undefined;
                this.preRegDateTime = undefined;
                this.showContactSelectionModal = false;

                this.rescheduleAppointment = this.appointmentList.find(record => record.id === event.detail.actionParams.appointmentId);
                this.appointmentId = event.detail.actionParams.appointmentId;
                if (!this.rescheduleAppointment.isTesting) {
                    checkVaccinationReschedule({
                            appointmentId: this.appointmentId
                        })
                        .then(result => {
                            if (result.furtherDosesNotReqd) {
                                showMessageWithLink(this, 'error', this.msgObjData['furtherDosesNotReqd'], this.msgObjData['vermontMsgData']);
                            } else if (result.isSelectedDoseAlreadyScheduled && this.appointmentId != result.existingAppointmentData.Id) {
                                // else if (result.isSelectedDoseAlreadyScheduled && (event.detail.actionName == 'scheduleNextDose')) {
                                createMessage(result.doseNumber, this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyScheduled']);
                            } else if (result.isSelectedDoseAlreadyCompleted) {
                                showMessage(this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyCompleted']);
                            } else if (result.previousDoseInScheduledStatus) {
                                showMessage(this, 'Error!', 'error', result.previousDoseInScheduledStatus);
                            } else if (result.noPrivateAccessAssignmentFound) {
                                showMessageWithLink(this, 'error', this.msgObjData['noPrivateAccessAssignmentFound'], this.msgObjData['noPrivateAccessAssignmentFoundUrl']);
                            } else if (result.ageRestriction) {
                                this.dispatchEvent(new ShowToastEvent({
                                    message: 'Because of your age, you are not yet eligible to get a COVID-19 vaccine. Visit the {0} to get more information and find out who is eligible for a vaccine now.',
                                    variant: 'error',
                                    mode: 'sticky',
                                    messageData: [{
                                        url: 'https://www.healthvermont.gov/covid-19/vaccine/aboutabout-covid-19-vaccines-vermont',
                                        label: 'Vermont Department of Health website'
                                    }]
                                }));
                            } else if (result.preRegGroupNotActive) {
                                this.dispatchEvent(new ShowToastEvent({
                                    message: result.preRegGroupNotActive,
                                    variant: 'error',
                                    mode: 'sticky'
                                }));
                            } else if (result.noOpenEventsFoundInPreRegGroups) {
                                this.dispatchEvent(new ShowToastEvent({
                                    message: result.noOpenEventsFoundInPreRegGroups,
                                    variant: 'error',
                                    mode: 'sticky'
                                }));
                            } else {
                                this.eventIds = (result.eventIds) ? result.eventIds : (result.isSelectedDoseAlreadyScheduled && result.existingAppointmentData && this.appointmentId == result.existingAppointmentData.Id &&  result.existingAppointmentData.Event__r && result.existingAppointmentData.Event__r.Private_Access__r) ? result.existingAppointmentData.Event__r.Private_Access__r.Id : null;
                                if (result.preRegMinDateTime) {
                                    this.preRegDateTime = result.preRegMinDateTime;
                                }
                                this.showCalendar = true;

                                if (!result.isFirstAppointment) {
                                    this.oldVaccineClass = result.oldVaccineClass;
                                    this.isSecondDose = true;
                                }
                            }
                            this.showSpinner = false;
                        })
                        .catch(error => {
                            showAsyncErrorMessage(this, error);
                            this.showSpinner = false;
                        });
                } else {
                    this.isCancelledWindow = true;
                    setTimeout(() => {
                        this.focusFirstEle();
                    }, 200);
                }
                break;
            case 'viewOnMap':
                this.retrieveMarker(event.detail.actionParams.siteId, () => {
                    this.titleMap = event.detail.actionParams.title;
                    this.showMap = true;
                });
                break;
            case 'editAppointment':
                this.showAppointmentModal = true;
                this.appointmentId = event.detail.actionParams.appointmentId;
                break;
            case 'cancelAppointment':
                this.showConfirmation = true;
                this.appointmentId = event.detail.actionParams.appointmentId;
                break;
            case 'schedule2ndDose':
                this.showSpinner = true;
                checkExistingAppointment({
                        appointmentId: event.detail.actionParams.appointmentId
                    })
                    .then(result => {
                        if (result.furtherDosesNotReqd) {
                            showMessageWithLink(this, 'error', this.msgObjData['furtherDosesNotReqd'], this.msgObjData['vermontMsgData']);
                        } else if (result.isSelectedDoseAlreadyScheduled) {
                            createMessage(result.doseNumber, this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyScheduled']);
                        } else if (result.isSelectedDoseAlreadyCompleted) {
                            showMessage(this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyCompleted']);
                        } else if (result.ageRestriction) {
                            showMessageWithLink(this, 'error', this.msgObjData['furtherDosesNotReqd'], this.msgObjData['ageRestriction']);
                        } else {
                            if (result.eventIds) {
                                this.appointmentId = event.detail.actionParams.appointmentId;
                                this.secondDoseData = this.appointmentIdVsData[event.detail.actionParams.appointmentId];
                                this.secondDoseEventIds = result.eventIds;
                                this.scheduleSecondDoseAppointment = true;;
                            } else {
                                this.dispatchEvent(new ShowToastEvent({
                                    variant: 'error',
                                    message: 'The person you are trying to schedule is not currently eligible for their next dose. To see when they may be eligible, stay up to date by visiting our website at Getting the COVID-19 Vaccine | {0}',
                                    messageData: [{
                                        url: 'https://www.healthvermont.gov/covid-19/vaccine/getting-covid-19-vaccine',
                                        label: 'Vermont Department of Health'
                                    }]
                                }));
                            }
                        }
                        this.showSpinner = false;
                    })
                    .catch(error => {
                        showAsyncErrorMessage(this, error);
                        this.showSpinner = false;
                    });
                break;
            case 'scheduleNextDose':
                this.showSpinner = true;
                checkExistingAppointmentForNextDose({
                        appointmentId: event.detail.actionParams.appointmentId
                    })
                    .then(result => {
                        if (result.ageRestriction) {
                            showMessageWithLink(this, 'error', this.msgObjData['ageRestriction'], this.msgObjData['vermontMsgData']);
                        } else if (result.furtherDosesNotReqd) {
                            showMessageWithLink(this, 'error', this.msgObjData['furtherDosesNotReqd'], this.msgObjData['vermontMsgData']);
                        } else if (result.isSelectedDoseAlreadyScheduled) {
                            createMessage(result.doseNumber, this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyScheduled']);
                        } else if (result.isSelectedDoseAlreadyCompleted) {
                            showMessage(this, 'Error!', 'error', this.msgObjData['isSelectedDoseAlreadyCompleted']);
                        } else if (result.isImmuneWeakNotAnsered) {
                            showMessage(this, 'Error!', 'error', result.isImmuneWeakNotAnsered);
                        } else {
                            if (result.eventIds) {
                                this.appointmentId = event.detail.actionParams.appointmentId;
                                this.secondDoseData = this.appointmentIdVsData[event.detail.actionParams.appointmentId];
                                this.secondDoseEventIds = result.eventIds;
                                this.scheduleSecondDoseAppointment = true;;
                            } else {
                                this.dispatchEvent(new ShowToastEvent({
                                    variant: 'error',
                                    message: 'The person you are trying to schedule is not currently eligible for their next dose. To see when they may be eligible, stay up to date by visiting our website at Getting the COVID-19 Vaccine | {0}',
                                    messageData: [{
                                        url: 'https://www.healthvermont.gov/covid-19/vaccine/getting-covid-19-vaccine',
                                        label: 'Vermont Department of Health'
                                    }]
                                }));
                            }
                        }
                        this.showSpinner = false;
                    })
                    .catch(error => {
                        showAsyncErrorMessage(this, error);
                    });
                break;

            default:
                break;
        }
    }

    viewCertificate(patientId) {
        var url = window.location.href;
        url = url.substring(0, url.indexOf("/s/"));
        url += '/apex/ResultCertificate?id=' + patientId;
        window.open(url, '_blank');
    }

    viewLocationOnMap() {
        this.showMap = true;
    }

    editAppointment() {
        this.showAppointmentModal = true;
    }

    cancelAppointment() {
        this.showConfirmation = true;
    }

    openCancel() {
        this.isCancelledWindow = true;
    }

    closeCancel() {
        this.isCancelledWindow = false;
    }

    hideModal() {
        this.callApex();
        this.showConfirmation = false;
        this.showAppointmentModal = false;
        this.scheduleSecondDoseAppointment = false;
        this.showMap = false;
    }

    msToTimeAMPM(s) {
        if (s != undefined) {
            if (s == 0) {
                return '12:00 AM';
            }

            var ms = s % 1000;
            s = (s - ms) / 1000;
            var secs = s % 60;
            s = (s - secs) / 60;
            var mins = s % 60;
            var hrs = (s - mins) / 60;
            let radian = 'AM';
            if (hrs >= 12) {
                radian = 'PM';
                if (hrs > 12) {
                    hrs = hrs - 12;
                }

            }
            return this.pad(hrs) + ':' + this.pad(mins) + ' ' + radian;
        }
        return '';
    }

    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }

    handleSubmitData(event) {
        if (event.detail.name == 'submit') {
            this.callApex();
            this.isCancelledWindow = false;
        }
    }

    handleContactSelectionModalClose() {
        this.showContactSelectionModal = false;
    }

    handleContactSelected(event) {
        this.patientId = event.detail;
        this.showContactSelectionModal = false;
    }

    handleFilterModalToggle() {
        this.showFilterModal = !this.showFilterModal;
        if (!this.showFilterModal) {
            this.typeOptions = JSON.parse(JSON.stringify(this.originalFilters.typeOptions));
            this.sortOptions = JSON.parse(JSON.stringify(this.originalFilters.sortOptions));
            this.rangeOptions = JSON.parse(JSON.stringify(this.originalFilters.rangeOptions));
            this.statusOptions = JSON.parse(JSON.stringify(this.originalFilters.statusOptions));
        }
    }

    handleClick(event) {
        switch (event.target.name) {
            case 'scheduleVaccination':
                this.forVaccination = true;
                this.showContactSelectionModal = true;
                break;
            case 'scheduleVaccinationNew':
                this.dispatchEvent(new CustomEvent('initiatevaccinescheduler')); // newly added w.r.t S-23914
                break;
            case 'scheduleAppointment':
                this.forVaccination = false;
                this.showContactSelectionModal = true;
                break;
            case 'addDependent':
                if(this.contactList && this.contactList.length > 0 && 
                    parseInt(this.appointmentReminderLabel.rateLimit) <= (this.contactList.length - 1)){
                        this.dispatchEvent(new ShowToastEvent({
                            message: 'You have reached the max limit to add dependents.',
                            variant: 'error',
                            mode: 'sticky'
                        }));
                }
                else{
                    this.dispatchEvent(new CustomEvent('adddependent'));
                }
                break;
            default:
                break;
        }
    }

    hideRescheduleModal() {
        this.showCalendar = false;
        this.isCancelledWindow = false;
        this.callApex();
    }

    focusFirstEle() {
        const focusableElements = 'lightning-combobox, lightning-button-icon, button, input, lightning-helptext, lightning-input, lightning-button';
        const modal = this.template.querySelector('.slds-modal');
        if (modal) {
            const firstFocusableElement = modal.querySelectorAll(focusableElements)[0];
            const focusableContent = modal.querySelectorAll(focusableElements);
            const lastFocusableElement = focusableContent[focusableContent.length - 1];
            firstFocusableElement.focus();
            this.template.addEventListener('keydown', function (event) {
                let isTabPressed = event.key === 'Tab' || event.keyCode === 9;
                if (!isTabPressed) {
                    return;
                }
                if (event.shiftKey) {
                    if (this.activeElement === firstFocusableElement) {
                        lastFocusableElement.focus();
                        event.stopPropagation()
                        event.preventDefault();
                    }
                } else {
                    if (this.activeElement === lastFocusableElement) {
                        firstFocusableElement.focus();
                        event.preventDefault();
                        event.stopPropagation()
                    }
                }
            });
        }
    }

    handleKeyDown(event) {
        if (event.code == 'Escape') {
            this.isCancelledWindow = false;
            event.preventDefault();
            event.stopImmediatePropagation();
        }
    }

    // Made for Appointment type
    showModal = false;
    handleCloseTestModal() {
        this.showModal = false;
    }

    /* added by santhosh */
    showFilterPopup() {
        this.isFiltered = true;
    }
    processRecords(result) {
        this.doseNumVsPatientIdMap.clear();
        if (result) {
            let testingResultMap = new Map();
            if (result.results) {
                for (let key in result.results) {
                    if (result.results.hasOwnProperty(key)) {
                        testingResultMap.set(key, result.results[key]);
                    }
                }
            }
            if (result.contact) {
                let tmpContactList = [...result.contact];
                let tmpContacts = [];
                tmpContactList.forEach(contactRecord => {
                    let addressString = '';
                    if (contactRecord.MailingCity != null && typeof contactRecord.MailingCity != 'undefined') {
                        addressString += contactRecord.MailingCity;
                    }
                    if (contactRecord.MailingState != null && typeof contactRecord.MailingState != 'undefined') {
                        addressString += addressString == '' ? contactRecord.MailingState : ', ' + contactRecord.MailingState;
                    }
                    if (contactRecord.MailingPostalCode != null && typeof contactRecord.MailingPostalCode != 'undefined') {
                        addressString += ' ' + contactRecord.MailingPostalCode;
                    }
                    tmpContacts.push({
                        id: contactRecord.Id,
                        /*
                            @author : Priyanshu Nalwaya
                            @Story  : S-13623
                            @Date Modified : 12/8/2020
                        */
                        name: contactRecord.Parent_Contact__c == null ? contactRecord.Name : contactRecord.Name + ' ( D )',
                        // S-13623 Ends Here
                        streetAddress: contactRecord.MailingStreet,
                        address: addressString,
                        isCommunityUser: this.currentUserDetail.ContactId == contactRecord.Id,
                        birthdate: contactRecord.Birthdate
                    });
                });
                this.contactList = tmpContacts;
            }
            if (result.appointment) {
                let tmpAppointmentList = [...result.appointment];
                let tmpAppointments = [];
                let tempAppointmentIdVsData = {};
                tmpAppointmentList = JSON.stringify(tmpAppointmentList);
                tmpAppointmentList = JSON.parse(tmpAppointmentList);
                tmpAppointmentList.forEach(apt => {
                    apt.Event__r = (apt.Event__r) ? apt.Event__r :  {};
                    apt.Event__r.Private_Access__r = (apt.Event__r.Private_Access__r) ? apt.Event__r.Private_Access__r :  {};
                    let aptRecord = {};
                    let isCompleted = apt.Status__c == 'Completed' || apt.Status__c == 'Closed';
                    let isScheduled = apt.Status__c == 'Scheduled';
                    let isToBeScheduled = apt.Status__c == 'To Be Scheduled';
                    let isCancelled = apt.Status__c == 'Cancelled';
                    let isVaccination = apt.Lab_Center__r.Event_Process__c == 'Vaccination';
                    if (isCompleted || isScheduled) {
                        let vaccineTypeList = [];
                        if (!this.doseNumVsPatientIdMap.get(apt.Patient__c)) {
                            vaccineTypeList.push(apt.Event__r.Private_Access__r.Vaccine_Type__c);
                            this.doseNumVsPatientIdMap.set(apt.Patient__c, vaccineTypeList);
                        } else {
                            vaccineTypeList = this.doseNumVsPatientIdMap.get(apt.Patient__c);
                            vaccineTypeList.push(apt.Event__r.Private_Access__r.Vaccine_Type__c);
                            this.doseNumVsPatientIdMap.set(apt.Patient__c, vaccineTypeList);
                        }

                    }
                    aptRecord.id = apt.Id;
                    if (isScheduled && apt.Patient__r.Auto_Scheduled__c) {
                        aptRecord.startTime = this.msToTimeAMPM(apt.Appointment_Start_Time_v1__c);
                        aptRecord.startDate = apt.Appointment_Start_Date_v1__c;
                    }
                    if (isScheduled && !apt.Patient__r.Auto_Scheduled__c && apt.Appointment_Slot__c) {
                        aptRecord.startTime = isScheduled ? this.msToTimeAMPM(apt.Appointment_Slot__r.Start_Time__c) : this.msToTimeAMPM(apt.Patient__r.Preferred_Time__c);
                        aptRecord.startDate = isScheduled ? apt.Appointment_Slot__r.Date__c : apt.Patient__r.Preferred_Date__c;
                    }
                    if (isCancelled) {
                        aptRecord.startTime = this.msToTimeAMPM(apt.Appointment_Start_Time_v1__c);
                        aptRecord.startDate = apt.Appointment_Start_Date_v1__c;
                    }
                    let addressString = '';
                    if (apt.Lab_Center__c) {
                        if (apt.Lab_Center__r.BillingStreet != null &&
                            typeof apt.Lab_Center__r.BillingStreet != 'undefined') {
                            addressString += apt.Lab_Center__r.BillingStreet;
                        }
                        if (apt.Lab_Center__r.BillingCity != null && typeof apt.Lab_Center__r.BillingCity != 'undefined') {
                            addressString += addressString == '' ? apt.Lab_Center__r.BillingCity : ', ' + apt.Lab_Center__r.BillingCity;
                        }
                        if (apt.Lab_Center__r.BillingState != null && typeof apt.Lab_Center__r.BillingState != 'undefined') {
                            addressString += addressString == '' ? apt.Lab_Center__r.BillingState : ', ' + apt.Lab_Center__r.BillingState;
                        }
                        if (apt.Lab_Center__r.BillingPostalCode != null && typeof apt.Lab_Center__r.BillingPostalCode != 'undefined') {
                            addressString += ' ' + apt.Lab_Center__r.BillingPostalCode;
                        }
                        aptRecord.siteName = apt.Lab_Center__r.Name;
                    }
                    aptRecord.address = addressString;
                    aptRecord.isScheduled = isScheduled;
                    aptRecord.isCancelled = isCancelled;
                    aptRecord.isToBeScheduled = isToBeScheduled;
                    aptRecord.siteId = apt.Lab_Center__c;
                    aptRecord.eventDescription = apt.Event_Description__c;
                    aptRecord.isCompleted = isCompleted;
                    aptRecord.patientName = apt.Patient__r.Name;
                    aptRecord.patientId = apt.Patient__c;
                    aptRecord.isVaccination = isVaccination;
                    aptRecord.accountId = apt.Lab_Center__c;
                    aptRecord.eventId = apt.Event__c;
                    aptRecord.oldVaccineClass = '';
                    aptRecord.isSecondDose = false;
                    aptRecord.isSecondDoseAppointment = false;
                    aptRecord.appointmentType = apt.Lab_Center__r.Event_Process__c == 'Vaccination' ? 'COVID Vaccination' : 'COVID Testing';
                    aptRecord.isTesting = apt.Lab_Center__r.Event_Process__c === 'Vaccination' ? false : true;
                    aptRecord.eventProcess = apt.Lab_Center__r.Event_Process__c;
                    aptRecord.vaccineType = undefined;
                    aptRecord.isInitialDose = result.isInitialDose;
                    aptRecord.nextDoseButtonVisible = apt.nextDoseButtonVisible;
                    aptRecord.formattedDate = apt.formattedDate;
                    if (apt.hasOwnProperty('Event__r') && apt.Event__r.hasOwnProperty('Private_Access__r') && apt.Lab_Center__r.Event_Process__c == 'Vaccination') {
                        aptRecord.vaccineType = apt.Event__r.Private_Access__r.Vaccine_Type__c;

                        // added for Booster Dose
                        aptRecord.nextDoseNumber = apt.nextDoseNumber;
                        aptRecord.isNextDoseAvailable = apt.isNextDoseAvailable;
                    }

                    // S-14564 second dose story
                    aptRecord.isShowSecondDoseButton = false;
                    if (isVaccination) {
                        if (apt.Event__r && apt.Event__r.hasOwnProperty('Private_Access__r')) {
                            if (apt.Event__r.Private_Access__r.Vaccine_Type__c === 'Vaccination-2') {
                                aptRecord.isSecondDoseAppointment = true;
                            }
                        }
                    }
                    if (isVaccination && isCompleted) {
                        if (apt.hasOwnProperty('Event__r') && apt.Event__r.hasOwnProperty('Private_Access__r') && !apt.cannotScheduleNextDose) {
                            if (
                                apt.Event__r.Private_Access__r.Requires_Followup_Dose__c === 'Yes') {
                                aptRecord.isShowSecondDoseButton = !tmpAppointmentList.
                                find((a) =>
                                    (a.Patient__c === aptRecord.patientId &&
                                        a.Event_Process__c == 'Vaccination' &&
                                        (a.Status__c == 'Scheduled')));
                                aptRecord.isSecondDose = true;
                                aptRecord.isSecondDoseAppointment = false;
                                aptRecord.oldVaccineClass = apt.Event__r.Private_Access__r.Vaccine_Class_Name__c;
                            }
                        }
                    }

                    if (testingResultMap.has(apt.Id)) {
                        const testingResult = testingResultMap.get(apt.Id);
                        aptRecord.results = testingResult;

                        if (testingResult.result) {
                            this.hasResults = true;
                            aptRecord.hasResults = true;
                            aptRecord.isCompleted = false;
                        }
                    }
                    tempAppointmentIdVsData[aptRecord.id] = aptRecord;
                    tmpAppointments.push(aptRecord);
                });
                this.appointmentIdVsData = tempAppointmentIdVsData;
                this.allRecords = tmpAppointments;
                this.appointmentList = tmpAppointments;
                this.checkforCancelledAppointments(this.appointmentList);
            } else {
                this.showSpinner = false;
                this.appointmentList = [];
            }
        } else {
            this.showSpinner = false;
            this.appointmentList = [];
        }
    }

    handleChangeStatus(event) {
        this.handleActiveCheckboxClass(event);
        let queryValue = this.statusOptions[event.currentTarget.dataset.index].queryValue;
        this.statusOptions[event.currentTarget.dataset.index].value = event.target.checked;
        this.selectedStatus = (!event.target.checked && this.selectedStatus) ? this.selectedStatus.filter(val => val !== queryValue) : this.selectedStatus;
        event.target.checked && this.selectedStatus && !this.selectedStatus.includes(queryValue) && this.selectedStatus.push(queryValue);
    }
    handleChangeSort(event) {
        this.sortOptions.forEach((element => {
            element.value = false;
            element.class = 'slds-checkbox';
        }));
        this.sortOptions[event.currentTarget.dataset.index].value = event.target.checked;
        this.sort = (!event.target.checked) ? 'Appointment_Date_Time_Complete__c' : this.sortOptions[event.currentTarget.dataset.index].queryValue;
        this.handleActiveCheckboxClass(event);


    }
    handleChangeRange(event) {
        this.rangeOptions.forEach((element => {
            element.value = false;
            element.class = 'slds-checkbox';
        }));
        this.rangeOptions[event.currentTarget.dataset.index].value = event.target.checked;
        this.range = (event.target.checked && this.rangeOptions[event.currentTarget.dataset.index].queryValue != 'All') ?
            this.rangeOptions[event.currentTarget.dataset.index].queryValue : 'All';
        this.handleActiveCheckboxClass(event);
    }

    handleChangeType(event) {
        this.handleActiveCheckboxClass(event);
        this.typeOptions[event.currentTarget.dataset.index].value = event.target.checked;
    }

    handleActiveCheckboxClass(event) {
        var filterType = event.currentTarget.dataset.type;
        switch (filterType) {
            case 'type':
                this.typeOptions[event.currentTarget.dataset.index].class = (event.target.checked) ? 'slds-checkbox active' : 'slds-checkbox';
                break;
            case 'status':
                this.statusOptions[event.currentTarget.dataset.index].class = (event.target.checked) ? 'slds-checkbox active' : 'slds-checkbox';
                break;
            case 'range':
                this.rangeOptions[event.currentTarget.dataset.index].class = (event.target.checked) ? 'slds-checkbox active' : 'slds-checkbox';
                break;
            case 'sort':
                this.sortOptions[event.currentTarget.dataset.index].class = (event.target.checked) ? 'slds-checkbox active' : 'slds-checkbox';
                break;

            default:
                break;
        }
    }

    // added by vamsi 
    handleApplyFilters() {
        // this would bring filtered appointments 
        this.fromDashboard = false;
        this.callApex();
        this.updatePillFilter();
        this.handleFilterModalToggle();
    }


}