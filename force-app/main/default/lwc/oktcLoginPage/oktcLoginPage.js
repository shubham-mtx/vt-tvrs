import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import login from '@salesforce/apex/okpcLoginController.login';
import forgotPasswordURL from '@salesforce/label/c.OKTC_Forgot_Password_URL';
import communityURL from '@salesforce/label/c.OKTC_Community_URL';
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';

export default class OktcLoginPage extends NavigationMixin(LightningElement) {


    @track showError = false;
    @track errorMessage = '';
    @track username = '';
    @track password = '';

    customImage = nysdohResource;
    navigateToForgotPassword() {
        location.href = forgotPasswordURL;
    }
    navigateToHome(data) {
        location.href = data;
    }

    nameChange(event) {
        this.username = event.target.value;
    }
    passwordChange(event) {
        this.password = event.target.value;
    }

    doLogin() {
       // login({ username: this.username, password: this.password, startUrl: communityURL + 's/', delimiter: '.testsiteuser' }) //Added delimiter by Sajal
        login({ username: this.username, password: this.password, startUrl: communityURL + 's/', delimiter: '' }) //removed by Abhimanyu
            .then(data => {
                this.navigateToHome(data);
            })
            .catch(error => {
                this.showError = true;
                showAsyncErrorMessage(this,error);  
                var msg = JSON.stringify(error.body.message);
                this.errorMessage = msg.substring(1, msg.length - 1);
            });
    }

}