/**
 * @description       : 
 * @author            : Prashant Gupta
 * @group             : 
 * @last modified on  : 12-18-2020
 * @last modified by  : Saloni Adlakha
 * Modifications Log 
 * Ver   Date         Author           Modification
 * 1.0   12-15-2020   Prashant Gupta   Initial Version
**/
public without sharing class PUA_UpdateEmailController {
    
    @AuraEnabled
    public static ContactWrapper doInitConAddress(){
        try{
            User user = [SELECT Id,ContactId FROM user WHERE Id =: UserInfo.getUserId()];
            Contact contact = [SELECT Id, Email, Phone,
                MailingStreet, MailingCity, MailingState, MailingPostalCode
                FROM Contact
                WHERE  Id=: user.ContactId
            ];
            ContactWrapper wrapper = (new ContactWrapper()).getContactAddress(contact);
            return wrapper; 
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        } 
    }

        
    @AuraEnabled
    public static Boolean getContactUserDetail(String currentcontactId){
        try{
            if(!VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS) && 
               !VT_SecurityLibrary.UserHasProfileAccess(new List<String>{PUA_Constants.PROFILE_Claim_Center_Staff,PUA_Constants.PROFILE_CLAIM_CENTER_STAFF_ADMIN,PUA_Constants.PROFILE_CLAIM_CENTER_STAFF_READ_ONLY,PUA_Constants.PROFILE_STANDARD_VT_USER,PUA_Constants.PROFILE_SYSTEM_ADMINISTRSTOR} )){
                throw new VT_Exception(System.Label.VT_Unauthorised_Access_Error_Message);
            }
            List<User> userList = [SELECT Id,ContactId FROM user WHERE ContactId =:currentcontactId];
            if (userList != null && userList.size() > 0) {
                return true;
            }else{
                return false;
            }
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }  
    }
    
    @AuraEnabled
    public static ContactWrapper doInitContactAddress(String contactId){
        try{
            if(!VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS) && 
               !VT_SecurityLibrary.UserHasProfileAccess(new List<String>{PUA_Constants.PROFILE_Claim_Center_Staff,PUA_Constants.PROFILE_CLAIM_CENTER_STAFF_ADMIN,PUA_Constants.PROFILE_CLAIM_CENTER_STAFF_READ_ONLY,PUA_Constants.PROFILE_STANDARD_VT_USER,PUA_Constants.PROFILE_SYSTEM_ADMINISTRSTOR} )){
                throw new VT_Exception(System.Label.VT_Unauthorised_Access_Error_Message);
            }
            Contact contact = [SELECT Id, Email, Phone,
                MailingStreet, MailingCity, MailingState, MailingPostalCode
                FROM Contact
                WHERE Id =: contactId
            ];
            ContactWrapper wrapper = (new ContactWrapper()).getContactAddress(contact);
            return wrapper; 
        }catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        } 
    }

    @AuraEnabled
    public static void saveContactAddress(String responseWrapper, String contactId){
        ContactWrapper wrap;
        try{
            if(!VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS) && 
               !VT_SecurityLibrary.UserHasProfileAccess(new List<String>{PUA_Constants.PROFILE_Claim_Center_Staff,PUA_Constants.PROFILE_CLAIM_CENTER_STAFF_ADMIN,PUA_Constants.PROFILE_CLAIM_CENTER_STAFF_READ_ONLY,PUA_Constants.PROFILE_STANDARD_VT_USER,PUA_Constants.PROFILE_SYSTEM_ADMINISTRSTOR} )){
                throw new VT_Exception(System.Label.VT_Unauthorised_Access_Error_Message);
            }
            if(!String.isEmpty(responseWrapper) && !String.isEmpty(contactId)){
                wrap = (ContactWrapper) JSON.deserialize(responseWrapper, ContactWrapper.class);
                    Contact con = new Contact();
                    con.Id = contactId;
                    con.Phone = wrap.phone;
                    con.MailingStreet = wrap.mailingStreet;
                    con.MailingCity = wrap.mailingCity;
                    con.MailingState = wrap.mailingState;
                    con.MailingPostalCode = wrap.mailingPostalCode;
                    con.Email = wrap.conEmail;
                    update con;
           } 
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }  
    }

    @AuraEnabled
    public static UserWrapper doInitUserDetails(String contactId){
        if(!VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS) && 
           !VT_SecurityLibrary.UserHasProfileAccess(new List<String>{PUA_Constants.PROFILE_Claim_Center_Staff,PUA_Constants.PROFILE_CLAIM_CENTER_STAFF_ADMIN,PUA_Constants.PROFILE_CLAIM_CENTER_STAFF_READ_ONLY,PUA_Constants.PROFILE_STANDARD_VT_USER,PUA_Constants.PROFILE_SYSTEM_ADMINISTRSTOR} )){
            throw new AuraHandledException(System.Label.VT_Unauthorised_Access_Error_Message);
        }


        User user = [SELECT Id, Username,Email FROM User WHERE ContactId =: contactId];
        UserWrapper wrapper = (new UserWrapper()).getUserDetails(user);
        return wrapper; 
    }

    @AuraEnabled
    public static UserInfoWrapper saveUserDetails(String responseWrapper , String userId){
        UserWrapper wrap;
        UserInfoWrapper userWrap = new UserInfoWrapper();
        try{
            if(!VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS) && 
               !VT_SecurityLibrary.UserHasProfileAccess(new List<String>{PUA_Constants.PROFILE_Claim_Center_Staff,PUA_Constants.PROFILE_CLAIM_CENTER_STAFF_ADMIN,PUA_Constants.PROFILE_CLAIM_CENTER_STAFF_READ_ONLY,PUA_Constants.PROFILE_STANDARD_VT_USER,PUA_Constants.PROFILE_SYSTEM_ADMINISTRSTOR} )){
                throw new VT_Exception(System.Label.VT_Unauthorised_Access_Error_Message);
            }
            if(!String.isEmpty(responseWrapper) && !String.isEmpty(userId)){
                wrap = (UserWrapper) JSON.deserialize(responseWrapper, UserWrapper.class);
                    User user = new User();
                    user.Id = userId;
                    user.Email = wrap.email;
                    user.Username = wrap.username;
                    if (!getUserExist(user.username)) {
                        userWrap.error = 'Username already exists';
                        return userWrap;
                    }
                    update user;
            } 
            return null;
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }  
    }

    public static Boolean getUserExist(String username) {
        List < User > listOfUser = DAO_SOS_User.selectByUsername(username);
        return listOfUser.isEmpty();
    }

    @AuraEnabled
    public static String resetPassword(String username) {

        if(!VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS) && 
           !VT_SecurityLibrary.UserHasProfileAccess(new List<String>{PUA_Constants.PROFILE_Claim_Center_Staff,PUA_Constants.PROFILE_CLAIM_CENTER_STAFF_ADMIN,PUA_Constants.PROFILE_CLAIM_CENTER_STAFF_READ_ONLY,PUA_Constants.PROFILE_STANDARD_VT_USER,PUA_Constants.PROFILE_SYSTEM_ADMINISTRSTOR} )){
            throw new AuraHandledException(System.Label.VT_Unauthorised_Access_Error_Message);
        }


        User user =[SELECT Id FROM User WHERE Username =: username];
        if(!Site.isValidUsername(username)) {
            return Label.Site.invalid_email;
        }
        if(username!= null || username!=''){
            System.resetPassword(user.Id,true);          
        } else{
        }
        return 'success';
    }

    public class ContactWrapper{
        @AuraEnabled public String Id;
        @AuraEnabled public String phone;
        @AuraEnabled public String mailingStreet;
        @AuraEnabled public String mailingState;
        @AuraEnabled public String mailingCity;
        @AuraEnabled public String mailingPostalCode;
        @AuraEnabled public String conEmail;
        public ContactWrapper getContactAddress(Contact con) {
            try{
                ContactWrapper wrap = new ContactWrapper();
                wrap.Id = con.Id;
                wrap.phone =  con.Phone;
                wrap.mailingStreet = con.MailingStreet;
                wrap.mailingCity = con.MailingCity;
                wrap.mailingState = con.MailingState;
                wrap.mailingPostalCode = con.MailingPostalCode;
                wrap.conEmail = con.Email; 
                return wrap;
            }
            catch(Exception ex){
                throw new AuraHandledException(ex.getMessage());
            }
        }
    }

    public class UserWrapper{
        @AuraEnabled public String Id;
        @AuraEnabled public String username;
        @AuraEnabled public String email;
        public UserWrapper getUserDetails(User user) {
            try{
                UserWrapper wrap = new UserWrapper();
                    wrap.Id =  user.Id;
                    wrap.username = user.Username;
                    wrap.email = user.Email; 
                    return wrap;
            }catch(Exception ex){
                throw new AuraHandledException(ex.getMessage());
            }
        } 
    }

    public class UserInfoWrapper{
        @AuraEnabled public String error {
            get;
            set;
        }
        public UserInfoWrapper(){
            this.error = null;
        }
    }

}