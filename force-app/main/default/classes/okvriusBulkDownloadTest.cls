@isTest
public class okvriusBulkDownloadTest {
    @TestSetup
    static void makeData(){
        
        String recordTypeID = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        String citizenCovidId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
      
        //String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String labRecordId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
        //String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;
        
        Account labAccount = TestDataFactoryAccount.createAccount('testLab', labRecordId, true);
        
        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0)
        );
        insert con;
        
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType IN : customerUserTypes  limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.comtestException',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            //userRoleId = portalRole.Id,
            contactId = con.id
        );
        insert newUser;
        
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        createAppointment.Status__c = 'Scheduled';
        
        createAppointment.Appointment_Start_Date_v1__c=date.today();
        insert createAppointment;
        
        
    }
    public static testMethod void getAppointmentBulkTest(){
        List<Contact> contactList = [Select Id , Name,Testing_Site__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Appointment__c> appList = [Select id,Name,Patient__r.MobilePhone,Patient__r.email,Patient__r.Testing_Site__c,
                                        Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,Patient__r.What_type_of_record_do_you_have_question__c,Patient__r.Which_specialist_do_you_need__c,
                                        Patient__r.Account.Name,status__c,Appointment_Start_Time__c,Formatted_Appointment_Start_Date__c,Reason_for_cancellation__c,Patient__r.Need_Spanish_Interpreter__c,
                                        createddate,Formatted_Appointment_Start_Time__c,Formatted_appointment_Complete_Date__c,Appointment_Schedule_Generated_Date__c,Appointment_Complete_Date__c
                                        FROM Appointment__c 
                                        WHERE Patient__c =:contactList[0].Id LIMIT 1 ];
        
        String labId = String.valueOf(appList[0].Patient__r.Testing_Site__c);
        System.debug('labid--'+labId);
        Date TodateChnage = date.today();
        Date FromdateChange =  Date.newInstance(2020, 01, 01);
        
        List<okvriusBulkDownload.AppointmentWrapper> lastAppWrapperResult= okvriusBulkDownload.getAppointmentBulk(labId,TodateChnage,FromdateChange);
        system.assert(!lastAppWrapperResult.isEmpty());
        
        User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{
                okvriusBulkDownload.getAppointmentBulk(labId,TodateChnage,FromdateChange);
            }catch(Exception e){
                
            }
        }
    }
    
    public static testMethod void insertDataTest(){
        List<Contact> contactList = [Select Id , Name,Testing_Site__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Appointment__c> appList = [Select id,Name,Patient__r.MobilePhone,Patient__r.email,Patient__r.Testing_Site__c,
                                        Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,Patient__r.What_type_of_record_do_you_have_question__c,Patient__r.Which_specialist_do_you_need__c,
                                        Patient__r.Account.Name,status__c,Appointment_Start_Time__c,Formatted_Appointment_Start_Date__c,Reason_for_cancellation__c,Patient__r.Need_Spanish_Interpreter__c,
                                        createddate,Formatted_Appointment_Start_Time__c,Formatted_appointment_Complete_Date__c,Appointment_Schedule_Generated_Date__c,Appointment_Complete_Date__c
                                        FROM Appointment__c 
                                        WHERE Patient__c =:contactList[0].Id LIMIT 1 ];
        String appointmentId = appList[0].Id;
        
        String jsonData = '[{"appointmentId":"'+appointmentId+'","noShow":"yes","appointmentStatus":"Scheduled"}]'; 
        String jsonData2 = '[{"appointmentId":"'+appointmentId+'","noShow":"yes","appointmentStatus":"Completed"}]'; 
        
        
        Test.startTest();
        Map<String,Object> resultMap = okvriusBulkDownload.insertData(jsonData);
        System.assertEquals('success', resultMap.get('type'));
        Map<String,Object> resultMapNew = okvriusBulkDownload.insertData(jsonData2);
        System.assertEquals('error', resultMapNew.get('type'));
        
        User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{
                okvriusBulkDownload.insertData(jsonData2);
            }catch(Exception e){
                
            }
        }
        Test.stopTest();
    }
}