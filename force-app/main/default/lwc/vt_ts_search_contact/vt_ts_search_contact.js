import { LightningElement,track, api } from 'lwc';
import getContactList from '@salesforce/apex/VT_TS_GetContacts.getContactList';

export default class Vt_ts_search_contact extends LightningElement {
    @api selectedContacts;
    @api modalHeader = 'Contact';
    @api needParentContactOnly;
    @api alreadySelectedContact;

    @track showDatatable;
    @track isContact;
    @track error;
    @track firstInitial = "";
    @track filterContacts;
    @track showNoRecords = false;
    @track lastInitial = "";
    @track dob;
    @track patient = "";
    @track isDisable;
    @track columns = [{
        label: 'Patient Id',
        fieldName: 'Patient_Id__c',
        type: 'text',
        sortable: true
    },
    {
        label: 'Last Name',
        fieldName: 'LastName',
        type: 'text',
        sortable: true
    },
    {
        label: 'First Name',
        fieldName: 'FirstName',
        type: 'text',
        sortable: true
    },
    {
        label: 'Email',
        fieldName: 'Email',
        type: 'email',
        sortable: true
    },
    {
        label: 'Birthdate',
        fieldName: 'Birthdate',
        type: 'date-local',
        sortable: true
    },
    {
        label: 'Address',
        fieldName: 'Combined_Address__c',
        type: 'text',
        sortable: true
    }
];
    showSpinner;

    connectedCallback(){
        if(this.modalHeader === 'Contact'){
            this.isContact = true;
        }
    }

    handleChange(event){
        if(event.target.name === 'fname'){
            this.firstInitial = event.target.value;
            
        }else if(event.target.name === 'lname'){
            this.lastInitial = event.target.value;
            
        }else if(event.target.name === 'birthdate'){
            this.dob = event.target.value;

        }else if(event.target.name === 'patientId'){
            this.patient = event.target.value;
        }
    }

    handleDOB(event) {
        this.dob = event.detail;
    }

    handleClick(){
        this.showSpinner = true;
        this.showDatatable = false;
        if(this.isValid()){
            if(this.patient){
                this.template.querySelector('c-vt_ts_custom_date').cleanErrorParent();
            }
            if(this.dob){
                if(!this.template.querySelector('c-vt_ts_custom_date').isValid()){
                    return;
                }
            }
            this.showNoRecords = true;
            this.showDatatable = true;
            getContactList(
                {
                    firstInitial : this.firstInitial, 
                    lastInitial : this.lastInitial, 
                    dob : this.dob, 
                    patientId : this.patient,
                    needParentContactOnly : this.needParentContactOnly,
                    alreadySelectedContact : this.alreadySelectedContact
                }
            )
            .then(data=> {   
                if(data){
                    if(data.length === 0){
                        this.showNoRecords = true;
                    }else{
                        this.showNoRecords = false;
                        this.filterContacts = data;
                    }
                    this.showSpinner = false;
                }
            }).catch(error => {
                this.showSpinner = false;
            });
        }
    }
        
    handleRowSelection = event => {
        this.selectedContacts = event.detail.selectedRows;
    }

    selectedRecords(){

   
        const selectedEvent = new CustomEvent('next', { detail: this.selectedContacts });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
        this.showDatatable = false;
      
        
    }

    isValid(){
        this.error = undefined;
        
        if(this.modalHeader == 'Contact') {
            if((!this.firstInitial || !this.lastInitial || !this.dob) && !this.patient) {
                this.error = 'First Name, Last Name and Date of Birth or Patient ID are Required for Search.';
                this.showSpinner = false;
                return false;
            }
        } else {
            if((!this.firstInitial || !this.lastInitial || !this.dob)) {
                this.error = 'First Name, Last Name and Date of Birth are Required for Search.';
                this.showSpinner = false;
                return false;
            }
        }


        return true;
    }

    handleCancel(){
        const selectedEvent = new CustomEvent('cancel');
        this.dispatchEvent(selectedEvent);
    }
    
}