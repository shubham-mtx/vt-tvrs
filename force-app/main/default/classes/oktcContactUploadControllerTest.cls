/**
 * @description       : 
 * @author            : Vamsi Mudaliar
 * @group             : 
 * @last modified on  : 12-28-2021
 * @last modified by  : Vamsi Mudaliar
**/
@isTest
public class oktcContactUploadControllerTest {
    @TestSetup
    static void makeData(){
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        
        //User user = TestDataFactoryAccount.createUser(true);
        
        Account acc = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        acc.BillingCity = 'testCity';
        acc.BillingCountry = 'testCountry';
        acc.BillingState = 'testState';
        acc.BillingStreet = 'testStreet';
        acc.BillingPostalCode = '12345';
        acc.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        acc.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert acc;
        
        Contact con = new Contact (
            AccountId = acc.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = acc.Id,
            //RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0)
        );
        insert con;


        Contact con1 = new Contact (
            AccountId = acc.id,
            LastName = 'portalUserv1Demo',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = acc.Id,
            //RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0)
        );
        insert con1;

        Date eligibleEventDate = System.today() - Integer.valueOf(System.Label.Bulk_Upload_Event_Past_Days);
        System.debug('@@@@@@@@@@@'+eligibleEventDate);
        VTS_Event__c event = new VTS_Event__c(
        	Location__c = acc.Id,
            Start_Date__c = eligibleEventDate
        );
        insert event;
        
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where Name = 'Covid Citizen User'  limit 1];  
        Profile prfile1 = [select Id,name from Profile where Name = 'Testing Site Community Profile'  limit 1];  
      	User newUser = new User(
            profileId = prfile.id,
            username = 'testUser@gmail.com.test.citizen',
            Email = 'testUser@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = con.id,
            IsActive = true
        );
        insert newUser;
        User newUser1 = new User(
            profileId = prfile1.id,
            username = 'testUser@gmail.com.testingUser',
            Email = 'testUser@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='randomLast',
            contactId = con1.id,
            IsActive = true
        );
        insert newUser1;
    }
    
    @isTest
    public static void testGetEvents(){
        Account acc = [Select Id from Account limit 1];
        User user = [SELECT Id FROM User WHERE username = 'testUser@gmail.com.testingUser'];
            System.runAs(user){
                oktcContactUploadController.eventWrapper obj = new oktcContactUploadController.eventWrapper(null,null,Date.today(),Date.today());
                oktcContactUploadController.constructorResultWrapper test = oktcContactUploadController.getEvents(acc.Id,'Vaccination');
                system.assertEquals(true, test != null, 'testGetEvents');
            }
        // catch(Exception e){
        //     System.assert(true, 'ERROR');
        // }
    }

    @isTest
    public static void testGetEvents1(){
        Account acc = [Select Id from Account limit 1];
        User user = [SELECT Id FROM User WHERE username = 'testUser@gmail.com.test.citizen'];
        System.runAs(user){
        	List<Account> test = oktcContactUploadController.getAccountDetails();
            system.assertEquals(true, test.size() > 0, 'testGetEvents1');
        }
    }

    @isTest
    public static void testInsertContacts(){
        Date eligibleEventDate = System.today() - Integer.valueOf(System.Label.Bulk_Upload_Event_Past_Days);
        VTS_Event__c event = [SELECT Id FROM VTS_Event__c WHERE Start_Date__c =:eligibleEventDate];
        String inputString = '[{"consent":"true","agreeToRegisterForSomeone":"Yes","firstName":"Shobhit","LastName":"Pant","email":"pantshobhit@gmail.com","Mobile":"8559880111","phoneType":"Mobile","eighteenPlus":"yes","receiveMethod":"E-Mail","receiveViaEmail":"yes","receiveViaSMS":"yes","dateOfBirth":"10/30/1995","streetAddress":"123","City":"test city","State":"vt","Gender":"Male","Zip":"12345","preferredLanguage":"English","Race":"Asian","Ethnicity":"Other","firstCovidTest":"No","employedInHealthCare":"No","symptomatic":"Yes","symptomaticOnsetDate":"08/17/2020","residentInCongregate":"Yes","pregnant":"No","reasonForTesting":"I am worried / concerned about an outbreak in my community","hasInsurance":"Yes","insuredAuthorizedForPayment":"TRUE","patientAuthorizationToReleaseInfo":"TRUE","insuranceType":"Medicare","insuranceIdNumber":"12345","insurancePlanName":"test program","fecaNumber":"123ds","reationToInsured":"Self","insuredFirstName":"Shobhit","insuredMiddleName":"test","insuredLastName":"paant","insuredStreetAddress":"123","insuredCity":"testttt city insured","insuredState":"vt","insuredZip":"12345","insuredTelephone":"1123456432","insuredDOB":"10-30-1995","insuredSex":"Male","anotherHealthPlan":"yes","addInPlanName":"add plan test","addInPolicyGroupNo":"123dsfsdf","addInFirstName":"test name 3","addInMiddleName":"middle name 3","addInLastName":"last name 3","agreetoTheTerms":"TRUE","nameOfParent":"Shobhit","I agree  (Minor State Representative Consent)":"FALSE"},{"consent":""}]';
        InsertContactQueueable.ResultWrapper testList = oktcContactUploadController.insertContacts(inputString, event.Id);
        system.assertEquals(true, testList != null, 'testInsertContacts');
    }

}