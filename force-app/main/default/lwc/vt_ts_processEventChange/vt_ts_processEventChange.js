import { LightningElement, api , wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import EVENT_ACCOUNT from '@salesforce/schema/VTS_Event__c.Location__c';
import EVENT_START_DATE from '@salesforce/schema/VTS_Event__c.Start_Date__c';
import EVENT_END_DATE from '@salesforce/schema/VTS_Event__c.End_Date__c';
import EVENT_STATUS from '@salesforce/schema/VTS_Event__c.Status__c';
import EVENT_PROCESS from '@salesforce/schema/VTS_Event__c.Location__r.Event_Process__c';
import updateEvent from '@salesforce/apex/VT_TS_ProcessEventChangeController.updateEvent';
import getMatchingEventsOfNewLocation from '@salesforce/apex/VT_TS_ProcessEventChangeController.getMatchingEventsOfNewLocation';
import hasPermission from '@salesforce/customPermission/VTS_Event_Modifiers';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';
export default class Vt_ts_processEventChange extends LightningElement {
    @api recordId;

    filters = ' Active__c = true AND Event_Type__c = \'State-Sponsored\' ';
    oldAccount;
    modalContent;
    takeConfirmation;
    selectedAccount;
    showSpinner;
    
    @wire(getRecord, { recordId: '$recordId', fields: [ EVENT_STATUS, EVENT_START_DATE, EVENT_END_DATE, EVENT_ACCOUNT, EVENT_PROCESS ] } )
    getOldAccountRecord ({error, data}) {
        if (error) {
        } else if (data) {
            if(getFieldValue(data, EVENT_STATUS) === 'Canceled') {
                this.dispatchEvent(new CustomEvent('permissionerror'));
                this.dispatchEvent(new ShowToastEvent({
                    message: 'Location cannot be modified for Canceled events.',
                    variant: 'error'
                })); 
            } else {
                this.oldAccount = data;
                this.filters += " AND RecordTypeId = \'"+data.fields.Location__r.value.recordTypeId+"\' AND Event_Process__c = \'"+getFieldValue(data, EVENT_PROCESS)+"\' AND Id != \'"+getFieldValue(data, EVENT_ACCOUNT)+"\' ";
                this.oldAccountName = data.fields.Location__r.displayValue;
            }
        }
    }

    get disableButton() {
        return (this.selectedAccount)?false:'disabled';
    }

    connectedCallback() {
        if(!hasPermission) {
            this.dispatchEvent(new CustomEvent('permissionerror'));
            this.dispatchEvent(new ShowToastEvent({
                message: 'You do not have enough permission to access this',
                variant: 'error'
            }));
        }
    }

    handleSelection(event) {
        this.selectedAccount = event.detail.data.selectedId;
    }

    modalCancel() {
        this.takeConfirmation = false;
    }

    changeConfirmed() {
        this.takeConfirmation = false;
        this.showSpinner = true;
        updateEvent({ data: 
            {
                eventId : this.recordId, 
                newLocationId : this.selectedAccount, 
                oldLocationId : this.oldAccount.fields.Location__c.value
            } 
        })
        .then(() => {
            this.showSpinner = false;
            this.dispatchEvent(new ShowToastEvent({
                message: 'Location Changed Succesfully. It might take couple of minutes to reflect on manage appointment slot window.',
                variant: 'success'
            }));
            this.dispatchEvent(new CustomEvent('recordsave'));
        })
        .catch(error => {
            this.showSpinner = false;
            showAsyncErrorMessage(this,error);
            this.dispatchEvent(new ShowToastEvent({
                message: error.body.message,
                variant: 'error'
            }));
        });
    }

    openModalConfirmation() {
        this.showSpinner = true;
        getMatchingEventsOfNewLocation({
            inputData : {
                newLocationId : this.selectedAccount
            },
            eventStartDate : getFieldValue(this.oldAccount, EVENT_START_DATE),
            eventEndDate : getFieldValue(this.oldAccount, EVENT_END_DATE)
        })
        .then(result => {
            this.showSpinner = false;
            const isEmpty = val => val == null || !(Object.keys(val) || val).length;
            if(!isEmpty(result)) {
                this.modalContent = 'An event already exists for the date and location. Please confirm before changing.';
            } else {
                this.modalContent = 'Are you sure you want to change this Event to selected Location?';
            }
            this.takeConfirmation = true;
        })
        .catch(error => {
            this.showSpinner = false;
            showAsyncErrorMessage(this,error);
            this.dispatchEvent(new ShowToastEvent({
                message: error.body.message,
                variant: 'error'
            }));
        });
    }
}