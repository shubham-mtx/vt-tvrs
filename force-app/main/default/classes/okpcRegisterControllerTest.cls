@isTest
public class okpcRegisterControllerTest {
		private static final string citizenId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        private static final string citizenAccount = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
    @testSetup static void setup() {
    
       

        Account createAccountCitizen = TestDataFactoryAccount.createAccount('TestSite', citizenAccount, false);
        createAccountCitizen.BillingCity = 'testCity';
        createAccountCitizen.BillingCountry = 'testCountry';
        createAccountCitizen.BillingState = 'testState';
        createAccountCitizen.BillingStreet = 'testStreet';
        createAccountCitizen.BillingPostalCode = '12345';
        createAccountCitizen.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountCitizen.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountCitizen;

        Contact con = new Contact (
            AccountId = createAccountCitizen.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-44151',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            RecordTypeId = citizenId,
            Login_Attempt_Count__c = 2
            
        );
        insert con;
        Database.DMLOptions dml = new Database.DMLOptions();
		dml.DuplicateRuleHeader.AllowSave = true; 
        
        Contact con4 = new Contact (
            AccountId = createAccountCitizen.id,
            LastName = 'portalTestUserv2',
            MiddleName = 'm',
            FirstName = 'testFirst2',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Female',
            MobilePhone = '(702)379-40151',
            Phone = '(702)379-40151',
            Street_Address1__c ='testStreet11',
            Street_Address_2__c ='testStreet21',
            City__c = 'TestCity1',
            State__c = 'OH',
            ZIP__c = '12310',
            RecordTypeId = citizenId,
            Login_Attempt_Count__c = 2
            
        );
        Database.insert(con4, dml); 
        
        
        
        
         Contact con1 = new Contact (
            AccountId = createAccountCitizen.id,
            LastName = 'portalTestUserv2',
            MiddleName = 'm11',
            FirstName = 'testFirs1111t',
            Email = 'testportalTestUserv111111@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(712)379-44151',
            Phone = '(712)379-44151',
            Street_Address1__c ='testStr11eet1',
            Street_Address_2__c ='testStr11eet2',
            City__c = 'Test11City',
            State__c = 'OH',
            ZIP__c = '12111',
            RecordTypeId = citizenId
        );
        insert con1;
        
        Contact con2 = new Contact (
            AccountId = createAccountCitizen.id,
            LastName = 'testForgotPassword',
            MiddleName = 'm11',
            FirstName = 'testFirs1111t',
            Email = 'testforgotpassword@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(712)379-44151',
            Phone = '(712)379-44151',
            Street_Address1__c ='testStr11eet1',
            Street_Address_2__c ='testStr11eet2',
            City__c = 'Test11City',
            State__c = 'OH',
            ZIP__c = '12111',
            RecordTypeId = citizenId
        );
        insert con2;

        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountCitizen.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
        createAppointment.Lab_Center__c = createAccountCitizen.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        
    }

    public static testMethod void test_01() {
        //List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'testLast' LIMIT 1];
        
        List<Contact> contactList = [Select Id , Name , Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        contactList[0].Login_Attempt_Count__c = 2;
        update contactList;
        
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'testUser@gmail.com.test.citizen',
            Email = 'testUser@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id,
            IsActive = true
        );
        insert newUser;

        System.runAs(newUser){
            try{
                Map < String, Object > mapVal = okpcRegisterController.selfRegister(contactList[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(Date.today().addYears(-22)));
                system.assertEquals(true, !mapVal.isEmpty(), 'test_01');
            }catch(Exception e){
                
            }
            
        }
    }
    
      public static testMethod void test_010() {
        //List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'testLast' LIMIT 1];
        
        List<Contact> contactList = [Select Id , Name ,Login_Attempt_Count__c,birthdate, Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        contactList[0].Login_Attempt_Count__c = 2;
        update contactList[0];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'testUser@gmail.com.test.citizen',
            email = 'testUser@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id,
            IsActive = true
        );
        insert newUser;

        System.runAs(newUser){
            try{
                Map < String, Object > mapVal = okpcRegisterController.selfRegister(contactList[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(Date.today().addYears(-22)));
                system.assertEquals(true, !mapVal.isEmpty(), 'test_010');
            }catch(Exception e){
                
            }
            
        }
    }
    
     public static testMethod void test_011() {
        //List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'testLast' LIMIT 1];
        
        List<Contact> contactList = [Select Id , Name ,Login_Attempt_Count__c, birthdate, Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        contactList[0].Login_Attempt_Count__c = 2;
        contactList[0].birthdate = (Date.today().addYears(-22));
        update contactList[0];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'testUser@gmail.com.test.citizen',
            email = 'testUser@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id,
            IsActive = true
        );
        insert newUser;
		Date bday = Date.newInstance(1994, 5, 5);
        System.runAs(newUser){
            try{
                Map < String, Object > mapVal = okpcRegisterController.selfRegister(contactList[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(Date.today().addYears(-22)));
        		Map < String, Object > mapVal2 = okpcRegisterController.selfRegister(contactList[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(bday));
                system.assertEquals(true, !mapVal2.isEmpty(), 'test_011');
            }catch(Exception e){
                
            }
            
        }
    }
    
    public static testMethod void test_012() {
        //List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'testLast' LIMIT 1];
        
        List<Contact> contactList = [Select Id , Name ,Login_Attempt_Count__c, birthdate, Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        contactList[0].Login_Attempt_Count__c = 3;
        contactList[0].birthdate = (Date.today().addYears(-22));
        update contactList[0];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'testUser@gmail.com.test.citizen',
            email = 'testUser@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id,
            IsActive = true
        );
        insert newUser;

        System.runAs(newUser){
            try{
                Map < String, Object > mapVal = okpcRegisterController.selfRegister(contactList[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(Date.today().addYears(-22)));
            	Map < String, Object > mapVal2 = okpcRegisterController.selfRegister(contactList[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen','','5-5-1994');
                system.assertEquals(true, !mapVal2.isEmpty(), 'test_012');
            }catch(Exception e){
                
            }
            

        }
    }

    
      public static testMethod void test_013() {
        //List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'testLast' LIMIT 1];
        
        List<Contact> contactList = [Select Id , Name ,Login_Attempt_Count__c, birthdate, Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        contactList[0].Login_Attempt_Count__c = 0;
        contactList[0].birthdate = (Date.today().addYears(-22));
        update contactList[0];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'maktestmessi@gmail.com.test.citizen',
            email = 'maktestmessi@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id,
            IsActive = true
        );
        insert newUser;
		
        System.runAs(newUser){
            try{
                Map < String, Object > mapVal = okpcRegisterController.selfRegister(contactList[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(Date.today().addYears(-22)));
                system.assertEquals(true, !mapVal.isEmpty(), 'test_013');
            }catch(Exception e){
                
            }
            
        }
    }
    
     public static testMethod void test_015() {
        //List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'testLast' LIMIT 1];
        
        List<Contact> contactList = [Select Id , Name ,Login_Attempt_Count__c, birthdate, Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        contactList[0].Login_Attempt_Count__c = 0;
        contactList[0].birthdate = (Date.today().addYears(-22));
        update contactList[0];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
       
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(contactList[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(Date.today().addYears(-22)));
        system.assertEquals(true, !mapVal.isEmpty(), 'test_015');
    }
    public static testMethod void test_register() {
        //List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'testLast' LIMIT 1];
        
        List<Contact> contactList = [Select Id , Name ,Login_Attempt_Count__c, birthdate, Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        contactList[0].Login_Attempt_Count__c = 3;
        contactList[0].birthdate = (Date.today().addYears(-22));
        update contactList[0];
        
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'testUser@gmail.com.test.citizen',
            email = 'maktestmessi@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id,
            IsActive = true
        );
        insert newUser;
        
        
       
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(contactList[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(Date.today().addYears(-22)));
        system.assertEquals(true, !mapVal.isEmpty(), 'test_register');
    }
    public static testMethod void test_register2() {
        //List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'testLast' LIMIT 1];
        
        List<Contact> contactList = [Select Id , Name ,Login_Attempt_Count__c, birthdate, Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        contactList[0].Login_Attempt_Count__c = 2;
        contactList[0].birthdate = (Date.today().addYears(-20));
        update contactList[0];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
       
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(contactList[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(Date.today().addYears(-22)));
        system.assertEquals(true, !mapVal.isEmpty(), 'test_register2');
    }
    public static testMethod void test_register3() {
        //List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'testLast' LIMIT 1];
        
        List<Contact> contactList = [Select Id , Name ,Login_Attempt_Count__c, birthdate, Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        contactList[0].Login_Attempt_Count__c = 1;
        contactList[0].birthdate = (Date.today().addYears(-22));
        update contactList[0];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
        User newUser = new User(
            profileId = prfile.id,
            username = 'maktestmessi@gmail.com.test.citizen',
            email = 'maktestmessi@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id,
            IsActive = true
        );
        insert newUser;
        
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(contactList[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(Date.today().addYears(-22)));
        system.assertEquals(true, !mapVal.isEmpty(), 'test_register3');   
    }
    
     public static testMethod void test_014() {
        //List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'testLast' LIMIT 1];
        
        List<Contact> contactList = [Select Id , Name ,Login_Attempt_Count__c, birthdate, Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        contactList[0].Login_Attempt_Count__c = 0;
        contactList[0].birthdate = (Date.today().addYears(-22));
        update contactList[0];
         
        List<Contact> contactList1 = [Select Id , Name ,Login_Attempt_Count__c, birthdate, Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv2' LIMIT 1];
        contactList1[0].Login_Attempt_Count__c = 0;
        contactList1[0].birthdate = (Date.today().addYears(-22));
        update contactList1[0];
         
         
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'maktestmessi@gmail.com.test.citizen',
            email = 'maktestmessi@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id,
            IsActive = true
        );
        insert newUser;

        System.runAs(newUser){
            try{
                 Map < String, Object > mapVal = okpcRegisterController.selfRegister(contactList1[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(Date.today().addYears(-22)));
                 system.assertEquals(true, !mapVal.isEmpty(), 'test_014');
                }catch(Exception e){
                
            }
           
        }
    }
    public static testMethod void test_0() {
        Contact con = TestDataFactoryContact.createContact(false, 'testRegister', 'testFirst');
        con.MobilePhone = '(702)379-44151';
        insert con;
        Contact con1 = [Select Id, Patient_Id__c FROM Contact where lastName = 'testRegister'
            limit 1
        ];
        System.debug('con--' + con1);
        //[Selct Id,Patient_Id__c,Email from Contact WHERE Id =:con.Id]
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(con1.Patient_Id__c, 'test@gmail.com', 'test123@gmail.com', 'lo9(ki8*ju7&', 'lo9(ki8*ju7&', '.citizen','' , String.valueOf(Date.today().addYears(-22)));
        system.assertEquals(true, !mapVal.isEmpty(), 'test_0');
    }

    public static testMethod void test() {
        Contact con = TestDataFactoryContact.createContact(false, 'testRegister', 'testFirst');
        con.MobilePhone = '(702)379-44151';
        con.Email = 'test@gmail.com';
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Citizen (COVID)').getRecordTypeId();
        insert con;

        Contact con1 = [Select Id, Patient_Id__c FROM Contact where LastName = 'testRegister'
            limit 1
        ];
        System.debug('con--' + con1);
        //[Selct Id,Patient_Id__c,Email from Contact WHERE Id =:con.Id]
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(con1.Patient_Id__c, 'test@gmail.com', 'testtest123@gmail.com', 'lo9(ki8*ju7&', 'lo9(ki8*ju7&', '.citizen','',String.valueOf(Date.today().addYears(-22)));
        system.assertEquals(true, !mapVal.isEmpty(), 'test');
    }

    
    public static testMethod void test1() {
        List<Contact> contactList = [Select Id , Name , Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'testUser@gmail.com.test.citizen',
            Email = 'testUser@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id,
            IsActive = true
        );
        insert newUser;
        okpcRegisterController.forgotPassword('testUser@gmail.com.test','.citizen');
        system.assertEquals(false, String.isBlank(okpcRegisterController.forgotPassword('testforgotpassword@gmail.com','.citizen')), 'test1');
        //System.assertEquals(okpcRegisterController.forgotPassword('fakeUser', '.citizen'), Label.Site.invalid_email);
        //System.assertEquals(okpcRegisterController.forgotPassword('testtest123@gmail.com', '.citizen'), 'success');

        // String s = okpcRegisterController.forgotPassword('testtest123@gmail.com');

    }
 /*   public static testMethod void test2() {
        Id s = okpcRegisterController.getProfileId('oktc');
        Id s1 = okpcRegisterController.getProfileId('okcp');
        Id s2 = okpcRegisterController.getProfileId('oklc');
       // Id s3 = okpcRegisterController.getProfileId('okAdmin');
    }*/
    public static testMethod void test3() {
    //    Id s = okpcRegisterController.getProfileId('abc');
    	List<Contact> contactList = [Select Id , Name ,Login_Attempt_Count__c, birthdate, Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
    	contactList[0].Login_Attempt_Count__c = 1;
    	contactList[0].birthdate = (Date.today().addYears(-22));
    	update contactList[0];
        
    	Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
        User newUser = new User(
            profileId = prfile.id,
            username = 'maktestmessi@gmail.com.test.testsiteuser',
            email = 'maktestmessi@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id,
            IsActive = true
        );
        insert newUser;
     	Map < String, Object > mapValNew = okpcRegisterController.selfRegisterForTestSite('', 'maktestmessi@gmail.com.test', 'lo9(ki8*ju7&', 'lo9(ki8*ju7&');
         system.assertEquals(true, !mapValNew.isEmpty(), 'test3');
    }
    public static testMethod void test4() {
        Contact con = TestDataFactoryContact.createContact(true, 'test', 'testFirst');
        okpcRegisterController.ContactInfoWrapper wrapper = okpcRegisterController.getContactInformation(con.Id);
        okpcRegisterController.ContactInfoWrapper wrapper1 = okpcRegisterController.getContactInformation(null);
        system.assertEquals(true, wrapper1 == null, 'test4');
    }

    
    public static testMethod void test6() {
       Account acc = new Account (Name = 'TestAccount');
        
        insert acc;
        Contact con = TestDataFactoryContact.createContact(false, 'testRegister', 'testFirst');
        con.MobilePhone = '(702)379-44151';
        con.AccountId = acc.Id;
        con.Email = 'test123@gmail.com';
        con.Login_Attempt_Count__c = 2;
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Citizen (COVID)').getRecordTypeId();
        insert con;

        Contact con1 = [Select Id, Patient_Id__c FROM Contact where lastName = 'testRegister'
            limit 1
        ];
        System.debug('con--' + con1);
        //[Selct Id,Patient_Id__c,Email from Contact WHERE Id =:con.Id]
        Map < String, Object > mapVal = okpcRegisterController.selfRegister('oktc', 'test123@gmail.com', 'testtest@gmail.com', 'lo9(ki8*ju7&', 'lo9(ki8*ju7&', '.citizen','',String.valueOf(Date.today().addYears(-22)));
        Map < String, Object > mapVal1 = okpcRegisterController.selfRegister('okcp', 'test123@gmail.com', 'testtest@gmail.com', 'lo9(ki8*ju7&', 'lo9(ki8*ju7&', '.citizen','',String.valueOf(Date.today().addYears(-22)));
        Map < String, Object > mapVal2 = okpcRegisterController.selfRegisterForTestSite( con1.Id,'testtest@gmail.com', 'lo9(ki8*ju7&', 'lo9(ki8*ju7&');
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
            Profile profile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = profile.id,
            username = 'testtest@gmail.com.covid',
            email = 'test123@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = con.id,
            IsActive = true
            
        );
        
        Map < String, Object > mapValNew = okpcRegisterController.selfRegisterForTestSite(con.Id, 'testtest@gmail.com', 'lo9(ki8*ju7&', 'lo9(ki8*ju7&');
        system.assertEquals(true, !mapValNew.isEmpty(), 'test6');
    }

    public static testMethod void test7() {
        Contact con = TestDataFactoryContact.createContact(false, 'testRegister', 'testFirst');
        con.MobilePhone = '(702)379-44151';
        con.RecordTypeId = citizenId;
        insert con;

        Contact con1 = [Select Id, Patient_Id__c FROM Contact where lastName = 'testRegister'
            limit 1
        ];
        System.debug('con--' + con1);
        //[Selct Id,Patient_Id__c,Email from Contact WHERE Id =:con.Id]
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(con1.Patient_Id__c, 'testtestRegister@gmail.com', 'testtest@gmail.com.citizen', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(Date.today().addYears(-22)));
        system.assertEquals(true, !mapVal.isEmpty(), 'test7');
    }

    public static testMethod void test8() {
        Contact con = TestDataFactoryContact.createContact(false, 'testRegister', 'testFirst');
        con.MobilePhone = '(702)379-44151';
        con.RecordTypeId = citizenId; //OKPCRecordTypeUtility.retrieveRecordTypeId('Contact','Lab_User');
        insert con;

        Contact con1 = [Select Id, Patient_Id__c FROM Contact where lastName = 'testRegister'
            limit 1
        ];
        System.debug('con--' + con1);
        //[Selct Id,Patient_Id__c,Email from Contact WHERE Id =:con.Id]
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(con1.Patient_Id__c, 'testtestRegister@gmail.com', 'testtest@gmail.com.citizen', 'lo9(ki', 'lo9(ki', '.citizen','',String.valueOf(Date.today().addYears(-22)));
        system.assertEquals(true, !mapVal.isEmpty(), 'test8');
    }
    
    public static testMethod void test9() {
        Contact con = TestDataFactoryContact.createContact(false, 'testRegister', 'testFirst');
        con.MobilePhone = '(702)379-44151';
        con.RecordTypeId = citizenId; //OKPCRecordTypeUtility.retrieveRecordTypeId('Contact','Lab_User');
        insert con;

        Contact con1 = [Select Id, Patient_Id__c FROM Contact where lastName = 'testRegister'
            limit 1
        ];
        String dem = '==================================================================================================================Some string which is longer than 255 characters================================================================================================================';

        System.debug('con--' + con1);
        //[Selct Id,Patient_Id__c,Email from Contact WHERE Id =:con.Id]
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(null, 'testtestRegister@gmail.com', 'testtest@gmail.com.citizen', 'lo9(ki', 'lo9(ki', dem,'',String.valueOf(Date.today().addYears(-22)));
        system.assertEquals(true, !mapVal.isEmpty(), 'test9');
    }

	public static testMethod void test10() {
        
        Contact con = TestDataFactoryContact.createContact(false, 'testRegister', 'testFirst');
        con.MobilePhone = '(702)379-44151';
        con.RecordTypeId = citizenId;
        insert con;
 
        String message = okpcRegisterController.sendPatientId('testtestRegister@gmail.com');
        String message2 = okpcRegisterController.sendPatientId('xyz@gmail.com');
        system.assertEquals(false, String.isBlank(message2), 'test10');
    }
}