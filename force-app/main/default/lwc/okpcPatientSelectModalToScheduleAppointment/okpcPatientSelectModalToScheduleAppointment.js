import {
    LightningElement,
    api,
    track
} from 'lwc';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent'
import validateAllRequiredFields from "@salesforce/apex/VT_TS_CP_ValidateRequiredContactField.validateFields";
import{
    showMessage,
    showMessageWithLink,
    msgObj,
    createMessage,
    showAsyncErrorMessage
}from 'c/vtTsUtility';
export default class OkpcPatientSelectModalToScheduleAppointment extends LightningElement {
    @api patientList;
    @api forVaccination;
    @api eventId;
    @track showMessageVaccination = false;
    @track value = '';
    @track labelName = '';
    @track errorMessage = '';
    @track warrning_message ="Great news! You can now schedule an appointment to get your first dose of a COVID-19 vaccine. Keep in mind that vaccine supplies are very limited, and appointments are available on a first come, first served basis. New appointments will be added as they become available. If you have questions, visit the <a href='https://www.healthvermont.gov/covid-19/vaccine/about-covid-19-vaccines-vermont'>Vermont Department of Health website</a> or call the COVID-19 Call Center at 802-863-7240.";
    @track isValidated = true;
    @track isShowUpdateDetailPage = false;
    @track vaccinationPrivateAccessId;
    // added by vamsi mudaliar
    //@track isSubmitButtonEnabled=true;

    buttonLabel = 'Update Details';
    privateAccess;
    selectedContact;
    msgObjData = msgObj();
    get options() {
        let patientOptions = [];
        patientOptions.push({
            label: 'Select',
            value: ''
        });
        if (this.patientList && this.patientList.length) {
            this.patientList.forEach(patient => {
                patientOptions.push({
                    label: patient.name,
                    value: patient.id
                });
            });
        }
        return patientOptions;
    }

    connectedCallback() {
        setTimeout(()=>{
            this.focusFirstEle();
        },200);
    }

    calculateAge(date) {
        var Bday = +new Date(date);
        return ~~((Date.now() - Bday) / (31557600000));
    }

    handleChange(event) {
        //this.isSubmitButtonEnabled = true;
        // added by vamsi mudaliar
        // reseting the updateDetail button and error message if prev contact didnt fill his demographics
        this.isShowUpdateDetailPage = false;
        this.isValidated = true;

        this.value = event.currentTarget.value;
        this.labelName = event.target.options.find(options => options.value === this.value).label;
        
        this.patientList.forEach(patient => {
            if(patient.id === event.currentTarget.value) {
                this.selectedContact = patient;
            }
        });

        if(this.value) {

            validateAllRequiredFields({
                contactId: this.value,
                eventId: this.eventId,
                contactAge: this.calculateAge(this.selectedContact.birthdate),
                isVaccination : this.forVaccination
            }).then((result) => {
                this.showMessageVaccination = false;
                //console.log('THE JSON STRING',JSON.stringify(result));
                if( this.forVaccination &&
                    !result.isSelectedDoseAlreadyCompleted &&
                    !result.isSelectedDoseAlreadyScheduled &&
                    !result.furtherDosesNotReqd &&
                    !result.isAlreadyCompleted && 
                    !result.secondDoseNotRequired && 
                    !result.isAlreadyScheduled && 
                    !result.is2ndDoseAlreadyScheduled &&
                    !result.is2ndDoseAlreadyCompleted && 
                    !result.ageRestriction &&
                    !result.preRegGroupNotActive && 
                    !result.preRegPending && 
                    !result.noOpenEventsFoundInPreRegGroups && 
                    !result.missingDemographicDetails) // making sure all the demographic details are filled before accessing events 
                {
                    this.showMessageVaccination = true;
                }
                // else if(result.missingDemographicDetails) { // added by vamsi mudaliar
                //     this.isSubmitButtonEnabled = false;
                //     this.errorMessage = 'The person you are trying to schedule has Incomplete Demographic Details. Please click UPDATE DETAILS button!';
                //     showMessage(this,'Error!','error',this.errorMessage);
                    
                // }
    
            }).catch((error) => {
                let message = error.message || error.body.message;
                showAsyncErrorMessage(this, message);
            });
        } else {
            this.showMessageVaccination = false;
        }

    }
    
    handleClick(event) {
        if(event.currentTarget.name === 'submit') {
            if (this.value) {
                this.isShowUpdateDetailPage = false;
                validateAllRequiredFields({
                    contactId: this.value,
                    eventId: this.eventId,
                    contactAge: this.calculateAge(this.selectedContact.birthdate),
                    isVaccination : this.forVaccination
                }).then((result) => {
                    this.isShowSpinner = false;
                    if(result.eventIds){
                        this.vaccinationPrivateAccessId = result.eventIds;
                    }
                    if( this.forVaccination && result.noEvents ){
                        showMessage(this,'Error!','error',this.msgObjData['noEvents']);
                    }
                    else{
                        // added this check to make user fill demographic details.
                        if(result.missingDemographicDetails){
                            this.isValidated = false;
                            //this.isSubmitButtonEnabled = false;
                            this.errorMessage = 'The person you are trying to schedule has Incomplete Demographic Details. Please click UPDATE DETAILS button!';
                            this.isShowUpdateDetailPage = true;
                            return false;
                        }
                        if( result.consentIssue ){
                            this.isValidated = false;
                            this.errorMessage = 'The person you are trying to schedule has an Incomplete profile.  Please click UPDATE DETAILS button!';
                            this.isShowUpdateDetailPage = true;
                        }
                        else if( this.forVaccination && 
                            (
                                result.isSelectedDoseAlreadyCompleted ||
                                result.isSelectedDoseAlreadyScheduled ||
                                result.furtherDosesNotReqd ||
                                result.ageRestriction || 
                                result.isAlreadyScheduled || 
                                result.isAlreadyCompleted || 
                                result.secondDoseNotRequired ||
                                result.is2ndDoseAlreadyScheduled ||
                                result.is2ndDoseAlreadyCompleted || 
                                result.preRegPending ||
                                result.preRegGroupNotActive || 
                                result.noOpenEventsFoundInPreRegGroups
                            )
                            ) {

                            //Error 1: If Pre-reg is pending
                            if( result.preRegPending ) {
                                this.isValidated = false;
                                this.buttonLabel = 'PRE-REGISTER';
                                this.errorMessage = 'The person you are trying to schedule has an Incomplete profile. Please click PRE-REGISTER button!';
                                this.isShowUpdateDetailPage = true;
                                return false;
                            }

                            //Error 2
                            if( result.preRegGroupNotActive ) {
                                showMessage(this,'Error!','error',result.preRegGroupNotActive);
                                // this.showToast('error', result.preRegGroupNotActive);
                                return false;
                            }

                            if(result.noOpenEventsFoundInPreRegGroups){
                                showMessageWithLink(this,'error',this.msgObjData['noPrivateAccessAssignmentFound'],this.msgObjData['noPrivateAccessAssignmentFoundUrl']);
                                // this.showToast('error', result.noOpenEventsFoundInPreRegGroups);   
                                return false;  
                            }

                            if(result.isSelectedDoseAlreadyScheduled){
                                createMessage(result.doseNumber,this,'Error!','error',this.msgObjData['isSelectedDoseAlreadyScheduled']);
                            }
                            else if(result.isSelectedDoseAlreadyCompleted){
                                showMessage(this,'Error!','error',this.msgObjData['isSelectedDoseAlreadyCompleted']);
                            }

                            //Error 3
                            if( result.ageRestriction ) {
                                showMessageWithLink(this,'error',this.msgObjData['ageRestriction'],this.msgObjData['vermontMsgData']);
                                return false;
                            }
                        }
                        else{
                            if(result.isValidate){
                                this.isValidated = true;

                                this.dispatchEvent(new CustomEvent('scheduleappointment', {
                                    bubbles: true,
                                    composed: true,
                                    detail: {
                                        value : this.value, 
                                        forVaccination: this.forVaccination, 
                                        privateAccessIds : this.vaccinationPrivateAccessId,
                                        preRegDateTime: result.preRegMinDateTime
                                    }
                                }));
                            }
                            else{
                                this.isValidated = false;
                                this.errorMessage = 'Update '+ this.labelName +'\'s information before scheduling an appointment: ' + result.missingFields + '.      To update details, click on the Dependents tab and select “UPDATE DETAILS” for the person you need to update';
                            }
                        }
                    }
                }).catch((error) => {
                    let message = error.message || error.body.message;
                    this.isShowSpinner = false;
                    showAsyncErrorMessage(this, message);
                });
                
            } else {
                const event = new ShowToastEvent({
                    variant: 'error',
                    title: '',
                    message: 'Please select a contact to proceed',
                });
                this.dispatchEvent(event);
            }
        }else{
            this.dispatchEvent(new CustomEvent('close'));
        }
    }

    showToast(type, msg) {
        this.dispatchEvent(new ShowToastEvent({
            message: msg,
            variant: type
        }));
    }

    handleGoToUpdateDetails(){
        this.dispatchEvent(new CustomEvent('updatedetails', {
            bubbles: true, composed: true, detail: {
                patientId: this.value,
                isDependentContact: true
            }
        }));
    }

    handleKeyDown(event) {
        if(event.code === 'Escape') {
            this.dispatchEvent(new CustomEvent('close'));
            event.preventDefault();
            event.stopPropagation();
        }
    }

    focusFirstEle() {
        const focusableElements = 'lightning-combobox, lightning-button-icon, button, input, lightning-helptext, lightning-input, lightning-button';
        const modal = this.template.querySelector('.slds-modal');
        const firstFocusableElement = modal.querySelectorAll(focusableElements)[0];
        const focusableContent = modal.querySelectorAll(focusableElements);
        const lastFocusableElement = focusableContent[focusableContent.length - 1];
        firstFocusableElement.focus();
        this.template.addEventListener('keydown', function(event) {
        let isTabPressed = event.key === 'Tab' || event.keyCode === 9;
            if (!isTabPressed) {
                return;
            }
            if (event.shiftKey) {               
                if (this.activeElement === firstFocusableElement) {
                   lastFocusableElement.focus(); 
                    event.stopPropagation()
                    event.preventDefault();
                }
            } else { 
                if (this.activeElement === lastFocusableElement) {    
                    firstFocusableElement.focus(); 
                    event.preventDefault();
                    event.stopPropagation()
                }
            }
        });
    }
}