/**
 * @description       : 
 * @author            : Vamsi Mudaliar
 * @group             : 
 * @last modified on  : 12-06-2021
 * @last modified by  : Vamsi Mudaliar
**/
public without sharing class VT_TS_LookupController {

    @AuraEnabled(cacheable=true)
    public static List<sObject> lookUp(String searchTerm, String objectName, String filters, String recordId, String fields) {

        try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                Integer limitNum = 20;

                String finalQuery = 'SELECT ' + String.escapeSingleQuotes(fields) + ' FROM ' + String.escapeSingleQuotes(objectName);

                if(String.isBlank(recordId)){
                    if(String.isNotBlank(searchTerm)){
                        finalQuery += ' WHERE Name Like  \'%' + String.escapeSingleQuotes(searchTerm) + '%\' ';
                    }
                    
                    if(String.isNotBlank(filters)){
                        finalQuery += String.isNotBlank(searchTerm) ? ' AND ' : ' WHERE ';
                        finalQuery += filters;
                    }
                }else{
                    finalQuery += ' WHERE Id =  \'' + recordId + '\'';
                }

                finalQuery+= ' WITH SECURITY_ENFORCED LIMIT ' + limitNum;

                List<sObject> lookUpList = database.query(finalQuery);

                return lookUpList;  
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }

        
    }
}