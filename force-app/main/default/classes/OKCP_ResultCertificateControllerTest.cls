/**
 * @description       : 
 * @author            : Vamsi Mudaliar
 * @group             : 
 * @last modified on  : 11-25-2021
 * @last modified by  : Vamsi Mudaliar
**/
@isTest
public class OKCP_ResultCertificateControllerTest {

    @testSetup
    public static void testData() {

        Id accRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Test_Center').getRecordTypeId();
        Id conRecTypeId1 = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        Id conRecTypeId2 = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;

        Account acc = TestDataFactoryAccount.createAccount('TestAcc', accRecTypeId, false);
        acc.BillingCity = 'testCity';
        acc.BillingCountry = 'testCountry';
        acc.BillingState = 'testState';
        acc.BillingStreet = 'testStreet';
        acc.BillingPostalCode = '12345';
        acc.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        acc.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert acc;

        Contact con = new Contact (
            AccountId = acc.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = acc.Id,
            RecordTypeId = conRecTypeId2,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Primary_Language__c = 'Cantonese'
        );
        insert con;

        // Contact con = TestDataFactory.createContact('LN', 'FN', false);
        // con.RecordTypeId = conRecTypeId;
        // insert con;
        Contact testContact = TestDataFactory.createContact('lastName', 'firstName', true);

        Appointment_Slot__c appSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, acc.Id);

        Appointment__c  appointment = TestDataFactoryAppointment.createAppointment( false );
        appointment.Patient__c = con.Id;
        appointment.Lab_Center__c = acc.Id;
        appointment.Appointment_Slot__c = appSlot.id;
        appointment.Status__c = 'Completed';

        insert appointment;
        
        Appointment__c  appointment1 = TestDataFactoryAppointment.createAppointment( false );
        appointment1.Patient__c = con.Id;
        appointment1.Lab_Center__c = acc.Id;
        appointment1.Appointment_Slot__c = appSlot.id;
        appointment1.Status__c = 'Completed';
        
        insert appointment1;


        // Appointment__c  appointment2 = TestDataFactoryAppointment.createAppointment( false );
        // appointment1.Patient__c = con12.Id;
        // appointment1.Lab_Center__c = acc.Id;
        // appointment1.Appointment_Slot__c = appSlot.id;
        // appointment1.Status__c = 'Completed';
        
        // insert appointment2;


        system.debug([SELECT Id FROM Appointment__c WHERE Patient__c =: testContact.Id AND Status__c = 'Completed' LIMIT 1]);

        Antibody_Testing__c antiBodyTesting = TestDataFactoryAntiBody.createAppointment( false, acc.Id, appointment.Id, appSlot.Id, con.Id );
        antiBodyTesting.Result_Upload_Date__c = Date.today();
        antiBodyTesting.Results__c = 'Not Tested';
        insert antiBodyTesting;
        

        Antibody_Testing__c antiBodyTestingPos = TestDataFactoryAntiBody.createAppointment( false, acc.Id, appointment1.Id, appSlot.Id, con.Id );
        antiBodyTestingPos.Result_Upload_Date__c = Date.today();
        antiBodyTestingPos.Results__c = 'Positive';
        insert antiBodyTestingPos;
    }
    
    @isTest
    static void resultCertificateControllerTest(){
        
        
        //create an appointment for the contact and also add a few more details to the contact record
		Contact testContact = [ SELECT Id FROM Contact WHERE FirstName = 'testFirst' LIMIT 1 ];
        Test.StartTest(); 

        PageReference pageRef = Page.ResultCertificate;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',testContact.id);
        
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        //OKCP_ResultCertificateController obj = new OKCP_ResultCertificateController();

        Test.StopTest();
        system.assertEquals(true, String.isNotBlank(testContact.Id), 'Contact Found');
    }

    @isTest
    static void setTestingId_Test(){

        Account acc = [ SELECT Id FROM Account WHERE Name = 'TestAcc' ];
        Antibody_Testing__c antibodyTestingRec = [ SELECT Id FROM Antibody_Testing__c WHERE Testing_Site__c = :acc.Id LIMIT 1 ];

        Test.StartTest(); 
      	
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        OKCP_ResultCertificateController obj = new OKCP_ResultCertificateController();
        obj.setTestingId( antibodyTestingRec.Id );
		obj.getTestingId();
        Test.StopTest();
        system.assertEquals(true, String.isNotBlank(antibodyTestingRec.Id), 'Result Found');
    }
    
    @isTest
    static void setTestingIdPositive_Test(){

        Account acc = [ SELECT Id FROM Account WHERE Name = 'TestAcc' ];
        Antibody_Testing__c antibodyTestingRec = [ SELECT Id FROM Antibody_Testing__c WHERE Testing_Site__c = :acc.Id AND Results__c = 'Positive' LIMIT 1 ];

        Test.StartTest(); 
      	
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        OKCP_ResultCertificateController obj = new OKCP_ResultCertificateController();
        obj.setTestingId( antibodyTestingRec.Id );
        Test.StopTest();
        system.assertEquals(true, String.isNotBlank(antibodyTestingRec.Id), 'Result Found');
    }
}