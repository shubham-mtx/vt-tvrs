/**
 * @description       : 
 * @author            : Balram Dhawan
 * @group             : 
 * @last modified on  : 01-18-2022
 * @last modified by  : Balram Dhawan
**/
import { LightningElement,track,api,wire } from 'lwc';
import { registerListener,unregisterAllListeners,fireEvent } from "c/pubsub";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getContactInformation from "@salesforce/apex/OkpcContactInformationController.getContactInformation";
import changeAppointmentStatus from "@salesforce/apex/OkpcContactInformationController.changeAppointmentStatus";
import notChangeAppointmentStatus from "@salesforce/apex/OkpcContactInformationController.notChangeAppointmentStatus";

import updateAppointmentDetailsOnSearchEvent from "@salesforce/apex/OkpcContactInformationController.updateAppointmentDetailsOnSearchEvent";
import createAppointment from "@salesforce/apex/OkpcContactInformationController.createAppointment";
import getAppointmentCurrentSteps from "@salesforce/apex/OkpcContactInformationController.getAppointmentCurrentStep";


import isPrivateAccessObject from "@salesforce/apex/OKPCDashboardController.isPrivateAccessObject";


import retrieveCurrentUser from '@salesforce/apex/OKPCDashboardController.retrieveCurrentUser';

export default class OkcpRegistrationForm extends LightningElement {
    selectedEventId = '';
    selectedAccountId = '';
    @track currentStep = 1;
    @track originalStep = 1;
    @track urlEventId;
    @track vaccinationPrivateAccess;
    showSpinner = true;
    @track edit = false;
    @track source;
    @track appointmentObject;
    @track hideStep3 = false; //ad
    @track showDashboard;
    @track appoitmentId;
    @track preRegDateTime;
    isDependentContact = false;
    dependentContactId = '';
    isLoaded = false;
    contactId;
    currentUserDetail = {};
    consentDetails = {};
    isVaccination;
    showLabelPopup = false;
    showLabelPopup2 = false;


    // S-23914
    runningVaccineScheduler;

    @api
    get selectedEvent() {
        return this.selectedEventId;
    }

    get showpage1() {
        setTimeout(()=>
        this.showLabelPopup = true,1000);
        return this.currentStep === 1;
    }

    get showpage2() {
        if(this.currentStep == 2) {
            setTimeout(()=>
            this.showLabelPopup2 = true,1000);
        }
        return this.currentStep === 2;
    }

    get showpage3() {
        return this.currentStep === 3;
    }

    get showpage4() {
        return this.currentStep === 4;
    }
    // Commented by Vamsi Mudaliar

    get showpage5() {
        return this.currentStep === 5;
    }

   

    get showpage7() {
        //Commented By Virendra Kumar as per S-16828
       //return this.currentStep == 7;
       return this.currentStep === 6;
   }

    get showpage8() {
        return this.currentStep === 8;
    }

    get showpage9() {
        return this.currentStep === 9;
    }

    

    connectedCallback() {
        retrieveCurrentUser()
        .then(data => {
            if( data ){
                this.currentUserDetail = data[0];
                this.contactId = this.currentUserDetail.ContactId;
            }
        })
        .catch(error => {
            this.showSpinner = false;
        });

        this.showSpinner = true;
        let urlArray = location.search.split('=');
        if( urlArray.length > 0 ) {
            urlArray.forEach((e) => {
                if( e.includes('eventId') === true  ) {
                    this.urlEventId = urlArray[urlArray.indexOf(e)+1]; 

                    //Check if private access group is for Vaccination
                    isPrivateAccessObject({eventId : this.urlEventId})
                    .then(result => {
                        this.isVaccination = result;
                        this.showSpinner = false;
                    })
                    .catch(error => {
                        this.showSpinner = false;
                    });
                }
            });
        }
        registerListener("handleSideBarClick", this.handleSideBarClick, this);
        this.getStep();
    }

    getStep() {
        getContactInformation()
            .then(result => {
                if (result.Source__c === 'MMS') {
                    this.hideStep3 = true;
                } else {
                    this.hideStep3 = false;
                }
                if (result.Current_Step__c) {
                    this.originalStep = result.Current_Step__c;
                    this.handleSideBarClick(this.originalStep);
                }
                
                if (result.Form_Submitted__c == true) {
                    this.showDashboard = true;
                } else {
                    this.showDashboard = false;
                    // this.app
                }
                this.isLoaded = true;
                this.showSpinner = false;
            })
            .catch(error => {
                this.showSpinner = false;
            })
    }

    getAppointmentCurrentStep() {
        var patientId;
        if (this.dependentContactId) {
            patientId = this.dependentContactId;
        }
        getAppointmentCurrentSteps({ contactId: patientId, eventId :this.urlEventId })
            .then(result => {
                if (result.noRecord === 'true') {
                    this.currentStep = 3;

                    this.originalStep = 3;
                    //this.determineOriginalStep();
                } else {
                    this.appoitmentId = result.id;

                    
                    this.currentStep = result.step + 1;
                    this.originalStep = result.step;
                    this.selectedAccountId = result.labId;
                    this.selectedEventId = result.eventId;
                    this.determineOriginalStep();
                    //}
                }
            })
            .catch(error => {
                let mesg = (error)?((error.message)?error.message:((error.body)?((error.body.message)?error.body.message:JSON.stringify(error)):JSON.stringify(error))):"Somethmesging went wrong!";
            })
    }

    handleAddDependent(event) {
        this.showDashboard = false;
        this.isDependentContact = true;
        this.originalStep = 1;
        this.currentStep = 1
        this.contactId = '';
        this.consentDetails = {};
    }

    handleUpdateDetails(event) {
        this.showDashboard = false;
        this.originalStep = 1;
        this.currentStep = 1;
        this.isDependentContact = false;//event.detail.isDependentContact;
        this.contactId = event.detail.patientId;
        this.consentDetails = {};
        this.runningVaccineScheduler = false;
    }

    handleScheduleAppointment(event) {
        this.appoitmentId = '';
        this.showDashboard = false;
        this.isDependentContact = false;
        this.dependentContactId = event.detail.value;
        this.isVaccination = event.detail.forVaccination;
        this.vaccinationPrivateAccess = event.detail.privateAccessIds;
        this.preRegDateTime = event.detail.preRegDateTime;
        this.currentStep = 3;
        this.originalStep = 3;
        this.getAppointmentCurrentStep();
    }

    handleCancelAppointment() {
        this.removeEventIdFromURL();
        this.appoitmentId = '';
        this.selectedEventId = '';
        this.showDashboard = true;
    }
   removeParam(key, sourceURL) {
        let rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        return rtn;
    }
    navigateToConsent() {
        this.currentStep = 1;
        this.determineOriginalStep();
    }

    navigateToPersonalInfo(event) {
        if (event.target.name == 'next') {
            this.template.querySelector('c-okcp-consent-page-new').saveConsent();
        } else {
            this.currentStep = 2;
            this.determineOriginalStep();
        }
    }

    //1st Page
    consentSave(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else if (event.detail.status === 'saveNew') {
            this.currentStep = 2;
            this.determineOriginalStep();
            this.consentDetails = event.detail.consentParams;
        } else {
            this.consentDetails = {};
            this.currentStep = 2;
            this.determineOriginalStep();
        }
    }

    //Save Contact
    saveContacts(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.showDashboard = true;
        }
    }

    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc pre-reg save handler and display Dashboard component
    */
    preRegSaveHandler() {
        this.showDashboard = true;
    }





    handleEdit() {
        this.edit = true;
        this.template.querySelector('c-okpc-contact-info').editContacts();
    }

    handleCancel() {
        this.edit = false;
        this.template.querySelector('c-okpc-contact-info').handleCancelButton();
    }
    handleSave() {
        this.edit = false;
        this.template.querySelector('c-okpc-contact-info').saveContacts();
    }

    determineOriginalStep() {
      
        this.originalStep = (this.originalStep>=this.currentStep)? this.originalStep:this.currentStep;
    }

    handleSideBarClick(currentStep) {
        this.currentStep = currentStep;
         
        if(this.currentStep===5)
            this.currentStep = this.currentStep+1; // if demographics go to waiver page.
    }

    handleThankYouBack() {
        window.open('/s/dashboard', '_self');
    }

    handleback(event) {
        this.currentStep = this.currentStep - 1;

        // we do not have demographics flow. 
        if(this.currentStep===5)
            this.currentStep = this.currentStep-1;
    }



    //2nd Page
    navigateToAppointment(event) {
        if (event.target.name === 'submit') {
            this.template.querySelector('c-okcp-contact-info-new').saveContacts(() => {
                this.showDashboard = true;
            });
           
        } else {
            this.currentStep = 3;
            this.determineOriginalStep();
        }
    }



    //3rd Page
    handleSubmitAppointment(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        }
    }

    navigateToRegistration(event) {
        if (!this.selectedEventId || !this.selectedAccountId) {
            this.dispatchEvent(new ShowToastEvent({
                title: 'Select an Event',
                variant: 'error',
                message: event.detail.message,
            }));
            return;
        }
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
           
        }

        if (!this.appoitmentId) {
            this.showSpinner = true;
            createAppointment({
                contactId: this.dependentContactId,
                eventId: this.selectedEventId,
                accountId: this.selectedAccountId
            }).then(result => {
                this.appoitmentId = result;
                this.currentStep = 3;
                this.updateAppointmentDetails();
                this.determineOriginalStep();
                this.showSpinner = false;
                // this.selectedEventId = undefined;
            }).catch(error => {
                this.dispatchEvent(new ShowToastEvent({
                    message: error.body.message,
                    variant: 'error'
                }));
                this.showSpinner = true;
            });
        } else {
            this.updateAppointmentDetails();
        }
    }

    updateAppointmentDetails() {
        this.showSpinner = true;
        updateAppointmentDetailsOnSearchEvent({
            appointmentId: this.appoitmentId,
            testingSiteId: this.selectedAccountId,
            eventId: this.selectedEventId
        }).then(result => {
            this.currentStep = 4;
            this.determineOriginalStep();
            this.showSpinner = false;
            // this.selectedEventId = undefined;
        }).catch(error => {
            this.dispatchEvent(new ShowToastEvent({
                message: error.body.message,
                variant: 'error'
            }));
        });

    }


    //4th Page
    registrationSaved(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.currentStep = 4;
            this.determineOriginalStep();
        }
    }

    

    //5th Page
    testingSaved(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.currentStep = 6; // skipping demoGraphic Page
            this.determineOriginalStep();
        }
    }

    
    insuranceSaved(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.currentStep = 7;
            this.determineOriginalStep();
        }
    }

    //8th page
    handleWaiver(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.currentStep = 8;
            this.determineOriginalStep();
        }
    }

    //9th Page
    handleSubmitData(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            // this.selectedEvent = undefined;
            this.selectedEventId = null;
            this.handleSubmit(event);
        }
    }

    removeEventIdFromURL(){
        var originalURL =window.location.href;
        var alteredURL = this.removeParam('eventId', originalURL);
        history.pushState({
            id: 'dashboard'
        }, 'Covid Portal', alteredURL);
        this.urlEventId = null;
        
    }

    handleSubmit(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.currentStep = 8;
            this.determineOriginalStep();
            if (event.target.name === 'submit') {
                changeAppointmentStatus({
                    currentstep: this.currentStep
                }).then(result => {
                    this.removeEventIdFromURL();
                    this.showDashboard = true;
                })
            }

            notChangeAppointmentStatus({
                currentstep: this.currentStep
            })
                .then(result => {
                    this.removeEventIdFromURL();
                    this.showDashboard = true;
                })
                .catch(error => {
                });
        }
    }

   

    handleEventSelected(event) {
        this.selectedEventId = event.detail;
        if(this.appoitmentId) {
            updateAppointmentDetailsOnSearchEvent({
                appointmentId: this.appoitmentId,
                testingSiteId: this.selectedAccountId,
                eventId: this.selectedEventId
            }).then(result => {
            }).catch(error => {
            });
        }
    }

    handleAccountSelected(event) {
        this.selectedAccountId = event.detail;
    }

    // S-23914
    initiateVaccineScheduler(event) {
        this.showDashboard = false; 
        this.runningVaccineScheduler = true;
    }

    closeVaccineScheduler(event) {
        this.runningVaccineScheduler = false;
        this.showDashboard = true;
    }

    schedulerCompletionHandler(event) {
        this.runningVaccineScheduler = false;
        this.handleScheduleAppointment(event);
    }
}