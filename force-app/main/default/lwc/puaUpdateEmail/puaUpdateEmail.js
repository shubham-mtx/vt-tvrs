/**
 * @description       : 
 * @author            : Prashant Gupta
 * @group             : 
 * @last modified on  : 01-20-2022
 * @last modified by  : Mohit Karani
 * Modifications Log 
 * Ver   Date         Author           Modification
 * 1.0   12-15-2020   Prashant Gupta   Initial Version
 **/
 import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    ShowToastEvent
} from "lightning/platformShowToastEvent";
import {
    NavigationMixin
} from 'lightning/navigation';
import doInitContactAddress from '@salesforce/apex/PUA_UpdateEmailController.doInitContactAddress';
import doInitUserDetails from '@salesforce/apex/PUA_UpdateEmailController.doInitUserDetails';
import saveContactAddress from '@salesforce/apex/PUA_UpdateEmailController.saveContactAddress';
import saveUserDetails from '@salesforce/apex/PUA_UpdateEmailController.saveUserDetails';
import resetPassword from '@salesforce/apex/PUA_UpdateEmailController.resetPassword';
import getContactUserDetail from '@salesforce/apex/PUA_UpdateEmailController.getContactUserDetail';



export default class PuaUpdateEmail extends NavigationMixin(LightningElement) {

    @api showEmail;
    @api showResetPassword;
    @api showUsername;
    @api editEmail;
    @api editUsername;
    @api recordId;
    @api updateprefix;

    @track responseWrapper = {
        conDetails: '',
        userDetails: ''
    };
    @track showSpinner = true;
    @track showError = false;
    @track emailConfirmationModal = false;
    @track showEmailModel = false;
    @track conId;
    @track userId;
    @track oldusername;
    @track disableEmail = false;
    @track showSuccessMessage = false;
    @track showConfirmationModal = false;
    @track showUserInfo = false;


    connectedCallback() {

        getContactUserDetail({ currentcontactId: this.recordId })
            .then(result => {
                if (result == true) {
                    this.showUserInfo = true;
                } else {
                    this.showUserInfo = false;
                }
            })
            .catch(error => {
            });

        if (this.editEmail == false) {
            this.disableEmail = true;
        } else
            this.disableEmail = false;

        if(this.updateprefix == undefined){
            this.updateprefix = '';
        }
    }
    handleInput(event) {
        if (event.target.name === "username") {
            this.responseWrapper.userDetails.username = event.target.value.trim();
            this.emailRecover.username = event.target.value.trim();
        } else if (event.target.name === "email") {
            this.responseWrapper.userDetails.email = event.target.value.trim();
            this.responseWrapper.conDetails.conEmail = event.target.value.trim();
            //this.responseWrapper.userDetails.username = event.target.value.trim() + '.covid';
            this.responseWrapper.userDetails.username = event.target.value.trim() + this.updateprefix;
            if(this.oldusername == this.responseWrapper.userDetails.username){
                this.showError = true; 
            }
        }
    }

    cancelUpdateEmailModal() {
        this.showConfirmationModal = false;
        this.showEmailModel = true;
        this.dispatchEvent(new CustomEvent('modalcancel'));
    }

    handleSaveContinue() {
        this.showSpinner = true;
        saveContactAddress({
                responseWrapper: JSON.stringify(this.responseWrapper.conDetails),
                contactId: this.conId,
            })
            .then(result => {})
            .catch(error => {})
            .finally(() => this.showSpinner = false);
        this.showSpinner = true;
        saveUserDetails({
                responseWrapper: JSON.stringify(this.responseWrapper.userDetails),
                userId: this.userId
            })
            .then(result => {
                if(this.showError){
                if(result.error){
                    const event = new ShowToastEvent({
                        title: "Error",
                        message: "Username already exists. Please use a different username!",
                        variant: "error",
                        mode: "dismissable"
                    });
                    this.dispatchEvent(event); 
                }
            }
                else{
                this.showConfirmationModal = false;
                this.emailConfirmationModal = true;
                }
            })
            .catch(error => {
                this.showSpinner = true;
                const event = new ShowToastEvent({
                    title: "Error",
                    message: "You do not have permission to update the email!",
                    variant: "error",
                    mode: "dismissable"
                });
                this.dispatchEvent(event);
            })
            .finally(() => this.showSpinner = false);
    }

    handleResetPassword() {
        this.showSpinner = true;
        this.showEmailModel = false;
        try {
            // this.responseWrapper.userDetails.username
            resetPassword({
                    username: this.oldusername
                })
                .then(result => {
                    this.showSpinner = false;
                    const event = new ShowToastEvent({
                        title: "Success",
                        message: "Reset Password Email has been sent!",
                        variant: "success",
                        mode: "dismissable"
                    });
                    this.dispatchEvent(event);
                })
                .catch(error => {
                    this.showSpinner = false;
                });
        } catch (ex) {
            this.showSpinner = false;
        }
    }
    openEmailModel() {
        this.showEmailModel = true;
        this.showSpinner = true;
        doInitContactAddress({
                contactId: this.recordId,
            })
            .then(result => {
                if (result != null) {
                    this.responseWrapper.conDetails = result;
                    this.conId = this.responseWrapper.conDetails.Id;
                }
            })
            .catch(error => {})
            .finally(() => this.showSpinner = false);
        this.showSpinner = true;
        doInitUserDetails({
                contactId: this.recordId
            })
            .then(result => {
                if (result != null) {
                    this.responseWrapper.userDetails = result;
                    this.userId = this.responseWrapper.userDetails.Id;
                    this.oldusername = this.responseWrapper.userDetails.username;
                }
            })
            .catch(error => {})
            .finally(() => this.showSpinner = false);

    }

    cancelEmailModal() {
        this.showEmailModel = false;
        this.dispatchEvent(new CustomEvent('modalcancel'));
    }

    isValid() {
        let valid = false;
        valid = [...this.template.querySelectorAll("lightning-input")].reduce(
            (validSoFar, input) => {
                input.reportValidity();
                return validSoFar && input.checkValidity();
            },
            true
        );

        valid &= [...this.template.querySelectorAll("lightning-combobox")].reduce(
            (validSoFar, input) => {
                input.reportValidity();
                return validSoFar && input.checkValidity();
            },
            true
        );
        return valid;
    }
    handleEmailSave() {
        if (this.isValid()) {
            if (this.responseWrapper.userDetails.email == '') {
                return;
            }
            this.showEmailModel = false;
            this.showConfirmationModal = true;
        }
    }
    cancelConfirmationModal() {
        window.location.reload();
        this.emailConfirmationModal = false;
        this.dispatchEvent(new CustomEvent('modalcancel'));
    }
}