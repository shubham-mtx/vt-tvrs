import { LightningElement, api, track } from 'lwc';
import updateAppointmentrec from '@salesforce/apex/DC_ScheduleAppointment.updateAppointment';
import getAppointmentDetailrec from '@salesforce/apex/DC_ScheduleAppointment.getAppointmentDetail';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';
export default class VtpcScheduleAppointment extends LightningElement {
    @api eventid;
    @api recordid;
    @api selectedSlotId;
    @api isSecondDose;

    @track options = [];
    @track selectedOption;
    @track isAttributeRequired = false;
    @api fieldName;
    @track contactid;
    @api objectName;
    @track fieldLabelName;
    @track accountList = [];
    @track isSiteSelected = false;
    @track preferredTime;
    @track preferredDate;
    @api selectvalue;
    @api hidePrefferedTime = false;
    @track slotId;
    @track showSchedule = false;

    @track showMapModal = false;

    @api eventStartDate;
    @api eventEndDate;
    @api eventStartTime;
    @api eventEndTime;

    connectedCallback() {
        if(!this.isSecondDose) {
            getAppointmentDetailrec({ appointmentId: this.recordid })
            .then(data => {
                let appointment = data.appointment;
                this.contactid = appointment.Patient__c;
                this.preferredTime = this.msToTimeAMPM(appointment.Patient__r.Preferred_Time__c);
                this.preferredDate = appointment.Patient__r.Preferred_Date__c;
                if (appointment.Status__c === 'Cancelled' || appointment.Status__c === 'Scheduled' || appointment.Status__c === 'To Be Scheduled' || appointment.Status__c === 'Eligible' || appointment.Status__c === null || typeof appointment.Status__c === 'undefined') {
                    // this.retrieveTestingSites();
                    this.isSiteSelected = true;
                } else {
                    // added this check due to Booster Dose
                    if(appointment.Event_Process__c !== 'Vaccination') {
                        this.dispatchEvent(new CustomEvent('close', {}));
                        const event = new ShowToastEvent({
                            title: 'Error!',
                            variant: 'error',
                            message: 'You are not allowed to update this appointment.',
                        });
                        this.dispatchEvent(event);
                    }
                }
            })
            .catch(error => {
                showAsyncErrorMessage(this, error);
            });
        }
    }

    setId(event) {
        this.showSchedule = true;
        this.slotId = event.detail;
        this.dispatchEvent(new CustomEvent('slotselected', { detail: event.detail }));
    }

    createAppointmentRecord() {
        updateAppointmentrec({ accountId: this.selectvalue, appointmentId: this.recordid, slotId: this.slotId }).then(result => {
            if (result.type === 'error') {
                const event = new ShowToastEvent({
                    title: 'Error!',
                    variant: 'error',
                    message: result.message,
                });
                this.dispatchEvent(event);
            } else if (result.type === 'success') {
                const event = new ShowToastEvent({
                    title: 'Success!',
                    variant: 'success',
                    message: result.message,
                });
                this.dispatchEvent(event);
                // this.createCalendar(result);
                this.dispatchEvent(new CustomEvent('close', {
                    detail: 'save'
                }));
            }

        }).catch(error => {
            showAsyncErrorMessage(this, error.message);
        });
    }

    msToTimeAMPM(s) {
        if (s === '' || s === null || typeof s === 'undefined')
            return '';
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;
        let radian = 'AM';
        if (hrs > 12) {
            radian = 'PM';
            hrs = hrs - 12;
        }
        return this.pad(hrs) + ':' + this.pad(mins) + ' ' + radian;
    }

    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }

}