import {
    LightningElement,
    track,
    wire
} from 'lwc';


import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import sendEmailMethod from '@salesforce/apex/VTTS_SendEmailController.sendEmailMethod';
import eventList from '@salesforce/apex/VTTS_SendEmailController.eventList';

const col = [
  //  { label: 'Id', fieldName: 'Id' },
    { label: 'First Name', fieldName: 'FirstName', type: 'text' },
    { label: 'Last Name', fieldName: 'LastName', type: 'text' },
    { label: 'Email', fieldName: 'Email', type: 'email' }
];

export default class OkTestingSendEmail extends LightningElement {

    @track showSpinner = false;
    @track showError = false;
    @track showSuccess = false;
    @track errorMessage = '';
    @track thankYouMessage = '';

    @track emailBody = '';
    @track eventOptions = [];
    @track event;

    //modal
    @track showModal = false;
    @track eventContactMap;
    @track contactsToDisplay;
    @track contactEmailsToNotify = [];
    @track precheckedContacts = [];
    columns = col;

    connectedCallback(){
        this.showSpinner = true;
        eventList()
        .then(result => {
			 this.eventOptions = result.options;
            this.eventContactMap = result.eventIdToContactsMap;
            this.showSpinner = false;
        })
        .catch(error => {
            this.showToast('', JSON.stringify(error.body.message), 'error');
            return;
        });

    }

    renderedCallback() {
        if (this.hasRendered) return;
        this.hasRendered = true;
    
        const style = document.createElement('style');
        style.innerText = `.slds-listbox__option .slds-truncate {width: 100% !important; white-space: pre-wrap !important;}`;
        
        this.template.querySelector('.style').appendChild(style);
    }

    handleChange(event) {
        if (event.target.name === 'emailBody') {
            this.emailBody = event.target.value;

        } else if (event.target.name === 'event') {
            this.event = event.target.value;
            this.showModal = true;
            let contactIds = new Set();
            let tempContactList = [];
            for (let index = 0; index < this.eventContactMap[this.event].length; index++) {
                if( contactIds.has(this.eventContactMap[this.event][index]['Id']) === false ) {
                    contactIds.add(this.eventContactMap[this.event][index]['Id']);
                    tempContactList.push(this.eventContactMap[this.event][index]);
                }
            }
            this.contactsToDisplay = tempContactList;
            for(var i=0; i<this.contactsToDisplay.length; i++) {
                this.precheckedContacts[i] = this.contactsToDisplay[i].Id;
            }
        } 
    }

    handleContactSelection(){
        let selectedContacts = this.template.querySelector('lightning-datatable').getSelectedRows();
        this.contactEmailsToNotify = [];
        for(var i=0; i<selectedContacts.length; i++) {
            this.contactEmailsToNotify[i] = selectedContacts[i].Email;
        }
        this.showModal = false;
    }

    //Modal methods
    openModal() {
        // to open modal set isModalOpen tarck value as true
        this.showModal = true;
    }

    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.showModal = false;
    }

    sendEmails() {
        if (this.event === '' || typeof this.event === 'undefined') {
            this.showToast('', 'Please select an event', 'warning');
            return;
        }

        if (this.emailBody === '' || typeof this.emailBody === 'undefined') {
            this.showToast('', 'Email body cannot be empty', 'warning');
            return;
        }
        if (this.contactEmailsToNotify === '' || typeof this.contactEmailsToNotify === 'undefined') {
            this.showToast('', 'Please select atleast one contact', 'warning');
            return;
        }

        this.showSpinner = true;
        sendEmailMethod({
                emailBody: this.emailBody,
                eventId : this.event,
                contactEmailsToNotify : this.contactEmailsToNotify
            })
            .then(result => {
                if (result.success) {
                    this.showToast('', result.message, 'success');
                } else {
                    this.showToast('', result.message, 'error');
                }
                this.showSpinner = false;
            })
            .catch(error => {
                this.showToast('', JSON.stringify(error.body.message), 'error');
                this.showSpinner = false;
                return;
            });        
    }

    showToast(title, message, variant) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(event);
    }
}