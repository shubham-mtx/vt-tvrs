@isTest
private class VT_VTS_EventTriggerHandlerTest {
    
    @TestSetup
    static void setupData() {
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        String laboratoryRecTypeId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
       // String labUserRecTypeId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;
        
        Account labAccount = TestDataFactoryAccount.createAccount('TestLabAccount', laboratoryRecTypeId, false);
        labAccount.BillingCity = 'testCity';
        labAccount.BillingCountry = 'testCountry';
        labAccount.BillingState = 'testState';
        labAccount.BillingStreet = 'testStreet';
        labAccount.BillingPostalCode = '12345';
        labAccount.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        labAccount.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert labAccount;
        
        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-44151',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            what_was_the_last_date_of_contact__c = Date.newInstance(2020, 2, 20),
            Did_you_have_a_nasal_swab__c = 'Yes	',
            What_was_your_test_result__c = 'Negative',
            Approximately_what_date_were_you_tested__c = Date.newInstance(2020, 3, 3),
            Had_contact_with_anyone_diagon__c = 'Yes',
            Do_you_take_prescription_medications__c = '	Yes',
            medication_for_fever_in_the_past_3_days__c = 'Yes',
            First_Date_you_have_been_feeling_sick__c = Date.newInstance(2020, 3, 1) ,
            First_date_you_began_feeling_well__c= Date.newInstance(2020, 3, 10),
            Fever_Cough_or_Difficulty_breathing__c = 'Yes',
            Sick_since_Jan_1_2020__c = 'Yes',
            Opt_out_for_SMS_Updates__c = true,
            HasOptedOutOfEmail = false,
            Current_Step__c = 1,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con;
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        
        VTS_Event__c event = new VTS_Event__c(Status__c = 'Open', 
                                              Start_Date__c = Date.today().addDays(-1), 
                                              End_Date__c = Date.today().addDays(10),
                                              Start_Time__c = Time.newInstance(7, 0, 0, 0),
                                              End_Time__c = Time.newInstance(17, 0, 0, 0),
                                              Location__c = createAccountTestSite.Id,
                                              Description__c = 'Test Event'); 
        insert event;
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        createAppointment.Event__c = event.id;
        insert createAppointment; 
    }
    
    @isTest
    static void retrieveDashboardValueTest(){ 
        List<VTS_Event__c> eventList = new List<VTS_Event__c>();
        eventList = [Select Id FROM VTS_Event__c WHERE Status__c = 'Open' LIMIT 1];
        
        Test.startTest();
            eventList[0].Description__c = 'Updated Event Description';
            update eventList;
            eventList[0].Status__c = 'Canceled';
            update eventList[0];
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        //system.assertEquals(2, invocations);
    }
    
}