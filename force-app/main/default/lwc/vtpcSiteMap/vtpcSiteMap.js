import {
    LightningElement,
    track,
    api
} from "lwc";
import appTitle from '@salesforce/label/c.OKPC_Location_Map_Title';
import getEventsWithFilters from "@salesforce/apex/OKPC_LocationMapController.getEventsWithFilters";
import getTestingSitesWithFilters from "@salesforce/apex/OKPC_LocationMapController.getTestingSitesWithFilters";
import cssFile from '@salesforce/resourceUrl/VT_TVRS_Filtter_CSS';
import {
    loadStyle,
} from 'lightning/platformResourceLoader';
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';

export default class VtpcSiteMap extends LightningElement {

    label = {
        appTitle
    }
    selectedEvent = '';
    @api isFromApptScheduler;
    @api contactId;
    @track siteEvents = [];
    @track markersTitle = 'Locations';
    @track showFooter = true;
    @track listView = 'auto';
    @track selectedMarkerValue = '';
    @track mapIdToAccount = {};
    @track error;
    @track showMap = false;
    @track startTime;
    @track endTime;
    @track errorMessage;
    @track showError;
    @track appointmentDate;
    @track flag;
    @track momentRef;
    @track mapMarkers = [];
    @track results = [];
    @track isLoaded = true;
    @track isEventLoaded = false;
    @track isEnableSearch = true;
    @track filterValues;
    @track sponser;
    @track isOtherSponser = false;
    @track accDescription;
    @track accSiteLink;
    @track isAfterSearchMsg = false;
    siteLinkLabel = this.accSiteLink + "this link will open up in a new tab";

    @api preRegDateTime;
    @api isFirstAppointment;
    //@api preRegMinDate;
    //@api preRegMaxDate;
    @api privateAccessId;
    @api isVaccination;
    @api vaccinationPrivateAccess;
    @api eventProcess;
    @api showPrivateAccessFilter;
    @api zoomLevel = 9;
    @api siteId;
    @api eventId;
    @api showLoader = false;
    @api islobby = false;
    @api locationTitle;
    @api hideLocationLabel = false;
    @api isRecordSpecific = false;
    @api vaccinationType;
    @api secondDoseData;
    @api filterValuesPreviousButtonHanlder;
    @api eventIds;
    @api isSecondDose = false; // S-14229 (Second Dose)
    @api
    get selectedSite() {
        return this.selectedMarkerValue ? this.mapIdToAccount[this.selectedMarkerValue] : '';
    }

    get eventCount() {
        return this.siteEvents.length;
    }

    get hasLocation() {
        return this.mapMarkers.length;
    }

    get showVaccinationMessageIfNoAppointmemtsFound() { // S-18679
        return this.isVaccination || (this.filterValues.hasOwnProperty('vaccineType') && (this.filterValues.vaccineType === 'Vaccination-1' || this.filterValues.vaccineType === 'Vaccination-2'));
    }

    @track isdefaultload = false;
    @track hidefilters = false;
    @track showPreRegTimeIssue;

    connectedCallback() {
        loadStyle(this, cssFile);
        if (this.eventProcess === 'Vaccination') {
            this.label.appTitle = 'Vaccination Sites';
        } else {
            this.label.appTitle = 'Testing Sites';
        }
        if (this.siteId || this.eventId || this.vaccinationPrivateAccess) {
            this.isdefaultload = true;
            this.hidefilters = true;
            //this.privateAccessId = this.eventId;
        }

        setTimeout(() => {
            this.focusFirstEle();
        }, 200);
    }

    handleSearch(event) {
        this.isAfterSearchMsg = false;
        this.filterValues = event.detail;
        //Pre-Reg Minimum time filter in case of Pending Verification
        this.showPreRegTimeIssue = false;
        this.sponser = event.detail.sponBy;
        if (this.sponser.includes('Other')) {
            this.isOtherSponser = true;
        } else {
            this.isOtherSponser = false;
        }
        this.mapMarkers = [];
        this.showMap = false;

        if (this.preRegDateTime) {
            //Date
            let dateTimeString = this.preRegDateTime.split('**');
            let preRegM_Datetime = moment(dateTimeString[0] + ' ' + dateTimeString[1], "MM/DD/YYYY hh:mm:ss");
            let filterM_Sdatetime = moment(this.filterValues.sDate + ' ' + this.filterValues.sTime, "MM/DD/YYYY hh:mm:ss");
            let filterM_Edatetime = moment(this.filterValues.eDate + ' ' + this.filterValues.eTime, "MM/DD/YYYY hh:mm:ss");

            //If Start Date and End Date from filters both are less then minimum date then show the error
            if (filterM_Sdatetime < preRegM_Datetime && filterM_Edatetime < preRegM_Datetime) {
                this.showPreRegTimeIssue = true;
                this.dateTimeStringPreReg = preRegM_Datetime.format('MM/DD/YYYY hh:mm A');
                return;
            }
            if (filterM_Sdatetime < preRegM_Datetime && filterM_Edatetime > preRegM_Datetime) {
                this.filterValues.sDate = dateTimeString[0];
                // VK: this.filterValues.sTime = dateTimeString[1];
            }
        }


        var date = new Date();
        let  year = date.getFullYear();
        let month = date.getMonth() + 1;
        let dt = date.getDate();

        if (dt < 10) {
            dt = '0' + dt;
        }
        if (month < 10) {
            month = '0' + month;
        }
        this.filterValues.cDate = month+'/' + dt + '/'+year;
        this.filterValues.ctime = date.getHours()+':'+date.getMinutes()+ ':' + date.getSeconds();
        let temp = JSON.parse(JSON.stringify(this.filterValues));
        let vaccineClass = this.secondDoseData && temp.vaccineType !== 'Vaccination-1' ? (this.secondDoseData.hasOwnProperty('oldVaccineClass') ? this.secondDoseData.oldVaccineClass : null) : null
        this.showLoader = true;
        getTestingSitesWithFilters({
                sObj: JSON.stringify(this.filterValues),
                accountId: undefined,
                privateAccessId: this.privateAccessId,
                vaccinationPrivateAccess: this.vaccinationPrivateAccess,
                vaccineClass: vaccineClass
            })
            .then(result => {
                this.selectedEvent = undefined;
                const eventSelected = new CustomEvent('eventselected', {
                    detail: {
                        accountId: undefined,
                        eventId: undefined
                    }
                });
                this.dispatchEvent(eventSelected);

                this.isAfterSearchMsg = true;
                this.showLoader = false;
                let mapMarkers = [...result];
                this.results = mapMarkers;
                mapMarkers.forEach((marker) => {
                    this.mapIdToAccount[marker.value] = marker.title;
                });


                if (mapMarkers && mapMarkers.length) {

                    if (mapMarkers.length === 1) {
                        this.selectedMarkerValue = mapMarkers[0].value;
                        this.accDescription = mapMarkers[0].othersDescription;
                        this.accSiteLink = mapMarkers[0].otherSiteLink;
                    } else {
                        this.selectedMarkerValue = undefined;
                        this.accDescription = undefined;
                        this.accSiteLink = undefined;
                    }
                    this.getFilteredEvents(this.filterValues, undefined, this.privateAccessId);
                    this.flag = 1;
                }
                this.mapMarkers = mapMarkers;

                this.showMap = true;
                this.isLoaded = true;
                const screenwidth = window.outerWidth;
                if (screenwidth < 767) {
                    setTimeout(() => {
                        const topDiv = this.template.querySelector('.event_selection');
                        topDiv.scrollIntoView({
                            behavior: "smooth"
                        });
                    }, 1000);
                }
            })
            .catch(error => {
                let message = (typeof error === 'string') ? error : error.body.message;
                showAsyncErrorMessage(this, message);
                this.showLoader = false;
                this.error = error;
                this.isLoaded = false;
            });
    }


    getFilteredEvents(sObj, accountId, privateAccessId) {
        this.isEventLoaded = false;
        this.siteEvents = [];

        getEventsWithFilters({
                sObj: JSON.stringify(sObj),
                accountId: accountId,
                privateAccessId: privateAccessId,
                vaccinationPrivateAccess: this.vaccinationPrivateAccess,
                vaccineClass: this.secondDoseData ? (this.secondDoseData.hasOwnProperty('oldVaccineClass') ? this.secondDoseData.oldVaccineClass : '') : ''
            })
            .then(result => {
                this.isLoaded = true;
                let i;
                for (i = 0; i < result.length; i++) {
                    let tmpEvt = result[i].event;
                    tmpEvt.isSelectable = result[i].isSlotAvailable;
                    tmpEvt.isSelected = tmpEvt.Id === (this.eventId || this.selectedEvent);
                    this.siteEvents.push(tmpEvt);
                }
                if (this.siteEvents.length > 0) {
                    this.siteEvents.sort(function (a, b) {
                        var dateA = new Date(a.Start_Date__c),
                            dateB = new Date(b.Start_Date__c);
                        return dateA - dateB;
                    });
                }
                this.isEventLoaded = true;
            }).catch(error => {
                // TODO Error handling
                this.error = error;
                showAsyncErrorMessage(this, error);
            });
    }

    handleMarkerSelect(event) {
        // if (this.flag == 0) {
        if (this.selectedMarkerValue !== event.target.selectedMarkerValue) {
            this.selectedEvent = undefined;
            this.dispatchEvent(new CustomEvent('eventselected', {
                detail: {
                    accountId: event.target.selectedMarkerValue,
                    eventId: false
                }
            }));
        }
        this.siteEvents = [];
        this.selectedMarkerValue = event.target.selectedMarkerValue;
        if (this.selectedMarkerValue) {
            if (!this.isOtherSponser) {
                this.getFilteredEvents(this.filterValues, this.selectedMarkerValue, this.privateAccessId);
            } else {
                let mapMarkers = this.results;
                mapMarkers.forEach((marker) => {
                    if (this.selectedMarkerValue === marker.value) {
                        this.accDescription = marker.othersDescription;
                        this.accSiteLink = marker.otherSiteLink;
                    }
                });
            }
        }
    }

    handleEventSelect(event) {
        const selectedEventId = event.detail;
        let tmpSelectedEvtId = '';
        this.siteEvents.forEach(eventLocal => {
            if (!eventLocal.isSelected && eventLocal.Id === selectedEventId) {
                tmpSelectedEvtId = selectedEventId;
                eventLocal.isSelected = true;
                this.selectedMarkerValue = eventLocal.Location__c;
            } else {
                eventLocal.isSelected = false;
            }
        });

        this.selectedEvent = tmpSelectedEvtId;

        this.dispatchEvent(new CustomEvent('eventselected', {
            detail: {
                accountId: this.selectedMarkerValue,
                eventId: this.selectedEvent,
                filterValues: this.filterValues
            }
        }));

        this.dispatchEvent(new CustomEvent('eventselectedschedular', {
            detail: {
                accountId: this.selectedMarkerValue,
                eventId: this.selectedEvent,
                filterValues: this.filterValues
            }
        }));
    }

    resetValues(event) {
        this.siteEvents = [];
        this.mapMarkers = [];
        this.isAfterSearchMsg = false;
    }

    focusFirstEle() {
        const focusableElements = 'lightning-combobox, lightning-button-icon, button, input, lightning-helptext, lightning-input, lightning-button';
        const modal = this.template.querySelector('.slds-modal');
        if (modal) {
            const firstFocusableElement = modal.querySelectorAll(focusableElements)[0];
            const focusableContent = modal.querySelectorAll(focusableElements);
            const lastFocusableElement = focusableContent[focusableContent.length - 1];
            firstFocusableElement.focus();
            this.template.addEventListener('keydown', function (event) {
                let isTabPressed = event.key === 'Tab' || event.keyCode === 9;
                if (!isTabPressed) {
                    return;
                }
                if (event.shiftKey) {
                    if (this.activeElement === firstFocusableElement) {
                        lastFocusableElement.focus();
                        event.stopPropagation()
                        event.preventDefault();
                    }
                } else {
                    if (this.activeElement === lastFocusableElement) {
                        firstFocusableElement.focus();
                        event.preventDefault();
                        event.stopPropagation()
                    }
                }
            });
        }
    }
}