/**
 * @description       : 
 * @author            : Wasef Mohiuddin
 * @group             : 
 * @last modified on  : 02-17-2021
 * @last modified by  : Wasef Mohiuddin
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   02-17-2021   Wasef Mohiuddin   Initial Version
**/
@isTest
public class VT_VaccinationActivationEmailBatchTest {
    @testSetup
    public static void testData(){
        Contact conRec = new Contact(
            FirstName = 'Super', 
            LastName = 'Man', 
            Email = 'superman@gmail.com.invalid',
            BirthDate = Date.today());
        insert conRec;

        Pre_Registration_Group__c preRegGrpRec = new Pre_Registration_Group__c();
        preRegGrpRec.Name = 'Test Pre-Registration Group';
        preRegGrpRec.Status__c = 'Draft';
        preRegGrpRec.Email_Acknowledgement__c = false;
        insert preRegGrpRec;

        Pre_Registration__c preRegRec = new Pre_Registration__c();
        preRegRec.Pre_Registration_Group__c = preRegGrpRec.Id;
        preRegRec.Contact__c = conRec.Id;
        preRegRec.Status__c = 'Assigned';
        insert preRegRec;
    }

    @isTest
    public static void testVaccinationActivationEmailBatchExecution(){
        Set<Id> preRegGrpRecIdSet = new Set<Id>();
        Pre_Registration_Group__c preRegGrpRec = [SELECT Id, Name, Status__c, Email_Acknowledgement__c FROM Pre_Registration_Group__c WHERE Name = 'Test Pre-Registration Group' LIMIT 1];
        preRegGrpRec.Status__c = 'Activate';
        preRegGrpRec.Email_Acknowledgement__c = true;
        update preRegGrpRec;
        preRegGrpRecIdSet.add(preRegGrpRec.Id);

        Test.startTest();
			Id batchId = Database.executeBatch(new VT_TS_VaccinationActivationEmailBatch(preRegGrpRecIdSet));
        	System.assert( batchId != null );
		Test.stopTest();
    }
}