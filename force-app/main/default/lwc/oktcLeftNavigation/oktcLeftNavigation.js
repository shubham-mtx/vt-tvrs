import {
    LightningElement,api,track
} from 'lwc';
import { NavigationMixin } from "lightning/navigation";
import formFactorPropertyName from '@salesforce/client/formFactor';
import isVaccinationAccount from "@salesforce/apex/OkpcContactInformationController.isVaccinationAccount";
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';
export default class OktcLeftNavigation extends NavigationMixin(LightningElement)  {
    
    @api showFooter;
    @track selectedName;
    @api compName;
    @track redirectPage;
    @track home = true;
    @track dashboard = false;
    @track result = false;
    @api selectedTab;
    @track homeTabCss = 'padding_space_button';
    @track appointmenttabCss = 'padding_space_button';
    @track schedulertabCss = 'padding_space_button';
    @track downloadstabcss = 'padding_space_button';
    @track createContacttabcss = 'padding_space_button';
    @track uploadtabcss = 'padding_space_button';
    @track sendTabcss = 'padding_space_button';
    
    isTesting = true;

    get isMobile() {
        return formFactorPropertyName != 'Large';
    }

    connectedCallback(){
        if(this.selectedTab == 'dashboard' || this.selectedTab == 'default'){
            this.homeTabCss = this.homeTabCss + ' active_tab';
        } else if(this.selectedTab == 'appointments'){
            this.appointmenttabCss = this.appointmenttabCss + ' active_tab';
        } else if(this.selectedTab == 'scheduler'){
            this.schedulertabCss = this.schedulertabCss + ' active_tab';
        }else if(this.selectedTab == 'downloads'){
            this.downloadstabcss = this.downloadstabcss + ' active_tab';
        }else if(this.selectedTab == 'upload'){
            this.uploadtabcss = this.uploadtabcss + ' active_tab';
        } else if(this.selectedTab == 'sendEmail'){
            this.sendTabcss = this.sendTabcss + ' active_tab';
        }

        this.doInit();
    }

    doInit() {
        isVaccinationAccount()
            .then(result => {
                if(result) {
                    this.isTesting = false;
                }
            })
            .catch(error => {
                showAsyncErrorMessage(this, error);
            });
    }

    navigateToPage(event) {
        this.redirectPage = event.currentTarget.getAttribute("data-navigatepage");
        // if (this.redirectPage == "home") {
        //     this.home = true;
        //     this.result = false;
        // } else if(this.redirectPage == "result") {
        //     this.result = true;
        //     this.home = false;

        // }
        this.navigateToOther(this.redirectPage);
        
    }

    navigateToOther(pageName){
        if (pageName == "appointments") {
            this.navigateToWebPage('/TestingSite/s/oktc-appointments');
        } else if (pageName == "dashboard") {
            this.navigateToWebPage('/TestingSite/s/');
        } else if (pageName == "scheduler") {
            this.navigateToWebPage('/TestingSite/s/oktcconfigscheduler');
        }else if (pageName == "downloads") {
            this.navigateToWebPage('/TestingSite/s/oktcdownloads');
        }else if (pageName == "createContact") {
            this.navigateToWebPage('/TestingSite/s/create-contact');
        }else if (pageName == "upload") {
            this.navigateToWebPage('/TestingSite/s/oktcuploads');
        } else if (pageName == "sendEmail") {
            this.navigateToWebPage('/TestingSite/s/send-emails');
        }
    }
    navigateToWebPage(url) {
        // Navigate to a URL
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: url
            }
        },
        true // Replaces the current page in your browser history with the URL
        );
    }
    
}