@isTest
public with sharing class okpcRenderAsPDF_AntiTestBodiesTest {
    
    @TestSetup
    static void makeData(){

        Id accRecTypeId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;

        Account accRec = TestDataFactoryAccount.createAccount('TestSite', accRecTypeId, true);

        Contact con = TestDataFactoryContact.createCitizenContact( true, 'LN', 'FN' );

        Appointment_Slot__c appSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, accRec.Id);

        Appointment__c  app = TestDataFactoryAppointment.createAppointment(false);
        app.Patient__c = con.Id;
        app.Lab_Center__c = accRec.Id;
        app.Appointment_Slot__c = appSlot.Id;
        app.Status__c = 'Cancelled';
        insert app;

        Antibody_Testing__c rec = TestDataFactoryAntiBody.createAppointment( true, accRec.Id, app.Id, appSlot.Id, con.Id);

    }
    
    @isTest
    public static void test_1(){

        Contact con = [ SELECT Id FROM Contact WHERE FirstName = 'FN' LIMIT 1 ];
        Antibody_Testing__c testRec = [ SELECT Id FROM Antibody_Testing__c WHERE Patient__c = :con.Id ];

        ApexPages.currentPage().getParameters().put('id', String.valueOf(testRec.Id));
        okpcRenderAsPDF_AntiTestBodies rec = new okpcRenderAsPDF_AntiTestBodies();
    }
}