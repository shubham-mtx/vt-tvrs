/**
 * @description       : 
 * @author            : Balram Dhawan
 * @group             : 
 * @last modified on  : 01-15-2022
 * @last modified by  : Balram Dhawan
**/
import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';
import {
    registerListener
} from "c/pubsub";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import getContactInformation from "@salesforce/apex/OkpcContactInformationController.getContactInformation";
import isPrivateEvent from '@salesforce/apex/OkpcContactInformationController.isPrivateEvent';
import createContactWithAppointments from "@salesforce/apex/OkpcContactInformationController.createContactWithAppointments";
import createContactWithOverrideAppointment from "@salesforce/apex/OkpcContactInformationController.createContactWithOverrideAppointment";
import {
    getRecord,
    getFieldValue
} from 'lightning/uiRecordApi';
import USER_CONTACT_ID from '@salesforce/schema/User.ContactId';
import USER_CONTACT_ACCOUNT_ID from '@salesforce/schema/User.Contact.AccountId';
import {
    showAsyncErrorMessage,
    showMessage,
    msgObj
} from 'c/vtTsUtility';

export default class OkContactIntakeForm extends LightningElement {
    @api slotId;
    @api eventId;
    @api fromTestingSite;
    @api accountId;
    @api appointmentId;
    @api appointmentContact = {
        'resultRecieveConsent': false
    };
    @api istesting = false;
    @api isVaccineEvent;
    @api isFromApptScheduler;

    @track showError = false;
    @track showSuccess = false;
    @track errorMessage = '';
    @track showThankYou;
    @track thankYouMessage = '';
    @track preRegMinDateTime;
    @track eventIds;

    @track toastMessage = '';
    @track toastTitle = '';
    @track toastType = '';

    selectedEventId = '';
    selectedAccountId = '';

    @track currentStep = 1;
    @track originalStep = 1;

    showSpinner = true;
    @track edit = false;
    @track source;
    @track appointmentObject;
    @track hideStep3 = false;
    @track showDashboard;
    @track isTestingAndSelectedDoseCompleted;
    isDependentContact = false;
    dependentContactId = '';
    isLoaded = false;
    contactId;
    currentUserDetail = {};
    consentDetails = {};
    overrideConfirmation;
    existingAppointmentData;
    vaccineType;
    msgObjData = msgObj();
    showAsteriskMessage

    @api
    get selectedEvent() {
        return this.selectedEventId;
    }

    get showpage1() {
        return this.currentStep === 1;
    }

    get showpage2() {
        return this.currentStep === 2;
    }

    get showpage3() {
        return this.currentStep === 3;
    }

    get showpage4() {
        return this.currentStep === 4;
    }


    get showpage6() {
        return this.currentStep === 5;
    }

    get showpage7() {
        return this.currentStep === 7;
    }

    get showpage8() {
        return this.currentStep === 8;
    }

    get showpage9() {
        return this.currentStep === 9;
    }

    @wire(getRecord, {
        recordId: '$Id',
        fields: [USER_CONTACT_ID, USER_CONTACT_ACCOUNT_ID]
    })
    getCurrentUserDetail({
        error,
        data
    }) {
        if (data) {
            this.currentUserDetail = data;
            this.contactId = getFieldValue(this.currentUserDetail, USER_CONTACT_ID);
        }
    }

    loadStyle() {
        Promise.all([
            loadStyle(this, TVRS_INTERNAL_STYLE)
        ]).then(() => {
            console.log('css loaded');
        }).catch((error) => {
            showAsyncErrorMessage(this, error);
        });
    }

    connectedCallback() {

        this.showSpinner = true;
        this.showAsteriskMessage = (this.isFromApptScheduler || this.fromTestingSite);
        registerListener("handleSideBarClick", this.handleSideBarClick, this);
        this.getStep();
        isPrivateEvent({
                eventId: this.eventId
            })
            .then(result => {
                this.isVaccineEvent = result;
                this.showSpinner = false;
            })
            .catch(error => {
                showAsyncErrorMessage(this, error);
                this.showSpinner = false;
            })

    }



    getStep() {
        getContactInformation()
            .then(result => {
                this.isLoaded = true;
                this.showSpinner = false;
            })
            .catch(error => {
                showAsyncErrorMessage(this, error);
                this.showSpinner = false;
            })
    }

    handleCancelAppointment() {
        this.showDashboard = true;
        this.dispatchEvent(new CustomEvent('cancelappointment'));
    }

    navigateToConsent() {
        this.currentStep = 1;
        this.determineOriginalStep();
    }

    //Save Contact
    saveContacts(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.showDashboard = true;
        }
    }

    handleEdit() {
        this.edit = true;
        this.template.querySelector('c-okpc-contact-info').editContacts();
    }

    handleCancel() {
        this.edit = false;
        this.template.querySelector('c-okpc-contact-info').handleCancelButton();
    }
    handleSave() {
        this.edit = false;
        this.template.querySelector('c-okpc-contact-info').saveContacts();
    }

    determineOriginalStep() {
        if (this.originalStep >= this.currentStep) {
            this.originalStep = this.originalStep;
        } else {
            this.originalStep = this.currentStep;
        }
    }

    handleSideBarClick(currentStep) {
        this.currentStep = currentStep;
        if(this.currentStep===4)
            this.currentStep=5;
    }

    handleback(event) {
        this.currentStep = this.currentStep - 1;
        if(this.currentStep===4)
            this.currentStep=3;
    }

    showToast(title, message, type) {
        if (!this.istesting) {
            this.toastMessage = message;
            this.toastTitle = title;
            this.toastType = type;
            this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
        } else {
            this.dispatchEvent(new ShowToastEvent({
                message: 'Please select consent to move forward',
                variant: 'error'
            }));
        }
    }

    //1st Page
    consentSave(event) {
        if (event.detail.status === "error") {
            this.showToast('Error!!!', event.detail.message, 'error');
        } else if (event.detail.status === 'saveNew') {
            this.currentStep = 2;
            this.determineOriginalStep();
            this.consentDetails = event.detail.consentParams;
            this.appointmentContact.consentValue = event.detail.consentParams.consentvalue;
        } else {
            this.consentDetails = {};
            this.currentStep = 2;
            this.determineOriginalStep();
        }
    }

    //2nd Page
    navigateToRegistration(event) {
        this.template.querySelector('c-okcp-contact-info-new').getContactObj();
    }

    setContactPage2(event) {
        const detail = JSON.parse(event.detail.jsonData);
        this.vaccineType = event.detail.vaccineType;
        this.isTestingAndSelectedDoseCompleted =  event.detail.isTestingAndSelectedDoseCompleted;
        this.appointmentContact = {
            ...this.appointmentContact,
            ...detail
        };
        this.currentStep = 3;
        this.determineOriginalStep();
    }

    //3rd Page
    setContactPage3(event) {
        const detail = event.detail;
        this.appointmentContact = {
            ...this.appointmentContact,
            ...detail
        };
        this.currentStep = 5;
        this.determineOriginalStep();
    }

    demogrphicsSaved(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        }
        this.showToast('Error!!!', event.detail.message, 'error');
    }

    //4th Page
    setContactPage4(event) {
        const detail = event.detail;
        this.appointmentContact = {
            ...this.appointmentContact,
            ...detail
        };
        this.isTestingAndSelectedDoseCompleted = event.detail.isTestingAndSelectedDoseCompleted;
        this.currentStep = 5; // 4->5
        this.determineOriginalStep();
    }

    testingSaved(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        }
        this.showToast('Error!!!', event.detail.message, 'error');

    }


    //6th Page
    setContactPage6(event) {
        if (event.detail.eventId) {
            this.eventId = event.detail.eventId;
        }
        if (event.detail.slotId) {
            this.slotId = event.detail.slotId;
        }
        const detail = event.detail.data;
        this.appointmentContact = {
            ...this.appointmentContact,
            ...detail
        };
        this.handleSubmit();
    }

    handleWaiver(event) {
        if (event.detail.status === "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        }
        this.showToast('Error!!!', event.detail.message, 'error');
    }

    handleSubmit() {
        this.appointmentContact.testingSite = this.accountId;
        if (!this.eventId) {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please Select an Event',
            }));
            this.showToast('Error!!!', 'Please Select an Event', 'error');
            return;
        }else if(this.isTestingAndSelectedDoseCompleted){
            if (!this.istesting) {
                this.toastMessage = 'It looks like the Contact already had their selected dose of a COVID-19 vaccine.  Please use the Schedule Next Dose Button from the contact record to schedule their Next dose Appointment.';
                this.toastTitle = 'Error !';
                this.toastType = 'Error !';
                this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
            } else {
                showMessage(this,'Error!','error',this.msgObjData['isSelectedDoseAlreadyCompleted']);
            }
            return;
        }

        this.showSpinner = true;
        this.appointmentContact.vaccineType = this.vaccineType;

        createContactWithAppointments({
            JsonData: JSON.stringify(this.appointmentContact),
            recordType: 'Citizen_COVID',
            slotId: this.slotId,
            eventId: this.eventId,
            isTesting: this.istesting
        }).then(result => {
            if (result) {


                if (result.is3rdDoseAlreadyScheduled || result.isSelectedDoseAlreadyScheduled||  result.is2ndDoseAlreadyScheduled || result.furtherDosesNotReqd || result.isSelectedDoseAlreadyCompleted || result.isAlreadyScheduled || result.isAlreadyCompleted || result.secondDoseNotRequired || result.privateAcesssAssignmentNotFound) {
                    if (result.furtherDosesNotReqd) {
                        this.showError = true;
                        this.errorMessage = 'You have already received all required doses of your COVID-19 vaccine, so you do not need to sign up to get any additional doses. If you have questions, please visit the <a href="https://www.healthvermont.gov/covid-19/vaccine/about-covid-19-vaccines-vermont" target="_blank">Vermont Department of Health website</a> or call the COVID-19 Call Center at 802-863-7240.';
                    } 
                    else if(result.isSelectedDoseAlreadyScheduled){
                        if (this.istesting) {
                            if((result.doseNumber === 'First Dose' && this.appointmentContact.VaccineQuestion_DoseNum__c !== 'Second Dose') ||result.doseNumber !== 'First Dose' ){
                                result.existingAppointmentData.Appointment_Start_Time__c = result.existingAppointmentData.hasOwnProperty('Appointment_Start_Time__c') ? result.existingAppointmentData.Appointment_Start_Time__c.substr(0, 5) : '';
                                this.existingAppointmentData = result.existingAppointmentData;
                                this.overrideConfirmation = true;
                            }
                        }
                        else {
                            this.showError = true;
                            this.errorMessage = 'It looks like you already have an appointment to get your '+result.doseNumber+ ' of a COVID-19 vaccine. If you want to change your appointment, you need to cancel your existing appointment first.';
                        } 
                    }
                    
                    else if (result.isSelectedDoseAlreadyCompleted) {
                        this.showError = true;
                        this.errorMessage = 'It looks like you have already completed your selected dose of a COVID Vaccine. If you would like to cancel any follow-up appointments, please use the CANCEL Button on your appointment record.';
                        
                    } else if (result.isAlreadyCompleted && result.vaccineType !== 'Vaccination-2') {
                        this.showError = true;
                        this.errorMessage = 'It looks like you have already had your first dose of a COVID Vaccine. If you would like to cancel any follow-up appointments, please use the CANCEL Button on your appointment record. However, in order to reschedule your 2nd dose appointment, you will need to contact the Vermont Department of Health COVID-19 Call Center at (802) 863-7240.';
                        
                    } else if (result.secondDoseNotRequired) {
                        this.showError = true;
                        this.errorMessage = 'You have already received your COVID-19 vaccine. The vaccine you got only requires one dose, so you do not need to sign up to get any additional doses. If you have questions, please visit the <a href="https://www.healthvermont.gov/covid-19/vaccine/about-covid-19-vaccines-vermont">Vermont Department of Health website</a> or call the COVID-19 Call Center at 802-863-7240.';
                    }
                    else if (result.previousDoseInScheduledStatus) {
                        this.showError = true;
                        this.errorMessage = result.previousDoseInScheduledStatus;
                    }
                    else if (result.privateAcesssAssignmentNotFound) {
                        this.showError = true;
                        this.errorMessage = result.privateAcesssAssignmentNotFound;
                    }
                    this.showSpinner = false;
                    return false;
                }
                this.showSpinner = false;
                if (this.istesting) {
                    if (result.type === 'success') {
                        this.dispatchEvent(new ShowToastEvent({
                            title: '',
                            variant: 'success',
                            message: result.message,
                        }));

                       
                        eval("$A.get('e.force:refreshView').fire();");
                    }

                }

                if (result.type !== 'success') {
                    this.showError = true;
                    this.errorMessage = result.message;
                    this.showSpinner = false;
                } else if (result.type === 'success' && !this.istesting) {
                    this.showSpinner = false;
                    this.showThankYou = true;
                    this.thankYouMessage = result.message;
                    this.dispatchEvent(new CustomEvent('registered'));
                }
            }
        }).catch(error => {
            this.showSpinner = false;
            // Issue Start I-25856
            var msg = JSON.stringify(error.body.message);
            this.errorMessage = msg;
            this.showError = true;
            showAsyncErrorMessage(this, error);
        });
    }

    confirmationCancel() {
        this.overrideConfirmation = false;
    }

    confirmOverride() {
        this.showSpinner = true;
        createContactWithOverrideAppointment({
            JsonData: JSON.stringify(this.appointmentContact),
            recordType: 'Citizen_COVID',
            slotId: this.slotId,
            eventId: this.eventId,
            isTesting: this.istesting,
            overrideAppointment: true
        }).then(result => {
            this.showSpinner = false;
            if (result.type !== 'success') {
                this.showError = true;
                this.errorMessage = result.message;
                this.showSpinner = false;
            } else {
                this.overrideConfirmation = false;
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: result.message,
                }));
                eval("$A.get('e.force:refreshView').fire();");
            }
            this.showSpinner = false;
        }).catch(error => {
            // Issue Start I-25856
            var msg = JSON.stringify(error.body.message);
            this.errorMessage = msg;
            this.showError = true;
            // Issue End I-25856
            this.showSpinner = false;
        });
    }

    handleEventSelected(event) {
        this.selectedEventId = event.detail;
    }

    handleAccountSelected(event) {
        this.selectedAccountId = event.detail;
    }

    updateMinDateTimePreReg(event) {
        this.preRegMinDateTime = event.detail.preRegMinDateTime;
        this.eventIds = event.detail.eventIds;
    }

    handleChangeVaccineEvent(){
        this.dispatchEvent(new CustomEvent('changevaccineevent'));
    }

    @api doRegisteration() {

    }
}