/* 
* Class Name: VT_TS_AppointmentReminderWrapper
* Description: To get custom label json and convert it into Class Object.
* Author : Shubham Dadhich
* Email : shubham.dadhich@mtxb2b.com
*
* Version       Modification        User         Date               Description
* V1            Initial             Shubham D    09-March-2021      Intial Creation of Class   
*/

public class VT_TS_AppointmentReminderWrapper {

    public Integer prior_notification_days;
    public Integer cancilation_reminder_total_days;
    public Integer cancilation_reminder_gape_days;
    public Integer day_to_add_for_testing;
    public Integer firstDoseReminderDay;
    public Integer secondDoseReminderDay;
    public Integer thirdDoseReminderDay;
    public Integer fourthDoseReminderDay;
    public Integer notificationLeadTime;
    public Integer rateLimit;

}