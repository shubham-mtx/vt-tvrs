/**
 * @description       : 
 * @author            : Wasef Mohiuddin
 * @group             : 
 * @last modified on  : 02-17-2021
 * @last modified by  : Wasef Mohiuddin
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   02-17-2021   Wasef Mohiuddin   Initial Version
**/
@isTest
public class VT_PreRegPendingVerificationToAssignTest {
    @testSetup
    public static void testData(){
        Contact conRec = new Contact(
            FirstName = 'Super', 
            LastName = 'Man', 
            MiddleName = 'Clark',
            Email = 'superman@gmail.com.invalid',
            BirthDate = Date.today());
        insert conRec;

        Pre_Registration_Group__c preRegGrpRec = new Pre_Registration_Group__c();
        preRegGrpRec.Name = 'Test Pre-Registration Group';
        preRegGrpRec.Status__c = 'Open';
        preRegGrpRec.Email_Acknowledgement__c = false;
        preRegGrpRec.Verification_Delay__c = -1;
        preRegGrpRec.RecordTypeId = Schema.getGlobalDescribe().get('Pre_Registration_Group__c').getDescribe().getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        insert preRegGrpRec;

        Pre_Registration__c preRegRec = new Pre_Registration__c();
        preRegRec.Early_Group1_Health__c = preRegGrpRec.Id;
        preRegRec.Contact__c = conRec.Id;
        preRegRec.Status__c = 'Pending Verification';
        insert preRegRec;
    }

    @isTest
    public static void testPreRegPendingVerificationToAssignBatchExecution(){

        Test.startTest();
			Id batchId = Database.executeBatch(new VT_TS_PreRegPendingVerificationToAssign());
        	System.assert( batchId != null );
		Test.stopTest();
    }

    @isTest
    public static void testPreRegPendingVerificationToAssignScheduleJobExecution(){

        Test.startTest();
            VT_TS_PreRegPendingVerificationToAssign sc = new VT_TS_PreRegPendingVerificationToAssign();
            String hour = String.valueOf(Datetime.now().hour());
            String min = String.valueOf(Datetime.now().minute() + 1); 
            String ss = String.valueOf(Datetime.now().second());

            //parse to cron expression
            String cronExp = ss + ' ' + min + ' ' + hour + ' * * ?';
            String jobID = System.schedule('Job Started At ' + String.valueOf(Datetime.now()), cronExp, sc);
        	System.assert( jobID != null );
		Test.stopTest();
    }
}