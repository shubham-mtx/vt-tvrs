/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 12-30-2021
 * @last modified by  : Mohit Karani
**/
@isTest
public with sharing class TVRS_WrapperTest {
   
    @TestSetup
    static void makeData(){
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.Name = 'Self Registration';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        createAccountTestSite.Event_Process__c =VT_TS_Constants.TESTING_ACCOUNT_TYPE_TEXT;
        createAccountTestSite.Type_of_Contact__c ='Consultation';
        createAccountTestSite.Number_of_Weeks_Available__c ='4';
        createAccountTestSite.Appointment_Frequency__c ='5';
        insert createAccountTestSite;
        DateTime dt = DateTime.newInstance(2021, 12, 22, 0, 2, 2);
        Contact con = new Contact();
        con.FirstName='Test';
        con.LastName='User';
        con.Preferred_Date__c=date.newinstance(2021, 12, 22);
        con.Preferred_Time__c= dt.time();
        con.AccountId = createAccountTestSite.Id;
		insert con;
		Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);

        Appointment__c appointment = new Appointment__c();
        appointment.Status__c = VT_TS_Constants.SCHEDULED_TEXT;
        appointment.Symptomatic__c = 'Yes';
        appointment.Patient__c = con.Id;
        appointment.Appointment_Slot__c = createSlot.id;
        insert appointment;
    }
    @isTest
    public static void wrapperTest(){
        TVRS_Wrapper.SelectOptionWrapper selectionOptionWrapper = new TVRS_Wrapper.SelectOptionWrapper('value','label');
        Account acc = [SELECT Id,Name,BillingStreet,BillingCity,BillingState,BillingCountry,BillingPostalCode,BillingLatitude,
                        BillingLongitude,Phone,Event_Type__c,Description,Site_URL__c,Business_Start_Time__c,Business_End_Time__c,
                        Event_Process__c,Type_of_Contact__c,Number_of_Weeks_Available__c,Appointment_Frequency__c
                       FROM Account
                       LIMIT 1];
       	Appointment__c appointment = [SELECT Id,Appointment_Date__c,Start_Time__c,End_Time__c,Patient__r.Testing_Site__r.Name,
                                     Status__c,Patient__r.Preferred_Date__c,Patient__r.Preferred_Time__c,Patient__r.Testing_Site__c,
                                     Symptomatic__c 
                                     FROM Appointment__c
                                     LIMIT 1];
        
        TVRS_Wrapper.MapMarker mapMarkerWrapper = new TVRS_Wrapper.MapMarker(acc);
        TVRS_Wrapper.AccountContactWrapper accConWrapper = new TVRS_Wrapper.AccountContactWrapper(acc,null);
        TVRS_Wrapper.AppointmentWrapper appointmentWrapper = new TVRS_Wrapper.AppointmentWrapper(appointment);
        System.assertEquals(appointment.Status__c, appointmentWrapper.status, 'error');
    }
}