import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import register from '@salesforce/apex/okpcRegisterController.selfRegister';
import getContactInformation from '@salesforce/apex/okpcRegisterController.getContactInformation';
import communityURL from '@salesforce/label/c.OKPC_Community_url';
import loginURL from '@salesforce/label/c.OKPC_Login_URL';
import OK_PatientIdForgetURL from '@salesforce/label/c.OK_PatientIdForgetURL';
import OK_SelfRegistrationURL from '@salesforce/label/c.OK_SelfRegistrationURL';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import{
    showAsyncErrorMessage,
    validateEmail
}from'c/vtTsUtility';
export default class OkpcRegistrationpage extends NavigationMixin(LightningElement) {

    @track label = {
        OK_PatientIdForgetURL,
        OK_SelfRegistrationURL,
    }
    @track showError = false;
    @track errorMessage = '';
    @track patientId = '';
    @track username = '';
    @track password = '';
    @track email = '';
    @track confirmPassword = '';
    @track contactId;
    @track OK_SelfRegistrationURL;
    @track eventId='';
    @track dob;
    @track showSpinner = false;

    customImage = nysdohResource;
    navigateToLogin() {
        location.href = loginURL;
    }
    navigateToHome() {
        location.href = communityURL;
    }
    redirectToUrl(urlValue) {
        location.href = urlValue;
    }

    patientIdChange(event) {
        this.patientId = event.target.value;
    }
    emailChange(event) {
        this.email = event.target.value;
    }


    checkValue(str, max) {
        if (str.charAt(0) !== '0' || str === '00') {
            var num = parseInt(str);
            let dateInput = this.template.querySelector('.customDOB');
            if (isNaN(num) || num <= 0 || num > max) {
                dateInput.setCustomValidity('Invalid Date entered, please enter the date in MM-DD-YYY format');
                dateInput.reportValidity();
                num = 1;
            } else {
                dateInput.setCustomValidity('');
            }
            str = num > parseInt(max.toString().charAt(0)) && num.toString().length === 1 ? '0' + num : num.toString();
        };
        return str;
    }

    dobChange(event) {
        var input = event.target.value;
        if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
        var values = input.split('-').map(function(v) {
            return v.replace(/\D/g, '')
        });
        if (values[0]) {
            values[0] = this.checkValue(values[0], 12);
        } 
        if (values[1]) {
            if(values.length === 3 && values[2].length === 4) {
                values[1] = this.checkValue(values[1], new Date(values[2], values[0], 0).getDate());
            } else {
                values[1] = this.checkValue(values[1], 31);
            }
        } 
        var output = values.map(function(v, i) {
            return v.length === 2 && i < 2 ? v + '-' : v;
        });
        // this.value = output.join('').substr(0, 14);
        event.target.value = output.join('').substr(0, 14);
        var output = '';
        
        if (values.length === 3 && this.checkValue(values[1], new Date(values[2], values[0], 0).getDate())) {
            if(values[2].length === 4) {
                var year = values[2].length !== 4 ? parseInt(values[2]) + 2000 : parseInt(values[2]);
                var month = parseInt(values[0]) - 1;
                var day = parseInt(values[1]);
                var d = new Date(year, month, day);
                if (!isNaN(d)) {
                    
                    var dates = [d.getMonth() + 1, d.getDate(), d.getFullYear()];
                    output = dates.map(function(v) {
                        v = v.toString();
                        return v.length === 1 ? '0' + v : v;
                    }).join('-');
                }
            }
        };
        this.dob = output;
    }

    usernameChange(event) {
        this.username = event.target.value;
    }
    passwordChange(event) {
        this.password = event.target.value;
    }
    confirmPasswordChange(event) {
        this.confirmPassword = event.target.value;
    }

    connectedCallback() {
        this.eventId = this.findGetParameter('eventId');
        this.email = this.findGetParameter('emailId');
        this.OK_SelfRegistrationURL = (this.eventId) ? this.label.OK_SelfRegistrationURL + '?eventId=' + this.eventId : this.label.OK_SelfRegistrationURL;        
    }

    doRegisteration() {
        this.showError = false;
        if (this.patientId == '' || typeof this.patientId == 'undefined') {
            this.errorMessage = 'Please provide Patient Id.';
            this.showError = true;
            return;
        }
       
        if (this.email === '' || typeof this.email === 'undefined') {
            this.errorMessage = 'Please provide an email address.';
            this.showError = true;
            return;
        }

        if (this.dob === '' || typeof this.dob === 'undefined') {
            this.errorMessage = 'Please provide Date of Birth';
            this.showError = true;
            return;
        }
        
        if (this.password === '' || typeof this.password === 'undefined') {
            this.errorMessage = 'Please provide a password';
            this.showError = true;
            return;
        }
        if (this.confirmPassword === '' || typeof this.confirmPassword === 'undefined') {
            this.errorMessage = 'Please enter confirm password.';
            this.showError = true;
            return;
        }
        if (this.confirmPassword !== this.password) {
            this.errorMessage = 'Password and Confirm password do not match.';
            this.showError = true;
            return;
        }
        if (!this.validatePassword(this.password)) {
            this.errorMessage = 'Your password must include characters, numbers and at least one special character and minimum length of 10 characters.';
            this.showError = true;
            return;
        }
        if (!validateEmail(this.email)) {
            this.errorMessage = 'Please provide a valid email address. Example: "abc@xyz.com"';
            this.showError = true;
            return;
        }
        
        if(this.template.querySelector('c-vt_ts_custom_date').isValid()){
            // this.showSpinner = true;
            register({
                patientid: this.patientId /*'okcp'*/ ,
                email: this.email,
                username: this.email,
                password: this.password,
                confirmPassword: this.confirmPassword,
                delimiter: '.covid',
                eventId : this.eventId,
                dob     : this.dob
            }) //Added delimiter by Sajal
            .then(data => {
                this.showSpinner = false;
                if (data.type === 'error') {
                    this.showError = true;
                    this.errorMessage = data.message;
                } else if (data.type === 'success') {
                    this.redirectToUrl(data.redirectURL);
                }
    
            })
            .catch(error => {
                this.showSpinner = false;
                this.showError = true;
                showAsyncErrorMessage(this,JSON.stringify(error))
                this.errorMessage = msg.substring(1, msg.length - 1);
            });
        }else {
            this.dispatchEvent(new ShowToastEvent({
                message: 'Please check Date of Birth',
                variant: 'error'
            }));
        }
    }

    getContactInfo() {
        let newURL = new URL(window.location.href).searchParams;
        this.contactId = newURL.get('contactid');
        getContactInformation({
                contactId: this.contactId
            })
            .then(data => {
                this.username = data.email;
                this.patientId = data.patientId;
                this.email = data.email;
            })
            .catch(error => {
                showAsyncErrorMessage(this,JSON.stringify(error))
            });
    }

    validatePassword(password) {
        var regExpPhone = /^(?=.*[a-z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{10,30}$/;
        var isValid = password.match(regExpPhone);
        return isValid;
    }

    findGetParameter(parameterName){
        var result = null,
            tmp = [];
        location.search
            .substr(1)
            .split("&")
            .forEach(function (item) {
              tmp = item.split("=");
              if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]).replace(" ","+");
            });
        return result;
    }

    updateDOBField(event){

        this.dob = event.detail;
    }
}