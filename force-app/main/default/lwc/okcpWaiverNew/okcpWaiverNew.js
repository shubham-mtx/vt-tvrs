import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';
import getWaiver from "@salesforce/apex/OkpcContactInformationController.getWaiver";
import setWaiver from "@salesforce/apex/OkpcContactInformationController.setWaiver";

export default class OkcpWaiver extends LightningElement {
    @api isVaccination;
    @api isAppointment;
    @api appointmentContact = {};
    @api dependentContactId = '';
    @api appointmentId;
    @api isFromApptScheduler;
    @api accountId;
    @api preRegMinDateTime;
    @api eventIds
    @api insideCovidPortal;
    @track isReq = false;
    @track age;
    @track obj = {};
    @track showSpinner;
    @api eventId;
    @track slotId;
    @track showLocationPicker = false;

    currentstep = 7;
    isAgreeToTheTerms = true;
    openContactSearchWindow = false;
    showPageHeader = true;

    connectedCallback() {
        if (this.isAppointment) {
            this.obj = JSON.parse(JSON.stringify(this.appointmentContact));
            this.calculateAge(this.obj.dob);
            this.isReq = this.age < 18 ? true : false;
            if (this.isReq && this.obj.hasOwnProperty('parentContactId')) {
                this.obj.Name_of_Parent = this.obj.parentContactName;
                this.obj.parentId = this.obj.parentContactId;
            }

        } else {
            this.showSpinner = true;
            getWaiver({
                    appointmentId: this.appointmentId
                })
                .then(result => {
                    this.obj.Name_of_Parent = result.Name_of_Parent__c;
                    this.obj.Minor_State_Representative_Consent = result.Minor_State_Representative_Consent__c ? 'True' : result.Minor_State_Representative_Consent__c;
                    this.obj.Agree_to_the_Terms = result.Agree_to_the_Terms__c ? 'True' : result.Agree_to_the_Terms__c;
                    //added by Yogesh w.r.t I-27877
                    this.calculateAge(result.Patient__r.Birthdate);
                    this.isReq = this.age < 18 ? true : false;
                    this.showSpinner = false;
                })
                .catch(error => {
                    showAsyncErrorMessage(this, error);
                    this.showSpinner = false;
                })
        }

        this.focusTitle();
    }

    //added by Yogesh w.r.t I-27877


    handleChange(event) {
        this.obj[event.target.name] = event.target.value;
        if (event.target.name === 'Minor_State_Representative_Consent' || event.target.name === 'Agree_to_the_Terms') {
            this.obj[event.target.name] = event.target.checked ? 'True' : null;

        }
        if (event.target.name === 'Agree_to_the_Terms') {
            this.isAgreeToTheTerms = event.target.checked ? true : false;
        }

    }

    navigateToBack() {
        const selectedEvent = new CustomEvent("backevent", {
            detail: null
        });
        this.dispatchEvent(selectedEvent);
    }

    sendEventToParent(obj) {
        const selectedEvent = new CustomEvent("save", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }
    calculateAge(date) {

        var Bday = +new Date(date);
        this.age = ~~((Date.now() - Bday) / (31557600000));
    }

    saveData() {

        let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        if (!isAllValid) {
            this.template.querySelector('lightning-input').focus();
        }

        if (!this.isVaccination && isAllValid) {
            if (this.obj.Agree_to_the_Terms === null || !this.obj.Agree_to_the_Terms) {
                this.isAgreeToTheTerms = false;
                return;
            }
            //added by Yogesh w.r.t I-27877
            if (this.isReq && (!this.obj.Minor_State_Representative_Consent || this.obj.Minor_State_Representative_Consent === null || this.obj.Name_of_Parent === '' || this.obj.Name_of_Parent === null)) {
                if (this.isAppointment) {

                    let tempObj = {};
                    tempObj.message = "Please Fill all the required fields";
                    tempObj.status = "error";
                    this.sendEventToParent(tempObj);
                    return;
                } else {
                    return;
                }
            }
        }
        if (isAllValid) {
            if (this.isAppointment) {
                const selectedEvent = new CustomEvent("setobject", {
                    detail: {
                        data: this.obj,
                        eventId: this.eventId,
                        slotId: this.slotId
                    }
                });
                this.dispatchEvent(selectedEvent);
            } else {
                this.showSpinner = true;
                setWaiver({
                        data: this.obj,
                        currentstep: this.currentstep,
                        AppointmentId: this.appointmentId
                    })
                    .then(result => {
                        this.showSpinner = false;
                        let tempObj = {};
                        tempObj.message = "saved successfully";
                        tempObj.status = "success";
                        this.sendEventToParent(tempObj);
                    })
                    .catch(error => {
                        showAsyncErrorMessage(this, error);
                        this.showSpinner = false;
                    })
            }
        }
    }

    navigateToCancel() {
        this.dispatchEvent(new CustomEvent('cancelappointment'));
    }

    openContactSearch() {
        this.openContactSearchWindow = true;
    }

    closeContactSearch() {
        this.openContactSearchWindow = false;
    }

    selectNewLocation() {
        this.showLocationPicker = true;
    }

    setSelectedEventId(event) {
        this.eventId = event.detail;
    }

    closeModal() {
        this.showLocationPicker = false;
    }

    setEventAndSlotId(event) {
        this.eventId = event.detail.eventId;
        this.slotId = event.detail.slotId;
        this.accountId = event.detail.selAccId;
        this.showLocationPicker = false;
    }

    focusTitle() {
        let focus = setInterval(() => {
            let cmp = this.template.querySelector('.pageTitle');
            if (cmp) {
                cmp.focus();
                setTimeout(() => {
                    this.showPageHeader = false;
                }, 500);
                clearInterval(focus);
            }
        }, 200);
    }
}