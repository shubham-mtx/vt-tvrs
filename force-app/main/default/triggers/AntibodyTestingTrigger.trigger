trigger AntibodyTestingTrigger on Antibody_Testing__c (after insert,after update) {
    
    List <Antibody_Testing__c> newTestingIDs = new List < Antibody_Testing__c > ();
    List <Antibody_Testing__c> updatedTestingIDs = new List < Antibody_Testing__c > ();
    List <Antibody_Testing__c> updatedTestingWithResultsIDs = new List < Antibody_Testing__c > ();
    List <Antibody_Testing__c> updatedAppointmentIds = new List < Antibody_Testing__c > ();
    if(trigger.isInsert && trigger.isAfter){
        For(Antibody_Testing__c testing : [SELECT Id, Patient__r.RecordTypeId,Results__c, Notify_User_via_SMS__c, 
                                           Patient__r.Account.Communication__c, Patient__r.Opt_out_for_SMS_Updates__c, 
                                           Patient__r.Notify_Patients__c,RecordTypeId, Patient__r.MobilePhone,Appointment__r.Name 
                                           FROM Antibody_Testing__c 
                                           Where Id IN: trigger.new]){
                                               
                                               if(testing.Appointment__c!=null){
                                                   updatedAppointmentIds.add(testing);
                                               }
                                               
                                               if(testing.Results__c!=null){
                                                   newTestingIDs.add(testing);
                                               }
                                           }
        if(newTestingIDs.size()>0){
            AntibodyTestingTriggerHandler.sendConsentMessage(newTestingIDs);
          //  ResultTriggerHandler.populateAppointmentIdWithAppointmentNumber(updatedAppointmentIds);
        }
        if(!updatedAppointmentIds.isEmpty()){
            ResultTriggerHandler.populateAppointmentIdWithAppointmentNumber(updatedAppointmentIds);
        }
        
    }
    
    if(trigger.isUpdate && trigger.isAfter){
        For(Antibody_Testing__c testing : [SELECT Id, Patient__r.RecordTypeId, Results__c, Consent__c, Consent_Content__c, 
                                           Notify_User_via_SMS__c, patient__r.Account.Communication__c, Patient__r.Opt_out_for_SMS_Updates__c,
                                           Patient__r.Notify_Patients__c,RecordTypeId, Patient__r.MobilePhone 
                                           FROM Antibody_Testing__c 
                                           Where Id IN: trigger.new]){
                                               if((testing.Notify_User_via_SMS__c==true || testing.Consent__c==true) && (testing.Consent_Content__c==null) && (testing.Results__c == trigger.oldMap.get(testing.Id).Results__c)
                                                  && (testing.Notify_User_via_SMS__c != trigger.oldMap.get(testing.Id).Notify_User_via_SMS__c ||testing.Consent__c != trigger.oldMap.get(testing.Id).Consent__c)){
                                                      updatedTestingIDs.add(testing);
                                                  }
                                               System.debug('isUpdate updatedTestingWithResultsIDs---AntibodyTestingTrigger 1');
                                               if((testing.Notify_User_via_SMS__c==true || testing.Consent__c==true) && (testing.Results__c != trigger.oldMap.get(testing.Id).Results__c)){
                                                   updatedTestingWithResultsIDs.add(testing);
                                               }
                                           }
        if(updatedTestingIDs.size()>0){
            AntibodyTestingTriggerHandler.sendConsentMessage(updatedTestingIDs);
        }
        if(updatedTestingWithResultsIDs.size()>0){
            System.debug('updatedTestingWithResultsIDs---AntibodyTestingTrigger 2');
            AntibodyTestingTriggerHandler.sendConsentMessage(updatedTestingWithResultsIDs);
        }
    }
}