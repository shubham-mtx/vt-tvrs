/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 12-30-2021
 * @last modified by  : Mohit Karani
**/
public without sharing class VTTS_PreRegHelper {
    public static List<Pre_Registration__c> oldPreRegRecord;

    public static void preRegEligiblityHelper(Map<String, Object> result, Pre_Registration__c preRegRecord){
        
        //If Pre-Reg exists but no group is assigned
        if( String.isBlank(preRegRecord.Pre_Registration_Group__c) && 
            String.isBlank(preRegRecord.Early_Group_2_Access_Group__c) && 
            String.isBlank(preRegRecord.Early_Group1_Health__c) ){
            
            result.put('ageRestriction', true);
        }

        if(
            (   String.isBlank(preRegRecord.Pre_Registration_Group__c) || (preRegRecord.Pre_Registration_Group__c != null && preRegRecord.Pre_Registration_Group__r.Status__c != 'Activate')
            ) &&
            (
                String.isBlank(preRegRecord.Early_Group_2_Access_Group__c) || (preRegRecord.Early_Group_2_Access_Group__c != null && preRegRecord.Early_Group_2_Access_Group__r.Status__c != 'Activate')
            ) && 
            (
                String.isNotBlank(preRegRecord.Early_Group1_Health__c) && preRegRecord.Early_Group1_Health__r.Status__c == 'Activate') && 
                preRegRecord.Status__c == 'Pending Verification' ){
                
            if( preRegRecord.Chronic_Condition__c != 'Rejected' ){
                Datetime preRegDatetime;
                
                if( preRegRecord.Id != null && preRegRecord.High_Risk_Health_Registration_Date__c != null ){
                    preRegDatetime = preRegRecord.High_Risk_Health_Registration_Date__c;
                }
                else{
                    preRegDatetime = Datetime.now();
                }

                preRegDatetime = preRegDatetime.addHours(Integer.valueOf(preRegRecord.Early_Group1_Health__r.Verification_Delay__c));
                
                String minDate = preRegDatetime.month() + '/' + preRegDatetime.day() + '/' + preRegDatetime.year();
                String minTime = '00:00:00';

                result.put('preRegMinDateTime',minDate + '**'+ minTime);
                result.put('canSchedule', true);

                result.put('groupIds', preRegRecord.Early_Group1_Health__c );
            }
            else{
                result.put('preRegGroupNotActive', 'Thank you for Pre-registering for COVID-19 Vaccine. Your group is not Open for scheduling at this time, but you will be notified when your Group is opened for scheduling.');
            }
        }
        else if( (String.isNotBlank(preRegRecord.Pre_Registration_Group__c) && preRegRecord.Pre_Registration_Group__r.Status__c == 'Activate') || 
            (String.isNotBlank(preRegRecord.Early_Group1_Health__c) && preRegRecord.Early_Group1_Health__r.Status__c == 'Activate') || 
            (String.isNotBlank(preRegRecord.Early_Group_2_Access_Group__c) && preRegRecord.Early_Group_2_Access_Group__r.Status__c == 'Activate') ) {
            
            String groupIds = ''; 
            if( String.isNotBlank(preRegRecord.Pre_Registration_Group__c) && preRegRecord.Pre_Registration_Group__r.Status__c == 'Activate' ){
                groupIds += preRegRecord.Pre_Registration_Group__c + ',';
            }
            if( String.isNotBlank(preRegRecord.Early_Group1_Health__c) && preRegRecord.Early_Group1_Health__r.Status__c == 'Activate' && preRegRecord.Chronic_Condition__c != 'Rejected' ){
                groupIds += preRegRecord.Early_Group1_Health__c + ',';
            }
            if( String.isNotBlank(preRegRecord.Early_Group_2_Access_Group__c) && preRegRecord.Early_Group_2_Access_Group__r.Status__c == 'Activate' ){
                groupIds += preRegRecord.Early_Group_2_Access_Group__c + ',';
            }

            groupIds = groupIds.removeEnd(',');

            result.put('canSchedule', true);

            result.put('groupIds', groupIds );
        }
        else if( String.isNotBlank(preRegRecord.Pre_Registration_Group__c) || String.isNotBlank(preRegRecord.Early_Group1_Health__c) || String.isNotBlank(preRegRecord.Early_Group_2_Access_Group__c)  ){
            Boolean isAnyGroupActivae = false;

            if((String.isNotBlank(preRegRecord.Pre_Registration_Group__c) && preRegRecord.Pre_Registration_Group__r.Status__c == 'Activate')){
                isAnyGroupActivae = true;
            }

            if((String.isNotBlank(preRegRecord.Early_Group1_Health__c) && preRegRecord.Early_Group1_Health__r.Status__c == 'Activate')){
                isAnyGroupActivae = true;
            } 

            if((String.isNotBlank(preRegRecord.Early_Group_2_Access_Group__c) && preRegRecord.Early_Group_2_Access_Group__r.Status__c == 'Activate')){
                isAnyGroupActivae = true;
            }

            if(!isAnyGroupActivae){
                result.put('preRegGroupNotActive', 'Thank you for Pre-registering for COVID-19 Vaccine. Your group is not Open for scheduling at this time, but you will be notified when your Group is opened for scheduling.');
            }
        }
    }

    public static void fetchPrivateAccessGroups(Map<String,Object> result, String vaccineTypes, String vaccineClass) {
        if( result.containsKey('canSchedule') && (Boolean)result.get('canSchedule') ){
            system.debug('vaccineClass---'+vaccineClass);
            Set<String> eligibleGroupIds = new Set<String>( ( (String)result.get('groupIds') ).Split(',') );
            Set<Id> eligiblePrivateAccess = new Set<Id>();
            //Step 1: Query all Access Assignment
            for(Private_Access_Assignment__c privateAccAss : [SELECT Id, Pre_Registration_Groups__c, Private_Access_Groups__c 
                                                                FROM Private_Access_Assignment__c 
                                                                WHERE Active__c = true AND 
                                                                Pre_Registration_Groups__c IN: eligibleGroupIds AND 
                                                                Private_Access_Groups__r.Is_Active__c = true WITH SECURITY_ENFORCED]){
                if( privateAccAss.Private_Access_Groups__c != null){
                    eligiblePrivateAccess.add( privateAccAss.Private_Access_Groups__c );
                }
            }

            //Step 2: Query Private Access Groups
            Set<String> statusLst = new Set<String>();
            statusLst.add(Label.VTS_Event_Status_Open);
            
            if( FeatureManagement.checkPermission('Can_Utilize_Close_Citizen_Portal_Events') ){
                statusLst.add('Closed Citizen Portal');
            }
    
            String paQuery = 'SELECT Id, Name,Is_Active__c, Vaccine_Type__c, Age_Based__c,(select id from VTS_Events__r WHERE Status__c in: statusLst Limit 1) '+
                            'FROM Private_Access__c WHERE Id in: eligiblePrivateAccess AND Age_Based__c != null AND Is_Active__c = true AND '+
                            ' Event_Process__c = \'Vaccination\'';
            
            if(String.isNotBlank(vaccineTypes)){
                List<String> vTypes = vaccineTypes.split(',');
                paQuery += ' AND Vaccine_Type__c in: vTypes ';
            }
            
            if(String.isNotBlank(vaccineClass)) {
                List<String> vClass = vaccineClass.split(',');
                paQuery += ' AND Vaccine_Class_Name__c IN: vClass ';
            }
            //Step 3: Build Private Access Ids
            String eventIds = '';
            for(Private_Access__c privateAcc : Database.query(paQuery+' WITH SECURITY_ENFORCED') ){
                if( !privateAcc.VTS_Events__r.isEmpty() && eligiblePrivateAccess.contains(privateAcc.Id) ){
                    eventIds += privateAcc.Id + ',';
                }
            }
            eventIds = eventIds.removeEnd(',');

            if( String.isNotBlank(eventIds) ){
                result.put('eventIds',eventIds);
            }
        }
    }

    public static List<Pre_Registration__c> loadExistingPreRegRecord(String conId){
        VTTS_AppointmentHelper.checkUserAccess(conId, null);
        if( oldPreRegRecord == null || oldPreRegRecord.isEmpty() ){
            oldPreRegRecord = [SELECT Id, Status__c, High_Risk_Health_Registration_Date__c/*,Are_you_eligible_for_Vaccine_Risk_Group__c*/,
                                Pre_Registration_Group__r.Name, Pre_Registration_Group__r.Status__c, Pre_Registration_Group__c,
                                Early_Group1_Health__r.Verification_Delay__c,Chronic_Condition__c,Affiliated_to_any_other_Risk_Group__c,
                                Pre_Registration_Group__r.Verification_Delay__c,Early_Group_2_Access_Group__c, Actual_Pass_Code__c, 
                                Early_Group1_Health__c, CreatedDate, Early_Group_2_Access_Group__r.Status__c, Pass_Code_Risk_Group__c, 
                                Early_Group1_Health__r.Status__c, BIPOC__c FROM Pre_Registration__c WHERE Contact__c =: conId WITH SECURITY_ENFORCED];
        }
        return oldPreRegRecord;
    }
}