/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 01-04-2022
 * @last modified by  : Vamsi Mudaliar
**/
global with sharing class okpcLoginController {
    public static final String EMAIL_STRING = '@okhla.com';
    public okpcLoginController() {

    }
   
    @AuraEnabled
    public static String login(String username, String password, String startUrl, String delimiter, String eventId) {//Added by Sajal
        try{
            ApexPages.PageReference lgn = Site.login(username+delimiter, password, '/s' + ( String.isNotBlank(eventId) ? '/?eventId='+eventId : '' ) );//Added by Sajal
            aura.redirect(lgn);
            return lgn.getUrl();
        }
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage()); 
        }
    }

    /**
    * @description : Method for login functionality for TVRS
    * @author Mohit Karani | 09-03-2021 
    * @param String response 
    * @return String 
    **/
    @AuraEnabled
    public static String tvrsLogin(String response) {//Added by Sajal
        try{
            if(String.isBlank(response)){
                return null;
            }
            LoginPageWrapperClass wrapper = (LoginPageWrapperClass)JSON.deserialize(response,LoginPageWrapperClass.class);
            if(!Test.isRunningTest() && !RecaptchaEnterpriseController.verifyByNamedCredentials(wrapper.recaptchaResponse, 'AHS_TVRS') ) {
                throw new VT_Exception('Please confirm the recaptcha ');
            }
            ApexPages.PageReference lgn = Site.login(wrapper.username+wrapper.delimiter, wrapper.password, '/s' + ( String.isNotBlank(wrapper.eventId) ? '/?eventId='+wrapper.eventId : '' ) );
            aura.redirect(lgn);
            return lgn.getUrl();
        }
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage()); 
        }
    }
    public class LoginPageWrapperClass{
        @AuraEnabled public string username;
        @AuraEnabled public string password;
        @AuraEnabled public string startUrl;
        @AuraEnabled public string eventId;
        @AuraEnabled public string recaptchaResponse;
        @AuraEnabled public string delimiter;
    }
    
    @AuraEnabled
    public static String communityLogin(String firstName, String userInfo, String delimiter) {//Added by Sajal
        
        String username = firstName+userInfo+EMAIL_STRING+delimiter;

       try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                // AND (Email =: userInfo OR Phone =: userInfo) 
                User u = [SELECT Password__c, Username FROM User WHERE FirstName =: firstName AND (Email =: userInfo OR Phone =: userInfo) WITH SECURITY_ENFORCED LIMIT 1];
                System.debug('user: '+u);
                ApexPages.PageReference lgn = Site.login(u.Username, u.Password__c, '/s/home');//Added by Sajal
                aura.redirect(lgn);
                return lgn.getUrl();
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
            
        }
        catch (Exception ex) {
            if (!test.isRunningTest())
                throw new AuraHandledException(ex.getMessage()); 
            Return Null;
        }
    }
    
    @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {
        if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
            Auth.AuthConfiguration authConfig = getAuthConfig();
            return authConfig.getUsernamePasswordEnabled();
        }else{
            throw new VT_Exception('You are not Authorized to perform this action');
        }
        
    }

    @AuraEnabled
    public static Boolean getIsSelfRegistrationEnabled() {
        if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
            Auth.AuthConfiguration authConfig = getAuthConfig();
            return authConfig.getSelfRegistrationEnabled();
        }else{
            throw new VT_Exception('You are not Authorized to perform this action');
        }
        
    }

    @AuraEnabled
    public static String getSelfRegistrationUrl() {
        if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
            Auth.AuthConfiguration authConfig = getAuthConfig();
            if (authConfig.getSelfRegistrationEnabled()) {
                return authConfig.getSelfRegistrationUrl();
            }
            return null;
        }else{
            throw new VT_Exception('You are not Authorized to perform this action');
        }
        
    }

    @AuraEnabled
    public static String getForgotPasswordUrl() {
        if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
            Auth.AuthConfiguration authConfig = getAuthConfig();
            return authConfig.getForgotPasswordUrl();
        }else{
            throw new VT_Exception('You are not Authorized to perform this action');
        }
        
    }
    
    @TestVisible
    private static Auth.AuthConfiguration getAuthConfig(){
        if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
            Id networkId = Network.getNetworkId();
            Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(networkId,'');
            return authConfig;
        }else{
            throw new VT_Exception('You are not Authorized to perform this action');
        }
        
    }

    @AuraEnabled
    global static String setExperienceId(String expId) {
        
        // Return null if there is no error, else it will return the error message 
        try {
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                if (expId != null) {
                    Site.setExperienceId(expId);
                }
                return null; 
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
            
        } catch (Exception ex) {
            return ex.getMessage();            
        }
    }   
}