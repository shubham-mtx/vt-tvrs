@isTest
public class OKTC_GenericPaginatorControllerTest {
     private static final String LabSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
    private static final String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
@TestSetup
    static void makeData(){
        Account Agency = TestDataFactoryAccount.createAccount('TestAccount', testingSite, true);
        Appointment__c Appointment = TestDataFactoryAppointment.createAppointment(true);
    }
   
    @isTest
    public static void testretrieveRecords() {
        Appointment__c appointment = [SELECT Id from Appointment__c ORDER BY ID ASC LIMIT 1];
        Map<String,Object> parameters = new Map<String,Object>();
        parameters.put('objectName', 'Appointment__c');
        parameters.put('fields', new List<Object>{'Name', 'Patient__r.LastName'});
        parameters.put('searchStr', '2');
        parameters.put('whereClause', '');
        parameters.put('previousPageNo', 0);
        parameters.put('recId', appointment.Id);
        parameters.put('recVal','FirstName');
        parameters.put('newPageNo',2);
        parameters.put('limit', 11);
        parameters.put('sortBy', 'Name');
        parameters.put('sortDir', 'ASC');
        parameters.put('maxPageNo',5);
        Map<String,Object> res =  OKTC_GenericPaginatorController.retrieveRecords(parameters);    
       
        System.assertEquals(true, res.size() > 0);
       
    }
	 @isTest
    public static void testretrieveRecords1() {
        Appointment__c appointment = [SELECT Id from Appointment__c ORDER BY ID ASC LIMIT 1];
        Map<String,Object> parameters = new Map<String,Object>();
        parameters.put('objectName', 'Appointment__c');
        parameters.put('fields', new List<Object>{'Name'});
        parameters.put('searchStr', '2');
        parameters.put('whereClause', 'Name != null');
        parameters.put('previousPageNo', 3);
        parameters.put('recId', appointment.Id);
        parameters.put('recVal','FirstName');
        parameters.put('newPageNo',2);
        parameters.put('limit', 11);
        parameters.put('sortBy', 'Name');
        parameters.put('sortDir', 'ASC');
        parameters.put('maxPageNo',5);
        Map<String,Object> res =  OKTC_GenericPaginatorController.retrieveRecords(parameters);    
       
        System.assertEquals(true, res.size() > 0);
       
    }

    @isTest
    public static void testretrieveRecords2() {
        Appointment__c appointment = [SELECT Id from Appointment__c ORDER BY ID ASC LIMIT 1];
        Map<String,Object> parameters = new Map<String,Object>();
        parameters.put('objectName', 'Appointment__c');
        parameters.put('fields', new List<Object>{'Name'});
        parameters.put('searchStr', '2');
        parameters.put('whereClause', 'Name != null');
        parameters.put('previousPageNo', 0);
        parameters.put('recId', appointment.Id);
        parameters.put('recVal','FirstName');
        parameters.put('newPageNo',2);
        parameters.put('limit', 11);
        parameters.put('sortBy', 'Name');
        parameters.put('sortDir', 'ASC');
        parameters.put('maxPageNo',2);
        Map<String,Object> res =  OKTC_GenericPaginatorController.retrieveRecords(parameters);    
       
        System.assertEquals(true, res.size() > 0);
       
    }
}