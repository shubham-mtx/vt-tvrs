/**
* @description       : 
* @author            : Saloni Adlakha
* @group             : 
* @last modified on  : 12-18-2020
* @last modified by  : Saloni Adlakha
* Modifications Log 
* Ver   Date         Author           Modification
* 1.0   12-18-2020   Saloni Adlakha   Initial Version
**/
@isTest
public class OK_SelfRegistrationTest {
    
    @TestSetup
    static void makeData() {
        
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String labRecordId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;
        
        //Account labAccount = TestDataFactoryAccount.createAccount('testLab', labRecordId, true);
        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0)
        );
        insert con;     
        VTS_Event__c event = new VTS_Event__c( Start_Date__c = Date.today(), End_Date__c = Date.today() + 7);
        insert event;
        
        
    }
    
    @isTest
    public static void selfRegisterContactTest() {
        VTS_Event__c event =  [SELECT Id from VTS_Event__c];
        Test.setMock(HttpCalloutMock.class, new RecaptchaVerifyCaptchaMockTest());
        Test.startTest();
        Map<String, Object> selfRegisterContactResult = OK_SelfRegistration.selfRegisterContact('testFirst', 
                                                                                                'portalTestUserv1', 
                                                                                                '9878878877', 
                                                                                                'testportalTestUserv1@gmail.com',
                                                                                                'Citizen COVID',event.Id , Date.today().addYears(-22),'1234445');
        /*Map<String, Object> selfRegisterContactResult1 = OK_SelfRegistration.selfRegisterContact('testFirst', 
                                                                                                 'portalTestUserv1', 
                                                                                                 '9878878877', 
                                                                                                 'kumarsatish@bmmc.com',
                                                                                                 System.Label.OKSF_Registration_Covid_Citizen,event.Id , Date.today().addYears(-20),'1234445');
*/        
Test.stopTest();
        // yog - string contactType=System.Label.OKSF_Registration_Covid_Citizen;
        string contactType='Citizen COVID';
        
        Contact[] con=[select id,FirstName,email,LastName,phone,AccountId from contact where LastName='portalTestUserv1' limit 1];
        string profileId=[select id,name from profile where name='Covid Citizen User' limit 1].id;
        Contact_Metadata_Setting__mdt[] settings = [SELECT MasterLabel, Account_Id__c, Contact_RT_Developer_Name__c,
                                                    Profile_Name__c,
                                                    Testing_Site_Id__c FROM Contact_Metadata_Setting__mdt WHERE MasterLabel =: contactType LIMIT 1
                                                   ];
        String newUsername=con[0].email+'.dev';
        user u=new user();
        u.Username = con[0].email; //Modified by Sajal
        u.put('Email', con[0].email);
        u.ProfileId = profileId;
        u.FirstName = con[0].FirstName;
        u.LastName = con[0].LastName;
        u.Password__c = 'test1233@N';
        u.Phone = con[0].phone;
        u.ContactId = con[0].Id;
        u.emailencodingkey = 'UTF-8';
        u.languagelocalekey = 'en_US';
        u.localesidkey = 'en_US';
        u.timezonesidkey = 'America/Los_Angeles';
        u.alias = con[0].FirstName.substring(0, 3);
        String nickname = ((newUsername != null && newUsername.length() > 0) ? newUsername.substring(0, 1) : '');
        nickname += String.valueOf(Crypto.getRandomInteger()).substring(1, 7);
        u.put('CommunityNickname', nickname);
        insert u;
        string AccountId=con[0].AccountId;   
        system.assertEquals(true, con.size() > 0, 'Contact Found');
    }
    
    public static testMethod void createContactFromTestingSiteTest() {
        Contact con= [ Select Id, FirstName, Email, LastName, Birthdate, Phone, AccountId, Registered_Event__c FROM Contact WHERE LastName='portalTestUserv1' limit 1 ];
        Test.startTest();
            Map<String, Object> response = OK_SelfRegistration.createContactFromTestingSite( con, 'Antibody_Testing_Site' );
        Test.stopTest();
        system.assertEquals(true, String.isNotBlank(con.Id));
    }
    
    
    public static testMethod void createContactWithAppointmentsTest() {
        Contact con1= [ Select Id, FirstName, Email, LastName, Birthdate, Phone, AccountId, Testing_Site__c FROM Contact WHERE LastName='portalTestUserv1' limit 1 ];
        Contact con = new Contact();
        con.FirstName='Testing';
        con.LastName = 'Site';
        con.Email = 'kunal.srivastava+test@mtxb2b.com';
        
        Test.startTest();
        
        Appointment_Slot__c slot = TestDataFactoryAppointmentSlot.createAppointmentSlot( true, con1.AccountId );
        
        Appointment__c app = TestDataFactoryAppointment.createAppointment( false );
        app.Patient__c = con1.Id;
        insert app;
        
        VTS_Event__c event = new VTS_Event__c( Start_Date__c = Date.today(), End_Date__c = Date.today() + 7);
        insert event;
        
        Map<String, Object> response = OK_SelfRegistration.createContactWithAppointments( con, 'Antibody_Testing_Site', slot.Id, event.Id);
        Map<String, Object> responsenew = OK_SelfRegistration.createContactWithAppointments( con, 'Antibody_Testing_Site', slot.Id, event.Id);
        Map<String, Object> response1 = OK_SelfRegistration.createContactWithAppointments( con1, 'Antibody_Testing_Site', slot.Id, event.Id);
            OK_SelfRegistration.ContactWrapper conWrap = new OK_SelfRegistration.ContactWrapper();
            conWrap.dob = System.today();
            conWrap.firstName = 'FNAME';
            conWrap.lastName = 'lname';
            conWrap.race = 'race';
        Test.stopTest();
        List<Appointment_Slot__c> slots = [SELECT Id FROM Appointment_Slot__c WHERE Account__c =:con1.AccountId];
        system.assertEquals(true, slots.size() == 0, 'Slots Created');
    }
    
    public static testMethod void createContactWithAppointmentsTest11() {
        
        Contact con1= [ Select Id, FirstName, Email, LastName, Birthdate, Phone, AccountId, Testing_Site__c FROM Contact WHERE LastName='portalTestUserv1' limit 1 ];
        Contact con = new Contact();
        con.FirstName='Testing';
        con.LastName = 'Site';
        con.Email = 'kunal.srivastava+test@mtxb2b.com';
        con.Unique_Reference__c = VT_ContactTriggerHandler.buildUniqueReference(con.email, con.FirstName, con.LastName );
        Test.startTest();
        
        Appointment_Slot__c slot = TestDataFactoryAppointmentSlot.createAppointmentSlot( true, con1.AccountId );
        
        Appointment__c app = TestDataFactoryAppointment.createAppointment( false );
        app.Patient__c = con1.Id;
        insert app;
        
        VTS_Event__c event = new VTS_Event__c( Start_Date__c = Date.today(), End_Date__c = Date.today() + 7);
        insert event;
        
        Map<String, Object> response = OK_SelfRegistration.createContactWithAppointments( con, 'Antibody_Testing_Site', slot.Id, event.Id);
        Map<String, Object> responsenew = OK_SelfRegistration.createContactWithAppointments( con, 'Antibody_Testing_Site', slot.Id, event.Id);
        Map<String, Object> response1 = OK_SelfRegistration.createContactWithAppointments( con1, 'Antibody_Testing_Site', slot.Id, event.Id);
        OK_SelfRegistration.ContactWrapper conWrap = new OK_SelfRegistration.ContactWrapper();
        conWrap.dob = System.today();
        conWrap.firstName = 'FNAME';
        conWrap.lastName = 'lname';
        conWrap.race = 'race';
        Test.stopTest();
        List<Appointment__c> apps = [SELECT Id FROM Appointment__c WHERE Patient__c =:con1.Id];
        system.assertEquals(true, apps.size() > 0, 'Appointment gets Created');
    }

    public static testMethod void createContactWithAppointmentsFromTestingTest() {
        
        Contact con1= [ Select Id, FirstName, Email, LastName, Birthdate, Phone, AccountId, Testing_Site__c FROM Contact WHERE LastName='portalTestUserv1' limit 1 ];
        Contact con = new Contact();
        con.FirstName='Testing';
        con.LastName = 'Site';
        con.Email = 'kunal.srivastava+test@mtxb2b.com';
        Test.startTest();
        
        Appointment_Slot__c slot = TestDataFactoryAppointmentSlot.createAppointmentSlot( true, con1.AccountId );
        
        Appointment__c app = TestDataFactoryAppointment.createAppointment( false );
        app.Patient__c = con1.Id;
        insert app;
        
        VTS_Event__c event = new VTS_Event__c( Start_Date__c = Date.today(), End_Date__c = Date.today() + 7);
        insert event;
        
        Map<String, Object> response = OK_SelfRegistration.createContactWithAppointmentsFromTesting( con, 'Antibody_Testing_Site', '', event.Id);
        Map<String, Object> response1 = OK_SelfRegistration.createContactWithAppointmentsFromTesting( con1, 'Antibody_Testing_Site', slot.Id, event.Id);
        OK_SelfRegistration.ContactWrapper conWrap = new OK_SelfRegistration.ContactWrapper();
        conWrap.dob = System.today();
        conWrap.firstName = 'FNAME';
        conWrap.lastName = 'lname';
        conWrap.race = 'race';
        Test.stopTest();
        system.assertEquals('success', String.valueOf(response.get('type')), 'Contact with Appointment gets Created');
    }
    
    public static testMethod void createContactWithAppointmentsFromTestingTest12() {
        
        Contact con1= [ Select Id, FirstName, Email, LastName, Birthdate, Phone, AccountId, Testing_Site__c FROM Contact WHERE LastName='portalTestUserv1' limit 1 ];
        Contact con = new Contact();
        con.FirstName='Testing';
        con.LastName = 'Site';
        con.Email = 'kunal.srivastava+test@mtxb2b.com';
        con.Unique_Reference__c = VT_ContactTriggerHandler.buildUniqueReference(con.email, con.FirstName, con.LastName );
        insert con;
        Test.startTest();
        
        Appointment_Slot__c slot = TestDataFactoryAppointmentSlot.createAppointmentSlot( true, con1.AccountId );
        
        Appointment__c app = TestDataFactoryAppointment.createAppointment( false );
        app.Patient__c = con1.Id;
        insert app;
        
        VTS_Event__c event = new VTS_Event__c( Start_Date__c = Date.today(), End_Date__c = Date.today() + 7);
        insert event;
        
        Map<String, Object> response = OK_SelfRegistration.createContactWithAppointmentsFromTesting( con, 'Antibody_Testing_Site', '', event.Id);
        Map<String, Object> response1 = OK_SelfRegistration.createContactWithAppointmentsFromTesting( con1, 'Antibody_Testing_Site', slot.Id, event.Id);
        
        System.debug('response>>>>>>>>>>>>>>'+response);
        OK_SelfRegistration.ContactWrapper conWrap = new OK_SelfRegistration.ContactWrapper();
        conWrap.dob = System.today();
        conWrap.firstName = 'FNAME';
        conWrap.lastName = 'lname';
        conWrap.race = 'race';
        Test.stopTest();
        system.assertEquals('fail', String.valueOf(response.get('type')), 'Contact with Appointment gets Created');
    }
    
    public static testMethod void createContactWithAppointmentsFromTestingTest102() {
        
        // Contact con1= [ Select Id, FirstName, Email, LastName, Birthdate, Phone, AccountId, Testing_Site__c FROM Contact WHERE LastName='portalTestUserv1' limit 1 ];
        Contact con = new Contact();
        con.FirstName='Testing';
        con.LastName = 'Site';
        con.Email = 'kunal.srivastava+test@mtxb2b.com';
        con.Unique_Reference__c = VT_ContactTriggerHandler.buildUniqueReference(con.email, con.FirstName, con.LastName );
        //insert con;
        
        Contact con1 = new Contact();
        con1.FirstName='Testing';
        con1.LastName = 'Site';
        con1.Email = 'kunal.srivastava+test@mtxb2b.com';
        con1.Unique_Reference__c = VT_ContactTriggerHandler.buildUniqueReference(con.email, con.FirstName, con.LastName );
        insert con1;
        Test.startTest();
        
        Appointment_Slot__c slot = TestDataFactoryAppointmentSlot.createAppointmentSlot( true, con1.AccountId );
        
        Appointment__c app = TestDataFactoryAppointment.createAppointment( false );
        app.Patient__c = con1.Id;
        insert app;
        
        VTS_Event__c event = new VTS_Event__c( Start_Date__c = Date.today(), End_Date__c = Date.today() + 7);
        insert event;
        
        Map<String, Object> response = OK_SelfRegistration.createContactWithAppointmentsFromTesting( con, 'Antibody_Testing_Site', '', event.Id);
        Map<String, Object> response1 = OK_SelfRegistration.createContactWithAppointmentsFromTesting( con1, 'Antibody_Testing_Site', slot.Id, event.Id);
        
        System.debug('response>>>>>>>>>>>>>>'+response);
        OK_SelfRegistration.ContactWrapper conWrap = new OK_SelfRegistration.ContactWrapper();
        conWrap.dob = System.today();
        conWrap.firstName = 'FNAME';
        conWrap.lastName = 'lname';
        conWrap.race = 'race';
        Test.stopTest();
        system.assertEquals('success', String.valueOf(response.get('type')), 'Contact with Appointment gets Created');
    }
    
}