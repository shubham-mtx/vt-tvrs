@isTest
public class VT_TVRS_PreRegGroupAutoAssignTest {
   
    @testSetup
    static void makeData(){

        Account accountRecord = new Account();
        accountRecord.Name = 'Test Vaccine';
        accountRecord.Event_Process__c = 'Vaccination';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Test_Center').getRecordTypeId();
        INSERT accountRecord;

        List<Contact> contList = new List<Contact>();
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'FName';
        contactRecord.LastName = 'LName';
        contactRecord.Birthdate = date.today()-10;
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord.Email = 'test@test01.com';
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Consent__c = true;
        contList.add(contactRecord);
        
        contactRecord = new Contact();
        contactRecord.FirstName = 'TFName';
        contactRecord.LastName = 'TLName';
        contactRecord.Birthdate = date.today()-10;
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord.Email = 'test@test02.com';
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Consent__c = true;
        contList.add(contactRecord);
        
        contactRecord = new Contact();
        contactRecord.FirstName = 'TestF';
        contactRecord.LastName = 'TestL';
        contactRecord.Birthdate = date.today()-10;
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord.Email = 'test@test03.com';
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Consent__c = true;
        contList.add(contactRecord);
        INSERT contList;

        Pre_Registration_Group__c preRegGrpRec = new Pre_Registration_Group__c();
        preRegGrpRec.Name = 'Test Pre-Registration Group';
        preRegGrpRec.Group_Type__c = 'Age-Based';
        preRegGrpRec.Status__c = 'Open';
        preRegGrpRec.Email_Acknowledgement__c = false;
        insert preRegGrpRec;

        Pre_Registration__c preRegRec = new Pre_Registration__c();
        preRegRec.Pre_Registration_Group__c = preRegGrpRec.Id;
        preRegRec.Contact__c = contactRecord.Id;
        preRegRec.Status__c = 'Assigned';
        insert preRegRec;

    }

    @isTest
    private static void executeBatchTest(){
        Test.startTest();
        Id batchId = Database.executeBatch(new VT_TVRS_PreRegGroupAutoAssign());
        System.assert( batchId != null );
        Test.stopTest();
    }
    @isTest
    private static void schedulableTest(){
        VT_TVRS_PreRegGroupAutoAssign preGroup = new VT_TVRS_PreRegGroupAutoAssign();
		String sch = '0 0 2 1/1 * ? *'; 
        system.schedule('Schedule Reminder', sch, preGroup);
    }
}