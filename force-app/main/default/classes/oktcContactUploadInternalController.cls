/**
 * @description       : 
 * @author            : Vamsi Mudaliar
 * @group             : 
 * @last modified on  : 01-13-2022
 * @last modified by  : Mohit Karani
**/
public without sharing class oktcContactUploadInternalController {
    @AuraEnabled
    public static constructorResultWrapper getEvents(Id accId,String eventProcess) {
        ///get the contact id eventProcess
        List < EventWrapper > listOfEventsToReturn = new List < EventWrapper > ();
        Map < String, String > headerToReadableMap = new Map < String, String > ();
        Map < String, String > reversedMap = new Map < String, String > ();
        //S-17667 Start
        Map < String, String > headerToReadableMapVacination = new Map < String, String > ();
        Map < String, String > reversedMapVacination = new Map < String, String > ();
        List<String> finalEventType = new List<String>{'Public','Private'};
        //S-17667 END
        if (accId != null) {

            if(eventProcess == 'Vaccination'){
                finalEventType = new List<String>{'Private'};
            }
 
            Date eligibleEventDate = System.today() - Integer.valueOf(System.Label.Bulk_Upload_Event_Past_Days);
            for (VTS_Event__c evt: [SELECT Id, Name, Description__c,Location__r.Event_Process__c, Start_Date__c, End_Date__c 
                    FROM VTS_Event__c 
                    WHERE Location__c =: accId 
                    AND Start_Date__c <= TODAY 
                    AND Start_Date__c >=: eligibleEventDate 
                    AND Event_Type__c IN: finalEventType 
                    AND (Status__c = 'Open' OR Status__c = 'Closed Citizen Portal')
                    WITH SECURITY_ENFORCED
                    ORDER BY Start_Date__c DESC]) {

                EventWrapper ew = new EventWrapper(evt.id, evt.Description__c, evt.Start_Date__c, evt.End_Date__c,evt.Location__r.Event_Process__c);
                listOfEventsToReturn.add(ew);
            }
        }
        List<String> columnsToExclude = new List<String>{'race','ethnicity','gender','preferredLanguage'};
        //Also send the Header
        for (Contact_Upload_Header__mdt headerData: [Select Masterlabel, DeveloperName, Header__c, Column_Sequence__c,Active__c FROM Contact_Upload_Header__mdt where Active__c = true WITH SECURITY_ENFORCED ORDER BY Column_Sequence__c ASC]) {
              //  if(!columnsToExclude.contains(headerData.MasterLabel)) {
                    headerToReadableMap.put(headerData.Header__c, headerData.MasterLabel);
                    reversedMap.put(headerData.MasterLabel, headerData.Header__c);
               // }
        }
          //Header for Vacination
        //S-17667 Start
        for (Contact_Vacination_Upload_Header__mdt headerData: [Select Masterlabel, DeveloperName,Column_Sequence__c, Header__c FROM Contact_Vacination_Upload_Header__mdt WITH SECURITY_ENFORCED ORDER BY Column_Sequence__c ASC]) {
             //   if(!columnsToExclude.contains(headerData.MasterLabel)) {
                    headerToReadableMapVacination.put(headerData.Header__c, headerData.MasterLabel);
                    reversedMapVacination.put(headerData.MasterLabel, headerData.Header__c);
              //  }
        }
            //S-17667 END

        return new ConstructorResultWrapper(listOfEventsToReturn, headerToReadableMap, reversedMap,headerToReadableMapVacination,reversedMapVacination);
    }

    @AuraEnabled
    public static InsertContactQueueable.ResultWrapper insertContacts(String strfromle, String eventId) {
        List < InsertContactQueueable.FieldWrapper > datalist = (List < InsertContactQueueable.FieldWrapper > ) JSON.deserialize(strfromle, List < InsertContactQueueable.FieldWrapper > .class);
        System.debug('datalist---'+datalist);
        System.enqueueJob(new InsertContactQueueable(datalist, eventId));

        InsertContactQueueable.ResultWrapper rw = new InsertContactQueueable.ResultWrapper();
        rw.fieldWrapper = null;
        rw.message = 'Contact Upload Results will be soon emailed to the registered email address';
        rw.type = 'success';
        return rw;
    }

    public class eventWrapper {
        @AuraEnabled public String eventId;
        @AuraEnabled public String eventName;
        @AuraEnabled public String eventProcess;
        
        public eventWrapper(String eventId, String eventName, Date startDate, Date endDate, String eventProcess) {
            this.eventId = eventId;
            this.eventProcess = eventProcess;
            this.eventName = eventName + '(' + String.valueOf(startDate)+ ')';
        }
    }

    public class constructorResultWrapper {
        @AuraEnabled public List < EventWrapper > eventWrapperList;
        @AuraEnabled public Map < String, String > headerToReadableStringMap;
        @AuraEnabled public Map < String, String > reverseMap;
        //S-17667 Start
        @AuraEnabled public Map < String, String > headerToReadableStringVacination;
        @AuraEnabled public Map < String, String > reversedMapVacination;
        //S-17667 END



        public constructorResultWrapper(List < EventWrapper > eventList, Map < String, String > headerMap, Map < String, String > reverseMap,Map < String, String > headerMapVacination, Map < String, String > reverseMapVacination) {
            this.eventWrapperList = eventList;
            this.headerToReadableStringMap = headerMap;
            this.reverseMap = reverseMap;
            //S-17667 Start 
            this.headerToReadableStringVacination = headerMapVacination;
            this.reversedMapVacination = reverseMapVacination;
            //S-17667 End
        }
    }
}