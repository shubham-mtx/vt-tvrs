public without sharing class oklcAntibodyDownloadAnduploadController {
    @AuraEnabled
    public static String getLabId(){
      
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                List<User> userList = [SELECT Name, FirstName, LastName, CompanyName, Email, Phone, smallPhotoUrl, ContactId, Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()];
                AccountContactRelation accountContactRecord = OKPCHeaderController.getTestingSiteFromContact(userList[0].ContactId);
                if(accountContactRecord != null){
                    return accountContactRecord.AccountId;
                }else{
                    return null;
                }
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }
        
 }