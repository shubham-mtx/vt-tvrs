import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    NavigationMixin
} from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import login from '@salesforce/apex/okpcLoginController.tvrsLogin';
import forgotPasswordURL from '@salesforce/label/c.OKCP_Forgot_Passsword';
import communityURL from '@salesforce/label/c.OKPC_Community_url';

export default class Okpc_okdohLoginPage extends NavigationMixin(LightningElement) {

    @track showError = false;
    @track errorMessage = '';
    @track username = '';
    @track password = '';

    customImage = nysdohResource;

    navigateToForgotPassword() {
        location.href = forgotPasswordURL;
    }
    navigateToHome(data) {
        location.href = data;
    }

    nameChange(event) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(String(event.target.value).toLowerCase())) {
            this.showError = false;
            this.errorMessage = undefined;
            this.username = event.target.value;
        } else {
            this.showError = true;
            this.errorMessage = 'Please enter a valid email';
            this.username = event.target.value;
            this.template.querySelector('input').focus();
        }
    }
    passwordChange(event) {
        this.password = event.target.value;
    }

    doLogin() {
        let eId = this.findGetParameter('eventId');

        if (this.username && this.password) {
            let token = this.getRecaptcha();
            if (!token) {
                this.showError = true;
                this.errorMessage = 'Please confirm the recaptcha';
                this.showSpinner = false;
                return;
            } else {
                this.showError = false;
                this.errorMessage = null;
                let dataObj = {
                    username: this.username,
                    password: this.password,
                    startUrl: communityURL + 's/',
                    delimiter: '.COVID',
                    eventId: eId,
                    recaptchaResponse: token
                }
                login({
                        response: JSON.stringify(dataObj)
                    }) //Added delimiter by Sajal
                    .then(data => {
                        this.navigateToHome(data);
                    })
                    .catch(error => {
                        document.dispatchEvent( new CustomEvent( 'grecaptchaEnterpriseReset') );
                        this.template.querySelector('input').focus();
                        this.showError = true;
                        var msg = JSON.stringify(error.body.message);
                        this.errorMessage = msg.substring(1, msg.length - 1);
                    });
            }

        } else {
            document.dispatchEvent( new CustomEvent( 'grecaptchaEnterpriseReset') );
            let inputs = this.template.querySelectorAll('input');
            let focusGiven = false;
            inputs.forEach(element => {
                if (!element.value.trim() && !focusGiven) {
                    element.focus();
                    focusGiven = true;
                }
            });
            this.showError = true;
            this.errorMessage = 'Please enter username and password';
        }
    }

    getRecaptcha() {
        const v2cmp = this.template.querySelector('c-recaptcha');
        let token;
        if (v2cmp) {
            token = v2cmp.fetchToken();
        }
        return token;
    }

    findGetParameter(parameterName) {
        var result = null,
            tmp = [];
        location.search
            .substr(1)
            .split("&")
            .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
            });
        return result;
    }

    get loginSelRegUrl() {
        let myurl = window.location.href;
        let newURL = new URL(myurl).searchParams;
        let eventId = newURL.get('eventId');
        if (eventId) {
            return '/events/s/selfregistration/?eventId=' + eventId;
        }
        return '/events/s/selfregistration/';
    }
}