({
    closeQA : function(component, event, helper) {
		  $A.get("e.force:closeQuickAction").fire();
    },
    handlePermissionError : function (event) {
        $A.get("e.force:closeQuickAction").fire();
    }
})