import {
    LightningElement,
    track, api,
    wire
} from 'lwc';
import getDemographic from "@salesforce/apex/OkpcContactInformationController.getDemographic";
import setDemographic from "@salesforce/apex/OkpcContactInformationController.setDemographic";
import {
    getPicklistValues
} from 'lightning/uiObjectInfoApi';

import getAllPicklistValueForDemographics from "@salesforce/apex/OKPCDashboardController.getAllPicklistValueForDemographics";

export default class OkcpDemographics extends LightningElement {
    @api isAppointment;
    @api appointmentContact = {};

    @track obj = {};
    @track showSpinner;
    @api appointmentId;
    @track picklistOption2;
    currentstep = 4;
    showPageHeader = true;
    @track isLanguageEqualsOther = false;
    @api dependentContactId = '';

    connectedCallback() {
        this.loadPickVal();

        if(this.isAppointment) {
            this.obj = JSON.parse(JSON.stringify(this.appointmentContact));

        } else {
            this.showSpinner = true;
            getDemographic({appointmentId : this.appointmentId})
            .then(result => {
                this.obj.Gender = result.Gender__c;
                this.obj.Primary_Language = result.Primary_Language__c;
                this.obj.Race = result.Race__c;
                this.obj.Ethnicity = result.Ethnicity__c;
                this.obj.User_Language = result.Other_Language__c;
                this.showSpinner = false;
            })
            .catch(error => {
                this.showSpinner = false;
            })
        }

        this.focusTitle();
    }
    get showIsLanguageEqualsOther() {
        return this.obj.Primary_Language === 'Other' ? true : false;
    }

    handleChange(event) {

        this.obj[event.target.name] = event.target.value;
       

        if (event.target.checked) {
            this.obj[event.target.name] = 'True';
        }
    }

    handleLanguageInputChange(event) {
        this.obj.User_Language = event.target.value;
    }

    navigateToBack() {
        
        const selectedEvent = new CustomEvent("backevent", {
            detail: null
        });
        this.dispatchEvent(selectedEvent);
    }

    sendEventToParent(obj) {
        const selectedEvent = new CustomEvent("save", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }
    navigateToCancel() {
        this.dispatchEvent(new CustomEvent('cancelappointment'));
    }

    saveData() {
        if(!this.showIsLanguageEqualsOther){
            this.obj.User_Language = '';
        }

        //Valdiations
        if (!this.obj.Gender || !this.obj.Primary_Language || !this.obj.Race || !this.obj.Ethnicity) {
            let tempObj = {};
            tempObj.message = "Please Fill all the required fields";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return;
        }
        if(this.isAppointment) {
            const selectedEvent = new CustomEvent("setobject", {
                detail: this.obj
            });
            this.dispatchEvent(selectedEvent);
        } else {
            this.showSpinner = true;
            if(this.dependentContactId == ''){
                setDemographic({
                    data: this.obj,
                    currentstep: this.currentstep,
                    contactid : null
                })
                .then(result => {
                    this.showSpinner = false;
                    let tempObj = {};
                    tempObj.message = "saved successfully";
                    tempObj.status = "success";
                    this.sendEventToParent(tempObj);
                })
                .catch(error => {
                    this.showSpinner = false;
                })
            }else{
                setDemographic({
                    data: this.obj,
                    currentstep: this.currentstep,
                    contactid : this.dependentContactId,
                    AppointmentId: this.appointmentId
                })
                .then(result => {
                    this.showSpinner = false;
                    let tempObj = {};
                    tempObj.message = "saved successfully";
                    tempObj.status = "success";
                    this.sendEventToParent(tempObj);
                })
                .catch(error => {
                    this.showSpinner = false;
                })
            }
        }
       
    }

    
    @track picklistOption1;

    
    @track picklistOption3;

    @track picklistOption4;

    loadPickVal(){
        this.picklistOption2 = undefined;

        getAllPicklistValueForDemographics()
        .then(result => {
            this.picklistOption1 = result.Gender__c;
            this.picklistOption3 = result.Race__c;
            this.picklistOption4 = result.Ethnicity__c;

            let tempValues = [{label:'English', value:'English'}];
            result.Primary_Language__c.forEach(element => {
                if(element.value !== 'Chinese' && element.value !== 'English' && element.value !== 'Other') {
                    tempValues.push(element);
                }
            });
            tempValues.push({label:'Other',value:'Other'});
            
            this.picklistOption2 = tempValues;
        })
        .catch(error => {
            console.error(error);
        });
    }

    focusTitle() {
        let focus = setInterval(() => {
            let cmp = this.template.querySelector('.pageTitle');
            if(cmp) {
                cmp.focus();
                setTimeout(() => {
                    this.showPageHeader = false;
                }, 500);
                clearInterval(focus);
            }
        }, 200);   
    }

}