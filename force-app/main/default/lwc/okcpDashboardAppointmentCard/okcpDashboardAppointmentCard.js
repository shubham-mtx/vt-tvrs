import { LightningElement, api, track } from 'lwc';
import VACCINATION_IMAGE from '@salesforce/resourceUrl/VTTS_Vaccination';
import Enable_Vaccination_Button from "@salesforce/label/c.Enable_Vaccination_Button";

export default class OkcpDashboardAppointmentCard extends LightningElement {
    @api appointment;
    @track Enable_Vaccination_Button_2nd_Dose = JSON.parse(Enable_Vaccination_Button).Enable_2nd_Dose == 1;
    @track Enable_Vaccination_Button_2nd_Dose_Cancel = JSON.parse(Enable_Vaccination_Button).Enable_2nd_Dose_Cancel == 1;
    displayDose1RescheduleButton = JSON.parse(Enable_Vaccination_Button).Enable_1st_Dose_Rescheduling == 1;
    displayDose2RescheduleButton = JSON.parse(Enable_Vaccination_Button).Enable_2nd_Dose_Rescheduling == 1;
    vaccinationImage = VACCINATION_IMAGE;
    get siteName() {
        return this.appointment.hasResults
            ? 'Test Result Available'
            : this.appointment.isToBeScheduled
                ? `Submission (${this.appointment.siteName})`
                : this.appointment.isScheduled
                    ? `Scheduled (${this.appointment.siteName})`
                    : this.appointment.isCancelled
                        ? `Cancelled (${this.appointment.siteName})`
                        : this.appointment.isCompleted
                            ? `Completed (${this.appointment.siteName})`
                            : '';
    }

    get iconName() {
        return this.appointment.isToBeScheduled
            ? 'utility:package'
            : 'utility:package_org';
    }

    get iconStyle() {
        return 'icon-container '
            + (this.appointment.isToBeScheduled
                ? 'icon-yellow'
                : 'icon-green');
    }

    get timeLabel() {
        //As per story S-23753 Sample Type is replaced with Completion Date/time
        //Updated by Rajat

        return this.appointment.hasResults
            ? 'Date & Time of Completion'
            : this.appointment.isToBeScheduled
                ? 'Preferred Appointment Time'
                : this.appointment.isScheduled || this.appointment.isCancelled
                    ? 'Appointment Date & Time'
                    : this.appointment.isCompleted
                        ? 'Date & Time of Completion'
                        : '';
    }

    get canViewCertificate() {
        return this.appointment.hasResults && 
                this.appointment.results.result != 'Rejected';
    }

    get showFirstDoseRescheduleButton() {
        if(this.appointment.vaccineType === 'Vaccination-1' && this.displayDose1RescheduleButton === true) {
            return true;
        } 
        return false;
    }

    get showSecondDoseRescheduleButton() {
        if(this.appointment.vaccineType === 'Vaccination-2' && this.displayDose2RescheduleButton === true) {
            return true;
        } 
        return false;
    }

    get ariaLabelCancel() {
        return this.appointment.isScheduled ? 'Cancel appointment for '+this.appointment.patientName : '';
    }

    get viewMapAriaLabel() {
        return this.appointment.isScheduled ? 'View Map for '+this.appointment.patientName : '';
    }

    get rescheduleAriaLabel() {
        return this.appointment.isCancelled ? 'Reschedule appointment for '+this.appointment.patientName : '';
    }

    get viewCertificateLabel() {
        return 'View Certificate for '+this.appointment.patientName;
    }

    get secondDoseAriaLabel() {
        return this.appointment.isShowSecondDoseButton ? 'Schedule Next Dose for '+this.appointment.patientName : '';
    }

    get showNextDoseButton() {
        // this.completedAppointmentDate + CorrectMinDays - 27(Lead time) == TODAY then display the button. 
        return this.appointment.nextDoseButtonVisible && this.appointment.isShowSecondDoseButton && this.appointment.nextDoseNumber;
    }

    get showRescheduleOnCancel(){
        return this.appointment.isCancelled && !this.appointment.hideReschedule;
    }

    handleClick(event) {
        let actionParams = {};
        switch (event.target.name) {
            case 'viewCertificate':
                actionParams = {
                    contactId: this.appointment.results.testingId
                };
                break;
            case 'reschedule':
                actionParams = {
                    appointmentId: this.appointment.id,
                    appointmentStatus : event.currentTarget.dataset.status
                };
                break;
            case 'viewOnMap':
                actionParams = {
                    appointmentId: this.appointment.id,
                    siteId: this.appointment.siteId,
                    title: this.appointment.isVaccination ? 'Your Vaccination Location' : 'Your Testing Location'
                };
                break;
            case 'editAppointment':
                actionParams = {
                    appointmentId: this.appointment.id
                };
                break;
            case 'cancelAppointment':
                actionParams = {
                    appointmentId: this.appointment.id
                };
                break;
            case 'schedule2ndDose':
                actionParams = {
                    appointmentId: this.appointment.id
                };
                break;
            case 'scheduleNextDose':
                actionParams = {
                    appointmentId: this.appointment.id
                };
                break;

            default:
                break;
        }

        this.dispatchEvent(new CustomEvent('action', {
            detail: {
                actionName: event.target.name,
                actionParams: actionParams
            }
        }));

    }
}