public class SendSMSBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    public final String Query;
    public final List<Id> recordIdList;
    public final String smsbody;
   // public final Id conRT_consultation_portal = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_Consultation_Vital_Records').getRecordTypeId();
  //  public final Id conRT_citizen_portal = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_Lobby_Vital_Records').getRecordTypeId();
    
    @InvocableMethod
    public static void executeBatchMethod(List<Id> recordIdList) {
        Database.executeBatch(new SendSMSBatch('SELECT Id,Account.communication__c, MobilePhone,Testing_Site__c, Community_Link__c FROM Contact ', recordIdList, 'remainder'), 1);
    }
    
    public SendSMSBatch(String query, List<Id> recordIdList, String body){
        this.recordIdList = recordIdList;
        this.Query=query + ' WHERE ID IN :recordIdList';
        this.smsbody = body;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        if(scope.size()>0){
            try{
                System.debug('scope: '+scope);
                String body = smsbody;
                Boolean sendSMS = false;
                List<Account> account = new List<Account>();
                System.debug('smsbody>>>'+smsbody);
                if (smsbody == 'TestingSiteLocation' && String.valueOf(scope[0].getSobject('Account').get('Communication__c')) =='OKU COVID') {
                    System.debug('Inside Test111 ');
                    List<Appointment__c> appointmentList = [SELECT id, Appointment_Date__c,Appointment_Slot__r.Start_Time__c, Patient__r.Community_Link__c  FROM Appointment__c
                                                            WHERE Patient__c = :String.valueOf(scope[0].get('Id')) AND Appointment_Slot__c != null];
                    system.debug('Inside batch for Test Loc'+appointmentList);
                    account = [SELECT Id,Name,BillingStreet,BillingCity,BillingState,BillingCountry,BillingPostalCode FROM Account WHERE Id = :String.valueOf(scope[0].get('Testing_Site__c')) LIMIT 1];
                    system.debug('account'+account);
                    if(appointmentList.size() > 0) {
                        if(appointmentList.size()>0){
                            body = 'Your COVID-19 Testing Site is located at '+(account[0].Name!=null?account[0].Name:'')+' '+ (account[0].BillingStreet!=null?account[0].BillingStreet:'')+' '+(account[0].BillingCity!=null?account[0].BillingCity:'')+' '+(account[0].BillingState!=null?account[0].BillingState:'')+' '+(account[0].BillingCountry!=null?account[0].BillingCountry:'');
                            sendSMS = true;
                        }
                    }
                }
                
                if(smsbody == 'Contact'){
                    if(String.valueOf(scope[0].getSobject('Account').get('Communication__c')) =='OSDH Antibody'){
                        body = 'You have been selected for COVID-19 Antibody Testing. Please click here '+ String.valueOf(scope[0].get('Community_Link__c'))+' to register and set your appointment time.\nYour patient id is '+scope[0].get('Patient_Id__c');
                        sendSMS = true;
                    }/* else if(String.valueOf(scope[0].getSobject('Account').get('Communication__c')) =='Vital Records'){
                        body = 'Great! Vital Records has confirmed your contact details.' + '\n' + 'Click this link '+ String.valueOf(scope[0].get('Community_Link__c')) + ' and let\'s make your appointment.';
                        sendSMS = true;
                    } */
                    
                }
                
                
                if(smsbody == 'appointment' || smsbody == 'appointmentfollowup'){
                    //body = 'Great! Vital Records has confirmed your contact details.' + '\n' + ' Click this '+ String.valueOf(scope[0].get('Community_Link__c')) + ' and let\'s make your appointment.';
                    List<Appointment__c> appointmentList = [SELECT id, Appointment_Date__c,Appointment_Slot__r.Start_Time__c, Patient__r.RecordTypeId, Patient__r.Community_Link__c, Testing_Site_Formatted_Address__c FROM Appointment__c 
                                                            WHERE Patient__c = :String.valueOf(scope[0].get('Id')) AND Appointment_Slot__c != null];
                    account = [SELECT Id,Name,BillingStreet,BillingCity,BillingState,BillingCountry,BillingPostalCode FROM Account WHERE Id = :String.valueOf(scope[0].get('Testing_Site__c')) LIMIT 1];
                    if(appointmentList.size() > 0) {
                        String minute = String.valueof(appointmentList[0].Appointment_Slot__r.Start_Time__c.minute());
                        String appTime = String.valueof(appointmentList[0].Appointment_Slot__r.Start_Time__c.hour())+':'+(minute == '0' ? '00' : minute);
                        String appDate = String.valueOf(appointmentList[0].Appointment_Date__c);
                        Id recTypeId = String.valueOf(appointmentList[0].Patient__r.RecordTypeId);
                        String accntName = account[0].Name!=null?account[0].Name:'';
                        String communityLink = String.valueOf(appointmentList[0].Patient__r.Community_Link__c);
                        String mapAddress =  'https://maps.google.com/?q=' + appointmentList[0].Testing_Site_Formatted_Address__c;
                        mapAddress = mapAddress.replaceAll(' ', '');
                        //mapAddress = EncodingUtil.urlEncode(mapAddress, 'UTF-8');
                        system.debug('mapAddress: '+ mapAddress);
                        System.debug('communition: '+scope[0].getSobject('Account').get('Communication__c'));
                        if(scope[0].getSobject('Account').get('Communication__c') =='OSDH Antibody'){
                            if(smsbody == 'appointment')
                                body = 'Your COVID-19 Antibody Resistance Testing is scheduled on '+appDate+' '+ appTime+'. Please type Y,N,R to confirm, deny or reschedule your appointment.';
                            else if(smsbody == 'appointmentfollowup')
                                body = 'Your COVID-19 Antibody Testing Site is located at '+(account[0].Name!=null?account[0].Name:'')+' '+ (account[0].BillingStreet!=null?account[0].BillingStreet:'')+' '+(account[0].BillingCity!=null?account[0].BillingCity:'')+' '+(account[0].BillingState!=null?account[0].BillingState:'')+' '+(account[0].BillingCountry!=null?account[0].BillingCountry:'');
                        }else if(String.valueOf(scope[0].getSobject('Account').get('Communication__c')) == 'OKU COVID'){
                            body = 'Your COVID-19 Testing Site is located at '+(account[0].Name!=null?account[0].Name:'')+' '+ (account[0].BillingStreet!=null?account[0].BillingStreet:'')+' '+(account[0].BillingCity!=null?account[0].BillingCity:'')+' '+(account[0].BillingState!=null?account[0].BillingState:'')+' '+(account[0].BillingCountry!=null?account[0].BillingCountry:'');
                        
                        }/*else if(scope[0].getSobject('Account').get('Communication__c') =='Vital Records'){
                            System.debug('conRT_citizen_portal: '+conRT_citizen_portal);
                            System.debug('conRT_consultation_portal: '+conRT_consultation_portal);
                            System.debug('recTypeId: '+recTypeId);

                            if(recTypeId == conRT_citizen_portal){
                                body = 'Your VR appt is scheduled in ' + accntName + ' on ' + appDate + ' ' + appTime + '. ' + '\nMap link: ' + mapAddress + '\n' + 'Please bring your application, photo ID and $15 per record.' + '\n' + 'You may ONLY bring 1 person with you.' + '\n'+ 'Click here to Cancel/Reschedule ' +communityLink;
                            }else if(recTypeId == conRT_consultation_portal){
                                body = 'Your VR appt is scheduled in ' + accntName + ' on ' + appDate + ' ' + appTime + '. ' + '\nMap link: ' + mapAddress + '\n' + 'Please bring your application, photo ID and $15 per record.' + '\n' + 'You may ONLY bring 1 person with you.' + '\n'+ 'Click here to Cancel/Reschedule ' +communityLink;
                            }
                        } */
                        sendSMS = true;
                    }
                    
                }
                
                if(smsBody == 'cancel') {
                    List<Appointment__c> appointmentList = [SELECT id, Appointment_Start_Date_v1__c,Appointment_Start_Time_v1__c, Patient__r.RecordTypeId, Patient__r.Community_Link__c, Testing_Site_Formatted_Address__c, Testing_Site__c  FROM Appointment__c 
                                                            WHERE Patient__c = :String.valueOf(scope[0].get('Id'))];
                    account = [SELECT Id,Name,BillingStreet,BillingCity,BillingState,BillingCountry,BillingPostalCode FROM Account WHERE Id = :String.valueOf(scope[0].get('Testing_Site__c')) LIMIT 1];
                    
                    if(appointmentList.size() > 0) { 
                        String minute = String.valueof(appointmentList[0].Appointment_Start_Time_v1__c.minute());
                        String appTime = String.valueof(appointmentList[0].Appointment_Start_Time_v1__c.hour())+':'+(minute == '0' ? '00' : minute);
                        String appDate = String.valueOf(appointmentList[0].Appointment_Start_Date_v1__c);
                        Id recTypeId = String.valueOf(appointmentList[0].Patient__r.RecordTypeId);
                        String accntName = account[0].Name!=null?account[0].Name:'';
                        String communityLink = String.valueOf(appointmentList[0].Patient__r.Community_Link__c);
                        String lobbyName = appointmentList[0].Testing_Site__c;
                        
                      /*  if(scope[0].getSobject('Account').get('Communication__c') =='Vital Records') {
                            if(recTypeId == conRT_citizen_portal || recTypeId == conRT_consultation_portal)
                                body = 'Your appointment with Vital Records has been canceled for ' + appDate + ' ' + appTime + ' at '+ lobbyName + '.\n' + 'Click here to Reschedule ' + communityLink;
                        } */
                        sendSMS = true;
                    }
                }
                
                if(smsBody == 'remainder') {
                    List<Appointment__c> appointmentList = [SELECT id, Appointment_Date__c,Appointment_Slot__r.Start_Time__c, Patient__r.RecordTypeId, Patient__r.Community_Link__c, Testing_Site_Formatted_Address__c, Testing_Site__c  FROM Appointment__c 
                                                            WHERE Patient__c = :String.valueOf(scope[0].get('Id')) AND Appointment_Slot__c != null];
                    account = [SELECT Id,Name,BillingStreet,BillingCity,BillingState,BillingCountry,BillingPostalCode FROM Account WHERE Id = :String.valueOf(scope[0].get('Testing_Site__c')) LIMIT 1];
                    
                    if(appointmentList.size() > 0) { 
                        String minute = String.valueof(appointmentList[0].Appointment_Slot__r.Start_Time__c.minute());
                        String appTime = String.valueof(appointmentList[0].Appointment_Slot__r.Start_Time__c.hour())+':'+(minute == '0' ? '00' : minute);
                        String appDate = String.valueOf(appointmentList[0].Appointment_Date__c);
                        Id recTypeId = String.valueOf(appointmentList[0].Patient__r.RecordTypeId);
                        String accntName = account[0].Name!=null?account[0].Name:'';
                        String communityLink = String.valueOf(appointmentList[0].Patient__r.Community_Link__c);
                        String lobbyName = appointmentList[0].Testing_Site__c;
                        String mapAddress =  'https://maps.google.com/?q=' + appointmentList[0].Testing_Site_Formatted_Address__c;
                        mapAddress = mapAddress.replaceAll(' ', '');
                        
                      /*  if(scope[0].getSobject('Account').get('Communication__c') =='Vital Records' && (recTypeId == conRT_citizen_portal || recTypeId == conRT_consultation_portal)) {
                            body = 'Your VR appt is scheduled in ' + accntName + ' on ' + appDate +' ' + appTime + '. ' + '\nMap link: ' + mapAddress + '\n' + 'Please bring your application, photo ID and $15 per record.' + '\n' + 'You may ONLY bring 1 person with you.' + '\n' + 'Click here to Cancel/Reschedule ' +communityLink;
                            sendSMS = true;
                        } */
                       
                    }
                }
                
                
                if(sendSMS){
                    System.debug('body final: '+body);
                    TwilioSendSMS.sendSms(String.valueOf(scope[0].get('MobilePhone')), body);
                }
                
            }catch(Exception e){
                System.debug('Exception::'+e.getMessage()+' '+e.getLineNumber());
            }
        }
    }
    
    public void finish(Database.BatchableContext BC){
    }
}