import { LightningElement, api, track } from 'lwc';
import updateAppointmentrecWithEvent from '@salesforce/apex/DC_ScheduleAppointment.updateAppointmentWithEvent';

import getAppointmentDetailrec from '@salesforce/apex/DC_ScheduleAppointment.getAppointmentDetail';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'

export default class Oktcscheduleappointment extends LightningElement {
    @api eventid;
    @api recordid;
    @api ifcovid = false;
	@track options = [];
	@track selectedOption;
	@track isAttributeRequired = false;
	@api fieldName;
	@track contactid;
	@api objectName;
    @track fieldLabelName;
    @track accountList = [];
    @track isSiteSelected = false;
    @track preferredTime;
    @track preferredDate;
    @track buttonLabelValue;
    @api selectvalue;
    @api hidePrefferedTime = false;
    @track slotId;
    @track showSchedule = false;

    @track showMapModal = false;
    disableButton;

	connectedCallback() {
        if(this.ifcovid){
            this.buttonLabelValue = 'Schedule & Submit';
        }else {
            this.buttonLabelValue = 'Schedule Appointment';
        }
        getAppointmentDetailrec({appointmentId: this.recordid})
        .then(data => {
            let appointment = data.appointment;
            this.contactid = appointment.Patient__c;
            this.preferredTime = this.msToTimeAMPM(appointment.Patient__r.Preferred_Time__c);
            this.preferredDate = appointment.Patient__r.Preferred_Date__c;

            if(!this.selectvalue) {
				this.selectvalue = appointment.Lab_Center__c;
			}
			//this.selectvalue = appointment.Lab_Center__c;
			if(!this.eventid) {
				this.eventid = appointment.Event__c;
			}
			//this.eventid = appointment.Event__c;
            if(appointment.Status__c === 'Draft' || appointment.Status__c === 'Cancelled' || appointment.Status__c === 'Scheduled' || appointment.Status__c === 'To Be Scheduled' || appointment.Status__c === 'Eligible' || appointment.Status__c === null || typeof appointment.Status__c === 'undefined' || appointment.Status__c === '') {
                // this.retrieveTestingSites();
                this.isSiteSelected = true;
            } else {
                this.dispatchEvent(new CustomEvent('close', {}));
                const event = new ShowToastEvent({
                    title: 'Error!',
                    variant: 'error',
                    message: 'You are not allowed to update this appointment.',
                });
                this.dispatchEvent(event);
            }
        })
        .catch(error => {
        });

        setTimeout(()=>{
            this.focusFirstEle();
        },200);
    }
    
    selectionChangeHandler(event) {
        this.isSiteSelected = true;
        this.selectvalue = event.target.value;
    }
    setId(event) {
        this.showSchedule = true;
        this.slotId = event.detail;
    }
    disableButton(event) {
        this.showSchedule = false;
        //this.slotId = event.detail;
    }
    createAppointmentRecord(event) {
        // event.target.disabled = true;
        this.disableButton = true;
        updateAppointmentrecWithEvent({ accountId : this.selectvalue, appointmentId : this.recordid, slotId : this.slotId, eventId : this.eventid}).then(result => {
            if(result.is2ndDoseAlreadyScheduled || result.is2ndDoseAlreadyCompleted || result.isAlreadyScheduled || result.isAlreadyCompleted) {
                if(result.is2ndDoseAlreadyScheduled) {
                    this.showToast('error', 'It looks like you already have an appointment to get your second dose of a COVID-19 vaccine. If you want to change your appointment, you need to cancel your existing appointment first.');
                } else if(result.is2ndDoseAlreadyCompleted) {
                    this.showToast('error', 'You have already received all required doses of your COVID-19 vaccine, so you do not need to sign up to get any additional doses. If you have questions, please visit the <a href="https://www.healthvermont.gov/covid-19/vaccine/about-covid-19-vaccines-vermont" target="_blank">Vermont Department of Health website</a> or call the COVID-19 Call Center at 802-863-7240.');
                } else if(result.isAlreadyScheduled){
                    this.showToast('error', 'It looks like you already have an appointment to get your first dose of a COVID-19 vaccine. If you want to change your appointment, you need to cancel your existing appointment first.');
                } else if(result.isAlreadyCompleted){
                    this.showToast('error', 'It looks like you have already had your first dose of a COVID Vaccine. If you would like to cancel any follow-up appointments, please use the CANCEL Button on your appointment record. However, in order to reschedule your 2nd dose appointment, you will need to contact the Vermont Department of Health COVID-19 Call Center at (802) 863-7240.');
                }
                // event.target.disabled = false;
                this.disableButton = false;
                return false;
            }

            if (result.type === 'error') {
                const event = new ShowToastEvent({
                    title: 'Error!',
                    variant: 'error',
                    message: result.message,
                });
                // event.target.disabled = false;
                this.disableButton = false;
                this.dispatchEvent(event);
            } else if (result.type === 'success') {
                const event = new ShowToastEvent({
                    title: 'Success!',
                    variant: 'success',
                    message: result.message,
                });
                this.dispatchEvent(event);
                // this.createCalendar(result);
                this.dispatchEvent(new CustomEvent('close', {
                    detail: 'save'
                }));
            }
            // event.target.disabled = false;
            this.disableButton = false;
        })
        .catch(error => {
            const event = new ShowToastEvent({
                title: 'Error!',
                variant: 'error',
                message: error.message,
            });
            this.dispatchEvent(event);
            // event.target.disabled = false;
            this.disableButton = false;
        });
    }

    showToast(type, msg) {
        this.dispatchEvent(new ShowToastEvent({
            message: msg,
            variant: type
        }));
    }

    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }
    justCloseModal(){
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    handleMapModal() {
        this.showMapModal = true;
    }

    handleMapModalClose(event) {
        this.showMapModal = false;
        
        if(event.detail !== 'close') {
            this.selectvalue = event.detail
        }
        
    }

    msToTimeAMPM(s) {
        if(s === '' || s === null || typeof s === 'undefined')
            return '';
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;
        let radian = 'AM';
        if(hrs > 12) {
            radian = 'PM';
            hrs = hrs - 12;
        }
        return this.pad(hrs) + ':' + this.pad(mins) + ' '  + radian;
    }

    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }
 
    focusFirstEle() {
        const focusableElements = 'lightning-combobox, lightning-button-icon, button, input, lightning-helptext, lightning-input, lightning-button';
        const modal = this.template.querySelector('.slds-modal');
        if(modal) {
            const firstFocusableElement = modal.querySelectorAll(focusableElements)[0];
            const focusableContent = modal.querySelectorAll(focusableElements);
            const lastFocusableElement = focusableContent[focusableContent.length - 1];
            firstFocusableElement.focus();
            this.template.addEventListener('keydown', function(event) {
            let isTabPressed = event.key === 'Tab' || event.keyCode === 9;
                if (!isTabPressed) {
                    return;
                }
                if (event.shiftKey) {               
                    if (this.activeElement === firstFocusableElement) {
                       lastFocusableElement.focus(); 
                        event.stopPropagation()
                        event.preventDefault();
                    }
                } else { 
                    if (this.activeElement === lastFocusableElement) {    
                        firstFocusableElement.focus(); 
                        event.preventDefault();
                        event.stopPropagation()
                    }
                }
            });
        }
    }
}