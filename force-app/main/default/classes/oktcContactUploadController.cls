/**
 * @description       : 
 * @author            : Vamsi Mudaliar
 * @group             : 
 * @last modified on  : 01-13-2022
 * @last modified by  : Mohit Karani
**/
public without sharing class oktcContactUploadController {
    @AuraEnabled
    public static List<Account> getAccountDetails(){
    
        Id conId, accountId;
        List<Account> acc = new List<Account>();
       
        try {
            List<User> userList = [SELECT Name, FirstName, LastName, CompanyName, Email, Phone, smallPhotoUrl, ContactId, Contact.AccountId 
                                   FROM User WHERE Id = :UserInfo.getuserid() WITH SECURITY_ENFORCED];
 
            AccountContactRelation accountContactRecord = OKPCHeaderController.getTestingSiteFromContact(userList[0].ContactId);
           
            if(userList.size()>0){
                conId = userList[0].contactId;
                
                if(conId != null) {
                    accountId = accountContactRecord.AccountId;
                }
            }  
            if(accountId != null) {
                for(Account evt : [SELECT Id, Event_Process__c FROM Account WHERE Id =: accountId WITH SECURITY_ENFORCED]) {
                    acc.add(evt);
                }
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }

        return acc;
        
    }
    
    @AuraEnabled
    public static constructorResultWrapper getEvents(Id accId,String eventProcess){
        ///get the contact id
        Id conId, accountId;
        List<String> finalEventType = new List<String>{'Public', 'Private'};
        Map < String, String > headerToReadableMapVacination = new Map < String, String > ();
        Map < String, String > reversedMapVacination = new Map < String, String > ();
        List<EventWrapper> listOfEventsToReturn = new List<EventWrapper>();
        Map<String, String> headerToReadableMap = new Map<String, String>();
        Map<String, String> reversedMap = new Map<String, String>();
        List<User> userList = [SELECT Name, FirstName, LastName, CompanyName, Email, Phone, smallPhotoUrl, ContactId, Contact.AccountId FROM User WHERE Id = :UserInfo.getuserid() WITH SECURITY_ENFORCED];
        AccountContactRelation accountContactRecord = OKPCHeaderController.getTestingSiteFromContact(userList[0].ContactId);
        if(userList.size()>0){
            conId = userList[0].contactId;
            if(conId != null) {
                accountId = accountContactRecord.AccountId;
            }
        }  
        
        if(accountId != null) {
            if(eventProcess == 'Vaccination'){
                finalEventType = new List<String>{'Private'};
            }
            Date eligibleEventDate = System.today() - Integer.valueOf(System.Label.Bulk_Upload_Event_Past_Days);
            for(VTS_Event__c evt : [SELECT Id, Description__c, Start_Date__c, End_Date__c FROM VTS_Event__c 
            WHERE Location__c =: accountId 
            AND Start_Date__c <= TODAY 
            AND Start_Date__c >=: eligibleEventDate 
            AND Event_Type__c IN: finalEventType  
            AND (Status__c = 'Open'  OR Status__c = 'Closed Citizen Portal') 
            WITH SECURITY_ENFORCED
            ORDER BY Start_Date__c DESC]) {
                EventWrapper ew = new EventWrapper(evt.id, evt.Description__c, evt.Start_Date__c, evt.End_Date__c);
                listOfEventsToReturn.add(ew);
            }
        }
        List<String> itemsToExclude = new List<String>{'race','ethnicity','gender','preferredLanguage'};
        //Also send the Header
        for(Contact_Upload_Header__mdt  headerData : Contact_Upload_Header__mdt.getAll().values()) {
            //if(headerData.Masterlabel!='race' && headerData.Masterlabel!='ethnicity'){ // added by vamsi - S23923
           // if(!itemsToExclude.contains(headerData.MasterLabel)) {
            if(headerData.Active__c) {
                headerToReadableMap.put(headerData.Header__c, headerData.MasterLabel);
                reversedMap.put(headerData.MasterLabel, headerData.Header__c);   
            }
           // }
        }
        
          //Header for Vacination
        //S-17667 Start
        for (Contact_Vacination_Upload_Header__mdt headerData: [Select Masterlabel, DeveloperName,Column_Sequence__c, Header__c FROM Contact_Vacination_Upload_Header__mdt WITH SECURITY_ENFORCED ORDER BY Column_Sequence__c ASC]) {
            //if(headerData.Masterlabel!='race' && headerData.Masterlabel!='ethnicity'){ // added by vamsi - S23923
               // if(!itemsToExclude.contains(headerData.MasterLabel)) {  
                    headerToReadableMapVacination.put(headerData.Header__c, headerData.MasterLabel);
                    reversedMapVacination.put(headerData.MasterLabel, headerData.Header__c);
               // }
        }
        return new ConstructorResultWrapper(listOfEventsToReturn, headerToReadableMap, reversedMap, headerToReadableMapVacination,reversedMapVacination);
    }
    @AuraEnabled
    public static InsertContactQueueable.ResultWrapper insertContacts(String strfromle, String eventId){
        List < InsertContactQueueable.FieldWrapper > datalist = (List < InsertContactQueueable.FieldWrapper > ) JSON.deserialize(strfromle, List < InsertContactQueueable.FieldWrapper > .class);

        System.enqueueJob(new InsertContactQueueable(datalist, eventId));

        InsertContactQueueable.ResultWrapper rw = new InsertContactQueueable.ResultWrapper();
        rw.fieldWrapper = null;
        rw.message = 'Contact Upload Results will be soon emailed to the registered email address';
        rw.type = 'success';
        return rw;
    }
    
    public class eventWrapper{
        @AuraEnabled
        public String eventId;
        @AuraEnabled
        public String eventName;
        public eventWrapper(String eventId, String eventName, Date startDate, Date endDate) {
            this.eventId = eventId;
            this.eventName = eventName +'('+String.valueOf(startDate)+ ')';
        }
    }

    public class constructorResultWrapper{
        @AuraEnabled
        public List<EventWrapper> eventWrapperList;
        @AuraEnabled
        public Map<String, String> headerToReadableStringMap;
        @AuraEnabled
        public Map<String, String> reverseMap;
         //S-17667 Start
         @AuraEnabled public Map < String, String > headerToReadableStringVacination;
         @AuraEnabled public Map < String, String > reversedMapVacination;
         //S-17667 END
        public constructorResultWrapper(List<EventWrapper> eventList, Map<String, String> headerMap, Map<String, String> reverseMap ,Map < String, String > headerMapVacination, Map < String, String > reverseMapVacination) {
            this.eventWrapperList = eventList;
            this.headerToReadableStringMap = headerMap;
            this.reverseMap = reverseMap;
             //S-17667 Start 
             this.headerToReadableStringVacination = headerMapVacination;
             this.reversedMapVacination = reverseMapVacination;
             //S-17667 End
        }
    }    
}