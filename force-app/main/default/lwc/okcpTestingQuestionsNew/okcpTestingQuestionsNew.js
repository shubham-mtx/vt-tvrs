import {
    LightningElement,
    track,
    api
} from 'lwc';
import getTesting from "@salesforce/apex/OkpcContactInformationController.getTesting";
import setTesting from "@salesforce/apex/OkpcContactInformationController.setTesting";
import getAllPicklistValueTestingQuestions from "@salesforce/apex/OKPCDashboardController.getAllPicklistValueTestingQuestions";
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';

export default class OkcpTestingQuestions extends LightningElement {

    @api isAppointment;
    @api appointmentContact = {};
    @api appointmentId;
    @api dependentContactId;
    @track obj = {};
    @track showSpinner;
    @track isFutureDate = false;
    @track errorMessage;
    @track showError = false;
    @track picklistOption1;
    @track picklistOption2;
    @track picklistOption3;
    @track picklistOption4;
    @track picklistOption6;
    dateOnset;
    currentstep = 5;

    connectedCallback() {
        this.loadPickVal();

        if (this.isAppointment) {
            this.obj = JSON.parse(JSON.stringify(this.appointmentContact));
        } else {
            this.showSpinner = true;
            getTesting({
                    appointmentId: this.appointmentId
                })
                .then(result => {
                    this.obj.First_COVID_19_Test = result.First_COVID_19_Test__c;
                    this.obj.Employed_in_Healthcare = result.Employed_in_Healthcare__c;
                    this.obj.Symptomatic_as_defined_by_CDC = result.Symptomatic_as_defined_by_CDC__c;
                    this.obj.Symptoms_Onset_Date = result.Symptoms_Onset_Date__c;
                    this.obj.Resident_in_Congregate_Care_Setting = result.Resident_in_Congregate_Care_Setting__c;
                    // this.obj.Are_you_pregnant = result.Are_you_pregnant__c;
                    this.obj.Reason_for_Testing = result.Reason_for_Testing__c;
                    this.obj.Other_Reason_for_Testing = result.Other_Reason_for_Testing__c;

                    this.showSpinner = false;

                })
                .catch(error => {
                    this.showSpinner = false;
                    showAsyncErrorMessage(this, error);
                });
        }
    }

    handleChange(event) {
        if (event.target.name === 'Symptoms_Onset_Date') {
            if (event.target.value) {
                this.dateOnset = event.target.value;
                this.obj[event.target.name] = event.target.value;
                this.isFutureDate = false;

                let today = (new Date()).getTime();
                let DOB = (new Date(this.dateOnset)).getTime();


                if (DOB > today) {
                    this.isFutureDate = true;
                }
            }
        } else {
            this.obj[event.target.name] = event.target.value;

        }
        if (event.target.checked) {
            this.obj[event.target.name] = 'True';
        }

    }

    doValidation() {
        if (this.isFutureDate) {
            this.errorMessage = 'Please provide a valid Symptoms Onset Date It cannot be a future date';
            this.showError = true;
            return false;
        }
        return true;
    }

    navigateToBack() {
        const selectedEvent = new CustomEvent("backevent", {
            detail: null
        });
        this.dispatchEvent(selectedEvent);
    }

    sendEventToParent(obj) {
        const selectedEvent = new CustomEvent("save", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }

    saveData() {
        if (!this.isSymptomatic) {
            this.obj.Symptoms_Onset_Date = null;
            this.isFutureDate = false;
        }
        if (this.doValidation()) {
            //Valdiations
            let isAllValid = [...this.template.querySelectorAll('lightning-input')].reduce((validSoFar, input) => {
                input.reportValidity();
                return validSoFar && input.checkValidity();
            }, true);
            isAllValid &= [...this.template.querySelectorAll("lightning-combobox")]
                .reduce((validSoFar, inputCmp) => {
                    inputCmp.reportValidity();
                    return validSoFar && inputCmp.checkValidity();
                }, true);

            if (!isAllValid) {
                return;
            }

            if (this.isAppointment) {
                const selectedEvent = new CustomEvent("setobject", {
                    detail: this.obj
                });
                this.dispatchEvent(selectedEvent);
            } else {
                this.showSpinner = true;

                setTesting({
                        data: this.obj,
                        currentstep: this.currentstep,
                        AppointmentId: this.appointmentId
                    })
                    .then(() => {
                        this.showSpinner = false;
                        let tempObj = {};
                        tempObj.message = "saved successfully";
                        tempObj.status = "success";
                        this.sendEventToParent(tempObj);
                    })
                    .catch(error => {
                        this.showSpinner = false;
                        showAsyncErrorMessage(this, error);
                    })
            }
        }

    }

    get isSymptomatic() {
        return this.obj.Symptomatic_as_defined_by_CDC === 'Yes';
    }

    get isOtherReason() {
        return this.obj.Reason_for_Testing === 'Other';
    }

    navigateToCancel() {
        this.dispatchEvent(new CustomEvent('cancelappointment'));
    }

    loadPickVal() {

        getAllPicklistValueTestingQuestions()
            .then(result => {
                this.picklistOption1 = result.First_COVID_19_Test__c;
                this.picklistOption2 = result.Employed_in_Healthcare__c;
                this.picklistOption3 = result.Symptomatic_as_defined_by_CDC__c;
                this.picklistOption4 = result.Resident_in_Congregate_Care_Setting__c;
                this.picklistOption6 = result.Reason_for_Testing__c;
            })
            .catch(error => {
                showAsyncErrorMessage(this, error);
            });
    }
}