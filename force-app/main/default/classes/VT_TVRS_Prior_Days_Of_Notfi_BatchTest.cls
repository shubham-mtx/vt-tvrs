/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 10-04-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
@isTest
public class VT_TVRS_Prior_Days_Of_Notfi_BatchTest {
  
    @testSetup
    static void makeData(){
    	
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Vaccine';
        accountRecord.Event_Process__c = 'Vaccination';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Test_Center').getRecordTypeId();
        INSERT accountRecord;

        List<Contact> contList = new List<Contact>();
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'FirstName';
        contactRecord.LastName = 'LastName';
        contactRecord.Birthdate = Date.newInstance(2000, 1, 1);
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord.Email = 'sample@sample.com';
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Consent__c = true;
        contList.add(contactRecord);
        
        contactRecord = new Contact();
        contactRecord.FirstName = 'FirstName1';
        contactRecord.LastName = 'LastName1';
        contactRecord.Birthdate = date.today()-20;
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord.Email = 'sample@samplegmail.com';
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Consent__c = true;
        contList.add(contactRecord);
        
        contactRecord = new Contact();
        contactRecord.FirstName = 'TestFirstName';
        contactRecord.LastName = 'TestLastName';
        contactRecord.Birthdate = Date.newInstance(1998, 11, 24);
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord.Email = 'sample@s.com';
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Consent__c = true;
        contList.add(contactRecord);
        INSERT contList;
        
        List<Private_Access__c> privateAccessList = new List<Private_Access__c>();
        Private_Access__c privateAccess = new Private_Access__c(
        	Name = 'private test',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'Yes',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        privateAccessList.add(privateAccess);
        
        privateAccess = new Private_Access__c(
        	Name = 'private test2',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-2',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'Yes',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        privateAccessList.add(privateAccess);
        insert privateAccessList;
        
        List<VTS_Event__c> eventList = new List<VTS_Event__c>();
        VTS_Event__c event = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccessList[0].Id,
            Start_Date__c=Date.today()+3

        );
        eventList.add(event);
        
        event = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccessList[1].Id,
            Start_Date__c=Date.today()+1
        );
        eventList.add(event);
        insert eventList;
        
        List<Appointment__c> appList = new List<Appointment__c>();
        Appointment__c appointment = new Appointment__c(
        	Patient__c = contList[0].Id,
            Status__c = 'Scheduled',
            Event__c = eventList[0].Id,
            Lab_Center__c = accountRecord.Id,
            Email_Workflow_Datetime__c=Datetime.newInstance(2021, 12, 12),
            Email_Flow__c='Prior Reminder'
          
        );
        appList.add(appointment);
        
        appointment = new Appointment__c(
        	Patient__c = contList[2].Id,
            Status__c = 'Scheduled',
            Event__c = eventList[0].Id,
            Lab_Center__c = accountRecord.Id,
            Email_Workflow_Datetime__c=Datetime.newInstance(2021, 2, 2),
            Email_Flow__c='Prior Reminder'
            
        );
        appList.add(appointment);
        
        appointment = new Appointment__c(
        	Patient__c = contList[1].Id,
            Status__c = 'Scheduled',
            Event__c = eventList[0].Id,
            Lab_Center__c = accountRecord.Id,
            Email_Workflow_Datetime__c=Datetime.newInstance(2022, 12, 12),
            Email_Flow__c='Prior Reminder'
         
        );
        appList.add(appointment);
        
        appointment = new Appointment__c(
        	Patient__c = contList[1].Id,
            Status__c = 'Scheduled',
            Event__c = eventList[1].Id,
            Lab_Center__c = accountRecord.Id,
            Email_Workflow_Datetime__c=Datetime.newInstance(2021, 1, 1),
            Email_Flow__c='Prior Reminder'
        
        );
        appList.add(appointment);
        insert appList;
    }
    
     @isTest
    private static void executeBatchTest(){
        Test.startTest();
        VT_TVRS_Prior_Days_Of_Notfication_Batch batch=new VT_TVRS_Prior_Days_Of_Notfication_Batch();
        Database.executeBatch(batch);
        Appointment__c app=[SELECT Id,Email_Flow__c FROM Appointment__c LIMIT 1];
        System.assertEquals(app.Email_Flow__c, 'Prior Reminder');
        Test.stopTest();
    }
    
    @isTest
    private static void schedulableTest(){
        VT_TVRS_Prior_Days_Of_Notfication_Batch sh1 = new VT_TVRS_Prior_Days_Of_Notfication_Batch();
		String sch = '0 0 2 1/1 * ? *'; 
        system.schedule('Schedule Reminder', sch, sh1);
    }

}