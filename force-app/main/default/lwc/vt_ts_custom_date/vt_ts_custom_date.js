import { LightningElement, track,api } from 'lwc';
import momentPath from "@salesforce/resourceUrl/momentJs";
import {loadScript} from "lightning/platformResourceLoader";


export default class Vt_ts_custom_date extends LightningElement {
    @track dob;
    @api disbaleField;
    @api isNotRequired;
    @track max_date;
    // @api dobVal;

    @api
    get maxDate() {
        return this.max_date;
    }
    set maxDate(value) {
        this.max_date = value;
    }

    @api
    setDobVal(val) {
        if( !window.moment ){
            loadScript(this, momentPath).then(() => {
                this.doSetupDOB(val);
            });
        }
        else{
            this.doSetupDOB(val);
        }
    }

    doSetupDOB(value){
        if( value ){ 
            this.dob = moment(value).format('MM-DD-YYYY');
            
            let _this = this;
            setTimeout(function(){ 
                _this.isValid();
            }, 100);
        }
        else{
            this.dob = undefined;

            let _this = this;
            setTimeout(function(){ 
                let dateInput = _this.template.querySelector('.customDOB');
                _this.cleanError(dateInput);
            }, 100);
        }
    }

    @api
    isValid(){
        if(!this.disbaleField) {
            let dateInput = this.template.querySelector('.customDOB');
            this.cleanError(dateInput);
            
            let dateString = dateInput.value;
            let dateformat = /^([0][1-9]|1[0-2])[-]([0][1-9]|[1-2][0-9]|3[0-1])[-]\d{4}$/;      
                  
            // Match the date format through regular expression      
            if(dateString.match(dateformat)){          
                let datepart = dateString.split('-');
                let month= parseInt(datepart[0]);      
                let day = parseInt(datepart[1]);      
                let year = parseInt(datepart[2]);      
                      
                // Create list of days of a month      
                let ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];   
                
                //Check for feb month
                if(month==2){      
                    //Check for leap year
                    if ( (!(year % 4) && year % 100) || !(year % 400)) {      
                        ListofDays[1] = 29;
                    }
                }
    
                if ( day > ListofDays[month-1] ){      
                    this.reportError(dateInput);
                    this.updateDateOnParent(undefined);
                    return false;      
                }
            }
            else{ 
                this.reportError(dateInput);
                this.updateDateOnParent(undefined);
                return false;      
            }   
            
            //If max date is defined then check for max date validation
            if( this.max_date ){
                if( moment(dateString) > moment(this.max_date) ){
                    dateInput.setCustomValidity('Date cannot be in future.');
                    dateInput.reportValidity();
                    this.updateDateOnParent(undefined);
                    return false;
                }
            }
    
            this.dob = dateString;
            this.updateDateOnParent(this.dob);
            return dateString;
        }
    }

    reportError(dateInput){
        dateInput.setCustomValidity('Invalid Date entered, please enter the date in MM-DD-YYYY format');
        dateInput.reportValidity();
    }

    cleanError(dateInput){
        if(dateInput) {
            dateInput.setCustomValidity('');
            dateInput.reportValidity();
        }
    }

    updateDateOnParent(dt){
        let setDOBValue;
        if( dt ){
            let spDt = dt.split('-');
            setDOBValue = new CustomEvent("updatedob", {
                detail: spDt[2] + '-' + spDt[0] + '-' + spDt[1]
            });
        }
        else{
            setDOBValue = new CustomEvent("updatedob", {
                detail: dt
            });
        }
        
        this.dispatchEvent(setDOBValue);
    }

    @api
    cleanErrorParent(){
        let dateInput = this.template.querySelector('.customDOB');
        dateInput.setCustomValidity('');
        dateInput.reportValidity();
    }

}