import {
    LightningElement,
    track,
    wire
} from 'lwc';
import {
    NavigationMixin
} from 'lightning/navigation';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import {
    getPicklistValues
} from 'lightning/uiObjectInfoApi';
import {
    getObjectInfo
} from 'lightning/uiObjectInfoApi';
import RACE_FIELD from '@salesforce/schema/Contact.Race__c';
import GENDER_FIELD from '@salesforce/schema/Contact.Gender__c';
import fetchPicklist from "@salesforce/apex/OkpcContactInformationController.fetchPicklist";
import getEvents from "@salesforce/apex/OKPC_LocationMapController.getEvents";
import createContactWithAppointments from '@salesforce/apex/OK_SelfRegistration.createContactWithAppointmentsFromTesting';
import getBusinessTimeOnAccount from '@salesforce/apex/OkpcContactInformationController.getBusinessTimeOnAccount';

import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';

export default class OkTestingCreateContact extends NavigationMixin(LightningElement) {

    @track zip = '';
    @track state = '';
    @track isFutureDOB = false;
    @track city = '';
    @track address1 = '';
    @track showSpinner = true;
    @track showError = false;
    @track showSuccess = false;
    @track errorMessage = '';
    @track thankYouMessage = '';
    @track firstName = '';
    @track lastName = '';
    @track email = '';
    @track phone = '';
    @track race = '';
    @track gender = '';
    @track emailOpt = true;
    @track smsOpt = false;
    @track accountId;
    @track portalName;
    @track showThankYou = false;
    @track accounts = [];
    @track delimiter = '.vrcpuser'
    @track portal = [{
        label: 'Citizen (COVID)',
        value: "COVID"
    }, {
        label: 'Citizen (Antibody)',
        value: 'Antibody'
    }];

    @track account;
    @track contactType = 'Citizen_COVID';
    @track raceOptions;
    @track genderOptions;
    @track todayDate;
    @track dob;
    @track showEmail = false;
    @track showAddress = false;
    @track showMobile = false;
    @track siteEvents = [];
    @track accountId = '';
    @track selectedEvent;
    @track isVaccination = false;
    enableEventSelection;

    @wire(getObjectInfo, {
        objectApiName: CONTACT_OBJECT
    })
    objectInfo;

    @wire(getPicklistValues, {
        recordTypeId: '$objectInfo.data.defaultRecordTypeId',
        fieldApiName: RACE_FIELD
    })
    setRacePicklistOptions({
        error,
        data
    }) {
        if (data) {
            // Apparently combobox doesn't like it if you dont supply any options at all.
            // Even though selectedOption was assigned in step 1, it wont "select" it unless it also has options
            this.raceOptions = data.values;
        } else if (error) {
            showAsyncErrorMessage(this, error);
        }
    }

    @wire(getPicklistValues, {
        recordTypeId: '$objectInfo.data.defaultRecordTypeId',
        fieldApiName: GENDER_FIELD
    })
    setGenderPicklistOptions({
        error,
        data
    }) {
        if (data) {
            // Apparently combobox doesn't like it if you dont supply any options at all.
            // Even though selectedOption was assigned in step 1, it wont "select" it unless it also has options
            this.genderOptions = data.values;
        } else if (error) {
            showAsyncErrorMessage(this, error);
        }
    }

    customImage = nysdohResource;


    handleChange(event) {
       // Added by Manthan Starts
        switch(event.target.name){
            case 'firstName':
                this.firstName = event.target.value;
            case 'lastname':
                this.lastName = event.target.value;
            case 'phone':
                this.phone = event.target.value;
            case 'email':
                this.email = event.target.value;
            case 'race':
                this.race = event.target.value;
            case 'gender':
                this.gender = event.target.value;
            case 'smsOtp':
                this.smsOpt = this.template.querySelector('[data-field="sms"]').checked;
                this.showMobile=this.smsOpt;
            case 'emaitOtp':
                this.emailOpt = this.template.querySelector('[data-field="email"]').checked;
            case 'dob':
                this.dob = event.target.value;
                let today = (new Date()).getTime();
                let DOB = (new Date(this.dob)).getTime();
                this.isFutureDOB = (DOB > today) ? true : false;
            case 'zip':
                this.zip = event.target.value;
            case 'state':
                this.state = event.target.value;
            case 'city':
                this.city = event.target.value;
            case 'address1':
                this.address1 = event.target.value;
            case 'selectedEvent':
                this.selectedEvent = event.target.value;
                this.enableEventSelection = (event.target.value)
        }
        //Added by Manthan Ends
    }

    accountChange(event) {
        this.accountId = event.target.value;
    }
    portalChange(event) {
        this.portalName = event.target.value;
    }
    get resultCopyOptions() {
        return [{
                label: "Electronic (Fastest way to get test results; 2-3 business days)",
                value: "E-Mail"
            },
            {
                label: "USPS Letter (5-7 business days)",
                value: "USPS Letter"
            }
        ];
    }

    handleradiochange(event) {
        this.testResultCopy = event.detail.value;
        if (this.testResultCopy == 'E-Mail') {
            this.showEmail = true;
            this.showAddress = false;
            this.showMobile = false;
        } else if (this.testResultCopy == 'USPS Letter') {
            this.showAddress = true;
            this.showMobile = false;
            this.showEmail = false;
        } else if (this.testResultCopy == 'Phone Call') {
            this.showMobile = true;
            this.showEmail = false;
            this.showAddress = false;
        }
    }

    connectedCallback() {


        fetchPicklist({
                objectName: "Contact",
                fieldName: "State__c"
            })
            .then((result) => {
                this.stateOptions = result;
            })
        getBusinessTimeOnAccount()
            .then(result => {
                this.account = result;
                this.accountId = result.Id;
                if (result.Event_Process__c === 'Vaccination') {
                    this.isVaccination = true;
                }
                this.getEventsData();
            })
            .catch(error => {
                showAsyncErrorMessage(this, error);
                this.showSpinner = false;
            });



    }
    getEventsData() {
        let dataObj = {};
        var date = new Date();
        dataObj.ctime = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        dataObj.accountId = this.accountId
        getEvents({
                responseWrapper: JSON.stringify(dataObj)
            })
            .then((result) => {
                let tmpEvents = [];
                result.forEach(evt => {
                    let tmpEvt = {};
                    let event = {
                        ...evt.event
                    }
                    tmpEvt.label = (event.Description__c ? event.Description__c : event.Name) + ' (' + event.Start_Date__c + ')';
                    tmpEvt.label = tmpEvt.label + (event.Private_Access__c ? ' - ' + event.Private_Access__r.Name : '');
                    tmpEvt.value = event.Id;
                    tmpEvents.push(tmpEvt);
                });
                this.siteEvents = tmpEvents.sort();
                this.showSpinner = false;
            })
            .catch(error => {
                showAsyncErrorMessage(this, error);
                this.showSpinner = false;
            });
    }

    doRegisteration() {
        //this.showSpinner=true;
        this.showError = false;
        this.showSuccess = false;
        this.showThankYou = false;
        if (!this.isValid()) {
            return;
        }
        if (this.firstName == '' || typeof this.firstName == 'undefined') {
            this.errorMessage = 'Please provide First Name.';
            this.showError = true;
            return;
        }
        if (this.lastName == '' || typeof this.lastName == 'undefined') {
            this.errorMessage = 'Please provide Last Name';
            this.showError = true;
            return;
        }
        if ((this.email == '' || typeof this.email == 'undefined') && this.showEmail) {
            this.errorMessage = 'Please provide Email';
            showAsyncErrorMessage(this, this.errorMessage);
            this.showError = true;
            return;
        }
        if (!this.validateEmail(this.email) && this.showEmail) {
            this.errorMessage = 'Please provide a valid email address. example: "abc@xyz.com"';
            this.showError = true;
            return;
        }
        if (this.dob == '' || typeof this.dob == 'undefined') {
            this.errorMessage = 'Please provide Birthdate';
            this.showError = true;
            return;
        }
        if (this.isFutureDOB) {
            this.errorMessage = 'Please provide a valid D.O.B. It cannot be a future date';
            this.showError = true;
            return;
        }
        if (this.selectedEvent == '' || typeof this.selectedEvent == 'undefined') {
            this.errorMessage = 'Please provide Event.';
            this.showError = true;
            return;
        }
        this.showSpinner = true;
        let cont = {
            'sobjectType': 'Contact'
        };
        cont.FirstName = this.firstName;
        cont.LastName = this.lastName;
        cont.MobilePhone = this.phone;
        if (this.showEmail) {
            cont.Email = this.email;
        }
        cont.Auto_Scheduled__c = true;
        cont.Opt_out_for_SMS_Updates__c = !(this.smsOpt);
        cont.HasOptedOutOfEmail = !(this.emailOpt);
        cont.Birthdate = this.dob;
        cont.Gender__c = this.gender;
        cont.Race__c = this.race;
        cont.Testing_Site__c = this.account.Id;
        cont.How_do_you_want_to_recieve_a_copy_of_you__c = this.testResultCopy;
        cont.ZIP__c = this.zip;
        cont.Street_Address1__c = this.address1;
        cont.City__c = this.city;
        cont.State__c = this.state;
        cont.MailingCity = this.city;
        cont.MailingState = this.state;
        cont.Registered_Event__c = this.selectedEvent;
        createContactWithAppointments({
                c: cont,
                recordType: this.contactType,
                slotId: '',
                eventId: this.selectedEvent
            })
            .then(result => {
                if (result.type != 'success') {
                    this.showError = true;
                    this.errorMessage = result.message;
                } else if (result.type == 'success') {
                    this.showThankYou = true;
                    this.thankYouMessage = result.message;
                }
                this.showSpinner = false;
            })
            .catch(error => {
                this.showError = true;
                showAsyncErrorMessage(this, error);
                this.showSpinner = false;
            });
    }

    isValid() {
        let valid = true;
        let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        let isAllValid2 = [
            ...this.template.querySelectorAll("lightning-radio-group")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        let isAllValid3 = [
            ...this.template.querySelectorAll("lightning-combobox")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        valid = isAllValid && isAllValid2 && isAllValid3;
        return valid;
    }

    validateEmail(email) {
        var regExpEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;
        var isValid = email.match(regExpEmail);
        return isValid;
    }

    enableEventSelectionHandler() {
        this.enableEventSelection = false; // makes disable false
    }

    disableEventSelectionHandler() {
        this.enableEventSelection = true; // makes disable true
    }
    

    handleConsentCancellation() {
        this.selectedEvent = undefined;
        this.enableEventSelection = false;
    }
}