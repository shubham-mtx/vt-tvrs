/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 12-30-2021
 * @last modified by  : Mohit Karani
**/
@IsTest
private class VT_PreRegAssignmentEngineTest {
    
    static String recordTypeIdGroup = Schema.SObjectType.Pre_Registration_Group__c.RecordTypeInfosByDeveloperName.get('Pre_Registration_Group').RecordTypeId;
    static String recordTypeIdPasscode = Schema.SObjectType.Pre_Registration_Group__c.RecordTypeInfosByDeveloperName.get('Pre_Registration_Passcode').RecordTypeId;
    
    @testSetup
    private static void makeData() {
        Contact con = new Contact();
        con.FirstName = 'Akshay';
        con.LastName = 'Kumar';
        con.Birthdate = Date.newInstance(1990, 7, 23);
        con.Phone = '8591763419';

        Pre_Registration_Group__c preGroup = new Pre_Registration_Group__c();
        preGroup.RecordTypeId = recordTypeIdGroup;
        preGroup.Beginning_DOB__c = Date.newInstance(1991, 1, 1);
        preGroup.Ending_DOB__c = Date.newInstance(2021, 1, 1);
        preGroup.Name = '0-30 Age Group';
        preGroup.Group_Type__c = 'Age-Based';
        preGroup.Status__c = 'Activate';
        preGroup.Email_Acknowledgement__c = true;
        insert preGroup;
        
        Pre_Registration_Group__c preGroup1 = new Pre_Registration_Group__c();
        preGroup1.RecordTypeId = recordTypeIdGroup;
        preGroup1.Beginning_DOB__c = Date.newInstance(1990, 7, 23);
        preGroup1.Ending_DOB__c = Date.newInstance(2021, 1, 1);
        preGroup1.Name = 'Chronic Condition 0-30 Age Group';
        preGroup1.Status__c = 'Activate';
        preGroup1.Group_Type__c = 'Chronic';
        preGroup1.Email_Acknowledgement__c = true;
        insert preGroup1;
        
        Pre_Registration_Group__c groupPasscode = new Pre_Registration_Group__c();
        groupPassCode.RecordTypeId = recordTypeIdPasscode;
        groupPasscode.Passcode__c = 'health1234';
        groupPasscode.Passcode_Type__c = 'Health';
        groupPasscode.Organization__c = 'Test Organization';
        groupPassCode.Pre_Registration_Group__c = preGroup.Id;
        groupPassCode.Status__c = 'Activate';
        groupPassCode.Group_Type__c = 'Chronic';
        insert groupPassCode;
        
        Pre_Registration_Group__c groupPasscode1 = new Pre_Registration_Group__c();
        groupPassCode1.RecordTypeId = recordTypeIdPasscode;
        groupPasscode1.Passcode__c = 'risk123';
        groupPasscode1.Passcode_Type__c = 'Risk Group';
        groupPasscode1.Organization__c = 'Test Organization';
        groupPassCode1.Pre_Registration_Group__c = preGroup.Id;
        groupPassCode1.Status__c = 'Activate';
        insert groupPassCode1;
    }
    
    @isTest
    private static void processAssignmentTest() {
        Contact con = new Contact();
        con.FirstName = 'Akshay';
        con.LastName = 'Kumar';
        con.Birthdate = Date.newInstance(1990, 7, 23);
        con.Phone = '8591763419';
        
        Pre_Registration_Group__c prGroupHealth = new Pre_Registration_Group__c();
        prGroupHealth.Status__c = 'Activate';
        prGroupHealth.Group_Type__c = 'Chronic';
        prGroupHealth.Email_Acknowledgement__c = true;
        prGroupHealth.Verification_Delay__c = 72;
        prGroupHealth.Beginning_DOB__c = Date.newInstance(1990, 7, 23);
        prGroupHealth.Ending_DOB__c =  Date.newInstance(1990, 7, 23);
        prGroupHealth.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        insert prGroupHealth;
        
        Pre_Registration_Group__c prGroupOtherRisk = new Pre_Registration_Group__c();
        prGroupOtherRisk.Status__c = 'Activate';
        prGroupOtherRisk.Group_Type__c = 'Risk Group';
        prGroupOtherRisk.Email_Acknowledgement__c = true;
        prGroupOtherRisk.Verification_Delay__c = 72;
        prGroupOtherRisk.Beginning_DOB__c = Date.newInstance(1990, 7, 23);
        prGroupOtherRisk.Ending_DOB__c =  Date.newInstance(1990, 7, 23);
        prGroupOtherRisk.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        insert prGroupOtherRisk;
        
        Pre_Registration_Group__c passCode1 = new Pre_Registration_Group__c();
        passCode1.Status__c = 'Activate';
        passCode1.Pre_Registration_Group__c = prGroupOtherRisk.Id;
        passCode1.Passcode__c = 'risk1234';
        passCode1.Passcode_Type__c = 'Risk Group';
        passCode1.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Passcode').getRecordTypeId();
        insert passCode1;
        
        
        Pre_Registration__c preReg = new Pre_Registration__c();
        // preReg.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preReg.Chronic_Condition__c = 'Yes';
        preReg.Affiliated_to_any_other_Risk_Group__c = 'Yes';
        preReg.Pass_Code_Risk_Group__c = 'risk1234';
        preReg.Pass_Code_Health__c = 'health1234';
 
        VT_PreRegAssignmentEngine vtPreAssignEngine = new VT_PreRegAssignmentEngine();
        //vtPreAssignEngine.isAnyGroupActivateApartFromChronic = false;
                
        VT_PreRegAssignmentEngine.processAssignment(new Map<Contact, Pre_Registration__c>{con => preReg});
        
        VT_PreRegAssignmentEngine.identifyGroupBasedOnDOB(Date.newInstance(1990, 7, 23));
        system.assertEquals('Assigned', preReg.Status__c, 'processAssignmentTest');
    }
    
    @isTest
    private static void processAssignmentPendingVerificationTest() {
        Contact con = new Contact();
        con.FirstName = 'Akshay';
        con.LastName = 'Kumar';
        con.Birthdate = Date.newInstance(2005, 7, 23);
        con.Phone = '8591763419';
        
        Pre_Registration__c preReg = new Pre_Registration__c();
        // preReg.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preReg.Chronic_Condition__c = 'Yes';
        
        VT_PreRegAssignmentEngine.processAssignment(new Map<Contact, Pre_Registration__c>{con => preReg});
        system.assertEquals('Assigned', preReg.Status__c, 'processAssignmentPendingVerificationTest');
    }
    
    @isTest
    private static void processAssignmentRiskGroupTest() {
        Contact con = new Contact();
        con.FirstName = 'Akshay';
        con.LastName = 'Kumar';
        con.Birthdate = Date.newInstance(1998, 7, 23);
        con.Phone = '8591763419';
        
        Pre_Registration__c preReg = new Pre_Registration__c();
        // preReg.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preReg.Chronic_Condition__c = 'No';
        preReg.Affiliated_to_any_other_Risk_Group__c = 'Yes';
        preReg.Pass_Code_Risk_Group__c = 'risk1234';
        
        VT_PreRegAssignmentEngine.processAssignment(new Map<Contact, Pre_Registration__c>{con => preReg});
        system.assertEquals('Assigned', preReg.Status__c, 'processAssignmentRiskGroupTest');
    }
    
    @isTest
    private static void passCodeValidationTestPositive() {
        Contact con = new Contact();
        con.FirstName = 'Akshay';
        con.LastName = 'Kumar';
        con.Birthdate = Date.newInstance(1998, 7, 23);
        con.Phone = '8591763419';
        
        Pre_Registration__c preReg = new Pre_Registration__c();
        // preReg.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preReg.Chronic_Condition__c = 'Yes';
        preReg.Pass_Code_Health__c = 'health1234';
        preReg.Pass_Code_Risk_Group__c = 'risk123';
        preReg.Actual_Pass_Code__c = 'actualyCode';
        //insert preReg;
        
        Pre_Registration__c preRegOld = new Pre_Registration__c();
        // preRegOld.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preRegOld.Chronic_Condition__c = 'Yes';
        preRegOld.Pass_Code_Health__c = 'health1234';
        preRegOld.Pass_Code_Risk_Group__c = 'risk123';
        preRegOld.Actual_Pass_Code__c = 'actualPassCode';
        //insert preRegOld;
        
        Map<String, String> data = VT_PreRegAssignmentEngine.passCodeValidation(preReg, preRegOld);
        system.assertEquals(false, data.isEmpty(), 'passCodeValidationTestPositive');
    }
    
    @isTest
    private static void passCodeValidationTestNegative() {
        Contact con = new Contact();
        con.FirstName = 'Akshay';
        con.LastName = 'Kumar';
        con.Birthdate = Date.newInstance(1998, 7, 23);
        con.Phone = '8591763419';
        
        Pre_Registration__c preReg = new Pre_Registration__c();
        // preReg.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preReg.Chronic_Condition__c = 'Yes';
        preReg.Pass_Code_Health__c = 'health12345';
        preReg.Pass_Code_Risk_Group__c = 'risk1234';
        preReg.Actual_Pass_Code__c = 'actualPassCode';
        insert preReg;
        
        Pre_Registration__c preReg1 = new Pre_Registration__c();
        // preReg1.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preReg1.Chronic_Condition__c = 'No';
        preReg1.Pass_Code_Health__c = 'health12345';
        preReg1.Pass_Code_Risk_Group__c = 'risk1234';
        preReg1.Actual_Pass_Code__c = 'actualPassCode';
        //insert preReg1;
        Map<String, String> data = VT_PreRegAssignmentEngine.passCodeValidation(preReg, preReg1);
        system.assertEquals(false, data.isEmpty(), 'passCodeValidationTestNegative');
    }
    
    @isTest
    private static void identifyGroupBasedOnDobTest() {
        List<Pre_Registration_Group__c> data = VT_PreRegAssignmentEngine.identifyGroupBasedOnDOB(Date.newInstance(2000, 1, 1));
        system.assertEquals(false, data.isEmpty(), 'identifyGroupBasedOnDobTest');
    }
}