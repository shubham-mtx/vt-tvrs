/**
 * @description       : 
 * @author            : Balram Dhawan
 * @group             : 
 * @last modified on  : 01-21-2022
 * @last modified by  : Vamsi Mudaliar
**/
public without sharing class TVRS_CitizenPortalVaccinationController {

    /**
    * @description Checks requiredFields for a contact before scheduling an appointment
    * @author Balram Dhawan | 01-03-2022 
    * @param Map<String Object> request   
    * following are the keys should be present in request
    * patientId
    * @return Map<String, Object> 
    **/
    @AuraEnabled
    public static Map<String, Object> checkUserEligibitlity(Map<String, Object> request){
        try {
            Map<String, Object> response = new Map<String, Object>{'request' => request};
            response.put('needRescheduleWindow', false);
            String patientId = String.valueOf(request.get('patientId'));
            if(String.isBlank(patientId)) {
                throw new VT_Exception('Please provide a valid Patient Id');
            }
            response.putAll(checkUserEligibitlityHelper(patientId));
            return response;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    /**
    * @description check required fields of contact and return pending & scheduled Dose
    * @author Balram Dhawan | 01-04-2022 
    * @param String patientId 
    * @return Map<String, Object> 
    **/
    private static Map<String, Object> checkUserEligibitlityHelper(String patientId) {
        String missingRequiredFields = '';
        Map<String, Object> response = new Map<String, Object>();
        String requiredFields = VT_TS_Contact_Required_Field__mdt.getAll().values()?.get(0)?.Field_API_Name__c;
        String query = ' SELECT Id, ';
        query +=  (String.isNotBlank(requiredFields)) ? requiredFields+',' : '';
        query += ' (SELECT Id, Status__c, Event__r.Private_Access__r.Vaccine_Type__c, Event_Process__c, Event__r.Private_Access__r.Vaccine_Class_Name__c, ';
        query += ' Appointment_Date__c, Appointment_Start_Time__c, Appointment_Start_Time_v1__c, Event__r.Private_Access__c, Patient__c, ';
        query += ' Appointment_Complete_Date__c, Event__r.Private_Access__r.Requires_Followup_Dose__c, Event__r.Private_Access__r.Weak_Immune_Min_Days__c, ';
        query += ' Event__r.Private_Access__r.Min__c, Event__r.Private_Access__r.Max__c, Event__r.Private_Access__r.Weak_Immune_Max_Days__c ';
        query += ' FROM Appointments2__r ';
        query += ' WHERE ';
        query += ' Event__r.Private_Access__r.Vaccine_Type__c != null ';
        query += ' AND ';
        query += ' Event_Process__c = \'Vaccination\' ';
        query += ' AND ';
        query += ' Status__c IN (\'Scheduled\', \'Completed\') ';
        query += ' ORDER BY Event__r.Private_Access__r.Vaccine_Type__c ASC), ';
        query += ' (SELECT Id, Is_Immune_Weak__c,BIPOC__c,Contact__c FROM Pre_Registrations__r) ';
        query += ' FROM Contact WHERE Id=:patientId WITH SECURITY_ENFORCED ';
        Contact con = Database.query(query);
        response.put('preRegRecord', con.Pre_Registrations__r);
        TVRS_Utility.requiredFieldsCheck(con, requiredFields);
        response.putAll(getPendingDosesByPatient(con));
        return response;
    }

    /**
    * @description return pending Doses by looping through Patient's Appointment
    * @author Balram Dhawan | 01-04-2022 
    * @param Contact con // should have contact's appointment ORDER BY Status__c DESC
    * @return Map<String, Object> 
    **/
    private static Map<String, Object> getPendingDosesByPatient(Contact con) {
        Map<String, Object> response = new Map<String, Object>();
        Map<String, String> doseTypes = getVaccineTypes();
        Set<String> completedDoses = new Set<String>();
        for (Appointment__c appointment : con.Appointments2__r) {
            if(appointment.Status__c == VT_TS_Constants.SCHEDULED_TEXT) {
                response.put('needRescheduleWindow', true);
                response.put('appointmentToReschedule', appointment);
                // break;
            } else if(appointment.Status__c == VT_TS_Constants.COMPLETED_TEXT) {
                doseTypes.remove(appointment.Event__r.Private_Access__r.Vaccine_Type__c);
                completedDoses.add(appointment.Event__r.Private_Access__r.Vaccine_Type__c);
                response.put('appointment', appointment);   // latestCompletedAppiontment
            } 
        }

        if(!response.containsKey('needRescheduleWindow') && response.containsKey('appointment')) {
            Appointment__c latestAppointment = (Appointment__c) response.get('appointment');
            if(latestAppointment.Event__r.Private_Access__r.Requires_Followup_Dose__c == 'No') {
                throw new VT_Exception('It looks like you have completed all required/available vaccine doses');
            }
        }

        response.put('pendingDoses', doseTypes);
        response.put('completedDoses', completedDoses);
        return response;
    }

    /**
    * @description returns how many number of doses we have
    * @author Balram Dhawan | 12-21-2021 
    * @return Object 
    **/
    private static Map<String, String> getVaccineTypes() {
        Map<String, String> doseTypes = new Map<String, String>();
        List<Schema.PicklistEntry> entries = Schema.Private_Access__c.Vaccine_Type__c.getDescribe().getPicklistValues();
        for (Schema.PicklistEntry pe : entries) {
            doseTypes.put(pe.getValue(), getDoseNumberFromVaccineType().get(pe.getValue()));
        }
        return doseTypes;
    }

    /**
    * @description get dose Number based on VaccineType
    * @author Balram Dhawan | 12-21-2021 
    * @return Object 
    **/
    private static Map<String, String> getDoseNumberFromVaccineType() {
        return new Map<String, String>{
            'Vaccination-1'     =>  'First Dose',
            'Vaccination-2'     =>  'Second Dose',
            'Vaccination-3'     =>  'Third Dose',
            'Vaccination-4'     =>  'Fourth Dose',
            'Vaccination-5'     =>  'Fifth Dose'
        };
    }
    
    /**
    * @description returns greaterVaccines
    * @author Balram Dhawan | 01-13-2022 
    * @param String vaccineType 
    * @return List<String> 
    **/
    private static List<String> getGreaterVaccines(String vaccineType) {
        switch on vaccineType {
            when 'Vaccination-1' {
                return new List<String>{'Vaccination-2', 'Vaccination-3', 'Vaccination-4', 'Vaccination-5'};
            }
            when 'Vaccination-2' {
                return new List<String>{'Vaccination-3', 'Vaccination-4', 'Vaccination-5'};
            }
            when 'Vaccination-3' {
                return new List<String>{'Vaccination-4', 'Vaccination-5'};
            }
            when 'Vaccination-4' {
                return new List<String>{'Vaccination-5'};
            }
            when else {
                return new List<String>();
            }
        }
    }

    /**
    * @description saves appointment
    * @author Balram Dhawan | 12-29-2021 
    * @param Map<String Object> inputData 
    * sample payload {"patientId":"003r000000in3X9AAI","userRequestsForVaccineType":"Vaccination-3","vaccineClass":"Pfizer","vaccineDate":"2022-01-10","vaccineType":"Vaccination-1", "minimumNextDoseDate": "2021-12-12"}
    * @return Map<String, Object> 
    **/
    @AuraEnabled
    public static Map<String, Object> saveAppointment(Map<String, Object> inputData){
         try {
            Map<String, Object> response = new Map<String, Object>{'request' => inputData};
            List<Appointment__c> appointmentsToInsert = new List<Appointment__c>();
            List<Appointment__c> appointmentsToCancel = new List<Appointment__c>();
            String patientId = (String) inputData.get('patientId');
            String vaccineType = String.valueOf(inputData.get('vaccineType'));
            String vaccineClass = String.valueOf(inputData.get('vaccineClass'));
            Date vaccineDate = (inputData.containsKey('vaccineDate') ? Date.valueOf(String.valueOf(inputData.get('vaccineDate'))) : null);

            if(String.isBlank(patientId)) {
                throw new VT_Exception('Please provide a valid Patient Id');
            }

            if(String.isBlank(vaccineClass) || vaccineDate == null || String.isBlank(vaccineType)) {
                throw new VT_Exception('Vaccine Type, class and date is required');
            }

            if(vaccineDate > System.today()) {
                throw new VT_Exception('Vaccine Date can\'t be a future date');
            }

            List<VTS_Event__c> events = [SELECT Id, Private_Access__r.Vaccine_Class_Name__c, Location__c 
                                        FROM VTS_Event__c
                                        WHERE 
                                            Start_Date__c <= :vaccineDate 
                                                AND
                                            End_Date__c >= :vaccineDate
                                                AND
                                            Private_Access__c != null
                                                AND
                                            Private_Access__r.Is_Active__c = true
                                                AND
                                            Private_Access__r.Vaccine_Type__c =:vaccineType
                                                AND
                                            Private_Access__r.Vaccine_Class_Name__c =:vaccineClass
                                                AND
                                            Status__c = 'Self Attest'
                                        WITH SECURITY_ENFORCED];
            if(events.isEmpty()) {
                throw new VT_Exception('Event Not Found');
            }

            cancelExistingScheduledAppointment(vaccineType, vaccineDate, patientId);

            Appointment__c app = new Appointment__c();
            app.Status__c = VT_TS_Constants.COMPLETED_TEXT;
            app.Lab_Center__c = events[0].Location__c;
            app.Appointment_Slot__c = getSlotId(events[0].Location__c, vaccineDate);
            app.Appointment_Complete_Date__c = vaccineDate;
            app.Event__c = events[0].Id;
            app.VaccineQuestion_DoseNum__c = getDoseNumberFromVaccineType().get(vaccineType);
            app.Patient__c = patientId;
            app.Other_Reason_for_Testing__c = null;
            insert VT_SecurityLibrary.getAccessibleData('Appointment__c', new List<Appointment__c>{app}, 'insert');
            response.put('createdAppointment', app);
            response.putAll(checkExistingDoses(inputData));
            return response;
         } catch (Exception e) {
             throw new AuraHandledException(e.getMessage());
        }
    }

    /**
    * @description cancelExistingScheduledAppointment of same vaccineType and also added check for the vaccineDate that shouldn't be greater than last vaccination date
    * @author Balram Dhawan | 01-10-2022 
    * @param String vaccineType 
    * @param Date vaccineDate 
    * @param String patientId 
    **/
    private static void cancelExistingScheduledAppointment(String vaccineType, Date vaccineDate, String patientId) {
        String previousVaccineType = TVRS_Utility.getPreviousVaccineType(vaccineType);
        Appointment__c latestCompletedAppointment = new Appointment__c();
        List<Appointment__c> appointmentsToCancel = new List<Appointment__c>();

        Contact contactRecord = TVRS_Utility.getPatientDataWithVaccinations(patientId);
        Pre_Registration__c preRegRecord = (contactRecord.Pre_Registrations__r.isEmpty()) ? null : contactRecord.Pre_Registrations__r[0];

        for (Appointment__c app : contactRecord.Appointments2__r) {
            if(app.Event__r.Private_Access__r.Vaccine_Type__c == vaccineType && app.Status__c == VT_TS_Constants.SCHEDULED_TEXT) {
                app.Status__c = VT_TS_Constants.CANCELLED_TEXT;
                appointmentsToCancel.add(app);
            } else if(  app.Status__c == VT_TS_Constants.COMPLETED_TEXT &&
                        app.Event__r.Private_Access__r.Vaccine_Type__c == previousVaccineType && 
                        vaccineDate < Date.valueOf(TVRS_Utility.getMinimumNextDoseDate(preRegRecord, app).get('minimumNextDoseDate'))) {
                    throw new VT_Exception('Date provided is invalid based on previous dose Information');
                // throw new VT_Exception(getDoseNumberFromVaccineType().get(vaccineType)+' date can\'t be less than '+getDoseNumberFromVaccineType().get(app.Event__r.Private_Access__r.Vaccine_Type__c)+' date ');
            } else if(  app.Status__c == VT_TS_Constants.COMPLETED_TEXT &&
                        getGreaterVaccines(vaccineType).contains(app.Event__r.Private_Access__r.Vaccine_Type__c) && 
                        vaccineDate > app.Appointment_Complete_Date__c ) {
                throw new VT_Exception('Date provided is invalid based on previous dose Information');
                // throw new VT_Exception(getDoseNumberFromVaccineType().get(vaccineType)+' date can\'t be greater than '+getDoseNumberFromVaccineType().get(app.Event__r.Private_Access__r.Vaccine_Type__c)+' date ');
            }
        }
        if(!appointmentsToCancel.isEmpty()) {
            update VT_SecurityLibrary.getAccessibleData('Appointment__c', appointmentsToCancel, 'update');
        }
    }
    
    /**
    * @description create slot and returns its id
    * @author Balram Dhawan | 01-09-2022 
    * @param String locationId 
    * @param Date vaccineDate 
    * @return Id 
    **/
    private static Id getSlotId(String locationId, Date vaccineDate) {
        Datetime currentTime = System.now();
        Appointment_Slot__c slot = new Appointment_Slot__c();
        slot.Account__c = locationId;
        slot.Date__c = vaccineDate;
        slot.Start_Time__c = Time.newInstance(currentTime.hour(), currentTime.minute(), currentTime.second(), 0);
        slot.End_Time__c = Time.newInstance(currentTime.addMinutes(5).hour(), currentTime.addMinutes(5).minute(), currentTime.addMinutes(5).second(), 0);
        insert slot;    // can't enforce security check here this is the only place we are creating slot from Citizen User, and we don't want to give create access to slot to any portal user
        return slot.Id;
    }

    /**
    * @description returns missing existing dose data
    * @author Balram Dhawan | 12-31-2021 
    * @param Map<String Object> inputData 
    * following keys are required in request
    * patientId, userRequestsForVaccineType
    * @return Map<String, Object> 
    **/
    @AuraEnabled
    public static Map<String, Object> checkExistingDoses(Map<String, Object> inputData){
        try {
            Map<String, Object> response = new Map<String, Object>();
            String patientId = String.valueOf(inputData.get('patientId'));
            String userRequestsForVaccineType = String.valueOf(inputData.get('userRequestsForVaccineType'));  //  userRequest/choosen vaccineType  (Vaccination-1, Vaccination-2, Vaccination-3)
            if(String.isBlank(patientId)) {
                throw new VT_Exception('Something went wrong, please try again later or contact to System Administrator');
            }
            List<Appointment__c> appointments = [SELECT Id, Event__r.Private_Access__r.Vaccine_Type__c  
                                                    FROM Appointment__c 
                                                    WHERE 
                                                        Status__c = 'Completed'
                                                            AND
                                                        Patient__c = :patientId
                                                            AND
                                                        Event__r.Private_Access__c != null
                                                            AND
                                                        Event_Process__c = 'Vaccination'
                                                    WITH SECURITY_ENFORCED];
            List<String> missingExistingDoses = new List<String>(getMissingExistingDoses(userRequestsForVaccineType, appointments));

            response.put('missingExistingDoses', missingExistingDoses);
            String finalVaccineTypeToConsider = (missingExistingDoses.isEmpty()) ? userRequestsForVaccineType : missingExistingDoses.get(0);
            inputData.put('action', (finalVaccineTypeToConsider == 'Vaccination-1') ? 'schedule' : 'scheduleNextDose');
            inputData.put('vaccineType', finalVaccineTypeToConsider);
            response.putAll(TVRS_EventFinderController.loadInitialData(inputData));
            return response;
        } catch (Exception e) {
            throw new VT_Exception(e.getMessage());
            // throw new AuraHandledException(e.getMessage());
        }
    }

    /**
    * @description helper to find get missing doses
    * @author Balram Dhawan | 01-09-2022 
    * @param String userRequestsForVaccineType 
    * @param List<Appointment__c> appointments 
    * @return Set<String> 
    **/
    private static Set<String> getMissingExistingDoses(String userRequestsForVaccineType, List<Appointment__c> appointments) {
        Map<String, Set<String>> vaccineTypeVsRequiredVaccines = new Map<String, Set<String>>{
            'Vaccination-1' =>  new Set<String>(),
            'Vaccination-2' =>  new Set<String>{'Vaccination-1'},
            'Vaccination-3' =>  new Set<String>{'Vaccination-1', 'Vaccination-2'},
            'Vaccination-4' =>  new Set<String>{'Vaccination-1', 'Vaccination-2', 'Vaccination-3'},
            'Vaccination-5' =>  new Set<String>{'Vaccination-1', 'Vaccination-2', 'Vaccination-3', 'Vaccination-4'}
        };
        Set<String> requiredVaccines = vaccineTypeVsRequiredVaccines.get(userRequestsForVaccineType);

        
        for (Appointment__c appointment : appointments) {
            requiredVaccines.remove(appointment.Event__r.Private_Access__r.Vaccine_Type__c);
        }
        
        return requiredVaccines;
    }

    /**
    * @description This method fetches the parent and child contacts for loggedIn user, valid only for citizen portal
    * @author Balram Dhawan | 01-03-2022 
    * @return Map<String, Object> 
    **/
    @AuraEnabled
    public static Map<String, Object> getSelfAndChildPatients(){
        try {
            Map<String, Object> response = new Map<String, Object>();
            String patientId = [SELECT Id, ContactId FROM User WHERE Id =:UserInfo.getUserId()]?.ContactId;
            List<PicklistValue> picklistValues = new List<PicklistValue>();
            List<Contact> patientsData = new List<Contact>();
            for (Contact patient : [SELECT Id, Name, (SELECT Id FROM Appointments2__r) FROM Contact 
                                    WHERE 
                                        (Id =:patientId OR (Parent_Contact__c =:patientId AND Parent_Contact__c != null)) 
                                    WITH SECURITY_ENFORCED
                                    ORDER BY CreatedDate ASC]) {
                PicklistValue value = new PicklistValue();
                value.label = (patientId == patient.Id) ? patient.Name : patient.Name +' (D)';
                value.value = patient.Id;
                if(patientId == patient.Id && picklistValues.size() > 0) {
                    picklistValues.add(0, value);
                    patientsData.add(0, patient);
                } else {
                    picklistValues.add(value);
                    patientsData.add(patient);
                }
            }
            response.put('patients', picklistValues);
            response.put('patientsData', patientsData);
            return response;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class PicklistValue {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
    }
}