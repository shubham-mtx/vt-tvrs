import {
    LightningElement,
    track,
    api
} from "lwc";
import appTitle from '@salesforce/label/c.OKPC_Location_Map_Title';

export default class OkpcLocationMap extends LightningElement {

    label = {
        appTitle
    }
    selectedEvent = '';
    isLoaded = false;

    @track siteEvents = [];
    @track startTime;
    @track endTime;
    @track appointmentDate;
    @track mindate;
    @track showMap = false;
    @track today;
    @track selectedMarkerValue = '';
    @track mapIdToAccount = {};
    @track showFooter = true;
    @track listView = 'auto';
    @track error;
    @track mapMarkers = [];
    @track isLoaded = true;
    @track center = {
        location: {
            State: 'OK',
        },
    };
    @track isVaccination;
    @api zoomLevel = 9;
    @api islobby = false;
    @api locationTitle = 'Pick Testing Site';
    @api hideLocationLabel = false;
    @api eventId;
    @api privateAccessId;
    @api privateAccessGroupName;
    @api eventProcess;
    @api vaccinationPrivateAccess;
    @api preRegDateTime;
    @api  secondDoseData = {};

    showPageHeader = true;

    get hasLocation() {
        return this.mapMarkers.length;
    }

    get eventCount() {
        return this.siteEvents.length;
    }

    get isSelectDisabled() {
        return !this.selectedEvent;
    }

    connectedCallback(){
        //this.privateAccessId = this.eventId;
        this.locationTitle = (this.eventProcess==='Vaccination')?'Pick Vaccination Site':'Pick Testing Site';
        if(this.eventProcess==='Vaccination'){
            this.isVaccination = true;
        }
        setTimeout(()=>{
            this.focusFirstEle();
        },500);

        this.focusTitle();
    }

    handleEventSelected(event){
        this.selectedMarkerValue = event.detail.accountId;
        this.selectedEvent = event.detail.eventId;
    }
   

    handleSaveModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: this.selectedMarkerValue
        }));
        this.dispatchEvent(new CustomEvent('eventselected', {
            bubbles: true,
            composed: true,
            detail: this.selectedEvent
        }));
    }

    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    handleKeyDown(event) {
        if(event.code == 'Escape') {
            this.dispatchEvent(new CustomEvent('close', {
                detail: 'close'
            }));
            event.preventDefault();
            event.stopImmediatePropagation();
        }
    }

    focusFirstEle() {
        const focusableElements = 'lightning-combobox, lightning-button-icon, button, input, lightning-helptext, lightning-input, lightning-button';
        const modal = this.template.querySelector('.slds-modal');
        const firstFocusableElement = modal.querySelectorAll(focusableElements)[0];
        const focusableContent = modal.querySelectorAll(focusableElements);
        const lastFocusableElement = focusableContent[focusableContent.length - 1];
        firstFocusableElement.focus();
        this.template.addEventListener('keydown', function(event) {
        let isTabPressed = event.key === 'Tab' || event.keyCode === 9;
            if (!isTabPressed) {
                return;
            }
            if (event.shiftKey) {               
                if (this.activeElement === firstFocusableElement) {
                   lastFocusableElement.focus(); 
                    event.stopPropagation()
                    event.preventDefault();
                }
            } else { 
                if (this.activeElement === lastFocusableElement) {    
                    firstFocusableElement.focus(); 
                    event.preventDefault();
                    event.stopPropagation()
                }
            }
        });
    }

    focusTitle() {
        let focus = setInterval(() => {
            let cmp = this.template.querySelector('.pageTitle');
            if(cmp) {
                cmp.focus();
                setTimeout(() => {
                    this.showPageHeader = false;
                }, 500);
                clearInterval(focus);
            }
        }, 200);   
    }
}