public with sharing class OKPCRecordTypeUtility {

    public static Id retrieveRecordTypeId(String objectName, String recordTypeDeveloperName) {
            Map<String,Schema.SobjectType> globalDescribe = Schema.getGlobalDescribe();
            if(globalDescribe.containsKey(objectName)) {
                Map<String,Schema.RecordtypeInfo> recordTypeMap = globalDescribe.get(objectName).getDescribe().getRecordTypeInfosByDeveloperName();
                if(recordTypeMap.containsKey(recordTypeDeveloperName)) {
                    return recordTypeMap.get(recordTypeDeveloperName).RecordTypeId;
                }
            }
        return null;
    }
}