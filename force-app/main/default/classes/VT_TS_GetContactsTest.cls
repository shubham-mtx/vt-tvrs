@isTest
public class VT_TS_GetContactsTest {
	@TestSetup
    static void makeData(){
        
        String recordTypeID = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        String citizenCovidId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Event_Process__c = 'Testing Site';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;
        
        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-44151',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            what_was_the_last_date_of_contact__c = Date.newInstance(2020, 2, 20),
            Did_you_have_a_nasal_swab__c = 'Yes	',
            What_was_your_test_result__c = 'Negative',
            Approximately_what_date_were_you_tested__c = Date.newInstance(2020, 3, 3),
            Had_contact_with_anyone_diagon__c = 'Yes',
            Do_you_take_prescription_medications__c = '	Yes',
            medication_for_fever_in_the_past_3_days__c = 'Yes',
            First_Date_you_have_been_feeling_sick__c = Date.newInstance(2020, 3, 1) ,
            First_date_you_began_feeling_well__c= Date.newInstance(2020, 3, 10),
            Fever_Cough_or_Difficulty_breathing__c = 'Yes',
            Sick_since_Jan_1_2020__c = 'Yes',
            Opt_out_for_SMS_Updates__c = true,
            HasOptedOutOfEmail = false,
            Current_Step__c = 1,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con;
        
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType IN : customerUserTypes  limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.comtestException',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            //userRoleId = portalRole.Id,
            contactId = con.id
        );
        insert newUser;
        
        Id testCenterRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Citizen (COVID)').getRecordTypeId();
        Contact newContact = new Contact();
        newContact.RecordTypeId = testCenterRecordTypeId;
        newContact.FirstName = 'test';
        newContact.LastName = 'Search';
        newContact.Birthdate = Date.today()-5;
        newContact.Email = 'testSearch@gmail.com';
        insert newContact;
        
        Contact parentContact = new Contact();
        parentContact.RecordTypeId = testCenterRecordTypeId;
        parentContact.FirstName = 'parent';
        parentContact.LastName = 'Search';
        parentContact.Birthdate = Date.today()-10;
        parentContact.Email = 'parentsearch@gmail.com';
        insert parentContact;
    }
    @IsTest
    static void getContactListTest(){
        List<Contact> contact = [SELECT Id, FirstName,LastName,Birthdate,Email,Result_Recieve_Consent__c,MailingAddress,RecordTypeId FROM Contact WHERE Email = 'testSearch@gmail.com'];
        //Id contactId = [SELECT Id, FirstName,LastName,Birthdate,Email,Result_Recieve_Consent__c,MailingAddress,RecordTypeId FROM Contact WHERE Email = 'testSearch@gmail.com'].Id;
        Date birthDate = Date.today()-5;
        Id parentId = [SELECT Id FROM Contact WHERE Email = 'parentsearch@gmail.com'].Id;
        Test.startTest();
        List<Contact> contactList = VT_TS_GetContacts.getContactList('t', 'S',birthDate, '', true, parentId);
        List<Contact> contactList2 = VT_TS_GetContacts.getContactList('t', 'S',birthDate, contact[0].Id, false, '');
        List<Contact> contactList3 = VT_TS_GetContacts.getContactList('', 'S',birthDate, contact[0].Id, true, parentId);
        System.assertEquals(contact.size(), contactList.size());
        
        User usr = [select id from User where userName='newUser@yahoo.comtestException'];
		system.runAs(usr){
            try{
                List<Contact> contacts = VT_TS_GetContacts.getContactList('t', 'S',birthDate, '', true, parentId);
                system.assertEquals(true, contacts.size() > 0, 'getContactListTest');
            }catch(Exception e){
                
            }
        }
        Test.stopTest();
    }
}