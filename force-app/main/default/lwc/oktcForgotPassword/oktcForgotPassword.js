import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import forgotPassword from '@salesforce/apex/okpcRegisterController.forgotPassword';
import loginURL from '@salesforce/label/c.OKTC_Login_URL';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';
export default class OktcForgotPassword extends NavigationMixin(LightningElement) {

    @track showError = false;
    @track errorMessage = '';
    @track username = '';

    customImage = nysdohResource;

    navigateToLogin() {
        location.href = loginURL;
    }

    nameChange(event) {
        this.username = event.target.value;
    }

    doForgotPasswordCheck() {
        this.showError = false;

        if (this.username == '' || typeof this.username == 'undefined') {
            this.errorMessage = 'Please provide an email address.';
            this.showError = true;
            return;
        }

        if (!this.validateEmail(this.username)) {
            this.errorMessage = 'Please provide a valid email address. example: "abc@xyz.com"';
            this.showError = true;
            return;
        }

        forgotPassword({ userName: this.username, delimiter: '' })//Added delimiter by Sajal
            .then(data => {
                //this.navigateToLogin();
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'A message has been sent to you by email with instructions on how to reset your password.'
                }));
                setTimeout(() => {
                    this.navigateToLogin();
                }, 3000);
            })
            .catch(error => {
                showAsyncErrorMessage(this,error);
            });
    }

    validateEmail(email) {
        var regExpEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;
        var isValid = email.match(regExpEmail);
        return isValid;
    }

}