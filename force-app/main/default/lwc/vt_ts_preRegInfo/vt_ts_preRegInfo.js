/**
 * @lastModifiedBy balram.dhawan@mtxb2b.com
 * @lastModifiedDate 2021-02-15
 * @desc handle Pre-reg for vaccination appointments
 */
 import {
    LightningElement,
    api,
    track
} from 'lwc';
import getPreRegInfo from "@salesforce/apex/VTTS_PreRegInfoController.getPreRegRecord";
import getAgeBasedGroup from "@salesforce/apex/VTTS_PreRegInfoController.getAgeBasedGroup";
import BIPOC_PASSCODE from '@salesforce/label/c.BIPOC_Passcode';
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';
export default class Vt_ts_preRegInfo extends LightningElement {

    @api contactId;
    @api fromCovidPortal;
    @api fromTestingSite;
    @api fromSchedular;
    @api confirmationModal;

    @track preRegData = {
        // "Do_not_have_a_Provider__c": false,
        "Do_you_have_a_Health_Risk_Passcode__c": "",
        "Affiliated_to_any_other_Risk_Group__c": "",
        // "Are_you_eligible_for_Vaccine_Risk_Group__c": "",
        "Chronic_Condition__c": "",
        "Pass_Code_Health__c": "",
        "Pass_Code_Risk_Group__c": "",
        "Pre_Registration_Group__c": "",
        // "Provider_Name__c": "",
        // "Provider_Phone__c": "",
        "Status__c": "",
        "Pre_Registration_Group__r": {
            "Name": "",
            "Group_Description__c": "",
            "Estimated_Date__c": ""
        },
        "Early_Group1_Health__c": "",
        "Early_Group1_Health__r": {
            "Name": "",
            "Group_Description__c": "",
            "Estimated_Date__c": ""
        },
        "Early_Group_2_Access_Group__c": "",
        "Early_Group_2_Access_Group__r": {
            "Name": "",
            "Group_Description__c": "",
            "Estimated_Date__c": ""
        }
    };
    @track ageBasedGroup = {};
    @track doNotHaveProvider;
    immuneWeakQues = `<ul style='box-sizing: border-box; margin-top: 0px; margin-right: 0px; margin-bottom: var(--lwc-spacingSmall,0.75rem); margin-left: var(--lwc-spacingLarge,1.5rem); padding: 0px; list-style: disc; color: var(--lwc-colorTextLabel,#696969); font-family: -apple-system, system-ui, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";  font-size: var(--lwc-formLabelFontSize,0.75rem); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;'>
    <li style="box-sizing: border-box;"><span style="box-sizing: border-box; color: windowtext;">Been receiving active cancer treatment for tumors or cancers of the blood&nbsp;</span>
            <li style="box-sizing: border-box;"><span style="box-sizing: border-box; color: windowtext;">Received an organ transplant and are taking medicine to suppress the immune system&nbsp;</span></li>
            <li style="box-sizing: border-box;"><span style="box-sizing: border-box; color: windowtext;">Received a stem cell transplant within the last 2 years or are taking medicine to suppress the immune system&nbsp;</span></li>
            <li style="box-sizing: border-box;"><span style="box-sizing: border-box; color: windowtext;">Moderate or severe primary immunodeficiency (such as DiGeorge syndrome, Wiskott-Aldrich syndrome)&nbsp;</span></li>
            <li style="box-sizing: border-box;"><span style="box-sizing: border-box; color: windowtext;">Advanced or untreated HIV infection&nbsp;</span></li>
            <li style="box-sizing: border-box;"><span style="box-sizing: border-box; color: windowtext;">Active treatment with high-dose corticosteroids&nbsp;(</span><span style="box-sizing: border-box; color: windowtext;">&ge;</span><span style="box-sizing: border-box; color: windowtext;">20mg prednisone or equivalent per day) or other drugs that may suppress one&rsquo;s immune response&nbsp;</span></li>
    </li>
</ul>`;
    isImmuneWeakQues;
    abbrPosition = '';
    // highRiskHealthConditions;
    // highRiskHealthConditionSection;
    healthPassCodeSection;
    otherRiskPassCodeSection;
    vaccinePriorityGroups;
    yesNoOptions = [{
        label: 'Yes',
        value: 'Yes'
    }, {
        label: 'No',
        value: 'No'
    }];
    yesNoHealthRiskOptions = [{
        label: 'Yes',
        value: 'Yes'
    }, {
        label: 'No',
        value: 'No'
    }];
    priorityJobGroupOptions;
    chronicConditionDetailsOptions;
    disabled;
    disabledHealthRisk;
    disabledIsImmune;
    disabledOtherRisk;
    showChronicConditionDetails;
    primaryCareRequired;
    chronicConditionDetailValues;
    showSpinner = true;
    passcodeInfo;
    oldPreRegData = {};
    healthRiskPasscodeMsg;
    showNewPassCodeField;
    earlyGroup1Health = {
        "Id": "",
        "Name": "",
        "Group_Description__c": "",
        "Estimated_Date__c": ""
    };
    earlyGroup2Health = {
        "Id": "",
        "Name": "",
        "Group_Description__c": "",
        "Estimated_Date__c": ""
    };
    isConnectedCallbackLoaded = false;
    localDob;
    contactAge;
    otherRiskGroupSection;
    disabledBIPOC;
    bipocPasscode = JSON.parse(BIPOC_PASSCODE);
    showClinicCodeField;
    chronicConditionDisabled;
    showHighRiskQuestion;
    marginAbbrSectionFromClinicalPortal = 'margin-left:-1%;position:absolute;'; 
    passCodeSectionPaddingClinical = 'padding-top: 0.5%;display: inline-block;font-size: var(--lwc-formLabelFontSize,0.75rem);color: var(--lwc-colorTextLabel,#696969);font-weight: 600;';
    vaccinePaddingClinical ='display:inline-block;font-size: var(--lwc-formLabelFontSize,0.75rem);color: var(--lwc-colorTextLabel,#696969);font-weight: 600;'

    get displayAgeBasedGroupSection() {
        return (this.ageBasedGroup && Object.keys(this.ageBasedGroup).length > 0);
    }

    @api
    setAgeBasedGroup(dob) {
        this.localDob = dob;
        if (dob && this.isConnectedCallbackLoaded) {
            this.showSpinner = true;
            getAgeBasedGroup({
                    dob: dob,
                    contactId: this.contactId
                })
                .then(result => {
                    if (!result.hasCompletedAppointment) {
                        if (result.group) {
                            // this.ageBasedGroup && 
                            if (this.ageBasedGroup.Id !== result.group.Id) {
                                this.ageBasedGroup = result.group;
                                this.preRegData.Pre_Registration_Group__c = result.group.Id;
                                this.preRegData.Pre_Registration_Group__r = result.group;
                                this.dispatchEvent(new CustomEvent('defaultgroupchanged', {
                                    detail: (this.ageBasedGroup) ? JSON.parse(JSON.stringify(this.ageBasedGroup)) : {
                                        "Id": "",
                                        "Name": "",
                                        "Group_Description__c": "",
                                        "Estimated_Date__c": ""
                                    }
                                }));
                                this.resetValuesOnDefaultGroupChanged();
                            } else {
                                this.ageBasedGroup = result.group;
                                this.preRegData.Pre_Registration_Group__c = result.group.Id;
                                this.preRegData.Pre_Registration_Group__r = result.group;
                            }
                        } else {
                            if(this.preRegData.Pre_Registration_Group__c) { // condition added for that case if contact doesn't had age based group earlier as well
                                this.ageBasedGroup = undefined;
                                this.preRegData.Pre_Registration_Group__c = undefined;
                                this.preRegData.Pre_Registration_Group__r = undefined;
                                this.dispatchEvent(new CustomEvent('defaultgroupchanged', {
                                    detail: (this.ageBasedGroup) ? JSON.parse(JSON.stringify(this.ageBasedGroup)) : {
                                        "Id": "",
                                        "Name": "",
                                        "Group_Description__c": "",
                                        "Estimated_Date__c": ""
                                    }
                                }));
                                this.resetValuesOnDefaultGroupChanged();
                            }
                        }
                    }
                    this.showSpinner = false;
                })
                .catch(error => {
                    showAsyncErrorMessage(this, error);
                });
        }
    }

    get chronicConditionLabel() {
        return 'Do you have one or more of the following high risk health conditions OR are you the primary caregiver of a child under the age of 16 who has any of the following conditions?';
    }

    connectedCallback() {
        this.isImmuneWeakQues = (!this.fromSchedular) ? `<p style='box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(8, 7, 7); font-family: -apple-system, system-ui, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;'><p style="box-sizing: border-box; font-size: var(--lwc-formLabelFontSize,0.75rem); color: var(--lwc-colorTextLabel,#696969);">I certify that I/the patient meet one or more of the following criteria, which makes me eligible for a booster dose of the COVID-19 vaccine:&nbsp;</p><span style="box-sizing: border-box; color: windowtext; font-size: 11.5pt;">&nbsp;</span></p>` + this.immuneWeakQues : `<p style='box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(8, 7, 7); font-family: -apple-system, system-ui, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;'><p style="box-sizing: border-box; padding-left:0.5rem; font-size: var(--lwc-formLabelFontSize,0.75rem); color: var(--lwc-colorTextLabel,#696969);">I certify that I/the patient meet one or more of the following criteria, which makes me eligible for a booster dose of the COVID-19 vaccine:&nbsp;</p><span style="box-sizing: border-box; color: windowtext; font-size: 11.5pt;">&nbsp;</span></p>` + this.immuneWeakQues;
        this.abbrPosition = (this.fromSchedular) ? 'position:absolute' : this.abbrPosition;
        this.passCodeSectionPaddingClinical = (this.fromSchedular && !this.fromTestingSite) ? 'padding-left: 1%;color:var(--lwc-colorTextLabel,#696969);' : this.passCodeSectionPaddingClinical;
        this.vaccinePaddingClinical = (this.fromSchedular && !this.fromTestingSite) ? 'padding-top: 0.5%;display: inline-block;font-size: var(--lwc-formLabelFontSize,0.75rem);color: var(--lwc-colorTextLabel,#696969);padding-left: 1%;' : this.vaccinePaddingClinical;
        this.marginAbbrSectionFromClinicalPortal = (this.fromSchedular && !this.fromTestingSite) ? 'padding-top:0.3%;position: absolute;' : this.marginAbbrSectionFromClinicalPortal;
        this.doInit();
        setTimeout(() => {
            this.focusFirstEle();
        }, 200);
    }

    get isImmuneDisabled() {
        return (this.preRegData.Is_Immune_Weak__c === true) ? 'disabled' : 'false';
    }

    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc fetch existing record
     */
    @api
    doInit() {
        if (this.contactId) {
            getPreRegInfo({
                    contactId: this.contactId
                })
                .then(result => {
                    this.isConnectedCallbackLoaded = true;
                    if (result.Pre_Registrations__r) {

                        Object.assign(this.oldPreRegData, result.Pre_Registrations__r[0]);

                        this.doInitHelper(result.Pre_Registrations__r[0]);
                    } else {
                        this.localDob = result.Birthdate;
                        this.setAgeBasedGroup(this.localDob);
                    }
                    this.showSpinner = false;
                })
                .catch(error => {
                    showAsyncErrorMessage(this, error);
                });
        } else {
            this.isConnectedCallbackLoaded = true;
            this.refreshAllProperties();
            this.showSpinner = false;

            this.setAgeBasedGroup(this.localDob);
        }
    }

    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc Parse result and set other required values
     */
    doInitHelper(result) {
        var _preRegData = {};
        this.healthRiskPasscodeMsg = false;
        if (result.Pre_Registration_Group__c) {
            this.ageBasedGroup = result.Pre_Registration_Group__r;
        }

        for (const key in result) {
                this.handleChange({
                    target: {
                        name: key,
                        value: result[key]
                    }
                });
        }

        if (result.Early_Group1_Health__c) {
            this.vaccinePriorityGroups = true;
            this.earlyGroup1Health = result.Early_Group1_Health__r;
        }

        if (result.Early_Group_2_Access_Group__c) {
            this.vaccinePriorityGroups = true;
            this.earlyGroup2Health = result.Early_Group_2_Access_Group__r;
        }

        if (result.BIPOC__c === 'Yes') {
            this.disabledBIPOC = 'disabled';
        }

        //Update by Shubham as Per Security Issue Sheet
        if (result.BIPOC__c === 'No' && result.Early_Group_2_Access_Group__c) {
            this.disabledBIPOC = 'disabled';
            this.disabledOtherRisk = 'disabled';
        }

        // w.r.t 51841
        if (result.BIPOC__c === 'Yes' && result.Early_Group_2_Access_Group__c) {
            this.otherRiskGroupSection = false;
        }

        // this.doNotHaveProvider = result.Do_not_have_a_Provider__c;

        _preRegData = JSON.parse(JSON.stringify(this.preRegData));
        Object.assign(_preRegData, result);
        if (_preRegData.Chronic_Condition__c === 'Yes' || _preRegData.Chronic_Condition__c === 'Rejected') {
            this.healthRiskPasscodeMsg = true;
            this.disabledHealthRisk = 'disabled';
        }
        this.chronicConditionDisabled = (_preRegData.Chronic_Condition__c === 'Yes');
        this.disabledIsImmune = (_preRegData.Is_Immune_Weak__c === 'Yes');

        this.preRegData = _preRegData;
        console.log(JSON.parse(JSON.stringify(this.preRegData)));
        if (this.preRegData.Id) {
            this.dispatchEvent(new CustomEvent('alreadyregistered', {
                detail: this.preRegData
            }));
        }
    }

    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc Handle dependent fields
     */
    handleChange(event) {
        let fieldName = event.target.name;
        let fieldValue = event.target.value;
        let previousValueForImmuneWeak = (fieldName === 'Is_Immune_Weak__c') ? this.preRegData.Is_Immune_Weak__c : null;

        switch(fieldName) {
            case 'Chronic_Condition__c':
                this.preRegData.Chronic_Condition__c = fieldValue;
                this.doNotHaveProvider = false;
    
                if (fieldValue === 'Yes') {
                    this.healthRiskPasscodeMsg = true;
                    this.preRegData.Do_you_have_a_Health_Risk_Passcode__c = 'No';
                } else {
                    this.healthPassCodeSection = false;
                    this.healthRiskPasscodeMsg = false;
                    this.cleanupFieldValues(['Do_you_have_a_Health_Risk_Passcode__c', 'Pass_Code_Health__c', /*'Provider_Name__c', 'Provider_Phone__c', 'Do_not_have_a_Provider__c'*/]);
                }
                break;
            case 'Affiliated_to_any_other_Risk_Group__c':
                if (fieldValue === 'Yes') {
                    this.otherRiskPassCodeSection = true;
                    this.showClinicCodeField = (this.fromTestingSite) ? true : false;
                } else {
                    this.otherRiskPassCodeSection = false;
                    this.showClinicCodeField = false;
                    this.preRegData.Pass_Code_Risk_Group__c = null;
                    this.cleanupFieldValues(['Pass_Code_Risk_Group__c']);
                }
                break;
            case 'BIPOC__c':
                this.cleanupFieldValues(['Affiliated_to_any_other_Risk_Group__c', 'Pass_Code_Risk_Group__c']);
                if (fieldValue !== 'Yes' && !this.fromCovidPortal) {
                    this.otherRiskGroupSection = true;
                } else {
                    this.otherRiskGroupSection = false;
                }
                break;
            case 'Is_Immune_Weak__c':
                if(fieldValue === 'Yes' &&  this.preRegData.Chronic_Condition__c != 'Yes'){
                    this.chronicConditionDisabled = true;
                    this.doNotHaveProvider = true;
                    this.disabledHealthRisk = true;
                }
                if(fieldValue === 'Yes'){
                    this.preRegData.Chronic_Condition__c = 'Yes';
                    this.chronicConditionDisabled = true;
                    this.showHighRiskQuestion = false;
                    
                    this.checkFieldValidity();
                }
                if(fieldValue === 'No'){
                    this.disabledHealthRisk = false;
                    this.chronicConditionDisabled = false;
                    this.showHighRiskQuestion = true;
                }
                if(previousValueForImmuneWeak == 'Yes' && fieldValue === 'No'){
                   
                    this.doNotHaveProvider = false;
                    this.preRegData.Chronic_Condition__c = null;
                }
                this.preRegData.Is_Immune_Weak__c = fieldValue ;
                break;
                
            default:
                break;
        }
    }

    checkFieldValidity(){
        let input = this.template.querySelector('[data-id="chronicCondition"]');
        if(input){
            input.value =  this.preRegData.Chronic_Condition__c;
            // input.setCustomValidity("");
            input.reportValidity();
        }
    }
    cleanupFieldValues(fields) {
        fields.forEach(f => {
            this.preRegData[f] = undefined;
        });
    }

    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc Format phone number in US format
     */
    formatPhone(obj) {
        var numbers = obj.value.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj.value = "";
        for (var i = 0; i < numbers.length; i++) {
            obj.value += (char[i] || "") + numbers[i];
        }
    }

    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc check standard field validations
     */
    isValid() {
        let data = {};
        const allValid = [...this.template.querySelectorAll('lightning-input, lightning-combobox, lightning-dual-listbox')]
            .reduce((validSoFar, inputCmp) => {
                if (inputCmp.type === 'checkbox') {
                    data[inputCmp.name] = inputCmp.checked;
                } else {
                    data[inputCmp.name] = inputCmp.value;
                }
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
        Object.assign(this.preRegData, data);
        return allValid;
    }

    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc runs when user confirm the popup for non-editable pre-reg
     */
    confirmationPositive(event) {
        event.target.disabled = true;
        this.dispatchEvent(new CustomEvent('confirmation'));
    }

    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc runs when user cancel confirmation modal
     */
    confirmationCancel() {
        this.confirmationModal = false;
        this.dispatchEvent(new CustomEvent('modalcancellation'));
    }

    /**
     * @lastModifiedBy balram.dhawan@mtxb2b.com
     * @lastModifiedDate 2021-02-15
     * @desc pop-up non-editable confirmation modal
     */
    @api
    getPreRegData() {
        let data = {
            success: false,
            msg: ''
        };
        const allValid = this.isValid();
        if (!allValid) {
            data.msg = 'Please fill all required fields';
        } else {
            data.success = true;
            this.preRegData.Pass_Code_Risk_Group__c = (this.preRegData.BIPOC__c === 'Yes') ? (this.contactAge < 18) ? 'BIPOC'+this.contactAge : this.bipocPasscode['18_PLUS'] : this.preRegData.Pass_Code_Risk_Group__c ;
            this.preRegData.Early_Group_2_Access_Group__c = (this.fromCovidPortal && this.preRegData.BIPOC__c != this.oldPreRegData.BIPOC__c && (!this.preRegData.BIPOC__c || this.preRegData.BIPOC__c === 'No') && this.preRegData.Early_Group_2_Access_Group__c) ? null : this.preRegData.Early_Group_2_Access_Group__c;
            this.preRegData.Early_Group1_Health__c = (!this.preRegData.Chronic_Condition__c || this.preRegData.Chronic_Condition__c === 'No') ? null : this.preRegData.Early_Group1_Health__c;
            data.preRegData = this.preRegData;
            if(data.preRegData){
                data.preRegData.contactAge = this.contactAge;
            }
            data.oldPreRegData = this.oldPreRegData;
        }
        return data;
    }

    showPopup() {
        this.passcodeInfo = true;
    }

    hidePopup() {
        this.passcodeInfo = false;
    }

    refreshAllProperties() {
        // this.highRiskHealthConditions = undefined;
        // this.highRiskHealthConditionSection = undefined;
        this.healthPassCodeSection = undefined;
        this.otherRiskPassCodeSection = undefined;
        this.vaccinePriorityGroups = undefined;
        this.yesNoOptions = [{
            label: 'Yes',
            value: 'Yes'
        }, {
            label: 'No',
            value: 'No'
        }];
        this.yesNoHealthRiskOptions = [{
            label: 'Yes',
            value: 'Yes'
        }, {
            label: 'No',
            value: 'No'
        }];
        this.priorityJobGroupOptions = undefined;
        this.chronicConditionDetailsOptions = undefined;
        this.disabled = undefined;
        this.disabledHealthRisk = undefined;
        this.disabledIsImmune = undefined;
        this.disabledOtherRisk = undefined;
        this.showChronicConditionDetails = undefined;
        this.primaryCareRequired = undefined;
        this.chronicConditionDetailValues = undefined;
        this.showSpinner = true;
        this.passcodeInfo = undefined;
        this.doNotHaveProvider = undefined;
        this.ageBasedGroup = {};
        this.disabledBIPOC = undefined;
        this.otherRiskGroupSection = undefined;
        this.preRegData.Pre_Registration_Group__c = undefined;
        this.preRegData.Pre_Registration_Group__r = undefined;
        this.earlyGroup1Health = {
            "Id": "",
            "Name": "",
            "Group_Description__c": "",
            "Estimated_Date__c": ""
        };
        this.earlyGroup2Health = {
            "Id": "",
            "Name": "",
            "Group_Description__c": "",
            "Estimated_Date__c": ""
        };
        this.preRegData = {
            // "Do_not_have_a_Provider__c": false,
            "Do_you_have_a_Health_Risk_Passcode__c": "",
            "Affiliated_to_any_other_Risk_Group__c": "",
            // "Are_you_eligible_for_Vaccine_Risk_Group__c": "",
            "Chronic_Condition__c": "",
            "Pass_Code_Health__c": "",
            "Pass_Code_Risk_Group__c": "",
            "Pre_Registration_Group__c": "",
            // "Provider_Name__c": "",
            // "Provider_Phone__c": "",
            "Status__c": "",
            "Pre_Registration_Group__r": {
                "Name": "",
                "Group_Description__c": "",
                "Estimated_Date__c": ""
            },
            "Early_Group1_Health__c": "",
            "Early_Group1_Health__r": {
                "Name": "",
                "Group_Description__c": "",
                "Estimated_Date__c": ""
            },
            "Early_Group_2_Access_Group__c": "",
            "Early_Group_2_Access_Group__r": {
                "Name": "",
                "Group_Description__c": "",
                "Estimated_Date__c": ""
            }
        };
    }

    calculateAge_Deprecated(date) {
        let dateArray = date.split('-');
        var Bday = new Date(parseInt(dateArray[0]), parseInt(dateArray[1]) - 1, parseInt(dateArray[2]), 11, 59);
        return ~~((Date.now() - Bday) / (31557600000));
    }

    calculateAge(date) {
        let enteredData = date;
        let enteredDataArray = enteredData.split('-');
        let enteredDataYear = parseInt(enteredDataArray[0]);
        let enteredDataMonth = parseInt(enteredDataArray[1]) - 1;
        let enteredDataDate = parseInt(enteredDataArray[2]);
        let todayDate = new Date();
        let initialAge = todayDate.getFullYear() - enteredDataYear;
        if (enteredDataMonth < todayDate.getMonth()) {
            return initialAge;
        } else if (enteredDataMonth > todayDate.getMonth()) {
            initialAge--;
            return initialAge;
        } else if (enteredDataDate > todayDate.getDate()) {
            initialAge--;
            return initialAge;
        } else {
            return initialAge;
        }
    }

    @api
    setContactAge(value) {
        this.contactAge = this.calculateAge(value);
    }

    resetValuesOnDefaultGroupChanged() {
        // this.preRegData.Are_you_eligible_for_Vaccine_Risk_Group__c = undefined;
        this.chronicConditionDisabled = undefined;          // added at 21-Jan-2022
        this.preRegData.Is_Immune_Weak__c = undefined;      // added at 21-Jan-2022
        this.showHighRiskQuestion = undefined;              // added at 21-Jan-2022

        this.preRegData.BIPOC__c = undefined;
        this.preRegData.Chronic_Condition__c = undefined;

        this.disabled = undefined;
        this.disabledHealthRisk = undefined;
        this.disabledIsImmune = undefined;
        this.disabledOtherRisk = undefined;
        this.disabledBIPOC = undefined;
        this.preRegData.Early_Group1_Health__c = undefined;
        this.preRegData.Early_Group1_Health__r = undefined;
        this.preRegData.Early_Group_2_Access_Group__c = undefined;
        this.preRegData.Early_Group_2_Access_Group__r = undefined;
        this.preRegData.Status__c = undefined;
        this.preRegData.Actual_Pass_Code__c = undefined;
        this.yesNoHealthRiskOptions = [{
            label: 'Yes',
            value: 'Yes'
        }, {
            label: 'No',
            value: 'No'
        }];
        this.earlyGroup1Health = {
            "Id": "",
            "Name": "",
            "Group_Description__c": "",
            "Estimated_Date__c": ""
        };
        this.earlyGroup2Health = {
            "Id": "",
            "Name": "",
            "Group_Description__c": "",
            "Estimated_Date__c": ""
        };
    }

    handleKeyDown(event) {
        if (event.code == 'Escape') {
            this.confirmationModal = false;
            this.dispatchEvent(new CustomEvent('preregconfirmationclose'));
            event.preventDefault();
            event.stopImmediatePropagation();
        }
    }



    focusModalClose() {
        setTimeout(() => {
            this.template.querySelector('.modalClose').focus();
        }, 1000);
    }

    @api
    focusFirstEle() {
        const focusableElements = 'lightning-combobox, lightning-button-icon, button, input, lightning-helptext, lightning-input, lightning-button';
        const modal = this.template.querySelector('.slds-modal');
        if (modal) {
            const firstFocusableElement = modal.querySelectorAll(focusableElements)[0];
            const focusableContent = modal.querySelectorAll(focusableElements);
            const lastFocusableElement = focusableContent[focusableContent.length - 1];
            firstFocusableElement.focus();
            this.template.addEventListener('keydown', function (event) {
                let isTabPressed = event.key === 'Tab' || event.keyCode === 9;
                if (!isTabPressed) {
                    return;
                }
                if (event.shiftKey) {
                    if (this.activeElement === firstFocusableElement) {
                        lastFocusableElement.focus();
                        event.stopPropagation()
                        event.preventDefault();
                    }
                } else {
                    if (this.activeElement === lastFocusableElement) {
                        firstFocusableElement.focus();
                        event.preventDefault();
                        event.stopPropagation()
                    }
                }
            });
        }
    }
}