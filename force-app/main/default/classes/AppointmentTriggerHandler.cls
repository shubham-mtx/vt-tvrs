/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 01-19-2022
 * @last modified by  : Mohit Karani
**/
public without sharing class AppointmentTriggerHandler {
    public static void beforeInsert(List<Appointment__c> appointments){
        updateStartDateStartTime(appointments, null);
        updateAppointmentDirections(appointments, null);
        preventDuplicateVaccineAppointment(appointments,null);
    }
    public static void afterInsert(List<Appointment__c> appointments) {
        filterAppointmentSlots(appointments, null);
    }
    public static void beforeUpdate(List<Appointment__c> appointments, Map<Id,Appointment__c> appointmentsMap) {
        updateStartDateStartTime(appointments, appointmentsMap);
        updateAppointmentDirections(appointments, appointmentsMap);
        updateFieldsOnAppointmentForNextDoseReminder(appointments, appointmentsMap);
        preventDuplicateVaccineAppointment(appointments,appointmentsMap);
    }
    
    public static void afterUpdate(List<Appointment__c> appointments,Map<Id,Appointment__c> appointmentsMap) {
        filterAppointmentSlots(appointments, appointmentsMap);

    }
    public static void afterDelete(List<Appointment__c> appointments) {
        filterAppointmentSlots(appointments, null);
    }
    public static void afterUndelete(List<Appointment__c> appointments) {
        filterAppointmentSlots(appointments, null);
    }
    
    /**
     * description : Update Appointment Slot Count with Number of Appointments 
     */
    public static void filterAppointmentSlots(List<Appointment__c> appointments, Map<Id,Appointment__c> appointmentsMap) {
        Set<Id> appointmentSlotIds = new Set<Id>();
        List<Id> contactIdList = new List<Id>();
        Map<Id, Integer> appointmentSlotVsCount = new Map<Id, Integer>();
        for(Appointment__c appointment: appointments) {
            if(appointment.Appointment_slot__c != null && appointmentsMap == null && (appointment.Status__c == 'Scheduled' || appointment.Status__c == 'Completed')) {
                if(!appointmentSlotVsCount.containsKey(appointment.Appointment_Slot__c)) {
                    appointmentSlotVsCount.put(appointment.Appointment_Slot__c, 0);
                }
                appointmentSlotVsCount.put(appointment.Appointment_Slot__c, appointmentSlotVsCount.get(appointment.Appointment_Slot__c)+1);
            } else if(appointmentsMap != null && appointment.Appointment_slot__c != appointmentsMap.get(appointment.Id).Appointment_slot__c  && (appointment.Status__c == 'Scheduled' || appointment.Status__c == 'Completed')) {
                if(appointment.Appointment_slot__c != null){
                    if(!appointmentSlotVsCount.containsKey(appointment.Appointment_Slot__c)) {
                        appointmentSlotVsCount.put(appointment.Appointment_Slot__c, 0);
                    }
                    appointmentSlotVsCount.put(appointment.Appointment_Slot__c, appointmentSlotVsCount.get(appointment.Appointment_Slot__c)+1);
                }  
            }
        }
        if(!appointmentSlotVsCount.isEmpty()) {
            List<Appointment_Slot__c> slotsToBeUpdate = new List<Appointment_Slot__c>();
            for (Id slotId : appointmentSlotVsCount.keySet()) {
                Appointment_Slot__c slot = new Appointment_Slot__c(Id=slotId);
                slot.Appointment_Count__c = appointmentSlotVsCount.get(slotId);
                slotsToBeUpdate.add(slot);
            }
            if(!slotsToBeUpdate.isEmpty()) {
                update slotsToBeUpdate;
            }
        }
    }    

    /**
     * @description : This method sets the appointment date time by querying the slotId available in the appointment
     */
    public static void updateStartDateStartTime(List<Appointment__c> appointments, Map<Id,Appointment__c> appointmentsMap){
        Set<Id> appointmentSlotIds = new Set<Id>();
        List<Id> contactIdList = new List<Id>();
        List<Appointment__c> appList = new List<Appointment__c>();
        for(Appointment__c appointment: appointments) {
            if((appointmentsMap == null) || (appointmentsMap != null && 
                                             appointmentsMap.get(appointment.Id).Appointment_slot__c != appointment.Appointment_slot__c)){
                                                 appointmentSlotIds.add(appointment.Appointment_slot__c); 
                                                 appList.add(appointment);                           
                                             }
        }
        Map<Id,Appointment_slot__c> appointmentSlotMap = new Map<Id,Appointment_slot__c>([SELECT Id,Start_Time__c,Date__c FROM Appointment_slot__c WHERE Id IN :appointmentSlotIds]);
        
        for(Appointment__c app : appList){
            if(appointmentSlotMap.containsKey(app.Appointment_slot__c) && appointmentSlotMap.get(app.Appointment_slot__c) != null){
                app.Appointment_Start_Date_v1__c = appointmentSlotMap.get(app.Appointment_slot__c).Date__c;
                app.Appointment_Start_Time_v1__c = appointmentSlotMap.get(app.Appointment_slot__c).Start_Time__c;
                DateTime dt = DateTime.newInstance(app.Appointment_Start_Date_v1__c, app.Appointment_Start_Time_v1__c);
                //system.debug('time ' + dt);
                app.Appointment_Start_Date_Time__c = dt;
            }else if(app.Status__c == 'Scheduled' && app.Appointment_slot__c == null){
                app.Appointment_Start_Date_v1__c = System.today();
                app.Appointment_Start_Time_v1__c = system.now().time();
            }
            
        }
    }

    /**
     * description : This method sets the appointment direction from Testing Site (Account) Lookup
     * @author : Balram Dhawan
     */
    public static void updateAppointmentDirections(List<Appointment__c> appointments, Map<Id, Appointment__c> oldAppointmentsMap) {
        Map<Id, String> accountIdVsDirections = new Map<Id, String>();
        for (Appointment__c app : appointments) {
            if(String.isNotBlank(app.Lab_Center__c)) {
                if(oldAppointmentsMap != null) {
                    if(oldAppointmentsMap.get(app.Id).Lab_Center__c != app.Lab_Center__c) {
                        accountIdVsDirections.put(app.Lab_Center__c, '');
                    }
                } else {
                    accountIdVsDirections.put(app.Lab_Center__c, '');
                }
            } else {
                app.Directions_long__c = null;
            }
        }

        if(!accountIdVsDirections.isEmpty()) {
            List<Account> accounts = [SELECT Id, Directions__c FROM Account WHERE Id IN :accountIdVsDirections.keySet()];
            for (Account acc : accounts) {
                accountIdVsDirections.put(acc.Id, acc.Directions__c);
            }

            for (Appointment__c app : appointments) {
                if(accountIdVsDirections.containsKey(app.Lab_Center__c)) {
                    app.Directions_long__c = accountIdVsDirections.get(app.Lab_Center__c);
                }
            }
        }
    }


    /**
    * @description : Method to update appointment fields for scheduling next dose for resending the email.
    * @author Mohit Karani | 11-30-2021 
    * @param List<Appointment__c> newList,Map<Id,Appointment__c> oldMap 
    **/
    public static void updateFieldsOnAppointmentForNextDoseReminder(List<Appointment__c> newList,Map<Id,Appointment__c> oldMap){
        Map<Id, Appointment__c> updatedAppointmentMap = new Map<Id, Appointment__c>();
        for(Appointment__c appointment : newList){
            if(appointment.Next_Dose_Notification_Sent_Email__c == VT_TS_Constants.RESEND_TEXT && 
                appointment.Next_Dose_Notification_Sent_Email__c != oldMap.get(appointment.Id).Next_Dose_Notification_Sent_Email__c
                && (oldMap.get(appointment.Id).Next_Dose_Notification_Sent_Email__c == VT_TS_Constants.EMAIL_SENT_TEXT
                || appointment.Next_Dose_Notification_Process__c == VT_TS_Constants.NOT_ELIGIBLE 
                || appointment.Next_Dose_Notification_Process__c == VT_TS_Constants.NOT_REQUIRED_STATUS) 
                && appointment.Patient__r.RecordTypeId == VT_TS_Constants.CITIZEN_COVID_RECORD_TYPE_ID ){
                    updatedAppointmentMap.put(appointment.Id,appointment);
            }
            else if(appointment.Next_Dose_Notification_Process__c == VT_TS_Constants.SKIPPED_OUT_OF_STATE 
                    || appointment.Next_Dose_Notification_Process__c == VT_TS_Constants.SKIPPED_ALREADY_SCHEDULED
                    || oldMap.get(appointment.Id).Next_Dose_Notification_Sent_Email__c == VT_TS_Constants.EMAIL_OPTED_OUT){
                        appointment.Next_Dose_Notification_Sent_Email__c = oldMap.get(appointment.Id).Next_Dose_Notification_Sent_Email__c == VT_TS_Constants.EMAIL_OPTED_OUT ?
                        VT_TS_Constants.EMAIL_OPTED_OUT : null;
            }
        }

        // Querying the same record again in order to fetch the values for the parent record fields as well. 
        List<Appointment__c> appoitmentListToUpdate = new List<Appointment__c>();
        for(Appointment__c appointmentToProcess : [SELECT Id,Patient__c,Status__c,Event__r.Private_Access__r.Requires_Followup_Dose__c,Next_Dose_Notification_Process__c,Next_Dose_Notification_Sent_Email__c,
                                                    Event__r.Private_Access__r.Vaccine_Type__c,Appointment_Complete_Date__c,Event__r.Private_Access__r.Weak_Immune_Min_Days__c,
                                                    Event__r.Private_Access__r.Min__c,Notification_Date__c,Appointment_Date__c, Patient__r.HasOptedOutOfEmail, 
                                                    Event__r.Private_Access__r.Vaccine_Class_Name__c, Event__r.Private_Access__r.Alternate_Doses__c, Event__r.Private_Access__r.Is_Alternate_Dose_Available__c,Event__r.Private_Access__r.Next_Dose_Include_Out_of_State__c,Event__r.Private_Access__r.Next_Dose_Notification_Type__c ,Event__r.Private_Access__r.Next_Dose_Notification_Lead_Days__c 
                                                    FROM Appointment__c 
                                                    WHERE ID IN : updatedAppointmentMap.keySet()]){
            
            appointmentToProcess.Next_Dose_Notification_Sent_Email__c = updatedAppointmentMap.get(appointmentToProcess.Id).Next_Dose_Notification_Sent_Email__c;
            appoitmentListToUpdate.add(appointmentToProcess);
        }
        if(!appoitmentListToUpdate.isEmpty()){
            updatedAppointmentMap = new Map<Id, Appointment__c>(TVRS_ScheduleNextDoseEmailHelper.updateAppointmentFields(appoitmentListToUpdate));

            for(Appointment__c app : newList){
                if(updatedAppointmentMap.containsKey(app.Id)) {
    
                    app.Next_Dose_Type__c = updatedAppointmentMap.get(app.Id).Next_Dose_Type__c;
                    app.Next_Dose_Start_Date__c = updatedAppointmentMap.get(app.Id).Next_Dose_Start_Date__c;
                    app.Next_Dose_Notification_Process__c = updatedAppointmentMap.get(app.Id).Next_Dose_Notification_Process__c;
                    app.Next_Dose_Notification_Sent_Email__c = updatedAppointmentMap.get(app.Id).Next_Dose_Notification_Sent_Email__c == VT_TS_Constants.EMAIL_OPTED_OUT ? VT_TS_Constants.EMAIL_OPTED_OUT :  app.Next_Dose_Notification_Process__c != VT_TS_Constants.SEND_TEXT ? null : app.Next_Dose_Notification_Sent_Email__c ;
                    // app.Next_Dose_Notification_Process__c = (app.Next_Dose_Notification_Process__c == VT_TS_Constants.SEND_TEXT && app.Next_Dose_Notification_Sent_Email__c ==  VT_TS_Constants.EMAIL_OPTED_OUT) ? null :  app.Next_Dose_Notification_Process__c;
                    app.Notification_Date__c = updatedAppointmentMap.get(app.Id).Notification_Date__c;
    
                }   
            }
        }
    }

    public static void preventDuplicateVaccineAppointment(List<Appointment__c> appointmentList,Map<Id,Appointment__c> oldMap){
        Map<Id, List<Appointment__c>> eventIdVsAppointments = new Map<Id, List<Appointment__c>>();
        Map<Id, Set<String>> patientIdVsVaccineSetInTrigger = new Map<Id, Set<String>>();
        Map<Id, Set<String>> patientIdVsCompletedDoseSetInDatabase = new Map<Id, Set<String>>();
        Set<String> vaccineTypesInTrigger = new Set<String>();
        for (Appointment__c appointmentRec : appointmentList) {
            if( String.isNotBlank(appointmentRec.Event__c) && 
                String.isNotBlank(appointmentRec.Patient__c) && 
                (appointmentRec.Status__c == VT_TS_Constants.COMPLETED_TEXT || appointmentRec.Status__c == VT_TS_Constants.SCHEDULED_TEXT) &&  
                ((oldMap != null && oldMap.get(appointmentRec.Id).Status__c != appointmentRec.Status__c) || oldMap == null)) {
                    if(!eventIdVsAppointments.containsKey(appointmentRec.Event__c)) {
                        eventIdVsAppointments.put(appointmentRec.Event__c, new List<Appointment__c>());
                    }
                    eventIdVsAppointments.get(appointmentRec.Event__c).add(appointmentRec);
            }
        }

        System.debug('eventIdVsAppointments-->'+eventIdVsAppointments);

        if(!eventIdVsAppointments.isEmpty()) {
            for (VTS_Event__c event : [SELECT Id, Private_Access__r.Vaccine_Type__c 
                                        FROM VTS_Event__c 
                                        WHERE Id IN :eventIdVsAppointments.keySet()
                                        AND Private_Access__r.Vaccine_Type__c != null
                                        AND Private_Access__r.Event_Process__c = 'Vaccination'
                                        WITH SECURITY_ENFORCED]) {
                vaccineTypesInTrigger.add(event.Private_Access__r.Vaccine_Type__c);
                for (Appointment__c appointment : eventIdVsAppointments.get(event.Id)) {
                    if(!patientIdVsVaccineSetInTrigger.containsKey(appointment.Patient__c)) {
                        patientIdVsVaccineSetInTrigger.put(appointment.Patient__c, new Set<String>());
                    }
                    patientIdVsVaccineSetInTrigger.get(appointment.Patient__c).add(event.Private_Access__r.Vaccine_Type__c);
                }
            }
            if(!patientIdVsVaccineSetInTrigger.isEmpty()) {
                for (Appointment__c appointment : [SELECT Id, Patient__c, Event__r.Private_Access__r.Vaccine_Type__c FROM Appointment__c 
                                                    WHERE 
                                                    Event__r.Private_Access__r.Vaccine_Type__c IN :vaccineTypesInTrigger
                                                    AND
                                                    Patient__c IN :patientIdVsVaccineSetInTrigger.keySet()
                                                    AND 
                                                    Status__c IN ('Completed', 'Scheduled')
                                                    AND ID NOT IN : appointmentList
                                                    WITH SECURITY_ENFORCED]) {
                    if(!patientIdVsCompletedDoseSetInDatabase.containsKey(appointment.Patient__c)) {
                        patientIdVsCompletedDoseSetInDatabase.put(appointment.Patient__c, new Set<String>());
                    }
                    patientIdVsCompletedDoseSetInDatabase.get(appointment.Patient__c).add(appointment.Event__r.Private_Access__r.Vaccine_Type__c);   
                }

                if(!patientIdVsCompletedDoseSetInDatabase.isEmpty()) {
                    for (Appointment__c appointment : appointmentList) {
                        if(patientIdVsCompletedDoseSetInDatabase.containsKey(appointment.Patient__c)) {
                            for (String completedDose : patientIdVsCompletedDoseSetInDatabase.get(appointment.Patient__c)) {
                                if(patientIdVsVaccineSetInTrigger.get(appointment.Patient__c).contains(completedDose) && String.isNotBlank(completedDose)) {
                                    appointment.addError('The patient had already received this dose');
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}