@isTest
public class okpcLoginControllerTest {

    public static testMethod void test(){
         okpcLoginController controller = new okpcLoginController();//constructor coverage
        User adminUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'okpclogincontrolleruser@vt.com',
            Username = 'okpclogincontrolleruser@vt.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
         System.runAs(adminUser){
             String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
             Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
             createAccountTestSite.BillingCity = 'testCity';
             createAccountTestSite.BillingCountry = 'testCountry';
             createAccountTestSite.BillingState = 'testState';
             createAccountTestSite.BillingStreet = 'testStreet';
             createAccountTestSite.BillingPostalCode = '12345';
             createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
             createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
             insert createAccountTestSite;
             
             Id citizenConsultationVitalRecords_RT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen').getRecordTypeId();
             String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
             Contact createContact = new Contact (
                 AccountId = createAccountTestSite.id,
                 LastName = 'portalTestUserv1',
                 MiddleName = 'm',
                 FirstName = 'testFirst',
                 Email = 'testportalTestUserv1@gmail.com',
                 Birthdate = Date.newInstance(1994, 5, 5),
                 Gender__c = 'Male',
                 MobilePhone = '(702)379-44151',
                 Phone = '(702)379-4415',
                 Street_Address1__c ='testStreet1',
                 Street_Address_2__c ='testStreet2',
                 City__c = 'TestCity',
                 State__c = 'OH',
                 ZIP__c = '12311',
                 Testing_Site__c = createAccountTestSite.Id,
                 RecordTypeId = testingSiteId,
                 Preferred_Date__c = Date.today(),
                 Preferred_Time__c = Time.newInstance(16, 0, 0, 0)
             );
             insert createContact;
             
             VTS_Event__c vtsEvent = new VTS_Event__c();
             vtsEvent.Location__c = createAccountTestSite.Id;
             insert vtsEvent;
            // okpcLoginController.createAppointment(vtsEvent.Id,adminUser.userName);
        }     

       
        String s = '';
          try{
             s = okpcLoginController.login('abc@gmail.com','lo9(ki8*ju','https://test.salesforce.com/','.citizen',null);
          }catch(exception e){
             s = null;
          }        
        System.assert(s == null);

    }
    public static testMethod void test1(){
        Boolean result = okpcLoginController.getIsUsernamePasswordEnabled();
        system.assertEquals(true, result,'test1');
    }
    public static testMethod void test2(){
        Boolean result = okpcLoginController.getIsSelfRegistrationEnabled();
        system.assertEquals(false, result,'test2');
    }
    public static testMethod void test3(){
        String s = okpcLoginController.getSelfRegistrationUrl();
        System.assert(s == null);

    }
    public static testMethod void test4(){
        String result = okpcLoginController.getForgotPasswordUrl();
        system.assertEquals(false, String.isNotBlank(result), 'test4');
    }
    public static testMethod void test5(){
       	okpcLoginController.getAuthConfig();
    }
    public static testMethod void test6(){
        okpcLoginController.setExperienceId('test');
        String result = okpcLoginController.setExperienceId(null);
        system.assertEquals(false, String.isNotBlank(result), 'test6');
    }   
    
    private static testMethod void communityLoginTest(){
        //try{
        User adminUser = [Select Id, UserRoleId,email,firstname,username From User Where Profile.Name='System Administrator' AND UserRoleID != NULL AND IsActive = TRUE Limit 1];
        system.runAs(adminUser){
            String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
            Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
            createAccountTestSite.BillingCity = 'testCity';
            createAccountTestSite.BillingCountry = 'testCountry';
            createAccountTestSite.BillingState = 'testState';
            createAccountTestSite.BillingStreet = 'testStreet';
            createAccountTestSite.BillingPostalCode = '12345';
            createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
            createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
            insert createAccountTestSite;
            
            Id citizenConsultationVitalRecords_RT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen').getRecordTypeId();
            String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
            Contact dummyContactTest = TestDataFactoryContact.createCitizenContact(false , 'TestLastName','TestFirstName');
            /*
            createContact.Notify_Patients__c = true;
            createContact.AccountId=createAccountTestSite.id;
            createContact.Opt_out_for_SMS_Updates__c = false;
            createContact.recordtypeID=citizenConsultationVitalRecords_RT;
            insert createContact;*/
            Contact createContact = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0)
        );
        insert createContact;

            
            contact[] createContact2=[select id,email,firstName,lastName,phone from contact where lastname='portalTestUserv1' limit 1];
            
            string profileId=[select id,name from profile where name='Covid Citizen User' limit 1].id;
           // string roleId=[select id,name from userrole where name='CEO' limit 1].id;
            string contactType=System.Label.OKSF_Registration_Covid_Citizen;
            
            String newUsername=createContact2[0].email+'.dev';
            user u=new user();
            u.Username = createContact2[0].email; //Modified by Sajal
            u.email = createContact2[0].email;
            u.ProfileId = profileId;
            u.IsActive = true;
          //  u.UserRoleId=roleId;
            u.FirstName = createContact2[0].FirstName;
            u.LastName = createContact2[0].LastName;
            u.Password__c = 'test1233@N';
            u.Phone = createContact2[0].phone;
            u.ContactId = createContact2[0].Id;
            u.emailencodingkey = 'UTF-8';
            u.languagelocalekey = 'en_US';
            u.localesidkey = 'en_US';
            u.timezonesidkey = 'America/Los_Angeles';
            u.alias = createContact2[0].FirstName.substring(0, 3);
    
            String nickname = ((newUsername != null && newUsername.length() > 0) ? newUsername.substring(0, 1) : '');
            nickname += String.valueOf(Crypto.getRandomInteger()).substring(1, 7);
            u.put('CommunityNickname', nickname);
            insert u;
            
            string communityResult=okpcLoginController.communityLogin(u.FirstName, u.email, 'dev');
            system.assertEquals(false, String.isNotBlank(communityResult), 'communityLoginTest');
        
        }
      //} catch(system.Exception exp){
			//system.debug('exp============'+exp.getMessage());            
      //}
        
    }


    @IsTest
    static void testTVRSLogin_Case1(){
        //'abc@gmail.com','lo9(ki8*ju','https://test.salesforce.com/','.citizen',null

        String res = '{"username":"TestDemoUser","password":"lo9(ki8*ju","startUrl":"","eventId":"","delimiter":".citizen" }';

        Test.startTest();
        
            try {
                okpcLoginController.tvrsLogin(res);
            }
            catch(Exception err) {
                System.assert(true,err.getMessage()+err.getStackTraceString());
            }
        Test.stopTest();
    
}


@IsTest
static void testTVRSLogin_Case2(){
    //'abc@gmail.com','lo9(ki8*ju','https://test.salesforce.com/','.citizen',null

    String res = '{"username":"TestDemoUser","password":"lo9(ki8*ju","startUrl":"","eventId":"","delimiter":".citizen" }';
    String bres = '';
    Test.startTest();
    
        try {
            okpcLoginController.tvrsLogin(bres);
        }
        catch(Exception err) {
            System.assert(true,err.getMessage()+err.getStackTraceString());
        }
    Test.stopTest();

}


}