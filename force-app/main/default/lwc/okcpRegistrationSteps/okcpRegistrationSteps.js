/**
 * @description       : 
 * @author            : Balram Dhawan
 * @group             : 
 * @last modified on  : 01-09-2022
 * @last modified by  : Balram Dhawan
**/
import { LightningElement, api, track } from 'lwc';
import { fireEvent, sharedData } from "c/pubsub";

export default class OkcpFlowSteps extends LightningElement {
    @track onLoadStep;
    @track isNewEntity;
    @track activeCss = 'event active';
    @track completedCss = 'event completed';
    @track step1CSS;
    @track step2CSS;
    @track step3CSS;
    @track step4CSS;
    @track step5CSS;
    @track step6CSS;
    @track step7CSS;
    @track step8CSS;
    @track step9CSS;

    @api originalStep = 0;
    @api isVaccination = false;
    @api isLicensedUser = false;
    @api isProvider = false;
    @api hideStep = false;
    @api areInitialStepsOver = false;

    step1ariaLabel;
    step2ariaLabel;
    step3ariaLabel;
    step4ariaLabel;
    step4ariaLabel;
    step5ariaLabel;
    step6ariaLabel;
    step6ariaLabel;
    step7ariaLabel;
    step4ariaLabelVacc;

    step1tabIndex;
    step2tabIndex;
    step3tabIndex;
    step4tabIndex;
    step4tabIndex;
    step5tabIndex;
    step6tabIndex;
    step6tabIndex;
    step7tabIndex;

    connectedCallback() {
        this.onLoadStep = this.currentStep;
        // this.isNewEntity = sharedData.isNewEntity;
    }

    @api
    get currentStep() {
        return this.currentStep2;
    }
    set currentStep(value) {
        this.currentStep2 = value;
        var _currentStep = parseInt(value);
        this.step1CSS = 'event ' + (_currentStep === 1 ? 'active ' : '') + (_currentStep > 1 || this.originalStep >= 1 ? ' completed' : '');
        this.step2CSS = 'event ' + (_currentStep === 2 ? 'active ' : '') + (_currentStep > 2 || this.originalStep >= 2 ? ' completed' : '');
        this.step3CSS = 'event ' + (_currentStep === 3 ? 'active ' : '') + (_currentStep > 3 || this.originalStep >= 3 ? ' completed' : '');
        this.step4CSS = 'event ' + (_currentStep === 4 ? 'active ' : '') + (_currentStep > 4 || this.originalStep >= 4 ? ' completed' : '');
        this.step5CSS = 'event ' + (_currentStep === 5 ? 'active ' : '') + (_currentStep > 5 || this.originalStep >= 5 ? ' completed' : '');
        this.step6CSS = 'event ' + (_currentStep === 6 ? 'active ' : '') + (_currentStep > 6 || this.originalStep >= 6 ? ' completed' : '');
        this.step7CSS = 'event ' + (_currentStep === 7 ? 'active ' : '') + (_currentStep > 7 || this.originalStep >= 7 ? ' completed' : '');
        this.step8CSS = 'event ' + (_currentStep === 8 ? 'active ' : '') + (_currentStep > 8 || this.originalStep >= 8 ? ' completed' : '');
        this.step9CSS = 'event ' + (_currentStep === 9 ? 'active ' : '') + (_currentStep > 9 || this.originalStep >= 9 ? ' completed' : '');

        // this.step1ariaLabel = 'Consent '+ (_currentStep > 1 ? 'Completed' : (_currentStep > 1 ? ' Current ' : ''));

        this.step1ariaLabel = 'Consent' + (_currentStep > 1 ? ' Completed ' : (_currentStep === 1 ? ' Current Step ' : ''));
        this.step2ariaLabel = 'Attendee Personal Information' + (_currentStep > 2 ? ' Completed ' : (_currentStep === 2 ? ' Current Step ' : ''));
        this.step3ariaLabel = 'Search Event' + (_currentStep > 3 ? ' Completed ' : (_currentStep === 3 ? ' Current Step ' : ''));
        this.step4ariaLabel = 'Testing Questions' + (_currentStep > 4 ? ' Completed ' : (_currentStep === 4 ? ' Current Step ' : ''));
        this.step4ariaLabelVacc = 'Vaccine Questions' + (_currentStep > 4 ? ' Completed ' : (_currentStep === 4 ? ' Current Step ' : ''));
        this.step5ariaLabel = 'Demographics' + (_currentStep > 5 ? ' Completed ' : (_currentStep === 5 ? ' Current Step ' : ''));
        this.step6ariaLabel = 'Waiver' + (_currentStep > 6 ? ' Completed ' : (_currentStep === 6 ? ' Current Step ' : ''));
        this.step7ariaLabel = 'Schedule' + (_currentStep > 7 ? ' Completed ' : (_currentStep === 8 ? ' Current Step ' : ''));

        this.step1tabIndex = (_currentStep > 1 ? '0' : '-1');
        this.step2tabIndex = (_currentStep > 2 ? '0' : '-1');
        this.step3tabIndex = (_currentStep > 3 ? '0' : '-1');
        this.step4tabIndex = (_currentStep > 4 ? '0' : '-1');
        this.step4tabIndex = (_currentStep > 4 ? '0' : '-1');
        this.step5tabIndex = (_currentStep > 5 ? '0' : '-1');
        this.step6tabIndex = (_currentStep > 6 ? '0' : '-1');
        this.step6tabIndex = (_currentStep > 6 ? '0' : '-1');
        this.step7tabIndex = (_currentStep > 7 ? '0' : '-1');


        if (value > 2) {
            this.areInitialStepsOver = true;
        } else {
            this.areInitialStepsOver = false;
        }
    }

    @api
    handleCurrentStep(value) {
        this.currentStep2 = value;
        var _currentStep = parseInt(value);
        this.step1CSS = 'event ' + (_currentStep === 1 ? 'active ' : '') + (_currentStep > 1 || this.originalStep >= 1 ? ' completed' : '');
        this.step2CSS = 'event ' + (_currentStep === 2 ? 'active ' : '') + (_currentStep > 2 || this.originalStep >= 2 ? ' completed' : '');
        this.step3CSS = 'event ' + (_currentStep === 3 ? 'active ' : '') + (_currentStep > 3 || this.originalStep >= 3 ? ' completed' : '');
        this.step4CSS = 'event ' + (_currentStep === 4 ? 'active ' : '') + (_currentStep > 4 || this.originalStep >= 4 ? ' completed' : '');
        this.step5CSS = 'event ' + (_currentStep === 5 ? 'active ' : '') + (_currentStep > 5 || this.originalStep >= 5 ? ' completed' : '');
        this.step6CSS = 'event ' + (_currentStep === 6 ? 'active ' : '') + (_currentStep > 6 || this.originalStep >= 6 ? ' completed' : '');
        this.step7CSS = 'event ' + (_currentStep === 7 ? 'active ' : '') + (_currentStep > 7 || this.originalStep >= 7 ? ' completed' : '');
        this.step8CSS = 'event ' + (_currentStep === 8 ? 'active ' : '') + (_currentStep > 8 || this.originalStep >= 8 ? ' completed' : '');
        this.step9CSS = 'event ' + (_currentStep === 9 ? 'active ' : '') + (_currentStep > 9 || this.originalStep >= 9 ? ' completed' : '');
    }

    @api
    handlePaidValue(value) {
        this.isNewEntity = this.isNewEntity || value;
    }

    //Commented By Virendra Kumar as per S-16828
    handleUserClicks(event) {
        let _currentStep = (event.target.value === 7)?8:event.target.value;
        if (_currentStep <= this.originalStep) {
            fireEvent(this.pageRef, 'handleSideBarClick', _currentStep);
        }
    }
}