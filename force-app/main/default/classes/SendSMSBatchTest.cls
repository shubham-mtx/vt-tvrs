@isTest
public class SendSMSBatchTest {
    static testMethod void testMethod1(){
         String LabSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
         String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
         Id conRT_consultation_portal = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
    
         
        Account acc2 = TestDataFactory.createAccount('Testaccount123',false);
        acc2.Communication__c = 'Vital Records';
        insert acc2;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, true);
        Account createAccountLabSite = TestDataFactoryAccount.createAccount('LabSite', LabSite, true);

        Contact createContact2 = TestDataFactory.createContact('TestLastName123','TestFirstName321',false);
        createContact2.Birthdate = System.today().addYears(-20);
        createContact2.RecordTypeId = conRT_consultation_portal;
        createContact2.PHOCIS_Response_Count__c = '2';
        createContact2.Notify_Patients__c = true;
        createContact2.Opt_out_for_SMS_Updates__c = false;
        createContact2.AccountId = acc2.Id;
        createContact2.Testing_Site__c = createAccountTestSite.Id;
        insert createContact2;
      
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact2.Id;
        createAppointment.Status__c='';
       // createAppointment.Testing_Site__c = createAccountTestSite.id;
        
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        createAppointment.Status__c='Scheduled';
        update createAppointment;
        Test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        TwilioSendSMS.sendSms('123456789', 'appointmentfollowup');
        Test.stopTest();
        
    }
    static testMethod void testMethod2(){
        
        String smsbody = 'appointment';
        String query = 'SELECT Id,Name,Account.Communication__c,MobilePhone,Testing_Site__c,Patient_Id__c FROM Contact ';
        String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        insert createAccountTestSite;
        
        List<Id> contactList = new List<Id>();
        Contact createContact = TestDataFactoryContact.createContact(false , 'TestLastName','TestFirstName');
        createContact.Testing_Site__c = createAccountTestSite.Id;
        String citizenRecordTypeId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        createContact.RecordTypeId = citizenRecordTypeId;
        insert createContact;
        
        Appointment__c  createAppointment1 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment1.Patient__c = createContact.Id;
        createAppointment1.Status__c='';
        createAppointment1.Lab_Center__c = createAccountTestSite.id;
        insert createAppointment1;
        
        Appointment__c fetchAppointment = [SELECT Id,Name FROM Appointment__c WHERE Patient__c =: createContact.id LIMIT 1 ];
        contactList.add(createContact.id);
        
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createSlot.Account__c = createAccountTestSite.id;
        createSlot.Start_Time__c = time.newInstance(7, 0, 0, 0);
        createSlot.End_Time__c = time.newInstance(8, 8, 8, 8);
        insert createSlot;
        
        fetchAppointment.Appointment_Slot__c = createSlot.id;
        update fetchAppointment;
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        
        test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        SendSMSBatch smsBatchtest = new SendSMSBatch(query, contactList, smsbody);
        Database.executeBatch(smsBatchtest, 1);           
        Test.stopTest();
        
    }
    
    static testMethod void testMethod2_appointmentfollowup(){
        
        String smsbody = 'appointmentfollowup';
        String query = 'SELECT Id,Name,Account.Communication__c,MobilePhone,Testing_Site__c,Patient_Id__c FROM Contact ';
        String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        insert createAccountTestSite;
        
        List<Id> contactList = new List<Id>();
        Contact createContact = TestDataFactoryContact.createContact(false , 'TestLastName','TestFirstName');
        createContact.Testing_Site__c = createAccountTestSite.Id;
        String citizenRecordTypeId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        createContact.RecordTypeId = citizenRecordTypeId;
        insert createContact;
        
        Appointment__c  createAppointment1 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment1.Patient__c = createContact.Id;
        createAppointment1.Status__c='';
        createAppointment1.Lab_Center__c = createAccountTestSite.id;
        insert createAppointment1;
        
        Appointment__c fetchAppointment = [SELECT Id,Name FROM Appointment__c WHERE Patient__c =: createContact.id LIMIT 1 ];
        contactList.add(createContact.id);
        
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createSlot.Account__c = createAccountTestSite.id;
        createSlot.Start_Time__c = time.newInstance(7, 0, 0, 0);
        createSlot.End_Time__c = time.newInstance(8, 8, 8, 8);
        insert createSlot;
        
        fetchAppointment.Appointment_Slot__c = createSlot.id;
        update fetchAppointment;
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        
        test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        SendSMSBatch smsBatchtest = new SendSMSBatch(query, contactList, smsbody);
        Database.executeBatch(smsBatchtest, 1);   
        List<Id> conList = new List<Id>();
        conList.add(createAppointment.Id);
        SendSMSBatch.executeBatchMethod(conList);        
        Test.stopTest();
        
    }
    static testMethod void testAntibody(){
         Id contactCitizen_COVIDTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId(); 
        Account acc2 = TestDataFactory.createAccount('Testaccount123',false);
        acc2.Communication__c = 'OSDH Antibody';
        insert acc2;
        Contact createContact2 = TestDataFactory.createContact('TestLastName123','TestFirstName321',false);
        createContact2.Birthdate = System.today().addYears(-20);
        createContact2.RecordTypeId = contactCitizen_COVIDTypeId;
        createContact2.PHOCIS_Response_Count__c = '2';
        createContact2.Notify_Patients__c = true;
        createContact2.Opt_out_for_SMS_Updates__c = false;
        createContact2.AccountId = acc2.Id;
        insert createContact2;
      
        
        Test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        TwilioSendSMS.sendSms('123456789', 'Contact');
        Antibody_Testing__c test_11 = TestDataFactory.createTesting('TestInsert',false);
        test_11.Results__c = 'Positive';
        test_11.Patient__c = createContact2.Id;
        test_11.Result_Upload_Date__c = Date.today();
        insert test_11;
        Test.stopTest();
        
    }
    static testMethod void testAppointment(){
        
         String LabSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
         String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
         Id contactCitizen_COVIDTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId(); 
        Account acc2 = TestDataFactory.createAccount('Testaccount123',false);
        acc2.Communication__c = 'OSDH Antibody';
        insert acc2;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, true);
        Account createAccountLabSite = TestDataFactoryAccount.createAccount('LabSite', LabSite, true);
        Contact createContact2 = TestDataFactory.createContact('TestLastName123','TestFirstName321',false);
        createContact2.Birthdate = System.today().addYears(-20);
        createContact2.RecordTypeId = contactCitizen_COVIDTypeId;
        createContact2.PHOCIS_Response_Count__c = '2';
        createContact2.Notify_Patients__c = true;
        createContact2.Opt_out_for_SMS_Updates__c = false;
        createContact2.AccountId = acc2.Id;
        createContact2.Testing_Site__c = createAccountTestSite.Id;
        insert createContact2;
      
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact2.Id;
        createAppointment.Status__c='';
       // createAppointment.Testing_Site__c = createAccountTestSite.id;
        
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        createAppointment.Status__c='Scheduled';
        update createAppointment;
        Test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        TwilioSendSMS.sendSms('123456789', 'appointment');
        Test.stopTest();
        
    }
    static testMethod void testMethod3(){
        
        String smsbody = 'TestingSiteLocation';
        String query = 'SELECT Id,Name,Account.Communication__c,MobilePhone,Testing_Site__c,Patient_Id__c FROM Contact ';
        //String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Citizen').RecordTypeId;
        String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
         createAccountTestSite.communication__c  = 'OKU COVID';
        insert createAccountTestSite;
        
        List<Id> contactList = new List<Id>();
        Contact createContact = TestDataFactoryContact.createContact(false , 'TestLastName','TestFirstName');
        createContact.AccountId = createAccountTestSite.Id;
        createContact.Testing_Site__c = createAccountTestSite.Id;
        String citizenRecordTypeId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        createContact.RecordTypeId = citizenRecordTypeId;
        insert createContact;
        
        Appointment__c  createAppointment1 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment1.Patient__c = createContact.Id;
        createAppointment1.Status__c='';
        createAppointment1.Lab_Center__c = createAccountTestSite.id;
        insert createAppointment1;
        
        Appointment__c fetchAppointment = [SELECT Id,Name FROM Appointment__c WHERE Patient__c =: createContact.id LIMIT 1 ];
        contactList.add(createContact.id);
        
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createSlot.Account__c = createAccountTestSite.id;
        createSlot.Start_Time__c = time.newInstance(7, 0, 0, 0);
        createSlot.End_Time__c = time.newInstance(8, 8, 8, 8);
        insert createSlot;
        
        fetchAppointment.Appointment_Slot__c = createSlot.id;
        update fetchAppointment;
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        
        test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        SendSMSBatch smsBatchtest = new SendSMSBatch(query, contactList, smsbody);
        Database.executeBatch(smsBatchtest, 1);           
        Test.stopTest();
        
    }
    
    static testMethod void testMethod3_CotactBody(){
        
        String smsbody = 'Contact';
        String query = 'SELECT Id,Name,Account.Communication__c,MobilePhone,Testing_Site__c,Patient_Id__c FROM Contact ';
        String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
         createAccountTestSite.communication__c  = 'OKU COVID';
        insert createAccountTestSite;
        
        List<Id> contactList = new List<Id>();
        Contact createContact = TestDataFactoryContact.createContact(false , 'TestLastName','TestFirstName');
        createContact.AccountId = createAccountTestSite.Id;
        createContact.Testing_Site__c = createAccountTestSite.Id;
        String citizenRecordTypeId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        createContact.RecordTypeId = citizenRecordTypeId;
        insert createContact;
        
        Appointment__c  createAppointment1 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment1.Patient__c = createContact.Id;
        createAppointment1.Status__c='';
        createAppointment1.Lab_Center__c = createAccountTestSite.id;
        insert createAppointment1;
        
        Appointment__c fetchAppointment = [SELECT Id,Name FROM Appointment__c WHERE Patient__c =: createContact.id LIMIT 1 ];
        contactList.add(createContact.id);
        
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createSlot.Account__c = createAccountTestSite.id;
        createSlot.Start_Time__c = time.newInstance(7, 0, 0, 0);
        createSlot.End_Time__c = time.newInstance(8, 8, 8, 8);
        insert createSlot;
        
        fetchAppointment.Appointment_Slot__c = createSlot.id;
        update fetchAppointment;
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        
        test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        SendSMSBatch smsBatchtest = new SendSMSBatch(query, contactList, smsbody);
        Database.executeBatch(smsBatchtest, 1);           
        Test.stopTest();
        
    }
    
    static testMethod void smsCancelTest(){
        
        String smsbody = 'cancel';
        String query = 'SELECT Id,Name,Account.Communication__c,MobilePhone,Testing_Site__c,Patient_Id__c FROM Contact ';
        String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
      //   createAccountTestSite.communication__c  = 'Vital Records';
        insert createAccountTestSite;
        
        List<Id> contactList = new List<Id>();
        Contact createContact = TestDataFactoryContact.createContact(false , 'TestLastName','TestFirstName');
        createContact.AccountId = createAccountTestSite.Id;
        createContact.Testing_Site__c = createAccountTestSite.Id;
        ID conRT_citizen_portal = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        createContact.RecordTypeId = conRT_citizen_portal;
        insert createContact;
        
        Appointment__c  createAppointment1 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment1.Patient__c = createContact.Id;
        createAppointment1.Status__c='';
        createAppointment1.Lab_Center__c = createAccountTestSite.id;
        insert createAppointment1;
       
        
        Appointment__c fetchAppointment = [SELECT Id,Name FROM Appointment__c WHERE Patient__c =: createContact.id LIMIT 1 ];
        contactList.add(createContact.id);
        
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createSlot.Account__c = createAccountTestSite.id;
        createSlot.Start_Time__c = time.newInstance(7, 0, 0, 0);
        createSlot.End_Time__c = time.newInstance(8, 8, 8, 8);
        insert createSlot;
        
        fetchAppointment.Appointment_Slot__c = createSlot.id;
        update fetchAppointment;
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        
        test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        SendSMSBatch smsBatchtest = new SendSMSBatch(query, contactList, smsbody);
        Database.executeBatch(smsBatchtest, 1);           
        Test.stopTest();
        
    }
    static testMethod void smsremainderTest(){
        
        String smsbody = 'remainder';
        String query = 'SELECT Id,Name,Account.Communication__c,MobilePhone,Testing_Site__c,Patient_Id__c FROM Contact ';
        String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        // createAccountTestSite.communication__c  = 'Vital Records';
        insert createAccountTestSite;
        
        List<Id> contactList = new List<Id>();
        Contact createContact = TestDataFactoryContact.createContact(false , 'TestLastName','TestFirstName');
        createContact.AccountId = createAccountTestSite.Id;
        createContact.Testing_Site__c = createAccountTestSite.Id;
        ID conRT_citizen_portal = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        createContact.RecordTypeId = conRT_citizen_portal;
        insert createContact;
        
        Appointment__c  createAppointment1 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment1.Patient__c = createContact.Id;
        createAppointment1.Status__c='';
        createAppointment1.Lab_Center__c = createAccountTestSite.id;
        insert createAppointment1;
        
        Appointment__c fetchAppointment = [SELECT Id,Name FROM Appointment__c WHERE Patient__c =: createContact.id LIMIT 1 ];
        contactList.add(createContact.id);
        
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createSlot.Account__c = createAccountTestSite.id;
        createSlot.Start_Time__c = time.newInstance(7, 0, 0, 0);
        createSlot.End_Time__c = time.newInstance(8, 8, 8, 8);
        insert createSlot;
        
        fetchAppointment.Appointment_Slot__c = createSlot.id;
        update fetchAppointment;
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        
        test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        SendSMSBatch smsBatchtest = new SendSMSBatch(query, contactList, smsbody);
        Database.executeBatch(smsBatchtest, 1);           
        Test.stopTest();
        
    }
    
}