/**
 * @description       : 
 * @author            : Wasef Mohiuddin
 * @group             : 
 * @last modified on  : 02-17-2021
 * @last modified by  : Wasef Mohiuddin
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   02-16-2021   Wasef Mohiuddin   Initial Version
**/
global with sharing class VT_TS_PreRegPendingVerificationToAssign implements Database.Batchable<sobject>, Schedulable{

    global Database.Querylocator start(Database.BatchableContext bc){
        String soqlQuery = 'SELECT Id, Name, CreatedDate,Early_Group1_Health__c, Early_Group1_Health__r.Verification_Delay__c ';
        soqlQuery += 'FROM Pre_Registration__c WHERE Status__c = \'Pending Verification\' AND Chronic_Condition__c != \'Rejected\'';
        return Database.getQueryLocator(soqlQuery);
    }

    global void execute(Database.BatchableContext bc, List<Pre_Registration__c> scope){

        List<Pre_Registration__c> regList = new List<Pre_Registration__c>();

        for(Pre_Registration__c reg: scope){
            if(reg.Early_Group1_Health__c != null){
                Datetime delayedTime = reg.CreatedDate.addHours((Integer)reg.Early_Group1_Health__r.Verification_Delay__c);
                if( delayedTime <= Datetime.now() ){
                    reg.Status__c = 'Assigned';
                    regList.add(reg);
                }
            }
            
        }

        if(!regList.isEmpty()){
            update regList;
        }
    }

    global void execute(SchedulableContext ctx) {
        VT_TS_PreRegPendingVerificationToAssign batch = new VT_TS_PreRegPendingVerificationToAssign();
        Database.executeBatch(batch, 200);

        /*
        Cron Expression: To schedule job for every one hour starting from execution
        VT_TS_PreRegPendingVerificationToAssign sc = new VT_TS_PreRegPendingVerificationToAssign();
        String cronExp = '0 0 * * * ?';
        String jobID = System.schedule('Hourly Scheduled Job to update PreRegPendingVerificationToAssign', cronExp, sc);
        */
    }

    global void finish(Database.BatchableContext bc){ 
    }
}