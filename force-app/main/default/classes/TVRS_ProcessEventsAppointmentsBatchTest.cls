@IsTest
private with sharing class TVRS_ProcessEventsAppointmentsBatchTest {
    @IsTest
    static void testBatch(){
        Contact c = new Contact();
        c.FirstName = 'Test--';
        c.LastName = '--Last';
        c.Email = 'test--last@email.com';
        c.Birthdate = Date.newInstance(1995, 07, 23);
        insert c;
        
        Account a = new Account();
        a.Name = 'Testing Site';
        a.BillingStreet = '61  Saint Marys Avenue';
        a.BillingCity = 'Liverpool';
        a.BillingState = 'NY';
        a.BillingPostalCode = '13088';
        a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Test_Center').getRecordTypeId();
        insert a;
        
        VTS_Event__c event = new VTS_Event__c();
        event.Status__c = 'Canceled';
        event.Start_Date__c = Date.today();
        event.End_Date__c = Date.today();
        event.Description__c = 'Test Description';
        event.Location__c = a.Id;
        insert event;

        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, a.Id);

        Appointment__c app = new Appointment__c();
        app.Status__c = 'Scheduled';
        app.Patient__c = c.Id;
        app.Event__c = event.Id;
        app.Appointment_Slot__c = createSlot.Id;
        insert app;

        Test.startTest();
            Database.executeBatch(new VT_TVRS_ProcessEventsAppointmentsBatch(new Set<Id>{event.Id},new Set<Id>(), new Map<Id, VTS_Event__c>{event.Id => event}));
        Test.stopTest();
        
    }
}