({
    doInit: function (component, event, helper) {
        var loginlabel = $A.get("$Label.c.OKTC_Login_URL");
        component.set("v.loginURL", loginlabel);
        var registrationlabel = $A.get("$Label.c.OKTC_Registration_URL");
        component.set("v.registrationURL", registrationlabel);
        var supportlabel = $A.get("$Label.c.oktcSupportURL");
        component.set("v.supportURL", supportlabel);

        var action = component.get("c.getContactAccountRelations");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var userDetails = response.getReturnValue().UserInfo;
                var optionsTestingSite = response.getReturnValue().TestingSites;
                var primaryTestingSite = response.getReturnValue().primary;
                component.set("v.testingSites",optionsTestingSite);
                component.set("v.selectedTestingSiteId",primaryTestingSite);
                component.set("v.userName", userDetails.Name);
                component.set("v.imageResource", userDetails.SmallPhotoUrl);
            }else{
               
            }
        });
        $A.enqueueAction(action);
    },

    updateTestingSite : function(component, event, helper){
        var value = event.getParam("value");
        
        var action = component.get("c.updatePrimary");
        action.setParams({'selectedId':value });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                window.location.reload();
            }else{
               
            }
        });
        $A.enqueueAction(action);
    },

    redirectToLogin: function (component, event, helper)  {
        var logoutRedirectURl = $A.get("$Label.c.OKTC_Logout_Redirect_URL");
        window.location.replace(logoutRedirectURl);
    }
})