/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 12-06-2021
 * @last modified by  : Mohit Karani
**/
public without sharing class OKPC_IntakeRequestController {


    /** 
     *@description getAppointmentData
        * @return Object
        * @param AppointmentId
    */
   
    @AuraEnabled
    public static Object getAppointmentData(String AppointmentId) {
      try{

        //added for security check by Ajit Kumar on 10th june 2021
          VTTS_AppointmentHelper.checkUserAccess(null, appointmentId);

          if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){ 
            User loggedInUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
            Id contId;
            if(loggedInUser == null) {
                return null;
            } 
            contId = loggedInUser.ContactId;
            DataWrapper dataWrapperObj = new DataWrapper();
            dataWrapperObj.contactId = contId;

            List<Appointment__c> existingAppointmentList = new List<Appointment__c>();
            existingAppointmentList = [SELECT Id, Appointment_Date__c, Start_Time__c, Testing_Site__c, End_Time__c,
            Patient__r.Testing_Site__r.Name , Status__c,Patient__r.Preferred_Date__c,Patient__r.Preferred_Time__c,Symptomatic__c 
            FROM Appointment__c 
            WHERE Id =: AppointmentId WITH SECURITY_ENFORCED LIMIT 1];
            if(existingAppointmentList.size() > 0) {
                dataWrapperObj.appointmentId = existingAppointmentList[0].Id;
                dataWrapperObj.appointmentObj = new AppointmentWrapper(existingAppointmentList[0]);
            }
            
            return dataWrapperObj;
          }else{
            throw new VT_Exception('You are not Authorized to perform this action');
          }
            

      }catch(Exception ex){
        return ex.getMessage();
      }
        
    }

    /** 
     * @description getAppointmentDataForContact
     * @return Object
     * @param AppointmentId
     * @param eventId
    */

    @AuraEnabled
    public static Object getAppointmentDataForContact(String contactId, String eventId) {
        try{
            //added for security check by Ajit Kumar on 10th june 2021
            VTTS_AppointmentHelper.checkUserAccess(contactId, null);
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
            
                Id contId;
                DataWrapper dataWrapperObj = new DataWrapper();

                if(String.isNotBlank(contactId)) {
                    contId = Id.valueOf(contactId);
                } else {
                    return null;
                }
                dataWrapperObj.contactId = contId;

                List<Appointment__c> existingAppointmentList = new List<Appointment__c>();
                existingAppointmentList = [SELECT Id, Appointment_Date__c, Start_Time__c, Testing_Site__c,End_Time__c,
                                                Patient__r.Testing_Site__r.Name , Status__c,Patient__r.Preferred_Date__c,
                                                Patient__r.Preferred_Time__c,Symptomatic__c 
                                                FROM Appointment__c 
                                                WHERE Patient__c =: contId AND Event__c =:eventId AND Status__c = 'Draft' WITH SECURITY_ENFORCED 
                                                ORDER BY LastModifiedDate DESC LIMIT 1];
                if(existingAppointmentList.size() > 0) {
                    dataWrapperObj.appointmentId = existingAppointmentList[0].Id;
                    dataWrapperObj.appointmentObj = new AppointmentWrapper(existingAppointmentList[0]);
                }
                
                return dataWrapperObj;
                
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            return ex.getMessage();
        }
        
    }

    /** 
     * @description saveAppointMentData
     * @return Object
     * @param JsonData
     * @param appointMentId
    */
	@AuraEnabled 
    public static Object saveAppointMentData(String JsonData,String appointMentId){
        try{
            //added for security check by Ajit Kumar on 10th june 2021
            VTTS_AppointmentHelper.checkUserAccess(null, appointMentId);
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){ 
                AppointmentWrapper appWrapper = (AppointmentWrapper) JSON.deserialize(JsonData, AppointmentWrapper.class);
                Appointment__c appObj = new Appointment__c();
                appObj.Id = appWrapper.appointmentId;
                appObj.Symptomatic__c = appWrapper.symptomatic;

                //added for security check by Ajit Kumar on 10th june 2021
                appObj = (Appointment__c)VT_SecurityLibrary.getAccessibleData('Appointment__c', new list<Appointment__c>{appObj}, 'update')[0];
                
                update appObj;

                return getAppointmentData(appointMentId);
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }

        }catch(Exception ex){
            return ex.getMessage();
        }
        
        
    }
    
    public class DataWrapper {
        @AuraEnabled public String contactId;
        @AuraEnabled public String appointmentId;
        @AuraEnabled public AppointmentWrapper appointmentObj;

        public DataWrapper() {
            this.contactId = '';
            this.appointmentId = '';
        }
    }

    public class AppointmentWrapper {
        @AuraEnabled public String appointmentId;
        @AuraEnabled public Date appointmentDate;
        @AuraEnabled public String startTime;
        @AuraEnabled public String endTime;
        @AuraEnabled public String siteName;
        @AuraEnabled public String status;
        @AuraEnabled public Date preferredDate;
        @AuraEnabled public Time preferredTime;
        @AuraEnabled public String preferredSite;
        @AuraEnabled public String preferredSiteId;
		@AuraEnabled public String symptomatic;
        public AppointmentWrapper(Appointment__c appObj) {
            this.appointmentId = appObj.Id;
            this.appointmentDate = appObj.Appointment_Date__c;
            this.startTime = appObj.Start_Time__c;
            this.endTime = appObj.End_Time__c;
            this.siteName = appObj.Patient__r.Testing_Site__r.Name;
            this.status = appObj.Status__c;
            this.preferredDate = appObj.Patient__r.Preferred_Date__c;
            this.preferredTime = appObj.Patient__r.Preferred_Time__c;
            this.preferredSite = appObj.Patient__r.Testing_Site__r.Name;
            this.preferredSite = appObj.Patient__r.Testing_Site__c;
            this.symptomatic = appObj.Symptomatic__c;
        }
    }
    
}