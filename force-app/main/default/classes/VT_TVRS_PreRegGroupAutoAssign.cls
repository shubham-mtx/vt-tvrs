/* 
* Class Name: VT_TVRS_PreRegGroupAutoAssign
* Description: To Automate Update the Pre Reg Date to related Pre Reg Group
* Author : Shubham Dadhich
* Email : shubham.dadhich@mtxb2b.com
*
* Version       Modification        User         Date               Description
* V1            Initial             Shubham D    24-March-2021      Intial Creation of Class   
*/

global class VT_TVRS_PreRegGroupAutoAssign implements  Database.Batchable<sObject>, Database.Stateful, Schedulable{
    /**
     * Author Shubham Dadhich
     * Description: Default start :  Query On Appointment Object only for Second Dose
    */
    global Database.QueryLocator start(Database.BatchableContext batchContext) {
        String query = 'SELECT Id, Pre_Registration_Group__c,Contact__r.Id, Pass_Code_Risk_Group__c, Pass_Code_Health__c, Early_Group_2_Access_Group__c, Chronic_Condition__c, '+
        ' Early_Group1_Health__c, BIPOC__c, Is_Email_Sent__c, Pre_Registration_Group__r.Status__c, Pre_Registration_Group__r.Group_Type__c,Pre_Registration_Group__r.Name, Contact__c, Contact__r.Name, Contact__r.Birthdate, Contact__r.Age__c '+ 
        ' FROM Pre_Registration__c WHERE ((Pre_Registration_Group__r.Group_Type__c = \'Age-Based\') OR Pre_Registration_Group__c = null OR Pre_Registration_Group__r.Group_Type__c = \'Chronic\' OR BIPOC__c = \'Yes\') ' +
        ' AND Contact__c != null AND Contact__r.Birthdate != null WITH SECURITY_ENFORCED';
        return Database.getQueryLocator(query);
    }
    
    /**
     * Author Shubham Dadhich
     * Description: Default execute : To Automate Assigning for Wrong Pre Reg Groups to Correct Pre Reg Groups 
    */
    global void execute(Database.BatchableContext batchContext, List<Pre_Registration__c> preRegList) {

        Map<Contact, Pre_Registration__c> mapOfContactWithPreReg = new Map<Contact, Pre_Registration__c>();
        Set<Id> contactIds = new Set<Id>();
        Set<Id> contactIdsOfCompletedAppointments = new Set<Id>();
        Map<String, Object> bipocPassCodeMap = (Map<String, Object>) JSON.deserializeUntyped(Label.BIPOC_PASSCODE);
        for(Pre_Registration__c preRegRecord : preRegList){
            contactIds.add(preRegRecord.Contact__c);
        }


        for(Pre_Registration__c preRegRecord : preRegList){
                if(preRegRecord.BIPOC__c == 'Yes' && preRegRecord.Contact__r.Age__c < 18) {
                    preRegRecord.Pass_Code_Risk_Group__c = 'BIPOC'+Integer.valueOf(preRegRecord.Contact__r.Age__c);
                } else if(preRegRecord.BIPOC__c == 'Yes' && preRegRecord.Contact__r.Age__c >= 18) {
                    preRegRecord.Pass_Code_Risk_Group__c = String.valueOf(bipocPassCodeMap.get('18_PLUS'));
                }
                mapOfContactWithPreReg.put(preRegRecord.Contact__r, preRegRecord);
        }

        Map<Contact, Pre_Registration__c> updateMaoOfContactpreReg = new Map<Contact, Pre_Registration__c>();
        updateMaoOfContactpreReg = VT_PreRegAssignmentEngine.processAssignment(mapOfContactWithPreReg);

        UPDATE updateMaoOfContactpreReg.values();
    }

    /**
     * Author Shubham Dadhich
     * Description: Default execute: For Schedule Job
    */
    global void execute(SchedulableContext ctx) {
        VT_TVRS_PreRegGroupAutoAssign batch = new VT_TVRS_PreRegGroupAutoAssign();
        Database.executeBatch(batch, 200);
    }

    /**
     * Author Shubham Dadhich
     * Description: Default finish : 
    */
    global void finish(Database.BatchableContext batchContext) {
        
    }
}