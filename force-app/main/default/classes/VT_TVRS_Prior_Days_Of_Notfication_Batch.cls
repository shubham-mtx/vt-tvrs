/* 
* Class Name: TVRS_Prior_Days_Of_Notfication_Batch
* Description: To send email reminder to the before 1 day and 3 days of Appointment Date.
* Author : kavya konagonda
* Email : kavya.konagonda@mtxb2b.com             
*/

global class VT_TVRS_Prior_Days_Of_Notfication_Batch  implements Database.Batchable<Sobject>,Schedulable{
    
    Integer TVRS_REMINDER_DAYS;
    Integer TVRS_CLASS_RUNN_DAYS;
    
    public VT_TVRS_Prior_Days_Of_Notfication_Batch(){
        //Get Days Configrations for the Reminders
        VT_TS_AppointmentReminderWrapper appointmentReminder = (VT_TS_AppointmentReminderWrapper) JSON.deserialize(System.Label.TVRS_APPOINTMENT_REMINDERS , VT_TS_AppointmentReminderWrapper.class);
        TVRS_REMINDER_DAYS = appointmentReminder.prior_notification_days;
        TVRS_CLASS_RUNN_DAYS = appointmentReminder.day_to_add_for_testing;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        

        String query = 'SELECT Id,Status__c,Email_Workflow_Datetime__c,Email_Flow__c, Event_Process__c,Appointment_Date__c FROM Appointment__c'+
        ' WHERE Status__c = \'Scheduled\' AND Event_Process__c = \'Vaccination\' AND Appointment_Date__c >= Today AND Appointment_Date__c <= NEXT_N_DAYS: ' + (TVRS_REMINDER_DAYS+TVRS_CLASS_RUNN_DAYS);
        
        return Database.getQueryLocator(query);
    }
    
     /**
     * Author Kavya Konagonda
     * Description: Default execute : To Send Email to the patient and Update their field 
    */
    public void execute(Database.BatchableContext BC, List<Appointment__c> scope){
        
        // Varibale Declaration
        Integer priorDays;
        List<Appointment__c>  appointmentList= new List<Appointment__c>();
        
        // Step 1 : Loop on appointmentList and if condition met update Reminders__c
        for(Appointment__c app: scope ){
            
            //Calculate Number of Days Remaing to Send Reminder
            Date runnigDateOfClass = Date.today().addDays(TVRS_CLASS_RUNN_DAYS);
            priorDays= Math.abs(runnigDateOfClass.daysBetween(app.Appointment_Date__c));
            
            //Step 1.1: Check for is Days Remaining to send Reminder
            if(priorDays == 1 || priorDays == TVRS_REMINDER_DAYS){

                app.Email_Workflow_Datetime__c = Datetime.now();
                app.Email_Flow__c = 'Prior Reminder';

                appointmentList.add(app);
            }
            
        }
        // Update List if size() > 0
        if(!appointmentList.isEmpty()){
            VT_TS_SkipTriggerExecution.executeAppointmentTrigger = false;
            update appointmentList;
        }
        
    }
    
    /**
     * Author Kavya Konagonda
     * Description: Default finish
    */
    public void finish(Database.BatchableContext BC){
        
    }
    /**
     * Author Kavya Konagonda
     * Description: Default execute: For Schedule Job
    */
     public void execute(SchedulableContext SC) {

         VT_TVRS_Prior_Days_Of_Notfication_Batch batchClass= new VT_TVRS_Prior_Days_Of_Notfication_Batch();
          Database.executeBatch(batchClass, 200);

        /** Cron Expression: To schedule job for every day
        * VT_TVRS_Prior_Days_Of_Notfication_Batch sc = new VT_TVRS_Prior_Days_Of_Notfication_Batch();
        * String cronExp = '0 0 2 1/1 * ? *';
        * String jobID = System.schedule('Daily Schedule Reminder VT_TVRS_Prior_Days_Of_Notfication_Batch', cronExp, sc);
        **/
        }
   }