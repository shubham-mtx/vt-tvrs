import {
    LightningElement,
    track,
    api
} from 'lwc';
import getBusinessTimeOnAccount from '@salesforce/apex/OkpcContactInformationController.getBusinessTimeOnAccount';
import setBusinessTimeOnAccount from '@salesforce/apex/OkpcContactInformationController.setBusinessTimeOnAccount';
import retrieveDashboardValue from '@salesforce/apex/OkpcContactInformationController.retrieveDashboardValue';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';

export default class OktcDashboard extends LightningElement {
    @track startTime = null;
    @track endTime = null;
    @track siteName = null;
    @track streetAddress = null;
    @track address = null;
    @track endTime = null;
    @track appointmentCounts = [];
    @track loaded = false;
    
    connectedCallback(){
        getBusinessTimeOnAccount()
        .then(result =>{
            if(result != null){
                this.startTime = (result.Business_Start_Time__c != null && typeof result.Business_Start_Time__c != 'undefined')? this.msToTime(result.Business_Start_Time__c) : '';
                this.endTime = (result.Business_End_Time__c != null && typeof result.Business_End_Time__c != 'undefined')? this.msToTime(result.Business_End_Time__c) : '';
                this.siteName = result.Name;
                this.streetAddress = result.BillingStreet;
                let addressString = '';
                if(result.BillingCity != null && typeof result.BillingCity != 'undefined')
                    addressString += result.BillingCity;
                if(result.BillingState != null && typeof result.BillingState != 'undefined')
                    addressString += addressString == '' ? result.BillingState : ', ' + result.BillingState;
                if(result.BillingPostalCode != null && typeof result.BillingPostalCode != 'undefined')
                    addressString += ' ' + result.BillingPostalCode;
                this.address = addressString;
            }
            this.loaded = true;
        })
        .catch(error =>{
            showAsyncErrorMessage(this,error);
        })
        retrieveDashboardValue()
        .then(result =>{
            if(result != null){
                this.appointmentCounts = result.appointmentCounts;
            }
            this.loaded = true;
        })
        .catch(error =>{
            showAsyncErrorMessage(this,error);
        })
    }

    handleInputChange(event) {
        let data = event.detail;
        if (data.label == 'Start Time' ) {
            this.startTime = data.value;
        } else if (data.label == 'End Time' ) {
            this.endTime = data.value;
        }
    }

    handleClick(){
        
        if(this.startTime && this.endTime) {
            if(this.startTime > this.endTime) {
                const event = new ShowToastEvent({
                    title: 'Error!',
                    variant: 'error',
                    message: 'End time must be greater than the start time.',
                });
                this.dispatchEvent(event);
                return;
            }
            this.loaded = false;
            setBusinessTimeOnAccount({startTime: this.startTime, endTime: this.endTime})
            .then(result =>{
                this.loaded = true;
                if(result == 'success') {
                    const event = new ShowToastEvent({
                        title: 'Success!',
                        variant: 'success',
                        message: 'Updated successfully.',
                    });
                    this.dispatchEvent(event);
                }
                else {
                    const event = new ShowToastEvent({
                        title: 'Error!',
                        variant: 'error',
                        message: result,
                    });
                    this.dispatchEvent(event);
                }
            })
            .catch(error =>{
                showAsyncErrorMessage(this,error);
            })
        } else {
            const event = new ShowToastEvent({
                title: 'Error!',
                variant: 'error',
                message: 'Please fill required fields.',
            });
            this.dispatchEvent(event);
        }
    }

    msToTime(s) {
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;

        return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs) + '.' + this.pad(ms, 3);
    }
    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }
}