@isTest
public class oktcContactUploadInternalControllerTest {
     
    @TestSetup
    static void makeData(){ 
        //create Self Registration Account
        Id testCenterRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Test Center').getRecordTypeId();
        Account parentAccount = TestDataFactoryAccount.createAccount('Self Registration', testCenterRecordTypeId, true);

        //Create an Event
        VTS_Event__c event = new VTS_Event__c();
        event.Description__c = 'testDESC';
        event.Status__c = 'Open';
        event.Event_Type__c = 'Public';
		event.End_Date__c = System.today() + 1;
        event.Start_Date__c = System.today();
        event.Start_Time__c = Time.newInstance(11, 0, 0,0 );
        event.End_Time__c = Time.newInstance(12, 00, 0, 0);
        event.Location__c = parentAccount.Id;
        insert event;
        
        //create Contact for community user
        Id testingSiteRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Testing Site').getRecordTypeId();
        Contact loginUserContact = TestDataFactoryContact.createContact(false, 'TestingSite', 'Testuser');
        loginUserContact.RecordTypeId = testingSiteRecordTypeId;
        loginUserContact.AccountId = parentAccount.Id;
        loginUserContact.Email = 'test@test.com';
        insert loginUserContact;

        //create User for running the context as
        Profile pf= [Select Id from profile where Name='Covid Citizen User'];
            User user=new User(firstname = 'ABC',
                          lastName = 'XYZ',
                          email = 'test' + '@test'  + '.org',
                          Username = 'testUserCreation' + '@test1'  + '.org1',
                          EmailEncodingKey = 'ISO-8859-1',
                          Alias = 'testuser',
                          TimeZoneSidKey = 'America/Los_Angeles',
                          LocaleSidKey = 'en_US',
                          LanguageLocaleKey = 'en_US',
                          ProfileId = pf.Id,
                          contactId = loginUserContact.id
                         );
            insert user;
    }

    @IsTest
    static void testGetEventsMethod(){
        User contextUser = [SELECT Id, contactId, Contact.AccountId FROM User WHERE LastName = 'XYZ' LIMIT 1];
        Account acc = [SELECT Id,Name FROM Account Limit 1];
        Test.startTest();
        //System.runAs(contextuser){
            oktcContactUploadInternalController.constructorResultWrapper cow = oktcContactUploadInternalController.getEvents(acc.Id,'Vaccination');
            //system.assertEquals(1, cow.eventWrapperList.size());
            oktcContactUploadInternalController.eventWrapper e = new oktcContactUploadInternalController.eventWrapper('test','test',date.today(),date.today(),'test');
            system.assertEquals(true, e != null, 'testGetEventsMethod');
        //}
        Test.stopTest();
    }
    @IsTest
    static void testBadInputString1(){
        User contextUser = [SELECT Id, contactId, Contact.AccountId FROM User WHERE LastName = 'XYZ' LIMIT 1];
        String inputString = '[{"consent":"true","agreeToRegisterForSomeone":"Yes","firstName":"Shobhit","LastName":"Pant","email":"pantshobhit@gmail.com","Mobile":"8559880111","phoneType":"Mobile","eighteenPlus":"yes","receiveMethod":"E-Mail","receiveViaEmail":"yes","receiveViaSMS":"yes","dateOfBirth":"10/30/1995","streetAddress":"123","City":"test city","State":"vt","Gender":"Male","Zip":"12345","preferredLanguage":"English","Race":"Asian","Ethnicity":"Other","firstCovidTest":"No","employedInHealthCare":"No","symptomatic":"Yes","symptomaticOnsetDate":"08/17/2020","residentInCongregate":"Yes","pregnant":"No","reasonForTesting":"I am worried / concerned about an outbreak in my community","hasInsurance":"Yes","insuredAuthorizedForPayment":"TRUE","patientAuthorizationToReleaseInfo":"TRUE","insuranceType":"Medicare","insuranceIdNumber":"12345","insurancePlanName":"test program","fecaNumber":"123ds","reationToInsured":"Self","insuredFirstName":"Shobhit","insuredMiddleName":"test","insuredLastName":"paant","insuredStreetAddress":"123","insuredCity":"testttt city insured","insuredState":"vt","insuredZip":"12345","insuredTelephone":"1123456432","insuredDOB":"10-30-1995","insuredSex":"Male","anotherHealthPlan":"yes","addInPlanName":"add plan test","addInPolicyGroupNo":"123dsfsdf","addInFirstName":"test name 3","addInMiddleName":"middle name 3","addInLastName":"last name 3","agreetoTheTerms":"TRUE","nameOfParent":"Shobhit","I agree  (Minor State Representative Consent)":"FALSE"},{"consent":""}]';
        VTS_Event__c eventRecord = [SELECT Id FROM VTS_Event__c WHERE Description__c = 'testDESC' LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            InsertContactQueueable.resultWrapper rew = oktcContactUploadInternalController.insertContacts(inputString, eventRecord.Id);
            system.assertEquals(true, rew != null, 'testBadInputString1');
        }
        Test.stopTest();
    }
    @IsTest
    static void testBadInputString2(){
        User contextUser = [SELECT Id, contactId, Contact.AccountId FROM User WHERE LastName = 'XYZ' LIMIT 1];
        String inputString = '[{"agreetoTheTerms":"no","agreeToRegisterForSomeone":"no","consent":"no","City":"test city","dateOfBirth":"10-23-2020","eighteenPlus":"no","email":"pantshobhit@gmail.com","Ethnicity":"brother","firstName":"Test","Gender":"asian","LastName":"Pant","minorStateRepConsent":"no",		"Mobile":"8559880111","nameOfParent":"Shobhit","phoneType":"Mobilephone","preferredLanguage":"English","Race":"Asian","receiveMethod":"E-mall","receiveViaEmail":"no","receiveViaSMS":"no","State":"vermont","streetAddress":"123","Zip":"12345"}]';
      //  String inputString = '[{"consent":"what","agreeToRegisterForSomeone":"sorry","firstName":"Test","LastName":"Pant","email":"pantshobhit@gmail.com","Mobile":"8559880111","phoneType":"Mobilephone","eighteenPlus":"lolno","receiveMethod":"E-mall","receiveViaEmail":"lolno","receiveViaSMS":"lolno","dateOfBirth":"07-19-2020","streetAddress":"123","City":"test city","State":"vermont","Gender":"asian","Zip":"12345","preferredLanguage":"English","Race":"english","Ethnicity":"brother","firstCovidTest":"yesyes","employedInHealthCare":"neo","symptomatic":"yess","symptomaticOnsetDate":"2020/12/12","residentInCongregate":"yess","pregnant":"neo","reasonForTesting":"waise hi","hasInsurance":"neo","insuredAuthorizedForPayment":"qwqw","patientAuthorizationToReleaseInfo":"qwe","insuranceType":"abc","insuranceIdNumber":"12345","insurancePlanName":"test program","fecaNumber":"123ds","reationToInsured":"father","insuredFirstName":"Shobhit","insuredMiddleName":"test","insuredLastName":"paant","insuredStreetAddress":"123","insuredCity":"testttt city insured","insuredState":"vt","insuredZip":"12345","insuredTelephone":"1123456432","insuredDOB":"10-30-1995","insuredSex":"Male","anotherHealthPlan":"yes","addInPlanName":"add plan test","addInPolicyGroupNo":"123dsfsdf","addInFirstName":"test name 3","addInMiddleName":"middle name 3","addInLastName":"last name 3","agreetoTheTerms":"no","nameOfParent":"Shobhit","I agree  (Minor State Representative Consent)":"no"}]';
        VTS_Event__c eventRecord = [SELECT Id FROM VTS_Event__c WHERE Description__c = 'testDESC' LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            InsertContactQueueable.resultWrapper rew = oktcContactUploadInternalController.insertContacts(inputString, eventRecord.Id);
            system.assertEquals(true, rew != null, 'testBadInputString2');
        }
        Test.stopTest();
    }
    @IsTest
    static void testCorrectInputString(){
        User contextUser = [SELECT Id, contactId, Contact.AccountId FROM User WHERE LastName = 'XYZ' LIMIT 1];
        String inputString = '[{"consent":"true","agreeToRegisterForSomeone":"Yes","firstName":"Shobhit","LastName":"Pant","email":"pantshobhit@gmail.com","Mobile":"8559880111","phoneType":"Mobile","eighteenPlus":"yes","receiveMethod":"E-Mail","receiveViaEmail":"yes","receiveViaSMS":"yes","dateOfBirth":"10-30-1995","streetAddress":"123","City":"test city","State":"vt","Gender":"Male","Zip":"12345","preferredLanguage":"English","Race":"Asian","Ethnicity":"Other","firstCovidTest":"No","employedInHealthCare":"No","symptomatic":"Yes","symptomaticOnsetDate":"08/17/2020","residentInCongregate":"Yes","pregnant":"No","reasonForTesting":"I am worried / concerned about an outbreak in my community","hasInsurance":"Yes","insuredAuthorizedForPayment":"TRUE","patientAuthorizationToReleaseInfo":"TRUE","insuranceType":"Medicare","insuranceIdNumber":"12345","insurancePlanName":"test program","fecaNumber":"123ds","reationToInsured":"Self","insuredFirstName":"Shobhit","insuredMiddleName":"test","insuredLastName":"paant","insuredStreetAddress":"123","insuredCity":"testttt city insured","insuredState":"vt","insuredZip":"12345","insuredTelephone":"1123456432","insuredDOB":"10-30-1995","insuredSex":"Male","anotherHealthPlan":"yes","addInPlanName":"add plan test","addInPolicyGroupNo":"123dsfsdf","addInFirstName":"test name 3","addInMiddleName":"middle name 3","addInLastName":"last name 3","agreetoTheTerms":"TRUE","nameOfParent":"Shobhit","I agree  (Minor State Representative Consent)":"FALSE"},{"consent":""}]';
        VTS_Event__c eventRecord = [SELECT Id FROM VTS_Event__c WHERE Description__c = 'testDESC' LIMIT 1];
        List<InsertContactQueueable.fieldWrapper> datalist = (List<InsertContactQueueable.fieldWrapper>)JSON.deserialize(inputString, List<InsertContactQueueable.fieldWrapper>.class);

        Test.startTest();
        System.runAs(contextuser){
            InsertContactQueueable.resultWrapper rew = oktcContactUploadInternalController.insertContacts(inputString, eventRecord.Id);
            system.assertEquals(true, rew != null, 'testCorrectInputString');
        }
        Test.stopTest();
    }
    @IsTest
    static void testDuplicateEntry(){
        User contextUser = [SELECT Id, contactId, Contact.AccountId FROM User WHERE LastName = 'XYZ' LIMIT 1];
        String inputString = '[{"consent":"TRUE","agreeToRegisterForSomeone":"TRUE","firstName":"Shobhit","LastName":"Pant","email":"testemail@test.com","Mobile":"8559880111","phoneType":"Mobile","eighteenPlus":"yes","receiveMethod":"E-mail","receiveViaEmail":"yes","receiveViaSMS":"yes","dateOfBirth":"10/30/1995","streetAddress":"123","City":"test city","State":"vt","Gender":"Male","Zip":"12345","preferredLanguage":"English","Race":"Asian","Ethnicity":"Other","firstCovidTest":"no","employedInHealthCare":"no","symptomatic":"yes","symptomaticOnsetDate":"08/17/2020","residentInCongregate":"yes","pregnant":"no","reasonForTesting":"I am worried / concerned about an outbreak in my community","hasInsurance":"yes","insuredAuthorizedForPayment":"TRUE","patientAuthorizationToReleaseInfo":"TRUE","insuranceType":"abc","insuranceIdNumber":"12345","insurancePlanName":"test program","fecaNumber":"123ds","reationToInsured":"father","insuredFirstName":"Shobhit","insuredMiddleName":"test","insuredLastName":"paant","insuredStreetAddress":"123","insuredCity":"testttt city insured","insuredState":"vt","insuredZip":"12345","insuredTelephone":"1123456432","insuredDOB":"10-30-1995","insuredSex":"Male","anotherHealthPlan":"yes","addInPlanName":"add plan test","addInPolicyGroupNo":"123dsfsdf","addInFirstName":"test name 3","addInMiddleName":"middle name 3","addInLastName":"last name 3","agreetoTheTerms":"TRUE","nameOfParent":"Shobhit","I agree  (Minor State Representative Consent)":"FALSE"},{"FirstName":"Shobhit"}]';
        VTS_Event__c eventRecord = [SELECT Id FROM VTS_Event__c WHERE Description__c = 'testDESC' LIMIT 1];
        //create a scenario where a covid contact with same email already exists
        Id covidRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Citizen (COVID)').getRecordTypeId();
        Contact existingContact = TestDataFactoryContact.createContact(false, 'TestingSite', 'Testuser');
        existingContact.RecordTypeId = covidRecordTypeId;
        existingContact.Email = 'testemail@test.com';
        insert existingContact;

        Test.startTest();
        System.runAs(contextuser){
            InsertContactQueueable.resultWrapper rew = oktcContactUploadInternalController.insertContacts(inputString, eventRecord.Id);
            system.assertEquals(true, rew != null, 'testDuplicateEntry');
        }
        Test.stopTest();
    }

}