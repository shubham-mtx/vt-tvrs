/**
 * @author Balram Dhawan
 * @email balram.dhawan@mtxb2b.com
 * @create date 2021-01-11 15:22:58
 * @modify date 2021-10-29 13:15:46
 * @desc Display Vaccination Questions
 */
import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';
import getVaccinationQuestions from "@salesforce/apex/OkpcContactInformationController.getVaccinationQuestions";
import setVaccinationQuestions from "@salesforce/apex/OkpcContactInformationController.setVaccinationQuestions";
import secondDoseAvailable from "@salesforce/apex/OkpcContactInformationController.secondDoseAvailable";
import getAllPicklistValueForVaccine from "@salesforce/apex/OKPCDashboardController.getAllPicklistValueForVaccine";
import isStateQuestionShow from '@salesforce/label/c.TVRS_HIDE_OUT_OF_STATE';

export default class OkcpVaccineQuestionsNew extends LightningElement {

    @api isAppointment;
    @api appointmentContact = {};
    @api appointmentId;
    @api dependentContactId;
    @api appointmentType;
    @api eventId;
    @api insideCovidPortal;
    @api isTestingSite;
    @api vaccineType = 'Vaccination-2';
    @api isTestingAndSelectedDoseCompleted;
    @track obj = {};
    @track showSpinner;
    @track errorMessage;
    @track showError = false;
    @track outOfStateOptions;
    @track showOutOfStateQuestion = false;

    showPageHeader = true;
    dateOnset;
    currentstep = 5;
    isVermontState;
    outOfStateInfo;
    allergyInfo;
    allergyInfo3a;
    allergyInfo3b;
    allergyInfo3c;
    show3b;
    show3c;
    covid19Info;
    fourteenDayInfo;
    antibodyTreatmentInfo;
    immuneSystemInfo;
    pregnantInfo;
    breastFeedingInfo;
    doseNumFirstInfo;
    doseNumSecondInfo;
    doseNumInfo;
    bleedingDisorderInfo;
    showAllergy;
    showCovid19;
    showFourteenDay;
    showAntibodyTreatment;
    showImmuneSystem;
    showPregnant;
    showBreastFeeding;
    showBleedingDisorder;
    showDoseNum;
    isDisqualified = true;
    customErrorMessage;
    isSelectedDoseAvailable;
    overrideConfirmation;
    overrideConfirmed;
    doseNum;
    @track doseNumOptions;
    @track allergyOptions;
    @track allergyOptions3a;
    @track allergyOptions3b;
    @track allergyOptions3c;
    @track covid19Options;
    @track fourteenDayOptions;
    @track antibodyTreatmentOptions;
    @track immuneSystemOptions;
    @track bleedingDisorderOptions;
    @track breastFeedingOptions;
    doseNumValueSelected;
    selectedDoseVsVaccinationMap = {};
    callbackRendered;
    calledFromRetain;

    loadPickVals() {
        getAllPicklistValueForVaccine()
            .then(result => {
                this.breastFeedingOptions = result.VaccineQuestion_Breastfeeding__c;
                this.bleedingDisorderOptions = result.VaccineQuestion_BleedingDisorder__c;
                this.immuneSystemOptions = result.VaccineQuestion_ImmuneSystem__c;
                this.antibodyTreatmentOptions = result.VaccineQuestion_AntibodyTreatment90__c;
                this.fourteenDayOptions = result.VaccineQuestion_14Day__c;
                this.covid19Options = result.VaccineQuestion_Covid19__c;
                this.allergyOptions3c = result.VaccineQuestion_Allergy3c__c;
                this.allergyOptions3b = result.VaccineQuestion_Allergy3b__c;
                this.allergyOptions3a = result.VaccineQuestion_Allergy3a__c;
                this.allergyOptions = result.VaccineQuestion_Allergy__c;
                this.doseNumOptions = result.VaccineQuestion_DoseNum__c;
                result.tvrsVaccineTypsVsDoseNumberRecords && result.tvrsVaccineTypsVsDoseNumberRecords.forEach(record => {
                    this.selectedDoseVsVaccinationMap[record.label] = record.value;
                });
                let outOfStateValuesTemp = result.VaccineQuestion_OutOfState__c;
                let newValues = [];
                outOfStateValuesTemp.forEach(element => {
                    if (element.label !== 'None of the Above' &&
                        element.label !== 'My Primary Care Provider is located in Vermont' &&
                        element.label !== 'I have moved here within the last 6 months with the intention of becoming a resident' &&
                        element.label !== 'work in Vermont' &&
                        element.label !== 'have a Primary Care Provider located in Vermont' &&
                        element.label !== 'have an out of state address because you moved here within the last 6 months with the intention of becoming a resident') {
                        newValues.push({
                            label: element.label,
                            value: element.label
                        });
                    }
                });
                newValues.push({
                    label: 'None of the Above',
                    value: 'None of the Above'
                });
                this.outOfStateOptions = newValues;
                if (this.isAppointment) {
                    this.obj = JSON.parse(JSON.stringify(this.appointmentContact));
                    this.overrideConfirmed = this.obj.bonusDose;
                    this.retainValues();
                    if (this.obj.state !== 'VT' && isStateQuestionShow == '1') {
                        this.isVermontState = true;
                    } else {
                        this.showDoseNum = true;
                    }
                } else {
                    this.showSpinner = true;
                    this.doInit();
                }
                this.checkSecondDoseAvailablility();
                this.focusTitle();
            })
            .catch(error => {
                showAsyncErrorMessage(this, error);
            });

    }

    connectedCallback() {
        this.loadPickVals();
        window.scroll(0, 0);
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @modify date 2021-01-11 15:23:11
     * @desc manage questions on back or if already answered
     */
    retainValues(data) {
        if (!data) {
            data = JSON.parse(JSON.stringify(this.appointmentContact));
        }
        if (data.hasOwnProperty('VaccineQuestion_DoseNum__c') && data.VaccineQuestion_DoseNum__c !== null && data.VaccineQuestion_DoseNum__c !== undefined) {
            this.callbackRendered = true;
            this.calledFromRetain = true;
            for (const key in data) {
                if (key.includes('Vaccine')) {
                    let freshData = {
                        target: {
                            name: key,
                            value: data[key]
                        }
                    };
                    this.handleChange(freshData);
                }
            }
        }
    }

    checkSecondDoseAvailablility() {
        if (this.eventId) {
            secondDoseAvailable({
                    eventId: this.eventId,
                    vaccineType: this.vaccineType
                })
                .then(result => {
                    this.isSelectedDoseAvailable = result;
                })
                .catch(error => {
                    showAsyncErrorMessage(this, error);
                });
        }
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @modify date 2021-01-11 15:23:11
     * @desc Get Vaccination Questions
     */
    doInit() {
        if (this.appointmentId) {
            getVaccinationQuestions({
                    appointmentId: this.appointmentId
                })
                .then(result => {
                    this.obj = result;
                    this.retainValues(JSON.parse(JSON.stringify(this.obj)));
                    if (result.hasOwnProperty('Patient__r') && result.Patient__r.State__c === 'VT') {
                        this.isVermontState = false;
                        this.showDoseNum = true;
                    } else {
                        if (isStateQuestionShow == '1') {
                            this.isVermontState = true;
                        } else {
                            this.isVermontState = false;
                            this.showDoseNum = true;
                        }

                    }
                    this.showSpinner = false;
                })
                .catch(error => {
                    this.showSpinner = false;
                    this.customErrorMessage = this.errorHandler(error);
                    this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
                });
        }
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @modify date 2021-01-11 15:23:11
     * @desc Handle Input change event
     */
    handleChange(event) {
        this.obj[event.target.name] = event.target.value;
        this.dependencyHelper(event.target.name, event.target.value);
    }

    handleIputChange(event) {
        this.obj.isValid = false;
        this.calledFromRetain = false;
        this.handleChange(event);
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @modify date 2021-01-11 15:23:11
     * @desc This method manages the dependency of info messages
     */
    dependencyHelper(fieldName, fieldValue) {
        //Added by Manthan Starts
        switch (fieldName) {
            case 'VaccineQuestion_OutOfState__c':
                //         //Added switch for nested if
                switch (fieldValue) {
                    case 'None of the Above':
                        this.resetFieldValues(['VaccineQuestion_DoseNum__c', 'VaccineQuestion_Allergy3a__c', 'VaccineQuestion_Allergy3b__c', 'VaccineQuestion_Covid19__c', 'VaccineQuestion_14Day__c', 'VaccineQuestion_AntibodyTreatment90__c', 'VaccineQuestion_ImmuneSystem__c', 'VaccineQuestion_BleedingDisorder__c', 'VaccineQuestion_Pregnant__c', 'VaccineQuestion_Breastfeeding__c']);
                        this.isDisqualified = true;
                        this.outOfStateInfo = true;
                        this.showDoseNum = false;
                        this.doseNumInfo = false;
                        this.showAllergy = false;
                        this.showCovid19 = false;
                        // this.showFourteenDay = false;         // S-18793
                        this.show3b = false;
                        this.show3c = false;
                        this.showAntibodyTreatment = false;
                        this.showImmuneSystem = false;
                        this.showBleedingDisorder = false;
                        this.showPregnant = false;
                        this.showBreastFeeding = false;
                        break;
                    default:
                        this.showDoseNum = true;
                        this.isDisqualified = false;
                        this.outOfStateInfo = false;
                }
                break;
            case 'VaccineQuestion_DoseNum__c':
                this.resetFieldValues(['VaccineQuestion_Allergy3a__c', 'VaccineQuestion_Allergy3b__c', 'VaccineQuestion_Allergy3c__c', 'VaccineQuestion_Covid19__c', 'VaccineQuestion_14Day__c', 'VaccineQuestion_AntibodyTreatment90__c', 'VaccineQuestion_ImmuneSystem__c', 'VaccineQuestion_BleedingDisorder__c', 'VaccineQuestion_Pregnant__c', 'VaccineQuestion_Breastfeeding__c']);
                this.showAllergy = false;
                this.isDisqualified = false;
                this.allergyInfo3a = false;
                this.allergyInfo3b = false;
                this.allergyInfo3c = false;
                this.show3b = false;
                this.show3c = false;
                this.doseNumFirstInfo = false;
                this.doseNumSecondInfo = false;
                this.showCovid19 = false;
                // this.showFourteenDay = false;         // S-18793
                this.showAntibodyTreatment = false;
                this.showImmuneSystem = false;
                this.showBleedingDisorder = false;
                this.showPregnant = false;
                this.showBreastFeeding = false;

                this.doseNumValueSelected = fieldValue;
                this.doseNumInfo = true;
                this.overrideConfirmed = (this.selectedDoseVsVaccinationMap[fieldValue] !== this.vaccineType && this.isTestingSite);
                this.overrideConfirmation = this.overrideConfirmed && !this.callbackRendered;
                this.callbackRendered = (this.callbackRendered) ? false : this.callbackRendered;
                if (this.isSelectedDoseAvailable && this.selectedDoseVsVaccinationMap[fieldValue] !== this.vaccineType && this.isTestingSite) {
                    this.doseNum = fieldValue;
                } else {
                    if (fieldValue === 'First Dose') {
                        if (this.isSelectedDoseAvailable && this.selectedDoseVsVaccinationMap[fieldValue] !== this.vaccineType && !this.isTestingSite) {
                            this.isDisqualified = true;
                            this.doseNumSecondInfo = true;
                            this.doseNumFirstInfo = false;
                            this.showAllergy = false;
                        } else {
                            this.isDisqualified = false;
                            this.doseNumSecondInfo = false;
                            this.doseNumFirstInfo = true;
                            this.showAllergy = true;
                        }
                    } else if ((fieldValue === 'Second Dose' || fieldValue === 'Third Dose') && this.isSelectedDoseAvailable && this.selectedDoseVsVaccinationMap[fieldValue] == this.vaccineType && !this.insideCovidPortal) {
                        this.overrideConfirmed = false;
                        this.doseNumInfo = false;
                        this.isDisqualified = false;
                        this.doseNumFirstInfo = false;
                        this.showAllergy = true;
                    } else {
                        this.showAllergy = false;
                        this.isDisqualified = true;
                        this.show3b = false;
                        this.show3c = false;
                        this.doseNumFirstInfo = false;
                        this.doseNumSecondInfo = true;
                        this.showCovid19 = false;
                        // this.showFourteenDay = false;         // S-18793
                        this.showAntibodyTreatment = false;
                        this.showImmuneSystem = false;
                        this.showBleedingDisorder = false;
                        this.showPregnant = false;
                        this.showBreastFeeding = false;
                    }
                }
                break;
            case 'VaccineQuestion_Allergy3a__c':
                //         //Added switch for nested if
                switch (fieldValue) {
                    case 'Yes':
                        this.resetFieldValues(['VaccineQuestion_Allergy3c__c', 'VaccineQuestion_Allergy3b__c', 'VaccineQuestion_Covid19__c', 'VaccineQuestion_14Day__c', 'VaccineQuestion_AntibodyTreatment90__c', 'VaccineQuestion_ImmuneSystem__c', 'VaccineQuestion_BleedingDisorder__c', 'VaccineQuestion_Pregnant__c', 'VaccineQuestion_Breastfeeding__c']);
                        this.isDisqualified = true;
                        this.show3b = false;
                        this.show3c = false;
                        this.allergyInfo3a = true;
                        this.allergyInfo3b = false;
                        // this.showFourteenDay = false;         // S-18793
                        this.showAntibodyTreatment = false;
                        this.showImmuneSystem = false;
                        this.showBleedingDisorder = false;
                        this.showPregnant = false;
                        this.showBreastFeeding = false;
                        this.showCovid19 = false;
                        break;
                    default:
                        this.show3b = true;
                        this.isDisqualified = false;
                        this.allergyInfo3a = false;
                }
                break;
            case 'VaccineQuestion_Allergy3b__c':
                if (fieldValue === 'No' || fieldValue == 'Not Applicable') {
                    this.resetFieldValues(['VaccineQuestion_Allergy3c__c', 'VaccineQuestion_Covid19__c']);
                    this.show3c = false;
                    this.allergyInfo3b = false;
                    this.isDisqualified = false;
                    this.showCovid19 = true;
                } else {
                    this.resetFieldValues(['VaccineQuestion_Allergy3c__c', 'VaccineQuestion_Covid19__c']);
                    this.allergyInfo3b = true;
                    this.show3c = true;
                    this.isDisqualified = false;
                    this.showCovid19 = false;
                }
                break;
            case 'VaccineQuestion_Allergy3c__c':
                //         //Added Switch for nested if
                switch (fieldValue) {
                    case 'No':
                        this.resetFieldValues(['VaccineQuestion_Covid19__c', 'VaccineQuestion_14Day__c', 'VaccineQuestion_AntibodyTreatment90__c', 'VaccineQuestion_ImmuneSystem__c', 'VaccineQuestion_BleedingDisorder__c', 'VaccineQuestion_Pregnant__c', 'VaccineQuestion_Breastfeeding__c']);
                        this.isDisqualified = true;
                        this.showCovid19 = false;
                        this.allergyInfo3c = true;
                        // this.showFourteenDay = false;         // S-18793
                        this.showAntibodyTreatment = false;
                        this.showImmuneSystem = false;
                        this.showBleedingDisorder = false;
                        this.showPregnant = false;
                        this.showBreastFeeding = false;
                        break;
                    default:
                        this.showCovid19 = true;
                        this.isDisqualified = false;
                        this.allergyInfo3c = false;
                }
                break;

            case 'VaccineQuestion_Covid19__c':
                //          //Added switch for nested if
                switch (fieldValue) {
                    case 'Yes':
                        this.resetFieldValues(['VaccineQuestion_14Day__c', 'VaccineQuestion_AntibodyTreatment90__c', 'VaccineQuestion_ImmuneSystem__c', 'VaccineQuestion_BleedingDisorder__c', 'VaccineQuestion_Pregnant__c', 'VaccineQuestion_Breastfeeding__c']);
                        // this.showFourteenDay = false;         // S-18793
                        this.isDisqualified = true;
                        this.covid19Info = true;
                        this.showAntibodyTreatment = false;
                        this.showImmuneSystem = false;
                        this.showBleedingDisorder = false;
                        this.showPregnant = false;
                        this.showBreastFeeding = false;
                        break;
                    default:
                        this.showAntibodyTreatment = true;
                        this.isDisqualified = false;
                        this.covid19Info = false;

                }
                break;
            case 'VaccineQuestion_14Day__c':
                if (fieldValue === 'Yes' || fieldValue === 'Not Sure') {
                    this.fourteenDayInfo = true;
                } else {
                    this.fourteenDayInfo = false;
                }
                break;


            case 'VaccineQuestion_AntibodyTreatment90__c':
                //Added switch for nested if
                switch (fieldValue) {
                    case 'Yes':
                        this.resetFieldValues(['VaccineQuestion_ImmuneSystem__c', 'VaccineQuestion_BleedingDisorder__c', 'VaccineQuestion_Pregnant__c', 'VaccineQuestion_Breastfeeding__c']);
                        this.isDisqualified = true;
                        this.antibodyTreatmentInfo = true;
                        this.showImmuneSystem = false;
                        this.showPregnant = false;
                        this.showBreastFeeding = false;
                        this.showBleedingDisorder = false;
                        break;
                    default:
                        this.showImmuneSystem = true;
                        this.showPregnant = true;
                        this.showBreastFeeding = true;
                        this.showBleedingDisorder = true;

                        this.isDisqualified = false;
                        this.antibodyTreatmentInfo = false;
                }
                break;
            case 'VaccineQuestion_ImmuneSystem__c':
                this.immuneSystemInfo = (fieldValue === 'Yes');
                break;
            case 'VaccineQuestion_BleedingDisorder__c':
                this.bleedingDisorderInfo = (fieldValue === 'Yes');
                break;
            case 'VaccineQuestion_Pregnant__c':
                this.pregnantInfo = (fieldValue === 'Yes' || fieldValue === 'Not sure');
                break;
            case 'VaccineQuestion_Breastfeeding__c':
                this.breastFeedingInfo = (fieldValue === 'Yes');
                break;
        }

        //Added by Manthan Ends
        this.isDisqualified = (!this.isDisqualified && this.doseNumValueSelected) ? (this.selectedDoseVsVaccinationMap[this.doseNumValueSelected] !== this.vaccineType && !this.isTestingSite && !this.insideCovidPortal) : this.isDisqualified;
        this.doseNumInfo = (this.obj.isValid) ? false : this.doseNumInfo;
        this.showAllergy = (this.calledFromRetain) ? true : this.showAllergy;
        this.obj.VaccineQuestion_Covid19__c = (this.calledFromRetain && !this.covid19Info) ? 'No' : this.obj.VaccineQuestion_Covid19__c;
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @modify date 2021-01-11 15:23:11
     * @desc Reset Field Values
     */
    resetFieldValues(data) {
        data.forEach(element => {
            this.dependencyHelper(element, '');
            this.obj[element] = '';
        });
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @modify date 2021-01-11 15:23:11
     * @desc Handle Back Navigation
     */
    navigateToBack() {
        const selectedEvent = new CustomEvent("backevent", {
            detail: null
        });
        this.dispatchEvent(selectedEvent);
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @modify date 2021-01-11 15:23:11
     * @desc Helper to send event to Parent
     */
    sendEventToParent(obj) {
        const selectedEvent = new CustomEvent("save", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @modify date 2021-01-11 15:23:11
     * @desc Save Appointment Data
     */
    saveData() {
        if (this.isAppointment && this.isValid()) {
            this.obj.bonusDose = this.overrideConfirmed;
            this.obj.bonusDoseNumber = this.selectedDoseVsVaccinationMap[this.obj.VaccineQuestion_DoseNum__c];
            this.obj.isTestingAndSelectedDoseCompleted = ((this.isTestingAndSelectedDoseCompleted && typeof this.isTestingAndSelectedDoseCompleted !== 'boolean') && this.isTestingAndSelectedDoseCompleted.includes(this.obj.VaccineQuestion_DoseNum__c));
            this.obj.isValid = true;
            const selectedEvent = new CustomEvent("setobject", {
                detail: this.obj
            });
            this.dispatchEvent(selectedEvent);
        } else {
            if (this.isValid()) {
                setVaccinationQuestions({
                        data: this.obj,
                        currentstep: this.currentstep,
                        AppointmentId: this.appointmentId
                    })
                    .then(result => {
                        this.showSpinner = false;
                        let tempObj = {};
                        tempObj.message = "saved successfully";
                        tempObj.status = "success";
                        this.sendEventToParent(tempObj);
                    })
                    .catch(error => {
                        this.showSpinner = false;
                        this.customErrorMessage = this.errorHandler(error);
                        this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
                    })
            }
        }
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @modify date 2021-01-11 15:23:11
     * @desc Standard and Custom Validation
     */
    isValid() {
        if (this.obj.hasOwnProperty('Patient__r')) {
            delete this.obj.Patient__r;
        }
        const allValid = [...this.template.querySelectorAll('lightning-combobox')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        if (allValid && this.isDisqualified === false) {
            return true;
        } else {
            // this.customErrorMessage = 'You are not enough qualified to schedule an appointment';
            // this.template.querySelector('c-lwc-custom-toast').showCustomNotice();
        }
        return false;
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @modify date 2021-01-11 15:23:11
     * @desc Handle navigation on Cancel Button
     */
    navigateToCancel() {
        this.dispatchEvent(new CustomEvent('cancelappointment'));
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @modify date 2021-01-11 15:23:11
     * @desc Error handler return from Apex Invoker
     */
    errorHandler(error) {
        if (Array.isArray(error.body)) {
            return error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            return error.body.message;
        }
    }

    modalCancelHandler() {
        this.resetFieldValues(['VaccineQuestion_Allergy3a__c', 'VaccineQuestion_Covid19__c', 'VaccineQuestion_14Day__c', 'VaccineQuestion_AntibodyTreatment90__c', 'VaccineQuestion_ImmuneSystem__c', 'VaccineQuestion_BleedingDisorder__c', 'VaccineQuestion_Pregnant__c', 'VaccineQuestion_Breastfeeding__c']);
        this.showAllergy = false;
        this.isDisqualified = true;
        this.show3b = false;
        this.doseNumFirstInfo = false;
        this.doseNumSecondInfo = true;
        this.showCovid19 = false;
        // this.showFourteenDay = false;	         // S-18793
        this.showAntibodyTreatment = false;
        this.showImmuneSystem = false;
        this.showBleedingDisorder = false;
        this.showPregnant = false;
        this.showBreastFeeding = false;
        this.overrideConfirmation = false;
        this.overrideConfirmed = false;
    }
    modalConfirmHandler() {
        this.overrideConfirmed = true;
        this.overrideConfirmation = false;
        this.isDisqualified = false;
        this.doseNumSecondInfo = false;
        this.doseNumFirstInfo = true;
        this.showAllergy = true;
    }

    focusTitle() {
        let focus = setInterval(() => {
            let cmp = this.template.querySelector('.pageTitle');
            if (cmp) {
                cmp.focus();
                setTimeout(() => {
                    this.showPageHeader = false;
                }, 500);
                clearInterval(focus);
            }
        }, 200);
    }
}