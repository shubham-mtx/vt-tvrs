/**
 * @description       : 
 * @author            : Garima Saxena
 * @group             : 
 * @last modified on  : 02-16-2021
 * @last modified by  : Wasef Mohiuddin
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   02-16-2021   Wasef Mohiuddin   Initial Version
**/
public with sharing class PreRegistrationGroupTriggerHandler {

    public static void afterUpdate(List<Pre_Registration_Group__c> newList, Map<Id, Pre_Registration_Group__c> newMap, Map<Id, Pre_Registration_Group__c> oldMap) {
        sendBatchEmail(newMap, oldMap);
    }

    public static void sendBatchEmail(Map<Id, Pre_Registration_Group__c> newMap, Map<Id, Pre_Registration_Group__c> oldMap){
        
        Set<Id> prGroupIdSet = new Set<Id>();
        for(Pre_Registration_Group__c prGroup : newMap.values()){
            if(prGroup.Email_Acknowledgement__c && prGroup.Status__c.equals('Activate') && oldMap.get(prGroup.Id).Status__c != 'Activate'){
                prGroupIdSet.add(prGroup.Id);
            }
        }
        
        if(!prGroupIdSet.isEmpty()){
            VT_TS_VaccinationActivationEmailBatch batchRecord = new VT_TS_VaccinationActivationEmailBatch(prGroupIdSet);
            Database.executeBatch(batchRecord, 200);
        }
    }
}