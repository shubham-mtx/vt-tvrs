public without sharing class VTTS_SendEmailController {
    
    public static final String NO_CONTACTS_FOUND = 'The selected event does not have any registered Contacts';
    public static final String SENDER_DISPLAY_NAME = 'Testing Site User';
    public static final String EMAIL_SUBJECT_TEXT = 'Event Update';
    public static final String SUCCESS_MESSAGE = 'Email sent successfully';
    public static final String FAILURE_MESSAGE = 'Error while sending emails';
    
    @AuraEnabled
    public static DataWrapper eventList() {
        DataWrapper dw = new DataWrapper();
        return dw;
        /*try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                List < SelectOptionWrapper > options = new List < SelectOptionWrapper > ();
                Map<String, List<Contact>> eventIdToContactMap = new Map<String, List<Contact>>();
                
                String userId = UserInfo.getUserId();
                User user = [SELECT Name, FirstName, LastName, CompanyName, Email, Phone, smallPhotoUrl, ContactId, Contact.AccountId FROM User WHERE Id = :userId];
                AccountContactRelation accountContactRecord = OKPCHeaderController.getTestingSiteFromContact(user.ContactId);
                Contact con = [SELECT Id, FirstName, LastName, AccountId, Email FROM Contact Where Id = :user.ContactId WITH SECURITY_ENFORCED LIMIT 1];
                
                Map<Id, VTS_Event__c> eventsListMap = new Map<Id, VTS_Event__c>([
                    SELECT Id, Name, Location__c,Description__c, Start_Date__c, End_Date__c FROM VTS_Event__c WHERE Location__c = :accountContactRecord.AccountId WITH SECURITY_ENFORCED] );
                
                for (Appointment__c appointment : [ SELECT Id,Name, Patient__c, Patient_Email__c, Patient_FirstName__c, Patient_LastName__c, 
                                                Event__c FROM Appointment__c WHERE Event__c IN :eventsListMap.keySet() AND Patient_Email__c != NULL WITH SECURITY_ENFORCED]) {
                    if (appointment.Patient__c != null) {
                        Contact con2 = new Contact();
                        con2.Id = appointment.Patient__c;
                        con2.Email = appointment.Patient_Email__c;
                        con2.FirstName = appointment.Patient_FirstName__c;
                        con2.LastName = appointment.Patient_LastName__c;
                        
                        if (eventIdToContactMap.containsKey(appointment.Event__c)) {
                            eventIdToContactMap.get(appointment.Event__c).add(con2);
                        } else {
                            eventIdToContactMap.put(appointment.Event__c, new List<Contact>{con2});
                        }
                    }
                }
                
                
                for (VTS_Event__c event : eventsListMap.values()) {
                    
                    String labelShow = String.isNotBlank(event.Description__c) ? event.Description__c : event.Name ;
                    if (String.isNotBlank(String.valueOf(event.Start_Date__c)) || String.isNotBlank(String.valueOf(event.End_Date__c))) {
                        labelShow += ' (';
                        if (String.isNotBlank(String.valueOf(event.Start_Date__c))) {
                            labelShow += event.Start_Date__c.year()+'-'+event.Start_Date__c.month()+'-'+event.Start_Date__c.day();
                        }
                        if (String.isNotBlank(String.valueOf(event.End_Date__c))) {
                            labelShow = labelShow + ' - ' + event.End_Date__c.year()+'-'+event.End_Date__c.month()+'-'+event.End_Date__c.day();
                        }
                        labelShow += ')';
                    }
                    //options.add(new SelectOptionWrapper(event.Id, String.isBlank(event.Description__c) ? event.Name : event.Description__c));
                    options.add(new SelectOptionWrapper(event.Id, labelShow));
                }
                DataWrapper dw = new DataWrapper();
                dw.eventIdToContactsMap = eventIdToContactMap;
                dw.options = options;
                return dw;
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }
        catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }*/
    }

    public class DataWrapper {
        @AuraEnabled public Map<String, List<Contact>> eventIdToContactsMap;
        @AuraEnabled public List<SelectOptionWrapper> options;
        public dataWrapper() {
        }
    }
    public class SelectOptionWrapper {
        @AuraEnabled public string value;
        @AuraEnabled public string label;
        
        public SelectOptionWrapper(string value, string label) {
            this.value = value;
            this.label = label;
        }
    }
    
    @AuraEnabled
    public static ResponseWrapper sendEmailMethod(String emailBody, String eventId, List<String> contactEmailsToNotify) {
        String errorString = '';
        try {

            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
                List<VTS_Event__c> evtList = [SELECT Id, Description__c FROM VTS_Event__c WHERE Id = :eventId WITH SECURITY_ENFORCED];
                VTS_Event__c evt = evtList.get(0);
                for (String contactEmail : contactEmailsToNotify) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(new List<String>{contactEmail});
                    mail.setOrgWideEmailAddressId(System.Label.VT_ATTENDES_FROM_EMAIL);
                    mail.setSubject(EMAIL_SUBJECT_TEXT + ' - ' + (String.isBlank(evt.Description__c) ? '':evt.Description__c) );
                    mail.setHtmlBody(emailBody);
                    mail.setWhatId(eventId);
                    mail.setSaveAsActivity(false);
                    mailList.add(mail);
                }
                if (mailList.size() > 0) {
                    List<Messaging.SendEmailResult> emailResult = Messaging.sendEmail(mailList);
                    for (Messaging.SendEmailResult result : emailResult) {
                        if (!result.isSuccess()) {
                            String errorMessage = result.getErrors()[0].getMessage();
                            errorString += errorMessage;
                        }
                    }
                    if (errorString != '') {
                        return new ResponseWrapper(false, FAILURE_MESSAGE + ' - ' + errorString);
                    }
                }
                return new ResponseWrapper(true, SUCCESS_MESSAGE);
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
            
        } catch (Exception e) {
            return new ResponseWrapper(false, e.getMessage());
        }
        
    }
    public class ResponseWrapper {
        @AuraEnabled public boolean success;
        @AuraEnabled public String message;
        public ResponseWrapper(Boolean success, String message) {
            this.success = success;
            this.message = message;
        }
    }
    
    @AuraEnabled
    public static DataWrapper eventContactList(String eventId) {
        try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                List < SelectOptionWrapper > options = new List < SelectOptionWrapper > ();
                Map<String, List<Contact>> eventIdToContactMap = new Map<String, List<Contact>>();
                eventIdToContactMap.put(eventId, new List<Contact>());
                
                for (Appointment__c appointment : [
                    SELECT Id,Name, Patient__c, Patient_Email__c, Patient_FirstName__c, Patient_LastName__c
                    FROM Appointment__c
                    WHERE Event__c = :eventId WITH SECURITY_ENFORCED
                ]) {
                    if (appointment.Patient__c != null) {
                        Contact con = new Contact();
                        con.Id = appointment.Patient__c;
                        con.Email = appointment.Patient_Email__c;
                        con.FirstName = appointment.Patient_FirstName__c;
                        con.LastName = appointment.Patient_LastName__c;
                        eventIdToContactMap.get(eventId).add(con);
                    }
                }
                DataWrapper dw = new DataWrapper();
                dw.eventIdToContactsMap = eventIdToContactMap;
                return dw;
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
        
    }
}