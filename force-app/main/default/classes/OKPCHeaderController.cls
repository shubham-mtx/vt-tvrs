/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 01-12-2022
 * @last modified by  : Mohit Karani
**/
public without sharing class OKPCHeaderController {
    static Id userIds = UserInfo.getUserId();
    public static AccountContactRelation getTestingSiteFromContact(String ContactId){
        
        try{
            //security check
            VTTS_AppointmentHelper.checkUserAccess(contactid, null);
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                if(ContactId != null){
                    AccountContactRelation accountContactRecord = [SELECT Id, AccountId, ContactId, Account.Event_Process__c, Account.Name, Is_Primary__c FROM AccountContactRelation WHERE ContactId =: ContactId AND Is_Primary__c = true LIMIT 1];
                    return accountContactRecord;
        
                }
                return null;
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }

        
    }

    @AuraEnabled
    public static Account getAccountDetail(String recordId){
        try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                Account accRecord = [SELECT Id, Event_Process__c FROM Account WHERE Id =: recordId WITH SECURITY_ENFORCED LIMIT 1];
                return accRecord;
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
    }

    @AuraEnabled(cacheable=true)
    public static Map<String,Object> getUserDetailsAndTestingSite(){
        try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                Map<String, Object> result = new Map<String, Object>();
                List<User> userList = [SELECT Name, FirstName, LastName, CompanyName, Email, Phone, MobilePhone, smallPhotoUrl, ContactId, Contact.AccountId FROM User WHERE Id = :userIds WITH SECURITY_ENFORCED];
                if(String.isNotBlank(userList[0].ContactId)) {
                    AccountContactRelation accountContactRecord = getTestingSiteFromContact(userList[0].ContactId);
                    result.put('userDetail',userList[0]);
                    result.put('testingSiteId',accountContactRecord.AccountId);
                    result.put('testingSiteType',accountContactRecord.Account.Event_Process__c);
                }
                return result;
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
        
    }

    /* @author: SP - 28th July 2020
     * @description: Method to send email for appointment confirmation
     */
    @AuraEnabled(cacheable=true)
    public static Boolean sendEmail(String appointmentId) {

        try{
            //security check
            VTTS_AppointmentHelper.checkUserAccess(null, appointmentId);
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                List<Appointment__c> appointmentList = new List<Appointment__c>();
                List<EmailTemplate> emailTemplateList = new List<EmailTemplate>();

                appointmentList = [SELECT Id, Patient__c, Patient__r.Parent_Contact__c,Event__r.Private_Access__r.Vaccine_Type__c,Patient__r.Parent_Contact__r.Email,Patient__r.Email, Lab_Center__r.Event_Process__c FROM Appointment__c WHERE Id = :appointmentId WITH SECURITY_ENFORCED];
                if(appointmentList[0].Lab_Center__r.Event_Process__c == 'Vaccination'){
                    if(appointmentList[0].Event__r.Private_Access__r.Vaccine_Type__c == 'Vaccination-1') {
                        emailTemplateList = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Citizen_Appointment_Creation_Vaccine' WITH SECURITY_ENFORCED];
                    } else {
                        emailTemplateList = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Citizen_Appointment_Creation_Vaccine_2' WITH SECURITY_ENFORCED];
                    }
                }else{
                    emailTemplateList = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Citizen_Appointment_Creation_Testing' WITH SECURITY_ENFORCED];
                }

                if(appointmentList[0].Patient__r.Email == '' || appointmentList[0].Patient__r.Email == null){
                    return false;
                }
                if(!emailTemplateList.isEmpty() && !appointmentList.isEmpty()) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    if(appointmentList[0].Patient__r.Parent_Contact__c != null){
                        mail.setToAddresses(new List<String>{appointmentList[0].Patient__r.Email,appointmentList[0].Patient__r.Parent_Contact__r.Email}) ;
                    }else{
                        mail.setToAddresses(new List<String>{appointmentList[0].Patient__r.Email}) ; 
                    }
                    mail.setTargetObjectId(appointmentList[0].Patient__c); 
                    mail.setTemplateId(emailTemplateList[0].Id); 
                    mail.setWhatId(appointmentList[0].Id);
                    mail.setOrgWideEmailAddressId(System.Label.VT_ATTENDES_FROM_EMAIL);//0D2350000004DlwCAE
                    mail.setSaveAsActivity(false); 
                    if(!Test.isRunningTest()){
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    }
                }
                return true;
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
        
    }

    /* @author: SP - 28th July 2020
     * @description: Method to get testing site id for the appointment
     */
    @AuraEnabled(cacheable=true)
    public static String getTestingSite(String appointmentId) {
        try{
            //security check
            VTTS_AppointmentHelper.checkUserAccess(null, appointmentId);
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                String testingSiteId = '';
                List<Appointment__c> appointmentList = new List<Appointment__c>();

                appointmentList = [SELECT Patient__r.Testing_Site__c 
                                FROM Appointment__c 
                                WHERE Id = :appointmentId WITH SECURITY_ENFORCED];

                if(!appointmentList.isEmpty() && String.isNotBlank(appointmentList[0].Patient__c) &&  String.isNotBlank(appointmentList[0].Patient__r.Testing_Site__c)) {
                    testingSiteId = String.valueOf(appointmentList[0].Patient__r.Testing_Site__c);
                } 
                return testingSiteId;
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
        
        
    }
    /* @author: SP - 18th Dec 2020
     * @description: Method to get whether we can checked in Appointment or not.
     * @lastModified: Kunal Srivastava
     */
    @AuraEnabled
    public static Map<String,Object> canCheckedIn(String appointmentId) {
        try{
            //security check
            VTTS_AppointmentHelper.checkUserAccess(null, appointmentId);
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                List<Appointment__c> appointmentList = new List<Appointment__c>();

                appointmentList = [SELECT Id,Patient__c,Appointment_Start_Date_v1__c,Event__r.Private_Access__r.Vaccine_Type__c
                                FROM Appointment__c 
                                WHERE Id = :appointmentId WITH SECURITY_ENFORCED];
                Map<String,Object> result = new Map<String,Object> ();
                if(appointmentList.isEmpty()){
                    result.put('invalidAppointment',true);
                }
                Appointment__c app = appointmentList[0];
                VTTS_AppointmentHelper appHlpr = new VTTS_AppointmentHelper ();

                if(String.isNotBlank(app.Event__r.Private_Access__r.Vaccine_Type__c)){
                    VTTS_AppointmentHelper.VT_TS_Wrapper validationWrapper = new VTTS_AppointmentHelper.VT_TS_Wrapper();
                    validationWrapper.contactId = app.Patient__c;
                    validationWrapper.currentVaccine =  app.Event__r.Private_Access__r.Vaccine_Type__c;
                    validationWrapper.isCheckIn = true;
                    for(OKPCDashboardController.SelectOptionWrapper wrapper : OKPCDashboardController.fetchPicklist('Private_Access__c','Vaccine_Type__c')){
                        validationWrapper.vaccineTypes = String.isBlank(validationWrapper.vaccineTypes) ? wrapper.value : validationWrapper.vaccineTypes + ',' +wrapper.value;
                    }
                    appHlpr.validateAppointmentsBooster(JSON.serialize(validationWrapper));
                    if(appHlpr.isInvalid() && String.isNotBlank(app.Event__r.Private_Access__c)){
                        return appHlpr.getResult();
                    }
                }
                if(app.Appointment_Start_Date_v1__c != null && System.today() < app.Appointment_Start_Date_v1__c) {
                    result.put('canCheckIn',false);
                    return result;
                }
                result.put('canCheckIn',true);
                return result;
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
        
    }

    @AuraEnabled
    public static Appointment__c getAppointmentData(String appointmentId) {
        try{
            //security check
            VTTS_AppointmentHelper.checkUserAccess(null, appointmentId);
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                return [SELECT Id,Testing_Site__c,Lab_Center__r.Name,Patient_FirstName__c,Patient_LastName__c,Patient_Middle_Name__c,Date_of_Birth__c,
                Gender__c,Race__c,Ethnicity__c,Patient__r.MailingAddress,Patient_State__c,Patient__r.MailingCountry,Patient_Zip__c,
                MRN__c,Patient_Phone_Number__c,EPIC_MRN__c,Type_of_Insurance__c,Insured_First_Name__c,Patient_s_relationship_to_insured__c,
                Insurance_ID_Number__c,Policy_Group_or_FECA_number__c,Additional_Insurance_Plan_Name__c,Additional_Insurance_First_Name__c,
                Additional_Insurance_Last_Name__c,Add_l_Insurance_Relationship_to_Insured__c,Additional_Insurance_Policy_Group_Number__c,
                Test_Ordered__c,Appointment_Date__c,Patient_Street__c,Patient_City__c,
                Patient__r.LeadSource,If_Other_Specimen_specify__c,Reason_for_Testing__c,Appointment_Complete_Date__c,Patient__r.Source__c,
                Name,EPIC_Instrument_ID__c,Reason_for_Test__c,Submitting_Institution__c,Ordering_Provider_First_Name__c,
                Ordering_Provider_Last_Name__c,NPI__c,Ordering_Provider_Phone_Number__c,Ordering_Provider_Fax_Number__c,
                Patient_Email__c,Additional_Comments_Information__c,First_COVID_19_Test__c,Employed_in_Healthcare__c,
                Symptomatic_as_defined_by_CDC__c,Symptoms_Onset_Date__c,Is_the_patient_hospitalized__c,Is_the_patient_currently_in_the_ICU__c,
                Resident_in_Congregate_Care_Setting__c,Are_you_pregnant__c, Event__r.Description__c, Canceled_Date__c 
                FROM Appointment__c WHERE Id = :appointmentId WITH SECURITY_ENFORCED];
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
        
    }

    @AuraEnabled
    public static Map<String,Object> getContactAccountRelations(){
        //try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                Map<String,Object> result = new Map<String,Object>();
                List<TestingSiteOptions> listOfTestingSiteOptions = new List<TestingSiteOptions>();
                TestingSiteOptions testingSiteOptionsObject;
                User currentLoggedInUser = [SELECT Name, FirstName, LastName, CompanyName, Email, Phone, smallPhotoUrl, ContactId, Contact.AccountId FROM User WHERE Id = :userIds LIMIT 1];
                for(AccountContactRelation accountContactRecord : [SELECT Id, AccountId, ContactId, Account.Name, Is_Primary__c 
                                                                    FROM AccountContactRelation WHERE ContactId =: currentLoggedInUser.ContactId WITH SECURITY_ENFORCED ORDER BY Account.Name ]){
                    testingSiteOptionsObject = new TestingSiteOptions();
                    testingSiteOptionsObject.value = accountContactRecord.AccountId;
                    testingSiteOptionsObject.label = accountContactRecord.Account.Name;
                    if(accountContactRecord.Is_Primary__c){
                        result.put('primary',accountContactRecord.AccountId);
                    }
                    listOfTestingSiteOptions.add(testingSiteOptionsObject);
                }
                result.put('UserInfo',currentLoggedInUser);
                result.put('TestingSites',listOfTestingSiteOptions);
                return result;
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        /*}catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }*/
        
    }

    @AuraEnabled
    public static void updatePrimary(String selectedId){
        //try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                User currentLoggedInUser = [SELECT Name, FirstName, LastName, CompanyName, Email, Phone, smallPhotoUrl, ContactId, Contact.AccountId FROM User WHERE Id = :userIds LIMIT 1];
                List<AccountContactRelation> listToUpdateContactAccountReltionship = new List<AccountContactRelation>();
                for(AccountContactRelation accountContactRecord : [SELECT Id, AccountId, ContactId, Account.Name, Is_Primary__c FROM AccountContactRelation WHERE ContactId =: currentLoggedInUser.ContactId WITH SECURITY_ENFORCED]){
                    if(accountContactRecord.Is_Primary__c){
                        accountContactRecord.Is_Primary__c = false;
                        listToUpdateContactAccountReltionship.add(accountContactRecord);
                    }
                    if(accountContactRecord.AccountId == selectedId){
                        accountContactRecord.Is_Primary__c = true;
                        listToUpdateContactAccountReltionship.add(accountContactRecord);
                    }
                }
                if(!listToUpdateContactAccountReltionship.isEmpty()){
                    //security check
                    listToUpdateContactAccountReltionship = (list<AccountContactRelation>)VT_SecurityLibrary.getAccessibleData('AccountContactRelation', listToUpdateContactAccountReltionship, 'update');
                    UPDATE listToUpdateContactAccountReltionship;
                }
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        /*}catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }*/
        
    }

    public class TestingSiteOptions{
        @AuraEnabled public String value;
        @AuraEnabled public String label;

        public TestingSiteOptions(){
            value = '';
            label = '';
        }
    }
    
    public class Micro211Wrapper {
        @AuraEnabled public String label{get;set;}
        @AuraEnabled public String value{get;set;}
        @AuraEnabled public Integer order{get;set;}

        public Micro211Wrapper(String label, String value, Integer order) {
            this.label = label;
            this.value = value;
            this.order = order;
        }
    }
    
}