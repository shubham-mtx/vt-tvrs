/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 01-21-2022
 * @last modified by  : Mohit Karani
**/
public without sharing class okpcRegisterController {

    @AuraEnabled
    public static String sendPatientId(String email) {

        try{
            List<Contact> contactList = [SELECT Id,
                                        Email,
                                        Patient_Id__c FROM Contact WHERE Email =: email AND Patient_Id__c != null AND RecordTypeId=:VT_TS_Constants.CITIZEN_COVID_RECORD_TYPE_ID  LIMIT 1 ];

            List<EmailTemplate> emailTemplateList = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :Label.Patient_Id_Email_Template_Name ];
            if(!contactList.isEmpty() && !emailTemplateList.isEmpty()) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new List<String>{contactList[0].Email});
                mail.setTargetObjectId(contactList[0].Id);
                mail.setTemplateId(emailTemplateList[0].Id);
                mail.setSaveAsActivity(false);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                return null;
            }
            return 'No User Found with this email.';
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
        
    }
    
    @AuraEnabled
    public static Map<String,Object> selfRegister(String patientid ,String email, String username, String password, String confirmPassword, String delimiter,String eventId , String dob) {//Added by Sajal
        
        Map<String, Object> result = new Map<String, Object>();
        String newUsername = username+delimiter;//Added by Sajal
        Savepoint sp = null;
        try{
            sp = Database.setSavepoint();
            List<Contact> con = [SELECT Id,FirstName,LastName,Email,AccountId,RecordTypeId, Birthdate, formatted_Date_of_Birth__c, Patient_Id__c,Login_Attempt_Count__c 
            FROM Contact WHERE Email =: email AND Patient_Id__c=:patientid AND RecordTypeId=:VT_TS_Constants.CITIZEN_COVID_RECORD_TYPE_ID LIMIT 1 FOR Update];
            
            // @VM : 14-12-2020 To check if DOB Mathes to related contact with this patient id
            if ( !con.isEmpty() && String.valueOf(con[0].Birthdate) != dob ) {
                
                result.put('message','The Date of Birth does not match with what you provided during account creation.');
                result.put('type','error');

                if ( con[0].Login_Attempt_Count__c >= 2 ) {
                    result.put('message','You have reached attempt limit. Please contact your system admin.');
                    result.put('type','error');

                    if ( con[0].Login_Attempt_Count__c == 2 ) {
                            con[0].Login_Attempt_Count__c = con[0].Login_Attempt_Count__c + 1;
                            //security check
                            con = (list<Contact>)VT_SecurityLibrary.getAccessibleData('Contact', con, 'update');
                            update con[0];
                    }
                }
                else {
                    con[0].Login_Attempt_Count__c = con[0].Login_Attempt_Count__c!=null ? con[0].Login_Attempt_Count__c  + 1 : 1;
                    //security check
                    // con = (list<Contact>)VT_SecurityLibrary.getAccessibleData('Contact', con, 'update');
                    update con[0];
                }

                return result;
            }
            else if ( !con.isEmpty() && String.valueOf(con[0].Birthdate) == dob ) {
                    if ( con[0].Login_Attempt_Count__c == null || con[0].Login_Attempt_Count__c <= 2 ) {
                        if ( con[0].Login_Attempt_Count__c != 0 ){
                            con[0].Login_Attempt_Count__c = 0;
                            //security check
                            //con = (list<Contact>)VT_SecurityLibrary.getAccessibleData('Contact', con, 'update');
                            update con[0];
                        }
                    }else{
                        result.put('message','You have reached attempt limit. Please contact your system admin.');
                        result.put('type','error');
                        return result;
                    }
            }
            // @VM : END

            List<User> userNameList = [SELECT Id, Name FROM User WHERE userName = :newUsername AND IsActive = true WITH SECURITY_ENFORCED LIMIT 1];
            if(userNameList.size()>0) {
                result.put('message','Username is already in use. Please enter a different username. ');
                result.put('type','error');
                return result;
            }
            if(con.size() > 0 ) {
                List <User> userList = [SELECT Id FROM User WHERE ContactId = :con[0].ID AND IsActive = true WITH SECURITY_ENFORCED LIMIT 1];
                if(userList.size()>0) {
                    result.put('message','User already exists.');
                    result.put('type','error');
                }
                else {
                    User u = new User();
                    u.Username = newUsername;//Modified by Sajal
                    u.put('Email',email);
                    u.ProfileId = [SELECT Id FROM Profile WHERE Name = :System.Label.OKPC_Profile_Name  LIMIT 1].Id;
                    u.FirstName = con[0].FirstName;
                    u.LastName = con[0].LastName;   
                    u.contactId = con[0].Id;
                    u.TimeZoneSidKey = 'America/New_York';
                    //user ka time zone fix same as vermont company time zone
                    
                    String nickname = ((newUsername != null && newUsername.length() > 0) ? newUsername.substring(0,1) : '' );
                    nickname += String.valueOf(Crypto.getRandomInteger()).substring(1,7);
                    u.put('CommunityNickname', nickname);
                    List<Contact> contactEmailsList = [SELECT Id FROM Contact WHERE Email =: con[0].Email AND RecordTypeId =:VT_TS_Constants.CITIZEN_COVID_RECORD_TYPE_ID LIMIT 2];
                    if(con[0].RecordTypeId == VT_TS_Constants.CITIZEN_COVID_RECORD_TYPE_ID && contactEmailsList.size() > 1){
                        Id actualAccountId = con[0].accountId;
                        Account tempAcc = new Account(name='Temp Test Scheduling Dummy'+Math.random() * 1000);
                        
                        //security check
                        tempAcc = (Account)VT_SecurityLibrary.getAccessibleData('Account', new list<Account>{tempAcc}, 'insert')[0];
                        INSERT tempAcc;

                        con[0].AccountId = tempAcc.Id;

                        //security check
                        con = (list<Contact>)VT_SecurityLibrary.getAccessibleData('Contact', con, 'update');

                        UPDATE con[0];
                        
                        String userId = Site.createPortalUser(u, tempAcc.Id, password);
                        con[0].AccountId = actualAccountId;
                        //security check
                        con = (list<Contact>)VT_SecurityLibrary.getAccessibleData('Contact', con, 'update');

                        update con[0];
                        
                        //security check
                        tempAcc = (Account)VT_SecurityLibrary.getAccessibleData('Account', new list<Account>{tempAcc}, 'delete')[0];
                        
                        DELETE tempAcc;
                    }else{
                        String userId = Site.createPortalUser(u, con[0].accountId, password);
                    }
                    
                    // User userUpdated  = [select id,contactid from user where id=:userId];
                    // userUpdated.ContactId = con[0].Id;
                    // Update userUpdated;
                    ApexPages.PageReference lgn = Site.login(newUsername, password, '/s' + ( String.isNotBlank(eventId) ? '/?eventId='+eventId : ''));
                    aura.redirect(lgn);
                    
                    result.put('message', 'User registered successfully. Please check your e-mail.');
                    result.put('type','success');
                    result.put('redirectURL',lgn.getUrl());
                }
            } else {
                result.put('message','Information not found. Please contact your administrator.');
                result.put('type','error');
            }
        } catch(DMLException ex) {
            Database.rollback(sp);
            
            String errorMessage = ex.getMessage();
            if (errorMessage.contains('Your request cannot be processed at this time')){
                errorMessage = 'Your request cannot be processed at this time. The site administrator has been alerted. In the meantime, please try again by clicking the CONFIRM button again.';
            }
            result.put('message', errorMessage);

            result.put('type','error');
        }
        catch (Exception ex) {
            Database.rollback(sp);
            
            String errorMessage = ex.getMessage();
            if (errorMessage.contains('Your request cannot be processed at this time')){
                errorMessage = 'Your request cannot be processed at this time. The site administrator has been alerted. In the meantime, please try again by clicking the CONFIRM button again.';
            }
            result.put('message', errorMessage);

            result.put('type','error');         
        }
        return result;
    }

    @AuraEnabled
    public static Map<String,Object> selfRegisterForTestSite(String recordId, String username, String password, String confirmPassword){        
        Map<String, Object> result = new Map<String, Object>();
        String newUsername = username+'.testsiteuser';
        Savepoint sp = null;

        try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                //security check
                VTTS_AppointmentHelper.checkUserAccess(recordId, null);
                
                sp = Database.setSavepoint();
                List<Contact> con = [SELECT Id,FirstName,LastName,Email,AccountId,RecordTypeId FROM Contact WHERE Id =: recordId WITH SECURITY_ENFORCED];
                List<User> userNameList = [SELECT Id, Name FROM User WHERE userName = :newUsername AND IsActive = true WITH SECURITY_ENFORCED];
                
                if(userNameList.size()>0) {
                    result.put('message','Username is already in use. Please enter a different username. ');
                    result.put('type','error');
                    return result;
                }

                if( !con.isEmpty() ) {
                    List <User> userList = [SELECT Id FROM User WHERE ContactId = :con[0].ID AND IsActive = true WITH SECURITY_ENFORCED];
                
                    if(userList.size()>0) {
                        result.put('message','User already exists.');
                        result.put('type','error');
                    }
                    else {
                        User u = new User();
                        u.Username = newUsername;
                        u.put('Email',con[0].email);
                        u.ProfileId = [SELECT Id FROM Profile WHERE Name = :System.Label.OKTC_Profile_Name].Id;
                        u.FirstName = con[0].FirstName;
                        u.LastName = con[0].LastName;   
                        u.contactId = con[0].Id;
                        u.TimeZoneSidKey = 'America/New_York';
                        
                        String nickname = ((newUsername != null && newUsername.length() > 0) ? newUsername.substring(0,1) : '' );
                        nickname += String.valueOf(Crypto.getRandomInteger()).substring(1,7);
                        u.put('CommunityNickname', nickname);

                        String userId = Site.createPortalUser(u, con[0].accountId, password);
                        ApexPages.PageReference lgn = Site.login(newUsername, password, '/s');
                        aura.redirect(lgn);
                        
                        result.put('message', 'User registered successfully. Please check your e-mail.');
                        result.put('type','success');
                        result.put('redirectURL',lgn.getUrl());

                    }
                }
                else {
                    result.put('message','Information not found. Please contact your administrator.');
                    result.put('type','error');
                }
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
            
            
        }catch(DMLException ex) {
            Database.rollback(sp);
            result.put('message', ex.getDMLMessage(0)+ex.getStackTraceString());
            result.put('type','error');
        }
        catch (Exception ex) {
            Database.rollback(sp);
            result.put('message', ex.getMessage());
            result.put('type','error');         
        }
        return result;
    }
    
    @AuraEnabled
    public static String forgotPassword(String userName, String delimiter) {//Added delimiter by Sajal

        
        String newUsername = userName + delimiter;
        String userId;
        String url = './CheckPasswordResetEmail';
        try {
            List<User> usr = [SELECT Id, Username FROM User WHERE Username =: newUsername WITH SECURITY_ENFORCED];
            //List<User> usrName = [SELECT Id, Username, ContactId FROM User WHERE Username =: userName];
            
            if(!usr.isEmpty()){
                if(!Site.isValidUsername(newUsername)) {//Modified by Sajal
                    return Label.Site.invalid_email;
                }
                Site.forgotPassword(newUsername);
                ApexPages.PageReference checkEmailRef = new PageReference(url);
                
                If(!test.isRunningTest())
                aura.redirect(checkEmailRef);
                    
                System.debug('success');
                return 'success';
            } else {
                String recordType = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
                List<Contact> listOfCOntactId = [SELECT Id, Name, Email FROM Contact WHERE Email=:userName AND RecordTypeId =:recordType AND Parent_Contact__c = null LIMIT 1];
                if(!listOfCOntactId.isEmpty()){
                    listOfCOntactId[0].Forgot_Password_Date_Time__c = System.now();
                    //security check(Commented by mohit as we are not giving read/edit access on Contact object to guest user profile).
                    // listOfCOntactId = (list<Contact>)VT_SecurityLibrary.getAccessibleData('Contact', listOfCOntactId, 'update');
                    update listOfCOntactId;

                    return 'success';
                    // return 'Non-Verified User'; // commented w.r.t Issue-68139

                }else
                {
                    return 'success';
                    // return 'No user found with this Email';     // commented w.r.t Issue-68139
                }
            }
        }
        catch (Exception ex) {
            return ex.getMessage();
        }
        
    }
    
    
    @AuraEnabled
    public static ContactInfoWrapper getContactInformation(String contactId) {
        try{
            //security check
            VTTS_AppointmentHelper.checkUserAccess(contactid, null);
            
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                List<Contact> contactList = [SELECT Id, Name, Patient_Id__c, email FROM Contact WHERE Id = :contactId WITH SECURITY_ENFORCED];
                ContactInfoWrapper wrapper = new ContactInfoWrapper();
                if(contactList.size()>0) {
                    wrapper.patientId = contactList[0].Patient_Id__c;
                    wrapper.email = contactList[0].email;
                    return wrapper;
                }else{
                    return null;
                }
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
        
    }

    public class ContactInfoWrapper {
        @AuraEnabled public String patientId {get; set;}
        @AuraEnabled public String email {get; set;}
    } 
}