/**
 * @description       : 
 * @author            : Wasef Mohiuddin
 * @group             : 
 * @last modified on  : 02-15-2021
 * @last modified by  : Wasef Mohiuddin
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   02-15-2021   Wasef Mohiuddin   Initial Version
**/
trigger ContactTrigger on Contact (before insert, before update) {
    if(Trigger.isBefore){
        if(Trigger.isUpdate || Trigger.isInsert ){
            if(VT_TS_SkipTriggerExecution.executeContactTrigger){
                VT_ContactTriggerHandler.onAfterUpdateOrInsert(Trigger.new);
                VT_ContactTriggerHandler.checkPermission(Trigger.new);
            }
        }
    }
}