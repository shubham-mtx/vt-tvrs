/**
 * @author balram.dhawan@mtxb2b.com
 * @description This Batch Update the Appointment with new account if event's account get changed from quick action called Change Account
 */
global with sharing class VT_TS_ProcessEventChangeBatch implements Database.Batchable<SObject> {
    public String oldLocationId;
    public String newLocationId;
    public String status = 'Scheduled';
    public String eventId;
    public Map<String, String> data;
    public Boolean processSlots;
    /**
     * @author balram.dhawan@mtxb2b.com
     * @description Move Appointment and SLots to Different Account
     * @inputParams Map<String, String> should have oldLocation, newLocationId and eventId 
     * second Param is processSlots, possible values are Slots or Appointments
     */
    global VT_TS_ProcessEventChangeBatch(Map<String, String> data, Boolean processSlots) {
        this.oldLocationId = data.get('oldLocationId');
        this.newLocationId = data.get('newLocationId');
        this.eventId = data.get('eventId');
        this.data = data;
        this.processSlots = processSlots;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        if(!processSlots) {
            return Database.getQueryLocator('SELECT Id, Appointment_Slot__c, Directions_long__c , Lab_Center__c FROM Appointment__c WHERE Lab_Center__c =:oldLocationId AND Event__c =:eventId AND status__c =:status');
        } else {
            List<VTS_Event__c> events = [SELECT Id, Start_Date__c, Start_Time__c, End_Time__c FROM VTS_Event__c WHERE Id =:eventId];
            Date eventStartDate = events[0].Start_Date__c;
            Time startTime = events[0].Start_Time__c;
            Time endTime = events[0].End_Time__c;
            String query = 'SELECT Id, Account__c FROM Appointment_Slot__c WHERE Account__c =:oldLocationId AND Date__c =:eventStartDate AND (Start_Time__c >=:startTime AND End_Time__c <=:endTime)';
            return Database.getQueryLocator(query);
        }
    }

    global void execute(Database.BatchableContext bc, List<SObject> scope) {
        if(!processSlots) {
            List<Appointment__c> appointments = (List<Appointment__c>) scope;
            String direction = [SELECT Id, Directions__c  FROM Account WHERE Id=: newLocationId LIMIT 1].Directions__c;
            for (Appointment__c appointment : appointments) {
                appointment.Lab_Center__c = newLocationId;
                appointment.Directions_long__c = direction;
            }
            update appointments;
        } else {
            List<Appointment_Slot__c> slotsToBeUpdate = (List<Appointment_Slot__c>) scope;
            for (Appointment_Slot__c slot : slotsToBeUpdate) {
                slot.Account__c = newLocationId;
            }
            if(!slotsToBeUpdate.isEmpty()) {
                update slotsToBeUpdate;
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        if(processSlots) {
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            string[] to = new string[] {UserInfo.getUserEmail()};
            
            email.setToAddresses(to);
            email.setSubject('Event\'s Location Change Request Status');
            
            email.setHtmlBody('Hello, <br/><br/>Your request for location change has been processed succesfully');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        } else {
            Database.executeBatch(new VT_TS_ProcessEventChangeBatch(data, true));
        }
    }
}