/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-21-2022
 * @last modified by  : Mohit Karani
**/
@isTest
public class OKPCHeaderControllerTest {
    static Id userIds = UserInfo.getUserId();
    private static final String LabSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
    private static final String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
	
    @testSetup
    public static void makeDat(){
        String recordTypeID = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        String citizenCovidId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        OKPCHeaderController.TestingSiteOptions obj = new OKPCHeaderController.TestingSiteOptions();
        
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Event_Process__c = 'Testing Site';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;
        
        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalException',
            MiddleName = 'ms',
            FirstName = 'testFirstException',
            Email = 'testportalserv1Exception@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)370-44151',
            Phone = '(702)370-44151',
            Street_Address1__c ='testStreet11',
            Street_Address_2__c ='testStreet21',
            City__c = 'TestCity1',
            State__c = 'OH',
            ZIP__c = '12310',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            what_was_the_last_date_of_contact__c = Date.newInstance(2020, 2, 20),
            Did_you_have_a_nasal_swab__c = 'Yes	',
            What_was_your_test_result__c = 'Negative',
            Approximately_what_date_were_you_tested__c = Date.newInstance(2020, 3, 3),
            Had_contact_with_anyone_diagon__c = 'Yes',
            Do_you_take_prescription_medications__c = '	Yes',
            medication_for_fever_in_the_past_3_days__c = 'Yes',
            First_Date_you_have_been_feeling_sick__c = Date.newInstance(2020, 3, 1) ,
            First_date_you_began_feeling_well__c= Date.newInstance(2020, 3, 10),
            Fever_Cough_or_Difficulty_breathing__c = 'Yes',
            Sick_since_Jan_1_2020__c = 'Yes',
            Opt_out_for_SMS_Updates__c = true,
            HasOptedOutOfEmail = false,
            Current_Step__c = 1,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con;
        
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType IN : customerUserTypes  limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.comtestException',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            //userRoleId = portalRole.Id,
            contactId = con.id
        );
        insert newUser;
    }
    static testMethod void testGetUserDetailsAndTestingSite(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Account acc1 = new Account (
            Name = 'newAcc1'
        );  

        insert acc1;
        
        Account acc2 = new Account (
            Name = 'newAcc2'
        );  

        insert acc2;

        Contact conCase = new Contact (
            AccountId = acc1.id,
            LastName = 'portalTestUserv1',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId(),
            MobilePhone = '123123123'
        );
        insert conCase;
        
        AccountContactRelation accountContactRecord = new AccountContactRelation();
        accountContactRecord.AccountId = acc2.Id;
        accountContactRecord.ContactId = conCase.Id;
        accountContactRecord.Is_Primary__c = true;
		INSERT accountContactRecord;
        
        Profile prfile = [select Id,name from Profile where Name = 'Covid Citizen User' limit 1];
        
        User newUser1 = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = conCase.id
        );
        insert newUser1;
        
        // System.runAs(newUser1){
            Map<String, Object> testResult = OKPCHeaderController.getUserDetailsAndTestingSite();
            OKPCHeaderController.getContactAccountRelations();
            system.assertEquals(false, !testResult.isEmpty(), 'testGetUserDetailsAndTestingSite');
        // }
        
        // User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        // system.runAs(usr){
        //     try{
        //         OKPCHeaderController.getUserDetailsAndTestingSite();
        //     } catch(Exception e){
                
        //     }
        // }
    }
    
    static testMethod void testGetUserDetailsAndTestingSite_Case2(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Account acc1 = new Account (
            Name = 'newAcc1'
        );  
        insert acc1;
        
        Account acc2 = new Account (
            Name = 'newAcc2'
        );  
        insert acc2;

        Contact conCase = new Contact (
            AccountId = acc1.id,
            LastName = 'portalTestUserv1',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId(),
            MobilePhone = '123123123'
        );
        insert conCase;
        
        AccountContactRelation accountContactRecord = new AccountContactRelation();
        accountContactRecord.AccountId = acc2.Id;
        accountContactRecord.ContactId = conCase.Id;
        accountContactRecord.Is_Primary__c = true;
		INSERT accountContactRecord;
        
        Profile prfile = [select Id,name from Profile where Name = 'Covid Citizen User' limit 1];
        
        User newUser1 = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = conCase.id
        );
        insert newUser1;
        
        Test.startTest();

       // System.runAs(newUser1){

            Map<String,Object> d = OKPCHeaderController.getContactAccountRelations();
            system.assertEquals(true, d.keySet().size()>0, 'Error here!');
        //}
        
        Test.stopTest();
        // User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        // system.runAs(usr){
        //     try{
        //         OKPCHeaderController.getUserDetailsAndTestingSite();
        //     } catch(Exception e){
                
        //     }
        // }
    }
    



    static testMethod void testMethod1(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Account acc1 = new Account (
            Name = 'newAcc1'
        );  

        insert acc1;
        Account acc2 = new Account (
            Name = 'newAcc2'
        );  

        insert acc2;

        Contact conCase = new Contact (
            AccountId = acc1.id,
            LastName = 'portalTestUserv1',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            MobilePhone = '123123123'
        );

        insert conCase;
        Contact conCase2 = new Contact (
            AccountId = acc2.id,
            LastName = 'portalTestUserv2',
            FirstName = 'testFirst2',
            Email = 'testportalTestUserv2@gmail.com',
            MobilePhone = '123123123'
        );

        insert conCase2;
        //Create user
        
        Profile prfile = [select Id,name from Profile where Name = 'Covid Citizen User' limit 1];
        
        Profile prfile1 = [select Id,name from Profile where UserType  IN : customerUserTypes limit 1];
        
        User newUser1 = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = conCase.id
        );
        insert newUser1;
        
        User newUser2 = new User(
            profileId = prfile1.id,
            username = 'newUser@yahootest.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = conCase2.id
        );
        insert newUser2;

        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, true);

        Account createAccountLabSite = TestDataFactoryAccount.createAccount('LabSite', LabSite, true);

        Contact createContact = TestDataFactoryContact.createContact(true , 'TestLastName','TestFirstName');

        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);

        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        createAppointment.Patient__c = newUser1.ContactId;

        insert createAppointment;

        Antibody_Testing__c createAntiTest = TestDataFactoryAntiBody.createAppointment(true, createAccountTestSite.id, createAppointment.id, createSlot.id, conCase.id);

        //system.runAs(newUser1){
            // OKPCHeaderController.getUserDetails();
            // OKPCHeaderController.getAntibodyList();
            OKPCHeaderController.getUserDetailsAndTestingSite();
            OKPCHeaderController.getTestingSite(createAppointment.Id);
            OKPCHeaderController.canCheckedIn(createAppointment.Id);
            OKPCHeaderController.Micro211Wrapper wrap = new OKPCHeaderController.Micro211Wrapper('label','value',2);
            OKPCHeaderController.updatePrimary(acc2.Id);
            OKPCHeaderController.getAccountDetail(acc2.Id);
        //}
      
        OKPCHeaderController.sendEmail(createAppointment.Id);
        OKPCHeaderController.getAppointmentData(createAppointment.Id);
        
        system.runAs(newUser2){
            // try{
            //     OKPCHeaderController.getUserDetails();
                
            // }catch(Exception e){
                
            // }
            
              
        }

        // try {
        //     OKPCHeaderController.getUserDetails();
        // }
        // catch(Exception e) {
        //     System.assert(true,e.getMessage());
        // }
        
    }

    static testMethod void canCheckedInVaccination(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Account acc1 = new Account (
            Name = 'newAcc1'
        );  

        insert acc1;
        Account acc2 = new Account (
            Name = 'newAcc2'
        );  

        insert acc2;

        Contact conCase = new Contact (
            AccountId = acc1.id,
            LastName = 'portalTestUserv1',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            MobilePhone = '123123123'
        );

        insert conCase;
        Contact conCase2 = new Contact (
            AccountId = acc2.id,
            LastName = 'portalTestUserv2',
            FirstName = 'testFirst2',
            Email = 'testportalTestUserv2@gmail.com',
            MobilePhone = '123123123'
        );

        insert conCase2;
        //Create user
        
        Profile prfile = [select Id,name from Profile where Name = 'Covid Citizen User' limit 1];
        
        Profile prfile1 = [select Id,name from Profile where UserType  IN : customerUserTypes limit 1];
        
        User newUser1 = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = conCase.id
        );
        insert newUser1;
        
        User newUser2 = new User(
            profileId = prfile1.id,
            username = 'newUser@yahootest.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = conCase2.id
        );
        insert newUser2;

        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, true);

        Account createAccountLabSite = TestDataFactoryAccount.createAccount('LabSite', LabSite, true);

        Contact createContact = TestDataFactoryContact.createContact(true , 'TestLastName','TestFirstName');

        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);

        Private_Access__c privateAccess = new Private_Access__c(
            Name = 'Private Access',
            Age_Based__c = 0,
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Pfizer',
            Vaccine_Type__c = 'Vaccination-1'
        );
        insert privateAccess;

        VTS_Event__c  event = new VTS_Event__c(Account__c = createAccountTestSite.Id, Start_Date__c = Date.today(),
                                               Location__c = createAccountTestSite.Id,Description__c='testCenter',
                                               End_Date__c = Date.today() +10, Private_Access__c = privateAccess.Id);

        insert event;

        Appointment__c  createAppointment1 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment1.Patient__c = createContact.Id;
        createAppointment1.Lab_Center__c = createAccountTestSite.id;
        createAppointment1.Appointment_Slot__c = createSlot.id;
        createAppointment1.Patient__c = newUser1.ContactId;
        createAppointment1.Event__c = event.Id;
        insert createAppointment1;
        

        // Antibody_Testing__c createAntiTest = TestDataFactoryAntiBody.createAppointment(true, createAccountTestSite.id, createAppointment.id, createSlot.id, conCase.id);

        system.runAs(newUser1){
            OKPCHeaderController.canCheckedIn(createAppointment1.Id);
        }
    }
    
    /*static testMethod void testMethod2(){
    	User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{ 
                OKPCHeaderController.getAntibodyList();
            }catch(Exception e){
                
            }
        }
    }*/
    
    static testMethod void testMethod3(){
    	User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{ 
                OKPCHeaderController.sendEmail(null);
            }catch(Exception e){
                
            }
        }
    }
    static testMethod void testMethod4(){
    	User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{ 
                OKPCHeaderController.getTestingSite(null);
            }catch(Exception e){
                
            }
        }
    }
    static testMethod void testMethod5(){
    	User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{ 
                OKPCHeaderController.canCheckedIn(null);
            }catch(Exception e){
                
            }
        }
    }
    static testMethod void testMethod6(){
    	User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{ 
                OKPCHeaderController.Micro211Wrapper wrap = new OKPCHeaderController.Micro211Wrapper('label','value',2);
            }catch(Exception e){
                
            }
        }
    }
    static testMethod void testMethod7(){
    	User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{ 
                OKPCHeaderController.updatePrimary(null);
                
            }catch(Exception e){
                
            }
        }
    }
    static testMethod void testMethod8(){
    	User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{ 
                OKPCHeaderController.getAccountDetail(null);
            }catch(Exception e){
                
            }
        }
    }
    
    static testMethod void testMethod9(){
    	User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{ 
                OKPCHeaderController.getAppointmentData(null);
            }catch(Exception e){
                
            }
        }
    }
    
    static testMethod void testMethod10(){ 
    	User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        system.runAs(usr){
            try{ 
                OKPCHeaderController.getTestingSiteFromContact(null);
            }catch(Exception e){
                
            }
        }
    }



    public static void prepareDataForCase1() {
        Account accountRecord = new Account();
        accountRecord.Name = 'Self Registration';
        accountRecord.Event_Process__c = 'Vaccination';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Test_Center').getRecordTypeId();
        INSERT accountRecord;

        
        Contact contactRecordCase1 = new Contact();
        contactRecordCase1.FirstName = 'Test1';
        contactRecordCase1.LastName = '00036';
        contactRecordCase1.Birthdate = date.today()-10;
        contactRecordCase1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecordCase1.Email = 'test1@test.com';
        contactRecordCase1.AccountId = accountRecord.Id;
        contactRecordCase1.Consent__c = true;
        INSERT contactRecordCase1;
        

        Private_Access__c privateAccessCase1 = new Private_Access__c(
            Name = 'private test',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'No',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        insert privateAccessCase1;

        VTS_Event__c eventCase1 = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccessCase1.Id
        );
        insert eventCase1;


        Appointment__c appointmentCase1 = new Appointment__c();
        appointmentCase1.Patient__c = contactRecordCase1.Id;
        appointmentCase1.Status__c = 'Completed';
        appointmentCase1.Event__c = eventCase1.Id;
        appointmentCase1.Lab_Center__c = accountRecord.Id;
        
        insert appointmentCase1;
    }

    
    static testMethod void testSendEmail(){ 
        prepareDataForCase1();
        

        Contact con = [SELECT Id FROM Contact WHERE LastName ='00036'];
        Appointment__c app = [SELECT Id FROM Appointment__c WHERE Patient__c=:con.Id];

        //User usr = [select id from User where userName='newUser@yahoo.comtestException'];
        // system.runAs(usr){
        //     try{ 
        Test.startTest();
            try {
                OKPCHeaderController.sendEmail(app.Id);
            }
            catch(Exception er) {
                System.assert(true,'error');
            }
        Test.stopTest();
    }
    


    
    
}