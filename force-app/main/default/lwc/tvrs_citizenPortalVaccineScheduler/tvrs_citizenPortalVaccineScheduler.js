/**
 * @description       : 
 * @author            : Balram Dhawan
 * @group             : 
 * @last modified on  : 01-21-2022
 * @last modified by  : Balram Dhawan
**/
import { LightningElement, wire } from 'lwc';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import VACCINE_TYPE_FIELD from '@salesforce/schema/Private_Access__c.Vaccine_Type__c';
import getSelfAndChildPatients from '@salesforce/apex/TVRS_CitizenPortalVaccinationController.getSelfAndChildPatients';
import checkUserEligibitlity from '@salesforce/apex/TVRS_CitizenPortalVaccinationController.checkUserEligibitlity';
import checkExistingDoses from '@salesforce/apex/TVRS_CitizenPortalVaccinationController.checkExistingDoses';
import saveAppointment from '@salesforce/apex/TVRS_CitizenPortalVaccinationController.saveAppointment';

var VACCINE_TYPE_VS_DOSE_NUMBER = {'Vaccination-1' : 'First Dose', 'Vaccination-2' : 'Second Dose', 'Vaccination-3' : 'Third Dose', 'Vaccination-4' : 'Fourth Dose', 'Vaccination-5' : 'Fifth Dose'};

export default class Tvrs_citizenPortalVaccineScheduler extends LightningElement {
    patientId;
    mainClass;
    pendingDoses;
    vaccineTypes;
    patients = [];
    vaccineClasses;
    isLoading = true;
    preRegRecord = {};
    disableNextDoseButton;
    requireNextDoseWindow;
    appointmentToReschedule;
    userEnteredDoseData = {};
    missingProfileInformation;
    latestCompletedAppointment;
    userRequestsForVaccineType;
    openVaccineConfirmationModal;
    alreadyVaccinatedWithFirstDose;
    previousVaccineType;
    currentStep = 'patientSelector';
    selectedReschedule;
    /**
    * @description handle the mainClass because we have different main class for each step
    * @author Balram Dhawan | 01-07-2022
    **/
    get mainCss() {
        var value;
        switch (this.currentStep) {
            case 'rescheduleWindow':
                value = 'slds-card already-scheduled';
                break;
            case 'doseSelector':
                value = 'slds-card scheduling-screen-1';
                break;
            case 'enterExistingDoseInformation':
                value = 'slds-card vaccine-and-date';
                break;
            case 'enterExistingDoseInformationConfirmation':
                value = 'slds-card vaccine-and-date';
                break;
            case 'vaccineClassNotSupported':
                value = 'slds-card no-scheduled-appointment-1';
                break;
            case 'notEligible':
                value = 'slds-card no-scheduled-appointment-2';
                break;
            case 'waiver':
                value = 'slds-card waiver-screen';
                break;
        
            default:
                break;
        }
        return value;
    }

    /**
    * @description returns vaccineType name E.g. Vaccination-1 becomes First Dose, Vaccination-2 becomes Second Dose
    * @author Balram Dhawan | 01-07-2022
    **/
    get vaccineTypeName() {
        return VACCINE_TYPE_VS_DOSE_NUMBER[this.previousVaccineType];
    }

    get notEligible() {
        return this.currentStep == 'notEligible';
    }

    get vaccineClassNotSupported() {
        return this.currentStep == 'vaccineClassNotSupported';
    }

    get patientSelector() {
        return this.currentStep === 'patientSelector';
    }

    get enterExistingDoseInformation() {
        return (this.currentStep == 'enterExistingDoseInformation' || this.currentStep === 'enterExistingDoseInformationConfirmation');
    }

    get rescheduleWindow() {
        return this.currentStep === 'rescheduleWindow';
    }

    get doseSelector() {
        return this.currentStep === 'doseSelector';
    }

    get waiver() {
        return this.currentStep === 'waiver';
    }

    get eventSelector() {
        return this.currentStep === 'eventSelector';
    }

    get oldVaccineClass() {
        return (this.latestCompletedAppointment) ? this.latestCompletedAppointment.Event__r.Private_Access__r.Vaccine_Class_Name__c : this.appointmentToReschedule.Event__r.Private_Access__r.Vaccine_Class_Name__c;
    }

    get hideNextButton() {
        return (this.currentStep === 'notEligible' || this.currentStep === 'vaccineClassNotSupported');
    }

    get enteredVaccineDate() {
        if(this.userEnteredDoseData.selectedVaccineDate) {
            var vaccineDateArray = this.userEnteredDoseData.selectedVaccineDate.split('-');
            return vaccineDateArray[1]+'/'+vaccineDateArray[2]+'/'+vaccineDateArray[0];
        }
        return '';
    }

    set setCurrentStep(value) {
        this.currentStep = value;
    }

    @wire(getPicklistValues, { recordTypeId: '012000000000000AAA', fieldApiName: VACCINE_TYPE_FIELD })
    getPicklistValues ({error, data}) {
        if (error) {
            this.showErrorMessage(error);
        } else if (data) {
            this.vaccineTypes = data.values;
        }
    }
    
    connectedCallback() {
        setTimeout(()=>{
            this.focusFirstEle();
        },200);

        this.doInit();
    }

    /**
    * @description fetches loggedIn contact and its child contacts
    * @author Balram Dhawan | 01-07-2022
    **/
    doInit() {
        getSelfAndChildPatients()
            .then(result => {
                this.getSelfAndChildPatientsResponseHandler(result);
            })
            .catch(error => {
                this.showErrorMessage(error);
            });
    }

    /**
    * @description handles response of apexCall - getSelfAndChildPatients
    * @author Balram Dhawan | 01-07-2022
    **/
    getSelfAndChildPatientsResponseHandler(result) {
        this.patients = result.patients;
        this.isLoading = false;
    }

    /**
    * @description handle submit events and process appropriate action based on currentStep
    * @author Balram Dhawan | 01-07-2022
    **/
    handleSubmit() {
        this.selectedReschedule = false;
        switch (this.currentStep) {
            case 'patientSelector':
                var patientField = this.template.querySelector('lightning-combobox');
                patientField.reportValidity();
                if(patientField.value) {
                    this.patientId = patientField.value;
                    this.isLoading = true;
                    checkUserEligibitlity({request: {patientId : patientField.value}})
                        .then(result => {
                            this.checkUserEligibitlityResponseHandler(result);
                        })
                        .catch(error => {
                            this.showErrorMessage(error);
                        });
                }
                break;
            
            case 'rescheduleWindow':
                var allInputs = this.template.querySelectorAll('input');
                var action;
                if(allInputs) {
                    allInputs.forEach(element => {
                        if(element.checked) {
                            action = element.value;
                        }
                    });

                    if(action === 'reschedule') {
                        this.userRequestsForVaccineType =  (this.appointmentToReschedule.Event__r && this.appointmentToReschedule.Event__r.Private_Access__r) ? this.appointmentToReschedule.Event__r.Private_Access__r.Vaccine_Type__c : null;
                        let inputData = {patientId : this.patientId, userRequestsForVaccineType: this.userRequestsForVaccineType };
                        if(this.userRequestsForVaccineType){
                            this.selectedReschedule = true;
                            this.handleCheckExistingDoses(inputData);
                        }
                        this.setCurrentStep = 'eventSelector';
                        // this.setCurrentStep = 'waiver';
                    } else if(action === 'scheduleNextDose') {
                        this.setCurrentStep = 'doseSelector';
                    } else {
                        this.showErrorMessage('Please select atleaset one action');
                    }
                }
                break;
            
            case 'doseSelector':
                var allInputs = this.template.querySelectorAll('input');
                if(allInputs) {
                    allInputs.forEach(element => {
                        if(element.checked && !element.disabled) {
                            this.userRequestsForVaccineType = element.value;
                            let inputData = {patientId : this.patientId, userRequestsForVaccineType: element.value};
                            this.handleCheckExistingDoses(inputData);
                        }
                    });
                }

                if(!this.userRequestsForVaccineType) {
                    this.showErrorMessage('Please select the dose you would like to receive');   
                }
                break;

            case 'enterExistingDoseInformation':
                this.userEnteredDoseData = this.getSelectedVaccineClassAndDate();
                if(this.userEnteredDoseData.selectedVaccineClass === 'Other') {
                    this.setCurrentStep = 'vaccineClassNotSupported';
                } else if(this.userEnteredDoseData.selectedVaccineClass && this.userEnteredDoseData.selectedVaccineDate) {
                    this.openVaccineConfirmationModal = true;
                } else {
                    this.showErrorMessage('Please select vaccine class and enter the date');
                }
                break;

            case 'enterExistingDoseInformationConfirmation':
                this.userEnteredDoseData = this.getSelectedVaccineClassAndDate();
                if(this.userEnteredDoseData.selectedVaccineClass && this.userEnteredDoseData.selectedVaccineDate) {
                    this.isLoading = true;
                    saveAppointment({inputData: 
                        {
                            patientId : this.patientId, 
                            userRequestsForVaccineType: this.userRequestsForVaccineType, 
                            vaccineClass : this.userEnteredDoseData.selectedVaccineClass, 
                            vaccineDate: this.userEnteredDoseData.selectedVaccineDate,
                            vaccineType: this.previousVaccineType
                        }})
                        .then(result => {
                            this.saveAppointmentResponseHandler(result);
                        })
                        .catch(error => {
                            this.currentStep = 'enterExistingDoseInformation';
                            this.showErrorMessage(error);
                        });
                } else {
                    this.showErrorMessage('Please select vaccine class and enter the date');
                }
                break;
        
            case 'waiver':
                var waiverFlag = this.template.querySelector('input');
                if(waiverFlag.checked) {
                    this.setCurrentStep = 'eventSelector';
                } else {
                    waiverFlag.reportValidity();
                    this.showErrorMessage('Please check required fields');
                }
                break;

            default:
                break;
        }
    }
    handleCheckExistingDoses(wrapper){
        this.isLoading = true;
        checkExistingDoses({inputData: wrapper})
        .then(result => {
            this.checkExistingDosesResponseHandler(result);
        })
        .catch(error => {
            this.showErrorMessage(error);
        });
    }
    /**
    * @description handle response of apexCall - saveAppointment
    * @author Balram Dhawan | 01-07-2022
    **/
    saveAppointmentResponseHandler(result) {
        if(result.missingExistingDoses && result.missingExistingDoses.length > 0) {
            var _authorizedBrands = result.authorizedBrands;
            _authorizedBrands.push({label: 'I received a different vaccine', value: 'Other'});
            this.vaccineClasses = _authorizedBrands;
            this.userRequestsForVaccineType = result.missingExistingDoses[0];
            this.previousVaccineType = result.missingExistingDoses[0];
            this.userEnteredDoseData = {};
            this.resetVaccineClassAndDate();
            this.setCurrentStep = 'enterExistingDoseInformation';
        }
        else if(!result.canScheduleBasedOnGroupAssignment || result.followUpDoseNotRequired) {
            this.setCurrentStep = 'notEligible';
        } 
        else if(result.privateAccessIds && result.privateAccessIds.length > 0) {
            this.userRequestsForVaccineType = result.vaccineType;
            this.latestCompletedAppointment = result.appointment;
            this.alreadyVaccinatedWithFirstDose = (this.latestCompletedAppointment.Event__r.Private_Access__r.Vaccine_Type__c !== 'Vaccination-1');
            this.requireNextDoseWindow = true;
            // this.setCurrentStep = 'eventSelector';
            this.setCurrentStep = 'waiver';
        } else {
            this.setCurrentStep = 'notEligible';
        }

        this.isLoading = false;
    }

    /**
    * @description handle response of apexCall - checkUserEligibility
    * @author Balram Dhawan | 01-07-2022
    **/
    checkUserEligibitlityResponseHandler(result) {
       //As result.preRegRecord returns a list so fetching the record present at index 0;
       this.preRegRecord = result.preRegRecord && result.preRegRecord.length >0 ?  result.preRegRecord[0] : this.preRegRecord;
        // If Profile Information is UpToDate
        if(result.needRescheduleWindow) {
            var pendingDosesArray = Object.keys(result.pendingDoses);
            this.disableNextDoseButton = (pendingDosesArray[pendingDosesArray.length-1] === result.appointmentToReschedule.Event__r.Private_Access__r.Vaccine_Type__c);
            this.alreadyVaccinatedWithFirstDose = (result.appointment !== undefined);
            this.appointmentToReschedule = result.appointmentToReschedule;
            this.latestCompletedAppointment = (this.appointmentToReschedule.Event__r.Private_Access__r.Vaccine_Type__c === 'Vaccination-1' && this.appointmentToReschedule.Status__c === 'Scheduled') ? result.appointmentToReschedule : result.appointment;
            this.setCurrentStep = 'rescheduleWindow';
        } else if(result.pendingDoses) {
            this.setCurrentStep = 'doseSelector';
        }
        this.createDoseTypePicklistValues(result);
        this.isLoading = false;
    }

    /**
    * @description handle response of apexCall - checkExistingDoses
    * @author Balram Dhawan | 01-07-2022
    **/
    checkExistingDosesResponseHandler(result) {
        // removed this condition ( || result.notCompletedRequiredInterval) w.r.t I-97954
        if(!result.canScheduleBasedOnGroupAssignment) {
            this.setCurrentStep = 'notEligible';
            this.isLoading = false;
            return;
        } 
        // removed this condition ( && !result.notCompletedRequiredInterval) w.r.t I-97954
        else if(this.selectedReschedule && result.canScheduleBasedOnGroupAssignment) {
            this.setCurrentStep = 'eventSelector';
        }
        if(!this.selectedReschedule){
            if(result.missingExistingDoses && result.missingExistingDoses.length > 0) {
                var _authorizedBrands = result.authorizedBrands;
                _authorizedBrands.push({label: 'I received a different vaccine', value: 'Other'});
                this.vaccineClasses = _authorizedBrands;
                this.setCurrentStep = 'enterExistingDoseInformation';
                this.previousVaccineType = result.missingExistingDoses[0];
            } else if(result.privateAccessIds && result.privateAccessIds.length > 0) {
                if(this.userRequestsForVaccineType === 'Vaccination-1') { // if Vaccination-1, flow should initiate from okcpRegistrationForm itSelf from okcpPreferenceComponentNew
                    var event = new CustomEvent('completed', {
                        detail: { value : this.patientId, forVaccination: true, privateAccessIds: result.privateAccessIds.join() }
                    });
                    this.dispatchEvent(event);
                } else {    // Flow should initiate from this component itself via okcpScheduleAppointment cmp
                    this.latestCompletedAppointment = result.appointment;
                    this.alreadyVaccinatedWithFirstDose = (this.latestCompletedAppointment.Event__r.Private_Access__r.Vaccine_Type__c !== 'Vaccination-1');
                    this.requireNextDoseWindow = true;
                    // this.setCurrentStep = 'eventSelector';
                    this.setCurrentStep = 'waiver';
                }
            } else {
                this.setCurrentStep = 'notEligible';
            }
        }
        this.isLoading = false;
    }

    handleUpdateDetails() {
        this.dispatchEvent(new CustomEvent('updatedetails', {
            bubbles: true, composed: true, detail: {
                patientId: this.patientId,
                // isDependentContact: true
            }
        }));
    }

    /**
    * @description send event back to parent okcpRegistrationForm
    * @author Balram Dhawan | 01-07-2022
    **/
    appointmentScheduledHandler(event) {
        this.dispatchEvent(new CustomEvent('flowcancelled'));
    }

    /**
    * @description create DoseType options E.g. First Dose, Second Dose, Third Dose
    * @author Balram Dhawan | 01-07-2022
    **/
    createDoseTypePicklistValues(result) {
        var data = result.pendingDoses;
        var completedDoses = result.completedDoses;
        var _vaccineTypes = JSON.parse(JSON.stringify(this.vaccineTypes));
        _vaccineTypes.forEach(element => {
            element.label = VACCINE_TYPE_VS_DOSE_NUMBER[element.value];
            if(data.hasOwnProperty(element.value)) {
                element.class = 'slds-checkbox';
                element.disabled = (completedDoses.length > 0) ? this.checkCompletedPreviousDoses(completedDoses, element.value) : false;
                element.name = 'doseType';
                element.labelClass = (element.disabled) ? 'slds-checkbox__label disabled' : 'slds-checkbox__label';
            } else {
                element.isCompleted = true;
                element.class = 'slds-checkbox completed';
                element.disabled = true;
                element.labelClass = 'slds-checkbox__label';
            }
        });
        this.pendingDoses = _vaccineTypes;
    }

    checkCompletedPreviousDoses(completedDoses, currentVaccineType) {
        var returnValue = false;
        var currentVaccineNumber = parseInt(currentVaccineType.split('-')[1]);
        for (let index = 0; index < completedDoses.length; index++) {
            var doseNumber = parseInt(completedDoses[index].split('-')[1]);
            if(currentVaccineNumber < doseNumber) {
                returnValue = true;
            }
        }
        return returnValue;
    }

    /**
    * @description closesModal and send event back to parent component okcpRegistrationForm and that shows the dashboard
    * @author Balram Dhawan | 01-07-2022
    **/
    handleModalCancel() {
        this.dispatchEvent(new CustomEvent('flowcancelled'));
    }

    /**
    * @description handles patient Change and update patientId to current Selected Patient
    * @author Balram Dhawan | 01-07-2022
    **/
    handlePatientChange(event) {
        this.missingProfileInformation = false;
        this.patientId = event.target.value;
    }

    /**
    * @description handles onchange event of checkboxes
    * @author Balram Dhawan | 01-07-2022
    **/
    handleChange(event) {
        this.handleValueSelectionClass(event.target.value);
        this.showVaccineDateField(event.target.value);
    }

    /**
    * @description toggle the active class on checkboxes
    * @author Balram Dhawan | 01-07-2022
    **/
    handleValueSelectionClass(selectedValue) {
        var allInputs = this.template.querySelectorAll('input');
        if(allInputs) {
            allInputs.forEach(element => {
                if(!element.disabled) {
                    if(element.value === selectedValue) {
                        element.parentElement.classList.add('active');
                    } else {
                        element.parentElement.classList.remove('active');
                        element.checked = false;
                    }
                }
            });
        }
    }

    /**
    * @description toggle the appropriate VaccineClass field on VaccineType selection
    * @author Balram Dhawan | 01-07-2022
    **/
    showVaccineDateField(selectedValue) {
        if(this.currentStep === 'enterExistingDoseInformationConfirmation' || this.currentStep === 'enterExistingDoseInformation') {
            var allInputs = this.template.querySelectorAll('lightning-input');
            allInputs.forEach(element => {
                if(element.dataset.name === selectedValue && selectedValue !== 'Other') {
                    element.style.display = 'block';
                } else {
                    element.value = undefined;
                    element.style.display = 'none';
                }
            });
        }
    }

    /**
    * @description handle confirmation of existing dose entered
    * @author Balram Dhawan | 01-07-2022
    **/
    handleDoseConfirmation() {
        this.openVaccineConfirmationModal = false;
        this.currentStep = 'enterExistingDoseInformationConfirmation';
        this.handleSubmit();
    }

    /**
    * @description closes the vaccine confirmation modal
    * @author Balram Dhawan | 01-07-2022
    **/
    closeModal() {
        this.openVaccineConfirmationModal = false;
    }

    /**
    * @description returns selected Vaccine class and date when user hits next on screen asking for existingDoseData 
    * @author Balram Dhawan | 01-07-2022
    **/
    getSelectedVaccineClassAndDate() {
        var allInputs = this.template.querySelectorAll('lightning-input, input');
        var selectedVaccineClass;
        var vaccineDate;
        if(allInputs) {
            allInputs.forEach(element => {
                if(element.checked && element.name === 'vaccineClass') {
                    selectedVaccineClass = element.value;
                } else if(element.value && element.name === 'vaccineDate') {
                    vaccineDate = element.value;
                }
            });
        }
        return {selectedVaccineClass : selectedVaccineClass, selectedVaccineDate: vaccineDate};
    }

    /**
    * @description reset values of vaccineClass and date to input next missing dose data
    * @author Balram Dhawan | 01-07-2022
    **/
    resetVaccineClassAndDate() {
        var allInputs = this.template.querySelectorAll('lightning-input, input');
        if(allInputs) {
            allInputs.forEach(element => {
                if(element.name === 'vaccineClass') {
                    element.parentElement.classList.remove('active');
                    element.checked = false;
                } else {
                    element.value = '';
                }
            });
        }
    }

    /**
    * @description helper of toast error
    * @author Balram Dhawan | 01-07-2022
    **/
    showErrorMessage(error) {
        var finalError = (error) ? ((error.message) ? error.message : ((error.body) ? ((error.body.message) ? error.body.message : JSON.stringify(error)) : JSON.stringify(error))) : "Something went wrong!";
        if(finalError === 'missingProfileInformation') {    // check to handleException regarding Incompletion Profile
            this.missingProfileInformation = true;
        } else {
            this.dispatchEvent(new ShowToastEvent({
                message: finalError,
                variant: 'error'
            }));
        }
        this.isLoading = false;
    }

    /**
    * @description helper of toast success
    * @author Balram Dhawan | 01-07-2022
    **/
    showSuccessMessage(message) {
        this.dispatchEvent(new ShowToastEvent({
            message: message,
            variant: 'success'
        }));
    }

    /**
    * @description focus first element just for accesability
    * @author Balram Dhawan | 01-07-2022
    **/
    focusFirstEle() {
        var focusableElements = 'lightning-combobox, lightning-button-icon, button, input, lightning-helptext, lightning-input, lightning-button ';
        var modal = this.template.querySelector('.slds-modal');
        var firstFocusableElement = modal.querySelectorAll(focusableElements)[0];
        var focusableContent = modal.querySelectorAll(focusableElements);
        var lastFocusableElement = focusableContent[focusableContent.length - 1];
        firstFocusableElement.focus();
        this.template.addEventListener('keydown', function(event) {
        var isTabPressed = event.key === 'Tab' || event.keyCode === 9;
            if (!isTabPressed) {
                return;
            }
            if (event.shiftKey) {               
                if (this.activeElement === firstFocusableElement) {
                   lastFocusableElement.focus(); 
                    event.stopPropagation();
                    event.preventDefault();
                }
            } else { 
                if (this.activeElement === lastFocusableElement) {    
                    firstFocusableElement.focus(); 
                    event.preventDefault();
                    event.stopPropagation();
                }
            }
        });
    }
}