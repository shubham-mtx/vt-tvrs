/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 12-30-2021
 * @last modified by  : Mohit Karani
**/
@IsTest
private class VTTS_PreRegHelperTest {
@TestSetup
    static void makeData(){

        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where Name = 'Testing Site Community Profile' limit 1]; 

        Account accountRecord = new Account();
        accountRecord.Name = 'Self Registration';
        accountRecord.Event_Process__c = 'Vaccination';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Test_Center').getRecordTypeId();
        INSERT accountRecord;

        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'Test';
        contactRecord.LastName = '00001';
        contactRecord.Birthdate = date.today()-10;
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord.Email = 'test@test.com';
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Consent__c = true;
        INSERT contactRecord;
        
        Contact contactRecord2 = new Contact();
        contactRecord2.FirstName = 'Test';
        contactRecord2.LastName = '00002';
        contactRecord2.Birthdate = date.today()-15;
        contactRecord2.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord2.Email = 'test2@test.com';
        contactRecord2.AccountId = accountRecord.Id;
        contactRecord2.Consent__c = true;
        INSERT contactRecord2;
        
        Contact contactRecord3 = new Contact();
        contactRecord3.FirstName = 'Test';
        contactRecord3.LastName = '00003';
        contactRecord3.Birthdate = date.today()-15;
        contactRecord3.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord3.Email = 'test3@test.com';
        contactRecord3.AccountId = accountRecord.Id;
        contactRecord3.Consent__c = true;
        INSERT contactRecord3;
        
        Contact contactRecord4 = new Contact();
        contactRecord4.FirstName = 'Test';
        contactRecord4.LastName = '00004';
        contactRecord4.Birthdate = date.today()-15;
        contactRecord4.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord4.Email = 'test4@test.com';
        contactRecord4.AccountId = accountRecord.Id;
        contactRecord4.Consent__c = true;
        INSERT contactRecord4;
        
        Contact contactRecord5 = new Contact();
        contactRecord5.FirstName = 'Test';
        contactRecord5.LastName = '00005';
        contactRecord5.Birthdate = date.today()-15;
        contactRecord5.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        contactRecord5.Email = 'test5@test.com';
        contactRecord5.AccountId = accountRecord.Id;
        contactRecord5.Consent__c = true;
        INSERT contactRecord5;

        User newUser = new User(
            profileId = prfile.id,
            username = 'testingUser@test103.com',
            email = 'pMyselfb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactRecord.Id
        );
        insert newUser;

        Private_Access__c privateAccess = new Private_Access__c(
        	Name = 'private test',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'Yes',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        insert privateAccess;
        
        Private_Access__c privateAccess5 = new Private_Access__c(
        	Name = 'private test',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'Yes',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        insert privateAccess5;
        
        Private_Access__c privateAccess6 = new Private_Access__c(
        	Name = 'private test',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'No',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        insert privateAccess6;
        
        Private_Access__c privateAccess2 = new Private_Access__c(
        	Name = 'private test2',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-2',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'Yes',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        insert privateAccess2;
        
        Private_Access__c privateAccess4 = new Private_Access__c(
        	Name = 'private test2',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-2',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'Yes',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        insert privateAccess4;
        
        Private_Access__c privateAccess3 = new Private_Access__c(
        	Name = 'private test',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Requires_Followup_Dose__c = 'No',
            Min__c = 1,
            Max__c = 2,
            Is_Active__c = true
        );
        insert privateAccess3;
        
        VTS_Event__c event = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccess.Id
        );
        insert event;
        
        VTS_Event__c event5 = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccess5.Id
        );
        insert event5;
        
        VTS_Event__c event6 = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccess6.Id
        );
        insert event6;
        
        VTS_Event__c event2 = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccess3.Id
        );
        insert event2;
        VTS_Event__c event4 = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccess4.Id
        );
        insert event4;
        
        VTS_Event__c event3 = new VTS_Event__c(
            Location__c = accountRecord.Id,
            Private_Access__c = privateAccess2.Id
        );
        insert event3;
        
        
        Appointment__c appointment = new Appointment__c(
        	Patient__c = contactRecord.Id,
            Status__c = 'Completed',
            Event__c = event.Id
        );
        insert appointment;
        
        Appointment__c appointment2 = new Appointment__c(
        	Patient__c = contactRecord2.Id,
            Status__c = 'Completed',
            Event__c = event2.Id
        );
        insert appointment2;
        
        Appointment__c appointment3 = new Appointment__c(
        	Patient__c = contactRecord2.Id,
            Status__c = 'Completed',
            Event__c = event3.Id
        );
        insert appointment3;
        
        Appointment__c appointment4 = new Appointment__c(
        	Patient__c = contactRecord3.Id,
            Status__c = 'Scheduled',
            Event__c = event4.Id
        );
        insert appointment4;
        
        Appointment__c appointment5 = new Appointment__c(
        	Patient__c = contactRecord4.Id,
            Status__c = 'Scheduled',
            Event__c = event5.Id
        );
        insert appointment5;
        
        Appointment__c appointment6 = new Appointment__c(
        	Patient__c = contactRecord5.Id,
            Status__c = 'Completed',
            Event__c = event6.Id
        );
        insert appointment6;
        
        Appointment_Slot__c appSlot = new Appointment_Slot__c(
            Account__c = accountRecord.Id
        );
        insert appSlot;
    }
    
	@IsTest
    private static void preRegEligiblityHelper() {
    	Contact c = [SELECT Id, Name FROM Contact LIMIT 1];
        
        Pre_Registration_Group__c prGroup = new Pre_Registration_Group__c();
        prGroup.Status__c = 'Activate';
        prGroup.Group_Type__c = 'Chronic';
        prGroup.Email_Acknowledgement__c = true;
        prGroup.Verification_Delay__c = 72;
        prGroup.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        insert prGroup;
        
        Pre_Registration_Group__c passCode = new Pre_Registration_Group__c();
        passCode.Status__c = 'Activate';
        passCode.Pre_Registration_Group__c = prGroup.Id;
        passCode.Passcode__c = 'health2134';
        passCode.Passcode_Type__c = 'Health';
        passCode.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Passcode').getRecordTypeId();
        insert passCode;

        Private_Access_Assignment__c paa = new Private_Access_Assignment__c();
        paa.Private_Access_Groups__c = [SELECT Id FROM Private_Access__c WHERE Name = 'private test' LIMIT 1]?.Id;
        paa.Pre_Registration_Groups__c = prGroup.Id;
        paa.Active__c = true;
        insert paa;
        
		Pre_Registration__c preReg = new Pre_Registration__c();
        // preReg.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preReg.Chronic_Condition__c = 'Yes';
        preReg.Contact__c = c.Id;
        preReg.Pass_Code_Health__c = 'health2134';
        preReg.Early_Group1_Health__c = prGroup.Id;
        preReg.Early_Group1_Health__r = prGroup;
       	preReg.Status__c = 'Pending Verification';
        insert preReg;
        
        Pre_Registration__c preReg1 = new Pre_Registration__c();
        // preReg1.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preReg1.Chronic_Condition__c = 'Yes';
        preReg1.Contact__c = c.Id;
        preReg1.Pass_Code_Health__c = 'health2134';
        preReg1.Early_Group1_Health__c = prGroup.Id;
        preReg1.Early_Group1_Health__r = prGroup;
       	preReg1.Status__c = 'Pending Verification';
        insert preReg1;
        
        Map<String, Object> result = new Map<String, Object>();
        Test.startTest();
        	VTTS_PreRegHelper.preRegEligiblityHelper(result, preReg);
            system.assertEquals(true, !result.isEmpty(), 'preRegEligiblityHelper');
        Test.stopTest();
    }
    
    @IsTest
    private static void preRegEligiblityHelperTest() {
    	Contact c = [SELECT Id, Name FROM Contact LIMIT 1];
        
        Pre_Registration_Group__c prGroup = new Pre_Registration_Group__c();
        prGroup.Status__c = 'Activate';
        prGroup.Group_Type__c = 'Chronic';
        prGroup.Email_Acknowledgement__c = true;
        prGroup.Verification_Delay__c = 72;
        prGroup.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        insert prGroup;
        
        Pre_Registration_Group__c passCode = new Pre_Registration_Group__c();
        passCode.Status__c = 'Activate';
        passCode.Pre_Registration_Group__c = prGroup.Id;
        passCode.Passcode__c = 'health2134';
        passCode.Passcode_Type__c = 'Health';
        passCode.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Passcode').getRecordTypeId();
        insert passCode;

        Private_Access_Assignment__c paa = new Private_Access_Assignment__c();
        paa.Private_Access_Groups__c = [SELECT Id FROM Private_Access__c WHERE Name = 'private test' LIMIT 1]?.Id;
        paa.Pre_Registration_Groups__c = prGroup.Id;
        paa.Active__c = true;
        insert paa;
        
		Pre_Registration__c preReg = new Pre_Registration__c();
        // preReg.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preReg.Chronic_Condition__c = 'Yes';
        preReg.Contact__c = c.Id;
        preReg.Pass_Code_Health__c = 'health2134';
        preReg.Early_Group1_Health__c = prGroup.Id;
        preReg.Early_Group1_Health__r = prGroup;
        
        Map<String, Object> result = new Map<String, Object>();
        Test.startTest();
        	VTTS_PreRegHelper.preRegEligiblityHelper(result, preReg);
            system.assertEquals(true, !result.isEmpty(), 'preRegEligiblityHelperTest');
        Test.stopTest();
    }
    
    @IsTest
    private static void preRegEligiblityHelperTestOne() {
    	Contact c = [SELECT Id, Name FROM Contact LIMIT 1];
        
        Pre_Registration_Group__c prGroup = new Pre_Registration_Group__c();
        prGroup.Status__c = 'Activate';
        prGroup.Group_Type__c = 'Chronic';
        prGroup.Email_Acknowledgement__c = true;
        prGroup.Verification_Delay__c = 72;
        prGroup.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        insert prGroup;
        
        Pre_Registration_Group__c passCode = new Pre_Registration_Group__c();
        passCode.Status__c = 'Activate';
        passCode.Pre_Registration_Group__c = prGroup.Id;
        passCode.Passcode__c = 'health2134';
        passCode.Passcode_Type__c = 'Health';
        passCode.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Passcode').getRecordTypeId();
        insert passCode;

        Private_Access_Assignment__c paa = new Private_Access_Assignment__c();
        paa.Private_Access_Groups__c = [SELECT Id FROM Private_Access__c WHERE Name = 'private test' LIMIT 1]?.Id;
        paa.Pre_Registration_Groups__c = prGroup.Id;
        paa.Active__c = true;
        insert paa;
        
		Pre_Registration__c preReg = new Pre_Registration__c();
        // preReg.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preReg.Chronic_Condition__c = 'Yes';
        preReg.Contact__c = c.Id;
        preReg.Pass_Code_Health__c = 'health2134';
        
        Map<String, Object> result = new Map<String, Object>();
        Test.startTest();
        	VTTS_PreRegHelper.preRegEligiblityHelper(result, preReg);
            system.assertEquals(true, !result.isEmpty(), 'preRegEligiblityHelperTestOne');
        Test.stopTest();
    }
    
    @IsTest
    private static void fetchPrivateAccessGroups() {
    	Contact c = [SELECT Id, Name FROM Contact LIMIT 1];
        User usr = [SELECT Id,Username FROM User WHERE Username ='testingUser@test103.com' LIMIT 1];

        Pre_Registration_Group__c prGroup = new Pre_Registration_Group__c();
        prGroup.Status__c = 'Activate';
        prGroup.Group_Type__c = 'Chronic';
        prGroup.Email_Acknowledgement__c = true;
        prGroup.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        insert prGroup;
        
        Pre_Registration_Group__c passCode = new Pre_Registration_Group__c();
        passCode.Status__c = 'Activate';
        passCode.Pre_Registration_Group__c = prGroup.Id;
        passCode.Passcode__c = 'health2134';
        passCode.Passcode_Type__c = 'Health';
        passCode.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Passcode').getRecordTypeId();
        insert passCode;

        Private_Access_Assignment__c paa = new Private_Access_Assignment__c();
        paa.Private_Access_Groups__c = [SELECT Id FROM Private_Access__c WHERE Name = 'private test' LIMIT 1]?.Id;
        paa.Pre_Registration_Groups__c = prGroup.Id;
        paa.Active__c = true;
        insert paa;
        
        Pre_Registration__c oldPreReg = new Pre_Registration__c();
        // oldPreReg.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        oldPreReg.Chronic_Condition__c = 'Yes';
        oldPreReg.Contact__c = c.Id;
        oldPreReg.Pass_Code_Health__c = 'health2134';
        oldPreReg.Early_Group1_Health__c = prGroup.Id;
        insert oldPreReg;
        
		Pre_Registration__c preReg = new Pre_Registration__c();
        // preReg.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preReg.Chronic_Condition__c = 'Yes';
        preReg.Contact__c = c.Id;
        preReg.Pass_Code_Health__c = 'health2134';
        preReg.Early_Group1_Health__c = prGroup.Id;
        
        Map<String, Object> result = new Map<String, Object>();
        result.put('canSchedule', true);
        result.put('groupIds', prGroup.Id);
        Test.startTest();
        System.runAs(usr) {
	        VTTS_PreRegHelper.loadExistingPreRegRecord(c.Id);
        	VTTS_PreRegHelper.fetchPrivateAccessGroups(result, 'Vaccination-1', 'Moderna');
            system.assertEquals(true, result.get('groupIds') != null, 'fetchPrivateAccessGroups');
        }
        Test.stopTest();
    }
}