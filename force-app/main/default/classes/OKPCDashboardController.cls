/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 01-21-2022
 * @last modified by  : Mohit Karani
**/
public without sharing class OKPCDashboardController {

    @AuraEnabled
    public static List<User> retrieveCurrentUser() {
        return [Select Id,ContactId,Contact.AccountId from user where Id =: UserInfo.getUserId() WITH SECURITY_ENFORCED];
    }

    @AuraEnabled
    public static List < SelectOptionWrapper > fetchPicklist(String objectName, String fieldName) {
        List < SelectOptionWrapper > opts = new List < SelectOptionWrapper > ();

        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult r = s.getDescribe();
        Map < String, Schema.SObjectField > fields = r.fields.getMap();
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();

        for (Schema.PicklistEntry pickListVal: ple) {
            opts.add(new SelectOptionWrapper(pickListVal.getValue(), pickListVal.getLabel()));
        }

        return opts;
    }

    @AuraEnabled
    public static User getCurrentUserDetail(){
        try {
            User u = [SELECT Id, Name, ContactId, Contact.AccountId FROM User WHERE Id =:UserInfo.getUserId() WITH SECURITY_ENFORCED];
            return u;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Map < String, Object > getAllPicklistValueForVaccine() {
        Map < String, Object > response = new Map < String, Object > ();

        response.put('VaccineQuestion_OutOfState__c', fetchPicklist('Appointment__c', 'VaccineQuestion_OutOfState__c'));
        response.put('VaccineQuestion_DoseNum__c', fetchPicklist('Appointment__c', 'VaccineQuestion_DoseNum__c'));
        response.put('VaccineQuestion_Allergy__c', fetchPicklist('Appointment__c', 'VaccineQuestion_Allergy__c'));
        response.put('VaccineQuestion_Allergy3a__c', fetchPicklist('Appointment__c', 'VaccineQuestion_Allergy3a__c'));
        response.put('VaccineQuestion_Allergy3b__c', fetchPicklist('Appointment__c', 'VaccineQuestion_Allergy3b__c'));
        response.put('VaccineQuestion_Allergy3c__c', fetchPicklist('Appointment__c', 'VaccineQuestion_Allergy3c__c'));
        response.put('VaccineQuestion_Covid19__c', fetchPicklist('Appointment__c', 'VaccineQuestion_Covid19__c'));
        response.put('VaccineQuestion_14Day__c', fetchPicklist('Appointment__c', 'VaccineQuestion_14Day__c'));
        response.put('VaccineQuestion_AntibodyTreatment90__c', fetchPicklist('Appointment__c', 'VaccineQuestion_AntibodyTreatment90__c'));
        response.put('VaccineQuestion_ImmuneSystem__c', fetchPicklist('Appointment__c', 'VaccineQuestion_ImmuneSystem__c'));
        response.put('VaccineQuestion_BleedingDisorder__c', fetchPicklist('Appointment__c', 'VaccineQuestion_BleedingDisorder__c'));
        response.put('VaccineQuestion_Breastfeeding__c', fetchPicklist('Appointment__c', 'VaccineQuestion_Breastfeeding__c'));
        response.put('tvrsVaccineTypsVsDoseNumberRecords', fetchTvrsVaccineTypsVsDoseNumberRecords());
        return response;
    }

    public static List<SelectOptionWrapper> fetchTvrsVaccineTypsVsDoseNumberRecords(){
        List < SelectOptionWrapper > opts = new List < SelectOptionWrapper > ();
        Map<String, object> vaccineTypeVsDoseNumberMap =  (Map<String, object>)JSON.deserializeUntyped(System.Label.TVRS_Vaccine_Type_Vs_Dose_Number);
        for(String vaccineType : vaccineTypeVsDoseNumberMap.keySet()){
            opts.add(new SelectOptionWrapper(vaccineType, String.valueOf(vaccineTypeVsDoseNumberMap.get(vaccineType))));

        }
        return opts;
    }
    @AuraEnabled
    public static Map < String, Object > getAllPicklistValueForDemographics() {
        Map < String, Object > response = new Map < String, Object > ();

        response.put('Gender__c', fetchPicklist('Appointment__c', 'Gender__c'));
        response.put('Primary_Language__c', fetchPicklist('Appointment__c', 'Primary_Language__c'));
        response.put('Race__c', fetchPicklist('Appointment__c', 'Race__c'));
        response.put('Ethnicity__c', fetchPicklist('Appointment__c', 'Ethnicity__c'));
        return response;
    }

    @AuraEnabled
    public static Map < String, Object > getAllPicklistValueTestingQuestions() {
        Map < String, Object > response = new Map < String, Object > ();

        response.put('First_COVID_19_Test__c', fetchPicklist('Appointment__c', 'First_COVID_19_Test__c'));
        response.put('Employed_in_Healthcare__c', fetchPicklist('Appointment__c', 'Employed_in_Healthcare__c'));
        response.put('Symptomatic_as_defined_by_CDC__c', fetchPicklist('Appointment__c', 'Symptomatic_as_defined_by_CDC__c'));
        response.put('Resident_in_Congregate_Care_Setting__c', fetchPicklist('Appointment__c', 'Resident_in_Congregate_Care_Setting__c'));
        response.put('Reason_for_Testing__c', fetchPicklist('Appointment__c', 'Reason_for_Testing__c'));
    
        return response;
    }

    public class SelectOptionWrapper {
        @AuraEnabled public string value;
        @AuraEnabled public string label;

        public SelectOptionWrapper(string value, string label) {
            this.value = value;
            this.label = label;
        }
    }


    public class PortalContent {

        @AuraEnabled public String id;
        @AuraEnabled public String message;
        @AuraEnabled public Decimal order;
        @AuraEnabled public Date startDate;
        @AuraEnabled public Date endDate;
        @AuraEnabled public String type;
        @AuraEnabled public Boolean isActive;
        
        PortalContent(TVRS_Portal_Content__c obj) {
            this.id = obj.Id;
            this.message = obj.Content__c;
            this.order = obj.Sequence__c;
            this.startDate = obj.Start_Date__c; 
            this.endDate = obj.End_Date__c;
            this.type = obj.Type__c;
            this.isActive = obj.Active__c;
        }
    }
    @AuraEnabled
    public static Map<String,List<PortalContent>> retrievePortalContent(){
        try {
            Map<String,List<PortalContent>> typeToPortalContent= new  Map<String,List<PortalContent>>();
            typeToPortalContent.put(VT_TS_Constants.IMPORTANT_LINKS_TEXT, new List<PortalContent>());
            typeToPortalContent.put(VT_TS_Constants.MAIN_SECTION_TEXT, new List<PortalContent>());
            typeToPortalContent.put(VT_TS_Constants.NEWS_FEED_TEXT, new List<PortalContent>());
            
            for(TVRS_Portal_Content__c portalContent : [SELECT Id,Sequence__c,Start_Date__c,End_Date__c,Type__c,Content__c,Active__c
                                                        FROM TVRS_Portal_Content__c
                                                        WHERE Active__c=true AND
                                                        Start_Date__c <= TODAY AND 
                                                        End_Date__c >= TODAY
                                                        WITH SECURITY_ENFORCED 
                                                        ORDER BY Sequence__c ASC NULLS LAST] ){

                PortalContent content = new PortalContent(portalContent);
                String contentType =content.type;
                if(contentType==VT_TS_Constants.IMPORTANT_LINKS_TEXT && typeToPortalContent.get(contentType).size()>=10) {
                    continue;
                }
                typeToPortalContent.get(contentType).add(content);
            }
            return typeToPortalContent;
        }
        catch(Exception e) {
            throw new AuraHandledException('Error While Retrieving Links and  News Feed');
        }
    }



    @AuraEnabled
    public static Boolean isPrivateAccessObject(String eventId){
        Boolean result = false;
        try {
            List<Private_Access__c> records = [SELECT Id FROM Private_Access__c WHERE Id =:eventId AND Event_Process__c = 'Vaccination' WITH SECURITY_ENFORCED];
            if(!records.isEmpty()) {
                result = true;
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return result;
    }

    public class Testing {
        @AuraEnabled public string testingId { get; set; }
        @AuraEnabled public string labName { get; set; }
        @AuraEnabled public string result { get; set; }
        @AuraEnabled public string specimen { get; set; }
        @AuraEnabled public datetime createddate { get; set; }
        @AuraEnabled public boolean isAfter24Hours { get; set; }
        @AuraEnabled public boolean isPositiveResult { get; set; }

    }

     /**
    * @description newVersion of retrieveResults
    * @author Balram Dhawan | 12-24-2021 
    * @return Map<String, Object> 
    **/
    @AuraEnabled
    public static Map<String, Object> getAppointments(String request){
        try {
            Map<String, Object> response = new Map<String, Object>();
            response.put('request', request);
            User currentUser = [SELECT Id, ContactId FROM User WHERE Id =:UserInfo.getUserId() WITH SECURITY_ENFORCED];
            String contactId = currentUser.ContactId;
            String citizenRecTypeId = VT_TS_Constants.CITIZEN_COVID_RECORD_TYPE_ID;
            TVRS_Wrapper.FilterWrapper wrapper;
            String query ='SELECT Id, MobilePhone, Email, Auto_Scheduled__c, Name, MailingStreet, MailingCity, MailingState, MailingPostalCode,'+ 
                'Patient_Id__c, Parent_Contact__c, Birthdate,';
            query +=  '(SELECT Id, Laboratory__c, Laboratory__r.Name, Createddate, Results__c, Source_of_Specimen__c, '+ 
                        'Result_Upload_Date__c, Appointment__c'+ 
                        ' FROM Antibodies_Testing__r)'+     
                        ' FROM Contact '+ 
                        ' WHERE RecordTypeId =: citizenRecTypeId '+ 
                        ' AND (Id =:contactId '+ 
                        ' OR Parent_Contact__c =:contactId) '+ 
                        ' WITH SECURITY_ENFORCED '+ 
                        ' ORDER BY CreatedDate ASC';
            List<Contact>contacts =  Database.query(query);
            response.put('contact', contacts);
            if(contacts.isEmpty()){
                return response;
            }
            String appointmentQuery = 'SELECT Id, Appointment_Start_Date_v1__c, Appointment_Start_Time_v1__c, Status__c, Patient__r.Testing_Site__c,'+ 
            'Lab_Center__r.Name, Lab_Center__r.BillingStreet,Lab_Center__r.BillingCity,Event_Description__c,'+ 
            'Lab_Center__r.BillingState,Lab_Center__r.BillingPostalCode, Appointment_Slot__r.Start_Time__c, '+ 
            'Appointment_Slot__r.Date__c, Patient__r.Preferred_Date__c, Patient__r.Preferred_Time__c, Patient__r.Auto_Scheduled__c,'+ 
            'Patient__r.Name, Patient__c, Lab_Center__c, Lab_Center__r.Event_Process__c, Event_Process__c, '+ 
            'Event__r.Private_Access__r.Vaccine_Type__c, Event__r.Private_Access__r.Requires_Followup_Dose__c,'+ 
            'Event__r.Private_Access__r.Age_Based__c, Event__r.Private_Access__r.Vaccine_Class_Name__c,'+ 
            'Event__r.Private_Access__r.Min__c,Event__r.Private_Access__r.Weak_Immune_Min_Days__c,Appointment_Complete_Date__c,Appointment_Date__c,Appointment_Date_Time_Complete__c,Appointment_Start_Date_Time__c'+ 
            ' FROM Appointment__c '+ 
            ' WHERE Patient__c IN : contacts'+
            ' AND Event__c != null '+
            ' AND  Status__c != \'\''+
            ' AND Status__c != \'Draft\'';
            if(String.isNotBlank(request)){
                wrapper  = (TVRS_Wrapper.FilterWrapper)JSON.deserialize(request, TVRS_Wrapper.FilterWrapper.class);
                
            }
            if(wrapper!= null && !wrapper.listStatus.isEmpty()){
                List<String> statusList = wrapper.listStatus;
                appointmentQuery+=' AND  Status__c IN: statusList';
            }
            if(wrapper!= null &&  !wrapper.type.isEmpty()){
                List<String> typeList = wrapper.type;
                appointmentQuery+=' AND Event_Process__c IN : typeList ';
            }
            if(wrapper!= null && wrapper.days == '30'){
                appointmentQuery+=' AND Appointment_Date__c >= LAST_N_DAYS:30 ';
            }
            if(wrapper!= null && wrapper.days == '60'){
                appointmentQuery+=' AND Appointment_Date__c >= LAST_N_DAYS:60 ';
            }
            appointmentQuery+=' WITH SECURITY_ENFORCED';
            if(wrapper!= null && String.isNotBlank(wrapper.sortValue) && wrapper.sortValue.trim() != null){
                String sortValue = String.escapeSingleQuotes(wrapper.sortValue);
                appointmentQuery= (sortValue =='Appointment_Date_Time_Complete__c') ? appointmentQuery + ' ORDER BY  Appointment_Start_Date_Time__c ASC NULLS LAST,Appointment_Date_Time_Complete__c ASC NULLS LAST' :  appointmentQuery + ' ORDER BY ' + sortValue + ' ASC,Appointment_Start_Date_Time__c ASC NULLS LAST,Appointment_Date_Time_Complete__c ASC NULLS LAST' ;
            }
            else{
                appointmentQuery += ' ORDER BY Event__r.Private_Access__r.Vaccine_Type__c DESC';
            }
            List<Map<String, Object>> appointmentList = new List<Map<String, Object>>();
            for(Appointment__c app : Database.query(appointmentQuery)) {
                Map<String, Object> appMap = new Map<String, Object>();
                appMap.putAll(app.getPopulatedFieldsAsMap());
                String formattedDate = (app.Appointment_Start_Date_Time__c != null) ? app.Appointment_Start_Date_Time__c.format() :(app.Appointment_Date_Time_Complete__c != null) ? app.Appointment_Date_Time_Complete__c.format() : null;
                formattedDate = (formattedDate != null) ? formattedDate.substringBefore(' ') +', ' +formattedDate.substringAfter(' ') : null;
                appMap.put('formattedDate',formattedDate);
                appointmentList.add(appMap);
            }
            response.put('appointment', appointmentList);
            response.putAll(addAppoinmentsAndResultsForContact(response));
            return response;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    public static Map<String,Object> addAppoinmentsAndResultsForContact(Map<String,Object> response){
        List<Antibody_Testing__c> resultsList = new List<Antibody_Testing__c>();
        for(Contact contactRec : (List<Contact>)response.get('contact')){
            resultsList.addAll(contactRec.Antibodies_Testing__r);
        }
        Map<Id, Testing> appointmentToTestingMap = new Map<Id, Testing>();
        for(Antibody_Testing__c testing: resultsList) {
            Testing testingRecord = new Testing();
            testingRecord.testingId = testing.Id;
            testingRecord.labName = String.isNotBlank(testing.Laboratory__c) ? testing.Laboratory__r.Name : '';
            testingRecord.result = String.isNotBlank(testing.Results__c) ? testing.Results__c : '';
            testingRecord.specimen = String.isNotBlank(testing.Source_of_Specimen__c) ? testing.Source_of_Specimen__c : '';
            testingRecord.createddate = testing.Createddate;
            Datetime after24HoursDate = (testing.Result_Upload_Date__c != null) ? testing.Result_Upload_Date__c.addHours(24) : null;
            testingRecord.isAfter24Hours = (testing.Result_Upload_Date__c != null && DateTime.now() >= after24HoursDate) ? true : false;
            testingRecord.isPositiveResult = testing.Results__c == 'Positive' ? true : false;
            appointmentToTestingMap.put(testing.Appointment__c, testingRecord);
        }
        if(!appointmentToTestingMap.keySet().isEmpty()) {
            response.put('results', appointmentToTestingMap);
        }
        return response;
    }
}