/**
 * @description       : 
 * @author            : Balram Dhawan
 * @group             : 
 * @last modified on  : 01-17-2022
 * @last modified by  : Balram Dhawan
**/
import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import scheduleAppointment from "@salesforce/apex/DC_ScheduleAppointment.updateAppointmentWithEvent";

import getAppointmentEventType from "@salesforce/apex/OKPC_LocationMapController.getAppointmentEventTypeNew";
import schedule2ndDoseAppointment from "@salesforce/apex/DC_ScheduleAppointment.scheduleSecondDoseAppointment";
import{
    showMessage,
    msgObj,
    showAsyncErrorMessage
}from 'c/vtTsUtility';
export default class OkpcScheduleAppointment extends LightningElement {
    @api isCitizenPortal;               // only available from CitizenPortal only    at 2022-01-06 18:15:17  w.r.t S-23914 
    @api doseType;                      // only available from CitizenPortal only    at 2022-01-06 18:15:17  w.r.t S-23914 
    @api latestCompletedAppointment;    // only available from CitizenPortal only    at 2022-01-07 10:55:32  w.r.t S-23914 
    @api appointmentToReschedule;       // only available from CitizenPortal only    at 2022-01-07 10:55:32  w.r.t S-23914 
    @api isImmuneWeak;                  // only available from CitizenPortal only    at 2022-01-07 10:55:32  w.r.t S-23914 
    @api contactId;                     // only available from CitizenPortal only    at 2022-01-07 10:55:32  w.r.t S-23914 

    @api accountId;
    @api appointmentId;
    @api eventProcess;
    @api oldVaccineClass;
    @api eventIds;
    @api isSecondDose = false;
    @api isRescheudle;
    @api preRegDateTime;

    
    @track vaccinationType;
    @track isLoaded = false;
    filterValues;
    currentStep = 1;
    eventId;
    slotId;
    registrationComplete = false;
    isShowSpinner = false;
    secondDoseData;
    msgObjData = msgObj();
    isFirstAppointment;
    get isStep1() {
        return this.currentStep === 1;
    }

    get isStep2() {
        return this.currentStep === 2;
    }

    get modalHeader() {
        if( this.eventProcess ){
            return this.eventProcess.includes('Vaccination') ? 'Select Vaccination Event' : 'Select Testing Event';
        }
        else{
            return this.isStep1 ? 'Select Event' : 'Select Appointment Slot';
        }
    }

    get isNextEnabled() {
        return !((this.isStep1 && this.accountId && this.eventId) || (this.isStep2 && this.slotId));
    }

    handleClick(event) {
        switch (event.target.name) {
            case 'previous':
                this.currentStep--;
                if (this.isStep1) {
                    this.slotId = '';
                   
                }
                
                break;
            case 'next':
                this.currentStep++;
                break;
            case 'cancel':
                this.dispatchEvent(new CustomEvent('closeappointment', { detail: 'cancel' }));
                break;
            case 'save':
                if(this.slotId) {
                    this.scheduleAppointment();
                } else {
                    this.dispatchEvent(new ShowToastEvent({
                        message: 'Please select a time slot to proceed',
                        variant: 'error'
                    }));
                }
                break;

            default:
                break;
        }
    }

    connectedCallback(){

        if( this.eventProcess == 'Vaccination' ){
            // w.r.t S-23914
            if(this.isCitizenPortal) {
                this.vaccinationType = this.doseType;
                this.isFirstAppointment = this.doseType === 'Vaccination-1';
                var secondDoseDataTemp = {};
                if(this.latestCompletedAppointment) {
                    secondDoseDataTemp = {...this.latestCompletedAppointment.Event__r.Private_Access__r};
                    if(this.isImmuneWeak && (this.vaccinationType != 'Vaccination-2' && this.vaccinationType != 'Vaccination-1' || this.latestCompletedAppointment.Event__r.Private_Access__r.Vaccine_Class_Name__c === 'Janssen')) {
                        secondDoseDataTemp.Min__c = secondDoseDataTemp.Weak_Immune_Min_Days__c;
                        secondDoseDataTemp.Max__c = secondDoseDataTemp.Weak_Immune_Max_Days__c;
                    }
                    // Use Appointment Date instead of Completed Appointment
                    secondDoseDataTemp.Appointment_Complete_Date__c = this.latestCompletedAppointment.Appointment_Date__c;
                    secondDoseDataTemp.oldVaccineClass = this.latestCompletedAppointment.Event__r.Private_Access__r.Vaccine_Class_Name__c;
                } else {
                    if(this.isRescheudle) {
                        this.isFirstAppointment = true;
                    }
                }
                secondDoseDataTemp.contactId = this.contactId;
                secondDoseDataTemp.currentAppointmentVaccineClass = (this.isRescheudle) ? this.appointmentToReschedule.Event__r.Private_Access__r.Vaccine_Class_Name__c : undefined;
                this.secondDoseData = secondDoseDataTemp;
                this.isLoaded = true;
            } else {
                getAppointmentEventType({
                    appointmentId: this.appointmentId
                }).then(result => {
                    if(result.success) {
                        let secondDoseDataTemp = {...result.appointment.Event__r.Private_Access__r};
                        let nextValidDoseNumber = result.nextValidDoseNumber;
                        if(result.appointment.Status__c === 'Scheduled' || result.appointment.Status__c === 'Cancelled') {
                            nextValidDoseNumber = result.appointment.Event__r.Private_Access__r.Vaccine_Type__c;
                        } 
                        // w.r.t S-23914  at 2022-01-06 18:15:17
                        if(this.isCitizenPortal) {
                            this.vaccinationType = this.doseType;
                        } else {
                            this.vaccinationType = (result.isFirstAppointmentRes && this.isRescheudle) ? 'Vaccination-1' : nextValidDoseNumber;
                        }
                        if(result.isImmuneWeak === 'Yes' && this.vaccinationType != 'Vaccination-2') {
                            secondDoseDataTemp.Min__c = secondDoseDataTemp.Weak_Immune_Min_Days__c;
                            secondDoseDataTemp.Max__c = secondDoseDataTemp.Weak_Immune_Max_Days__c;
                        }
                        this.isFirstAppointment = (!result.appointmentListSize || result.appointmentListSize == 0 || (result.appointmentListSize == 1 && result.appointment.Status__c === 'Scheduled'));
                        // Use Appointment Date instead of Completed Appointment
                        secondDoseDataTemp.Appointment_Complete_Date__c = result.appointment.Appointment_Date__c;
                        secondDoseDataTemp.oldVaccineClass = result.appointment.Event__r.Private_Access__r.Vaccine_Class_Name__c;
                        secondDoseDataTemp.contactId = (result && result.appointment) ? result.appointment.Patient__c : null;
                        secondDoseDataTemp.currentAppointmentVaccineClass = result.vaccineClass;
                        this.secondDoseData = secondDoseDataTemp;
                    } else {
                        this.dispatchEvent(new ShowToastEvent({
                            message: 'It looks like you have completed all required vaccine doses',
                            variant: 'error'
                        }));
                    }
                    this.isLoaded = true;
                }).catch(error => {
                    showAsyncErrorMessage(this, error);
                });
            }
        }
        else{
            this.isLoaded = true;
        }

        setTimeout(() => {
            this.focusFirstEle();
        }, 500);
    }

    handleEventSelected(event) {
        this.accountId = event.detail.accountId;
        this.eventId = event.detail.eventId;
       
    }

    handleSlotSelected(event) {
        this.slotId = event.detail;
    }

    scheduleAppointment() {
        this.isShowSpinner = true;
        
        if ( !this.isRescheudle ) {
            schedule2ndDoseAppointment({
                accountId: this.accountId,
                appointmentId: this.appointmentId,
                slotId: this.slotId,
                eventId: this.eventId
            })
            .then(result => {
                if(result.alreadyHaveAppointment || result.isSelectedDoseAlreadyScheduled){     // w.r.t S-23914
                    showMessage(this,'Error!','error',this.msgObjData['alreadyHaveAppointment']);
                    this.isShowSpinner = false;
                    return false;
                }
                if(result.slotIssue) {
                    
                    showMessage(this,'Error!','error',this.msgObjData['slotIssue']);
                    this.isShowSpinner = false;
                    return false;
                }
                if(result.is3rdDoseAlreadyScheduled || result.is2ndDoseAlreadyScheduled || result.furtherDosesNotReqd || result.isAlreadyScheduled || result.isAlreadyCompleted || result.isSelectedDoseAlreadyCompleted) {
                    if(result.furtherDosesNotReqd) {
                        showMessageWithLink(this,'error',this.msgObjData['furtherDosesNotReqd'],this.msgObjData['vermontMsgData']);            
                    }
                   
                    this.isShowSpinner = false;
                    return false;
                } else if(result.success) {
                    this.dispatchEvent(new ShowToastEvent({
                        title: 'Success!',
                        variant: 'success',
                        message: 'Your appointment has been scheduled',
                    }));
                    this.dispatchEvent(new CustomEvent('closeappointment', { detail: 'save' }));
                }
            })
            .catch(error => {
                this.isShowSpinner = false;
                showAsyncErrorMessage(this, error);
            });
        } else {
            scheduleAppointment({
                accountId: this.accountId,
                appointmentId: this.appointmentId,
                slotId: this.slotId,
                eventId: this.eventId
            }).then(result => {
                if(result.slotIssue) {
                    this.showToast('error', 'This particular slot has already been booked by someone else. Please choose a different timeslot');
                    this.isShowSpinner = false;
                    return false;
                }
                // updated By Mohit Karani for Booster Dose.
                if(result.is3rdDoseAlreadyScheduled || result.is2ndDoseAlreadyScheduled || result.furtherDosesNotReqd || result.isAlreadyScheduled || result.isAlreadyCompleted || result.isSelectedDoseAlreadyCompleted) {
                     if(result.furtherDosesNotReqd) {
                        showMessageWithLink(this,'error',this.msgObjData['furtherDosesNotReqd'],this.msgObjData['vermontMsgData']);
                        
                    }
                    else if(result.is3rdDoseAlreadyScheduled) {
                        
                        showMessage(this,'Error!','error',this.msgObjData['is3rdDoseAlreadyScheduled']);
                    }
                    else if(result.is2ndDoseAlreadyScheduled) {
                       
                        showMessage(this,'Error!','error',this.msgObjData['is2ndDoseAlreadyScheduled']);
                    } else if(result.isAlreadyScheduled){
                        
                        showMessage(this,'Error!','error',this.msgObjData['isAlreadyScheduled']);
                    }else if(result.isSelectedDoseAlreadyCompleted){
                        showMessage(this,'Error!','error',this.msgObjData['isSelectedDoseAlreadyCompleted']);
                        
                    }  
                    else if(result.isAlreadyCompleted){
                        showMessage(this,'Error!','error',this.msgObjData['isAlreadyCompleted']);
                        
                    }
                    this.isShowSpinner = false;
                    return false;
                }

                if (result.type == 'error') {
                    this.dispatchEvent(new ShowToastEvent({
                        title: 'Error!',
                        variant: 'error',
                        message: result.message,
                    }));
                } else if (result.type == 'success') {
                    this.dispatchEvent(new ShowToastEvent({
                        title: 'Success!',
                        variant: 'success',
                        message: result.message,
                    }));
                    this.dispatchEvent(new CustomEvent('closeappointment', { detail: 'save' }));
                }
                this.isShowSpinner = false;
            }).catch(error => {
                showAsyncErrorMessage(this, error);
                this.isShowSpinner = false;
            });   
        }
    }

    showToast(type, msg, messageData) {
        if(messageData) {
            this.dispatchEvent(new ShowToastEvent({
                message: msg,
                variant: type,
                messageData : messageData
            }));
        } else {
            this.dispatchEvent(new ShowToastEvent({
                message: msg,
                variant: type
            }));
        }
    }

    handleKeyDown(event) {
        if(event.code === 'Escape') {
            this.dispatchEvent(new CustomEvent('closeappointment', { detail: 'cancel' }));
            event.preventDefault();
            event.stopImmediatePropagation();
        }
    }

    closeModal() {
        this.dispatchEvent(new CustomEvent('closeappointment', { detail: 'cancel' }));
    }

    focusFirstEle() {
        const focusableElements = 'lightning-combobox, lightning-button-icon, button, input, lightning-helptext, lightning-input, lightning-button';
        const modal = this.template.querySelector('.slds-modal');
        if(modal) {
            const firstFocusableElement = modal.querySelectorAll(focusableElements)[0];
            const focusableContent = modal.querySelectorAll(focusableElements);
            const lastFocusableElement = focusableContent[focusableContent.length - 1];
            firstFocusableElement.focus();
            this.template.addEventListener('keydown', function(event) {
            let isTabPressed = event.key === 'Tab' || event.keyCode === 9;
                if (!isTabPressed) {
                    return;
                }
                if (event.shiftKey) {               
                    if (this.activeElement === firstFocusableElement) {
                       lastFocusableElement.focus(); 
                        event.stopPropagation()
                        event.preventDefault();
                    }
                } else { 
                    if (this.activeElement === lastFocusableElement) {    
                        firstFocusableElement.focus(); 
                        event.preventDefault();
                        event.stopPropagation()
                    }
                }
            });
        }
    }
    
    showToast(type, msg) {
        this.dispatchEvent(new ShowToastEvent({
            message: (type === 'error') ? msg.body.message : msg,
            variant: type
        }));
    }
}