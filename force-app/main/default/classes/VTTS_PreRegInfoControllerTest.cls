/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 10-04-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
@IsTest
private class VTTS_PreRegInfoControllerTest {
    @testSetup
    private static void makeData() {
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        String citizenCovidId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        String laboratoryRecTypeId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
        //String labUserRecTypeId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Lab_User').RecordTypeId;
        

        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Event_Process__c = 'Testing Site';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;
        
        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-44151',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = citizenCovidId,
            what_was_the_last_date_of_contact__c = Date.newInstance(2020, 2, 20),
            Did_you_have_a_nasal_swab__c = 'Yes	',
            What_was_your_test_result__c = 'Negative',
            Approximately_what_date_were_you_tested__c = Date.newInstance(2020, 3, 3),
            Had_contact_with_anyone_diagon__c = 'Yes',
            Do_you_take_prescription_medications__c = '	Yes',
            medication_for_fever_in_the_past_3_days__c = 'Yes',
            First_Date_you_have_been_feeling_sick__c = Date.newInstance(2020, 3, 1) ,
            First_date_you_began_feeling_well__c= Date.newInstance(2020, 3, 10),
            Fever_Cough_or_Difficulty_breathing__c = 'Yes',
            Sick_since_Jan_1_2020__c = 'Yes',
            Opt_out_for_SMS_Updates__c = true,
            HasOptedOutOfEmail = false,
            Current_Step__c = 1,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con;
        
        Pre_Registration__c pr = new Pre_Registration__c();
        pr.Pass_Code_Health__c = 'heatlh123';
        pr.Pass_Code_Risk_Group__c = 'risk1234';
        pr.Contact__c = [SELECT Id FROM Contact LIMIT 1]?.Id;
        insert pr;


        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType IN : customerUserTypes  limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'portalUser@test123.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            //userRoleId = portalRole.Id,
            contactId = con.id
        );
        insert newUser;

    }

    @IsTest
    private static void getRecordNegative() {
        try {
        Contact c = VTTS_PreRegInfoController.getPreRegRecord([SELECT Id FROM Contact WHERE Name = 'xyz' LIMIT 1]?.Id);
        }
        catch(Exception err) {
            system.assert(true,err.getMessage());
        }
    }
    
    @IsTest
    private static void emptyRecordID() {
        try {
            Contact c = VTTS_PreRegInfoController.getPreRegRecord('');
        }
        catch(Exception er) {
            system.assert(true,'getRecordNegative');
        }
    }
    @IsTest
    private static void getRecordPositive() {
        Pre_Registration__c pr = new Pre_Registration__c();
        try {
        pr.Contact__c = [SELECT Id FROM Contact LIMIT 1]?.Id;
        Contact c = VTTS_PreRegInfoController.getPreRegRecord([SELECT Id FROM Contact LIMIT 1]?.Id);
        }
        catch(Exception err) {
          system.assert(true,err.getMessage());
        }
    }
    
    @IsTest
    private static void getAgeBasedTest() {
        Date dateNewObject = System.today();
        dateNewObject = dateNewObject.addYears(-40);
        Map<String, Object> mapOfObjects = VTTS_PreRegInfoController.getAgeBasedGroup(dateNewObject, null);
        system.assertEquals(true, !mapOfObjects.isEmpty(), 'getAgeBasedTest');
    }



    @IsTest
    private static void getAgeBasedTestWithContact_Case1() {
        String groupRecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.RecordTypeInfosByDeveloperName.get('Pre_Registration_Group').RecordTypeId;
        Date dateNewObject = System.today();
        dateNewObject = dateNewObject.addYears(-40);
        String contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        
        Account acc = [select id from Account limit 1];
        //String labUserRecTypeId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Lab_User').RecordTypeId;

        Private_Access__c privateAccess2 = new Private_Access__c(
        	Name = 'private test2',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Is_Active__c = true,
            Requires_Followup_Dose__c = 'Yes',
            Max__c = 2,
            Min__c = 1
        );
        insert privateAccess2;
        
        Private_Access__c privateAccess3 = new Private_Access__c(
        	Name = 'private test3',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-2',
            Age_Based__c = 44,
            Is_Active__c = true,
            Requires_Followup_Dose__c = 'Yes',
            Max__c = 2,
            Min__c = 1
        );
        insert privateAccess3;
        
        VTS_Event__c event = new VTS_Event__c(
            Location__c = acc.Id,
            Private_Access__c = privateAccess2.Id
        );
        insert event;
        
        VTS_Event__c event2 = new VTS_Event__c(
            Location__c = acc.Id,
            Private_Access__c = privateAccess3.Id
        );
        insert event2;
        
        Appointment__c appointment = new Appointment__c(
        	Patient__c = contactId,
            Status__c = 'Scheduled',
            Event__c = event.Id 
        );
        insert appointment;

        Pre_Registration_Group__c prg = new Pre_Registration_Group__c(
            Name='Test Group',
            Group_Description__c='This is a test Group',
            Estimated_Date__c = Date.newInstance(2020,3,1),
            Status__c='Activate',
            Beginning_DOB__c=Date.newInstance(1981,5,1),
            Ending_DOB__c=Date.newInstance(1990,10,1),
            Group_Type__c='Age-Based',
            RecordTypeId=groupRecordTypeId,
            Email_Acknowledgement__c=true
        );
        insert prg;
        Test.startTest();
            Map<String, Object> mapOfObjects = VTTS_PreRegInfoController.getAgeBasedGroup(dateNewObject, contactId);
            system.assertEquals(true, !mapOfObjects.isEmpty(), 'getAgeBasedTestWithContact');
        Test.stopTest();
    }

    @isTest
    private static void getAgeBasedTestWithContact_Case2() {
        
        Date dateNewObject = System.today();
        dateNewObject = dateNewObject.addYears(-40);
        String contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        
        Account acc = [select id from Account limit 1];
        //String labUserRecTypeId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Lab_User').RecordTypeId;

        Private_Access__c privateAccess2 = new Private_Access__c(
        	Name = 'private test2',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Is_Active__c = true,
            Requires_Followup_Dose__c = 'Yes',
            Max__c = 2,
            Min__c = 1
        );
        insert privateAccess2;
        
        Private_Access__c privateAccess3 = new Private_Access__c(
        	Name = 'private test3',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-2',
            Age_Based__c = 44,
            Is_Active__c = true,
            Requires_Followup_Dose__c = 'Yes',
            Max__c = 2,
            Min__c = 1
        );
        insert privateAccess3;
        
        VTS_Event__c event = new VTS_Event__c(
            Location__c = acc.Id,
            Private_Access__c = privateAccess2.Id
        );
        insert event;
        
        VTS_Event__c event2 = new VTS_Event__c(
            Location__c = acc.Id,
            Private_Access__c = privateAccess3.Id
        );
        insert event2;
        
        Appointment__c appointment = new Appointment__c(
        	Patient__c = contactId,
            Status__c = 'Scheduled',
            Event__c = event.Id 
        );
        insert appointment;


        Test.startTest();
            Map<String, Object> mapOfObjects = VTTS_PreRegInfoController.getAgeBasedGroup(dateNewObject, contactId);
            system.assertEquals(true, !mapOfObjects.isEmpty(), 'getAgeBasedTestWithContact');
        Test.stopTest();
    }

    @isTest
    private static void getAgeBasedTestWithContact_Case3() {
        
        Date dateNewObject = System.today();
        dateNewObject = dateNewObject.addYears(-40);
        String contactId = [SELECT Id FROM Contact LIMIT 1]?.Id;
        
        Account acc = [select id from Account limit 1];
        //String labUserRecTypeId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Lab_User').RecordTypeId;

        Private_Access__c privateAccess2 = new Private_Access__c(
        	Name = 'private test2',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Age_Based__c = 44,
            Is_Active__c = true,
            Requires_Followup_Dose__c = 'Yes',
            Max__c = 2,
            Min__c = 1
        );
        insert privateAccess2;
        
        Private_Access__c privateAccess3 = new Private_Access__c(
        	Name = 'private test3',
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-2',
            Age_Based__c = 44,
            Is_Active__c = true,
            Requires_Followup_Dose__c = 'Yes',
            Max__c = 2,
            Min__c = 1
        );
        insert privateAccess3;
        
        VTS_Event__c event = new VTS_Event__c(
            Location__c = acc.Id,
            Private_Access__c = privateAccess2.Id
        );
        insert event;
        
        VTS_Event__c event2 = new VTS_Event__c(
            Location__c = acc.Id,
            Private_Access__c = privateAccess3.Id
        );
        insert event2;
        
        Appointment__c appointment = new Appointment__c(
        	Patient__c = contactId,
            Status__c = 'Completed',
            Event__c = event.Id 
        );
        insert appointment;

 
        Test.startTest();
            Map<String, Object> mapOfObjects = VTTS_PreRegInfoController.getAgeBasedGroup(dateNewObject, contactId);
            system.assertEquals(true, !mapOfObjects.isEmpty(), 'getAgeBasedTestWithContact');
        Test.stopTest();
    }


    public static  void prepareData() {
        // Record Type
        String citizenCovidId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        
        // create Account 
        Account acc = [SELECT Id FROM Account WHERE Name='TestSite'];

        // create Contact;
        Contact con = new Contact (
            AccountId = acc.Id,
            LastName = 'DemoUser123Test',
            MiddleName = 'ABC',
            FirstName = 'DemoPortalUser',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-44151',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = '12311',
            Testing_Site__c = acc.Id,
            RecordTypeId = citizenCovidId,
            what_was_the_last_date_of_contact__c = Date.newInstance(2020, 2, 20),
            Did_you_have_a_nasal_swab__c = 'Yes	',
            What_was_your_test_result__c = 'Negative',
            Approximately_what_date_were_you_tested__c = Date.newInstance(2020, 3, 3),
            Had_contact_with_anyone_diagon__c = 'Yes',
            Do_you_take_prescription_medications__c = '	Yes',
            medication_for_fever_in_the_past_3_days__c = 'Yes',
            First_Date_you_have_been_feeling_sick__c = Date.newInstance(2020, 3, 1) ,
            First_date_you_began_feeling_well__c= Date.newInstance(2020, 3, 10),
            Fever_Cough_or_Difficulty_breathing__c = 'Yes',
            Sick_since_Jan_1_2020__c = 'Yes',
            Opt_out_for_SMS_Updates__c = true,
            HasOptedOutOfEmail = false,
            Current_Step__c = 1,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con;
        

        Pre_Registration__c pr = new Pre_Registration__c();
        pr.Pass_Code_Health__c = 'heatlh123';
        pr.Pass_Code_Risk_Group__c = 'risk1234';
        pr.Contact__c = con.Id;
        insert pr;
    }

    public static void preparePortalUser() {
                // Record Type
                String citizenCovidId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
       
                // create Account 
                Account acc = [SELECT Id FROM Account WHERE Name='TestSite'];
      
                // create Contact;
                Contact portal_con = new Contact (
                    AccountId = acc.Id,
                    LastName = 'PortalUserTestDemo123',
                    MiddleName = 'DEF',
                    FirstName = 'DemoPortalUser',
                    Email = 'testportalTestUserv1@gmail.com',
                    Birthdate = Date.newInstance(1994, 5, 5),
                    Gender__c = 'Male',
                    MobilePhone = '(702)379-44151',
                    Phone = '(702)379-44151',
                    Street_Address1__c ='testStreet1',
                    Street_Address_2__c ='testStreet2',
                    City__c = 'TestCity',
                    State__c = 'OH',
                    ZIP__c = '12311',
                    Testing_Site__c = acc.Id,
                    RecordTypeId = citizenCovidId,
                    what_was_the_last_date_of_contact__c = Date.newInstance(2020, 2, 20),
                    Did_you_have_a_nasal_swab__c = 'Yes	',
                    What_was_your_test_result__c = 'Negative',
                    Approximately_what_date_were_you_tested__c = Date.newInstance(2020, 3, 3),
                    Had_contact_with_anyone_diagon__c = 'Yes',
                    Do_you_take_prescription_medications__c = '	Yes',
                    medication_for_fever_in_the_past_3_days__c = 'Yes',
                    First_Date_you_have_been_feeling_sick__c = Date.newInstance(2020, 3, 1) ,
                    First_date_you_began_feeling_well__c= Date.newInstance(2020, 3, 10),
                    Fever_Cough_or_Difficulty_breathing__c = 'Yes',
                    Sick_since_Jan_1_2020__c = 'Yes',
                    Opt_out_for_SMS_Updates__c = true,
                    HasOptedOutOfEmail = false,
                    Current_Step__c = 1,
                    Preferred_Date__c = Date.today(),
                    Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
                    Source__c = 'MMS',
                    Form_Submitted__c = false
                );
                insert portal_con;
          
              
                Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
                Profile prfile = [select Id,name from Profile where UserType IN : customerUserTypes  limit 1];        
                User newUser = new User(
                    profileId = prfile.id,
                    username = 'portalUser@test1203.com',
                    email = 'pb@f.com',
                    emailencodingkey = 'UTF-8',
                    localesidkey = 'en_US',
                    languagelocalekey = 'en_US',
                    timezonesidkey = 'America/Los_Angeles',
                    alias='nuser',
                    lastname='lastname',
                    //userRoleId = portalRole.Id,
                    contactId = portal_con.Id
                );
                insert newUser;      
    }


    @IsTest
    static void testAgeBasedGroupCatchBlock(){
        
        prepareData();
        preparePortalUser();
        //     1. createContact
        //     1.1. its Preregistration 

        // 2.  Create Contact 
        // 2.1 Convert it to Portal User - > RecordType -> "Covid Citizen User"
        
        // 3. Do a System.runas(Ref : 2)
        // 4. Call that first method with Ref :1
        
        Contact con = [SELECT Id FROM Contact WHERE LastName='DemoUser123Test' LIMIT 1];
        User portalUserId = [SELECT Id FROM User WHERE username='portalUser@test1203.com' LIMIT 1];
         
        Date dateNewObject = System.today();
        dateNewObject = dateNewObject.addYears(-40);

        Test.startTest();
        System.runAs(portalUserId){
            try {
                
                VTTS_PreRegInfoController.getAgeBasedGroup(dateNewObject, con.Id);
            }
            catch(Exception err) {
                System.assert(true,'error');
            }
        }
        Test.stopTest();
        
    }


    @IsTest
    static void testgetPreRegRecordCatchBlock(){
        
        prepareData();
        preparePortalUser();
        //     1. createContact
        //     1.1. its Preregistration 

        // 2.  Create Contact 
        // 2.1 Convert it to Portal User - > RecordType -> "Covid Citizen User"
        
        // 3. Do a System.runas(Ref : 2)
        // 4. Call that first method with Ref :1
        
        Contact con = [SELECT Id FROM Contact WHERE LastName='DemoUser123Test' LIMIT 1];
        User portalUserId = [SELECT Id FROM User WHERE username='portalUser@test1203.com' LIMIT 1];
        
        Test.startTest();
        System.runAs(portalUserId){
            try {
                
                VTTS_PreRegInfoController.getPreRegRecord(con.Id);
            }
            catch(Exception err) {
                System.assert(true,'error');
            }
        }
        Test.stopTest();
        
    }

}