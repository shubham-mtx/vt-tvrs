import {
    LightningElement,
    api,
    track
} from 'lwc';
import getSiteAccounts from '@salesforce/apex/DC_ScheduleAppointment.getSiteAccounts';
import getPreferredValues from '@salesforce/apex/OkpcContactInformationController.getPreferredValues';
import setPreferredValues from '@salesforce/apex/OkpcContactInformationController.setPreferredValues';
import getContactDetails from "@salesforce/apex/OkpcContactInformationController.getContactDetails";
import getAccountdetails from '@salesforce/apex/OKPC_LocationMapController.getAccountdetails';
import getAppointmentDetail from '@salesforce/apex/OKPC_IntakeRequestController.getAppointmentData';
import saveAppointmentDetail from '@salesforce/apex/OKPC_IntakeRequestController.saveAppointMentData';

import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent'
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';
export default class OkcpScheduleAppointment extends LightningElement {
    @track showSpinner;
    // map = new map();
    @api eventId;
    @api recordId;
    @api ifcovid = false;
    @api noConfirmationRequired = false;
    @track options = [];
    @track selectedOption;
    @track isAttributeRequired = false;
    @api fieldName;
    @track contactid;
    @api objectName;
    @api istesting;
    @track fieldLabelName;
    @track accountList = [];
    @track isSiteSelected = false;
    @track selectValue = null;
    @api appointmentId;
    @track selectSiteName;
    @track slotId;
    @track showSchedule = false;
    @track canSchedule = false;
    @track showMapModal = false;
    @track showCalendar = false;
    @track preferredDate = null;
    @track preferredTime = null;
    @track testingSite;
    @track showtestingSite = false;
    @track accountDetails = {};
    @track showLocationDetails = false;
    @track showSpinner = false;
    @track showSingleMap = false;
    @track mapMarkers = [];
    @track location = {};
    @track openModal = false;
    @track todayDate;
    @track preferredTimeMin = null;
    @track preferredTimeMax = null;
    @api showpickfromdashboard = false;
    @track symptomatic = '';
    @track showHelpText = false;
    @track isModalOpen = false;
    @track appData = {};
    map = new Map();
    selectedEventId = '';
    selectedSiteEventId = '';
    showPageHeader = true;
    @api dependentContactId = '';
    @api appoitmentId;
    get isScheduleAppointmentDisabled() {
        return !this.selectedEventId;
    }

    get selectedEventId() {
        this.selectedEventId = this.eventId;
    }

    get testingSiteId() {
        return this.noConfirmationRequired ? this.selectValue : this.recordId;
    }

    get siteEventId() {
        return this.eventId ? this.eventId : this.selectedSiteEventId ? this.selectedSiteEventId : '';
    }

    openModal() {
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
        setTimeout(() => {
            this.focusFirstEle();
        }, 200);
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
    }
    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        let valid = false;
        //     this.showSpinner = true;

        valid = [...this.template.querySelectorAll("lightning-radio-group")].reduce(
            (validSoFar, input) => {
                input.reportValidity();
                return validSoFar && input.checkValidity();
            },
            true
        );
        if (valid) {
            this.isModalOpen = false;

            this.showSpinner = true;
            this.appData.appointmentObj.symptomatic = this.symptomatic;
            saveAppointmentDetail({
                    JsonData: JSON.stringify(this.appData.appointmentObj),
                    appointMentId: this.appointmentId
                })
                .then(result => {
                   
                    this.appData = result;
                    this.showSpinner = false;


                })
                .catch(error => {
                    showAsyncErrorMessage(this,error);
                })
            this.showSpinner = false;
            this.showCalendar = true;


        }

    }
    connectedCallback() {
        this.ifcovid = true;
        this.todayDate = this.getTodayDate();
        this.retrieveTestingSites();
        this.getValuesPreferred();
        this.getAppointment();
        this.getContact();

        this.focusTitle();
    }

    getAppointment() {
        this.showSpinner = true;
        getAppointmentDetail({
                AppointmentId: this.appointmentId
            })
            .then(result => {
                this.appData = result;
                this.symptomatic = this.appData && this.appData.appointmentObj && this.appData.appointmentObj.symptomatic;
                if (this.symptomatic === 'Yes') {
                    this.showHelpText = true;
                } else {
                    this.showHelpText = false;
                }
            })
            .catch(error => {
                showAsyncErrorMessage(this,error);
            })
        this.showSpinner = false;

    }

    getContact() {
        this.showSpinner = true;
        getContactDetails()
            .then(result => {
              
                this.canSchedule = result.canSchedule;
                
                this.showSpinner = false;

            })
            .catch(error => {
                this.dispatchEvent(new ShowToastEvent({
                    message: (typeof error === 'string') ? error : error.body.message,
                    variant: 'error'
                }));
                this.showSpinner = false;
            })
    }
    getValuesPreferred() {
        this.showSpinner = true;
        getPreferredValues({
                appointmentId: this.appointmentId
            })
            .then(result => {
                this.preferredDate = result.Preferred_Date__c;

                if (result.Preferred_Time__c) {
                    this.preferredTime = this.msToTime(result.Preferred_Time__c);
                }
                if (!this.eventId && result.Event__c) {
                    this.selectedSiteEventId = result.Event__c;
                }
                if (result.Lab_Center__c) {
                    this.selectValue = result.Lab_Center__c;
                    this.getAccount(this.selectValue);
                    this.showtestingSite = true;
                    if (this.istesting) {
                        this.showtestingSite = false;
                    }
                    this.showLocationDetails = true;

                    if (this.showpickfromdashboard) {
                        this.showtestingSite = false;
                    } else {
                        this.showtestingSite = true;
                    }

                }
               
                this.showSpinner = false;

            })
            .catch(error => {
                this.showSpinner = false;
                showAsyncErrorMessage(this,error);
            })
    }

    @api
    setValues() {
        setPreferredValues({
                preferredDate: this.preferredDate,
                preferredTime: this.preferredTime,
                preferredSite: this.selectValue
            })
            .then(result => {
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Saved successfully',
                }));
                this.handleCloseModalonSave();
            })
            .catch(error => {
                showAsyncErrorMessage(this,error);

            })
    }
    handleInputChange(event) {
        if (event.target.name === 'preferredDate') {
            this.preferredDate = event.target.value;
        } else if (event.target.name === 'preferredTime') {
            this.preferredTime = event.target.value;
        }
    }

    retrieveTestingSites() {
        getSiteAccounts({})
            .then(data => {
                data.forEach((account) => {
                    var optionValue = new Object();
                    optionValue.label = account.Name;
                    optionValue.value = account.Id;
                    this.accountList.push(optionValue);
                    this.map.set(account.Id, account.Name);
                });
                this.options = this.accountList;
            })
            .catch(error => {
                showAsyncErrorMessage(this,error);
            });
    }

    selectionChangeHandler(event) {
        this.isSiteSelected = true;
        this.selectValue = event.target.value;
    }
    setId(event) {
        this.showSchedule = true;
        this.slotId = event.detail;
    }

    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    handleCloseModalonSave() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    justCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    showAppointments() {
        this.isModalOpen = true;
        setTimeout(() => {
            this.focusFirstEle();
        }, 200);

        

        this.focusTitle();
    }
    get Options() {
        return [{
                label: 'Yes',
                value: 'Yes'
            },
            {
                label: 'No',
                value: 'No'
            },
        ]
    }
    handleradiochange(event) {
        this.symptomatic = event.detail.value;
        this.appData.appointmentObj.symptomatic = event.detail.value;
        if (this.symptomatic === 'Yes') {
            this.showHelpText = true;
        } else {
            this.showHelpText = false;
        }
    }
    hideModal(event) {
        this.showCalendar = false;
        if (event.detail === 'save') {
            let tempObj = {};
            tempObj.name = "submit";
            const selectedEvent = new CustomEvent("submit", {
                detail: tempObj
            });
            this.dispatchEvent(selectedEvent);
        }
    }

    msToTime(s) {
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;

        return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs) + '.' + this.pad(ms, 3);
    }
    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }

    getAccount(Id) {
        this.showSpinner = true;
        getAccountdetails({
                accountId: Id
            })
            .then(result => {
                this.accountDetails = JSON.parse(JSON.stringify(result));
                this.location = result.location;
                if (this.accountDetails) {
                    this.showLocationDetails = true;
                    this.mapMarkers.length = 0;
                    this.mapMarkers.push(this.accountDetails);
                }
                if (result.businessStartTime) {
                    this.preferredTimeMin = this.msToTime(result.businessStartTime);
                } else {
                    this.preferredTimeMin = '07:00:00.000Z';
                }
                if (result.businessEndTime) {
                    this.preferredTimeMax = this.msToTime(result.businessEndTime);
                } else {
                    this.preferredTimeMax = '19:00:00.000Z';
                }
                this.showSpinner = false;
            })
            .catch(error => {
                this.showSpinner = false;
                showAsyncErrorMessage(this,error);
            })
    }

    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }

    handleEventSelected(event) {
        this.selectedEventId = event.detail;
    }

    handleKeyDown(event) {
        if (event.code === 'Escape') {
            this.isModalOpen = false;
        }
    }

    focusFirstEle() {
        const focusableElements = 'lightning-combobox, lightning-button-icon, button, input, lightning-helptext, lightning-input, lightning-button';
        const modal = this.template.querySelector('.slds-modal');
        if (modal) {
            const firstFocusableElement = modal.querySelectorAll(focusableElements)[0];
            const focusableContent = modal.querySelectorAll(focusableElements);
            const lastFocusableElement = focusableContent[focusableContent.length - 1];
            firstFocusableElement.focus();
            this.template.addEventListener('keydown', function (event) {
                let isTabPressed = event.key === 'Tab' || event.keyCode === 9;
                if (!isTabPressed) {
                    return;
                }
                if (event.shiftKey) {
                    if (this.activeElement === firstFocusableElement) {
                        lastFocusableElement.focus();
                        event.stopPropagation()
                        event.preventDefault();
                    }
                } else {
                    if (this.activeElement === lastFocusableElement) {
                        firstFocusableElement.focus();
                        event.preventDefault();
                        event.stopPropagation()
                    }
                }
            });
        }
    }

    focusTitle() {
        let focus = setInterval(() => {
            let cmp = this.template.querySelector('.pageTitle');
            if (cmp) {
                cmp.focus();
                setTimeout(() => {
                    this.showPageHeader = false;
                }, 500);
                clearInterval(focus);
            }
        }, 200);
    }
}