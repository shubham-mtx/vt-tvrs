import {
    LightningElement,
    track,
    wire
} from 'lwc';
import getDemographic from "@salesforce/apex/OkpcContactInformationController.getDemographic";
import setDemographic from "@salesforce/apex/OkpcContactInformationController.setDemographic";
import {
    getPicklistValues
} from 'lightning/uiObjectInfoApi';

import Gender_Field from '@salesforce/schema/Contact.Gender__c';
import Primary_Language_Field from '@salesforce/schema/Contact.Primary_Language__c';
import Race_Field from '@salesforce/schema/Contact.Race__c';
import Ethnicity_Field from '@salesforce/schema/Contact.Ethnicity__c';

export default class OkcpDemographics extends LightningElement {

    @track obj = {};
    @track showSpinner;
    currentstep = 5;
    @track isLanguageEqualsOther = false;

    connectedCallback() {
        this.showSpinner = true;
        getDemographic()
            .then(result => {
                this.obj.Gender = result.Gender__c;
                this.obj.Primary_Language = result.Primary_Language__c;
                this.obj.Race = result.Race__c;
                this.obj.Ethnicity = result.Ethnicity__c;
                this.showSpinner = false;
            })
            .catch(error => {
                console.log('error=>', JSON.stringify(error));
                this.showSpinner = false;
            })
    }
    get showIsLanguageEqualsOther() {
        return this.obj.Primary_Language === 'Other' ? true : false;
    }

    handleChange(event) {

        this.obj[event.target.name] = event.target.value;
        // if (event.target.name == 'Primary_Language' && event.target.value == 'Other') {
        //     console.log('PRIMARY LANGUAGE POST :' + event.target.value);
        //     isLanguageEqualsOther = true;
        // }

        if (event.target.checked) {
            this.obj[event.target.name] = 'True';
        }
    }

    handleLanguageInputChange(event) {

        console.log('User_Language :' + event.target.value);
        this.obj.User_Language = event.target.value;
    }

    navigateToBack() {
        const selectedEvent = new CustomEvent("backevent", {
            detail: null
        });
        this.dispatchEvent(selectedEvent);
    }

    sendEventToParent(obj) {
        const selectedEvent = new CustomEvent("save", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }

    saveData() {

        //Valdiations
        if (!this.obj.Gender || !this.obj.Primary_Language || !this.obj.Race || !this.obj.Ethnicity) {
            let tempObj = {};
            tempObj.message = "Please Fill all the required fields";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return;
        }

        this.showSpinner = true;
        setDemographic({
                data: this.obj,
                currentstep: this.currentstep
            })
            .then(result => {
                this.showSpinner = false;
                let tempObj = {};
                tempObj.message = "saved successfully";
                tempObj.status = "success";
                this.sendEventToParent(tempObj);
            })
            .catch(error => {
                console.log("error =>", JSON.stringify(error));
                this.showSpinner = false;
            })
    }

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: Gender_Field,
    })
    picklistOption1;

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: Primary_Language_Field,
    })
    picklistOption2;

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: Race_Field,
    })
    picklistOption3;

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: Ethnicity_Field,
    })
    picklistOption4;

}