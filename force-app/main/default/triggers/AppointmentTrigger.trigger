trigger AppointmentTrigger on Appointment__c (after insert, after update, after delete, after undelete, before insert, before update) {
    if(trigger.isAfter) {
        if(trigger.isInsert) {
            if(VT_TS_SkipTriggerExecution.executeAppointmentTrigger){
                AppointmentTriggerHandler.afterInsert(trigger.new);
            }
        }
        if(trigger.isUpdate) {
            if(VT_TS_SkipTriggerExecution.executeAppointmentTrigger){
                AppointmentTriggerHandler.afterUpdate(trigger.new,trigger.oldMap);
            }
        }
        if(trigger.isDelete) {
            if(VT_TS_SkipTriggerExecution.executeAppointmentTrigger){
                AppointmentTriggerHandler.afterDelete(trigger.old);
            }
        }
        if(trigger.isUndelete) {
            if(VT_TS_SkipTriggerExecution.executeAppointmentTrigger){
                AppointmentTriggerHandler.afterUndelete(trigger.new);
            }
        }
    }
    if(trigger.isBefore){
        if(trigger.isInsert){
            if(VT_TS_SkipTriggerExecution.executeAppointmentTrigger){
                AppointmentTriggerHandler.beforeInsert(trigger.new);
            }
        }
        if(trigger.isUpdate){
            if(VT_TS_SkipTriggerExecution.executeAppointmentTrigger){
                AppointmentTriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);
            }
        }
    }
}