import { LightningElement, api } from 'lwc';

export default class OkcpDependentCard extends LightningElement {
    @api patient;

    get cardStyle() {
        return 'slds-box slds-m-top_small ' + (this.patient.isCommunityUser ? 'community-user' : '');
    }

    handleClick() {
        this.dispatchEvent(new CustomEvent('updatedetails', {
            bubbles: true, composed: true, detail: {
                patientId: this.patient.id,
                isDependentContact: this.patient.isCommunityUser
            }
        }));
        // add event handler on the topmost component
    }

    get ariaLabelPreRegister() {
        return  'Pre Register for '+this.patient.name;
    }

    get ariaLabelUpdateDetails() {
        return  'Update Details for '+this.patient.name;
    }
}