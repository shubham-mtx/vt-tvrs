/**
 * @description       : 
 * @author            : Vamsi Mudaliar
 * @group             : 
 * @last modified on  : 01-20-2022
 * @last modified by  : Mohit Karani
**/
public without sharing class OKCP_ResultCertificateController {
    public String body{get;set;}
    public String testing_result{get;set;}
    public String preferredLanguage{get;set;}
    public Boolean allowdFooter{get;set;}
    public String testingId;
    public string debugText {get;set;}
    public String langTestResult {get;set;}


     
     public void setTestingId(String input){
        testingId = input;
         try{
             if(String.isNotBlank(testingId)) {
                 setTestingData(testingId);
             }
         } catch(Exception e) {  }
    }

    public String getTestingId(){
        return testingId;
    }
    
    public OKCP_ResultCertificateController() {
        testing_result = 'Not Tested';
        if(testingId == null ) {
            String restestingId = ApexPages.CurrentPage().getparameters().get('id');
            try{
                if(String.isNotBlank(restestingId)){      
                    setTestingData(restestingId);
                }
            } catch(Exception e) {  }
        }
        Apexpages.currentPage().getHeaders().put('content-disposition', 'attachment; filename="Testing Results.pdf"');
    }

    private void setTestingData(String testingRecordId) {
        allowdFooter = false;
        Id contactRecordId;
        Id appointmentRecordId;
        String language;
        testing_result = 'Not Tested';
        body = '';
        if(String.isNotBlank(testingRecordId)){      
            // adding Language Custom Field (Cross Object Formula Type ) to Appointment as Per - S-23923
            List<Antibody_Testing__c> testings = [
                SELECT Id, Appointment__c,Results__c,Appointment__r.Patient__c, Appointment__r.contact_language__c
                FROM Antibody_Testing__c
                WHERE Id = :testingRecordId WITH SECURITY_ENFORCED];
            
            if (!testings.isEmpty()) {
                testing_result = testings[0].Results__c;
                appointmentRecordId = testings[0].Appointment__c;
                contactRecordId = testings[0].Appointment__r.Patient__c;
                language = testings[0].Appointment__r.contact_language__c;
            }

            preferredLanguage = language;
            langTestResult = language+' - '+testing_result;

            Set<String> englishConsiderableLanuages = new Set<String>{'Amharic','Bengali','Bosnian','Creole','Dari','Dinka','Hindi','Japanese','Lao','Kinyarwanda','Maay Maay','Portuguese','Romanian','Russian','Tagalog','Taishanese','Thai','Tibetan','Turkish'};
            Set<String> chineseConsiderableLanguages = new Set<String>{'Mandarin','Cantonese'};

            if(englishConsiderableLanuages.contains(language)) {
                language = 'English';
            } else if(chineseConsiderableLanguages.contains(language)) {
                language = 'Chinese';                   
            }

            //Step 1: Just for Unicode
            if( ( String.isNotBlank(language) && 
                    language != 'Other' &&
                    language != 'Lingala' && 
                    language != 'English' && 
                    language != 'ASL') && 
                    ( testing_result == 'Positive' || testing_result == 'Negative' ) ){

                Result_Body__mdt bodymtd = [SELECT Id,Body__c FROM Result_Body__mdt WHERE Type__c =:testing_result AND Language__c =: language WITH SECURITY_ENFORCED LIMIT 1];
                body = '<div class="unicodeLang">'+bodymtd.Body__c+'</div>';

                allowdFooter = true;
            }

            //Step 2: For English
            Result_Body__mdt bodymtdForAll = [SELECT Id,Body__c FROM Result_Body__mdt WHERE Type__c =:testing_result AND Language__c = 'English' WITH SECURITY_ENFORCED LIMIT 1];
            if( String.isNotBlank(body) ){
                body = body+'<br/><br/><br/><br/>';
                body += '<div style="page-break-after:always;"/>';
            }
            body += bodymtdForAll.Body__c;


            List<Result_Body_Merge_Field__mdt> fieldMetadata = [SELECT Id,Field__c,Object__c,Type__c FROM Result_Body_Merge_Field__mdt WITH SECURITY_ENFORCED];               
            
            sObject contactRecord = new Contact();                
            sObject appointmentRecord = new Appointment__c();                
            sObject testingRecord  = new Antibody_Testing__c();
            
            if (contactRecordId != null) {
                String contactFields = 'Id';
                for(Result_Body_Merge_Field__mdt fieldmtd : fieldMetadata) {
                    if(fieldmtd.Object__c == 'Contact'){
                        contactFields +=','+fieldmtd.Field__c;
                    }
                }
                try{
                    contactRecord = Database.query('SELECT '+contactFields+' FROM Contact WHERE ID= :contactRecordId WITH SECURITY_ENFORCED');
                }
                catch(System.QueryException qe) {
                    throw new VT_Exception('YOU DO NOT HAVE SUFFICIENT PERMISSIONS TO ACCESS THESE FIELDS' + qe.getMessage());
                }
            }
            
            if (appointmentRecordId != null) {
                String appointmentFields = 'Id';
                for(Result_Body_Merge_Field__mdt fieldmtd: fieldMetadata) {
                    if(fieldmtd.Object__c == 'Appointment__c'){
                        appointmentFields +=','+fieldmtd.Field__c;
                    }
                }
               
                try{
                    appointmentRecord = Database.query('SELECT '+appointmentFields+' FROM Appointment__c WHERE ID= :appointmentRecordId WITH SECURITY_ENFORCED');
                }
                catch(System.QueryException qe) {
                    throw new VT_Exception('YOU DO NOT HAVE SUFFICIENT PERMISSIONS TO ACCESS THESE FIELDS' + qe.getMessage());
                }
            }
            
            if (testingRecordId != null) {
                String testingFields = 'Id';
                for(Result_Body_Merge_Field__mdt fieldmtd: fieldMetadata) {
                    if(fieldmtd.Object__c == 'Antibody_Testing__c'){
                        testingFields +=','+fieldmtd.Field__c;
                    }
                }
                debugText  = testingRecordId+' '+testingId;
                
                try{
                    testingRecord = Database.query('SELECT '+testingFields+' FROM Antibody_Testing__c WHERE ID= :testingRecordId WITH SECURITY_ENFORCED');
                }
                catch(System.QueryException qe) {
                    throw new VT_Exception('YOU DO NOT HAVE SUFFICIENT PERMISSIONS TO ACCESS THESE FIELDS' + qe.getMessage());
                }
            }
            
            //Merge Fields
            for(Result_Body_Merge_Field__mdt fieldmtd : fieldMetadata) {
                String value = '';
                if(fieldmtd.Object__c == 'Contact'){
                    //value = getFieldValue(fieldmtd.Field__c, contactRecord);
                    value = getFieldValue_New(appointmentRecord,fieldmtd.Field__c);
                    if(fieldmtd.Type__c == 'Date' && value != null) {
                        date myDate = date.valueof(value);
                        value = myDate.format();
                    }
                    body = body.replace('{!Contact.'+fieldmtd.Field__c+'}', value!= null ? value : ' ');
                }
                else if(fieldmtd.Object__c == 'Appointment__c'){
                    //value = getFieldValue(fieldmtd.Field__c, appointmentRecord);
                    value = getFieldValue_New(appointmentRecord,fieldmtd.Field__c);
                    if(fieldmtd.Type__c == 'Date' && value != null) {
                        date myDate = date.valueof(value);
                        value = myDate.format();
                    }
                    else if(fieldmtd.Type__c=='DateTime' && value!=null) { // added this check to Add AM or PM as per S-23625 
                        String tmpVal = value;
                        String currDate = value.split(' ')[0];
                        String []dateParts = currDate.split('-'); // yyyy-mm-dd -> mm-dd-yyyy 
                        
                        tmpVal = dateParts[1]+'-'+dateParts[2]+'-'+dateParts[0]+' ';

                        String currTime = value.split(' ')[1];
                        String []timeParts = currTime.split(':');
                        
                        tmpVal+=(timeParts[0]+':'+timeParts[1]+' '); // hh:mm

                        if(timeParts!=null && currTime!=null) {
                            String amOrPm = (Integer.valueOf(timeParts[0])<12?'AM':'PM');
                            value=tmpVal+amOrPm;
                        }
                    }
                    body = body.replace('{!Appointment__c.'+fieldmtd.Field__c+'}', value!= null ? value : ' ');

                }
                else {
                    //value = getFieldValue(fieldmtd.Field__c, testingRecord);               
                    value = getFieldValue_New(testingRecord,fieldmtd.Field__c);               
                    if(fieldmtd.Type__c == 'Date' && value != null) {
                        date myDate = date.valueof(value);
                        value = myDate.format();
                    }
                    body = body.replace('{!Antibody_Testing__c.'+fieldmtd.Field__c+'}', (value != null)?value:'');
                }                    
            }
        }
    }

    /*
        if fieldName is Event__r.Location__r.Name 
        this function recursively checks for the object and returns the field value
    */
    private static String getFieldValue_New(SObject record,String fieldName) {

        if(record==null){ 
            return null;
        }
        if(!fieldName.contains('.')) {
            return String.valueOf(record.get(fieldName));
        } 
        return getFieldValue_New(record.getSobject(fieldName.substringBefore('.')),fieldName.substringAfter('.')); // recursively calling for other objects
    }

}