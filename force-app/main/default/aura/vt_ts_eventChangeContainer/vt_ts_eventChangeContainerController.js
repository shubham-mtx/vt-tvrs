({
    recordSaveHandler : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire();
    },

    handlePermissionError : function (event) {
        $A.get("e.force:closeQuickAction").fire();
    }
})