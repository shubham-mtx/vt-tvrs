import {
    api,
    LightningElement,
    track,
    wire
} from 'lwc';
import momentPath from "@salesforce/resourceUrl/momentJs";
import {
    loadScript
} from "lightning/platformResourceLoader";
import {
    getPicklistValues,
    getObjectInfo
} from 'lightning/uiObjectInfoApi';
import COUNTY_FIELD from '@salesforce/schema/Account.Vermont_County__c';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import fetchPrivateAccessValues from "@salesforce/apex/OKPC_LocationMapController.fetchPrivateAccess";
import fetchPicklist from "@salesforce/apex/OKPCDashboardController.fetchPicklist";
import {
    loadStyle
} from 'lightning/platformResourceLoader';
import vtComboCSS from '@salesforce/resourceUrl/VT_Combo_CSS';

import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';
export default class Vt_event_scheduler_filters extends LightningElement {
    @api isdefaultload;
    @api hidefilters;
    @api showPrivateAccessFilter;
    @api eventProcess;
    @api privateAccessId;
    @api vaccinationPrivateAccess;
    @api vaccinationType;
    @api isSecondDose;
    @api secondDoseData;
    @api locationTitle;
    @api isFromApptScheduler;
    @api isFirstAppointment;
    @api contactId;
    @track searchObj = {
        selCounty: '',
        selDate: '',
        selSpoBy: '',
        selTimePref: '',
        eventType: '',
        privateAccess: '',
        selectedValccineClass: ''
    };

    @track availableCounties = [];
    @track availableEventType = [];
    @track availablePrivateAccess = [];
    @track availableDateRanges = [];
    @track availableSpoBy = [];
    @track availableTimePref = [];
    @track isActive = false;
    @track activePrivateAccess = false;
    @track vaccineClassDisabled = true;
    @track activeDateTimePrefrence = true;
    @track activeEventType = true;
    @track isError = false;
    @track errorMsg = 'Please check all fields before clicking on search.';
    @track showSpinner = true;
    @track isShowCustomDateRange = false;
    activeTimePrefrence = true;
    availableEventProcess;
    sDateDisabled;
    endDateMin;
    isCitizenPortal;
    resetClicked;
    @track vaccineClassList = []
    @track vaccineClassListToDisplay = [];
    @track availablePrivateAccessToDisplay = [];
    @wire(getObjectInfo, {
        objectApiName: ACCOUNT_OBJECT
    })
    accountMetadata;

    @wire(getPicklistValues, {
        recordTypeId: '$accountMetadata.data.defaultRecordTypeId',
        fieldApiName: COUNTY_FIELD
    })
    wiredPickListValue({
        data,
        error
    }) {
        if (data) {

            //Filter 1
            this.availableCounties = [];
            if (this.showPrivateAccessFilter) {
                this.showSpinner = true;
                var allCounties = {
                    label: 'All',
                    value: 'All'
                };
                this.availableCounties.push(allCounties);
            }
            data.values.forEach(element => {
                var county = {
                    label: element.label,
                    value: element.value
                };
                this.availableCounties.push(county);
            });

            this.error = undefined;
            loadScript(this, momentPath).then(() => {
                this.loadFilters();
            });

            setTimeout(() => {
                this.focusFirstEle();
            }, 200);
        }
        if (error) {
            showAsyncErrorMessage(this, error);
            this.availableCounties = undefined;
        }
    }

    get randomName() {
        return this.makeid(10);
    }

    get activeVaccineClass() {
        return this.vaccineClassListToDisplay && this.vaccineClassListToDisplay.length > 0 && ((!this.isCitizenPortal && this.eventProcess !== 'Testing Site') || (this.isCitizenPortal && this.eventProcess && this.eventProcess.includes('Vaccination')));
    }
    makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    connectedCallback() {
        if (location.href.includes('events/s')) {
            this.isCitizenPortal = true;
        }
        loadStyle(this, vtComboCSS);
        // this.showPrivateAccessFilter = (!this.showPrivateAccessFilter && this.eventProcess == 'Vaccination') ? true : this.showPrivateAccessFilter;
        this.showPrivateAccessFilter = (this.isCitizenPortal) ? false : this.showPrivateAccessFilter;
        this.showSpinner = false;
    }

    async loadFilters() {
        //Filter 2
        this.showSpinner = true;
        this.availableDateRanges = [{
                label: 'Today',
                value: this.formatDate(moment()) + '**' + this.formatDate(moment())
            },
            {
                label: 'Tomorrow',
                value: this.formatDate(moment().add(1, 'd')) + '**' + this.formatDate(moment().add(1, 'd'))
            },
            {
                label: 'Next 3 Days',
                value: this.formatDate(moment()) + '**' + this.formatDate(moment().add(3, 'd'))
            },
            {
                label: 'Next 7 Days',
                value: this.formatDate(moment()) + '**' + this.formatDate(moment().add(7, 'd'))
            }

        ];
        this.availableDateRanges.push({
            label: 'Next 4 Weeks',
            value: this.formatDate(moment()) + '**' + this.formatDate(moment().add(28, 'd'))
        });
        this.availableDateRanges.push({
            label: '5-8 Weeks',
            value: this.formatDate(moment().add(29, 'd')) + '**' + this.formatDate(moment().add(56, 'd'))
        });
        this.availableDateRanges.push({
            label: '9-12 Weeks',
            value: this.formatDate(moment().add(57, 'd')) + '**' + this.formatDate(moment().add(84, 'd'))
        });

        if (this.showPrivateAccessFilter && !this.isCitizenPortal) {
            this.availableDateRanges.push({
                label: 'Custom',
                value: 'Custom'
            });
        }

        // Remove all option for Citizen Portal
        if (this.isCitizenPortal) {
            let tempCounties = this.availableCounties && this.availableCounties.length > 0 && JSON.parse(JSON.stringify(this.availableCounties));
            let updatedCounties = [];
            tempCounties.forEach(element => {
                if (element.label !== 'All') {
                    updatedCounties.push(element);
                }
            });

            this.availableCounties = updatedCounties;
        }

        this.searchObj.selDate = this.availableDateRanges[4].value;

        //Filter 3
        this.availableSpoBy = [{
            label: 'State-Sponsored',
            value: 'State-Sponsored'
        }];
        this.searchObj.selSpoBy = this.availableSpoBy[0].value;

        //If event process is coming then use event process to identify
        if (this.locationTitle) {
            if (this.locationTitle.includes('Vaccination')) {
                this.availableSpoBy.push({
                    label: 'Other-Vaccination',
                    value: 'Other-Vaccination'
                });
            } else {
                this.availableSpoBy.push({
                    label: 'Other-Testing Site',
                    value: 'Other-Testing Site'
                });
            }
        } else if (this.locationTitle === undefined && this.eventProcess === undefined) {
            this.availableSpoBy.push({
                label: 'Other-Testing Site',
                value: 'Other-Testing Site'
            });
            this.availableSpoBy.push({
                label: 'Other-Vaccination',
                value: 'Other-Vaccination'
            });
        }

        //Filter 4
        this.availableTimePref = [{
                label: 'Anytime',
                value: '00:00:00' + '**' + '23:59:00'
            },
            {
                label: 'Morning',
                value: '00:00:00' + '**' + '11:59:00'
            },
            {
                label: 'Afternoon',
                value: '12:00:00' + '**' + '16:59:00'
            },
            {
                label: 'Evening',
                value: '17:00:00' + '**' + '23:59:00'
            }
        ];

        //Filter 5
        this.availableEventType = [{
                label: 'Public',
                value: 'Public'
            },
            {
                label: 'Private Access',
                value: 'Private Access'
            }
        ];
        this.searchObj.eventType = this.availableEventType[0].value;


        //Event Process Filters
        this.availableEventProcess = [
            // {label: 'All', value: 'All'},
            {
                label: 'Testing Site',
                value: 'Testing Site'
            }
        ];

        //Default
        this.searchObj.selTimePref = this.availableTimePref[0].value;

        //If its not testing site then it should have Vaccination options
        if (this.eventProcess === undefined || this.eventProcess !== 'Testing Site') {
            await fetchPicklist({
                objectName: 'Private_Access__c',
                fieldName: 'Vaccine_Type__c'
            }).then(result => {
                if (result && result.length > 0) {
                    result.forEach(element => {
                        this.availableEventProcess.push({
                            label: element.label,
                            value: element.value
                        });
                    });
                    if (this.eventProcess === 'Vaccination') {
                        this.eventProcess = 'Vaccination-1';
                    }
                    if (this.vaccinationType) {
                        this.eventProcess = this.vaccinationType;
                    }

                    //Filter 6
                    if (this.vaccinationType) {
                        this.getPrivateAccessValues(this.eventProcess, true);
                    } else {
                        this.getPrivateAccessValues(this.eventProcess, false);
                    }


                    if (this.eventProcess) {
                        if (this.eventProcess !== 'Testing Site') {
                            this.activePrivateAccess = true;
                            this.vaccineClassDisabled = false;
                            this.activeEventProcess = true;
                            this.searchObj.eventType = this.availableEventType[1].value;
                            this.searchObj.eventProcess = this.availableEventProcess[1].value;
                            this.searchObj.privateAccess = 'All';
                        } else {
                            this.searchObj.eventProcess = this.eventProcess;
                        }
                    }
                    if (this.vaccinationType) {
                        this.searchObj.eventProcess = this.vaccinationType;
                        this.activeEventProcess = false;
                        this.activeEventType = false;
                    }

                    //If it have the Testing Private Group or Vaccination Private Group then default events to Private
                    if (this.privateAccessId || this.vaccinationPrivateAccess) {
                        this.searchObj.eventType = this.availableEventType[1].value;
                        this.searchObj.privateAccess = 'All';
                    }
                    // this.isSecondDose   // replaced condition due to booster dose
                    if (this.vaccinationType && this.vaccinationType !== 'Vaccination-1' && !this.isFirstAppointment) { // S-14229 Second Dose  
                        this.searchObj.selDate = 'Custom';
                        this.isShowCustomDateRange = true;
                        this.activeDateTimePrefrence = false;
                        if (this.secondDoseData && this.secondDoseData.Appointment_Complete_Date__c) {

                            this.searchObj.sDate = moment(this.secondDoseData.Appointment_Complete_Date__c).add(this.secondDoseData.Min__c, 'd').format('YYYY-MM-DD');
                            this.searchObj.eDate = moment(this.secondDoseData.Appointment_Complete_Date__c).add(this.secondDoseData.Max__c, 'd').format('YYYY-MM-DD');

                            // w.r.t Issue-77011 
                            let momentInstance = moment();
                            let maxEndDate = moment().add(30, 'd');

                            if (momentInstance.isAfter(this.searchObj.sDate)) {
                                this.searchObj.sDate = momentInstance.format('YYYY-MM-DD');
                            }

                            if (maxEndDate.isAfter(this.searchObj.eDate)) {
                                this.searchObj.eDate = maxEndDate.format('YYYY-MM-DD');
                            }

                        }

                       this.endDateMin = this.searchObj.eDate;
                        this.sDateDisabled = true;
                    }

                    this.isActive = true;
                    this.isFromApptScheduler && this.updateDateFields();
                }
            }).catch(error => {
                showAsyncErrorMessage(this, error);
            }).finally(() => {
                this.showSpinner = false;
            })
        } else if (this.isFromApptScheduler) {

            this.updateDateFields();
        } else {
            this.showSpinner = false;;
        }
    }
    updateDateFields() {
        this.isShowCustomDateRange = false;
        this.activeDateTimePrefrence = true;
        this.sDateDisabled = false;
        this.searchObj.selDate = this.availableDateRanges[4].value;
        this.showSpinner = false;;
    }
    formatDate(dt) {
        return dt.format('MM/DD/YYYY');
    }

    handleSearchChange(event) {
        this.resetClicked = false;
        event.target.disabled = 'disabled';
        if (this.isValid()) {
            this.isError = false;
            let eventProcessForSearch;
            let vaccinationType;

            if (this.searchObj.eventProcess === 'Vaccination 1' || this.searchObj.eventProcess === 'Vaccination 2') {
                eventProcessForSearch = 'Vaccination';
            } else if (this.searchObj.eventProcess === 'Testing Site') {
                eventProcessForSearch = 'Testing Site';
            }

            if (this.searchObj.eventProcess !== 'Testing Site') {
                vaccinationType = this.searchObj.eventProcess;
            } else {
                vaccinationType = '';
            }
            if (this.searchObj.selectedValccineClass !== 'All' && this.searchObj.selectedValccineClass) {
                this.vaccineClassList = [];
                this.vaccineClassList.push(this.searchObj.selectedValccineClass);
            }
            else if(this.searchObj.selectedValccineClass === 'All'){
                this.vaccineClassListToDisplay.forEach(vaccineClass =>{
                    this.vaccineClassList.push(vaccineClass.label);
                })
            }
            let sObj = {
                county: this.searchObj.selCounty === 'All' ? '' : this.searchObj.selCounty,
                sDate: this.searchObj.selDate !== 'Custom' ? this.searchObj.selDate.split('**')[0] : this.formatDate(moment(this.searchObj.sDate)),
                eDate: this.searchObj.selDate !== 'Custom' ? this.searchObj.selDate.split('**')[1] : this.formatDate(moment(this.searchObj.eDate)),
                sTime: this.searchObj.selTimePref.split('**')[0],
                eTime: this.searchObj.selTimePref.split('**')[1],
                sponBy: this.searchObj.selSpoBy,
                eventType: this.searchObj.eventType,
                // privateAccess:(this.isCitizenPortal) ? null : this.searchObj.privateAccess,
                eventProcess: eventProcessForSearch,
                vaccineType: this.searchObj.eventType !== 'All' ? vaccinationType : '',
                vaccineClassList: this.vaccineClassList,
                privateAccessIdList: (this.searchObj.privateAccessIdList && this.searchObj.privateAccessIdList.length > 0) ? this.searchObj.privateAccessIdList : []
            };
           
            this.dispatchEvent(new CustomEvent("search", {
                detail: sObj
            }));
        } else {
            event.target.disabled = false;
            this.isError = true;
        }
    }

    isValid() {
        let isAllValid = [...this.template.querySelectorAll('lightning-combobox, lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        if (!isAllValid) {
            let forFocus = setInterval(() => {
                let input = this.template.querySelector('lightning-combobox');
                if (input) {
                    let end = (input.value) ? input.value.length : 0;
                    input.selectionStart = 0;
                    input.selectionEnd = end;
                    input.focus();
                    clearInterval(forFocus);
                }
            }, 100);
            return false;
        }

        return isAllValid;
    }

    handleChange(event) {
        this.resetClicked = false;
        this.template.querySelector('lightning-button').disabled = false;
        if (event.target.name === 'eventType') {

            if (event.target.value === 'Private Access') {
                // this.showSpinner = true;
                  this.activePrivateAccess = true;
                this.vaccineClassDisabled = false;
                this.searchObj.eventProcess = undefined; //this.availableEventProcess[0].value;
                this.searchObj.privateAccess = (this.availablePrivateAccessToDisplay && this.availablePrivateAccessToDisplay.length > 0) ? this.availablePrivateAccessToDisplay[0].value : null;
                this.activeEventProcess = true;
              
            } else {
                this.searchObj.selectedValccineClass = '';
                this.activeEventProcess = false;
                this.searchObj.privateAccess = '';
                this.searchObj.eventProcess = undefined;
                this.activePrivateAccess = false;
                this.vaccineClassDisabled = true;

                if (this.eventProcess) {
                    this.searchObj.eventProcess = this.eventProcess;
                }
               

            }

        } else if (event.target.name === 'selSpoBy') {
            
            if (event.target.value.includes('Other')) {
                this.searchObj.selDate = '';
                this.searchObj.selTimePref = '';
                this.searchObj.eventType = 'Public';
                this.searchObj.privateAccess = '';
                this.searchObj.selectedValccineClass = '';
                this.searchObj.eventType = '';
                this.searchObj.eventProcess = undefined; //'All';
                this.activeDateTimePrefrence = false;
                this.activeTimePrefrence = false;
                this.activeEventType = false;
                this.activePrivateAccess = false;
                this.vaccineClassDisabled = true;
                this.activeEventProcess = false;
            } else {
                this.searchObj.eventType = this.availableEventType[0].value;
                this.searchObj.selTimePref = this.availableTimePref[0].value;
                this.searchObj.selDate = this.availableDateRanges[4].value;
                this.activeTimePrefrence = true;
                this.activeDateTimePrefrence = true;
                this.activeEventType = true;

                this.loadFilters();
            }
        } else if (event.target.name === 'selDate') {
            this.isShowCustomDateRange = (event.target.value === 'Custom');

            if (!this.isShowCustomDateRange) {
                this.searchObj.sDate = undefined;
                this.searchObj.eDate = undefined;
            }
        }

        if (event.target.name === 'eventProcess') {
            this.vaccineClassDisabled = (event.target.value === 'Testing Site');
            this.getPrivateAccessValues(event.target.value, true);
        }

        if (event.target.dataset.name === 'selCounty') {
            this.searchObj[event.target.dataset.name] = event.target.value;
        } else if (event.target.name === 'selectedValccineClass') {
            this.searchObj[event.target.name] = event.target.value;

            let containsAll = false;
            if (event.target.value !== 'All') {
                  this.availablePrivateAccessToDisplay = this.availablePrivateAccess.filter(privateAccess => privateAccess.vaccineClass === event.target.value);
                this.searchObj.privateAccessIdList = [];
                this.availablePrivateAccessToDisplay.forEach(element => {
                    containsAll = (element.label === 'All') ? true : containsAll;
                    this.searchObj.privateAccessIdList.push(element.value);
                });
                this.vaccineClassList = [event.target.value];

                !containsAll && !this.isCitizenPortal && this.availablePrivateAccessToDisplay.unshift({
                    label: 'All',
                    value: 'All'
                })
            } else {
                let tempValues = [];

                this.vaccineClassListToDisplay.forEach(element => {
                    tempValues.push(element.label);
                });
                this.vaccineClassList = tempValues;
                this.availablePrivateAccessToDisplay = this.availablePrivateAccess;
                this.searchObj.privateAccessIdList = [];
                this.availablePrivateAccessToDisplay.forEach(element => {
                    containsAll = (element.label == 'All') ? true : containsAll;

                    this.searchObj.privateAccessIdList.push(element.value);

                });
                // let allExists = this.availablePrivateAccessToDisplay.find((pa) => pa.label === 'All');
                // if(!allExists) {
                !containsAll && !this.isCitizenPortal && this.availablePrivateAccessToDisplay.unshift({
                    label: 'All',
                    value: 'All'
                })
                // }
            }
            let privateAccessField = this.template.querySelector('lightning-combobox[data-field="privateAccess"]');
            if (privateAccessField) {
                privateAccessField.value = 'All';
                this.searchObj.privateAccess = 'All';
            }

        } else if (event.target.name === 'privateAccess') {
            this.searchObj.privateAccessIdList = [];
            if (event.target.value !== 'All') {
                this.searchObj.privateAccessIdList.push(event.target.value);
            } else {
                this.availablePrivateAccessToDisplay.forEach(element => {
                    this.searchObj.privateAccessIdList.push(element.value);

                });
            }
        } else {
            this.searchObj[event.target.name] = event.target.value;
        }
        this.dispatchEvent(new CustomEvent("reset"));
    }

    getPrivateAccessValues(eProcess, isDefault) {
        this.availablePrivateAccess = [];
        // this.vaccineClassListToDisplay = [];
        this.showSpinner = true;
        let dataObj = {
            eProcess: eProcess,
            vaccineBrand: ((this.isSecondDose || eProcess !== 'Vaccination-1') && this.secondDoseData) ? this.secondDoseData.Vaccine_Class_Name__c : '',
            vaccinationPrivateAccess: this.vaccinationPrivateAccess,
            isSecondDose: this.isSecondDose,
            contactId: (this.secondDoseData) ? this.secondDoseData.contactId : (this.contactId) ? this.contactId : null,
            isFirstAppointment: (this.isFirstAppointment) ? true : false
        };
        fetchPrivateAccessValues({
                response: JSON.stringify(dataObj)
            })
            .then(result => {
                let containsAll = false;
                let tempPac = [];

                let containsAllValue = (this.vaccineClassListToDisplay && this.vaccineClassListToDisplay.length > 0 && this.vaccineClassListToDisplay[0].value === 'All');
                if (!containsAllValue && (!this.secondDoseData || (!this.secondDoseData.contactId && this.contactId))){
                    this.vaccineClassListToDisplay.push({
                        label: 'All',
                        value: 'All'
                    });
                }
                let index = 0;
                let temp = 0;
                let tempVaccineClasslist = [];
                this.vaccineClassListToDisplay && this.vaccineClassListToDisplay.length > 0 && this.vaccineClassListToDisplay.forEach(vaccineClass => {
                    tempVaccineClasslist.push(vaccineClass.label);
                })
                result.forEach(ele => {
                    temp = (this.secondDoseData && ele.vaccineClass === this.secondDoseData.currentAppointmentVaccineClass) ? index : temp;
                    tempPac.push(ele);
                    index++;
                    if (!this.vaccineClassList.includes(ele.vaccineClass) && !tempVaccineClasslist.includes(ele.vaccineClass) && ele.vaccineClass) {
                        this.vaccineClassList.push(ele.vaccineClass);
                        this.vaccineClassListToDisplay.push({
                            label: ele.vaccineClass,
                            value: ele.vaccineClass
                        })
                    }
                });
                this.vaccineClassListToDisplay.sort((a, b) => (a.label > b.label) ? 1 : ((b.label > a.label) ? -1 : 0))

                this.availablePrivateAccess = tempPac;

                if (isDefault) {
                    // this.searchObj.selectedValccineClass = (!this.resetClicked && this.availablePrivateAccess[temp] && temp > 0) ? this.availablePrivateAccess[temp].vaccineClass : null;

                    this.searchObj.vaccineClass =  (!this.isCitizenPortal && (!this.secondDoseData || !this.secondDoseData.contactId)) ? 'All' : this.secondDoseData.currentAppointmentVaccineClass; //  this.searchObj.vaccineClass
                    this.searchObj.selectedValccineClass = (this.vaccineClassDisabled) ? null : (!this.isCitizenPortal && (!this.secondDoseData || (this.secondDoseData && !this.secondDoseData.contactId))) ? 'All' : this.searchObj.vaccineClass
                    this.searchObj.privateAccess = (!this.isCitizenPortal) ? 'All' : null;
                    this.availablePrivateAccessToDisplay = (!this.searchObj.selectedValccineClass || this.searchObj.selectedValccineClass === 'All') ? this.availablePrivateAccess : this.availablePrivateAccess.filter(privateAccess => privateAccess.vaccineClass === this.searchObj.selectedValccineClass);
                    this.searchObj.privateAccessIdList = [];
                    this.availablePrivateAccessToDisplay.forEach(element => {
                        containsAll = (element.label === 'All') ? true : containsAll;
                        this.searchObj.privateAccessIdList.push(element.value);

                    });
                }!containsAll && this.availablePrivateAccessToDisplay.unshift({
                    label: 'All',
                    value: 'All'
                })

                let vaccineClassField = this.template.querySelector('lightning-combobox[data-field="selectedValccineClass"]');
                let privateAccessField = this.template.querySelector('lightning-combobox[data-field="privateAccess"]');
                
                let vaccineClassPresentInList = false;
                this.vaccineClassListToDisplay && this.vaccineClassListToDisplay.length > 0 && this.vaccineClassListToDisplay.forEach(vaccineClass =>{
                    vaccineClassPresentInList = (vaccineClass.label ===  this.searchObj.selectedValccineClass) ? true : vaccineClassPresentInList
                });
                this.searchObj.selectedValccineClass = (vaccineClassPresentInList) ? this.searchObj.selectedValccineClass : null;
                if (vaccineClassField) {
                    vaccineClassField.value =  this.searchObj.selectedValccineClass ;
                }
                if (privateAccessField) {
                    privateAccessField.value = (!this.searchObj.privateAccess) ? 'All' : this.searchObj.privateAccess;
                }

     
                if(!this.isCitizenPortal && (!this.secondDoseData || (this.secondDoseData && !this.secondDoseData.contactId))) {
                    let finalVaccineClass = [];
                    let finalVaccineClassToDisplay = [{label: 'All', value: 'All'}];
                    this.availablePrivateAccessToDisplay.forEach(element => {
                        // let vaccineClassTemp = element.label.split(' - ')[1];
                        let vaccineClassTemp = element.vaccineClass;
                        if(vaccineClassTemp && finalVaccineClass.indexOf(vaccineClassTemp) < 0) {
                            finalVaccineClass.push(vaccineClassTemp);
                            finalVaccineClassToDisplay.push({label: vaccineClassTemp, value: vaccineClassTemp});
                        }
                    });
                    this.vaccineClassListToDisplay = finalVaccineClassToDisplay;
                }
                this.vaccineClassListToDisplay && this.vaccineClassListToDisplay.length >0 && this.vaccineClassListToDisplay.sort((a,b) => (a.label > b.label) ? 1 : ((b.label > a.label) ? -1 : 0));
                this.showSpinner = false;
            }).catch(error => {
                // TODO Error handling
                 this.error = error;
                this.showSpinner = false;
                showAsyncErrorMessage(this, error);
            });
    }


    resetValues() {
        this.resetClicked = true;
        this.dispatchEvent(new CustomEvent("reset"));
        this.searchObj.sDate = undefined;
        this.searchObj.eDate = undefined;
        this.activePrivateAccess = false;
        this.vaccineClassDisabled = true;
        this.activeEventProcess = false;
        this.searchObj.privateAccess = '';
        this.searchObj.eventProcess = undefined;
        this.activeDateTimePrefrence = true;
        this.activeEventType = true;
        this.isError = false;
        this.searchObj.selCounty = undefined;
        this.searchObj.selSpoBy = this.availableSpoBy[0].value;
        this.isShowCustomDateRange = false;
        this.searchObj.privateAccessIdList = [];
        this.searchObj.selectedValccineClass = null;
        this.availablePrivateAccessToDisplay = [];
        this.loadFilters();
    }

    focusFirstEle() {
        const focusableElements = 'lightning-combobox, lightning-button-icon, button, input, lightning-helptext, lightning-input, lightning-button';
        const modal = this.template.querySelector('.slds-modal');
        if (modal) {
            const firstFocusableElement = modal.querySelectorAll(focusableElements)[0];
            const focusableContent = modal.querySelectorAll(focusableElements);
            const lastFocusableElement = focusableContent[focusableContent.length - 1];
            firstFocusableElement.focus();
            this.template.addEventListener('keydown', function (event) {
                let isTabPressed = event.key === 'Tab' || event.keyCode === 9;
                if (!isTabPressed) {
                    return;
                }
                if (event.shiftKey) {
                    if (this.activeElement === firstFocusableElement) {
                        lastFocusableElement.focus();
                        event.stopPropagation()
                        event.preventDefault();
                    }
                } else {
                    if (this.activeElement === lastFocusableElement) {
                        firstFocusableElement.focus();
                        event.preventDefault();
                        event.stopPropagation()
                    }
                }
            });
        }
    }
}