/*
    Author : A Vamsi Mudaliar
    Email : vamsi.mudaliar@mtxb2b.com
    Desc : Send Email to contacts to make them take their next dose when it reached a threshold.
    */
    global with sharing class VT_TVRS_ScheduleNextDoseEmailBatch implements Database.Batchable <sObject>, Schedulable{
        public String patientId;
        public VT_TVRS_ScheduleNextDoseEmailBatch() {
            
        }

        public VT_TVRS_ScheduleNextDoseEmailBatch(String patientId) {
            this.patientId = patientId;
        }
        
        global Database.QueryLocator start(Database.BatchableContext bc){
            List<String> notificationProcess = new List<String>{
                VT_TS_Constants.SKIPPED_ALREADY_SCHEDULED,
                VT_TS_Constants.SEND_TEXT,
                VT_TS_Constants.SKIPPED_OUT_OF_STATE
            };
            String query =  ' SELECT Id, Email_Workflow_Datetime__c,Event_Process__c,Patient__c,Status__c, ';
            query += ' Event__r.Private_Access__r.Requires_Followup_Dose__c,Next_Dose_Notification_Process__c, ';
            query += ' Next_Dose_Notification_Sent_Email__c, Event__r.Private_Access__r.Vaccine_Type__c, ';
            query += ' Appointment_Complete_Date__c,Event__r.Private_Access__r.Weak_Immune_Min_Days__c, ';
            query += ' Event__r.Private_Access__r.Min__c,Notification_Date__c,Appointment_Date__c, ';
            query += ' Patient__r.HasOptedOutOfEmail, Event__r.Private_Access__r.Vaccine_Class_Name__c, ';
            query += ' Event__r.Private_Access__r.Alternate_Doses__c, Event__r.Private_Access__r.Is_Alternate_Dose_Available__c, ';
            query += ' Event__r.Private_Access__r.Next_Dose_Include_Out_of_State__c, ';
            query += ' Event__r.Private_Access__r.Next_Dose_Notification_Type__c, ';
            query += ' Event__r.Private_Access__r.Next_Dose_Notification_Lead_Days__c ';
            query += ' FROM Appointment__c ';
            query += ' WHERE Status__c = \'Completed\' ';
            query += ' AND Event_Process__c = \'Vaccination\' ';
            query += ' AND Patient__c!=null '; 
            query += ' AND (Next_Dose_Notification_Process__c NOT IN : notificationProcess) ';
            query += ' AND Event__r.Private_Access__r.Vaccine_Type__c != null ';
            if(String.isNotBlank(patientId)) {
                query += ' AND Patient__c = :patientId ';
            }
            query += ' ORDER BY Patient__c,Event__r.Private_Access__r.Vaccine_Type__c';
            return Database.getQueryLocator(query);
        }

        global void execute(Database.BatchableContext bc, List<Appointment__c> appointmentList) {
            
           List<Appointment__c>updatedAppointments = TVRS_ScheduleNextDoseEmailHelper.updateAppointmentFields(appointmentList);
            if(!updatedAppointments.isEmpty()){
                VT_TS_SkipTriggerExecution.executeAppointmentTrigger = false;
                update updatedAppointments;
            }
          
        }
        global void finish(Database.BatchableContext bc){}

        global void execute(SchedulableContext ctx) {
            VT_TVRS_ScheduleNextDoseEmailBatch batch = new VT_TVRS_ScheduleNextDoseEmailBatch();
            Database.executeBatch(batch, 200);
        }
    }