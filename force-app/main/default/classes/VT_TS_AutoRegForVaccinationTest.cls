/**
 * @description       : 
 * @author            : Wasef Mohiuddin
 * @group             : 
 * @last modified on  : 02-17-2021
 * @last modified by  : Wasef Mohiuddin
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   02-17-2021   Wasef Mohiuddin   Initial Version
**/
@isTest
public class VT_TS_AutoRegForVaccinationTest {
    public static final Id Citizen_COVID = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();

    @testSetup
    public static void testData(){

        Contact conRec = new Contact(
            FirstName = 'Super', 
            LastName = 'Man', 
            RecordTypeId = Citizen_COVID,
            Email = 'superman@gmail.com.invalid',
            BirthDate = Date.today());
        insert conRec;

        Appointment__c apptRecord = new Appointment__c();
        apptRecord.Status__c = 'Scheduled';
        apptRecord.VaccineQuestion_DoseNum__c = 'First Dose';
        apptRecord.Patient__c = conRec.Id;
        insert apptRecord;

        Pre_Registration_Group__c preRegGrpRec = new Pre_Registration_Group__c();
        preRegGrpRec.Name = System.label.TVRS_Default_Pre_Reg_Group;
        preRegGrpRec.Status__c = 'Draft';
        preRegGrpRec.Email_Acknowledgement__c = false;
        preRegGrpRec.Verification_Delay__c = -1;
        insert preRegGrpRec;
    }

    @isTest
    public static void testAutoRegForVaccinationBatchExecution(){

        Test.startTest();
			Id batchId = Database.executeBatch(new VT_TS_AutoRegForVaccination());
        	System.assert( batchId != null );
		Test.stopTest();
    }

}