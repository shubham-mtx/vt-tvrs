import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import insertContacts from '@salesforce/apex/oktcContactUploadController.insertContacts';
import getEvents from '@salesforce/apex/oktcContactUploadController.getEvents';
import getAccountDetails from '@salesforce/apex/oktcContactUploadController.getAccountDetails';
import TVRS_DOWNLOAD_TEMPLATE_GUIDE from '@salesforce/label/c.TVRS_DOWNLOAD_TEMPLATE_GUIDE';
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';
export default class OktcContactUpload extends LightningElement {

    filesUploaded = [];
    @track fileName; 
    @track showSpinner = false;
    @track showContactUpload = false;
    file;
    fileContents;
    fileReader;
    content;
    MAX_FILE_SIZE = 1500000;
    result;
    @track showFileName = false;
    @track listOfEvents = [];
    selectedEvent;
    selectedEventVacination;
    headerData;
    reverseMap;
    @track accId;
    @track eventProcess;
    @track headerDataVacination;
    @track reverseMapVacination;

    get templateDownloadLink() {
        let parsedURL = JSON.parse(TVRS_DOWNLOAD_TEMPLATE_GUIDE);
        let finalVaccinationURL = parsedURL.vaccination.split(".com")[1];
        let finalTestingURL = parsedURL.testing.split(".com")[1];
        return {
            vaccination : location.origin+finalVaccinationURL,
            testing : location.origin+finalTestingURL
        }
    } 

    connectedCallback() {
        getAccountDetails()
          .then(result => {
            this.eventProcess = result[0].Event_Process__c;
            this.accId = result[0].Id;
            if(this.eventProcess == 'Vaccination'){
                this.showContactUpload = false;
            }else{
                this.showContactUpload = true;
            }
            getEvents({
                accId: this.recordId,
                eventProcess: this.eventProcess
            })
                .then(result => {
                    if (result.eventWrapperList !== undefined) {
                        var eventList = result.eventWrapperList;
                        for (let i = 0; i < eventList.length; i++) {
                            this.listOfEvents = [...this.listOfEvents, { value: eventList[i].eventId, label: eventList[i].eventName }];
                        }
                    }
                    if (result.headerToReadableStringMap !== undefined) {
                        this.headerData = result.headerToReadableStringMap;
                        this.reverseMap = result.reverseMap;
                    }
                    //S-17667 Start
                if (result.headerToReadableStringVacination !== undefined) {
                    this.headerDataVacination = result.headerToReadableStringVacination;
                    this.reverseMapVacination = result.reversedMapVacination;
                }
                
                //S-17667 END 
    
                }).catch(error => {
                    showAsyncErrorMessage(this,error);
                })
          })
          .catch(error => {
            showAsyncErrorMessage(this,error);
        });

            
    }
    handleEventChange(event) {
        this.selectedEvent = event.detail.value;
    }

    downloadCSVFile() {
        let rowEnd = '\n';
        let csvString = '';
        // this set elminates the duplicates if have any duplicate keys
        let rowData = {};
        rowData = Object.keys(this.headerData);
        // splitting using ','
        csvString += rowData.join(',');
       // csvString += rowEnd;

        let downloadElement = document.createElement('a');

        // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
        downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
        downloadElement.target = '_self';
        // CSV File Name
        downloadElement.download = 'ContactUpload.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
       downloadElement.click();
    }

    // Upload Functionality

    handleFileChange(event) {
        this.filesUploaded = event.target.files;
        this.fileName = event.target.files[0].name;
        if (this.filesUploaded.length > 0) {
            let isError = false;
            for (let index = 0; index < this.filesUploaded.length; index++) {
                if( this.filesUploaded[index].name.substring(this.filesUploaded[index].name.length-4, this.filesUploaded[index].name.length) !== ".csv" ) {
                    isError = true;
                }
            }
            if ( isError === false ) {
                this.showFileName = true;
                this.handleUpload();
            } else {
                this.showTost('error', 'Only CSV File is Supported');
            }
        }
    }

    handleFileDelete() {
        this.showFileName = false;
        this.result = null;
        this.filesUploaded.length = 0;
        this.fileName = '';

    }

    handleUpload() {
        if (this.filesUploaded.length > 0) {
            this.uploadHelper();
        }
        else {
            this.fileName = 'Please select file to upload!!';
        }
    }

    uploadHelper() {
        this.file = this.filesUploaded[0];
        if (this.file.size > this.MAX_FILE_SIZE) {
            return;
        }
        this.showSpinner = true;
        this.fileReader = new FileReader();
        this.fileReader.readAsText(this.file, "UTF-8");

        this.fileReader.onload = (() => {
            this.fileContents = this.fileReader.result;
            this.result = this.csvToJson(this.fileContents);
            this.insertResults(result);
        });
        this.fileReader.onloadend = (() => {
            this.showSpinner = false;
        });
        this.fileReader.onerror = function (evt) {
            this.showSpinner = false;
        }
    }

    csvToJson(csv) {
        var arr = [];
        arr = csv.split('\n');
        if(arr.length < 2) {
            this.showFileName = null;
            this.fileName = null;
            this.dispatchEvent(new ShowToastEvent({
                message: 'Please upload atleast one record',
                variant: 'error'
            }));
            return;
        }
        var jsonObj = [];
        var headers = arr[0].split(',');
        if(this.eventProcess !== 'Vaccination' && headers.indexOf('1st or 2nd Dose') >= 0) {
            this.dispatchEvent(new ShowToastEvent({
                message: 'It looks like you are uploading invalid file, please download from the given link and try again.',
                variant: 'error'
            }));
            this.showFileName = undefined;
            this.fileName = undefined;
            return;
        } else if(this.eventProcess === 'Vaccination' && headers.indexOf('1st or 2nd Dose') < 0) {
            this.dispatchEvent(new ShowToastEvent({
                message: 'It looks like you are uploading invalid file, please download from the given link and try again.',
                variant: 'error'
            }));
            this.showFileName = undefined;
            this.fileName = undefined;
            return;
        }
        let invalidHeaders = [];
        let tempMetadataHeaders = '';

        for (let i = 0; i < headers.length; i++) {
            if(headers[i] === 'Minor State Representative Consent\r'){
                headers[i] = 'Minor State Representative Consent';
            }
        }
    
        if(this.eventProcess === 'Vaccination'){
 
             tempMetadataHeaders = Object.keys(this.headerDataVacination);
        }else{
             tempMetadataHeaders = Object.keys(this.headerData);
        }
       

        if(this.eventProcess === 'Vaccination'){
            for (let i = 0; i < headers.length; i++) {
                if (this.headerDataVacination[headers[i].replace('\r','').trim()] !== undefined) {
                    headers[i] = this.headerDataVacination[headers[i].trim()];
                } else {
                    invalidHeaders.push(headers[i].trim()+' to '+tempMetadataHeaders[i]);
                }
            }

        }else{

            for (let i = 0; i < headers.length; i++) {
                if (this.headerData[headers[i].trim()] !== undefined) {
                    headers[i] = this.headerData[headers[i].trim()];
                } else {
                    invalidHeaders.push(headers[i].trim()+' to '+tempMetadataHeaders[i]);
                }
            }

        }
       
        if( invalidHeaders.length > 0 ) {
            //this.showTost('error', 'Please correct the headers: '+invalidHeaders.join(', '));
            this.showTost('error','Invalid Headers Detected: Please correct the issue and try upload again');
            this.handleFileDelete();
            return false;
        }

        let dataArr = this.CSVToArray(csv, undefined );
       
        if(dataArr[dataArr.length-1].length !== 28) { // removing 4 columns 
            dataArr.pop();
        }

        for (var i = 1; i < dataArr.length; i++) {
            var data = dataArr[i];
            var obj = {};
            for (var j = 0; j < data.length; j++) {
                obj[headers[j].trim()] = data[j].trim();
            }
            jsonObj.push(obj);
        }

        var json = JSON.stringify(jsonObj);
        return json;
    }

    CSVToArray( strData, strDelimiter ){
		// Check to see if the delimiter is defined. If not,
		// then default to comma.
		strDelimiter = (strDelimiter || ",");

		// Create a regular expression to parse the CSV values.
		var objPattern = new RegExp(
			(
				// Delimiters.
				"(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

				// Quoted fields.
				"(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

				// Standard fields.
				"([^\"\\" + strDelimiter + "\\r\\n]*))"
			),
			"gi"
			);


		// Create an array to hold our data. Give the array
		// a default empty first row.
		var arrData = [[]];

		// Create an array to hold our individual pattern
		// matching groups.
		var arrMatches = null;


		// Keep looping over the regular expression matches
		// until we can no longer find a match.
		while (arrMatches = objPattern.exec( strData )){

			// Get the delimiter that was found.
			var strMatchedDelimiter = arrMatches[ 1 ];

			// Check to see if the given delimiter has a length
			// (is not the start of string) and if it matches
			// field delimiter. If id does not, then we know
			// that this delimiter is a row delimiter.
			if (
				strMatchedDelimiter.length &&
				(strMatchedDelimiter !== strDelimiter)
				){

				// Since we have reached a new row of data,
				// add an empty row to our data array.
				arrData.push( [] );

			}


			// Now that we have our delimiter out of the way,
			// let's check to see which kind of value we
			// captured (quoted or unquoted).
			if (arrMatches[ 2 ]){

				// We found a quoted value. When we capture
				// this value, unescape any double quotes.
				var strMatchedValue = arrMatches[ 2 ].replace(
					new RegExp( "\"\"", "g" ),
					"\""
					);

			} else {

				// We found a non-quoted value.
				var strMatchedValue = arrMatches[ 3 ];

			}

			// Now that we have our value string, let's add
			// it to the data array.
			arrData[ arrData.length - 1 ].push( strMatchedValue );
		}

		// Return the parsed data.
		return( arrData );
	}

    showTost(type, message) {
        this.dispatchEvent(new ShowToastEvent({
            title: '',
            variant: type,
            message: message,
            duration: 5000,
        }));
    }

    insertResults(jsonstr, eventId) {
        this.data = [];
        this.showSpinner = true;
        insertContacts({
            strfromle: jsonstr,
            eventId: eventId
        })
            .then(result => {
                if (result.fieldWrapper == null) {
                    this.showSpinner = false;
                    this.dispatchEvent(new ShowToastEvent({
                        title: '',
                        variant: result.type,
                        message: result.message,
                    }));
                    this.showFileName = false;
                    this.result = null;
                    return;
                } else {
                    if (result.type === 'warning' || result.type === 'success') {
                        this.downloadResultFile(result.fieldWrapper);
                        this.dispatchEvent(new ShowToastEvent({
                            title: '',
                            variant: result.type,
                            message: result.message,
                        }));
                    }
                    // this.handleFileDelete();
                    this.showSpinner = false;
                    this.dispatchEvent(new ShowToastEvent({
                        title: '',
                        variant: result.type,
                        message: result.message,
                    }));
                    this.showFileName = false;
                    this.result = null;
                }

            })
            .catch(error => {
                showAsyncErrorMessage(this,error);
                this.showSpinner = false;
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: error.body.message,
                }));
            })
    }

    downloadResultFile(object) {
        this.data = [];
        object.forEach(record => {
            let temparr = {};
            try {
                for (const [key, value] of Object.entries(this.reverseMap)) {
                    temparr[value] = record[key];
                }
                temparr.status = record.status;
                temparr.reason = record.reason;
                this.data.push(temparr);

            } catch (e) {
                showAsyncErrorMessage(this,error);
            }
        })
        if (this.data) {
            this.downloadCSVFileForStatus();
        }
    }

    handleSave() {
        try {
            if (this.result) {
                if (this.selectedEvent === undefined) {
                    var eventInput = this.template.querySelector("lightning-combobox");
                    eventInput.reportValidity();
                } else {
                    this.insertResults(this.result, this.selectedEvent);
                }

                if (this.selectedEventVacination === undefined) {
                    var eventInput = this.template.querySelector("lightning-combobox");
                    eventInput.reportValidity();
                } else {
                    this.insertResults(this.result, this.selectedEventVacination);
                }

                
            } else {
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Please select a CSV file'
                }));
            }
        } catch (e) {
            showAsyncErrorMessage(this,error);
        }
    }

    downloadCSVFileForStatus() {
        let rowEnd = '\n';
        let csvString = '';
        // this set elminates the duplicates if have any duplicate keys
        let rowData = new Set();

        // getting keys from data
        this.data.forEach(function (record) {
            Object.keys(record).forEach(function (key) {
                rowData.add(key);
            });
        });

        // Array.from() method returns an Array object from any object with a length property or an iterable object.
        rowData = Array.from(rowData);

        // splitting using ','
        csvString += rowData.join(',');
        csvString += rowEnd;

        // main for loop to get the data based on key value
        for (let i = 0; i < this.data.length; i++) {
            let colValue = 0;

            // validating keys in data
            for (let key in rowData) {
                if (rowData.hasOwnProperty(key)) {
                    // Key value 
                    // Ex: Id, Name
                    let rowKey = rowData[key];
                    // add , after every value except the first.
                    if (colValue > 0) {
                        csvString += ',';
                    }
                    // If the column is undefined, it as blank in the CSV file.
                    let value = this.data[i][rowKey] === undefined ? '' : this.data[i][rowKey];
                    csvString += '"' + value + '"';
                    colValue++;
                }
            }
            csvString += rowEnd;
        }

        // Creating anchor element to download
        let downloadElement = document.createElement('a');
        var universalBOM = "\uFEFF";
        // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
        downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(universalBOM + csvString);
        downloadElement.target = '_self';
        // CSV File Name
        downloadElement.download = 'Bulk Upload Status.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
        downloadElement.click();
    }

    handleEventChangeVacination(event) {
        this.selectedEventVacination = event.detail.value;
    }

    downloadVacinationCSVFile(){
        let rowEnd = '\n';
        let csvString = '';
        let rowData = {};
        rowData = Object.keys(this.headerDataVacination);
         csvString += rowData.join(',');
        //csvString += rowEnd;
        let downloadElement = document.createElement('a');
        downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
        downloadElement.target = '_self';
        downloadElement.download = 'ContactUpload.csv';
        document.body.appendChild(downloadElement);
       downloadElement.click();
    }

}