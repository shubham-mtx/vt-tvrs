import {
    LightningElement,
    track,
    api
} from 'lwc';
import getConsent from "@salesforce/apex/OkpcContactInformationController.getConsent";
import setConsent from "@salesforce/apex/OkpcContactInformationController.setConsent";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class OkpcConsentPage extends LightningElement {
    @track consentvalue = false;
    @track screeningvalue = false;
    @track showSpinner = false;
    @api currentStep;
    @api dependentContactId='';

    connectedCallback() {
        this.getContact();
        console.log('current step: ', this.currentStep);

    }

    getContact() {
        this.showSpinner = true;
        if ( this.dependentContactId == '') {
            getConsent({ contactid : null})
            .then(result => {
                this.consentvalue = result.Consent__c;
                this.screeningvalue = result.Is_Symptomatic__c;
                 console.log("consnet=>", JSON.stringify(result));
                this.showSpinner = false;
            })
            .catch(error => {
                console.log('error=>', JSON.stringify(error));
                this.showSpinner = false;
            })
        }else{
            getConsent({ contactid : this.dependentContactId})
            .then(result => {
                this.consentvalue = result.Consent__c;
                this.screeningvalue = result.Is_Symptomatic__c;
                 console.log("consnet=>", JSON.stringify(result));
                this.showSpinner = false;
            })
            .catch(error => {
                console.log('error=>', JSON.stringify(error));
                this.showSpinner = false;
            })
        }
       
    }

    saveConsent() {

        if (this.consentvalue != true) {
            console.log("Please check the consent");
            let tempObj = {};
            tempObj.message = "Please select consent to move forward";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return;
        }

        this.showSpinner = true;
        setConsent({
            consent: this.consentvalue,
            screeningvalue : this.screeningvalue,
            currentstep : this.currentStep
            })
            .then(result => {
                console.log('success saving consnt');
                this.showSpinner = false;
                let tempObj = {};
                tempObj.message = "saved successfully";
                tempObj.status = "success";
                tempObj.currentStep = "1";
                this.sendEventToParent(tempObj);
            })
            .catch(error => {
                console.log("error =>", JSON.stringify(error));
                this.showSpinner = false;
                
            })
    }


    handleConsentChange(event) {
        console.log(event.target.checked);
        this.consentvalue = event.target.checked;
    }

    handleScreeningChange(event) {
        console.log(event.target.checked);
        this.screeningvalue = event.target.checked;
    }

    sendEventToParent(obj) {
        const selectedEvent = new CustomEvent("consentsaved", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }

   
}