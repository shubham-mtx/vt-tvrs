/**
 * @author Balram Dhawan
 * @email balram.dhawan@mtxb2b.com
 * @desc This Component is used to search Event on public page on Covid Portal
*/

import { LightningElement } from 'lwc';
import GUEST_USER_SEARCH_EVENT_INFO from '@salesforce/label/c.VTTS_CP_guestUserSearchEventInformation';

export default class Vtts_cp_searchEventPublic extends LightningElement {
    currentStep = 1;
    accountId;
    eventId;
    slotId;
    registrationComplete = false;
    guestUserSearchEventInformation = GUEST_USER_SEARCH_EVENT_INFO.replace('create an account', '<a href="selfregistration">create an account</a>').replace('log in', '<a href="login">log in</a>');

    get isStep1() {
        return this.currentStep === 1;
    }

    get isStep2() {
        return this.currentStep === 2;
    }

    get isStep3() {
        return this.currentStep === 3;
    }

    get modalHeader() {
        return this.isStep1 ? 'Testing Site & Event' : this.isStep2 ? 'Appointment Slot' : 'Contact Information';
    }

    get isNextEnabled() {
        return !((this.isStep1 && this.accountId && this.eventId) || (this.isStep2 && this.slotId));
    }

    //update wrt I-26836=> height 63vh to 410px
    get contentStyle() {
        return this.isStep3 ? '' : 'overflow-y: auto; height: 410px;';
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @desc This method is used to manage Next and Previous Step in Map
    */
    handleClick(event) {
        switch (event.target.name) {
            case 'previous':
                this.currentStep--;
                if (this.isStep1) {
                    this.slotId = '';
                }
                this.accountId = '';
                this.eventId = '';
                break;
            case 'next':
                this.currentStep++;
                break;
            case 'finish':
                this.currentStep = 1;
                this.accountId = '';
                this.eventId = '';
                this.slotId = '';
                this.registrationComplete = false;
                break;
            default:
                break;
        }
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @desc To go back to Appointment
    */
    backToAppointment(){
        this.currentStep = 1;
        this.slotId = '';
    }
    
    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @desc Selection Map Marker
    */
    handleEventSelected(event) {
        this.accountId = event.detail.accountId;
        this.eventId = event.detail.eventId;
    }

    /**
     * @author Balram Dhawan
     * @email balram.dhawan@mtxb2b.com
     * @desc Change Selected EventId to Null If user selects past date
    */
    pastDateSelection() {
        this.eventId = false;
    }
}