/**
 * @description       : Helper class for updating fields on appointment record for scheduling next dose reminder.
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 12-22-2021
 * @last modified by  : Mohit Karani
**/
public with sharing class TVRS_ScheduleNextDoseEmailHelper {

    /**
    * @description : Method to update fields on appointment record.
    * @author Mohit Karani | 11-30-2021 
    * @param appointmentList 
    * @return List<Appointment__c> 
    **/
    public static List<Appointment__c> updateAppointmentFields(List<Appointment__c> appointmentList){
        Map<String,Appointment__c> mapOfContactAppointments = new Map<String,Appointment__c>();         // contact and its latest appointment
        Map<String,Pre_Registration__c> mapOfPatientImmune = new Map<String,Pre_Registration__c>();     // 
        Set<String> nextDoseVaccineTypes = new Set<String>{
            'Vaccination-2',
            'Vaccination-3'
        };                                           // next dose type (Vaccination-2, Vaccination-3) for patient
        Map<String,List<String>> mapOfContactVaccineType = new Map<String,List<String>>();
        List<Appointment__c> updatedAppointments = new List<Appointment__c>();
        List<String> statusText = new List<String>{'Cancelled','Scheduled','Completed'};
        VT_TS_AppointmentReminderWrapper appointmentReminder = (VT_TS_AppointmentReminderWrapper) JSON.deserialize(System.Label.TVRS_APPOINTMENT_REMINDERS , VT_TS_AppointmentReminderWrapper.class);
        Set<String> vaccineClasses = new Set<String>();
        Set<Id> preRegGroupIdSet = new Set<Id>();
        List<Appointment__c> appointmentToCheckForEligibility = new List<Appointment__c>();
        List<Appointment__c> appoinmentsWhoseNextDoseCompleted = new List<Appointment__c>();
        // Contact and its latest Appointment Dose also adding next doses into the set.

        for(Appointment__c app:appointmentList) {
            if(mapOfContactAppointments.containsKey(app.Patient__c)){
                appoinmentsWhoseNextDoseCompleted.add(mapOfContactAppointments.get(app.Patient__c));
            }
            mapOfContactAppointments.put(app.Patient__c,app);
        }
        
        for(Pre_Registration__c prReg:  [SELECT Id,Is_Immune_Weak__c,Contact__c,Contact__r.MailingState,
                                        Early_Group_2_Access_Group__r.Skip_Email_Notification__c,Early_Group_2_Access_Group__c,
                                        Early_Group1_Health__c,Pre_Registration_Group__c
                                        FROM Pre_Registration__c 
                                        WHERE Contact__c IN :mapOfContactAppointments.keySet()]) {
                mapOfPatientImmune.put(prReg.Contact__c, prReg);
        }
        for(Appointment__c appointmentRecord : [SELECT Id, Status__c, Patient__c,Event__r.Private_Access__r.Weak_Immune_Min_Days__c, Event__r.Private_Access__r.Vaccine_Class_Name__c, 
                                                        Event__r.Private_Access__r.Min__c,Appointment_Complete_Date__c,
                                                        Event__r.Private_Access__r.Vaccine_Type__c,Appointment_Date__c, Event__r.Private_Access__r.Alternate_Doses__c, 
                                                        Patient__r.HasOptedOutOfEmail, Event__r.Private_Access__r.Is_Alternate_Dose_Available__c  
                                                        FROM Appointment__c 
                                                        WHERE Patient__c IN: mapOfContactAppointments.keySet() 
                                                        AND Status__c IN :statusText 
                                                        AND Event__r.Private_Access__r.Vaccine_Type__c IN :nextDoseVaccineTypes  
                                                        AND Event_Process__c =: VT_TS_Constants.VACCINATION_TEXT
                                                        AND Id NOT IN :appointmentList
                                                        WITH SECURITY_ENFORCED
                                                        ORDER BY Event__r.Private_Access__r.Vaccine_Type__c DESC]) {

            if(mapOfContactVaccineType.get(appointmentRecord.Patient__c) == null){
                mapOfContactVaccineType.put(appointmentRecord.Patient__c , new List<String>());
            }
            List<String> vaccineList = mapOfContactVaccineType.get(appointmentRecord.Patient__c);
            vaccineList.add(appointmentRecord.Event__r.Private_Access__r.Vaccine_Type__c);
            mapOfContactVaccineType.put(appointmentRecord.Patient__c,vaccineList);

        }

        // Iterating through Latest Completed Appointments
        Integer gap;
        Datetime appCompletedDate;
        String vaccineTypeFirst;
        String nextDoseString;
        Integer calMinDate;
        Date runningBatchDate;
        Boolean skipEmailNotification;
        VTTS_AppointmentHelper appHelper = new VTTS_AppointmentHelper();

        for(Appointment__c app:mapOfContactAppointments.values()) {  
            Pre_Registration__c preRegRecord = mapOfPatientImmune.get(app.Patient__c);
            if(preRegRecord == null){
                continue;
            }
            vaccineTypeFirst = app.Event__r.Private_Access__r.Vaccine_Type__c;
            nextDoseString = VT_TS_Constants.VACCINATION_TEXT + '-'+(Integer.valueOf(vaccineTypeFirst.split('-')[1])+1);
            calMinDate = (Integer) ((preRegRecord.Is_Immune_Weak__c=='Yes' && vaccineTypeFirst == VT_TS_Constants.SECOND_DOSE_TEXT)? (app.Event__r.Private_Access__r.Weak_Immune_Min_Days__c != NULL ) ? app.Event__r.Private_Access__r.Weak_Immune_Min_Days__c : 0:(app.Event__r.Private_Access__r.Min__c != null) ? app.Event__r.Private_Access__r.Min__c : 0);
            runningBatchDate = Date.today().addDays(appointmentReminder.day_to_add_for_testing );
            appCompletedDate = app.Appointment_Complete_Date__c != NULL ? app.Appointment_Complete_Date__c : app.Appointment_Date__c; 
            gap = runningBatchDate.daysBetween(appCompletedDate.date() + calMinDate);                 
            // Date calDate = app.Appointment_Complete_Date__c.date() + calMinDate - (Integer)appointmentReminder.get('notificationLeadTime'); // latest appointment end date + calMinDate - 7 (LeadResponseTime)
            skipEmailNotification = (app.Patient__r.HasOptedOutOfEmail || preRegRecord.Early_Group_2_Access_Group__r.Skip_Email_Notification__c ||  (String.isNotBlank( preRegRecord.Contact__r.MailingState) &  (preRegRecord.Contact__r.MailingState != VT_TS_Constants.VERMONT_TEXT && !app.Event__r.Private_Access__r.Next_Dose_Include_Out_of_State__c))); 
            app.Next_Dose_Type__c = app.Event__r.Private_Access__r.Next_Dose_Notification_Type__c;
            app.Next_Dose_Notification_Sent_Email__c =  app.Patient__r.HasOptedOutOfEmail ? 
                                                        VT_TS_Constants.EMAIL_OPTED_OUT :
                                                        app.Next_Dose_Notification_Sent_Email__c == VT_TS_Constants.RESEND_TEXT ? 
                                                        app.Next_Dose_Notification_Sent_Email__c : null;
            app.Next_Dose_Notification_Process__c  = ((mapOfContactVaccineType.containsKey(app.Patient__c) && mapOfContactVaccineType.get(app.Patient__c) != null && !mapOfContactVaccineType.get(app.Patient__c).isEmpty() &&(mapOfContactVaccineType.get(app.Patient__c).contains(nextDoseString) || Integer.valueOf(mapOfContactVaccineType.get(app.Patient__c)[0].split('-')[1]) > Integer.valueOf(nextDoseString.split('-')[1])))) ?
                                                     VT_TS_Constants.SKIPPED_ALREADY_SCHEDULED : 
                                                     app.Event__r.Private_Access__r.Requires_Followup_Dose__c == 'No' ?
                                                     VT_TS_Constants.NOT_REQUIRED_STATUS :
                                                     (preRegRecord.Contact__r.MailingState != VT_TS_Constants.VERMONT_TEXT && !app.Event__r.Private_Access__r.Next_Dose_Include_Out_of_State__c) ?
                                                     VT_TS_Constants.SKIPPED_OUT_OF_STATE :
                                                     app.Patient__r.HasOptedOutOfEmail ?
                                                     VT_TS_Constants.SEND_TEXT:
                                                     null;
            app.Next_Dose_Start_Date__c = appCompletedDate.date() + calMinDate ;
            app.Notification_Date__c = (app.Event__r.Private_Access__r.Next_Dose_Notification_Lead_Days__c != NULL) ? appCompletedDate.date() + calMinDate - Integer.valueOf(app.Event__r.Private_Access__r.Next_Dose_Notification_Lead_Days__c) : appCompletedDate.date() + calMinDate - 7;
            app.Notification_Date__c = (app.Notification_Date__c > Date.today()) ? app.Notification_Date__c :  Date.today();
            updatedAppointments.add(app);
            skipEmailNotification = skipEmailNotification ? skipEmailNotification : (app.Next_Dose_Notification_Process__c ==  VT_TS_Constants.SKIPPED_ALREADY_SCHEDULED || app.Next_Dose_Notification_Process__c== VT_TS_Constants.NOT_REQUIRED_STATUS || app.Next_Dose_Notification_Process__c== VT_TS_Constants.SKIPPED_OUT_OF_STATE );
            if(!skipEmailNotification) { 
                vaccineClasses.add(app.Event__r.Private_Access__r.Vaccine_Class_Name__c);
                if(app.Event__r.Private_Access__r.Is_Alternate_Dose_Available__c && String.isNotBlank(app.Event__r.Private_Access__r.Alternate_Doses__c)) {
                    vaccineClasses.addAll(app.Event__r.Private_Access__r.Alternate_Doses__c.split(';'));
                }
                preRegGroupIdSet.add(preRegRecord.Early_Group_2_Access_Group__c);
                preRegGroupIdSet.add(preRegRecord.Early_Group1_Health__c);
                preRegGroupIdSet.add(preRegRecord.Pre_Registration_Group__c);
                appointmentToCheckForEligibility.add(app);
            }
        }
        Set<String> eligibilityCheckSet = new Set<String>();
        if(!appointmentToCheckForEligibility.isEmpty()){
            for(Private_Access_Assignment__c privateAccessAssignment : [SELECT Id,Vaccine_Class__c,Pre_Registration_Groups__c,
                                                                        Vaccination_Type__c
                                                                        FROM Private_Access_Assignment__c
                                                                        WHERE Active__c = TRUE 
                                                                        AND Private_Access_Active__c = TRUE
                                                                        AND Vaccination_Type__c IN : nextDoseVaccineTypes
                                                                        AND Vaccine_Class__c IN : vaccineClasses
                                                                        AND Pre_Registration_Groups__c IN : preRegGroupIdSet
                                                                        AND Vaccine_Class__c != NULL
                                                                        AND Vaccination_Type__c != NULL
                                                                        WITH SECURITY_ENFORCED]){
                String key = privateAccessAssignment.Vaccination_Type__c + '-' + privateAccessAssignment.Vaccine_Class__c + '-' +privateAccessAssignment.Pre_Registration_Groups__c; 
                eligibilityCheckSet.add(key);

            }
        }
        
        for(Appointment__c appointment : appointmentToCheckForEligibility){
            Pre_Registration__c preRegRecord = mapOfPatientImmune.get(appointment.Patient__c);
            String preRegGroup = preRegRecord.Pre_Registration_Group__c;
            String earlyGroup1Health = preRegRecord.Early_Group1_Health__c;
            String earlyGroup2Access = preRegRecord.Early_Group_2_Access_Group__c;
            String primaryVaccineClass = appointment.Event__r.Private_Access__r.Vaccine_Class_Name__c;
            String vaccineType = VT_TS_Constants.VACCINATION_TEXT + '-'+(Integer.valueOf(appointment.Event__r.Private_Access__r.Vaccine_Type__c.split('-')[1])+1); 
            String keyFirst =  vaccineType+ '-' +primaryVaccineClass + '-' + preRegGroup; 
            String keySecond = vaccineType+ '-' + primaryVaccineClass + '-' + earlyGroup1Health;
            String keyThird = vaccineType + '-' + primaryVaccineClass + '-' + earlyGroup2Access;
            Boolean isIneligible = true;
            vaccineTypeFirst = appointment.Event__r.Private_Access__r.Vaccine_Type__c;
            calMinDate = (Integer) ((preRegRecord.Is_Immune_Weak__c=='Yes' && vaccineTypeFirst == VT_TS_Constants.SECOND_DOSE_TEXT)? (appointment.Event__r.Private_Access__r.Weak_Immune_Min_Days__c != NULL ) ? appointment.Event__r.Private_Access__r.Weak_Immune_Min_Days__c : 0 : (appointment.Event__r.Private_Access__r.Min__c != null) ? appointment.Event__r.Private_Access__r.Min__c : 0);
            runningBatchDate = Date.today().addDays(appointmentReminder.day_to_add_for_testing );
            appCompletedDate = appointment.Appointment_Complete_Date__c != NULL ? appointment.Appointment_Complete_Date__c : appointment.Appointment_Date__c; 
            gap = runningBatchDate.daysBetween(appCompletedDate.date() + calMinDate); 
            Boolean notificationToBeSentToday = (Integer.valueOf(appointment.Event__r.Private_Access__r.Next_Dose_Notification_Lead_Days__c) == Integer.valueOf(gap) || appointment.Notification_Date__c == Date.today());
            if(eligibilityCheckSet.contains(keyFirst) || eligibilityCheckSet.contains(keySecond) || eligibilityCheckSet.contains(keyThird)){
                isIneligible = false;
                appointment.Next_Dose_Notification_Process__c = notificationToBeSentToday ? VT_TS_Constants.SEND_TEXT : null;
                continue;
            }
            if(appointment.Event__r.Private_Access__r.Is_Alternate_Dose_Available__c && String.isNotBlank(appointment.Event__r.Private_Access__r.Alternate_Doses__c)){
                for(String alternateVaccine :appointment.Event__r.Private_Access__r.Alternate_Doses__c.split(';')){
                    keyFirst = vaccineType + '-' + alternateVaccine + '-' + preRegGroup;
                    keySecond = vaccineType + '-' + alternateVaccine + '-' + earlyGroup1Health;
                    keyThird = vaccineType + '-' + alternateVaccine + '-' + earlyGroup2Access;
                    if(eligibilityCheckSet.contains(keyFirst) || eligibilityCheckSet.contains(keySecond) || eligibilityCheckSet.contains(keyThird)){
                        isIneligible = false;
                        // appointment.Second_Dose_Reminder_Completed__c = true;
                        appointment.Next_Dose_Notification_Process__c =  notificationToBeSentToday ? VT_TS_Constants.SEND_TEXT : null; // this will trigger the workflow rule.
                        break;
                    }
                }
            }
            // appointment.Skip_Email_Notification__c =  (appointment.Second_Dose_Reminder_Completed__c) ? appointment.Skip_Email_Notification__c : true;
            appointment.Next_Dose_Notification_Process__c = (isIneligible) ?VT_TS_Constants.NOT_ELIGIBLE :  appointment.Next_Dose_Notification_Process__c;
        }

        for(Appointment__c appointmentRecord : appoinmentsWhoseNextDoseCompleted){
            Pre_Registration__c preRegRecord = mapOfPatientImmune.get(appointmentRecord.Patient__c);
            if(preRegRecord == null){
                continue;
            }
            vaccineTypeFirst = appointmentRecord.Event__r.Private_Access__r.Vaccine_Type__c;
            nextDoseString = VT_TS_Constants.VACCINATION_TEXT + '-'+(Integer.valueOf(vaccineTypeFirst.split('-')[1])+1);
            calMinDate = (Integer) ((preRegRecord.Is_Immune_Weak__c=='Yes' && vaccineTypeFirst == VT_TS_Constants.SECOND_DOSE_TEXT)? (appointmentRecord.Event__r.Private_Access__r.Weak_Immune_Min_Days__c != NULL ) ? appointmentRecord.Event__r.Private_Access__r.Weak_Immune_Min_Days__c : 0 : (appointmentRecord.Event__r.Private_Access__r.Min__c != null) ? appointmentRecord.Event__r.Private_Access__r.Min__c : 0);
            appointmentRecord.Next_Dose_Notification_Process__c = VT_TS_Constants.SKIPPED_ALREADY_SCHEDULED;
            appointmentRecord.Notification_Date__c = (appointmentRecord.Event__r.Private_Access__r.Next_Dose_Notification_Lead_Days__c != NULL) ? appCompletedDate.date() + calMinDate - Integer.valueOf(appointmentRecord.Event__r.Private_Access__r.Next_Dose_Notification_Lead_Days__c) : appCompletedDate.date() + calMinDate - 7;
            appointmentRecord.Notification_Date__c = (appointmentRecord.Notification_Date__c > Date.today()) ? appointmentRecord.Notification_Date__c :  Date.today();
            appointmentRecord.Next_Dose_Type__c = appointmentRecord.Event__r.Private_Access__r.Next_Dose_Notification_Type__c;
            appCompletedDate = appointmentRecord.Appointment_Complete_Date__c != NULL ? appointmentRecord.Appointment_Complete_Date__c : appointmentRecord.Appointment_Date__c; 
            appointmentRecord.Next_Dose_Start_Date__c = appCompletedDate.date() + calMinDate ;
            updatedAppointments.add(appointmentRecord);
        }
        return updatedAppointments;

    }
}