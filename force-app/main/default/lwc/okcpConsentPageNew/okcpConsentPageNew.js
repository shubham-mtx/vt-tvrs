import { LightningElement, track, api } from 'lwc';
import getConsent from "@salesforce/apex/OkpcContactInformationController.getConsent";

export default class OkpcConsentPage extends LightningElement {
    @api istesting = false;
    @track consentvalue = false;
    @track screeningvalue = false;
    @track showSpinner = true;
    @api currentStep;
    @api dependentContactId = '';
    @api isDependentContact;
    @api showAsteriskMessage;
    @api 
    set contactId(val){
        if( !(this.conId) ){
            this.conId = val;
            this.getContact();
        }
    }
    get contactId(){
        return this.conId;
    }

    
    @track conId;
    @api consentDetails = {};

    showConsentCheckbox;
    showPageHeader = true;

    connectedCallback() {
        this.showAsteriskMessage = (this.showAsteriskMessage || this.istesting);
        if (this.conId) {
            this.getContact();
        }
        else {
            
            if (Object.keys(this.consentDetails).length !== 0 && this.consentDetails.constructor === Object) {
                this.consentvalue = this.consentDetails.consentvalue;
                this.screeningvalue = this.consentDetails.screeningvalue;
            }

        }

        

        setTimeout(() => {
            this.showConsentCheckbox =  true;
            this.showSpinner = false;
        }, 2000);

        this.focusTitle();
    }

    getContact() {
        getConsent({
            contactid: this.conId
        }).then(result => {
            this.consentvalue = result.Consent__c;
            this.screeningvalue = result.Is_Symptomatic__c;
           
        }).catch(error => {
            
            this.showSpinner = false;
        })
    }

    saveConsent() {
        if (this.validate()) {
            if (this.conId) {
                let tempObj = {};
                tempObj.message = "saved successfully";
                tempObj.status = "success";
                tempObj.currentStep = "1";
                this.sendEventToParent(tempObj);
            } else {
                let tempObj = {};
                tempObj.message = "saved successfully";
                tempObj.status = "saveNew";
                tempObj.currentStep = "1";
                tempObj.consentParams = {
                    consentvalue: this.consentvalue,
                    screeningvalue: this.screeningvalue
                };
                this.sendEventToParent(tempObj);
            }
        }
    }

    validate() {
        let isValid = true;
        if (this.consentvalue != true) {
            isValid = false;
            let tempObj = {};
            tempObj.status = 'error';
            tempObj.message = 'Please select consent to move forward';
            this.sendEventToParent(tempObj);
            let forFocus = setInterval(() => {
                let input = this.template.querySelector('input');
                if(input) {
                    input.focus();
                    clearInterval(forFocus);
                }
            }, 100);
        }
        return isValid;
    }

    handleConsentChange(event) {
        this.consentvalue = event.target.checked;
    }

    handleScreeningChange(event) {
        this.screeningvalue = event.target.checked;
    }

    sendEventToParent(obj) {
        const selectedEvent = new CustomEvent("consentsaved", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }

    navigateToCancel() {
        this.dispatchEvent(new CustomEvent('cancelappointment'));
    }

    handleConsentCancel() {
        const event = new CustomEvent('consentcancel', {
            bubbles : true,
            composed : true
        });
        this.dispatchEvent(event);
    }

    focusTitle() {
        let focus = setInterval(() => {
            let cmp = this.template.querySelector('.pageTitle');
            if(cmp) {
                cmp.focus();
                setTimeout(() => {
                    this.showPageHeader = false;
                }, 1000);
                clearInterval(focus);
            }
        }, 200);   
    }
}