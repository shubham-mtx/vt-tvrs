@IsTest
private class VT_TS_ProcessEventChangeBatchTest {
    @testSetup
    private static void makeData() {
        // Account, Contact, Event, Slot, Appointment
        Account acc = new Account();
        acc.Name = 'Testing Account 1';
        acc.RecordTypeId = Account.SObjectType.getDescribe().RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        acc.Event_Process__c = 'Testing Site';
        acc.Vermont_County__c = 'Addison';
        acc.Business_Start_Time__c = Time.newInstance(00, 00, 00, 00);
        acc.Business_End_Time__c = Time.newInstance(11, 00, 00, 00);
        acc.Event_Type__c = 'State-Sponsored';
        insert acc;
        
        Account acc1 = new Account();
        acc1.Name = 'Testing Account 2';
        acc1.RecordTypeId = Account.SObjectType.getDescribe().RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        acc1.Event_Process__c = 'Testing Site';
        acc1.Vermont_County__c = 'Addison';
        acc1.Business_Start_Time__c = Time.newInstance(00, 00, 00, 00);
        acc1.Business_End_Time__c = Time.newInstance(11, 00, 00, 00);
        acc1.Event_Type__c = 'State-Sponsored';
        insert acc1;
        
        Appointment_Slot__c slot = new Appointment_Slot__c();
        slot.Account__c = acc.Id;
        slot.Date__c = Date.today();
        slot.Start_Time__c = Time.newInstance(01, 00, 00, 00);
        slot.End_Time__c = Time.newInstance(02, 00, 00, 00);
        insert slot;
        
        VTS_Event__c event = new VTS_Event__c();
        event.Account__c = acc.Id;
        event.Start_Date__c = Date.today();
        event.End_Date__c = Date.today();
        event.Start_Time__c = Time.newInstance(00, 00, 00, 00);
        event.End_Time__c = Time.newInstance(11, 00, 00, 00);
        event.Event_Type__c = 'Public';
        event.Status__c = 'Open';
        event.Location__c = acc.Id;
        insert event;
        
        Contact c = new Contact();
        c.FirstName = 'First Name';
        c.LastName = 'Last Name';
        c.Email = 'first@yopmail.com';
        c.RecordTypeId = Contact.SObjectType.getDescribe().RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        c.Birthdate = Date.newInstance(1995, 07, 23);
        insert c;
        
        Appointment__c app = new Appointment__c();
        app.Patient__c = c.Id;
        app.Appointment_Slot__c = slot.Id;
        app.Lab_Center__c = acc.Id;
        insert app;
    }
    
    @IsTest
    private static void executeBatchForAppointments() {
        Test.startTest();
        	List<Account> accounts = [SELECT Id FROM Account];
        	Map<String, String> data = new Map<String, String>();
       		data.put('oldLocationId', accounts[0].Id);
	        data.put('newLocationId', accounts[1].Id);
        	data.put('eventId', [SELECT Id FROM VTS_Event__c LIMIT 1].Id);
        	Database.executeBatch(new VT_TS_ProcessEventChangeBatch(data, false));
        Test.stopTest();
    }
    
    @IsTest
    private static void executeBatchForSlots() {
        Test.startTest();
        	List<Account> accounts = [SELECT Id FROM Account];
        	Map<String, String> data = new Map<String, String>();
       		data.put('oldLocationId', accounts[0].Id);
	        data.put('newLocationId', accounts[1].Id);
        	data.put('eventId', [SELECT Id FROM VTS_Event__c LIMIT 1].Id);
        	Database.executeBatch(new VT_TS_ProcessEventChangeBatch(data, true));
        Test.stopTest();
    }
}