import { LightningElement,track } from 'lwc';
import insertData from '@salesforce/apex/okvriusBulkDownload.insertData';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import {
    showAsyncErrorMessage
} from 'c/vtTsUtility';

export default class OklcUploadFiletoCreateAntibody extends LightningElement {
    filesUploaded = [] ;
    @track fileName;
    @track showSpinner = false;
    file;
    fileContents;
    fileReader;
    content;
    MAX_FILE_SIZE = 1500000;
    result;
    @track showFileName = false;

    handleFileChange(event){
        this.filesUploaded = event.target.files;
        this.fileName = event.target.files[0].name;
        if(this.filesUploaded.length > 0) {
            this.showFileName = true;
            this.handleUpload();
        }
    }

    handleFileDelete(){
        this.showFileName = false;
        this.filesUploaded.length = 0;
        this.fileName ='';
    }

    handleUpload() {
        if(this.filesUploaded.length > 0) {
            this.uploadHelper();
        }
        else {
            this.fileName = 'Please select file to upload!!';
        }
    }

    uploadHelper(){
        this.file = this.filesUploaded[0];
        if (this.file.size > this.MAX_FILE_SIZE) {
            return ;
        }
        this.showSpinner = true;
        this.fileReader = new FileReader();
        this.fileReader.readAsText(this.file, "UTF-8");

        this.fileReader.onload = (() => {
            this.fileContents = this.fileReader.result;
            
           this.result = this.csvToJson(this.fileContents);
        //    this.insertResults(result);
        });
        this.fileReader.onloadend = (() =>{
            this.showSpinner = false;
        });
        this.fileReader.onerror = function (evt) {
            this.showSpinner = false;
        }
    }

    csvToJson(csv){
        var arr = []; 
        
        arr =  csv.split('\n');
        // arr.pop();
        var jsonObj = [];
        var headers = arr[0].split(',');
        for(var i = 1; i < arr.length; i++) {
            var data = arr[i].split(',');
            var obj = {};
            var isBadValues = false;
            for(var j = 0; j < data.length; j++) {
                if(data[j].trim() === '' && data[j].trim() === undefined && data[j].trim() === null){
                    isBadValues = true;
                    break;
                }
                obj[headers[j].trim()] = this.stripquotes(data[j].trim());
            }
            if(!isBadValues){
                jsonObj.push(obj);
            }
        }
        var json = JSON.stringify(jsonObj);
        return json;
    }

    stripquotes(a) {
        if (a.charAt(0) === '"' && a.charAt(a.length-1) === '"') {
            return a.substr(1, a.length-2);
        }
        return a;
    }

    insertResults(jsonstr){
        this.showSpinner = true;
        insertData({strfromle : jsonstr})
        .then(result =>{
            this.showSpinner = false;
            // this.handleFileDelete();
            if(result.type === 'error'){
                var message = '';
                if(result.message.message !== undefined ){
                    message = result.message.message.substr(result.message.message.indexOf('Status'), (result.message.message.length-1));
                }else{
                    message = result.message;
                }
                this.dispatchEvent(new ShowToastEvent({
                    title: 'Error',
                    variant: 'error',
                    message: message
                }));
            }else{
                this.dispatchEvent(new ShowToastEvent({
                    title: 'File Upload',
                    variant: result.type,
                    message: ''+result.message
                }));
            }
            this.showFileName = false;
            this.result = null;
        })
        .catch(error =>{
            showAsyncErrorMessage(this,error);
            this.showSpinner = false;
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: ''+error.body.message
            }));
        })
    }


    handleSave(){
        if(this.result){
            this.insertResults(this.result);
        }else{
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please select a CSV file'
            }));
        }
    }
    
}