/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 11-30-2021
 * @last modified by  : Mohit Karani
**/
@isTest
public class AppointmentTriggerHandlerTest {
    private static final String LabSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
    private static final String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;

    @testsetup
    public static void setUpData(){
        List<Appointment__c> appointmentListToInsert = new List<Appointment__c>();
        List<Appointment_Slot__c> appoinmentSlotListToInsert = new List<Appointment_Slot__c>();
        List<Account> accountListToInsert = new List<Account>();
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, false);
        Account createAccountLabSite = TestDataFactoryAccount.createAccount('LabSite', LabSite, false);

        Account vaccinationSite = TestDataFactoryAccount.createAccount('VaccinationSite', testingSite, false);
        vaccinationSite.BillingCity = 'testCity';
        vaccinationSite.BillingCountry = 'testCountry';
        vaccinationSite.BillingState = 'testState';
        vaccinationSite.BillingStreet = 'testStreet';
        vaccinationSite.BillingPostalCode = '12345';
        vaccinationSite.Event_Process__c = 'Vaccination';
        vaccinationSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        vaccinationSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        accountListToInsert.add(createAccountTestSite);
        accountListToInsert.add(createAccountLabSite);
        accountListToInsert.add(vaccinationSite);
        insert accountListToInsert;
        Contact createContact = TestDataFactoryContact.createContact(false , 'TestLastName','TestFirstName');
        createContact.Testing_Site__c = createAccountTestSite.id;
        createContact.MailingState = 'VT';

        insert createContact;
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        Appointment_Slot__c slotTwo = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountLabSite.Id);
        appoinmentSlotListToInsert.add(createSlot);
        appoinmentSlotListToInsert.add(slotTwo);
        insert appoinmentSlotListToInsert;

        Pre_Registration_Group__c preRegGroup = new  Pre_Registration_Group__c();
        preRegGroup.Status__c = 'Activate';
        preRegGroup.Group_Type__c = 'Age-Based';
        preRegGroup.Email_Acknowledgement__c  = true;
        insert preRegGroup;
        Pre_Registration__c preReg = new Pre_Registration__c(
            Contact__c=createContact.Id,
            Is_Immune_Weak__c='No',
            Pre_Registration_Group__c  = preRegGroup.Id
        );
        insert preReg;

        Private_Access__c privateAccess = new Private_Access__c(
            Name = 'Private Access',
            Age_Based__c = 40,
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-2',
            Min__c = 7,
            Max__c=10,
            Requires_Followup_Dose__c='Yes',
            Weak_Immune_Min_Days__c=0,
            Is_Alternate_Dose_Available__c=true,
            Alternate_Doses__c='Pfizer',
            Is_Active__c  = true
        );
        insert privateAccess;
        
         Private_Access__c alternatPrivateAccess = new Private_Access__c(
            Name = 'Private Access',
            Age_Based__c = 40,
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Pfizer',
            Vaccine_Type__c = 'Vaccination-3',
            Min__c = 7,
            Max__c=10,
            Requires_Followup_Dose__c='Yes',
            Weak_Immune_Min_Days__c=0,
            Is_Active__c  = true
        );
        insert alternatPrivateAccess;
        
        Private_Access_Assignment__c alternateDosePrivateAccessAssignment = new Private_Access_Assignment__c();
        alternateDosePrivateAccessAssignment.Pre_Registration_Groups__c = preRegGroup.Id;
        alternateDosePrivateAccessAssignment.Private_Access_Groups__c  = alternatPrivateAccess.Id;
        alternateDosePrivateAccessAssignment.Active__c = true;
        insert alternateDosePrivateAccessAssignment;

        VTS_Event__c event = new VTS_Event__c( 
            Status__c = 'Open',
            Location__c = vaccinationSite.Id,
      		Private_Access__c = privateAccess.Id,
            Start_Date__c = Date.Today() ,
            End_Date__c = Date.Today() + 10                  
        );
        insert event;

        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        appointmentListToInsert.add(createAppointment);

        Appointment__c  appointmentSecond = TestDataFactoryAppointment.createAppointment(false);
        appointmentSecond.Patient__c = createContact.Id;
        appointmentSecond.Lab_Center__c = vaccinationSite.id;
        appointmentSecond.Appointment_Slot__c = slotTwo.id;
        appointmentSecond.Next_Dose_Notification_Process__c = VT_TS_Constants.SEND_TEXT;
        appointmentSecond.Next_Dose_Notification_Sent_Email__c = VT_TS_Constants.EMAIL_SENT_TEXT;
        appointmentSecond.Status__c = 'Completed';
        appointmentSecond.Event__c = event.Id;
        appointmentSecond.Appointment_Complete_Date__c=System.now();
        appointmentListToInsert.add(appointmentSecond);
        insert appointmentListToInsert;
        Antibody_Testing__c createAntiTest = TestDataFactoryAntiBody.createAppointment(true, createAccountTestSite.id, createAppointment.id, createSlot.id, createContact.id);
    }
    static testMethod void testUpdate(){
         
        Account fetchAccount = [SELECT Id FROM Account WHERE Name = 'TestSite' LIMIT 1];
        Contact fetchContact = [SELECT Id FROM Contact WHERE LastName = 'TestLastName' LIMIT 1];
        Appointment__c fetchAppointment = [Select Id FROM Appointment__c WHERE Lab_Center__c =: fetchAccount.id LIMIT 1];
        Antibody_Testing__c fetchAntibody = [SELECT Id FROM Antibody_Testing__c WHERE  Appointment__c =: fetchAppointment.id LIMIT 1];
        Appointment_Slot__c createSlotLab = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, fetchAccount.Id);
        fetchAppointment.Appointment_Slot__c = createSlotLab.Id;
        update fetchAppointment;
        Test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.stopTest();
    }
    static testMethod void testDelete(){
        
        Test.startTest();
        Appointment__c fetchAppointment = [Select Id FROM Appointment__c LIMIT 1];
		delete fetchAppointment;
        undelete fetchAppointment;
        Test.stopTest();
    }
    
    static testMethod void testUpdateStartDateStartTime(){
        Account fetchAccount = [SELECT Id FROM Account WHERE Name = 'TestSite' LIMIT 1];
        Contact fetchContact = [SELECT Id FROM Contact WHERE LastName = 'TestLastName' LIMIT 1];
        Appointment__c fetchAppointment = [Select Id FROM Appointment__c WHERE Lab_Center__c =: fetchAccount.id LIMIT 1];
        Appointment_Slot__c createSlotLab = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, fetchAccount.Id);
        fetchAppointment.Appointment_Slot__c = createSlotLab.Id;
        Test.startTest();
        update fetchAppointment;
        Test.stopTest();
    }
    static testMethod void testInsertStartDateStartTime(){
        Account fetchAccount = [SELECT Id FROM Account WHERE Name = 'TestSite' LIMIT 1];
        Contact fetchContact = [SELECT Id FROM Contact WHERE LastName = 'TestLastName' LIMIT 1];
        Appointment_Slot__c createSlotLab = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, fetchAccount.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Appointment_Slot__c = createSlotLab.Id;
        Test.startTest();
        insert createAppointment;
        Test.stopTest();
    }

    static testMethod void updateFieldsOnAppointmentForNextDoseReminderTest(){
        Appointment__c appointment = [SELECT Id, Next_Dose_Notification_Process__c ,Event__r.Private_Access__r.Vaccine_Type__c
                                     FROM Appointment__c
                                     WHERE Next_Dose_Notification_Process__c =: VT_TS_Constants.SEND_TEXT
                                     AND Status__c =: VT_TS_Constants.COMPLETED_TEXT
                                     AND Event__r.Private_Access__r.Vaccine_Type__c != NULL];
        appointment.Next_Dose_Notification_Sent_Email__c = VT_TS_Constants.RESEND_TEXT;
        Test.startTest();
        update appointment;


        Appointment__c updatedAppointment = [SELECT Id, Next_Dose_Notification_Process__c ,Event__r.Private_Access__r.Vaccine_Type__c,Next_Dose_Notification_Sent_Email__c
                                            FROM Appointment__c
                                            WHERE Id =: appointment.id ];


       System.assertEquals(VT_TS_Constants.SEND_TEXT,updatedAppointment.Next_Dose_Notification_Process__c , 'Error');
        Test.stopTest();
    }
}