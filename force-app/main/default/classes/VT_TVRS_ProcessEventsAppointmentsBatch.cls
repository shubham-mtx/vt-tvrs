/*
 * Test Class for this batch is VT_TS_ProcessEventsAppointmentsBatchTest
*/
public with sharing class VT_TVRS_ProcessEventsAppointmentsBatch implements Database.Batchable<sObject> {
    Set<Id> eventIds = new Set<Id>();
    Set<Id> updatedEventIds = new Set<Id>();
    Map<Id, VTS_Event__c> eventsMap = new Map<Id, VTS_Event__c>();
    public VT_TVRS_ProcessEventsAppointmentsBatch(Set<Id> eventIds, Set<Id> updatedEventIds, Map<Id, VTS_Event__c> eventsMap) {
        this.eventIds = eventIds;
        this.eventsMap = eventsMap;
        this.updatedEventIds = updatedEventIds;
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        Set<Id> finalEventIds = new Set<Id>(eventIds);
        finalEventIds.addAll(updatedEventIds);
        String query = 'SELECT Event__c, Event__r.Location__r.BillingAddress, Event__r.Location__r.Name, Appointment_Slot__c,  '+
                        ' Patient__c, Patient__r.Name,Patient__r.Email  FROM Appointment__c WHERE Status__c = \'Scheduled\' '+
                        ' AND Event__c != null  AND Patient__c != null  AND Event__c IN :finalEventIds WITH SECURITY_ENFORCED';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Appointment__c> appointments) {
        List<Appointment__c> appointmentsForUpdateEmail = new List<Appointment__c>();
        List<Appointment__c> appointmentsForCancellationEmail = new List<Appointment__c>();
        if(!appointments.isEmpty()) {
            for(Appointment__c app : appointments) {
                if(eventIds.contains(app.Event__c)) {
                    appointmentsForCancellationEmail.add(app);
                } else if(updatedEventIds.contains(app.Event__c)) {
                    appointmentsForUpdateEmail.add(app);
                }
            }

            if(!appointmentsForCancellationEmail.isEmpty()) {
                // sendEmailToCancelledAppointments(appointmentsForCancellationEmail);
                List<Appointment_Slot__c> slotsToBeReleased = new List<Appointment_Slot__c>();
                for (Appointment__c app : appointmentsForCancellationEmail) {
                    app.Status__c = 'Cancelled';
                    app.Is_Event_Cancel__c = true;
                    if(String.isNotBlank(app.Appointment_Slot__c)) {
                        Appointment_Slot__c slot = new Appointment_Slot__c();
                        slot.Id = app.Appointment_Slot__c;
                        slot.Appointment_Count__c = null;
                        slotsToBeReleased.add(slot);
                        app.Appointment_Slot__c = null;
                    }
                }

                if(!slotsToBeReleased.isEmpty()) {
                    update slotsToBeReleased;
                }
                VT_TS_SkipTriggerExecution.executeAppointmentTrigger = false;
                update appointmentsForCancellationEmail;
            }
        }
    }

    public void finish(Database.BatchableContext bc) {

    }
}