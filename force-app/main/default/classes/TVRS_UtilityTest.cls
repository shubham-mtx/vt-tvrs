/**
 * @description       : 
 * @author            : Vamsi Mudaliar
 * @group             : 
 * @last modified on  : 01-21-2022
 * @last modified by  : Vamsi Mudaliar
**/
@isTest
public without sharing class TVRS_UtilityTest {

    @TestSetup
    static void makeData(){
        List<Account> accList = new List<Account>();
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String defaultAccount = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Default').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.Name = 'Self Registration';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Event_Process__c ='Vaccination';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        accList.add(createAccountTestSite);
        insert accList;

        Contact con = new Contact (

            AccountId = accList[0].id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Consent__c=true,
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con;

        Contact con1 = new Contact (

            LastName = 'dimensionPortalUser',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false,
            Consent__c=true
        );
        insert con1;

        Contact con2 = new Contact (
            LastName = 'dimensionPortalUser12',
            MiddleName = 'm',
            FirstName = 'testSecond',
            Email = 'portalTestUserv1@test.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = true
        );
        insert con2;

        Pre_Registration_Group__c prGroup1 = new Pre_Registration_Group__c();
        prGroup1.Status__c = 'Activate';
        prGroup1.Group_Type__c = 'Chronic';
        prGroup1.Email_Acknowledgement__c = true;
        prGroup1.Verification_Delay__c = 72;
        prGroup1.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        insert prGroup1;
        
        Pre_Registration__c pr = new Pre_Registration__c(
            Contact__c = con.Id,
            Chronic_Condition__c='No',
            Pre_Registration_Group__c=prGroup1.Id,
            Is_Immune_Weak__c='Yes',
            BIPOC__c='Yes'
        );
        insert pr;
        
        
        Pre_Registration__c pr1 = new Pre_Registration__c(
            Contact__c = con2.Id,
            Chronic_Condition__c='No',
            Pre_Registration_Group__c=prGroup1.Id,
            Is_Immune_Weak__c='No',
            BIPOC__c='Yes'
        );
        insert pr1;
        
        
        Private_Access__c privateAccess1 = new Private_Access__c(
            Name = 'Private Access',
            Age_Based__c = 40,
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-1',
            Is_Active__c=true,
            Requires_Followup_Dose__c='Yes',
            Min__c=0,
            Max__c=10,
            Weak_Immune_Min_Days__c=0,
            Weak_Immune_Max_Days__c=10
        );
        insert privateAccess1;
        
        Private_Access__c privateAccess2 = new Private_Access__c(
            Name = 'Private Access2',
            Age_Based__c = 40,
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-2',
            Requires_Followup_Dose__c='Yes',
            Is_Alternate_Dose_Available__c=true,
            Alternate_Doses__c='Moderna;Pfizer',
            Is_Active__c=true,
            Min__c=0,
            Max__c=10,
            Weak_Immune_Min_Days__c=0,
            Weak_Immune_Max_Days__c=10
        );
        insert privateAccess2;
        
        Private_Access__c privateAccess3 = new Private_Access__c(
            Name = 'Private Access3',
            Age_Based__c = 40,
            Event_Process__c = 'Vaccination',
            Vaccine_Class_Name__c = 'Moderna',
            Vaccine_Type__c = 'Vaccination-3',
            Requires_Followup_Dose__c='Yes',
            Is_Active__c=true,
            Min__c=0,
            Max__c=10,
            Weak_Immune_Min_Days__c=0,
            Weak_Immune_Max_Days__c=10
        );
        insert privateAccess3;


        VTS_Event__c event1 = new VTS_Event__c( 
            Status__c = 'Open',
            Location__c = createAccountTestSite.Id,
      		Private_Access__c = privateAccess1.Id ,
              Start_Date__c = System.today()                   
        );
        insert event1;
        
        VTS_Event__c event2 = new VTS_Event__c( 
            Status__c = 'Open',
            Location__c = createAccountTestSite.Id,
      		Private_Access__c = privateAccess2.Id,
            Start_Date__c = System.today()                        
        );
        insert event2;


        Appointment__c app1 = new Appointment__c(
        	Patient__c = con.Id,
            Status__c = 'Completed',
            Event__c = event1.Id,
            Lab_Center__c = createAccountTestSite.Id
        );
        insert app1;
    }


    @IsTest
    static void testRequiredFieldsCheck_Case0(){
        Contact con1 = [SELECT Id,Consent__c,LastName FROM Contact WHERE LastName='dimensionPortalUser12' LIMIT 1];
        Test.startTest();  
            try{
                TVRS_Utility.requiredFieldsCheck(con1,'LastName');
             }
            catch(Exception e) {
                System.assert(true,'ERROR');
            }
        Test.stopTest();
        
    }

    @IsTest
    static void testRequiredFieldsCheck_Case1(){
        Contact con = [SELECT Id FROM Contact WHERE LastName='dimensionPortalUser' LIMIT 1];
        Contact con1 = [SELECT Id,Consent__c,LastName FROM Contact WHERE LastName='dimensionPortalUser12' LIMIT 1];
        Test.startTest();      
            try{
                TVRS_Utility.requiredFieldsCheck(con,'LastName');
             }
            catch(Exception e) {
                System.assert(true,'ERROR');
            }
        Test.stopTest();
        
    }

    @IsTest
    static void testRequiredFieldsCheck_Case2(){
        Contact con = [SELECT Id,Consent__c,LastName,AccountId,(SELECT Id,Is_Immune_Weak__c FROM Pre_Registrations__r) FROM Contact WHERE LastName='portalTestUserv1' LIMIT 1];
        Test.startTest();        
            try{
                TVRS_Utility.requiredFieldsCheck(con, 'LastName,AccountId');
            }
            catch(Exception e) {
                System.assert(true,'ERROR');
            }
        Test.stopTest(); 
    }


    @IsTest
    static void testRequiredFieldsCheck_Case3(){
        Contact con = [SELECT Id,Consent__c,LastName,AccountId,(SELECT Id,Is_Immune_Weak__c FROM Pre_Registrations__r) FROM Contact WHERE LastName='dimensionPortalUser' LIMIT 1];
        Test.startTest();
            try{
                TVRS_Utility.requiredFieldsCheck(con, 'LastName,AccountId');
            }
            catch(Exception e) {
                System.assert(true,'ERROR');
            }
        Test.stopTest();        
    }
    
    
    @IsTest
    static void testGetMinimumNextDoseDate(){
        Pre_Registration__c pre = [SELECT Id,Is_Immune_Weak__c FROM Pre_Registration__c WHERE Is_Immune_Weak__c='Yes' LIMIT 1];
        Appointment__c app = [SELECT Id,Event__r.Private_Access__r.Vaccine_Class_Name__c,Event__r.Private_Access__r.Vaccine_Type__c,Event__r.Private_Access__r.Min__c,Appointment_Date__c FROM Appointment__c LIMIT 1];
        Test.startTest();
        // System.runAs(contextuser){
            TVRS_Utility.getMinimumNextDoseDate(pre,app);
        // }
        Test.stopTest();
    }

}