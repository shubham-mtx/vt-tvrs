/**
 * @description       : 
 * @author            : Vamsi Mudaliar
 * @group             : 
 * @last modified on  : 01-04-2022
 * @last modified by  : Mohit Karani
**/
public without sharing class okvriusBulkDownload {
    
        @AuraEnabled
        public static List<AppointmentWrapper> getAppointmentBulk(String labId ,Date toDate ,Date fromDate){ 

            try{
                if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                    List<AppointmentWrapper> wrapper = new List<AppointmentWrapper>(); 
                    Date FromdateChange = Date.valueOf(fromDate);
                    Date TodateChnage = Date.valueOf(toDate);
                    system.debug('FromdateChange' + FromdateChange);
                    for(Appointment__c app : [SELECT Id,Name,Date_of_Birth__c,Patient__r.MobilePhone,Patient__r.email,No_Show__c,Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.Primary_Language__c,
                                                Patient__r.MiddleName,Patient__r.What_type_of_record_do_you_have_question__c,Patient__r.Which_specialist_do_you_need__c,
                                                Patient__r.Account.Name,status__c,Appointment_Start_Time__c,Formatted_Appointment_Start_Date__c,Reason_for_cancellation__c,
                                                Patient__r.Need_Spanish_Interpreter__c, createddate,Formatted_Appointment_Start_Time__c,Formatted_appointment_Complete_Date__c,
                                                Appointment_Schedule_Generated_Date__c,Appointment_Complete_Date__c, Primary_Language__c, Event__r.Private_Access__r.Vaccine_Class_Name__c, 
                                                Event__r.Private_Access__r.Name, Event__r.Description__c,contact_language__c 
                                                FROM Appointment__c 
                                                WHERE Lab_Center__c =:labId AND Status__c != 'Draft' 
                                                AND Appointment_Date__c  >=: fromDate AND Appointment_Date__c  <=: toDate WITH SECURITY_ENFORCED  ORDER BY Name 
                                            ]){
                                                    wrapper.add(new AppointmentWrapper(app));
                                                }
                    if(wrapper.size()>0){
                        return wrapper;
                    }else{
                        return null;
                    }
                }else{
                    throw new VT_Exception('You are not Authorized to perform this action');
                }
            }catch(Exception ex){
                throw new AuraHandledException(ex.getMessage());
            }
            
        }
        
        public class AppointmentWrapper{
            @AuraEnabled public string appointmentId;
            @AuraEnabled public string patientId;
            @AuraEnabled public string firstName;
            @AuraEnabled public string lastName;
            @AuraEnabled public string email;
            @AuraEnabled public string mobileNumber;
            @AuraEnabled public string dateOfBirth; 
            @AuraEnabled public String appointmentStatus;
            @AuraEnabled public String appointmentDate;
            @AuraEnabled public string appointmentTime;
            @AuraEnabled public string noShow;
            @AuraEnabled public string preferredLanguage;
            @AuraEnabled public String vaccineClass;
            @AuraEnabled public String privateAccess;
            @AuraEnabled public String event;
           
            public AppointmentWrapper(Appointment__c app){
                this.appointmentId = app.Id != null ? app.Id : '';
                this.patientId = app.Patient__r.Patient_Id__c != null ? app.Patient__r.Patient_Id__c : '';
                this.firstName = app.Patient__r.FirstName != null ? app.Patient__r.FirstName : '';
                this.lastname = app.Patient__r.LastName != null ? app.Patient__r.LastName : '';
                this.email =  app.Patient__r.email != null ? app.Patient__r.email : '';
                this.mobileNumber = app.Patient__r.MobilePhone != null ? app.Patient__r.MobilePhone : '' ;
                this.dateOfBirth = String.valueof(app.Date_of_Birth__c) != null ? String.valueof(app.Date_of_Birth__c) : '';
                this.appointmentStatus = app.status__c != null ? app.status__c : '';
                this.appointmentDate = app.Formatted_Appointment_Start_Date__c != null ? app.Formatted_Appointment_Start_Date__c : '';
                this.appointmentTime = String.valueof(app.Formatted_Appointment_Start_Time__c) != null ? String.valueof(app.Formatted_Appointment_Start_Time__c) : '';
                this.noShow = (String.valueof(app.No_Show__c) != null && String.valueof(app.No_Show__c) == 'true') ? 'Yes' : 'No';
                this.preferredLanguage = app.Patient__r.Primary_Language__c;
                this.vaccineClass = (String.isNotBlank(app.Event__c) && String.isNotBlank(app.Event__r.Private_Access__c)) ? app.Event__r.Private_Access__r.Vaccine_Class_Name__c : '';
                this.privateAccess = (String.isNotBlank(app.Event__c) && String.isNotBlank(app.Event__r.Private_Access__c)) ? app.Event__r.Private_Access__r.Name : '';
                this.event = (String.isNotBlank(app.Event__c)) ? app.Event__r.Description__c : '';
            }
        }
        
     @AuraEnabled
    public static Map<String,Object> insertData(String strfromle){ 

        try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                Map<String, Object> result = new Map<String, Object>();
                String returnresponse ='';
                Boolean wrongEntry = false;
                List<String> appointmentList = new List<String>();
                List<Appointment__c> appointmentListtoInsert = new List<Appointment__c>();
                List<fieldWrapper> datalist = (List<fieldWrapper>)JSON.deserialize(strfromle, List<fieldWrapper>.class);
                List<Appointment__c> resultsForApps = new List<Appointment__c>();
                List<User> userList = [SELECT Name, FirstName, LastName, CompanyName, Email, Phone, smallPhotoUrl, ContactId, Contact.AccountId FROM User WHERE Id = :UserInfo.getuserid() WITH SECURITY_ENFORCED];
                AccountContactRelation accountContactRecord = OKPCHeaderController.getTestingSiteFromContact(userList[0].ContactId);  

                for(fieldWrapper wrapper: datalist){
                    if(!appointmentList.contains(wrapper.appointmentId)){
                        if(wrapper.noShow == 'Yes' && wrapper.appointmentStatus == 'Completed'){
                            wrongEntry = true;
                            break;
                        }else{
                            Appointment__c app = new  Appointment__c();
                            if(wrapper.appointmentId != null && wrapper.appointmentId != ''){
                                
                                app.Id = (Id)wrapper.appointmentId.replaceAll('"','');
                                
                                if(wrapper.noShow != null || !String.isBlank(wrapper.noShow)){
                                    app.No_Show__c = wrapper.noShow.toLowerCase() == 'yes' ? true : false;
                                }else{
                                    app.No_Show__c = false;
                                }
                                if(wrapper.appointmentStatus != null || !String.isBlank(wrapper.appointmentStatus)){
                                    app.Status__c = wrapper.appointmentStatus;
                                    System.debug('*******'+wrapper.appointmentStatus);
                                }

                                if(String.isNotBlank(wrapper.preferredLanguage)){
                                    app.Primary_Language__c = wrapper.preferredLanguage;
                                    System.debug('*******'+wrapper.appointmentStatus);
                                }

                                if(wrapper.appointmentStatus == 'Completed') {
                                    resultsForApps.add(app);
                                }
                                
                                appointmentListtoInsert.add(app); 
                                appointmentList.add(wrapper.appointmentId);
                            } 
                        }
                        
                    }    
                }
                if(!wrongEntry){
                    if(appointmentListtoInsert.size() > 0){
                        system.debug('acc List: '+ appointmentListtoInsert);
                        try {
                            //security check
                            appointmentListtoInsert = (list<Appointment__c>)VT_SecurityLibrary.getAccessibleData('Appointment__c', appointmentListtoInsert, 'update');
                            update appointmentListtoInsert;
                            if (!resultsForApps.isEmpty()) {
                                if(accountContactRecord.Account.Event_Process__c == 'Testing Site'){
                                    InsertContactQueueable.createTestingRecordOnAppointmentComplete(resultsForApps);
                                }
                            }
                            if(appointmentListtoInsert.size() == datalist.size()){
                                result.put('message',string.valueOf(appointmentListtoInsert.size())+' records updated successfully');
                                result.put('type','success');
                            }else{
                                result.put('message',string.valueOf(appointmentListtoInsert.size())+' records updated successfully. '+string.valueOf(datalist.size()-appointmentListtoInsert.size())+' records contains bad values or duplicate results.');
                                result.put('type','warning');
                            }
                            
                        }
                        catch(Exception ex){
                            result.put('message', ex);
                            result.put('type','error');
                        }
                    }else{
                        result.put('message', '0 records updated. Please check the CSV file for bad values.');
                        result.put('type','error');
                    }
                }else{
                    result.put('message', 'If Appointment Status is Completed then NoShow should be No.');
                    result.put('type','error');
                }
                
                return result;
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
        
    }

    
    public class fieldWrapper{        
        public string appointmentId;
        public string noShow;
        public string appointmentStatus;
        public string preferredLanguage;
    }
    
}