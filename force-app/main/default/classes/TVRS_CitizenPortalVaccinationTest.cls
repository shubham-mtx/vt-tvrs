/**
 * @description       : Test class for TVRS_CitizenPortalVaccinationController
 * @author            : Balram Dhawan
 * @group             : 
 * @last modified on  : 01-21-2022
 * @last modified by  : Vamsi Mudaliar
**/
@IsTest
private without sharing class TVRS_CitizenPortalVaccinationTest {
    @TestSetup
    static void makeData(){

        List<Private_Access__c> privateAccesses = new List<Private_Access__c>();
        Private_Access__c privateAccess1 = new Private_Access__c();
        privateAccess1.Is_Active__c = true;
        privateAccess1.Event_Process__c = 'Vaccination';
        privateAccess1.Vaccine_Type__c = 'Vaccination-1';
        privateAccess1.Age_Based__c = 0;
        privateAccess1.Requires_Followup_Dose__c = 'Yes';
        privateAccess1.Vaccine_Class_Name__c = 'Pfizer';
        privateAccess1.Min__c = 0;
        privateAccess1.Max__c = 0;
        privateAccess1.Weak_Immune_Min_Days__c = 0;
        privateAccess1.Weak_Immune_Max_Days__c = 0;
        privateAccess1.Is_Alternate_Dose_Available__c = true;
        privateAccess1.Alternate_Doses__c = 'Moderna';
        privateAccesses.add(privateAccess1);

        Private_Access__c privateAccess2 = new Private_Access__c();
        privateAccess2.Is_Active__c = true;
        privateAccess2.Event_Process__c = 'Vaccination';
        privateAccess2.Vaccine_Type__c = 'Vaccination-2';
        privateAccess2.Age_Based__c = 0;
        privateAccess2.Requires_Followup_Dose__c = 'Yes';
        privateAccess2.Vaccine_Class_Name__c = 'Pfizer';
        privateAccess2.Min__c = 0;
        privateAccess2.Max__c = 0;
        privateAccess2.Weak_Immune_Min_Days__c = 0;
        privateAccess2.Weak_Immune_Max_Days__c = 0;
        privateAccess2.Is_Alternate_Dose_Available__c = true;
        privateAccess2.Alternate_Doses__c = 'Moderna';
        privateAccesses.add(privateAccess2);

        Private_Access__c privateAccess3 = new Private_Access__c();
        privateAccess3.Is_Active__c = true;
        privateAccess3.Event_Process__c = 'Vaccination';
        privateAccess3.Vaccine_Type__c = 'Vaccination-3';
        privateAccess3.Age_Based__c = 0;
        privateAccess3.Requires_Followup_Dose__c = 'No';
        privateAccess3.Vaccine_Class_Name__c = 'Pfizer';
        privateAccess3.Min__c = 0;
        privateAccess3.Max__c = 0;
        privateAccess3.Weak_Immune_Min_Days__c = 0;
        privateAccess3.Weak_Immune_Max_Days__c = 0;
        privateAccess3.Is_Alternate_Dose_Available__c = true;
        privateAccess3.Alternate_Doses__c = 'Moderna';
        privateAccesses.add(privateAccess3);
        
        insert privateAccesses;

        List<Pre_Registration_Group__c> groups = new List<Pre_Registration_Group__c>();
        Pre_Registration_Group__c preRegGroup = new Pre_Registration_Group__c();
        preRegGroup.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        preRegGroup.Name = '0-150 Age Group';
        preRegGroup.Status__c = 'Activate';
        preRegGroup.Group_Description__c = 'Test Description';
        preRegGroup.Verification_Delay__c = 0;
        preRegGroup.Group_Type__c = 'Age-Based';
        preRegGroup.Beginning_DOB__c = System.today().addYears(-150);
        preRegGroup.Ending_DOB__c = System.today();
        preRegGroup.From_Age__c = 0;
        preRegGroup.Till_Age__c = 150;
        preRegGroup.Email_Acknowledgement__c = true;
        groups.add(preRegGroup);

        Pre_Registration_Group__c preRegGroup1 = new Pre_Registration_Group__c();
        preRegGroup1.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        preRegGroup1.Name = '0-150 Age Group';
        preRegGroup1.Status__c = 'Activate';
        preRegGroup1.Group_Description__c = 'Test Description';
        preRegGroup1.Verification_Delay__c = 0;
        preRegGroup1.Group_Type__c = 'Risk Group';
        preRegGroup1.Beginning_DOB__c = System.today().addYears(-150);
        preRegGroup1.Ending_DOB__c = System.today();
        preRegGroup1.From_Age__c = 0;
        preRegGroup1.Till_Age__c = 150;
        preRegGroup1.Email_Acknowledgement__c = true;
        groups.add(preRegGroup1);

        Pre_Registration_Group__c preRegGroup2 = new Pre_Registration_Group__c();
        preRegGroup2.RecordTypeId = Schema.SObjectType.Pre_Registration_Group__c.getRecordTypeInfosByDeveloperName().get('Pre_Registration_Group').getRecordTypeId();
        preRegGroup2.Name = '0-150 Age Group';
        preRegGroup2.Status__c = 'Activate';
        preRegGroup2.Group_Description__c = 'Test Description';
        preRegGroup2.Verification_Delay__c = 0;
        preRegGroup2.Group_Type__c = 'Chronic';
        preRegGroup2.Beginning_DOB__c = System.today().addYears(-150);
        preRegGroup2.Ending_DOB__c = System.today();
        preRegGroup2.From_Age__c = 0;
        preRegGroup2.Till_Age__c = 150;
        preRegGroup2.Email_Acknowledgement__c = true;
        groups.add(preRegGroup2);

        insert groups;

        List<Private_Access_Assignment__c> assignments = new List<Private_Access_Assignment__c>();
        Private_Access_Assignment__c assignment1 = new Private_Access_Assignment__c();
        assignment1.Pre_Registration_Groups__c = groups[0].Id;
        assignment1.Private_Access_Groups__c = privateAccesses[0].Id;
        assignments.add(assignment1);

        Private_Access_Assignment__c assignment2 = new Private_Access_Assignment__c();
        assignment2.Pre_Registration_Groups__c = groups[1].Id;
        assignment2.Private_Access_Groups__c = privateAccesses[1].Id;
        assignments.add(assignment2);

        Private_Access_Assignment__c assignment3 = new Private_Access_Assignment__c();
        assignment3.Pre_Registration_Groups__c = groups[2].Id;
        assignment3.Private_Access_Groups__c = privateAccesses[2].Id;
        assignments.add(assignment3);
        
        insert assignments;

        List<Account> accounts = new List<Account>();
        Account citizenAccount = new Account();
        citizenAccount.Name = 'Self Registration';
        accounts.add(citizenAccount);

        Account hospital = new Account();
        hospital.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Test_Center').getRecordTypeId();
        hospital.Name = 'Vaccination Hospital';
        hospital.Description = 'Hospital Description';
        hospital.Business_Start_Time__c = Time.newInstance(0, 0, 0, 0);
        hospital.Business_End_Time__c = Time.newInstance(23, 45, 0, 0);
        hospital.Event_Process__c = 'Vaccination';
        hospital.Vermont_County__c = 'Addison';
        hospital.Event_Type__c = 'State-Sponsored';
        hospital.Number_of_Weeks_Available__c = '1';
        hospital.Directions__c = 'Test Direction';
        accounts.add(hospital);
        
        insert accounts;
        citizenAccount = accounts[0];
        hospital = accounts[1];

        Contact patient = new Contact();
        patient.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        patient.FirstName = 'First Name';
        patient.LastName = 'Last Name';
        patient.Email = 'first.last@yopmail.com';
        patient.Gender__c = 'Male';
        patient.AccountId = citizenAccount.Id;
        patient.MobilePhone = '8591763419';
        patient.MailingCity = 'Toronto';
        patient.MailingState = 'VT';
        patient.MailingStreet = 'Test Street';
        patient.MailingPostalCode = '34874';
        patient.Phone_Type__c = 'Mobile';
        patient.How_do_you_want_to_recieve_a_copy_of_you__c = 'E-Mail';
        patient.Birthdate = System.today().addYears(-25);
        patient.Consent__c = true;
        patient.Primary_Language__c = 'English';
        patient.Race__c = 'Asian';
        patient.Ethnicity__c = 'Other';

        insert patient;

        Pre_Registration__c preReg = new Pre_Registration__c();
        preReg.Status__c = 'Assigned';
        preReg.Are_you_eligible_for_Vaccine_Risk_Group__c = 'Yes';
        preReg.Affiliated_to_any_other_Risk_Group__c = 'Yes';
        preReg.Chronic_Condition__c = 'Yes';
        preReg.Pre_Registration_Group__c =groups[0].Id;
        preReg.Early_Group_2_Access_Group__c = groups[1].Id;
        preReg.Early_Group1_Health__c = groups[2].Id;
        preReg.Contact__c = patient.Id;
        preReg.Is_Immune_Weak__c='Yes';
        preReg.BIPOC__c='No';
        insert preReg;

        List<VTS_Event__c> events = new List<VTS_Event__c>();
        VTS_Event__c event = new VTS_Event__c();
        event.Start_Date__c = System.today();
        event.End_Date__c = System.today();
        event.Event_Type__c = 'Private';
        event.Start_Time__c = Time.newInstance(0, 0, 0, 0);
        event.End_Time__c = Time.newInstance(23, 45, 0, 0);
        event.Status__c = 'Open';
        event.Private_Access__c = privateAccesses[0].Id;
        event.Description__c = 'Test event description';
        events.add(event);
        
        VTS_Event__c event1 = new VTS_Event__c();
        event1.Start_Date__c = System.today();
        event1.End_Date__c = System.today();
        event1.Event_Type__c = 'Private';
        event1.Start_Time__c = Time.newInstance(0, 0, 0, 0);
        event1.End_Time__c = Time.newInstance(23, 45, 0, 0);
        event1.Status__c = 'Open';
        event1.Private_Access__c = privateAccesses[1].Id;
        event1.Description__c = 'Test event1 description';
        events.add(event1);

        VTS_Event__c event2 = new VTS_Event__c();
        event2.Start_Date__c = System.today();
        event2.End_Date__c = System.today();
        event2.Event_Type__c = 'Private';
        event2.Start_Time__c = Time.newInstance(0, 0, 0, 0);
        event2.End_Time__c = Time.newInstance(23, 45, 0, 0);
        event2.Status__c = 'Open';
        event2.Private_Access__c = privateAccesses[2].Id;
        event2.Description__c = 'Test event description';
        events.add(event2);

        VTS_Event__c event3 = new VTS_Event__c();
        event3.Start_Date__c = System.today();
        event3.End_Date__c = System.today();
        event3.Event_Type__c = 'Private';
        event3.Start_Time__c = Time.newInstance(0, 0, 0, 0);
        event3.End_Time__c = Time.newInstance(23, 45, 0, 0);
        event3.Status__c = 'Self Attest';
        event3.Private_Access__c = privateAccesses[0].Id;
        event3.Description__c = 'Test event description';
        events.add(event3);

        VTS_Event__c event4 = new VTS_Event__c();
        event4.Start_Date__c = System.today();
        event4.End_Date__c = System.today();
        event4.Event_Type__c = 'Private';
        event4.Start_Time__c = Time.newInstance(0, 0, 0, 0);
        event4.End_Time__c = Time.newInstance(23, 45, 0, 0);
        event4.Status__c = 'Self Attest';
        event4.Private_Access__c = privateAccesses[1].Id;
        event4.Description__c = 'Test event description';
        events.add(event4);

        VTS_Event__c event5 = new VTS_Event__c();
        event5.Start_Date__c = System.today();
        event5.End_Date__c = System.today();
        event5.Event_Type__c = 'Private';
        event5.Start_Time__c = Time.newInstance(0, 0, 0, 0);
        event5.End_Time__c = Time.newInstance(23, 45, 0, 0);
        event5.Status__c = 'Self Attest';
        event5.Private_Access__c = privateAccesses[2].Id;
        event5.Description__c = 'Test event description';
        events.add(event5);

        insert events;

        String profileId = [SELECT Id FROM Profile WHERE Name = 'Covid Citizen User' LIMIT 1]?.Id;
        User newUser = new User(
            profileId = profileId,
            username = 'first.last@yopmail.com',
            email = 'first.last@yopmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = patient.Id
        );
        insert newUser;
        
        List<Appointment__c> apps = new List<Appointment__c>();
        Appointment__c appointment1 = new Appointment__c(
            Patient__c = patient.Id,
            Status__c = 'Completed',
            Event__c = events[0].Id
        );
        apps.add(appointment1);
        
        Appointment__c appointment2 = new Appointment__c(
            Patient__c = patient.Id,
            Status__c = 'Scheduled',
            Event__c = events[1].Id
        );
        apps.add(appointment2);
        //insert apps;
    }
    
    @IsTest
    static void getSelfAndChildPatients(){
        User contextUser = [SELECT Id, ContactId FROM User WHERE Contact.Email = 'first.last@yopmail.com' LIMIT 1];
        Map<String, Object> response = new Map<String, Object>();
        Test.startTest();
            System.runAs(contextuser){
                response.putAll(TVRS_CitizenPortalVaccinationController.getSelfAndChildPatients());
            }
        Test.stopTest();
        List<Contact> patients = (List<Contact>) response.get('patientsData');
        system.assertEquals(1, patients.size(), 'Patient and its child retrieved');
    }

    @IsTest
    static void checkUserEligibitlityConsentMissing(){
        User contextUser = [SELECT Id, ContactId FROM User WHERE Contact.Email = 'first.last@yopmail.com' LIMIT 1];
        Map<String, Object> response = new Map<String, Object>();
        Contact patient = new Contact();
        patient.Id = contextUser.ContactId;
        patient.Consent__c = false;
        update patient;
        Test.startTest();
            System.runAs(contextuser){
                try {
                    Map<String, Object> request = new Map<String, Object>();
                    request.put('patientId', contextUser.ContactId);
                    response.putAll(TVRS_CitizenPortalVaccinationController.checkUserEligibitlity(request));   
                } catch(Exception e) {
					// Exception catched user haven\'t checked consent
                }
            }
        Test.stopTest();
    }
    
    @IsTest
    static void requiredFieldsMissing(){
        User contextUser = [SELECT Id, ContactId FROM User WHERE Contact.Email = 'first.last@yopmail.com' LIMIT 1];
        Map<String, Object> response = new Map<String, Object>();
        Contact patient = new Contact();
        patient.Id = contextUser.ContactId;
        patient.Race__c = null;
        update patient;
        Test.startTest();
            System.runAs(contextuser){
                try {
                    Map<String, Object> request = new Map<String, Object>();
                    request.put('patientId', contextUser.ContactId);
                    response.putAll(TVRS_CitizenPortalVaccinationController.checkUserEligibitlity(request));   
                } catch(Exception e) {
                    // Exception catched user haven\'t checked consent
                }
            }
        Test.stopTest();
    }

    @IsTest
    static void checkUserEligibitlity(){
        User contextUser = [SELECT Id, ContactId FROM User WHERE Contact.Email = 'first.last@yopmail.com' LIMIT 1];
        Map<String, Object> response = new Map<String, Object>();
        Test.startTest();
            System.runAs(contextuser){
                Map<String, Object> request = new Map<String, Object>();
                request.put('patientId', contextUser.ContactId);
                response.putAll(TVRS_CitizenPortalVaccinationController.checkUserEligibitlity(request));
            }
        Test.stopTest();
    }
    
    @IsTest
    static void getPendingDoses(){
        User contextUser = [SELECT Id, ContactId FROM User WHERE Contact.Email = 'first.last@yopmail.com' LIMIT 1];
        Map<String, Object> response = new Map<String, Object>();
        Test.startTest();
            System.runAs(contextuser){
                Map<String, Object> request = new Map<String, Object>();
                request.put('patientId', contextUser.ContactId);
                response.putAll(TVRS_CitizenPortalVaccinationController.checkUserEligibitlity(request));
            }
        Test.stopTest();
        Map<String, Object> pendingDoses = (Map<String, Object>) response.get('pendingDoses');
        system.assertEquals(3, pendingDoses.keySet().size(), 'User haven\'t vaccinated for any dose yet');
    }

    @IsTest
    static void checkExistingMissingDoses(){
        User contextUser = [SELECT Id, ContactId FROM User WHERE Contact.Email = 'first.last@yopmail.com' LIMIT 1];
        Map<String, Object> response = new Map<String, Object>();
        Test.startTest();
            System.runAs(contextuser){
                Map<String, Object> request = new Map<String, Object>();
                request.put('patientId', contextUser.ContactId);
                request.put('userRequestsForVaccineType', 'Vaccination-2');
                response.putAll(TVRS_CitizenPortalVaccinationController.checkExistingDoses(request));
            }
        Test.stopTest();
        List<String> missingDoses = (List<String>) response.get('missingExistingDoses');
        system.assertEquals('Vaccination-1', missingDoses.get(0), 'First Dose is required in order to get Second Dose');
    }
    
    @IsTest
    static void saveAppointment(){
        User contextUser = [SELECT Id, ContactId FROM User WHERE Contact.Email = 'first.last@yopmail.com' LIMIT 1];
        
        Map<String, Object> response = new Map<String, Object>();
        Test.startTest();
            System.runAs(contextuser){
                Map<String, Object> request = new Map<String, Object>();
                request.put('patientId', contextUser.ContactId);
                request.put('vaccineType', 'Vaccination-1');
                request.put('vaccineClass', 'Pfizer');
                request.put('vaccineDate', System.today());
                request.put('userRequestsForVaccineType', 'Vaccination-3');
                response.putAll(TVRS_CitizenPortalVaccinationController.saveAppointment(request));
                request.put('vaccineType', 'Vaccination-2');
                response.putAll(TVRS_CitizenPortalVaccinationController.saveAppointment(request));
                try{
                     request.put('patientId', null);
                    response.putAll(TVRS_CitizenPortalVaccinationController.saveAppointment(request));
                }
                catch(exception e){
                    
                }
                try{
                     request.put('patientId', contextUser.ContactId);
                     request.put('vaccineClass', null);
                   response.putAll(TVRS_CitizenPortalVaccinationController.saveAppointment(request));
                }
                catch(exception e){
                    
                }
                try{
                     request.put('vaccineClass', 'Pfizer');
                     request.put('vaccineDate', date.today()+2);
                    response.putAll(TVRS_CitizenPortalVaccinationController.saveAppointment(request));
                }
                catch(exception e){
                    
                }
               
                List<Appointment__c>  appList= [select id,Status__c, event__r.private_access__r.Vaccine_Type__c from Appointment__c where patient__c =: contextUser.ContactId];
        system.debug('appList--'+appList.size());
        system.debug('appList--'+appList);
            }
        Test.stopTest();
        system.assertEquals(true, response.get('createdAppointment') != null, 'Back-dated apopointment have been creaated successfully');
    }
}