/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 01-18-2022
 * @last modified by  : Mohit Karani
**/
public without sharing class OKTC_ScheduleAppointment {
    @AuraEnabled
    public static Map<String,Object> getAccountSlots(String accountId){  
        try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)) {
                Integer daysCount = 7; 
                List<Account> accList = [SELECT id,Name,Number_of_Weeks_Available__c FROM Account WHERE id =: accountId WITH SECURITY_ENFORCED];
                if(accList.size() > 0 && accList[0].Number_of_Weeks_Available__c != null){
                    daysCount = Integer.valueof(accList[0].Number_of_Weeks_Available__c) * 7;
                }
                Map<String,Object> result = new Map<String,Object>();
                List<Appointment_Slot__c> appointmentSlot = new List<Appointment_Slot__c>();
                
                Set<Id> appointIds = new Set<Id>();
                Map<String,List<SObject>> slotMap = new Map<String,List<SObject>>();
                Map<Date,Map<String,List<SObject>>> dateMap = new Map<Date,Map<String,List<SObject>>>();
                Map<Date,Map<String,List<SObject>>> dateMapBooked = new Map<Date,Map<String,List<SObject>>>();
                
                for(Integer counter = 0 ; counter <= daysCount ; counter++ ) {
                    dateMapBooked.put(Date.today().addDays(counter),new Map<String,List<SObject>>());
                    dateMap.put(Date.today().addDays(counter),new Map<String,List<SObject>>());
                }  
                Date endDate = date.today().addDays(daysCount);
                appointmentSlot = [SELECT Id,Name,Account__c, Date__c,
                                   Start_Time__c, End_Time__c,
                                   Unscheduled__c, Appointment_Count__c 
                                   FROM Appointment_Slot__c
                                   WHERE Account__c =: accountId
                                   AND Date__c <=: endDate AND
                                   Date__c >= TODAY
                                   AND Start_Time__c != null WITH SECURITY_ENFORCED
                                   ORDER BY Date__c ASC,Start_Time__c ASC];
                
                for(Appointment_Slot__c slot : appointmentSlot){
                    DateTime dateTimeVar = DateTime.newInstance(Date.today(),slot.Start_Time__c);
                    if(dateMap.containsKey(slot.Date__c)) {
                        Map<String,List<SObject>> slotAndAppointMentMap = dateMap.get(slot.Date__c);
                        if(!slotAndAppointMentMap.containsKey(dateTimeVar.format('hh:mm aa'))) {
                            slotAndAppointMentMap.put(dateTimeVar.format('hh:mm aa'),new List<SObject>());
                        }
                        if(slot.Appointment_Count__c == 0 || slot.Appointment_Count__c == null)
                            slotAndAppointMentMap.get(dateTimeVar.format('hh:mm aa')).add(slot);
                        dateMap.put(slot.Date__c,slotAndAppointMentMap);
                    }
                    if(dateMapBooked.containsKey(slot.Date__c)) {
                        Map<String,List<SObject>> bookedslotAndAppointMentMap = dateMapBooked.get(slot.Date__c);
                        if(!bookedslotAndAppointMentMap.containsKey(dateTimeVar.format('hh:mm aa'))) {
                            bookedslotAndAppointMentMap.put(dateTimeVar.format('hh:mm aa'),new List<SObject>());
                        }
                        if(slot.Appointment_Count__c > 0)
                            bookedslotAndAppointMentMap.get(dateTimeVar.format('hh:mm aa')).add(slot);
                        dateMapBooked.put(slot.Date__c,bookedslotAndAppointMentMap);
                    }
                }
                
                List<String> dateHeaderList = new List<String>();
                List<String> weekHeaderList = new List<String>();
                
                for(Integer count = 0; count <= daysCount;count++ ) {
                    DateTime appointmentDate = DateTime.now().addDays(count);
                    dateHeaderList.add(appointmentDate.format('MM/dd'));
                    weekHeaderList.add(appointmentDate.format('E').toUpperCase());
                }
                result.put('weekHeaderList',weekHeaderList);
                result.put('dateHeaderList',dateHeaderList);
                result.put('records',dateMap);
                result.put('bookedRecords',dateMapBooked);
                return result; 
            } else {
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        } catch(Exception e) {
            throw new VT_Exception('You are not Authorized to perform this action');
        }
    }

    /**
     * Mock Response:
     * [{"recid":"a01r0000003n9f0AAA","slotid":"a01r0000003n9f0AAA","slotdate":"04/18","slottime":"07:30 AM"},{"recid":"a01r0000003n9d4AAA","slotid":"a01r0000003n9d4AAA","slotdate":"04/18","slottime":"11:30 AM"}]
     */
    @AuraEnabled
    public static void createSlots(String accId, String slotWrapperString, Integer numOfSlot) {
        try {
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)) {
                String appointmentFrequency = '';
        if(String.isNotBlank(accId)){
            appointmentFrequency = [Select Id, Appointment_Frequency__c From Account  Where Id = :accId WITH SECURITY_ENFORCED].Appointment_Frequency__c;
        }
        if(String.isBlank(appointmentFrequency)){
            appointmentFrequency = '30';
        }
        List<Appointment_Slot__c> appSlotsToBeInserted = new List<Appointment_Slot__c>();
        List<Appointment_Slot__c> appSlotsToBeDeleted = new List<Appointment_Slot__c>();
        System.debug('slotWrapperString----'+slotWrapperString);
        if(String.isNotEmpty(slotWrapperString)) {
            List<SlotWrapper> slotWrapperList = new SlotWrapper().parse(slotWrapperString);
            Map<String,List<Appointment_Slot__c>> appointSlotMap = new Map<String,List<Appointment_Slot__c>>();
            for(Appointment_Slot__c appointmentSlot : [SELECT Id,Name,Account__c, Date__c,
                                                                Start_Time__c, End_Time__c,
                                                                Unscheduled__c, Appointment_Count__c 
                                                        FROM Appointment_Slot__c
                                                        WHERE Account__c =: accId
                                                        AND (Date__c=TODAY OR Date__c = NEXT_N_DAYS:96) 
                                                        AND Start_Time__c != null   AND (Appointment_Count__c = 0 OR Appointment_Count__c = null) WITH SECURITY_ENFORCED
                                                        ORDER BY Date__c ASC,Start_Time__c ASC]) {
                String key = appointmentSlot.Date__c + '-' + appointmentSlot.Start_Time__c;
                if(!appointSlotMap.containsKey(key))
                    appointSlotMap.put(key,new List<Appointment_Slot__c>());
                appointSlotMap.get(key).add(appointmentSlot);
            }

            for(SlotWrapper slotObj: slotWrapperList) {
                Appointment_Slot__c aSlot = slotObj.getAppointment(accId, appointmentFrequency);
                String key = aSlot.Date__c + '-' + aSlot.Start_Time__c;
                Integer noOfNewRecords = 0;
                if(appointSlotMap.containsKey(key)) {
                    if(numOfSlot > appointSlotMap.get(key).size()) {
                        noOfNewRecords = numOfSlot - appointSlotMap.get(key).size();
                    } else if(numOfSlot < appointSlotMap.get(key).size()) {

                        for(Integer i = 0 ; i < appointSlotMap.get(key).size() - numOfSlot; i++) {
                            appSlotsToBeDeleted.add(appointSlotMap.get(key)[i]);
                        }
                    }
                } else {
                    noOfNewRecords = numOfSlot;
                }
                for(Integer i = 0; i < noOfNewRecords; i++ ) {
                    appSlotsToBeInserted.add(aSlot.clone(false,false,false,false));
                }
            }

            if(!appSlotsToBeInserted.isEmpty()) {
                appSlotsToBeInserted = VT_SecurityLibrary.getAccessibleData('Appointment_Slot__c', appSlotsToBeInserted, 'insert');
                insert appSlotsToBeInserted;
            }
            if(!appSlotsToBeDeleted.isEmpty()) {
                appSlotsToBeDeleted= VT_SecurityLibrary.getAccessibleData('Appointment_Slot__c', appSlotsToBeDeleted, 'delete');
                delete appSlotsToBeDeleted;
            }
        }
            } else {
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class SlotWrapper {

        public String recid;
        public String slotid;
        public String slotdate;
        public String slottime;
    
        public List<SlotWrapper> parse(String json) {
            return (List<SlotWrapper>) System.JSON.deserialize(json, List<SlotWrapper>.class);
        }

        public Appointment_Slot__c getAppointment(String accId, String appointmentFrequency) {

            Date dInstance = Date.today();

            Appointment_Slot__c aSlot = new Appointment_Slot__c();
            aSlot.Account__c = accId;
            Integer year = dInstance.year() ;
            Integer hour = Integer.valueOf(this.slottime?.substringBefore(':'));
            if(this.slottime?.split(' ')[1] == 'AM' && hour >= 12){
                hour = 00;
            }
            if(this.slottime?.split(' ')[1] == 'PM' && hour < 12){
                hour += 12;
            }
            if(dInstance.month()> Integer.valueOf(this.slotdate?.substringBefore('/')))
            {
                year =dInstance.year() +1;
            }
            aSlot.Start_Time__c = Time.newInstance(hour, Integer.valueOf(this.slottime?.substring(3, 5)), 0, 0);
            aSlot.End_Time__c = aSlot.Start_Time__c?.addMinutes(Integer.valueOf(appointmentFrequency));
            aSlot.Date__c = Date.newInstance(year, Integer.valueOf(this.slotdate?.substringBefore('/')), Integer.valueOf(this.slotdate?.substringAfter('/')));
            return aSlot;
        }
    }
}