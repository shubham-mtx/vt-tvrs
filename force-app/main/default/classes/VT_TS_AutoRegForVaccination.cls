/**
 * @description       : Create pre-reg record where appointment exists but pre-reg record is not created yet
 * @author            : Wasef Mohiuddin
 * @group             : 
 * @last modified on  : 01-20-2022
 * @last modified by  : Balram Dhawan
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   02-17-2021   Wasef Mohiuddin   Initial Version
**/
global with sharing class VT_TS_AutoRegForVaccination implements Database.Batchable<sobject>{

    public static final Id Citizen_COVID = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();

    global Database.Querylocator start(Database.BatchableContext bc){
        String soqlQuery = 'SELECT Id, Name, RecordTypeId ';
        soqlQuery += 'FROM Contact WHERE RecordTypeId =: Citizen_COVID ';
        return Database.getQueryLocator(soqlQuery);
    }

    global void execute(Database.BatchableContext bc, List<Contact> scope){

        Set<Id> filteredPatientIds = new Set<Id>();
        String defaultPreRegGroup = System.label.TVRS_Default_Pre_Reg_Group;
        List<Pre_Registration__c> newPreRegList = new List<Pre_Registration__c>();
        List<Pre_Registration_Group__c> preRegGroupList = [SELECT Id, Name FROM Pre_Registration_Group__c WHERE Name =: defaultPreRegGroup LIMIT 1];

        if(!preRegGroupList.isEmpty()){

            for(Appointment__c apptRecord : [SELECT Id, Patient__c, VaccineQuestion_DoseNum__c, Status__c FROM Appointment__c 
                                             WHERE Patient__c IN: scope AND (VaccineQuestion_DoseNum__c = 'First Dose' OR VaccineQuestion_DoseNum__c = 'Second Dose')
                                             AND (Status__c = 'Scheduled' OR Status__c = 'Completed')]){
                filteredPatientIds.add(apptRecord.Patient__c);
            }

            for(Pre_Registration__c existingPreReg : [SELECT Id, Contact__c FROM Pre_Registration__c WHERE Contact__c IN: filteredPatientIds]){
                filteredPatientIds.remove(existingPreReg.Contact__c);
            }

            if(!filteredPatientIds.isEmpty()){

                for(Contact con: [SELECT Id FROM Contact WHERE Id IN: filteredPatientIds]){
                    Pre_Registration__c preRegRecord = new Pre_Registration__c();
                    preRegRecord.Status__c = 'Assigned';
                    preRegRecord.Contact__c = con.Id;
                    preRegRecord.Pre_Registration_Group__c = preRegGroupList[0].Id;
                    preRegRecord.Is_Email_Sent__c = true;
                    // preRegRecord.Are_you_eligible_for_Vaccine_Risk_Group__c = 'No';
                    
                    newPreRegList.add(preRegRecord);
                }

                if(!newPreRegList.isEmpty()){
                    insert newPreRegList;
                }
            }
        }
    }

    global void finish(Database.BatchableContext bc){ 
    }

    /** Code to be run in Developer Console for the batch to run:
    * VT_TS_AutoRegForVaccination batch = new VT_TS_AutoRegForVaccination();
    * Database.executeBatch(batch); 
    **/
}