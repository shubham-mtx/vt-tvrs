/**
 * @description       : 
 * @author            : Gourav Sharma
 * @group             : 
 * @create on         : 22-04-2021
 * @last modified by  : Mohit Karani
**/
public with sharing class VT_TVRS_EventDateController {
     
    @AuraEnabled
    public Static Boolean getAllEventDetail(String eventDetail, Boolean isExistingEvent, String oldDate){

        try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                EventWrapper wrap = (EventWrapper) JSON.deserializeStrict(eventDetail, EventWrapper.class);
 
                Date oldDates = Date.valueOf(oldDate);

                String eventStartDate = wrap.eventStartDate;

                List<VTS_Event__c> event = [SELECT Id,Name, Start_Date__c, Start_Time__c, End_Date__c, End_Time__c, Location__c
                                            FROM VTS_Event__c  
                                            WHERE Location__c =: wrap.locationId
                                            AND Start_Date__c =: Date.valueOf(wrap.eventStartDate) WITH SECURITY_ENFORCED];
                if (!event.isEmpty()) {
                    if(isExistingEvent){
                        updateEventRecord(wrap.rcdId, eventStartDate,oldDates);
                        return false;
                    }
                    return true;
                }else{
                    updateEventRecord(wrap.rcdId, eventStartDate,oldDates);
                }
                return false;
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }

        

    }

    public Static void  updateEventRecord(String Id, String eventStartDate, Date oldDates){

        try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                Set<Id> appointmentSet = new  Set<Id>();
                Set<Id> appointmentSlotIdSet = new  Set<Id>();
                List<VTS_Event__c> updatedEVentlst = new List<VTS_Event__c>();
                Set<String> setOfCompletedSlotId = new Set<String>();

                List<VTS_Event__c> e = [SELECT Id,Name, Start_Date__c, Start_Time__c, End_Date__c, End_Time__c, Location__c,
                                        (SELECT id,Name,Appointment_Slot__c from appointments__r where status__c ='Scheduled' )
                                        FROM VTS_Event__c  
                                        WHERE Id =: Id
                                        WITH SECURITY_ENFORCED];
                for (VTS_Event__c lst : e) {
                                            
                    lst.Start_Date__c = Date.valueOf(eventStartDate);
                    lst.End_Date__c = Date.valueOf(eventStartDate);
                    updatedEVentlst.add(lst);
                                        
                }

                for(Appointment__c appt : [SELECT Id, Appointment_Slot__c FROM Appointment__C WHERE Event__c =: Id AND Status__c ='Completed' WITH SECURITY_ENFORCED]){
                    setOfCompletedSlotId.add(appt.Appointment_Slot__c);
                } 

                if (!updatedEVentlst.isEmpty()) {
                    VT_TS_SkipTriggerExecution.executeEventChangeLogicFromLayout = false;
                    //security check
                    updatedEVentlst = (list<VTS_Event__c>)VT_SecurityLibrary.getAccessibleData('VTS_Event__c', updatedEVentlst, 'update');

                    update updatedEVentlst;
                }            
                                        
                for (VTS_Event__c lst : e) {
                    for (Appointment__c lstapp : lst.appointments__r) {
                            appointmentSet.add(lstapp.Id);
                            appointmentSlotIdSet.add(lstapp.Appointment_Slot__c);
                    }
                }
                
                VT_TVRS_EventUpdateBatch sch = new VT_TVRS_EventUpdateBatch(appointmentSet,e[0].Location__c,Date.valueOf(eventStartDate),Id,oldDates, setOfCompletedSlotId,appointmentSlotIdSet);
                Database.executeBatch(sch);
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }

        
    }
 
    
    
   /*
    * Method Name: getEventDetail
    * Description: Event Details 
    * @return: String
    */
    @AuraEnabled
    public Static Map<String,Object> getEventDetail(String recordId){

        try{
            if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
                List<EventWrapper> selectEventWrapper = new List<EventWrapper>();
                Map<String,Object> response = new Map<String,Object>();

                for (VTS_Event__c ev : [SELECT Id,Name, Start_Date__c, Start_Time__c, End_Date__c, End_Time__c, Location__c,status__c 
                                        FROM VTS_Event__c  
                                        WHERE Id =: recordId WITH SECURITY_ENFORCED]) {

                            
                        EventWrapper evlst = new EventWrapper();
                        evlst.locationId = ev.Location__c;
                        evlst.rcdId = ev.Id;
                        evlst.eventStartDate = String.valueOf(ev.Start_Date__c);
                        evlst.status = ev.status__c;
                        selectEventWrapper.add(evlst);
                }

                response.put('eventDetails',selectEventWrapper);
            
                return response;
            }else{
                throw new VT_Exception('You are not Authorized to perform this action');
            }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }

        
  }

    // Wrapper Class Construction
    public class EventWrapper{

        @AuraEnabled public String locationId{get;set;}
        @AuraEnabled public String rcdId{get;set;}
        @AuraEnabled public String eventStartDate{get;set;}
        @AuraEnabled public String status{get;set;}

        // Wrapper class constructor
        public EventWrapper(){}
    }
}