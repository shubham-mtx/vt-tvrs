/**
 * @description       : Trigger on Account object
 * @author            : 
 * @last modified on  : 07-01-2021
 * @last modified by  : Preshit Chhabra (MTX)
 * Modifications Log 
 * Ver   Date         Author        Modification
 * 1.0   08-07-2020                 Initial Version
**/
trigger AccountTrigger on Account (before insert, before update,after insert,after update) {
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            AccountTriggerHandler.beforeInsert(trigger.new);
            VT_CRM_AccountTriggerHandler.beforeInsert(trigger.new);
            BP_SOSAccountTriggerHandler.beforeInsert(trigger.new);
        }
        if(Trigger.isUpdate) {
            AccountTriggerHandler.beforeUpdate(trigger.new,trigger.oldMap);
            VT_CRM_AccountTriggerHandler.beforeUpdate(trigger.new,trigger.oldMap);
            BP_SOSAccountTriggerHandler.beforeUpdate(trigger.newMap,trigger.oldMap);
        }
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            VT_CRM_AccountTriggerHandler.afterInsert(trigger.new,trigger.oldMap);
        }

        if(Trigger.isUpdate){
            BP_SOSAccountTriggerHandler.afterUpdate(trigger.newMap,trigger.oldMap);
        }
        
    }
}