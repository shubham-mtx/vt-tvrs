trigger VT_VTS_EventTrigger on VTS_Event__c (before insert,before update) {
    if(trigger.isBefore){
        if(trigger.isUpdate){
            System.debug('Event Fired>>>>');
            VT_VTS_EventTriggerHandler.BeforeUpdate(trigger.newMap,trigger.oldMap);
        }
    }
}