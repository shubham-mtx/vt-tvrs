import { api,track,LightningElement } from 'lwc';
import getEventDetail from '@salesforce/apex/VT_TVRS_EventDateController.getEventDetail';
import getAllEventDetail from '@salesforce/apex/VT_TVRS_EventDateController.getAllEventDetail';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import momentPath from "@salesforce/resourceUrl/momentJs";
import {loadScript} from "lightning/platformResourceLoader";
import hasPermission from '@salesforce/customPermission/VTS_Event_Modifiers';


export default class Vt_tvrs_updateeventdate extends LightningElement {

    @api
    recordId;
    @track showSpinner = false;
    @track isEditable = false;
    @track resultObject = [];
    @track showConfirmationBtn = false;
    @track locationId = '';
    @track startTime = null;
    @track endTime = null;
    @track startDate = null;
    @track endDate = null;
    @track noEdit = false;
    @track enableEditBtn = true;
    @track showMsg = false;
    @track today = '';
    @track showEventEdit = false;
    @track oldDate;
    @track modalContent = '';
     
    connectedCallback(){
        
            let today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
          
            if (dd < 10) dd = "0" + dd;
            if (mm < 10) mm = "0" + mm;
            this.today = yyyy + "-" + mm + "-" + dd;
 
         
        this.enableEditBtn= true;
        this.showEventEdit = false;
        if(!hasPermission) {
            this.dispatchEvent(new CustomEvent('permissionerror'));
            this.dispatchEvent(new ShowToastEvent({
                message: 'You do not have enough permission to access this',
                variant: 'error'
            }));
        }else{
            this.showSpinner = true;
        
        getEventDetail({
            recordId: this.recordId
        }).then((result) => {
            this.showSpinner = false;
            this.resultObject = result.eventDetails[0];

            this.oldDate = this.resultObject.eventStartDate;
            if(this.resultObject.status == 'Canceled'){
                this.dispatchEvent(new ShowToastEvent({
                    message: 'Date cannot be modified for Canceled events',
                    variant: 'error'
                }));
                this.closeQuickAction();
                this.showMsg = true;
                this.showEventEdit = true;
                return false;
            }

            
            this.noEdit = false;
            loadScript(this, momentPath).then(() => {
                var d  = new Date();
                if(moment(d).format('MM-DD-YYYY') > moment(this.resultObject.eventStartDate).format('MM-DD-YYYY')){
                    this.dispatchEvent(new ShowToastEvent({
                        message: 'Date cannot be modified for past events',
                        variant: 'error'
                    }));
                    this.dispatchEvent(new CustomEvent('close'));
                    this.noEdit = true;
                    this.showEventEdit = true;
                } 
            });
        }).catch((error) => {
            console.log(JSON.stringify(error));
           
        });
        }
    }

    isValid(){
        let valid = false;

			valid = [...this.template.querySelectorAll("lightning-input")].reduce(
				(validSoFar, input) => {
					input.reportValidity();
					return validSoFar && input.checkValidity();
				},
				true
			);

            return valid;
    }
 
    handleOnChange(event){
        this.enableEditBtn= false;
        this.resultObject[event.target.name]= event.detail.value; 
    }
    handleEventUpdate(event){
        if(this.isValid()){
            this.showConfirmationBtn = false;
            console.log('-=-=-',JSON.stringify(this.resultObject))
            getAllEventDetail({ eventDetail: JSON.stringify(this.resultObject),isExistingEvent : false, oldDate : this.oldDate})
            .then(result => {
                if(result){
    
                    // const event = new ShowToastEvent({
                    //     title: 'Info',
                    //     variant: 'Info',
                    //     message: 'An event already exists for the date. Please confirm before changing.',
                    // });
                    // this.dispatchEvent(event);
                    this.modalContent = 'An event already exists for the date. Please confirm before changing.';
                    this.showConfirmationBtn = true;
    
                }else{
                    this.updateRecordView(this.recordId);
                    const event = new ShowToastEvent({
                        title: 'Success',
                        variant: 'Success',
                        message: 'Event Date updated sucessfully. It might take couple of minutes to reflect on manage appointment slot window.',
                    });
                    this.dispatchEvent(event);
                    this.closeQuickAction();
                }
              })
              .catch(error => {
                console.error('Error:', error);
            });

        }

    }

    modalCancel() {
        this.showConfirmationBtn = false;
    }
    handleEventUpdateContinue(){
        if(this.isValid()){
        this.showConfirmationBtn = false;
        console.log('-=-=-',JSON.stringify(this.resultObject))

        getAllEventDetail({ eventDetail: JSON.stringify(this.resultObject), isExistingEvent : true,oldDate : this.oldDate}).then(result => {
            if(result){

                const event = new ShowToastEvent({
                    title: 'Info',
                    variant: 'Info',
                    message: 'An event already exists for the date. Please confirm before changing.',
                });
                this.dispatchEvent(event);
                this.showConfirmationBtn = true;

            }else{
                this.updateRecordView(this.recordId);
                const event = new ShowToastEvent({
                    title: 'Success',
                    variant: 'Success',
                    message: 'Event Date updated sucessfully. It might take couple of minutes to reflect on manage appointment slot window.',
                });
                this.dispatchEvent(event);
                this.closeQuickAction();
            }
          })
          .catch(error => {
            console.error('Error:', error);
        });
    }
    }
  
    closeQuickAction() {
        const closeQA = new CustomEvent('close');
        this.dispatchEvent(closeQA);
    }
    handleClick(event){         
         if (event.target.dataset.func == "Cancel") {
            this.closeQuickAction();
        }
    }
    updateRecordView(recordId) {
        updateRecord({fields: { Id: recordId }});
    }
}