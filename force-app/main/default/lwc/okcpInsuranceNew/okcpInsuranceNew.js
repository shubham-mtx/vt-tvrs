import { LightningElement, track, api, wire } from 'lwc';
import getInsurance from "@salesforce/apex/OkpcContactInformationController.getInsurance";
import setInsurance from "@salesforce/apex/OkpcContactInformationController.setInsurance";
import { getPicklistValues } from 'lightning/uiObjectInfoApi';

import Has_Insurance_Field from '@salesforce/schema/Contact.Has_Insurance__c';
import Type_of_Insurance_Field from '@salesforce/schema/Contact.Type_of_Insurance__c';
import Patient_relationship_to_insured_Field from '@salesforce/schema/Appointment__c.Patient_s_relationship_to_insured__c';
import Insured_Sex_Field from '@salesforce/schema/Contact.Insured_Sex__c';

export default class OkcpInsurance extends LightningElement {
    @api isAppointment;
    @api appointmentContact = {};

    @track obj = {};
    @track showSpinner;
    currentstep = 6;
    @api appointmentId;
    @api dependentContactId = '';
    @track hasInsurance = false;
    @track hasAdditionalInsurance = false;

    get onlyYesNoOptions() {
        return [{
            label: "Yes",
            value: "Yes"
        },
        {
            label: "No",
            value: "No"
        }];
    }

    connectedCallback() {       

        if(this.isAppointment) {
            this.obj = JSON.parse(JSON.stringify(this.appointmentContact));
            if(this.obj.Has_Insurance == 'Yes'){
                this.hasInsurance = true;
            }
            else{
                this.hasInsurance = false;

            }
        } else {
            this.showSpinner = true;
            getInsurance({ appointmentId: this.appointmentId })
            .then(result => {
                this.obj.Has_Insurance = result.Has_Insurance__c;
                if(this.obj.Has_Insurance == 'Yes'){
                    this.hasInsurance = true;
                }
                else{
                    this.hasInsurance = false;

                }

                this.obj.Insured_authorization_for_payment = result.Insured_s_authorization_for_payment__c ? 'True' : null;
                this.obj.Patient_authorization_to_release_info = result.Patient_s_Authorization_to_Release_Info__c ? 'True' : null;

                this.obj.Type_of_Insurance = result.Type_of_Insurance__c;
                this.obj.Insurance_ID_Number = result.Insurance_ID_Number__c;
                this.obj.Insurance_Plan_Name = result.Insurance_Plan_Name__c;
                this.obj.Policy_Group_or_FECA_number = result.Policy_Group_or_FECA_number__c;
                this.obj.Patient_relationship_to_insured = result.Patient_s_relationship_to_insured__c ? result.Patient_s_relationship_to_insured__c : 'Self';

                this.obj.Insured_Last_Name = result.Insured_Last_Name__c;
                this.obj.Insured_First_Name = result.Insured_First_Name__c;
                this.obj.Insured_Middle_Name = result.Insured_Middle_Name__c;
                this.obj.Insured_Street_Address = result.Insured_Street_Address__c;
                this.obj.Insured_s_City = result.Insured_s_City__c;
                this.obj.Insured_s_State = result.Insured_s_State__c;
                this.obj.Insured_Zip_Code = result.Insured_Zip_Code__c;
                if (result.Insured_Telephone__c) {
                    this.obj.Insured_Telephone = this.formatPhoneInput(result.Insured_Telephone__c);
                }
                this.obj.Insured_date_of_birth = result.Insured_date_of_birth__c;
                this.obj.Insured_Sex = result.Insured_Sex__c;

                this.obj.Another_Health_Plan = result.Another_Health_Plan__c ? 'Yes' : 'No';
                if(this.obj.Another_Health_Plan == 'Yes'){
                    this.hasAdditionalInsurance = true;
                 }
                 else{
                    this.obj.this.hasAdditionalInsurance = false;
                 }

                this.obj.Additional_Insurance_Plan_Name = result.Additional_Insurance_Plan_Name__c;
                this.obj.Additional_Insurance_Policy_Group_Number = result.Additional_Insurance_Policy_Group_Number__c;
                this.obj.Additional_Insurance_Last_Name = result.Additional_Insurance_Last_Name__c;
                this.obj.Additional_Insurance_First_Name = result.Additional_Insurance_First_Name__c;
                this.obj.Additional_Insurance_Middle_Name = result.Additional_Insurance_Middle_Name__c;

                this.showSpinner = false;
            })
            .catch(error => {
                console.log('error=>', JSON.stringify(error));
                this.showSpinner = false;
            })
        }
    }

    navigateToCancel() {
        this.dispatchEvent(new CustomEvent('cancelappointment'));
    }

    handleChange(event) {
        this.obj[event.target.name] = event.target.value;
        if (event.target.name == 'Insured_authorization_for_payment' || event.target.name == 'Patient_authorization_to_release_info') {
            this.obj[event.target.name] = event.target.checked ? 'True' : null;
        }
        if(event.target.name == 'Insured_Telephone') {
            this.obj[event.target.name] = this.formatPhone(event.target);
        }
        if(event.target.name == 'Has_Insurance'){
            this.hasInsuranceValue();
        }
        if(event.target.name == 'Another_Health_Plan'){
            this.hasAdditionalInsuranceShow();
        }
        if(event.target.name == 'Insurance_ID_Number'){
            console.log('In if before');
           /* if((event.keyCode >= 95 &&  event.keyCode <= 105) || (event.keyCode > 48 &&  event.keyCode <= 57)) {
                console.log('In if ');
                event.preventDefault();
            }*/

            
          this.formatInsuredNumber(event.target);
        }
        
    }

    navigateToBack() {
        const selectedEvent = new CustomEvent("backevent", {
            detail: null
        });
        this.dispatchEvent(selectedEvent);
    }

    sendEventToParent(obj) {
        const selectedEvent = new CustomEvent("save", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }
    formatInsuredNumber(obj) {
        console.log('in format');
        var numbers = obj.value.replace(/[^A-Za-z0-9]/, '');
        //var numbers = obj.value.replace(!/[^a-zA-Z0-9]/, '');
        
        //var numbers = obj.value.replace(/[&\/\\#, +()$~%.'":*?<>{}]/g, '');
        obj.value = "";
        for (var i = 0; i < numbers.length; i++) {
            obj.value += numbers[i];
        }
    
    }

    saveData() {
        if (this.isValid()) {
            //Valdiations
            console.log("obj =>", JSON.stringify(this.obj));
            if (!this.obj.Has_Insurance || (this.obj.Has_Insurance == 'Yes' && (this.obj.Insured_authorization_for_payment != 'True' || this.obj.Patient_authorization_to_release_info != 'True'))) {
                let tempObj = {};
                tempObj.message = "Please Fill all the required fields";
                tempObj.status = "error";
                this.sendEventToParent(tempObj);
                return;
            }

            if(this.isAppointment) {
                const selectedEvent = new CustomEvent("setobject", {
                    detail: this.obj
                });
                this.dispatchEvent(selectedEvent);
            } else {
                this.showSpinner = true;
                console.log('this.appointmentId ' + this.appointmentId);
                setInsurance({
                    data: this.obj,
                    currentstep: this.currentstep,
                    AppointmentId: this.appointmentId
                })
                .then(result => {
                    this.showSpinner = false;
                    let tempObj = {};
                    tempObj.message = "saved successfully";
                    tempObj.status = "success";
                    this.sendEventToParent(tempObj);
                })
                .catch(error => {
                    console.log("error =>", JSON.stringify(error));
                    this.showSpinner = false;
                })
            }
        }
    }

    isValid() {
        let valid = true;
        let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        valid = isAllValid;
        return valid;
    }

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: Has_Insurance_Field,
    })
    picklistOption1;

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: Type_of_Insurance_Field,
    })
    picklistOption2;

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: Patient_relationship_to_insured_Field,
    })
    picklistOption3;

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: Insured_Sex_Field,
    })
    picklistOption4;


     hasInsuranceValue() {
        if(this.obj.Has_Insurance == 'Yes'){
            this.hasInsurance =  true;
        }
        else{
            let temp = this.obj.Has_Insurance; 
            this.obj = {};
            this.obj.Has_Insurance = temp;
            this.hasInsurance =  false;
        }
        
    }

    get isSelfInsurance() {
        return this.obj.Patient_relationship_to_insured == 'Self';
    }

     hasAdditionalInsuranceShow() {
         if(this.obj.Another_Health_Plan == 'Yes'){
            this.hasAdditionalInsurance  = true;
         }
         else{
             this.obj.Additional_Insurance_Plan_Name = '';
             this.obj.Additional_Insurance_Policy_Group_Number = '';
             this.obj.Additional_Insurance_First_Name = '';
             this.obj.Additional_Insurance_Middle_Name = '';
             this.obj.Additional_Insurance_Last_Name = '';
             this.hasAdditionalInsurance = false;
         }
        
    }

    formatPhone(obj) {
        var numbers = obj.value.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj.value = "";
        for (var i = 0; i < numbers.length; i++) {
            obj.value += (char[i] || "") + numbers[i];
        }
        return obj.value;
    }

    formatPhoneInput(obj) {
        var numbers = obj.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj = "";
        for (var i = 0; i < numbers.length; i++) {
            obj += (char[i] || "") + numbers[i];
        }
        return obj;
    }
}