/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 01-15-2022
 * @last modified by  : Mohit Karani
**/
public without sharing class VTTS_AppointmentHelper {
    private Map<String,Object> result;
    private boolean isInvalid;
    public static Map<String, String> vaccineTypeVsDoseNumber = new Map<String, String>{
        'Vaccination-2' =>  'Second Dose',
        'Vaccination-3' =>  'Third Dose',
        'Vaccination-4' =>  'Fourth Dose'
    };

    public VTTS_AppointmentHelper(){ 
        result = new Map<String,Object>();
        isInvalid = false;
    }

    public void identifyPrivateAcccessUsingContactId(String contactId, String vaccineTypes, String vaccineClass){
        isInvalid = false;

        //Load Pre-Reg Records
        List<Pre_Registration__c> preRegs = VTTS_PreRegHelper.loadExistingPreRegRecord(contactId);
        if( !preRegs.isEmpty() ){
            Pre_Registration__c preRegRecord = preRegs[0];
            Boolean isEligible = false;

            VTTS_PreRegHelper.preRegEligiblityHelper(result, preRegRecord);

            if( String.isNotBlank(vaccineTypes) || String.isNotBlank(vaccineClass) ){
                vaccineClass = String.isBlank(vaccineClass) ? '':vaccineClass;
                vaccineTypes =  String.isBlank(vaccineTypes) ? '':vaccineTypes;
                String tempVaccineType = '';
                for(String vaccinetypeName : vaccineTypes.split(',')){
                    // tempVaccineType+= (vaccinetypeName == VT_TS_Constants.FIRST_DOSE_TEXT) ? ','+tempVaccineType : ','+ vaccinetypeName.split('-')[0] + '-'+ (Integer.valueOf(vaccinetypeName.split('-')[1] +1));
                    if(String.isBlank(tempVaccineType)){
                        tempVaccineType += (vaccinetypeName == VT_TS_Constants.FIRST_DOSE_TEXT) ? vaccinetypeName : vaccinetypeName.split('-')[0] + '-'+ ((Integer.valueOf(vaccinetypeName.split('-')[1]) -1));
                    }else{
                        tempVaccineType += (vaccinetypeName == VT_TS_Constants.FIRST_DOSE_TEXT) ?','+ vaccinetypeName : ','+ vaccinetypeName.split('-')[0] + '-'+ ((Integer.valueOf(vaccinetypeName.split('-')[1]) -1));
                    }
                }
                List<Private_Access__c> paList = [SELECT Id,Is_Alternate_Dose_Available__c,Alternate_Doses__c
                                                    FROM Private_Access__c
                                                    WHERE Is_Active__c = true
                                                    AND Is_Alternate_Dose_Available__c = true
                                                    AND Vaccine_Type__c IN : tempVaccineType.split(',')
                                                    AND Vaccine_Class_Name__c  IN : vaccineClass.split(',')
                                                    WITH SECURITY_ENFORCED];
                if(!paList.isEmpty() && String.isNotBlank(paList[0].Alternate_Doses__c)){
                    for(String alternateDose : paList[0].Alternate_Doses__c.split(';')){
                        vaccineClass+= ','+alternateDose;
                    }
                }
                VTTS_PreRegHelper.fetchPrivateAccessGroups(result, vaccineTypes, vaccineClass);
                if( result.containsKey('canSchedule') && !result.containsKey('eventIds') ){
                    isInvalid = true;
                    // result.put('noOpenEventsFoundInPreRegGroups', 'Your group is not Open for scheduling at this time, but you will be notified when your Group is opened for scheduling.');
                    result.put('noOpenEventsFoundInPreRegGroups', 'The person you are trying to schedule is not currently eligible for any Vaccine type. To see when they may be eligible, stay up to date by visiting our website at <a href="https://www.healthvermont.gov/covid-19/vaccine/getting-covid-19-vaccine" target="_blank"> Getting the COVID-19 Vaccine | Vermont Department of Health (healthvermont.gov)</a>');
                    result.remove('canSchedule');
                }
            }
        }
        else{
            result.put('preRegPending', true);
            isInvalid = true;
        }
    }

    public void identifyPrivateAcccessUsingPreRegData(Map<String, Object> result, String vaccineTypes, String vaccineClass) {
        Contact c = (Contact) result.get('data');
        Pre_Registration__c preRegRecord;
        Pre_Registration__c preReg = (Pre_Registration__c) result.get('preReg');
        List<Pre_Registration__c> pre = new List<Pre_Registration__c>();
        
        //Step 1: Check is contact is already saved or not if saved then check for existing pre-reg information
        if( c.Id != null ){
            pre = VTTS_PreRegHelper.loadExistingPreRegRecord(c.Id);
        }

        //Step 2: If pre-reg is not created then check validations
        if( pre.isEmpty() ||
           	(
                !pre.isEmpty() &&

                (
                    // (preReg.Are_you_eligible_for_Vaccine_Risk_Group__c == 'Yes' &&
                    // pre[0].Are_you_eligible_for_Vaccine_Risk_Group__c != 'Yes') ||

                    (preReg.Chronic_Condition__c == 'Yes' &&
                    pre[0].Chronic_Condition__c != 'Yes') ||

                    (preReg.Affiliated_to_any_other_Risk_Group__c == 'Yes' &&
                    pre[0].Affiliated_to_any_other_Risk_Group__c != 'Yes') || 

                    (String.isNotBlank(preReg.Actual_Pass_Code__c)) || 

                    (preReg.BIPOC__c == 'Yes' &&
                    pre[0].BIPOC__c != 'Yes') ||

                    (preReg.Pre_Registration_Group__c != 
                    pre[0].Pre_Registration_Group__c)
                )
            ) 
        ) { 
            VT_PreRegAssignmentEngine.processAssignment(new Map<Contact, Pre_Registration__c> {c => preReg});
            if (preReg.Affiliated_to_any_other_Risk_Group__c == 'Yes' && ((preReg.Early_Group_2_Access_Group__c == null && String.isNotBlank(preReg.Pass_Code_Risk_Group__c ))||  (preReg.Early_Group_2_Access_Group__r.Beginning_DOB__c != null && preReg.Early_Group_2_Access_Group__r.Ending_DOB__c != null &&(preReg.Early_Group_2_Access_Group__r.Ending_DOB__c < c.Birthdate || preReg.Early_Group_2_Access_Group__r.Beginning_DOB__c > c.Birthdate)))) {
                result.put('Pass_Code_Risk_Group__c', 'NOT_VALID');
            }
            if( preReg.Chronic_Condition__c == 'Yes' && String.isBlank(preReg.Early_Group1_Health__c) && 
                preReg.Affiliated_to_any_other_Risk_Group__c == 'Yes' && String.isBlank(preReg.Early_Group_2_Access_Group__c)) {

                result.put('invalidChronicAnswer', 'We could not find any pre-registration group based on the responses provided for the high-risk health conditions and other risk group.');
            }
            //Validation 1: Check if groupd was assigned based on the risk group information
            if(preReg.Chronic_Condition__c == 'Yes' && String.isBlank(preReg.Early_Group1_Health__c)) {
                result.put('invalidChronicAnswer', 'We could not find any pre-registration group based on the responses provided for the high-risk health conditions.');
            }
            else if(preReg.Affiliated_to_any_other_Risk_Group__c == 'Yes' && String.isBlank(preReg.Early_Group_2_Access_Group__c)) {
                result.put('invalidOtherRiskGroupAnswer', 'The Passcode you have used is either invalid or unauthorized for your situation.');
            }
            preRegRecord = preReg;
            if( !pre.isEmpty() ){
                preReg.High_Risk_Health_Registration_Date__c = pre[0].High_Risk_Health_Registration_Date__c;
                result.put('preRegOld', pre[0]);
            }
        }
        else {
            preRegRecord = pre[0];
            result.put('preRegOld', pre[0]);
        }

        //Validation 1: Check if any of the group is active or not
        if (preRegRecord != null) {
            VTTS_PreRegHelper.preRegEligiblityHelper(result, preRegRecord);

            if( String.isNotBlank(vaccineTypes) || String.isNotBlank(vaccineClass) ){

                VTTS_PreRegHelper.fetchPrivateAccessGroups(result, vaccineTypes, vaccineClass);
                
                if( result.containsKey('canSchedule') && !result.containsKey('eventIds') ){
                    isInvalid = true;
                    
                    // result.put('noOpenEventsFoundInPreRegGroups', 'Your group is not Open for scheduling at this time, but you will be notified when your Group is opened for scheduling.');
                    result.put('noOpenEventsFoundInPreRegGroups', 'The person you are trying to schedule is not currently eligible for any Vaccine type. To see when they may be eligible, stay up to date by visiting our website at <a href="https://www.healthvermont.gov/covid-19/vaccine/getting-covid-19-vaccine" target="_blank"> Getting the COVID-19 Vaccine | Vermont Department of Health (healthvermont.gov)</a>');

                    result.remove('canSchedule');
                }
            }
        }
    }

    public boolean isInvalid(){
        return isInvalid;
    }

    public Map<String,Object> getResult(){
        return result;
    }

    // This method can be replaced by validateAppointmentForBoosterDose
    public void validateAppointments( String contactId, String vaccineTypes , Boolean isSecondDose ){
        validateAppointments(contactId, vaccineTypes);
        
        if( isSecondDose && (result.containsKey('isAlreadyCompleted'))){
            result.remove('isAlreadyCompletedData');
            result.remove('isAlreadyCompleted');
            isInvalid = false;
        }
    }

    /**
    * @description : Method to validate appoint for booster dose.
    * @author Mohit Karani | 08-24-2021 
    * @param String contactId,String vaccineTypes,String currentVaccine
    **/
    public void validateAppointmentForBoosterDose(string jsonString){
        VT_TS_Wrapper validationWrapper = (VT_TS_Wrapper) JSON.deserialize(jsonString, VT_TS_Wrapper.class);
        validateAppointmentsBooster(jsonString);
    }


    /**************Appointment & Slot Related Methods*******************/
    public void validateAppointments(String contactId,String vaccineTypes ){
        isInvalid = false;

        //Step 2: Query Appointments and check for Dose Validations
        List<Appointment__c> appointments = [SELECT Id, Name, Status__c, Event__r.Private_Access__r.Requires_Followup_Dose__c,
                                            Event__r.Private_Access__r.Vaccine_Type__c, Lab_Center_Address__c, Lab_Center__r.BillingAddress, 
                                            Appointment_Date__c, Appointment_Start_Time__c, Lab_Center__r.Name  
                                            FROM Appointment__c 
                                            WHERE Event__r.Location__r.Event_Process__c = 'Vaccination' 
                                            AND Event__r.Private_Access__r.Vaccine_Type__c in: vaccineTypes.split(',')
                                            AND Patient__c =:contactId 
                                            AND (Status__c = 'Scheduled' OR Status__c = 'Completed') WITH SECURITY_ENFORCED
                                            Order By Event__r.Private_Access__r.Vaccine_Type__c Desc];
        
        for( Appointment__c appt : appointments ){
            if( appt.Event__r.Private_Access__r.Vaccine_Type__c == 'Vaccination-3' ){
                if( appt.Status__c == 'Scheduled'){
                    result.put('is3rdDoseAlreadyScheduledData', appt);
                    result.put('is3rdDoseAlreadyScheduled', true);
                    isInvalid = true;
                    break;
                }
                // updated by Mohit karani so we can schedule next dose only if follow-up dose is required.
                else if( appt.Status__c == 'Completed' && appt.Event__r.Private_Access__r.Requires_Followup_Dose__c == 'No'){
                    result.put('is3rdDoseAlreadyCompletedData', appt);
                    result.put('is3rdDoseAlreadyCompleted', true);
                    result.put('furtherDosesNotReqd', true);
                    isInvalid = true;
                    break;
                }
            }
            if( appt.Event__r.Private_Access__r.Vaccine_Type__c == 'Vaccination-2' ){
                if( appt.Status__c == 'Scheduled'){
                    result.put('is2ndDoseAlreadyScheduledData', appt);
                    result.put('is2ndDoseAlreadyScheduled', true);
                    isInvalid = true;
                    break;
                }

                // else if( appt.Status__c == 'Completed'){
                //     result.put('is2ndDoseAlreadyCompletedData', appt);
                //     result.put('is2ndDoseAlreadyCompleted', true);
                //     isInvalid = true;
                //     break;
                // }
                //updated by Mohit karani for booster so we can schedule next dose only if follow-up dose is required.
                else if( appt.Status__c == 'Completed' && appt.Event__r.Private_Access__r.Requires_Followup_Dose__c == 'No'){
                    result.put('is2ndDoseAlreadyCompletedData', appt);
                    result.put('is2ndDoseAlreadyCompleted', true);
                    result.put('furtherDosesNotReqd', true);
                    isInvalid = true;
                    break;
                }
                else if( appt.Status__c == 'Completed' && appt.Event__r.Private_Access__r.Requires_Followup_Dose__c == 'Yes'){
                    result.put('is2ndDoseAlreadyCompletedData', appt);
                    result.put('is2ndDoseAlreadyCompleted', true);
                    isInvalid = true;
                    break;
                }

            }
            else if( appt.Event__r.Private_Access__r.Vaccine_Type__c == 'Vaccination-1' ){
                if( appt.Status__c == 'Scheduled'){
                    result.put('isAlreadyScheduledData', appt);
                    result.put('isAlreadyScheduled', true);
                    isInvalid = true;
                    break;
                }
                else if(appt.Status__c == 'Completed' && appt.Event__r.Private_Access__r.Requires_Followup_Dose__c == 'No'){
                    result.put('secondDoseNotRequiredData', appt);
                    result.put('secondDoseNotRequired', true);
                    isInvalid = true;
                    break;
                }
                else if( appt.Status__c == 'Completed' && appt.Event__r.Private_Access__r.Requires_Followup_Dose__c == 'Yes'){
                    result.put('isAlreadyCompletedData', appt);
                    result.put('isAlreadyCompleted', true);
                    isInvalid = true;
                    break;
                }
            }
        }
    }

    public class VT_TS_Wrapper{
        public string contactId;
        public string currentVaccine;
        public string vaccineTypes;
        public Boolean isCheckIn = false;
        public Boolean isFromTestingSite = false;
    }
    public void validateAppointmentsBooster(String jsonString){
        isInvalid = false;
        VT_TS_Wrapper validationWrapper = (VT_TS_Wrapper) JSON.deserialize(jsonString, VT_TS_Wrapper.class);
        //Step 2: Query Appointments and check for Dose Validations
        List<Appointment__c> appointments = [SELECT Id, Name, Status__c, Event__r.Private_Access__r.Requires_Followup_Dose__c,
                                            Event__r.Private_Access__r.Vaccine_Type__c, Lab_Center_Address__c, Lab_Center__r.BillingAddress, 
                                            Appointment_Date__c, Appointment_Start_Time__c, Lab_Center__r.Name,VaccineQuestion_DoseNum__c,
                                            Event_Process__c	  
                                            FROM Appointment__c 
                                            WHERE Event__r.Location__r.Event_Process__c = 'Vaccination' 
                                            AND Event__r.Private_Access__r.Vaccine_Type__c in: validationWrapper.vaccineTypes.split(',')
                                            AND Patient__c =:validationWrapper.contactId 
                                            AND (Status__c =: VT_TS_Constants.COMPLETED_TEXT OR Status__c =: VT_TS_Constants.SCHEDULED_TEXT) WITH SECURITY_ENFORCED
                                            Order By Event__r.Private_Access__r.Vaccine_Type__c Desc];

        for( Appointment__c appt : appointments ){
            if(appt.Event_Process__c == VT_TS_Constants.VACCINATION_TEXT &&  appt.Event__r.Private_Access__r.Requires_Followup_Dose__c == 'No' && appt.Status__c == VT_TS_Constants.COMPLETED_TEXT){
                isInvalid = true;
                result.put('furtherDosesNotReqd', true);
                break;
            }
            else if(appt.Event_Process__c == VT_TS_Constants.VACCINATION_TEXT && validationWrapper.currentVaccine != appt.Event__r.Private_Access__r.Vaccine_Type__c  &&  appt.Status__c == VT_TS_Constants.SCHEDULED_TEXT){
                //since we allow him to schedule alternate dose
                isInvalid = true;  
                result.put('previousDoseInScheduledStatus', 'It looks like you already have an appointment to get your ' +  appt.VaccineQuestion_DoseNum__c + ' of a COVID-19 vaccine. If you want to change your appointment, you need to cancel your existing appointment first.');
                //result.put('previousDoseInScheduledStatus',true);
                break;
            }
            else if((validationWrapper.isFromTestingSite != null && validationWrapper.isFromTestingSite) && appt.Status__c == VT_TS_Constants.COMPLETED_TEXT){
                
                if(!result.containsKey('isTestingAndSelectedDoseCompleted')){
                    result.put('isTestingAndSelectedDoseCompleted', new List<Object>());
                }
                List<Object> temp = (List<Object>)result.get('isTestingAndSelectedDoseCompleted');
                temp.add(appt.VaccineQuestion_DoseNum__c);
                result.put('isTestingAndSelectedDoseCompleted',temp);
            }

            else if(appt.Event_Process__c == VT_TS_Constants.VACCINATION_TEXT && (validationWrapper.isFromTestingSite == null ||  !validationWrapper.isFromTestingSite) && appt.Event__r.Private_Access__r.Vaccine_Type__c != null &&  validationWrapper.currentVaccine != null &&(appt.Event__r.Private_Access__r.Vaccine_Type__c .split('-')[1] > validationWrapper.currentVaccine.split('-')[1]) || (validationWrapper.currentVaccine == appt.Event__r.Private_Access__r.Vaccine_Type__c &&  appt.Status__c == VT_TS_Constants.COMPLETED_TEXT)){
                isInvalid = true;
                result.put('isSelectedDoseAlreadyCompleted', true);
                break;
            }
            else if(validationWrapper.isCheckIn == null || !validationWrapper.isCheckIn && validationWrapper.currentVaccine == appt.Event__r.Private_Access__r.Vaccine_Type__c &&  appt.Status__c == VT_TS_Constants.SCHEDULED_TEXT){
                isInvalid = true;
                result.put('isSelectedDoseAlreadyScheduled', true);
                result.put('existingAppointmentData', appt);
                result.put('doseNumber',appt.VaccineQuestion_DoseNum__c);
                break;
            }
        }
    }

    public static Map<String,Object> checkIfreshecdulingAlreadyCompletedDose(Map<String,Object> result,String appointVacType,String currentVacType){
        if(VT_TS_Constants.VACCINE_TYPE_TO_ALREADY_HAD_VACCINES_MAP.get(appointVacType) != null && VT_TS_Constants.VACCINE_TYPE_TO_ALREADY_HAD_VACCINES_MAP.get(appointVacType).contains(currentVacType)){
            result.put('isSelectedDoseAlreadyCompleted', true);
        }
        return result;
    }

    /**
     * @author : balram.dhawan@mtxb2b.com
     * @description : validate existing dose and find next vaccine Dose
    */
    public void validateAppointments( String contactId ){
        isInvalid = false;

        //Step 2: Query Appointments and check for Dose Validations
        List<Appointment__c> appointments = [SELECT Id, Name, Status__c, Event__r.Private_Access__r.Requires_Followup_Dose__c,
                                            Event__r.Private_Access__r.Vaccine_Type__c, Lab_Center_Address__c, Lab_Center__r.BillingAddress, 
                                            Appointment_Date__c, Appointment_Start_Time__c, Lab_Center__r.Name  
                                            FROM Appointment__c 
                                            WHERE Event__r.Location__r.Event_Process__c = 'Vaccination' 
                                            AND Patient__c =:contactId 
                                            AND (Status__c = 'Scheduled' OR Status__c = 'Completed') WITH SECURITY_ENFORCED
                                            Order By Event__r.Private_Access__r.Vaccine_Type__c];

        for( Appointment__c appt : appointments ){
            if(appt.Status__c == 'Scheduled') {
                result.put('isAlreadyScheduledData', appt);
                result.put('isAlreadyScheduled', true);
                isInvalid = true;
                break;
            } else if (appt.Status__c == 'Completed') {
                String existingVaccineType = appt.Event__r.Private_Access__r.Vaccine_Type__c;
                Integer doseNumber = Integer.valueOf(appt.Event__r.Private_Access__r.Vaccine_Type__c.split('-')[1]);
                result.put('nextDose', doseNumber+1);
                result.put('isNextDoseAvailable', getLastDoseNumber() >= doseNumber);
            }
        }
    }

    public static Map<String, Object> checkExistingAppointment(Appointment__c appointment) {
        Map<String, Object> result = new Map<String, Object>();
        String vaccineType = [SELECT Id, Private_Access__r.Vaccine_Type__c FROM VTS_Event__c WHERE Id =:appointment.Event__c]?.Private_Access__r.Vaccine_Type__c;
        List<Appointment__c> appointments = [SELECT Id,Status__c FROM Appointment__c 
                                            WHERE Patient__c =:appointment.Patient__c 
                                            AND Event_Process__c = 'Vaccination'
                                            AND Event__r.Private_Access__r.Vaccine_Type__c =:vaccineType
                                            AND (Status__c = 'Scheduled' OR Status__c = 'Completed')
                                            WITH SECURITY_ENFORCED];
        if(!appointments.isEmpty()) {
            result.put('alreadyHaveAppointment', true);
            result.put('appointment', appointments[0]);
            result.put('status', appointments[0].Status__c);
        } else {
            result.put('alreadyHaveAppointment', false);
        }
        return result;
    }

    public static Appointment_Slot__c validateAndIdentifySlot(String slotId) {
        //Step 1: Query Slot based on ID passed from frontend
        List<Appointment_Slot__c> slotList = [Select Appointment_Count__c,Account__c,Start_Time__c,Date__c From Appointment_Slot__c 
                                                Where ID = :slotId];
        // Identify Slots
        if (!slotList.isEmpty()) {
            //Step 2: Query similar slots based on above slot's to check other available slots for same date and time.
            List<Appointment_Slot__c> availableSlotList = [Select Id,Appointment_Count__c,Account__c,Start_Time__c,Date__c 
                                                            From Appointment_Slot__c 
                                                            where Account__c = :slotList[0].Account__c 
                                                            AND Start_Time__c = :slotList[0].Start_Time__c 
                                                            AND Date__c = :slotList[0].Date__c 
                                                            AND (Appointment_Count__c = null OR Appointment_Count__c = 0) WITH SECURITY_ENFORCED];
            if(!availableSlotList.isEmpty()) {
                //Step 3: Pick one slot and re-verify if its available and put FOR UPDATE lock on it so other wont able to consume it.
                List<Appointment_Slot__c> asList = [SELECT Id, Appointment_Count__c FROM Appointment_Slot__c 
                WHERE Id =:availableSlotList[0].Id WITH SECURITY_ENFORCED FOR UPDATE];
                if(!asList.isEmpty() && asList[0].Appointment_Count__c != null && asList[0].Appointment_Count__c >= 1) {
                    return null;
                }
                else {
                    return availableSlotList[0];
                }
            }
            else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * description : This method checks if the appointment is of valid patient who tries to access information
    */
    public static void checkUserAccess(String contactId, String appointmentId) {
        User currentUser = [SELECT Id, ProfileId, Profile.Name, ContactId From User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if(currentUser.Profile.Name == VT_Constants.TVRS_COVID_CITIZEN_PROFILE) {
            List<Contact> contactsRelatedToCurrentUser = [SELECT Id FROM Contact WHERE Id =:currentUser.ContactId OR Parent_Contact__c =:currentUser.ContactId WITH SECURITY_ENFORCED];
            if(!contactsRelatedToCurrentUser.isEmpty()) {
                if(String.isNotBlank(appointmentId)) {
                    List<Appointment__c> appointments = [SELECT Id FROM Appointment__c WHERE Id =:appointmentId AND Patient__c IN :contactsRelatedToCurrentUser WITH SECURITY_ENFORCED LIMIT 1];
                    if(appointments.isEmpty()) {
                        throw new VT_Exception(VT_Constants.CRUD_PERMSSION_ERROR_MSG);
                    }
                } 
                if(String.isNotBlank(contactId)) {
                    Boolean isValidContact = false;
                    for (Contact c : [SELECT Id FROM Contact WHERE Id =:currentUser.ContactId OR Parent_Contact__c =:currentUser.ContactId WITH SECURITY_ENFORCED]) {
                        if(c.Id == contactid) {
                            isValidContact = true;
                            break;
                        }
                    }

                    if(!isValidContact) {
                        throw new VT_Exception(VT_Constants.CRUD_PERMSSION_ERROR_MSG);
                    }
                }
            }
        }
    }

    /**
     * @author: balram.dhawan@mtxb2b.com
     * @description: get Last/Maximum Dose Number
    */
    public static Integer getLastDoseNumber() {
        Schema.DescribeFieldResult fieldDescribe = Private_Access__c.Vaccine_Type__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntry = fieldDescribe.getPicklistValues();
        return Integer.valueOf(picklistEntry.get(picklistEntry.size()-1).getValue().split('-')[1]);
    }

    /**
     * @author: balram.dhawan@mtxb2b.com
     * @description: Find Next Dose
     * @return: Map<String, Object>{'isNextDoseAvailable' => true/false, 'nextValidDoseNumber' => 'Vaccination-1/Vaccination-2'}
    */
    public static Map<String, Object> validateNextDose(Appointment__c appointment) {
        Integer currentDose = Integer.valueOf(appointment.Event__r.Private_Access__r.Vaccine_Type__c.split('-')[1]);
        Map<String, Object> result = new Map<String, Object>();
        if(getLastDoseNumber() >= currentDose) {
            Integer nextVaccineDose = currentDose+1;
            result.put('isNextDoseAvailable', true);
            result.put('nextValidDoseNumber', 'Vaccination-'+nextVaccineDose);
        } else {
            result.put('isNextDoseAvailable', false);
        }
        return result;
    }
}