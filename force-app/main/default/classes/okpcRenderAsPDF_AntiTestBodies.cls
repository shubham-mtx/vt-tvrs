public class okpcRenderAsPDF_AntiTestBodies {
   public Antibody_Testing__c antiBodyData {get;set;}
    Public okpcRenderAsPDF_AntiTestBodies(){
        antiBodyData = [SELECT Id,Patient__r.patient_Id__c,Testing_Site__r.Name,
                        Appointment_Slot__r.Name,Status__c,
                        Appointment__r.Name FROM Antibody_Testing__c WHERE Id =: ApexPages.currentPage().getParameters().get('id')];
    }
}