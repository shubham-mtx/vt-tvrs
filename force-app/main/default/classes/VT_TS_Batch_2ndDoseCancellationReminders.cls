/* 
* Class Name: VT_TS_Batch_2ndDoseCancellationReminders
* Description: To send email reminder to the patients on their 2nd dose vaccination cancilation.
* Author : Shubham Dadhich
* Email : shubham.dadhich@mtxb2b.com
*
* Version       Modification        User         Date               Description
* V1            Initial             Shubham D    08-March-2021      Intial Creation of Class   
*/

global class VT_TS_Batch_2ndDoseCancellationReminders implements  Database.Batchable<sObject>, Database.Stateful, Schedulable{

    /**
     * Author Shubham Dadhich
     * Description: Default start :  Query On Appointment Object only for Second Dose
    */
    global Database.QueryLocator start(Database.BatchableContext batchContext) {

        // Appointment Query for Second Dose
        String query = 'SELECT Id, Appointment_Date__c, Event__c, Email_Workflow_Datetime__c,Email_Flow__c, Patient__c, No_Show__c, Status__c, Canceled_Date__c, Event_Process__c, Event__r.Private_Access__r.Vaccine_Type__c '+ 
        'FROM Appointment__c '+
        'WHERE Status__c = \'Cancelled\' AND No_Show__c = TRUE AND Event_Process__c = \'Vaccination\' AND '+
        'Event__r.Private_Access__r.Vaccine_Type__c = \'Vaccination-2\' AND Cancellation_Reminder_Completed__c = FALSE';

        return Database.getQueryLocator(query);
    }

    /**
     * Author Shubham Dadhich
     * Description: Default execute : To Send Email to the patient and Up date their field 
    */
    global void execute(Database.BatchableContext batchContext, List<Appointment__c> appointmentList) {

        //Get Days Configrations for the Reminders
        VT_TS_AppointmentReminderWrapper appointmentReminder = (VT_TS_AppointmentReminderWrapper) JSON.deserialize(System.Label.TVRS_APPOINTMENT_REMINDERS , VT_TS_AppointmentReminderWrapper.class);
        
        // Varibale Declaration
        Map<String, Appointment__c> mapOfContactAppointments = new Map<String, Appointment__c>();
        Set<String> setOfContactIds = new Set<String>();
        List<Appointment__c> listOfAppointmentRecordToUpdate = new List<Appointment__c>();

        //Step 1: Store Contact Ids in a Set
        for(Appointment__c appointmentRecord: appointmentList){
            setOfContactIds.add(appointmentRecord.Patient__c);
        }

        //Step 1.1: Query on their existing Scheduled or Completed Second Dose Appointment
        for(Appointment__c appointmentRecord : [SELECT Id, Status__c, Patient__c FROM Appointment__c WHERE Patient__c IN: setOfContactIds AND (Status__c = 'Completed' OR Status__c = 'Scheduled') AND Event__r.Private_Access__r.Vaccine_Type__c = 'Vaccination-2' AND Event_Process__c = 'Vaccination']){
            mapOfContactAppointments.put(appointmentRecord.Patient__c, appointmentRecord);
        }

        //Step 2: Loop Over AppointmentList and Send Email Accordingly
        for(Appointment__c appointmentRecord: appointmentList){
            
            //Step 2.1: Check for is 2nd Appointment is Scheduled or Completed
            if(!mapOfContactAppointments.containsKey(appointmentRecord.Patient__c)){

                //Calculate Number of Days Remaing to Send Reminder
                Date runnigDateOfClass = Date.today().addDays(appointmentReminder.day_to_add_for_testing);
                Integer daysRemaining = runnigDateOfClass.daysBetween(appointmentRecord.Canceled_Date__c);

                //Step 2.2: Check for is Days Remaining to send Reminder
                if(daysRemaining <= appointmentReminder.cancilation_reminder_total_days){

                    //Get Notification Date Difference
                    Integer notificationValidateValue = Math.mod((System.today()).daysBetween(appointmentRecord.Canceled_Date__c),appointmentReminder.cancilation_reminder_gape_days);

                    //Step 2.2.1: Validate the above result is getting 0
                    if(notificationValidateValue == 0){

                        appointmentRecord.Email_Workflow_Datetime__c = Datetime.now();
                        appointmentRecord.Email_Flow__c = 'Cancelled 2nd Dose Reminder';
                        listOfAppointmentRecordToUpdate.add(appointmentRecord);
                    }

                }else{
                    // Turn off the notification if days passes
                    appointmentRecord.Cancellation_Reminder_Completed__c = true;
                    listOfAppointmentRecordToUpdate.add(appointmentRecord);
                }
            }else{
                // Turn off the notification if days passes
                appointmentRecord.Cancellation_Reminder_Completed__c = true;
                listOfAppointmentRecordToUpdate.add(appointmentRecord);
            }
        }

        //Step 3: Update Appointments With respected Updates
        if(!listOfAppointmentRecordToUpdate.isEmpty()){
            VT_TS_SkipTriggerExecution.executeAppointmentTrigger = false;
            UPDATE listOfAppointmentRecordToUpdate;
        }

        

    }

    /**
     * Author Shubham Dadhich
     * Description: Default execute: For Schedule Job
    */
    global void execute(SchedulableContext ctx) {
        VT_TS_Batch_2ndDoseCancellationReminders batch = new VT_TS_Batch_2ndDoseCancellationReminders();
        Database.executeBatch(batch, 200);

        /** Cron Expression: To schedule job for every one hour starting from execution
        * VT_TS_Batch_2ndDoseCancellationReminders sc = new VT_TS_Batch_2ndDoseCancellationReminders();
        * String cronExp = '0 0 2 1/1 * ? *';
        * String jobID = System.schedule('Hourly Scheduled Job to update VT_TS_Batch_2ndDoseCancellationReminders', cronExp, sc);
        **/
    }

    /**
     * Author Shubham Dadhich
     * Description: Default finish : 
    */
    global void finish(Database.BatchableContext batchContext) {
        
    }
}