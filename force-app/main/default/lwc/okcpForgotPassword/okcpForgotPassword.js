import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import forgotPassword from '@salesforce/apex/okpcRegisterController.forgotPassword';
import loginURL from '@salesforce/label/c.OKCP_Login_URL';
import OKCP_FORGOT_PASSWORD_CONFIRMATION_MESSAGE from '@salesforce/label/c.OKCP_Forgot_Password_Confirmation_Message';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class OkpcForgotPassword extends NavigationMixin(LightningElement) {

    @track showError = false;
    @track errorMessage = '';
    @track username = '';

    customImage = nysdohResource;

    navigateToLogin() {
        location.href = loginURL;
    }

    nameChange(event) {
        this.username = event.target.value;
    }

    doForgotPasswordCheck() {

        this.showError = false;

        if (this.username === '' || typeof this.username === 'undefined') {
            this.errorMessage = 'Please provide an email address.';
            this.showError = true;
            let input = this.template.querySelector('.slds-input');
            input.focus();
            return;
        }

        if (!this.validateEmail(this.username)) {
            this.errorMessage = 'Please provide a valid email address. example: "abc@xyz.com"';
            this.showError = true;
            let input = this.template.querySelector('.slds-input');
            input.focus();
            return;
        }

        forgotPassword({
            userName: this.username,
            delimiter: '.COVID'
        })//Added delimiter by Sajal
            .then(data => {
                if(data === 'success'){
                    this.dispatchEvent(new ShowToastEvent({
                        title: '',
                        variant: 'success',
                        message: OKCP_FORGOT_PASSWORD_CONFIRMATION_MESSAGE
                    }));
                    setTimeout(() => {
                        this.navigateToLogin();
                    }, 3000);
                }else{

                    if(data === 'No user found with this Email'){
                        this.dispatchEvent(new ShowToastEvent({
                            title: '',
                            variant: 'error',
                            message: 'No user found with this Email'
                        }));
                    }else if( data  === 'Non-Verified User' ) {
                        this.dispatchEvent(new ShowToastEvent({
                            title: '',
                            variant: 'warning',
                            message: 'Your account verification process is incomplete, we have sent you an email requesting final verification of your account.'                            
                        }));
                    }
                    else{
                        this.dispatchEvent(new ShowToastEvent({
                            title: 'Error',
                            variant: 'error',
                            message: data                           
                        }));
                    }
                    
                }
            }).catch(error => {
                // error handling
            });

    }
    validateEmail(email) {
        var regExpEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;
        var isValid = email.match(regExpEmail);
        return isValid;
    }

}