({
    doInit: function (component, event, helper) {     
        if( location.pathname.includes('search-events') ) {
            component.set('v.activeClass', 'active');
        } else {
            component.set("v.searchEventUrl", "https://"+location.hostname+"/events/s/search-events")
        }        


       let eId = helper.findGetParameter('eventId');

        var loginlabel = $A.get("$Label.c.OKCP_Login_URL");
        loginlabel = (eId) ? loginlabel + '?eventId=' + eId : loginlabel;
        component.set("v.loginURL", loginlabel);

        var registrationlabel = $A.get("$Label.c.OKCP_Registration_URL");
        var pageUrl = registrationlabel.split('/');
        var pageName = pageUrl.pop();
        var modifiedUrlName = pageName.substring(2, pageName.length + 1);
        var modifiedUrl = "https://";
        for (var i = 1; i < pageUrl.length; i++) {
            if (pageUrl != "") {
                modifiedUrl += pageUrl[i] + '/';
            }
        }
        var modifiedRegisteratioUrl = modifiedUrl + modifiedUrlName;

        registrationlabel = (eId) ? registrationlabel + '?eventId=' + eId : registrationlabel;
        component.set("v.registrationURL", registrationlabel);

        var action = component.get("c.getUserDetails");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var userDetails = response.getReturnValue();
                component.set("v.userName", userDetails.Name);
                component.set("v.imageResource", userDetails.SmallPhotoUrl);
                if( userDetails.UserType === 'Guest' ) {
                    component.set("v.isGuest", true);    
                }
            }
        });
        $A.enqueueAction(action);
        
    },

    redirectToLogin: function (component, event, helper) {
        var logoutRedirectURl = $A.get("$Label.c.OKCP_LogOut_URL");
        window.location.replace(logoutRedirectURl);
    },

    redirectToSearchEvents : function () {
        window.location.replace("https://"+location.hostname+"/events/s/search-events");
    }
    
})