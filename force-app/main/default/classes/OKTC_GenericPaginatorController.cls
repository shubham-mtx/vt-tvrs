/**
 * @description       : 
 * @author            : Mohit Karani
 * @group             : 
 * @last modified on  : 01-04-2022
 * @last modified by  : Vamsi Mudaliar
**/
public without sharing class OKTC_GenericPaginatorController {
    
    @AuraEnabled
    public static Map<String,Object> retrieveRecords(Map<String,Object> parameters) {
        if(VT_SecurityLibrary.UserHasProfileAccess(VT_Constants.TVRS_PROFILES) || VT_SecurityLibrary.UserHasPermissionSetAccess(VT_Constants.TVRS_PERMISSION_SETS)){
            return retrieveRecordsWithSharing(parameters);
        }else{
            throw new VT_Exception('You are not Authorized to perform this action');
        }
            
    }

    public static Map<String,Object> retrieveRecordsWithSharing(Map<String,Object> parameters) {
        Map<String,Object> results = new Map<String,Object>();
        
        //Step 1: Base Query
        String queryString = 'SELECT Id';
        for(object obj : (List<Object>)parameters.get('fields')) {
            if(!((String)obj).equalsIgnoreCase('Id'))
                queryString += ', ' + (String)obj;
        }
        queryString += ' FROM ' + parameters.get('objectName');
        //Step 2: Page Limits
        Integer pageDifference = Integer.valueof(parameters.get('newPageNo')) - Integer.valueof(parameters.get('previousPageNo'));
        Integer pageLimit = (pageDifference > 0 ? pageDifference : pageDifference * -1) * Integer.valueof(parameters.get('limit'));
        String filters = '';
        
        //Step 3: Searching
        String searchString = parameters.containsKey('searchStr') ? (String)parameters.get('searchStr') : '';
        if( String.isNotBlank((searchString))) {
            filters += ' ( '; 
            for(Object obj : (List<Object>)parameters.get('fields')) {
                if((String.valueof(obj)).contains('.') ) {
                    if( !(String.valueof(obj)).contains('Private_Access__r') ){
                        List<String> objFieldList = (String.valueof(obj)).split('\\.');
                        String parentFieldName = objFieldList[objFieldList.size() - 2].replaceAll('__r','__c');
                        String objectName  = Schema.getGlobalDescribe().get((String)parameters.get('objectName')).getDescribe().fields.getMap().get((String)parentFieldName).getDescribe().getReferenceTo()[0].getDescribe().getName();

                        String fieldName = objFieldList[objFieldList.size() - 1];

                        Schema.DisplayType displayType = Schema.getGlobalDescribe().get((String)objectName).getDescribe().fields.getMap().get((String)fieldName).getDescribe().getType();
                        if( displayType == Schema.DisplayType.String || displayType == Schema.DisplayType.Email) {
                            filters += filters != ' ( ' ? ' OR ' : ' ';
                            filters +=  (String)obj + ' LIKE \'%' + String.escapeSinglequotes(searchString) + '%\' ';
                        }
                    }
                }
                else {
                    Schema.DisplayType displayType = Schema.getGlobalDescribe().get((String)parameters.get('objectName')).getDescribe().fields.getMap().get((String)obj).getDescribe().getType();
                    if( displayType == Schema.DisplayType.String || displayType == Schema.DisplayType.Email) {
                        filters += filters != ' ( ' ? ' OR ' : ' ';
                        filters +=  (String)obj + ' LIKE \'%' + String.escapeSinglequotes(searchString) + '%\' ';
                    }
                }
            }
            filters += ' )';
        }

        //Step 4: Add where condition is specified
        if(parameters.containsKey('whereClause') && String.isNotBlank((String)parameters.get('whereClause'))) {
            filters += filters != '' ? ' AND ' : '';
            filters += parameters.get('whereClause');
        }

     
        //Step 5: Record Counts
        String countFilter = filters == '' ? '' : ' WHERE ' + filters;
        Integer reccount = Database.countQuery('SELECT Count() FROM ' + parameters.get('objectName') + countFilter);
        results.put('totalCount',reccount);

        //Step 6: Sorting
        String recId = (String)parameters.get('recId');
        String recVal = parameters.get('recVal') != null ? String.escapeSinglequotes((String)parameters.get('recVal')) : '';
        if(parameters.get('newPageNo') != 1 && parameters.get('newPageNo') != parameters.get('maxPageNo')) {
            
            filters = filters != '' ? ' WHERE ' +  filters : filters;

            if(pageDifference > 0 && String.isNotBlank(recId)) {
                filters += ' ORDER BY ' + parameters.get('sortBy') + ' ' + parameters.get('sortDir') + '  NULLS FIRST ';
            } else if(pageDifference < 0 && String.isNotBlank(recId)) {
                String sortDir = parameters.get('sortDir') == 'ASC' ? ' DESC ' : ' ASC '; 
                filters += ' ORDER BY ' + parameters.get('sortBy') + ' ' + parameters.get('sortDir') + '  NULLS LAST ';
            }

        }
        else if(parameters.get('newPageNo') != 1 && parameters.get('newPageNo') == parameters.get('maxPageNo')) {
            pageLimit = Math.mod(reccount , Integer.valueof(parameters.get('limit'))) == 0 ? Integer.valueof(parameters.get('limit')) : Math.mod(reccount , Integer.valueof(parameters.get('limit')));
            String sortDir = parameters.get('sortDir') == 'ASC' ? ' DESC ' : ' ASC '; 
            filters = filters != '' ? ' WHERE ' +  filters : filters;
            filters += ' ORDER BY ' + parameters.get('sortBy') + ' ' + parameters.get('sortDir') + '  NULLS LAST  ';
        }
        else {
            filters = filters != '' ? ' WHERE ' +  filters : filters;
            filters += ' ORDER BY ' + parameters.get('sortBy') + ' ' + parameters.get('sortDir') + '  NULLS FIRST ';
        }
        
        //Step 7: Limit
        if(parameters.containsKey('limit')) {
            // filters += ' LIMIT ' + pageLimit;
            filters += ' LIMIT 20000';
        }

        List<Sobject> records = new List<Sobject>();
        results.put('query',queryString + filters);

      
        //Step 8: Query
        List<Sobject> queryResult = Database.query(queryString + filters);

        //Step 9: Pagination
        integer pLimit = Integer.valueof( parameters.get('limit') );
        integer sInd = Integer.valueof(parameters.get('newPageNo'));
        integer eInd = sInd * pLimit;
        eInd = eInd > queryResult.size() ? queryResult.size() : eInd;
      
        for(integer i = (sInd*pLimit)-pLimit ; i < eInd ; i++  ){
            records.add( queryResult[i] );
        }
        
        results.put('allrecords',queryResult);
        results.put('records',records);
        return results;//;
    }

    public static Boolean displayScheduleNextDoseButton(List<Appointment__c> records, Appointment__c currentRec){
        Boolean isCurrentDoseHighest = true;
      
        return isCurrentDoseHighest;
    }

    
}